﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>
struct U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2286;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2280;

// System.Void System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::.ctor()
extern "C" void U3CCreateRepeatIteratorU3Ec__IteratorE_1__ctor_m14315_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2286 * __this, const MethodInfo* method);
#define U3CCreateRepeatIteratorU3Ec__IteratorE_1__ctor_m14315(__this, method) (( void (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2286 *, const MethodInfo*))U3CCreateRepeatIteratorU3Ec__IteratorE_1__ctor_m14315_gshared)(__this, method)
// TResult System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C" Object_t * U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m14316_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2286 * __this, const MethodInfo* method);
#define U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m14316(__this, method) (( Object_t * (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2286 *, const MethodInfo*))U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m14316_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_IEnumerator_get_Current_m14317_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2286 * __this, const MethodInfo* method);
#define U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_IEnumerator_get_Current_m14317(__this, method) (( Object_t * (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2286 *, const MethodInfo*))U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_IEnumerator_get_Current_m14317_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_IEnumerable_GetEnumerator_m14318_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2286 * __this, const MethodInfo* method);
#define U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_IEnumerable_GetEnumerator_m14318(__this, method) (( Object_t * (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2286 *, const MethodInfo*))U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_IEnumerable_GetEnumerator_m14318_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C" Object_t* U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m14319_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2286 * __this, const MethodInfo* method);
#define U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m14319(__this, method) (( Object_t* (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2286 *, const MethodInfo*))U3CCreateRepeatIteratorU3Ec__IteratorE_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m14319_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::MoveNext()
extern "C" bool U3CCreateRepeatIteratorU3Ec__IteratorE_1_MoveNext_m14320_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2286 * __this, const MethodInfo* method);
#define U3CCreateRepeatIteratorU3Ec__IteratorE_1_MoveNext_m14320(__this, method) (( bool (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2286 *, const MethodInfo*))U3CCreateRepeatIteratorU3Ec__IteratorE_1_MoveNext_m14320_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::Dispose()
extern "C" void U3CCreateRepeatIteratorU3Ec__IteratorE_1_Dispose_m14321_gshared (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2286 * __this, const MethodInfo* method);
#define U3CCreateRepeatIteratorU3Ec__IteratorE_1_Dispose_m14321(__this, method) (( void (*) (U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2286 *, const MethodInfo*))U3CCreateRepeatIteratorU3Ec__IteratorE_1_Dispose_m14321_gshared)(__this, method)
