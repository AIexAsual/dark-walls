﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t545;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t1093;
// System.Collections.Generic.ICollection`1<System.Type>
struct ICollection_1_t3003;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Type>
struct KeyCollection_t2323;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Type>
struct ValueCollection_t2324;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t2320;
// System.Collections.Generic.IDictionary`2<System.String,System.Type>
struct IDictionary_2_t3004;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1104;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Type>[]
struct KeyValuePair_2U5BU5D_t3005;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Type>>
struct IEnumerator_1_t3006;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1527;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Type>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Type>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_18MethodDeclarations.h"
#define Dictionary_2__ctor_m3288(__this, method) (( void (*) (Dictionary_2_t545 *, const MethodInfo*))Dictionary_2__ctor_m14935_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m14936(__this, ___comparer, method) (( void (*) (Dictionary_2_t545 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14937_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m14938(__this, ___dictionary, method) (( void (*) (Dictionary_2_t545 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14939_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::.ctor(System.Int32)
#define Dictionary_2__ctor_m14940(__this, ___capacity, method) (( void (*) (Dictionary_2_t545 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m14941_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m14942(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t545 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14943_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m14944(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t545 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2__ctor_m14945_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m14946(__this, method) (( Object_t* (*) (Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m14947_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m14948(__this, method) (( Object_t* (*) (Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m14949_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m14950(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t545 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m14951_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m14952(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t545 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m14953_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m14954(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t545 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m14955_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m14956(__this, ___key, method) (( bool (*) (Dictionary_2_t545 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m14957_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m14958(__this, ___key, method) (( void (*) (Dictionary_2_t545 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m14959_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14960(__this, method) (( bool (*) (Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14961_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14962(__this, method) (( Object_t * (*) (Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14963_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14964(__this, method) (( bool (*) (Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14965_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14966(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t545 *, KeyValuePair_2_t2322 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14967_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14968(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t545 *, KeyValuePair_2_t2322 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14969_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14970(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t545 *, KeyValuePair_2U5BU5D_t3005*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14971_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14972(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t545 *, KeyValuePair_2_t2322 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14973_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m14974(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t545 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m14975_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14976(__this, method) (( Object_t * (*) (Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14977_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14978(__this, method) (( Object_t* (*) (Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14979_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Type>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14980(__this, method) (( Object_t * (*) (Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14981_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Type>::get_Count()
#define Dictionary_2_get_Count_m14982(__this, method) (( int32_t (*) (Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_get_Count_m14983_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Type>::get_Item(TKey)
#define Dictionary_2_get_Item_m14984(__this, ___key, method) (( Type_t * (*) (Dictionary_2_t545 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m14985_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m14986(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t545 *, String_t*, Type_t *, const MethodInfo*))Dictionary_2_set_Item_m14987_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m14988(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t545 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m14989_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m14990(__this, ___size, method) (( void (*) (Dictionary_2_t545 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m14991_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m14992(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t545 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m14993_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Type>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m14994(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2322  (*) (Object_t * /* static, unused */, String_t*, Type_t *, const MethodInfo*))Dictionary_2_make_pair_m14995_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Type>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m14996(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, Type_t *, const MethodInfo*))Dictionary_2_pick_key_m14997_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Type>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m14998(__this /* static, unused */, ___key, ___value, method) (( Type_t * (*) (Object_t * /* static, unused */, String_t*, Type_t *, const MethodInfo*))Dictionary_2_pick_value_m14999_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m15000(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t545 *, KeyValuePair_2U5BU5D_t3005*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m15001_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::Resize()
#define Dictionary_2_Resize_m15002(__this, method) (( void (*) (Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_Resize_m15003_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::Add(TKey,TValue)
#define Dictionary_2_Add_m15004(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t545 *, String_t*, Type_t *, const MethodInfo*))Dictionary_2_Add_m15005_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::Clear()
#define Dictionary_2_Clear_m15006(__this, method) (( void (*) (Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_Clear_m15007_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Type>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m15008(__this, ___key, method) (( bool (*) (Dictionary_2_t545 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m15009_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Type>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m15010(__this, ___value, method) (( bool (*) (Dictionary_2_t545 *, Type_t *, const MethodInfo*))Dictionary_2_ContainsValue_m15011_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m15012(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t545 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2_GetObjectData_m15013_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Type>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m15014(__this, ___sender, method) (( void (*) (Dictionary_2_t545 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m15015_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Type>::Remove(TKey)
#define Dictionary_2_Remove_m15016(__this, ___key, method) (( bool (*) (Dictionary_2_t545 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m15017_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Type>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m15018(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t545 *, String_t*, Type_t **, const MethodInfo*))Dictionary_2_TryGetValue_m15019_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Type>::get_Keys()
#define Dictionary_2_get_Keys_m15020(__this, method) (( KeyCollection_t2323 * (*) (Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_get_Keys_m15021_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Type>::get_Values()
#define Dictionary_2_get_Values_m15022(__this, method) (( ValueCollection_t2324 * (*) (Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_get_Values_m15023_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Type>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m15024(__this, ___key, method) (( String_t* (*) (Dictionary_2_t545 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m15025_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Type>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m15026(__this, ___value, method) (( Type_t * (*) (Dictionary_2_t545 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m15027_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Type>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m15028(__this, ___pair, method) (( bool (*) (Dictionary_2_t545 *, KeyValuePair_2_t2322 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m15029_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Type>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m15030(__this, method) (( Enumerator_t2325  (*) (Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_GetEnumerator_m15031_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,System.Type>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m15032(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t552  (*) (Object_t * /* static, unused */, String_t*, Type_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m15033_gshared)(__this /* static, unused */, ___key, ___value, method)
