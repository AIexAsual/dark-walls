﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// CameraFilterPack_Vision_Warp
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Warp.h"
// Metadata Definition CameraFilterPack_Vision_Warp
extern TypeInfo CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo;
// CameraFilterPack_Vision_Warp
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_WarpMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Vision_Warp_t210_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Vision_Warp_t210_0_0_0;
extern const Il2CppType CameraFilterPack_Vision_Warp_t210_1_0_0;
extern const Il2CppType MonoBehaviour_t4_0_0_0;
struct CameraFilterPack_Vision_Warp_t210;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Vision_Warp_t210_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Vision_Warp_t210_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1762/* fieldStart */
	, 1366/* methodStart */
	, -1/* eventStart */
	, 198/* propertyStart */

};
TypeInfo CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Vision_Warp"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 643/* custom_attributes_cache */
	, &CameraFilterPack_Vision_Warp_t210_0_0_0/* byval_arg */
	, &CameraFilterPack_Vision_Warp_t210_1_0_0/* this_arg */
	, &CameraFilterPack_Vision_Warp_t210_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Vision_Warp_t210)/* instance_size */
	, sizeof (CameraFilterPack_Vision_Warp_t210)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Vision_Warp_t210_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Vision_Warp2
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Warp2.h"
// Metadata Definition CameraFilterPack_Vision_Warp2
extern TypeInfo CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo;
// CameraFilterPack_Vision_Warp2
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Warp2MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Vision_Warp2_t211_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Vision_Warp2_t211_0_0_0;
extern const Il2CppType CameraFilterPack_Vision_Warp2_t211_1_0_0;
struct CameraFilterPack_Vision_Warp2_t211;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Vision_Warp2_t211_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Vision_Warp2_t211_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1774/* fieldStart */
	, 1373/* methodStart */
	, -1/* eventStart */
	, 199/* propertyStart */

};
TypeInfo CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Vision_Warp2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 648/* custom_attributes_cache */
	, &CameraFilterPack_Vision_Warp2_t211_0_0_0/* byval_arg */
	, &CameraFilterPack_Vision_Warp2_t211_1_0_0/* this_arg */
	, &CameraFilterPack_Vision_Warp2_t211_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Vision_Warp2_t211)/* instance_size */
	, sizeof (CameraFilterPack_Vision_Warp2_t211)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Vision_Warp2_t211_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// FPS
#include "AssemblyU2DCSharp_FPS.h"
// Metadata Definition FPS
extern TypeInfo FPS_t213_il2cpp_TypeInfo;
// FPS
#include "AssemblyU2DCSharp_FPSMethodDeclarations.h"
static const EncodedMethodIndex FPS_t213_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType FPS_t213_0_0_0;
extern const Il2CppType FPS_t213_1_0_0;
struct FPS_t213;
const Il2CppTypeDefinitionMetadata FPS_t213_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, FPS_t213_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1786/* fieldStart */
	, 1380/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FPS_t213_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "FPS"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &FPS_t213_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 653/* custom_attributes_cache */
	, &FPS_t213_0_0_0/* byval_arg */
	, &FPS_t213_1_0_0/* this_arg */
	, &FPS_t213_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FPS_t213)/* instance_size */
	, sizeof (FPS_t213)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Teleport
#include "AssemblyU2DCSharp_Teleport.h"
// Metadata Definition Teleport
extern TypeInfo Teleport_t214_il2cpp_TypeInfo;
// Teleport
#include "AssemblyU2DCSharp_TeleportMethodDeclarations.h"
static const EncodedMethodIndex Teleport_t214_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Teleport_t214_0_0_0;
extern const Il2CppType Teleport_t214_1_0_0;
struct Teleport_t214;
const Il2CppTypeDefinitionMetadata Teleport_t214_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, Teleport_t214_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1788/* fieldStart */
	, 1383/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Teleport_t214_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Teleport"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Teleport_t214_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 654/* custom_attributes_cache */
	, &Teleport_t214_0_0_0/* byval_arg */
	, &Teleport_t214_1_0_0/* this_arg */
	, &Teleport_t214_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Teleport_t214)/* instance_size */
	, sizeof (Teleport_t214)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CardboardOnGUI
#include "AssemblyU2DCSharp_CardboardOnGUI.h"
// Metadata Definition CardboardOnGUI
extern TypeInfo CardboardOnGUI_t220_il2cpp_TypeInfo;
// CardboardOnGUI
#include "AssemblyU2DCSharp_CardboardOnGUIMethodDeclarations.h"
extern const Il2CppType OnGUICallback_t218_0_0_0;
static const Il2CppType* CardboardOnGUI_t220_il2cpp_TypeInfo__nestedTypes[1] =
{
	&OnGUICallback_t218_0_0_0,
};
static const EncodedMethodIndex CardboardOnGUI_t220_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CardboardOnGUI_t220_0_0_0;
extern const Il2CppType CardboardOnGUI_t220_1_0_0;
struct CardboardOnGUI_t220;
const Il2CppTypeDefinitionMetadata CardboardOnGUI_t220_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CardboardOnGUI_t220_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CardboardOnGUI_t220_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1789/* fieldStart */
	, 1389/* methodStart */
	, 0/* eventStart */
	, 200/* propertyStart */

};
TypeInfo CardboardOnGUI_t220_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CardboardOnGUI"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CardboardOnGUI_t220_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CardboardOnGUI_t220_0_0_0/* byval_arg */
	, &CardboardOnGUI_t220_1_0_0/* this_arg */
	, &CardboardOnGUI_t220_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CardboardOnGUI_t220)/* instance_size */
	, sizeof (CardboardOnGUI_t220)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CardboardOnGUI_t220_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CardboardOnGUI/OnGUICallback
#include "AssemblyU2DCSharp_CardboardOnGUI_OnGUICallback.h"
// Metadata Definition CardboardOnGUI/OnGUICallback
extern TypeInfo OnGUICallback_t218_il2cpp_TypeInfo;
// CardboardOnGUI/OnGUICallback
#include "AssemblyU2DCSharp_CardboardOnGUI_OnGUICallbackMethodDeclarations.h"
static const EncodedMethodIndex OnGUICallback_t218_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	641,
	642,
	643,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
extern const Il2CppType ISerializable_t2210_0_0_0;
static Il2CppInterfaceOffsetPair OnGUICallback_t218_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType OnGUICallback_t218_1_0_0;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
struct OnGUICallback_t218;
const Il2CppTypeDefinitionMetadata OnGUICallback_t218_DefinitionMetadata = 
{
	&CardboardOnGUI_t220_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnGUICallback_t218_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, OnGUICallback_t218_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1401/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnGUICallback_t218_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnGUICallback"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &OnGUICallback_t218_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OnGUICallback_t218_0_0_0/* byval_arg */
	, &OnGUICallback_t218_1_0_0/* this_arg */
	, &OnGUICallback_t218_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_OnGUICallback_t218/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnGUICallback_t218)/* instance_size */
	, sizeof (OnGUICallback_t218)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// CardboardOnGUIMouse
#include "AssemblyU2DCSharp_CardboardOnGUIMouse.h"
// Metadata Definition CardboardOnGUIMouse
extern TypeInfo CardboardOnGUIMouse_t222_il2cpp_TypeInfo;
// CardboardOnGUIMouse
#include "AssemblyU2DCSharp_CardboardOnGUIMouseMethodDeclarations.h"
static const EncodedMethodIndex CardboardOnGUIMouse_t222_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CardboardOnGUIMouse_t222_0_0_0;
extern const Il2CppType CardboardOnGUIMouse_t222_1_0_0;
struct CardboardOnGUIMouse_t222;
const Il2CppTypeDefinitionMetadata CardboardOnGUIMouse_t222_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CardboardOnGUIMouse_t222_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1794/* fieldStart */
	, 1405/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CardboardOnGUIMouse_t222_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CardboardOnGUIMouse"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CardboardOnGUIMouse_t222_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CardboardOnGUIMouse_t222_0_0_0/* byval_arg */
	, &CardboardOnGUIMouse_t222_1_0_0/* this_arg */
	, &CardboardOnGUIMouse_t222_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CardboardOnGUIMouse_t222)/* instance_size */
	, sizeof (CardboardOnGUIMouse_t222)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CardboardOnGUIWindow
#include "AssemblyU2DCSharp_CardboardOnGUIWindow.h"
// Metadata Definition CardboardOnGUIWindow
extern TypeInfo CardboardOnGUIWindow_t224_il2cpp_TypeInfo;
// CardboardOnGUIWindow
#include "AssemblyU2DCSharp_CardboardOnGUIWindowMethodDeclarations.h"
static const EncodedMethodIndex CardboardOnGUIWindow_t224_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CardboardOnGUIWindow_t224_0_0_0;
extern const Il2CppType CardboardOnGUIWindow_t224_1_0_0;
struct CardboardOnGUIWindow_t224;
const Il2CppTypeDefinitionMetadata CardboardOnGUIWindow_t224_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CardboardOnGUIWindow_t224_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1800/* fieldStart */
	, 1408/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CardboardOnGUIWindow_t224_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CardboardOnGUIWindow"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CardboardOnGUIWindow_t224_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 659/* custom_attributes_cache */
	, &CardboardOnGUIWindow_t224_0_0_0/* byval_arg */
	, &CardboardOnGUIWindow_t224_1_0_0/* this_arg */
	, &CardboardOnGUIWindow_t224_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CardboardOnGUIWindow_t224)/* instance_size */
	, sizeof (CardboardOnGUIWindow_t224)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SkyboxMesh
#include "AssemblyU2DCSharp_SkyboxMesh.h"
// Metadata Definition SkyboxMesh
extern TypeInfo SkyboxMesh_t226_il2cpp_TypeInfo;
// SkyboxMesh
#include "AssemblyU2DCSharp_SkyboxMeshMethodDeclarations.h"
static const EncodedMethodIndex SkyboxMesh_t226_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SkyboxMesh_t226_0_0_0;
extern const Il2CppType SkyboxMesh_t226_1_0_0;
struct SkyboxMesh_t226;
const Il2CppTypeDefinitionMetadata SkyboxMesh_t226_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, SkyboxMesh_t226_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1414/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SkyboxMesh_t226_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SkyboxMesh"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SkyboxMesh_t226_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 661/* custom_attributes_cache */
	, &SkyboxMesh_t226_0_0_0/* byval_arg */
	, &SkyboxMesh_t226_1_0_0/* this_arg */
	, &SkyboxMesh_t226_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SkyboxMesh_t226)/* instance_size */
	, sizeof (SkyboxMesh_t226)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// StereoLensFlare
#include "AssemblyU2DCSharp_StereoLensFlare.h"
// Metadata Definition StereoLensFlare
extern TypeInfo StereoLensFlare_t227_il2cpp_TypeInfo;
// StereoLensFlare
#include "AssemblyU2DCSharp_StereoLensFlareMethodDeclarations.h"
static const EncodedMethodIndex StereoLensFlare_t227_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType StereoLensFlare_t227_0_0_0;
extern const Il2CppType StereoLensFlare_t227_1_0_0;
struct StereoLensFlare_t227;
const Il2CppTypeDefinitionMetadata StereoLensFlare_t227_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, StereoLensFlare_t227_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1416/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StereoLensFlare_t227_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "StereoLensFlare"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &StereoLensFlare_t227_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 662/* custom_attributes_cache */
	, &StereoLensFlare_t227_0_0_0/* byval_arg */
	, &StereoLensFlare_t227_1_0_0/* this_arg */
	, &StereoLensFlare_t227_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StereoLensFlare_t227)/* instance_size */
	, sizeof (StereoLensFlare_t227)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Cardboard
#include "AssemblyU2DCSharp_Cardboard.h"
// Metadata Definition Cardboard
extern TypeInfo Cardboard_t233_il2cpp_TypeInfo;
// Cardboard
#include "AssemblyU2DCSharp_CardboardMethodDeclarations.h"
extern const Il2CppType DistortionCorrectionMethod_t228_0_0_0;
extern const Il2CppType BackButtonModes_t229_0_0_0;
extern const Il2CppType Eye_t230_0_0_0;
extern const Il2CppType Distortion_t231_0_0_0;
extern const Il2CppType StereoScreenChangeDelegate_t232_0_0_0;
extern const Il2CppType U3CEndOfFrameU3Ec__Iterator1_t234_0_0_0;
static const Il2CppType* Cardboard_t233_il2cpp_TypeInfo__nestedTypes[6] =
{
	&DistortionCorrectionMethod_t228_0_0_0,
	&BackButtonModes_t229_0_0_0,
	&Eye_t230_0_0_0,
	&Distortion_t231_0_0_0,
	&StereoScreenChangeDelegate_t232_0_0_0,
	&U3CEndOfFrameU3Ec__Iterator1_t234_0_0_0,
};
static const EncodedMethodIndex Cardboard_t233_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Cardboard_t233_0_0_0;
extern const Il2CppType Cardboard_t233_1_0_0;
struct Cardboard_t233;
const Il2CppTypeDefinitionMetadata Cardboard_t233_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Cardboard_t233_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, Cardboard_t233_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1802/* fieldStart */
	, 1418/* methodStart */
	, 1/* eventStart */
	, 202/* propertyStart */

};
TypeInfo Cardboard_t233_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Cardboard"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Cardboard_t233_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Cardboard_t233_0_0_0/* byval_arg */
	, &Cardboard_t233_1_0_0/* this_arg */
	, &Cardboard_t233_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Cardboard_t233)/* instance_size */
	, sizeof (Cardboard_t233)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Cardboard_t233_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 86/* method_count */
	, 31/* property_count */
	, 30/* field_count */
	, 5/* event_count */
	, 6/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Cardboard/DistortionCorrectionMethod
#include "AssemblyU2DCSharp_Cardboard_DistortionCorrectionMethod.h"
// Metadata Definition Cardboard/DistortionCorrectionMethod
extern TypeInfo DistortionCorrectionMethod_t228_il2cpp_TypeInfo;
// Cardboard/DistortionCorrectionMethod
#include "AssemblyU2DCSharp_Cardboard_DistortionCorrectionMethodMethodDeclarations.h"
static const EncodedMethodIndex DistortionCorrectionMethod_t228_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair DistortionCorrectionMethod_t228_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DistortionCorrectionMethod_t228_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata DistortionCorrectionMethod_t228_DefinitionMetadata = 
{
	&Cardboard_t233_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DistortionCorrectionMethod_t228_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, DistortionCorrectionMethod_t228_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1832/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DistortionCorrectionMethod_t228_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DistortionCorrectionMethod"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DistortionCorrectionMethod_t228_0_0_0/* byval_arg */
	, &DistortionCorrectionMethod_t228_1_0_0/* this_arg */
	, &DistortionCorrectionMethod_t228_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DistortionCorrectionMethod_t228)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DistortionCorrectionMethod_t228)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Cardboard/BackButtonModes
#include "AssemblyU2DCSharp_Cardboard_BackButtonModes.h"
// Metadata Definition Cardboard/BackButtonModes
extern TypeInfo BackButtonModes_t229_il2cpp_TypeInfo;
// Cardboard/BackButtonModes
#include "AssemblyU2DCSharp_Cardboard_BackButtonModesMethodDeclarations.h"
static const EncodedMethodIndex BackButtonModes_t229_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair BackButtonModes_t229_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType BackButtonModes_t229_1_0_0;
const Il2CppTypeDefinitionMetadata BackButtonModes_t229_DefinitionMetadata = 
{
	&Cardboard_t233_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BackButtonModes_t229_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, BackButtonModes_t229_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1836/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BackButtonModes_t229_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "BackButtonModes"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BackButtonModes_t229_0_0_0/* byval_arg */
	, &BackButtonModes_t229_1_0_0/* this_arg */
	, &BackButtonModes_t229_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BackButtonModes_t229)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BackButtonModes_t229)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Cardboard/Eye
#include "AssemblyU2DCSharp_Cardboard_Eye.h"
// Metadata Definition Cardboard/Eye
extern TypeInfo Eye_t230_il2cpp_TypeInfo;
// Cardboard/Eye
#include "AssemblyU2DCSharp_Cardboard_EyeMethodDeclarations.h"
static const EncodedMethodIndex Eye_t230_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Eye_t230_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Eye_t230_1_0_0;
const Il2CppTypeDefinitionMetadata Eye_t230_DefinitionMetadata = 
{
	&Cardboard_t233_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Eye_t230_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Eye_t230_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1840/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Eye_t230_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Eye"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Eye_t230_0_0_0/* byval_arg */
	, &Eye_t230_1_0_0/* this_arg */
	, &Eye_t230_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Eye_t230)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Eye_t230)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Cardboard/Distortion
#include "AssemblyU2DCSharp_Cardboard_Distortion.h"
// Metadata Definition Cardboard/Distortion
extern TypeInfo Distortion_t231_il2cpp_TypeInfo;
// Cardboard/Distortion
#include "AssemblyU2DCSharp_Cardboard_DistortionMethodDeclarations.h"
static const EncodedMethodIndex Distortion_t231_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Distortion_t231_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Distortion_t231_1_0_0;
const Il2CppTypeDefinitionMetadata Distortion_t231_DefinitionMetadata = 
{
	&Cardboard_t233_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Distortion_t231_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Distortion_t231_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1844/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Distortion_t231_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Distortion"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Distortion_t231_0_0_0/* byval_arg */
	, &Distortion_t231_1_0_0/* this_arg */
	, &Distortion_t231_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Distortion_t231)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Distortion_t231)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Cardboard/StereoScreenChangeDelegate
#include "AssemblyU2DCSharp_Cardboard_StereoScreenChangeDelegate.h"
// Metadata Definition Cardboard/StereoScreenChangeDelegate
extern TypeInfo StereoScreenChangeDelegate_t232_il2cpp_TypeInfo;
// Cardboard/StereoScreenChangeDelegate
#include "AssemblyU2DCSharp_Cardboard_StereoScreenChangeDelegateMethodDeclarations.h"
static const EncodedMethodIndex StereoScreenChangeDelegate_t232_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	644,
	645,
	646,
};
static Il2CppInterfaceOffsetPair StereoScreenChangeDelegate_t232_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType StereoScreenChangeDelegate_t232_1_0_0;
struct StereoScreenChangeDelegate_t232;
const Il2CppTypeDefinitionMetadata StereoScreenChangeDelegate_t232_DefinitionMetadata = 
{
	&Cardboard_t233_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StereoScreenChangeDelegate_t232_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, StereoScreenChangeDelegate_t232_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1504/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StereoScreenChangeDelegate_t232_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "StereoScreenChangeDelegate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &StereoScreenChangeDelegate_t232_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StereoScreenChangeDelegate_t232_0_0_0/* byval_arg */
	, &StereoScreenChangeDelegate_t232_1_0_0/* this_arg */
	, &StereoScreenChangeDelegate_t232_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_StereoScreenChangeDelegate_t232/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StereoScreenChangeDelegate_t232)/* instance_size */
	, sizeof (StereoScreenChangeDelegate_t232)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Cardboard/<EndOfFrame>c__Iterator1
#include "AssemblyU2DCSharp_Cardboard_U3CEndOfFrameU3Ec__Iterator1.h"
// Metadata Definition Cardboard/<EndOfFrame>c__Iterator1
extern TypeInfo U3CEndOfFrameU3Ec__Iterator1_t234_il2cpp_TypeInfo;
// Cardboard/<EndOfFrame>c__Iterator1
#include "AssemblyU2DCSharp_Cardboard_U3CEndOfFrameU3Ec__Iterator1MethodDeclarations.h"
static const EncodedMethodIndex U3CEndOfFrameU3Ec__Iterator1_t234_VTable[9] = 
{
	626,
	601,
	627,
	628,
	647,
	648,
	649,
	650,
	651,
};
extern const Il2CppType IEnumerator_t464_0_0_0;
extern const Il2CppType IEnumerator_1_t2280_0_0_0;
extern const Il2CppType IDisposable_t538_0_0_0;
static const Il2CppType* U3CEndOfFrameU3Ec__Iterator1_t234_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CEndOfFrameU3Ec__Iterator1_t234_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CEndOfFrameU3Ec__Iterator1_t234_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct U3CEndOfFrameU3Ec__Iterator1_t234;
const Il2CppTypeDefinitionMetadata U3CEndOfFrameU3Ec__Iterator1_t234_DefinitionMetadata = 
{
	&Cardboard_t233_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CEndOfFrameU3Ec__Iterator1_t234_InterfacesTypeInfos/* implementedInterfaces */
	, U3CEndOfFrameU3Ec__Iterator1_t234_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CEndOfFrameU3Ec__Iterator1_t234_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1847/* fieldStart */
	, 1508/* methodStart */
	, -1/* eventStart */
	, 233/* propertyStart */

};
TypeInfo U3CEndOfFrameU3Ec__Iterator1_t234_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<EndOfFrame>c__Iterator1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CEndOfFrameU3Ec__Iterator1_t234_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 705/* custom_attributes_cache */
	, &U3CEndOfFrameU3Ec__Iterator1_t234_0_0_0/* byval_arg */
	, &U3CEndOfFrameU3Ec__Iterator1_t234_1_0_0/* this_arg */
	, &U3CEndOfFrameU3Ec__Iterator1_t234_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CEndOfFrameU3Ec__Iterator1_t234)/* instance_size */
	, sizeof (U3CEndOfFrameU3Ec__Iterator1_t234)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// CardboardEye
#include "AssemblyU2DCSharp_CardboardEye.h"
// Metadata Definition CardboardEye
extern TypeInfo CardboardEye_t240_il2cpp_TypeInfo;
// CardboardEye
#include "AssemblyU2DCSharp_CardboardEyeMethodDeclarations.h"
static const EncodedMethodIndex CardboardEye_t240_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CardboardEye_t240_0_0_0;
extern const Il2CppType CardboardEye_t240_1_0_0;
struct CardboardEye_t240;
const Il2CppTypeDefinitionMetadata CardboardEye_t240_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CardboardEye_t240_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1850/* fieldStart */
	, 1514/* methodStart */
	, -1/* eventStart */
	, 235/* propertyStart */

};
TypeInfo CardboardEye_t240_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CardboardEye"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CardboardEye_t240_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 710/* custom_attributes_cache */
	, &CardboardEye_t240_0_0_0/* byval_arg */
	, &CardboardEye_t240_1_0_0/* this_arg */
	, &CardboardEye_t240_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CardboardEye_t240)/* instance_size */
	, sizeof (CardboardEye_t240)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 3/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CardboardHead
#include "AssemblyU2DCSharp_CardboardHead.h"
// Metadata Definition CardboardHead
extern TypeInfo CardboardHead_t244_il2cpp_TypeInfo;
// CardboardHead
#include "AssemblyU2DCSharp_CardboardHeadMethodDeclarations.h"
static const EncodedMethodIndex CardboardHead_t244_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CardboardHead_t244_0_0_0;
extern const Il2CppType CardboardHead_t244_1_0_0;
struct CardboardHead_t244;
const Il2CppTypeDefinitionMetadata CardboardHead_t244_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CardboardHead_t244_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1860/* fieldStart */
	, 1527/* methodStart */
	, -1/* eventStart */
	, 238/* propertyStart */

};
TypeInfo CardboardHead_t244_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CardboardHead"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CardboardHead_t244_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CardboardHead_t244_0_0_0/* byval_arg */
	, &CardboardHead_t244_1_0_0/* this_arg */
	, &CardboardHead_t244_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CardboardHead_t244)/* instance_size */
	, sizeof (CardboardHead_t244)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CardboardPostRender
#include "AssemblyU2DCSharp_CardboardPostRender.h"
// Metadata Definition CardboardPostRender
extern TypeInfo CardboardPostRender_t246_il2cpp_TypeInfo;
// CardboardPostRender
#include "AssemblyU2DCSharp_CardboardPostRenderMethodDeclarations.h"
static const EncodedMethodIndex CardboardPostRender_t246_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CardboardPostRender_t246_0_0_0;
extern const Il2CppType CardboardPostRender_t246_1_0_0;
struct CardboardPostRender_t246;
const Il2CppTypeDefinitionMetadata CardboardPostRender_t246_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CardboardPostRender_t246_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1865/* fieldStart */
	, 1532/* methodStart */
	, -1/* eventStart */
	, 239/* propertyStart */

};
TypeInfo CardboardPostRender_t246_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CardboardPostRender"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CardboardPostRender_t246_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 715/* custom_attributes_cache */
	, &CardboardPostRender_t246_0_0_0/* byval_arg */
	, &CardboardPostRender_t246_1_0_0/* this_arg */
	, &CardboardPostRender_t246_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CardboardPostRender_t246)/* instance_size */
	, sizeof (CardboardPostRender_t246)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CardboardPostRender_t246_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 1/* property_count */
	, 22/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CardboardPreRender
#include "AssemblyU2DCSharp_CardboardPreRender.h"
// Metadata Definition CardboardPreRender
extern TypeInfo CardboardPreRender_t247_il2cpp_TypeInfo;
// CardboardPreRender
#include "AssemblyU2DCSharp_CardboardPreRenderMethodDeclarations.h"
static const EncodedMethodIndex CardboardPreRender_t247_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CardboardPreRender_t247_0_0_0;
extern const Il2CppType CardboardPreRender_t247_1_0_0;
struct CardboardPreRender_t247;
const Il2CppTypeDefinitionMetadata CardboardPreRender_t247_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CardboardPreRender_t247_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1887/* fieldStart */
	, 1548/* methodStart */
	, -1/* eventStart */
	, 240/* propertyStart */

};
TypeInfo CardboardPreRender_t247_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CardboardPreRender"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CardboardPreRender_t247_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 719/* custom_attributes_cache */
	, &CardboardPreRender_t247_0_0_0/* byval_arg */
	, &CardboardPreRender_t247_1_0_0/* this_arg */
	, &CardboardPreRender_t247_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CardboardPreRender_t247)/* instance_size */
	, sizeof (CardboardPreRender_t247)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CardboardProfile
#include "AssemblyU2DCSharp_CardboardProfile.h"
// Metadata Definition CardboardProfile
extern TypeInfo CardboardProfile_t255_il2cpp_TypeInfo;
// CardboardProfile
#include "AssemblyU2DCSharp_CardboardProfileMethodDeclarations.h"
extern const Il2CppType Screen_t248_0_0_0;
extern const Il2CppType Lenses_t249_0_0_0;
extern const Il2CppType MaxFOV_t250_0_0_0;
extern const Il2CppType Distortion_t251_0_0_0;
extern const Il2CppType Device_t252_0_0_0;
extern const Il2CppType ScreenSizes_t253_0_0_0;
extern const Il2CppType DeviceTypes_t254_0_0_0;
static const Il2CppType* CardboardProfile_t255_il2cpp_TypeInfo__nestedTypes[7] =
{
	&Screen_t248_0_0_0,
	&Lenses_t249_0_0_0,
	&MaxFOV_t250_0_0_0,
	&Distortion_t251_0_0_0,
	&Device_t252_0_0_0,
	&ScreenSizes_t253_0_0_0,
	&DeviceTypes_t254_0_0_0,
};
static const EncodedMethodIndex CardboardProfile_t255_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CardboardProfile_t255_0_0_0;
extern const Il2CppType CardboardProfile_t255_1_0_0;
struct CardboardProfile_t255;
const Il2CppTypeDefinitionMetadata CardboardProfile_t255_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CardboardProfile_t255_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CardboardProfile_t255_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1888/* fieldStart */
	, 1555/* methodStart */
	, -1/* eventStart */
	, 241/* propertyStart */

};
TypeInfo CardboardProfile_t255_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CardboardProfile"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CardboardProfile_t255_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CardboardProfile_t255_0_0_0/* byval_arg */
	, &CardboardProfile_t255_1_0_0/* this_arg */
	, &CardboardProfile_t255_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CardboardProfile_t255)/* instance_size */
	, sizeof (CardboardProfile_t255)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CardboardProfile_t255_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 1/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 7/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CardboardProfile/Screen
#include "AssemblyU2DCSharp_CardboardProfile_Screen.h"
// Metadata Definition CardboardProfile/Screen
extern TypeInfo Screen_t248_il2cpp_TypeInfo;
// CardboardProfile/Screen
#include "AssemblyU2DCSharp_CardboardProfile_ScreenMethodDeclarations.h"
static const EncodedMethodIndex Screen_t248_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Screen_t248_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata Screen_t248_DefinitionMetadata = 
{
	&CardboardProfile_t255_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Screen_t248_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1903/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Screen_t248_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Screen"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Screen_t248_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Screen_t248_0_0_0/* byval_arg */
	, &Screen_t248_1_0_0/* this_arg */
	, &Screen_t248_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Screen_t248)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Screen_t248)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Screen_t248 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CardboardProfile/Lenses
#include "AssemblyU2DCSharp_CardboardProfile_Lenses.h"
// Metadata Definition CardboardProfile/Lenses
extern TypeInfo Lenses_t249_il2cpp_TypeInfo;
// CardboardProfile/Lenses
#include "AssemblyU2DCSharp_CardboardProfile_LensesMethodDeclarations.h"
static const EncodedMethodIndex Lenses_t249_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Lenses_t249_1_0_0;
const Il2CppTypeDefinitionMetadata Lenses_t249_DefinitionMetadata = 
{
	&CardboardProfile_t255_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Lenses_t249_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1906/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Lenses_t249_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Lenses"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Lenses_t249_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Lenses_t249_0_0_0/* byval_arg */
	, &Lenses_t249_1_0_0/* this_arg */
	, &Lenses_t249_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Lenses_t249)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Lenses_t249)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Lenses_t249 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CardboardProfile/MaxFOV
#include "AssemblyU2DCSharp_CardboardProfile_MaxFOV.h"
// Metadata Definition CardboardProfile/MaxFOV
extern TypeInfo MaxFOV_t250_il2cpp_TypeInfo;
// CardboardProfile/MaxFOV
#include "AssemblyU2DCSharp_CardboardProfile_MaxFOVMethodDeclarations.h"
static const EncodedMethodIndex MaxFOV_t250_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MaxFOV_t250_1_0_0;
const Il2CppTypeDefinitionMetadata MaxFOV_t250_DefinitionMetadata = 
{
	&CardboardProfile_t255_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, MaxFOV_t250_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1913/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MaxFOV_t250_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MaxFOV"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MaxFOV_t250_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MaxFOV_t250_0_0_0/* byval_arg */
	, &MaxFOV_t250_1_0_0/* this_arg */
	, &MaxFOV_t250_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MaxFOV_t250)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MaxFOV_t250)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(MaxFOV_t250 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CardboardProfile/Distortion
#include "AssemblyU2DCSharp_CardboardProfile_Distortion.h"
// Metadata Definition CardboardProfile/Distortion
extern TypeInfo Distortion_t251_il2cpp_TypeInfo;
// CardboardProfile/Distortion
#include "AssemblyU2DCSharp_CardboardProfile_DistortionMethodDeclarations.h"
static const EncodedMethodIndex Distortion_t251_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Distortion_t251_1_0_0;
const Il2CppTypeDefinitionMetadata Distortion_t251_DefinitionMetadata = 
{
	&CardboardProfile_t255_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Distortion_t251_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1917/* fieldStart */
	, 1567/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Distortion_t251_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Distortion"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Distortion_t251_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Distortion_t251_0_0_0/* byval_arg */
	, &Distortion_t251_1_0_0/* this_arg */
	, &Distortion_t251_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Distortion_t251)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Distortion_t251)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Distortion_t251 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CardboardProfile/Device
#include "AssemblyU2DCSharp_CardboardProfile_Device.h"
// Metadata Definition CardboardProfile/Device
extern TypeInfo Device_t252_il2cpp_TypeInfo;
// CardboardProfile/Device
#include "AssemblyU2DCSharp_CardboardProfile_DeviceMethodDeclarations.h"
static const EncodedMethodIndex Device_t252_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Device_t252_1_0_0;
const Il2CppTypeDefinitionMetadata Device_t252_DefinitionMetadata = 
{
	&CardboardProfile_t255_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Device_t252_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1919/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Device_t252_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Device"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Device_t252_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Device_t252_0_0_0/* byval_arg */
	, &Device_t252_1_0_0/* this_arg */
	, &Device_t252_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Device_t252)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Device_t252)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Device_t252 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CardboardProfile/ScreenSizes
#include "AssemblyU2DCSharp_CardboardProfile_ScreenSizes.h"
// Metadata Definition CardboardProfile/ScreenSizes
extern TypeInfo ScreenSizes_t253_il2cpp_TypeInfo;
// CardboardProfile/ScreenSizes
#include "AssemblyU2DCSharp_CardboardProfile_ScreenSizesMethodDeclarations.h"
static const EncodedMethodIndex ScreenSizes_t253_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ScreenSizes_t253_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ScreenSizes_t253_1_0_0;
const Il2CppTypeDefinitionMetadata ScreenSizes_t253_DefinitionMetadata = 
{
	&CardboardProfile_t255_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScreenSizes_t253_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ScreenSizes_t253_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1923/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ScreenSizes_t253_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScreenSizes"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScreenSizes_t253_0_0_0/* byval_arg */
	, &ScreenSizes_t253_1_0_0/* this_arg */
	, &ScreenSizes_t253_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScreenSizes_t253)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScreenSizes_t253)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// CardboardProfile/DeviceTypes
#include "AssemblyU2DCSharp_CardboardProfile_DeviceTypes.h"
// Metadata Definition CardboardProfile/DeviceTypes
extern TypeInfo DeviceTypes_t254_il2cpp_TypeInfo;
// CardboardProfile/DeviceTypes
#include "AssemblyU2DCSharp_CardboardProfile_DeviceTypesMethodDeclarations.h"
static const EncodedMethodIndex DeviceTypes_t254_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair DeviceTypes_t254_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DeviceTypes_t254_1_0_0;
const Il2CppTypeDefinitionMetadata DeviceTypes_t254_DefinitionMetadata = 
{
	&CardboardProfile_t255_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DeviceTypes_t254_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, DeviceTypes_t254_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1933/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DeviceTypes_t254_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DeviceTypes"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DeviceTypes_t254_0_0_0/* byval_arg */
	, &DeviceTypes_t254_1_0_0/* this_arg */
	, &DeviceTypes_t254_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DeviceTypes_t254)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DeviceTypes_t254)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// GazeInputModule
#include "AssemblyU2DCSharp_GazeInputModule.h"
// Metadata Definition GazeInputModule
extern TypeInfo GazeInputModule_t258_il2cpp_TypeInfo;
// GazeInputModule
#include "AssemblyU2DCSharp_GazeInputModuleMethodDeclarations.h"
static const EncodedMethodIndex GazeInputModule_t258_VTable[25] = 
{
	600,
	601,
	602,
	603,
	655,
	656,
	657,
	658,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	667,
	668,
	669,
	670,
	671,
	672,
	673,
	674,
	675,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType GazeInputModule_t258_0_0_0;
extern const Il2CppType GazeInputModule_t258_1_0_0;
extern const Il2CppType BaseInputModule_t259_0_0_0;
struct GazeInputModule_t258;
const Il2CppTypeDefinitionMetadata GazeInputModule_t258_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInputModule_t259_0_0_0/* parent */
	, GazeInputModule_t258_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1937/* fieldStart */
	, 1569/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GazeInputModule_t258_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "GazeInputModule"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &GazeInputModule_t258_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GazeInputModule_t258_0_0_0/* byval_arg */
	, &GazeInputModule_t258_1_0_0/* this_arg */
	, &GazeInputModule_t258_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GazeInputModule_t258)/* instance_size */
	, sizeof (GazeInputModule_t258)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GazeInputModule_t258_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 25/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Pose3D
#include "AssemblyU2DCSharp_Pose3D.h"
// Metadata Definition Pose3D
extern TypeInfo Pose3D_t260_il2cpp_TypeInfo;
// Pose3D
#include "AssemblyU2DCSharp_Pose3DMethodDeclarations.h"
static const EncodedMethodIndex Pose3D_t260_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Pose3D_t260_0_0_0;
extern const Il2CppType Pose3D_t260_1_0_0;
struct Pose3D_t260;
const Il2CppTypeDefinitionMetadata Pose3D_t260_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Pose3D_t260_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1951/* fieldStart */
	, 1582/* methodStart */
	, -1/* eventStart */
	, 242/* propertyStart */

};
TypeInfo Pose3D_t260_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Pose3D"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Pose3D_t260_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Pose3D_t260_0_0_0/* byval_arg */
	, &Pose3D_t260_1_0_0/* this_arg */
	, &Pose3D_t260_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Pose3D_t260)/* instance_size */
	, sizeof (Pose3D_t260)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Pose3D_t260_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 4/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MutablePose3D
#include "AssemblyU2DCSharp_MutablePose3D.h"
// Metadata Definition MutablePose3D
extern TypeInfo MutablePose3D_t262_il2cpp_TypeInfo;
// MutablePose3D
#include "AssemblyU2DCSharp_MutablePose3DMethodDeclarations.h"
static const EncodedMethodIndex MutablePose3D_t262_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MutablePose3D_t262_0_0_0;
extern const Il2CppType MutablePose3D_t262_1_0_0;
struct MutablePose3D_t262;
const Il2CppTypeDefinitionMetadata MutablePose3D_t262_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Pose3D_t260_0_0_0/* parent */
	, MutablePose3D_t262_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1595/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MutablePose3D_t262_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MutablePose3D"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MutablePose3D_t262_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MutablePose3D_t262_0_0_0/* byval_arg */
	, &MutablePose3D_t262_1_0_0/* this_arg */
	, &MutablePose3D_t262_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MutablePose3D_t262)/* instance_size */
	, sizeof (MutablePose3D_t262)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// RadialUndistortionEffect
#include "AssemblyU2DCSharp_RadialUndistortionEffect.h"
// Metadata Definition RadialUndistortionEffect
extern TypeInfo RadialUndistortionEffect_t263_il2cpp_TypeInfo;
// RadialUndistortionEffect
#include "AssemblyU2DCSharp_RadialUndistortionEffectMethodDeclarations.h"
static const EncodedMethodIndex RadialUndistortionEffect_t263_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType RadialUndistortionEffect_t263_0_0_0;
extern const Il2CppType RadialUndistortionEffect_t263_1_0_0;
struct RadialUndistortionEffect_t263;
const Il2CppTypeDefinitionMetadata RadialUndistortionEffect_t263_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, RadialUndistortionEffect_t263_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1955/* fieldStart */
	, 1599/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RadialUndistortionEffect_t263_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "RadialUndistortionEffect"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &RadialUndistortionEffect_t263_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 736/* custom_attributes_cache */
	, &RadialUndistortionEffect_t263_0_0_0/* byval_arg */
	, &RadialUndistortionEffect_t263_1_0_0/* this_arg */
	, &RadialUndistortionEffect_t263_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RadialUndistortionEffect_t263)/* instance_size */
	, sizeof (RadialUndistortionEffect_t263)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// StereoController
#include "AssemblyU2DCSharp_StereoController.h"
// Metadata Definition StereoController
extern TypeInfo StereoController_t235_il2cpp_TypeInfo;
// StereoController
#include "AssemblyU2DCSharp_StereoControllerMethodDeclarations.h"
extern const Il2CppType U3CEndOfFrameU3Ec__Iterator2_t264_0_0_0;
static const Il2CppType* StereoController_t235_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CEndOfFrameU3Ec__Iterator2_t264_0_0_0,
};
static const EncodedMethodIndex StereoController_t235_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType StereoController_t235_0_0_0;
extern const Il2CppType StereoController_t235_1_0_0;
struct StereoController_t235;
const Il2CppTypeDefinitionMetadata StereoController_t235_DefinitionMetadata = 
{
	NULL/* declaringType */
	, StereoController_t235_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, StereoController_t235_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1956/* fieldStart */
	, 1602/* methodStart */
	, -1/* eventStart */
	, 246/* propertyStart */

};
TypeInfo StereoController_t235_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "StereoController"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &StereoController_t235_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 737/* custom_attributes_cache */
	, &StereoController_t235_0_0_0/* byval_arg */
	, &StereoController_t235_1_0_0/* this_arg */
	, &StereoController_t235_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StereoController_t235)/* instance_size */
	, sizeof (StereoController_t235)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StereoController_t235_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 3/* property_count */
	, 17/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// StereoController/<EndOfFrame>c__Iterator2
#include "AssemblyU2DCSharp_StereoController_U3CEndOfFrameU3Ec__Iterat.h"
// Metadata Definition StereoController/<EndOfFrame>c__Iterator2
extern TypeInfo U3CEndOfFrameU3Ec__Iterator2_t264_il2cpp_TypeInfo;
// StereoController/<EndOfFrame>c__Iterator2
#include "AssemblyU2DCSharp_StereoController_U3CEndOfFrameU3Ec__IteratMethodDeclarations.h"
static const EncodedMethodIndex U3CEndOfFrameU3Ec__Iterator2_t264_VTable[9] = 
{
	626,
	601,
	627,
	628,
	676,
	677,
	678,
	679,
	680,
};
static const Il2CppType* U3CEndOfFrameU3Ec__Iterator2_t264_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CEndOfFrameU3Ec__Iterator2_t264_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CEndOfFrameU3Ec__Iterator2_t264_1_0_0;
struct U3CEndOfFrameU3Ec__Iterator2_t264;
const Il2CppTypeDefinitionMetadata U3CEndOfFrameU3Ec__Iterator2_t264_DefinitionMetadata = 
{
	&StereoController_t235_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CEndOfFrameU3Ec__Iterator2_t264_InterfacesTypeInfos/* implementedInterfaces */
	, U3CEndOfFrameU3Ec__Iterator2_t264_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CEndOfFrameU3Ec__Iterator2_t264_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1973/* fieldStart */
	, 1620/* methodStart */
	, -1/* eventStart */
	, 249/* propertyStart */

};
TypeInfo U3CEndOfFrameU3Ec__Iterator2_t264_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<EndOfFrame>c__Iterator2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CEndOfFrameU3Ec__Iterator2_t264_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 757/* custom_attributes_cache */
	, &U3CEndOfFrameU3Ec__Iterator2_t264_0_0_0/* byval_arg */
	, &U3CEndOfFrameU3Ec__Iterator2_t264_1_0_0/* this_arg */
	, &U3CEndOfFrameU3Ec__Iterator2_t264_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CEndOfFrameU3Ec__Iterator2_t264)/* instance_size */
	, sizeof (U3CEndOfFrameU3Ec__Iterator2_t264)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// StereoRenderEffect
#include "AssemblyU2DCSharp_StereoRenderEffect.h"
// Metadata Definition StereoRenderEffect
extern TypeInfo StereoRenderEffect_t239_il2cpp_TypeInfo;
// StereoRenderEffect
#include "AssemblyU2DCSharp_StereoRenderEffectMethodDeclarations.h"
static const EncodedMethodIndex StereoRenderEffect_t239_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType StereoRenderEffect_t239_0_0_0;
extern const Il2CppType StereoRenderEffect_t239_1_0_0;
struct StereoRenderEffect_t239;
const Il2CppTypeDefinitionMetadata StereoRenderEffect_t239_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, StereoRenderEffect_t239_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1976/* fieldStart */
	, 1626/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StereoRenderEffect_t239_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "StereoRenderEffect"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &StereoRenderEffect_t239_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 762/* custom_attributes_cache */
	, &StereoRenderEffect_t239_0_0_0/* byval_arg */
	, &StereoRenderEffect_t239_1_0_0/* this_arg */
	, &StereoRenderEffect_t239_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StereoRenderEffect_t239)/* instance_size */
	, sizeof (StereoRenderEffect_t239)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// BaseCardboardDevice
#include "AssemblyU2DCSharp_BaseCardboardDevice.h"
// Metadata Definition BaseCardboardDevice
extern TypeInfo BaseCardboardDevice_t270_il2cpp_TypeInfo;
// BaseCardboardDevice
#include "AssemblyU2DCSharp_BaseCardboardDeviceMethodDeclarations.h"
extern const Il2CppType VREventCallback_t267_0_0_0;
static const Il2CppType* BaseCardboardDevice_t270_il2cpp_TypeInfo__nestedTypes[1] =
{
	&VREventCallback_t267_0_0_0,
};
static const EncodedMethodIndex BaseCardboardDevice_t270_VTable[33] = 
{
	626,
	601,
	627,
	628,
	681,
	0,
	682,
	683,
	0,
	0,
	0,
	0,
	0,
	684,
	685,
	686,
	687,
	688,
	689,
	690,
	691,
	692,
	693,
	694,
	695,
	696,
	697,
	698,
	699,
	700,
	701,
	702,
	703,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType BaseCardboardDevice_t270_0_0_0;
extern const Il2CppType BaseCardboardDevice_t270_1_0_0;
extern const Il2CppType BaseVRDevice_t236_0_0_0;
struct BaseCardboardDevice_t270;
const Il2CppTypeDefinitionMetadata BaseCardboardDevice_t270_DefinitionMetadata = 
{
	NULL/* declaringType */
	, BaseCardboardDevice_t270_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseVRDevice_t236_0_0_0/* parent */
	, BaseCardboardDevice_t270_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1978/* fieldStart */
	, 1630/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BaseCardboardDevice_t270_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseCardboardDevice"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &BaseCardboardDevice_t270_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseCardboardDevice_t270_0_0_0/* byval_arg */
	, &BaseCardboardDevice_t270_1_0_0/* this_arg */
	, &BaseCardboardDevice_t270_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseCardboardDevice_t270)/* instance_size */
	, sizeof (BaseCardboardDevice_t270)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 36/* method_count */
	, 0/* property_count */
	, 17/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 33/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// BaseCardboardDevice/VREventCallback
#include "AssemblyU2DCSharp_BaseCardboardDevice_VREventCallback.h"
// Metadata Definition BaseCardboardDevice/VREventCallback
extern TypeInfo VREventCallback_t267_il2cpp_TypeInfo;
// BaseCardboardDevice/VREventCallback
#include "AssemblyU2DCSharp_BaseCardboardDevice_VREventCallbackMethodDeclarations.h"
static const EncodedMethodIndex VREventCallback_t267_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	704,
	705,
	706,
};
static Il2CppInterfaceOffsetPair VREventCallback_t267_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType VREventCallback_t267_1_0_0;
struct VREventCallback_t267;
const Il2CppTypeDefinitionMetadata VREventCallback_t267_DefinitionMetadata = 
{
	&BaseCardboardDevice_t270_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, VREventCallback_t267_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, VREventCallback_t267_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1666/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo VREventCallback_t267_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "VREventCallback"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &VREventCallback_t267_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VREventCallback_t267_0_0_0/* byval_arg */
	, &VREventCallback_t267_1_0_0/* this_arg */
	, &VREventCallback_t267_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_VREventCallback_t267/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VREventCallback_t267)/* instance_size */
	, sizeof (VREventCallback_t267)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// BaseVRDevice
#include "AssemblyU2DCSharp_BaseVRDevice.h"
// Metadata Definition BaseVRDevice
extern TypeInfo BaseVRDevice_t236_il2cpp_TypeInfo;
// BaseVRDevice
#include "AssemblyU2DCSharp_BaseVRDeviceMethodDeclarations.h"
extern const Il2CppType DisplayMetrics_t271_0_0_0;
static const Il2CppType* BaseVRDevice_t236_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DisplayMetrics_t271_0_0_0,
};
static const EncodedMethodIndex BaseVRDevice_t236_VTable[32] = 
{
	626,
	601,
	627,
	628,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	687,
	707,
	689,
	690,
	708,
	692,
	0,
	0,
	0,
	0,
	697,
	709,
	699,
	700,
	710,
	702,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType BaseVRDevice_t236_1_0_0;
struct BaseVRDevice_t236;
const Il2CppTypeDefinitionMetadata BaseVRDevice_t236_DefinitionMetadata = 
{
	NULL/* declaringType */
	, BaseVRDevice_t236_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BaseVRDevice_t236_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1995/* fieldStart */
	, 1670/* methodStart */
	, -1/* eventStart */
	, 251/* propertyStart */

};
TypeInfo BaseVRDevice_t236_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseVRDevice"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &BaseVRDevice_t236_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseVRDevice_t236_0_0_0/* byval_arg */
	, &BaseVRDevice_t236_1_0_0/* this_arg */
	, &BaseVRDevice_t236_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseVRDevice_t236)/* instance_size */
	, sizeof (BaseVRDevice_t236)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BaseVRDevice_t236_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 40/* method_count */
	, 1/* property_count */
	, 18/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 32/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// BaseVRDevice/DisplayMetrics
#include "AssemblyU2DCSharp_BaseVRDevice_DisplayMetrics.h"
// Metadata Definition BaseVRDevice/DisplayMetrics
extern TypeInfo DisplayMetrics_t271_il2cpp_TypeInfo;
// BaseVRDevice/DisplayMetrics
#include "AssemblyU2DCSharp_BaseVRDevice_DisplayMetricsMethodDeclarations.h"
static const EncodedMethodIndex DisplayMetrics_t271_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DisplayMetrics_t271_1_0_0;
const Il2CppTypeDefinitionMetadata DisplayMetrics_t271_DefinitionMetadata = 
{
	&BaseVRDevice_t236_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, DisplayMetrics_t271_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2013/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DisplayMetrics_t271_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DisplayMetrics"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DisplayMetrics_t271_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DisplayMetrics_t271_0_0_0/* byval_arg */
	, &DisplayMetrics_t271_1_0_0/* this_arg */
	, &DisplayMetrics_t271_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DisplayMetrics_t271)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DisplayMetrics_t271)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(DisplayMetrics_t271 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048842/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CardboardiOSDevice
#include "AssemblyU2DCSharp_CardboardiOSDevice.h"
// Metadata Definition CardboardiOSDevice
extern TypeInfo CardboardiOSDevice_t272_il2cpp_TypeInfo;
// CardboardiOSDevice
#include "AssemblyU2DCSharp_CardboardiOSDeviceMethodDeclarations.h"
static const EncodedMethodIndex CardboardiOSDevice_t272_VTable[33] = 
{
	626,
	601,
	627,
	628,
	711,
	712,
	682,
	683,
	713,
	714,
	715,
	716,
	717,
	684,
	685,
	686,
	718,
	719,
	689,
	690,
	720,
	721,
	693,
	694,
	695,
	696,
	697,
	698,
	722,
	700,
	701,
	702,
	703,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CardboardiOSDevice_t272_0_0_0;
extern const Il2CppType CardboardiOSDevice_t272_1_0_0;
struct CardboardiOSDevice_t272;
const Il2CppTypeDefinitionMetadata CardboardiOSDevice_t272_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseCardboardDevice_t270_0_0_0/* parent */
	, CardboardiOSDevice_t272_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2017/* fieldStart */
	, 1710/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CardboardiOSDevice_t272_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CardboardiOSDevice"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CardboardiOSDevice_t272_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CardboardiOSDevice_t272_0_0_0/* byval_arg */
	, &CardboardiOSDevice_t272_1_0_0/* this_arg */
	, &CardboardiOSDevice_t272_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CardboardiOSDevice_t272)/* instance_size */
	, sizeof (CardboardiOSDevice_t272)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 25/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 33/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MNFeaturePreview
#include "AssemblyU2DCSharp_MNFeaturePreview.h"
// Metadata Definition MNFeaturePreview
extern TypeInfo MNFeaturePreview_t274_il2cpp_TypeInfo;
// MNFeaturePreview
#include "AssemblyU2DCSharp_MNFeaturePreviewMethodDeclarations.h"
static const EncodedMethodIndex MNFeaturePreview_t274_VTable[6] = 
{
	600,
	601,
	602,
	603,
	723,
	724,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNFeaturePreview_t274_0_0_0;
extern const Il2CppType MNFeaturePreview_t274_1_0_0;
struct MNFeaturePreview_t274;
const Il2CppTypeDefinitionMetadata MNFeaturePreview_t274_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, MNFeaturePreview_t274_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2019/* fieldStart */
	, 1735/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNFeaturePreview_t274_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNFeaturePreview"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MNFeaturePreview_t274_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNFeaturePreview_t274_0_0_0/* byval_arg */
	, &MNFeaturePreview_t274_1_0_0/* this_arg */
	, &MNFeaturePreview_t274_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNFeaturePreview_t274)/* instance_size */
	, sizeof (MNFeaturePreview_t274)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MNUseExample
#include "AssemblyU2DCSharp_MNUseExample.h"
// Metadata Definition MNUseExample
extern TypeInfo MNUseExample_t275_il2cpp_TypeInfo;
// MNUseExample
#include "AssemblyU2DCSharp_MNUseExampleMethodDeclarations.h"
static const EncodedMethodIndex MNUseExample_t275_VTable[6] = 
{
	600,
	601,
	602,
	603,
	723,
	724,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNUseExample_t275_0_0_0;
extern const Il2CppType MNUseExample_t275_1_0_0;
struct MNUseExample_t275;
const Il2CppTypeDefinitionMetadata MNUseExample_t275_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MNFeaturePreview_t274_0_0_0/* parent */
	, MNUseExample_t275_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2029/* fieldStart */
	, 1739/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNUseExample_t275_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNUseExample"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MNUseExample_t275_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNUseExample_t275_0_0_0/* byval_arg */
	, &MNUseExample_t275_1_0_0/* this_arg */
	, &MNUseExample_t275_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNUseExample_t275)/* instance_size */
	, sizeof (MNUseExample_t275)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MNP
#include "AssemblyU2DCSharp_MNP.h"
// Metadata Definition MNP
extern TypeInfo MNP_t276_il2cpp_TypeInfo;
// MNP
#include "AssemblyU2DCSharp_MNPMethodDeclarations.h"
static const EncodedMethodIndex MNP_t276_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNP_t276_0_0_0;
extern const Il2CppType MNP_t276_1_0_0;
struct MNP_t276;
const Il2CppTypeDefinitionMetadata MNP_t276_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MNP_t276_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1746/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNP_t276_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNP"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MNP_t276_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNP_t276_0_0_0/* byval_arg */
	, &MNP_t276_1_0_0/* this_arg */
	, &MNP_t276_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNP_t276)/* instance_size */
	, sizeof (MNP_t276)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MNPopup
#include "AssemblyU2DCSharp_MNPopup.h"
// Metadata Definition MNPopup
extern TypeInfo MNPopup_t278_il2cpp_TypeInfo;
// MNPopup
#include "AssemblyU2DCSharp_MNPopupMethodDeclarations.h"
static const EncodedMethodIndex MNPopup_t278_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNPopup_t278_0_0_0;
extern const Il2CppType MNPopup_t278_1_0_0;
struct MNPopup_t278;
const Il2CppTypeDefinitionMetadata MNPopup_t278_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, MNPopup_t278_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2031/* fieldStart */
	, 1749/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNPopup_t278_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNPopup"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MNPopup_t278_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNPopup_t278_0_0_0/* byval_arg */
	, &MNPopup_t278_1_0_0/* this_arg */
	, &MNPopup_t278_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNPopup_t278)/* instance_size */
	, sizeof (MNPopup_t278)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MNPopup_t278_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MobileNativeDialog
#include "AssemblyU2DCSharp_MobileNativeDialog.h"
// Metadata Definition MobileNativeDialog
extern TypeInfo MobileNativeDialog_t279_il2cpp_TypeInfo;
// MobileNativeDialog
#include "AssemblyU2DCSharp_MobileNativeDialogMethodDeclarations.h"
static const EncodedMethodIndex MobileNativeDialog_t279_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MobileNativeDialog_t279_0_0_0;
extern const Il2CppType MobileNativeDialog_t279_1_0_0;
struct MobileNativeDialog_t279;
const Il2CppTypeDefinitionMetadata MobileNativeDialog_t279_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MobileNativeDialog_t279_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2035/* fieldStart */
	, 1751/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MobileNativeDialog_t279_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MobileNativeDialog"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MobileNativeDialog_t279_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MobileNativeDialog_t279_0_0_0/* byval_arg */
	, &MobileNativeDialog_t279_1_0_0/* this_arg */
	, &MobileNativeDialog_t279_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MobileNativeDialog_t279)/* instance_size */
	, sizeof (MobileNativeDialog_t279)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MobileNativeDialog_t279_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MobileNativeMessage
#include "AssemblyU2DCSharp_MobileNativeMessage.h"
// Metadata Definition MobileNativeMessage
extern TypeInfo MobileNativeMessage_t280_il2cpp_TypeInfo;
// MobileNativeMessage
#include "AssemblyU2DCSharp_MobileNativeMessageMethodDeclarations.h"
static const EncodedMethodIndex MobileNativeMessage_t280_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MobileNativeMessage_t280_0_0_0;
extern const Il2CppType MobileNativeMessage_t280_1_0_0;
struct MobileNativeMessage_t280;
const Il2CppTypeDefinitionMetadata MobileNativeMessage_t280_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MobileNativeMessage_t280_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2037/* fieldStart */
	, 1756/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MobileNativeMessage_t280_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MobileNativeMessage"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MobileNativeMessage_t280_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MobileNativeMessage_t280_0_0_0/* byval_arg */
	, &MobileNativeMessage_t280_1_0_0/* this_arg */
	, &MobileNativeMessage_t280_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MobileNativeMessage_t280)/* instance_size */
	, sizeof (MobileNativeMessage_t280)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MobileNativeMessage_t280_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MobileNativeRateUs
#include "AssemblyU2DCSharp_MobileNativeRateUs.h"
// Metadata Definition MobileNativeRateUs
extern TypeInfo MobileNativeRateUs_t281_il2cpp_TypeInfo;
// MobileNativeRateUs
#include "AssemblyU2DCSharp_MobileNativeRateUsMethodDeclarations.h"
static const EncodedMethodIndex MobileNativeRateUs_t281_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MobileNativeRateUs_t281_0_0_0;
extern const Il2CppType MobileNativeRateUs_t281_1_0_0;
struct MobileNativeRateUs_t281;
const Il2CppTypeDefinitionMetadata MobileNativeRateUs_t281_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MobileNativeRateUs_t281_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2039/* fieldStart */
	, 1761/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MobileNativeRateUs_t281_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MobileNativeRateUs"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MobileNativeRateUs_t281_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MobileNativeRateUs_t281_0_0_0/* byval_arg */
	, &MobileNativeRateUs_t281_1_0_0/* this_arg */
	, &MobileNativeRateUs_t281_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MobileNativeRateUs_t281)/* instance_size */
	, sizeof (MobileNativeRateUs_t281)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MobileNativeRateUs_t281_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MNDialogResult
#include "AssemblyU2DCSharp_MNDialogResult.h"
// Metadata Definition MNDialogResult
extern TypeInfo MNDialogResult_t282_il2cpp_TypeInfo;
// MNDialogResult
#include "AssemblyU2DCSharp_MNDialogResultMethodDeclarations.h"
static const EncodedMethodIndex MNDialogResult_t282_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair MNDialogResult_t282_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNDialogResult_t282_0_0_0;
extern const Il2CppType MNDialogResult_t282_1_0_0;
const Il2CppTypeDefinitionMetadata MNDialogResult_t282_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MNDialogResult_t282_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, MNDialogResult_t282_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2048/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNDialogResult_t282_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNDialogResult"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNDialogResult_t282_0_0_0/* byval_arg */
	, &MNDialogResult_t282_1_0_0/* this_arg */
	, &MNDialogResult_t282_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNDialogResult_t282)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MNDialogResult_t282)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// MNAndroidNative
#include "AssemblyU2DCSharp_MNAndroidNative.h"
// Metadata Definition MNAndroidNative
extern TypeInfo MNAndroidNative_t283_il2cpp_TypeInfo;
// MNAndroidNative
#include "AssemblyU2DCSharp_MNAndroidNativeMethodDeclarations.h"
static const EncodedMethodIndex MNAndroidNative_t283_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNAndroidNative_t283_0_0_0;
extern const Il2CppType MNAndroidNative_t283_1_0_0;
struct MNAndroidNative_t283;
const Il2CppTypeDefinitionMetadata MNAndroidNative_t283_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MNAndroidNative_t283_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2055/* fieldStart */
	, 1768/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNAndroidNative_t283_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNAndroidNative"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MNAndroidNative_t283_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNAndroidNative_t283_0_0_0/* byval_arg */
	, &MNAndroidNative_t283_1_0_0/* this_arg */
	, &MNAndroidNative_t283_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNAndroidNative_t283)/* instance_size */
	, sizeof (MNAndroidNative_t283)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MNProxyPool
#include "AssemblyU2DCSharp_MNProxyPool.h"
// Metadata Definition MNProxyPool
extern TypeInfo MNProxyPool_t284_il2cpp_TypeInfo;
// MNProxyPool
#include "AssemblyU2DCSharp_MNProxyPoolMethodDeclarations.h"
static const EncodedMethodIndex MNProxyPool_t284_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNProxyPool_t284_0_0_0;
extern const Il2CppType MNProxyPool_t284_1_0_0;
struct MNProxyPool_t284;
const Il2CppTypeDefinitionMetadata MNProxyPool_t284_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MNProxyPool_t284_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1778/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNProxyPool_t284_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNProxyPool"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MNProxyPool_t284_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNProxyPool_t284_0_0_0/* byval_arg */
	, &MNProxyPool_t284_1_0_0/* this_arg */
	, &MNProxyPool_t284_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNProxyPool_t284)/* instance_size */
	, sizeof (MNProxyPool_t284)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MNAndroidDialog
#include "AssemblyU2DCSharp_MNAndroidDialog.h"
// Metadata Definition MNAndroidDialog
extern TypeInfo MNAndroidDialog_t285_il2cpp_TypeInfo;
// MNAndroidDialog
#include "AssemblyU2DCSharp_MNAndroidDialogMethodDeclarations.h"
static const EncodedMethodIndex MNAndroidDialog_t285_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNAndroidDialog_t285_0_0_0;
extern const Il2CppType MNAndroidDialog_t285_1_0_0;
struct MNAndroidDialog_t285;
const Il2CppTypeDefinitionMetadata MNAndroidDialog_t285_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MNPopup_t278_0_0_0/* parent */
	, MNAndroidDialog_t285_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2056/* fieldStart */
	, 1780/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNAndroidDialog_t285_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNAndroidDialog"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MNAndroidDialog_t285_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNAndroidDialog_t285_0_0_0/* byval_arg */
	, &MNAndroidDialog_t285_1_0_0/* this_arg */
	, &MNAndroidDialog_t285_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNAndroidDialog_t285)/* instance_size */
	, sizeof (MNAndroidDialog_t285)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MNAndroidMessage
#include "AssemblyU2DCSharp_MNAndroidMessage.h"
// Metadata Definition MNAndroidMessage
extern TypeInfo MNAndroidMessage_t286_il2cpp_TypeInfo;
// MNAndroidMessage
#include "AssemblyU2DCSharp_MNAndroidMessageMethodDeclarations.h"
static const EncodedMethodIndex MNAndroidMessage_t286_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNAndroidMessage_t286_0_0_0;
extern const Il2CppType MNAndroidMessage_t286_1_0_0;
struct MNAndroidMessage_t286;
const Il2CppTypeDefinitionMetadata MNAndroidMessage_t286_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MNPopup_t278_0_0_0/* parent */
	, MNAndroidMessage_t286_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2058/* fieldStart */
	, 1785/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNAndroidMessage_t286_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNAndroidMessage"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MNAndroidMessage_t286_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNAndroidMessage_t286_0_0_0/* byval_arg */
	, &MNAndroidMessage_t286_1_0_0/* this_arg */
	, &MNAndroidMessage_t286_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNAndroidMessage_t286)/* instance_size */
	, sizeof (MNAndroidMessage_t286)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MNAndroidRateUsPopUp
#include "AssemblyU2DCSharp_MNAndroidRateUsPopUp.h"
// Metadata Definition MNAndroidRateUsPopUp
extern TypeInfo MNAndroidRateUsPopUp_t287_il2cpp_TypeInfo;
// MNAndroidRateUsPopUp
#include "AssemblyU2DCSharp_MNAndroidRateUsPopUpMethodDeclarations.h"
static const EncodedMethodIndex MNAndroidRateUsPopUp_t287_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNAndroidRateUsPopUp_t287_0_0_0;
extern const Il2CppType MNAndroidRateUsPopUp_t287_1_0_0;
struct MNAndroidRateUsPopUp_t287;
const Il2CppTypeDefinitionMetadata MNAndroidRateUsPopUp_t287_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MNPopup_t278_0_0_0/* parent */
	, MNAndroidRateUsPopUp_t287_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2059/* fieldStart */
	, 1790/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNAndroidRateUsPopUp_t287_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNAndroidRateUsPopUp"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MNAndroidRateUsPopUp_t287_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNAndroidRateUsPopUp_t287_0_0_0/* byval_arg */
	, &MNAndroidRateUsPopUp_t287_1_0_0/* this_arg */
	, &MNAndroidRateUsPopUp_t287_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNAndroidRateUsPopUp_t287)/* instance_size */
	, sizeof (MNAndroidRateUsPopUp_t287)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MNIOSDialog
#include "AssemblyU2DCSharp_MNIOSDialog.h"
// Metadata Definition MNIOSDialog
extern TypeInfo MNIOSDialog_t288_il2cpp_TypeInfo;
// MNIOSDialog
#include "AssemblyU2DCSharp_MNIOSDialogMethodDeclarations.h"
static const EncodedMethodIndex MNIOSDialog_t288_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNIOSDialog_t288_0_0_0;
extern const Il2CppType MNIOSDialog_t288_1_0_0;
struct MNIOSDialog_t288;
const Il2CppTypeDefinitionMetadata MNIOSDialog_t288_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MNPopup_t278_0_0_0/* parent */
	, MNIOSDialog_t288_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2063/* fieldStart */
	, 1795/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNIOSDialog_t288_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNIOSDialog"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MNIOSDialog_t288_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNIOSDialog_t288_0_0_0/* byval_arg */
	, &MNIOSDialog_t288_1_0_0/* this_arg */
	, &MNIOSDialog_t288_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNIOSDialog_t288)/* instance_size */
	, sizeof (MNIOSDialog_t288)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MNIOSMessage
#include "AssemblyU2DCSharp_MNIOSMessage.h"
// Metadata Definition MNIOSMessage
extern TypeInfo MNIOSMessage_t289_il2cpp_TypeInfo;
// MNIOSMessage
#include "AssemblyU2DCSharp_MNIOSMessageMethodDeclarations.h"
static const EncodedMethodIndex MNIOSMessage_t289_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNIOSMessage_t289_0_0_0;
extern const Il2CppType MNIOSMessage_t289_1_0_0;
struct MNIOSMessage_t289;
const Il2CppTypeDefinitionMetadata MNIOSMessage_t289_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MNPopup_t278_0_0_0/* parent */
	, MNIOSMessage_t289_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2065/* fieldStart */
	, 1800/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNIOSMessage_t289_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNIOSMessage"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MNIOSMessage_t289_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNIOSMessage_t289_0_0_0/* byval_arg */
	, &MNIOSMessage_t289_1_0_0/* this_arg */
	, &MNIOSMessage_t289_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNIOSMessage_t289)/* instance_size */
	, sizeof (MNIOSMessage_t289)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MNIOSRateUsPopUp
#include "AssemblyU2DCSharp_MNIOSRateUsPopUp.h"
// Metadata Definition MNIOSRateUsPopUp
extern TypeInfo MNIOSRateUsPopUp_t290_il2cpp_TypeInfo;
// MNIOSRateUsPopUp
#include "AssemblyU2DCSharp_MNIOSRateUsPopUpMethodDeclarations.h"
static const EncodedMethodIndex MNIOSRateUsPopUp_t290_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNIOSRateUsPopUp_t290_0_0_0;
extern const Il2CppType MNIOSRateUsPopUp_t290_1_0_0;
struct MNIOSRateUsPopUp_t290;
const Il2CppTypeDefinitionMetadata MNIOSRateUsPopUp_t290_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MNPopup_t278_0_0_0/* parent */
	, MNIOSRateUsPopUp_t290_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2066/* fieldStart */
	, 1805/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNIOSRateUsPopUp_t290_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNIOSRateUsPopUp"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MNIOSRateUsPopUp_t290_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNIOSRateUsPopUp_t290_0_0_0/* byval_arg */
	, &MNIOSRateUsPopUp_t290_1_0_0/* this_arg */
	, &MNIOSRateUsPopUp_t290_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNIOSRateUsPopUp_t290)/* instance_size */
	, sizeof (MNIOSRateUsPopUp_t290)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MNIOSNative
#include "AssemblyU2DCSharp_MNIOSNative.h"
// Metadata Definition MNIOSNative
extern TypeInfo MNIOSNative_t291_il2cpp_TypeInfo;
// MNIOSNative
#include "AssemblyU2DCSharp_MNIOSNativeMethodDeclarations.h"
static const EncodedMethodIndex MNIOSNative_t291_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNIOSNative_t291_0_0_0;
extern const Il2CppType MNIOSNative_t291_1_0_0;
struct MNIOSNative_t291;
const Il2CppTypeDefinitionMetadata MNIOSNative_t291_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MNIOSNative_t291_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1811/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNIOSNative_t291_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNIOSNative"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MNIOSNative_t291_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNIOSNative_t291_0_0_0/* byval_arg */
	, &MNIOSNative_t291_1_0_0/* this_arg */
	, &MNIOSNative_t291_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNIOSNative_t291)/* instance_size */
	, sizeof (MNIOSNative_t291)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MNWP8Dialog
#include "AssemblyU2DCSharp_MNWP8Dialog.h"
// Metadata Definition MNWP8Dialog
extern TypeInfo MNWP8Dialog_t292_il2cpp_TypeInfo;
// MNWP8Dialog
#include "AssemblyU2DCSharp_MNWP8DialogMethodDeclarations.h"
static const EncodedMethodIndex MNWP8Dialog_t292_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNWP8Dialog_t292_0_0_0;
extern const Il2CppType MNWP8Dialog_t292_1_0_0;
struct MNWP8Dialog_t292;
const Il2CppTypeDefinitionMetadata MNWP8Dialog_t292_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MNPopup_t278_0_0_0/* parent */
	, MNWP8Dialog_t292_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1828/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNWP8Dialog_t292_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNWP8Dialog"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MNWP8Dialog_t292_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNWP8Dialog_t292_0_0_0/* byval_arg */
	, &MNWP8Dialog_t292_1_0_0/* this_arg */
	, &MNWP8Dialog_t292_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNWP8Dialog_t292)/* instance_size */
	, sizeof (MNWP8Dialog_t292)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MNWP8Message
#include "AssemblyU2DCSharp_MNWP8Message.h"
// Metadata Definition MNWP8Message
extern TypeInfo MNWP8Message_t293_il2cpp_TypeInfo;
// MNWP8Message
#include "AssemblyU2DCSharp_MNWP8MessageMethodDeclarations.h"
static const EncodedMethodIndex MNWP8Message_t293_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNWP8Message_t293_0_0_0;
extern const Il2CppType MNWP8Message_t293_1_0_0;
struct MNWP8Message_t293;
const Il2CppTypeDefinitionMetadata MNWP8Message_t293_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MNPopup_t278_0_0_0/* parent */
	, MNWP8Message_t293_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1833/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNWP8Message_t293_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNWP8Message"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MNWP8Message_t293_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNWP8Message_t293_0_0_0/* byval_arg */
	, &MNWP8Message_t293_1_0_0/* this_arg */
	, &MNWP8Message_t293_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNWP8Message_t293)/* instance_size */
	, sizeof (MNWP8Message_t293)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MNWP8RateUsPopUp
#include "AssemblyU2DCSharp_MNWP8RateUsPopUp.h"
// Metadata Definition MNWP8RateUsPopUp
extern TypeInfo MNWP8RateUsPopUp_t294_il2cpp_TypeInfo;
// MNWP8RateUsPopUp
#include "AssemblyU2DCSharp_MNWP8RateUsPopUpMethodDeclarations.h"
static const EncodedMethodIndex MNWP8RateUsPopUp_t294_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MNWP8RateUsPopUp_t294_0_0_0;
extern const Il2CppType MNWP8RateUsPopUp_t294_1_0_0;
struct MNWP8RateUsPopUp_t294;
const Il2CppTypeDefinitionMetadata MNWP8RateUsPopUp_t294_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MNPopup_t278_0_0_0/* parent */
	, MNWP8RateUsPopUp_t294_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1837/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MNWP8RateUsPopUp_t294_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MNWP8RateUsPopUp"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MNWP8RateUsPopUp_t294_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MNWP8RateUsPopUp_t294_0_0_0/* byval_arg */
	, &MNWP8RateUsPopUp_t294_1_0_0/* this_arg */
	, &MNWP8RateUsPopUp_t294_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MNWP8RateUsPopUp_t294)/* instance_size */
	, sizeof (MNWP8RateUsPopUp_t294)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// BaseWP8Popup
#include "AssemblyU2DCSharp_BaseWP8Popup.h"
// Metadata Definition BaseWP8Popup
extern TypeInfo BaseWP8Popup_t295_il2cpp_TypeInfo;
// BaseWP8Popup
#include "AssemblyU2DCSharp_BaseWP8PopupMethodDeclarations.h"
static const EncodedMethodIndex BaseWP8Popup_t295_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType BaseWP8Popup_t295_0_0_0;
extern const Il2CppType BaseWP8Popup_t295_1_0_0;
struct BaseWP8Popup_t295;
const Il2CppTypeDefinitionMetadata BaseWP8Popup_t295_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, BaseWP8Popup_t295_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2070/* fieldStart */
	, 1842/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BaseWP8Popup_t295_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseWP8Popup"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &BaseWP8Popup_t295_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseWP8Popup_t295_0_0_0/* byval_arg */
	, &BaseWP8Popup_t295_1_0_0/* this_arg */
	, &BaseWP8Popup_t295_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseWP8Popup_t295)/* instance_size */
	, sizeof (BaseWP8Popup_t295)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SA_DataConverter
#include "AssemblyU2DCSharp_SA_DataConverter.h"
// Metadata Definition SA_DataConverter
extern TypeInfo SA_DataConverter_t296_il2cpp_TypeInfo;
// SA_DataConverter
#include "AssemblyU2DCSharp_SA_DataConverterMethodDeclarations.h"
static const EncodedMethodIndex SA_DataConverter_t296_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SA_DataConverter_t296_0_0_0;
extern const Il2CppType SA_DataConverter_t296_1_0_0;
struct SA_DataConverter_t296;
const Il2CppTypeDefinitionMetadata SA_DataConverter_t296_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SA_DataConverter_t296_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2072/* fieldStart */
	, 1843/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SA_DataConverter_t296_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SA_DataConverter"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SA_DataConverter_t296_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SA_DataConverter_t296_0_0_0/* byval_arg */
	, &SA_DataConverter_t296_1_0_0/* this_arg */
	, &SA_DataConverter_t296_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SA_DataConverter_t296)/* instance_size */
	, sizeof (SA_DataConverter_t296)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SA_IdFactory
#include "AssemblyU2DCSharp_SA_IdFactory.h"
// Metadata Definition SA_IdFactory
extern TypeInfo SA_IdFactory_t299_il2cpp_TypeInfo;
// SA_IdFactory
#include "AssemblyU2DCSharp_SA_IdFactoryMethodDeclarations.h"
extern const Il2CppType U3CU3Ec__AnonStorey1E_t298_0_0_0;
static const Il2CppType* SA_IdFactory_t299_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CU3Ec__AnonStorey1E_t298_0_0_0,
};
static const EncodedMethodIndex SA_IdFactory_t299_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SA_IdFactory_t299_0_0_0;
extern const Il2CppType SA_IdFactory_t299_1_0_0;
struct SA_IdFactory_t299;
const Il2CppTypeDefinitionMetadata SA_IdFactory_t299_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SA_IdFactory_t299_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SA_IdFactory_t299_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2076/* fieldStart */
	, 1846/* methodStart */
	, -1/* eventStart */
	, 252/* propertyStart */

};
TypeInfo SA_IdFactory_t299_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SA_IdFactory"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SA_IdFactory_t299_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SA_IdFactory_t299_0_0_0/* byval_arg */
	, &SA_IdFactory_t299_1_0_0/* this_arg */
	, &SA_IdFactory_t299_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SA_IdFactory_t299)/* instance_size */
	, sizeof (SA_IdFactory_t299)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SA_IdFactory/<>c__AnonStorey1E
#include "AssemblyU2DCSharp_SA_IdFactory_U3CU3Ec__AnonStorey1E.h"
// Metadata Definition SA_IdFactory/<>c__AnonStorey1E
extern TypeInfo U3CU3Ec__AnonStorey1E_t298_il2cpp_TypeInfo;
// SA_IdFactory/<>c__AnonStorey1E
#include "AssemblyU2DCSharp_SA_IdFactory_U3CU3Ec__AnonStorey1EMethodDeclarations.h"
static const EncodedMethodIndex U3CU3Ec__AnonStorey1E_t298_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CU3Ec__AnonStorey1E_t298_1_0_0;
struct U3CU3Ec__AnonStorey1E_t298;
const Il2CppTypeDefinitionMetadata U3CU3Ec__AnonStorey1E_t298_DefinitionMetadata = 
{
	&SA_IdFactory_t299_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CU3Ec__AnonStorey1E_t298_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2077/* fieldStart */
	, 1849/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CU3Ec__AnonStorey1E_t298_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<>c__AnonStorey1E"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CU3Ec__AnonStorey1E_t298_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 777/* custom_attributes_cache */
	, &U3CU3Ec__AnonStorey1E_t298_0_0_0/* byval_arg */
	, &U3CU3Ec__AnonStorey1E_t298_1_0_0/* this_arg */
	, &U3CU3Ec__AnonStorey1E_t298_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CU3Ec__AnonStorey1E_t298)/* instance_size */
	, sizeof (U3CU3Ec__AnonStorey1E_t298)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SA_ScreenShotMaker
#include "AssemblyU2DCSharp_SA_ScreenShotMaker.h"
// Metadata Definition SA_ScreenShotMaker
extern TypeInfo SA_ScreenShotMaker_t300_il2cpp_TypeInfo;
// SA_ScreenShotMaker
#include "AssemblyU2DCSharp_SA_ScreenShotMakerMethodDeclarations.h"
extern const Il2CppType U3CSaveScreenshotU3Ec__Iterator3_t301_0_0_0;
static const Il2CppType* SA_ScreenShotMaker_t300_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CSaveScreenshotU3Ec__Iterator3_t301_0_0_0,
};
static const EncodedMethodIndex SA_ScreenShotMaker_t300_VTable[6] = 
{
	600,
	601,
	602,
	603,
	2147484373,
	2147484374,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SA_ScreenShotMaker_t300_0_0_0;
extern const Il2CppType SA_ScreenShotMaker_t300_1_0_0;
extern const Il2CppType SA_Singleton_1_t303_0_0_0;
struct SA_ScreenShotMaker_t300;
const Il2CppTypeDefinitionMetadata SA_ScreenShotMaker_t300_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SA_ScreenShotMaker_t300_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &SA_Singleton_1_t303_0_0_0/* parent */
	, SA_ScreenShotMaker_t300_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2078/* fieldStart */
	, 1851/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SA_ScreenShotMaker_t300_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SA_ScreenShotMaker"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SA_ScreenShotMaker_t300_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SA_ScreenShotMaker_t300_0_0_0/* byval_arg */
	, &SA_ScreenShotMaker_t300_1_0_0/* this_arg */
	, &SA_ScreenShotMaker_t300_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SA_ScreenShotMaker_t300)/* instance_size */
	, sizeof (SA_ScreenShotMaker_t300)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3
#include "AssemblyU2DCSharp_SA_ScreenShotMaker_U3CSaveScreenshotU3Ec__.h"
// Metadata Definition SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3
extern TypeInfo U3CSaveScreenshotU3Ec__Iterator3_t301_il2cpp_TypeInfo;
// SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3
#include "AssemblyU2DCSharp_SA_ScreenShotMaker_U3CSaveScreenshotU3Ec__MethodDeclarations.h"
static const EncodedMethodIndex U3CSaveScreenshotU3Ec__Iterator3_t301_VTable[9] = 
{
	626,
	601,
	627,
	628,
	727,
	728,
	729,
	730,
	731,
};
static const Il2CppType* U3CSaveScreenshotU3Ec__Iterator3_t301_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CSaveScreenshotU3Ec__Iterator3_t301_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CSaveScreenshotU3Ec__Iterator3_t301_1_0_0;
struct U3CSaveScreenshotU3Ec__Iterator3_t301;
const Il2CppTypeDefinitionMetadata U3CSaveScreenshotU3Ec__Iterator3_t301_DefinitionMetadata = 
{
	&SA_ScreenShotMaker_t300_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CSaveScreenshotU3Ec__Iterator3_t301_InterfacesTypeInfos/* implementedInterfaces */
	, U3CSaveScreenshotU3Ec__Iterator3_t301_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CSaveScreenshotU3Ec__Iterator3_t301_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2079/* fieldStart */
	, 1854/* methodStart */
	, -1/* eventStart */
	, 254/* propertyStart */

};
TypeInfo U3CSaveScreenshotU3Ec__Iterator3_t301_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<SaveScreenshot>c__Iterator3"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CSaveScreenshotU3Ec__Iterator3_t301_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 779/* custom_attributes_cache */
	, &U3CSaveScreenshotU3Ec__Iterator3_t301_0_0_0/* byval_arg */
	, &U3CSaveScreenshotU3Ec__Iterator3_t301_1_0_0/* this_arg */
	, &U3CSaveScreenshotU3Ec__Iterator3_t301_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CSaveScreenshotU3Ec__Iterator3_t301)/* instance_size */
	, sizeof (U3CSaveScreenshotU3Ec__Iterator3_t301)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition SA_Singleton`1
extern TypeInfo SA_Singleton_1_t3392_il2cpp_TypeInfo;
static const EncodedMethodIndex SA_Singleton_1_t3392_VTable[6] = 
{
	600,
	601,
	602,
	603,
	732,
	733,
};
extern const Il2CppType SA_Singleton_1_t3612_0_0_0;
extern const Il2CppType SA_Singleton_1_t3392_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition SA_Singleton_1_t3392_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5399 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5936 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3487 }/* Class */,
	{ IL2CPP_RGCTX_DATA_TYPE, 3487 }/* Type */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5400 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5401 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SA_Singleton_1_t3392_0_0_0;
extern const Il2CppType SA_Singleton_1_t3392_1_0_0;
struct SA_Singleton_1_t3392;
const Il2CppTypeDefinitionMetadata SA_Singleton_1_t3392_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, SA_Singleton_1_t3392_VTable/* vtableMethods */
	, SA_Singleton_1_t3392_RGCTXData/* rgctxDefinition */
	, 2085/* fieldStart */
	, 1860/* methodStart */
	, -1/* eventStart */
	, 256/* propertyStart */

};
TypeInfo SA_Singleton_1_t3392_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SA_Singleton`1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SA_Singleton_1_t3392_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SA_Singleton_1_t3392_0_0_0/* byval_arg */
	, &SA_Singleton_1_t3392_1_0_0/* this_arg */
	, &SA_Singleton_1_t3392_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 0/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// WWWTextureLoader
#include "AssemblyU2DCSharp_WWWTextureLoader.h"
// Metadata Definition WWWTextureLoader
extern TypeInfo WWWTextureLoader_t305_il2cpp_TypeInfo;
// WWWTextureLoader
#include "AssemblyU2DCSharp_WWWTextureLoaderMethodDeclarations.h"
extern const Il2CppType U3CLoadCoroutinU3Ec__Iterator4_t306_0_0_0;
static const Il2CppType* WWWTextureLoader_t305_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CLoadCoroutinU3Ec__Iterator4_t306_0_0_0,
};
static const EncodedMethodIndex WWWTextureLoader_t305_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType WWWTextureLoader_t305_0_0_0;
extern const Il2CppType WWWTextureLoader_t305_1_0_0;
struct WWWTextureLoader_t305;
const Il2CppTypeDefinitionMetadata WWWTextureLoader_t305_DefinitionMetadata = 
{
	NULL/* declaringType */
	, WWWTextureLoader_t305_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, WWWTextureLoader_t305_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2087/* fieldStart */
	, 1868/* methodStart */
	, 6/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WWWTextureLoader_t305_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WWWTextureLoader"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &WWWTextureLoader_t305_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WWWTextureLoader_t305_0_0_0/* byval_arg */
	, &WWWTextureLoader_t305_1_0_0/* this_arg */
	, &WWWTextureLoader_t305_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WWWTextureLoader_t305)/* instance_size */
	, sizeof (WWWTextureLoader_t305)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WWWTextureLoader_t305_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// WWWTextureLoader/<LoadCoroutin>c__Iterator4
#include "AssemblyU2DCSharp_WWWTextureLoader_U3CLoadCoroutinU3Ec__Iter.h"
// Metadata Definition WWWTextureLoader/<LoadCoroutin>c__Iterator4
extern TypeInfo U3CLoadCoroutinU3Ec__Iterator4_t306_il2cpp_TypeInfo;
// WWWTextureLoader/<LoadCoroutin>c__Iterator4
#include "AssemblyU2DCSharp_WWWTextureLoader_U3CLoadCoroutinU3Ec__IterMethodDeclarations.h"
static const EncodedMethodIndex U3CLoadCoroutinU3Ec__Iterator4_t306_VTable[9] = 
{
	626,
	601,
	627,
	628,
	734,
	735,
	736,
	737,
	738,
};
static const Il2CppType* U3CLoadCoroutinU3Ec__Iterator4_t306_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CLoadCoroutinU3Ec__Iterator4_t306_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CLoadCoroutinU3Ec__Iterator4_t306_1_0_0;
struct U3CLoadCoroutinU3Ec__Iterator4_t306;
const Il2CppTypeDefinitionMetadata U3CLoadCoroutinU3Ec__Iterator4_t306_DefinitionMetadata = 
{
	&WWWTextureLoader_t305_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CLoadCoroutinU3Ec__Iterator4_t306_InterfacesTypeInfos/* implementedInterfaces */
	, U3CLoadCoroutinU3Ec__Iterator4_t306_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CLoadCoroutinU3Ec__Iterator4_t306_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2090/* fieldStart */
	, 1875/* methodStart */
	, -1/* eventStart */
	, 260/* propertyStart */

};
TypeInfo U3CLoadCoroutinU3Ec__Iterator4_t306_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<LoadCoroutin>c__Iterator4"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CLoadCoroutinU3Ec__Iterator4_t306_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 788/* custom_attributes_cache */
	, &U3CLoadCoroutinU3Ec__Iterator4_t306_0_0_0/* byval_arg */
	, &U3CLoadCoroutinU3Ec__Iterator4_t306_1_0_0/* this_arg */
	, &U3CLoadCoroutinU3Ec__Iterator4_t306_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CLoadCoroutinU3Ec__Iterator4_t306)/* instance_size */
	, sizeof (U3CLoadCoroutinU3Ec__Iterator4_t306)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// AICharacterControl
#include "AssemblyU2DCSharp_AICharacterControl.h"
// Metadata Definition AICharacterControl
extern TypeInfo AICharacterControl_t308_il2cpp_TypeInfo;
// AICharacterControl
#include "AssemblyU2DCSharp_AICharacterControlMethodDeclarations.h"
static const EncodedMethodIndex AICharacterControl_t308_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType AICharacterControl_t308_0_0_0;
extern const Il2CppType AICharacterControl_t308_1_0_0;
struct AICharacterControl_t308;
const Il2CppTypeDefinitionMetadata AICharacterControl_t308_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, AICharacterControl_t308_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2094/* fieldStart */
	, 1881/* methodStart */
	, -1/* eventStart */
	, 262/* propertyStart */

};
TypeInfo AICharacterControl_t308_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "AICharacterControl"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &AICharacterControl_t308_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 793/* custom_attributes_cache */
	, &AICharacterControl_t308_0_0_0/* byval_arg */
	, &AICharacterControl_t308_1_0_0/* this_arg */
	, &AICharacterControl_t308_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AICharacterControl_t308)/* instance_size */
	, sizeof (AICharacterControl_t308)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CoroutineScript
#include "AssemblyU2DCSharp_CoroutineScript.h"
// Metadata Definition CoroutineScript
extern TypeInfo CoroutineScript_t310_il2cpp_TypeInfo;
// CoroutineScript
#include "AssemblyU2DCSharp_CoroutineScriptMethodDeclarations.h"
extern const Il2CppType U3CtestCorU3Ec__Iterator5_t309_0_0_0;
static const Il2CppType* CoroutineScript_t310_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CtestCorU3Ec__Iterator5_t309_0_0_0,
};
static const EncodedMethodIndex CoroutineScript_t310_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CoroutineScript_t310_0_0_0;
extern const Il2CppType CoroutineScript_t310_1_0_0;
struct CoroutineScript_t310;
const Il2CppTypeDefinitionMetadata CoroutineScript_t310_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CoroutineScript_t310_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CoroutineScript_t310_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2098/* fieldStart */
	, 1887/* methodStart */
	, -1/* eventStart */
	, 263/* propertyStart */

};
TypeInfo CoroutineScript_t310_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CoroutineScript"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CoroutineScript_t310_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CoroutineScript_t310_0_0_0/* byval_arg */
	, &CoroutineScript_t310_1_0_0/* this_arg */
	, &CoroutineScript_t310_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CoroutineScript_t310)/* instance_size */
	, sizeof (CoroutineScript_t310)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CoroutineScript/<testCor>c__Iterator5
#include "AssemblyU2DCSharp_CoroutineScript_U3CtestCorU3Ec__Iterator5.h"
// Metadata Definition CoroutineScript/<testCor>c__Iterator5
extern TypeInfo U3CtestCorU3Ec__Iterator5_t309_il2cpp_TypeInfo;
// CoroutineScript/<testCor>c__Iterator5
#include "AssemblyU2DCSharp_CoroutineScript_U3CtestCorU3Ec__Iterator5MethodDeclarations.h"
static const EncodedMethodIndex U3CtestCorU3Ec__Iterator5_t309_VTable[9] = 
{
	626,
	601,
	627,
	628,
	739,
	740,
	741,
	742,
	743,
};
static const Il2CppType* U3CtestCorU3Ec__Iterator5_t309_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CtestCorU3Ec__Iterator5_t309_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CtestCorU3Ec__Iterator5_t309_1_0_0;
struct U3CtestCorU3Ec__Iterator5_t309;
const Il2CppTypeDefinitionMetadata U3CtestCorU3Ec__Iterator5_t309_DefinitionMetadata = 
{
	&CoroutineScript_t310_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CtestCorU3Ec__Iterator5_t309_InterfacesTypeInfos/* implementedInterfaces */
	, U3CtestCorU3Ec__Iterator5_t309_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CtestCorU3Ec__Iterator5_t309_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2099/* fieldStart */
	, 1893/* methodStart */
	, -1/* eventStart */
	, 264/* propertyStart */

};
TypeInfo U3CtestCorU3Ec__Iterator5_t309_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<testCor>c__Iterator5"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CtestCorU3Ec__Iterator5_t309_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 798/* custom_attributes_cache */
	, &U3CtestCorU3Ec__Iterator5_t309_0_0_0/* byval_arg */
	, &U3CtestCorU3Ec__Iterator5_t309_1_0_0/* this_arg */
	, &U3CtestCorU3Ec__Iterator5_t309_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CtestCorU3Ec__Iterator5_t309)/* instance_size */
	, sizeof (U3CtestCorU3Ec__Iterator5_t309)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DoorController
#include "AssemblyU2DCSharp_DoorController.h"
// Metadata Definition DoorController
extern TypeInfo DoorController_t311_il2cpp_TypeInfo;
// DoorController
#include "AssemblyU2DCSharp_DoorControllerMethodDeclarations.h"
static const EncodedMethodIndex DoorController_t311_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DoorController_t311_0_0_0;
extern const Il2CppType DoorController_t311_1_0_0;
struct DoorController_t311;
const Il2CppTypeDefinitionMetadata DoorController_t311_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, DoorController_t311_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2103/* fieldStart */
	, 1899/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DoorController_t311_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DoorController"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DoorController_t311_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DoorController_t311_0_0_0/* byval_arg */
	, &DoorController_t311_1_0_0/* this_arg */
	, &DoorController_t311_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DoorController_t311)/* instance_size */
	, sizeof (DoorController_t311)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// FadeTexture
#include "AssemblyU2DCSharp_FadeTexture.h"
// Metadata Definition FadeTexture
extern TypeInfo FadeTexture_t313_il2cpp_TypeInfo;
// FadeTexture
#include "AssemblyU2DCSharp_FadeTextureMethodDeclarations.h"
static const EncodedMethodIndex FadeTexture_t313_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType FadeTexture_t313_0_0_0;
extern const Il2CppType FadeTexture_t313_1_0_0;
struct FadeTexture_t313;
const Il2CppTypeDefinitionMetadata FadeTexture_t313_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, FadeTexture_t313_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2104/* fieldStart */
	, 1905/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FadeTexture_t313_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "FadeTexture"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &FadeTexture_t313_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FadeTexture_t313_0_0_0/* byval_arg */
	, &FadeTexture_t313_1_0_0/* this_arg */
	, &FadeTexture_t313_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FadeTexture_t313)/* instance_size */
	, sizeof (FadeTexture_t313)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// GetFPS
#include "AssemblyU2DCSharp_GetFPS.h"
// Metadata Definition GetFPS
extern TypeInfo GetFPS_t314_il2cpp_TypeInfo;
// GetFPS
#include "AssemblyU2DCSharp_GetFPSMethodDeclarations.h"
static const EncodedMethodIndex GetFPS_t314_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType GetFPS_t314_0_0_0;
extern const Il2CppType GetFPS_t314_1_0_0;
struct GetFPS_t314;
const Il2CppTypeDefinitionMetadata GetFPS_t314_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, GetFPS_t314_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2106/* fieldStart */
	, 1910/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GetFPS_t314_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "GetFPS"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &GetFPS_t314_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GetFPS_t314_0_0_0/* byval_arg */
	, &GetFPS_t314_1_0_0/* this_arg */
	, &GetFPS_t314_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GetFPS_t314)/* instance_size */
	, sizeof (GetFPS_t314)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// GoTo
#include "AssemblyU2DCSharp_GoTo.h"
// Metadata Definition GoTo
extern TypeInfo GoTo_t315_il2cpp_TypeInfo;
// GoTo
#include "AssemblyU2DCSharp_GoToMethodDeclarations.h"
extern const Il2CppType U3CWaitAndEnableU3Ec__Iterator6_t316_0_0_0;
static const Il2CppType* GoTo_t315_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CWaitAndEnableU3Ec__Iterator6_t316_0_0_0,
};
static const EncodedMethodIndex GoTo_t315_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType GoTo_t315_0_0_0;
extern const Il2CppType GoTo_t315_1_0_0;
struct GoTo_t315;
const Il2CppTypeDefinitionMetadata GoTo_t315_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GoTo_t315_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, GoTo_t315_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2109/* fieldStart */
	, 1913/* methodStart */
	, -1/* eventStart */
	, 266/* propertyStart */

};
TypeInfo GoTo_t315_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "GoTo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &GoTo_t315_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GoTo_t315_0_0_0/* byval_arg */
	, &GoTo_t315_1_0_0/* this_arg */
	, &GoTo_t315_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GoTo_t315)/* instance_size */
	, sizeof (GoTo_t315)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GoTo_t315_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// GoTo/<WaitAndEnable>c__Iterator6
#include "AssemblyU2DCSharp_GoTo_U3CWaitAndEnableU3Ec__Iterator6.h"
// Metadata Definition GoTo/<WaitAndEnable>c__Iterator6
extern TypeInfo U3CWaitAndEnableU3Ec__Iterator6_t316_il2cpp_TypeInfo;
// GoTo/<WaitAndEnable>c__Iterator6
#include "AssemblyU2DCSharp_GoTo_U3CWaitAndEnableU3Ec__Iterator6MethodDeclarations.h"
static const EncodedMethodIndex U3CWaitAndEnableU3Ec__Iterator6_t316_VTable[9] = 
{
	626,
	601,
	627,
	628,
	744,
	745,
	746,
	747,
	748,
};
static const Il2CppType* U3CWaitAndEnableU3Ec__Iterator6_t316_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitAndEnableU3Ec__Iterator6_t316_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CWaitAndEnableU3Ec__Iterator6_t316_1_0_0;
struct U3CWaitAndEnableU3Ec__Iterator6_t316;
const Il2CppTypeDefinitionMetadata U3CWaitAndEnableU3Ec__Iterator6_t316_DefinitionMetadata = 
{
	&GoTo_t315_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitAndEnableU3Ec__Iterator6_t316_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitAndEnableU3Ec__Iterator6_t316_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitAndEnableU3Ec__Iterator6_t316_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2115/* fieldStart */
	, 1921/* methodStart */
	, -1/* eventStart */
	, 267/* propertyStart */

};
TypeInfo U3CWaitAndEnableU3Ec__Iterator6_t316_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitAndEnable>c__Iterator6"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CWaitAndEnableU3Ec__Iterator6_t316_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 804/* custom_attributes_cache */
	, &U3CWaitAndEnableU3Ec__Iterator6_t316_0_0_0/* byval_arg */
	, &U3CWaitAndEnableU3Ec__Iterator6_t316_1_0_0/* this_arg */
	, &U3CWaitAndEnableU3Ec__Iterator6_t316_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitAndEnableU3Ec__Iterator6_t316)/* instance_size */
	, sizeof (U3CWaitAndEnableU3Ec__Iterator6_t316)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// HeadRotation
#include "AssemblyU2DCSharp_HeadRotation.h"
// Metadata Definition HeadRotation
extern TypeInfo HeadRotation_t320_il2cpp_TypeInfo;
// HeadRotation
#include "AssemblyU2DCSharp_HeadRotationMethodDeclarations.h"
static const EncodedMethodIndex HeadRotation_t320_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType HeadRotation_t320_0_0_0;
extern const Il2CppType HeadRotation_t320_1_0_0;
struct HeadRotation_t320;
const Il2CppTypeDefinitionMetadata HeadRotation_t320_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, HeadRotation_t320_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1927/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HeadRotation_t320_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "HeadRotation"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &HeadRotation_t320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HeadRotation_t320_0_0_0/* byval_arg */
	, &HeadRotation_t320_1_0_0/* this_arg */
	, &HeadRotation_t320_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HeadRotation_t320)/* instance_size */
	, sizeof (HeadRotation_t320)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// IKHandler
#include "AssemblyU2DCSharp_IKHandler.h"
// Metadata Definition IKHandler
extern TypeInfo IKHandler_t322_il2cpp_TypeInfo;
// IKHandler
#include "AssemblyU2DCSharp_IKHandlerMethodDeclarations.h"
static const EncodedMethodIndex IKHandler_t322_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType IKHandler_t322_0_0_0;
extern const Il2CppType IKHandler_t322_1_0_0;
struct IKHandler_t322;
const Il2CppTypeDefinitionMetadata IKHandler_t322_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, IKHandler_t322_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2122/* fieldStart */
	, 1930/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IKHandler_t322_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "IKHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &IKHandler_t322_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IKHandler_t322_0_0_0/* byval_arg */
	, &IKHandler_t322_1_0_0/* this_arg */
	, &IKHandler_t322_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IKHandler_t322)/* instance_size */
	, sizeof (IKHandler_t322)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// InteractiveBaby
#include "AssemblyU2DCSharp_InteractiveBaby.h"
// Metadata Definition InteractiveBaby
extern TypeInfo InteractiveBaby_t324_il2cpp_TypeInfo;
// InteractiveBaby
#include "AssemblyU2DCSharp_InteractiveBabyMethodDeclarations.h"
static const EncodedMethodIndex InteractiveBaby_t324_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType InteractiveBaby_t324_0_0_0;
extern const Il2CppType InteractiveBaby_t324_1_0_0;
struct InteractiveBaby_t324;
const Il2CppTypeDefinitionMetadata InteractiveBaby_t324_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, InteractiveBaby_t324_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2128/* fieldStart */
	, 1934/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InteractiveBaby_t324_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "InteractiveBaby"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &InteractiveBaby_t324_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InteractiveBaby_t324_0_0_0/* byval_arg */
	, &InteractiveBaby_t324_1_0_0/* this_arg */
	, &InteractiveBaby_t324_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InteractiveBaby_t324)/* instance_size */
	, sizeof (InteractiveBaby_t324)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// InteractiveBurnMama
#include "AssemblyU2DCSharp_InteractiveBurnMama.h"
// Metadata Definition InteractiveBurnMama
extern TypeInfo InteractiveBurnMama_t326_il2cpp_TypeInfo;
// InteractiveBurnMama
#include "AssemblyU2DCSharp_InteractiveBurnMamaMethodDeclarations.h"
static const EncodedMethodIndex InteractiveBurnMama_t326_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType InteractiveBurnMama_t326_0_0_0;
extern const Il2CppType InteractiveBurnMama_t326_1_0_0;
struct InteractiveBurnMama_t326;
const Il2CppTypeDefinitionMetadata InteractiveBurnMama_t326_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, InteractiveBurnMama_t326_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2135/* fieldStart */
	, 1943/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InteractiveBurnMama_t326_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "InteractiveBurnMama"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &InteractiveBurnMama_t326_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InteractiveBurnMama_t326_0_0_0/* byval_arg */
	, &InteractiveBurnMama_t326_1_0_0/* this_arg */
	, &InteractiveBurnMama_t326_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InteractiveBurnMama_t326)/* instance_size */
	, sizeof (InteractiveBurnMama_t326)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// InteractiveDroppingObject
#include "AssemblyU2DCSharp_InteractiveDroppingObject.h"
// Metadata Definition InteractiveDroppingObject
extern TypeInfo InteractiveDroppingObject_t327_il2cpp_TypeInfo;
// InteractiveDroppingObject
#include "AssemblyU2DCSharp_InteractiveDroppingObjectMethodDeclarations.h"
static const EncodedMethodIndex InteractiveDroppingObject_t327_VTable[5] = 
{
	600,
	601,
	602,
	603,
	749,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType InteractiveDroppingObject_t327_0_0_0;
extern const Il2CppType InteractiveDroppingObject_t327_1_0_0;
extern const Il2CppType InteractiveObject_t328_0_0_0;
struct InteractiveDroppingObject_t327;
const Il2CppTypeDefinitionMetadata InteractiveDroppingObject_t327_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &InteractiveObject_t328_0_0_0/* parent */
	, InteractiveDroppingObject_t327_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2140/* fieldStart */
	, 1950/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InteractiveDroppingObject_t327_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "InteractiveDroppingObject"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &InteractiveDroppingObject_t327_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InteractiveDroppingObject_t327_0_0_0/* byval_arg */
	, &InteractiveDroppingObject_t327_1_0_0/* this_arg */
	, &InteractiveDroppingObject_t327_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InteractiveDroppingObject_t327)/* instance_size */
	, sizeof (InteractiveDroppingObject_t327)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// InteractiveFlashlight
#include "AssemblyU2DCSharp_InteractiveFlashlight.h"
// Metadata Definition InteractiveFlashlight
extern TypeInfo InteractiveFlashlight_t329_il2cpp_TypeInfo;
// InteractiveFlashlight
#include "AssemblyU2DCSharp_InteractiveFlashlightMethodDeclarations.h"
static const EncodedMethodIndex InteractiveFlashlight_t329_VTable[5] = 
{
	600,
	601,
	602,
	603,
	750,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType InteractiveFlashlight_t329_0_0_0;
extern const Il2CppType InteractiveFlashlight_t329_1_0_0;
struct InteractiveFlashlight_t329;
const Il2CppTypeDefinitionMetadata InteractiveFlashlight_t329_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &InteractiveObject_t328_0_0_0/* parent */
	, InteractiveFlashlight_t329_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2141/* fieldStart */
	, 1953/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InteractiveFlashlight_t329_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "InteractiveFlashlight"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &InteractiveFlashlight_t329_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InteractiveFlashlight_t329_0_0_0/* byval_arg */
	, &InteractiveFlashlight_t329_1_0_0/* this_arg */
	, &InteractiveFlashlight_t329_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InteractiveFlashlight_t329)/* instance_size */
	, sizeof (InteractiveFlashlight_t329)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// InteractiveMama
#include "AssemblyU2DCSharp_InteractiveMama.h"
// Metadata Definition InteractiveMama
extern TypeInfo InteractiveMama_t330_il2cpp_TypeInfo;
// InteractiveMama
#include "AssemblyU2DCSharp_InteractiveMamaMethodDeclarations.h"
extern const Il2CppType U3CWaitAndEnableMamaU3Ec__Iterator7_t331_0_0_0;
extern const Il2CppType U3CWaitAndMamaPosU3Ec__Iterator8_t332_0_0_0;
extern const Il2CppType U3CWaitAndHideMamaU3Ec__Iterator9_t333_0_0_0;
static const Il2CppType* InteractiveMama_t330_il2cpp_TypeInfo__nestedTypes[3] =
{
	&U3CWaitAndEnableMamaU3Ec__Iterator7_t331_0_0_0,
	&U3CWaitAndMamaPosU3Ec__Iterator8_t332_0_0_0,
	&U3CWaitAndHideMamaU3Ec__Iterator9_t333_0_0_0,
};
static const EncodedMethodIndex InteractiveMama_t330_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType InteractiveMama_t330_0_0_0;
extern const Il2CppType InteractiveMama_t330_1_0_0;
struct InteractiveMama_t330;
const Il2CppTypeDefinitionMetadata InteractiveMama_t330_DefinitionMetadata = 
{
	NULL/* declaringType */
	, InteractiveMama_t330_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, InteractiveMama_t330_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2144/* fieldStart */
	, 1959/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InteractiveMama_t330_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "InteractiveMama"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &InteractiveMama_t330_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InteractiveMama_t330_0_0_0/* byval_arg */
	, &InteractiveMama_t330_1_0_0/* this_arg */
	, &InteractiveMama_t330_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InteractiveMama_t330)/* instance_size */
	, sizeof (InteractiveMama_t330)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(InteractiveMama_t330_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// InteractiveMama/<WaitAndEnableMama>c__Iterator7
#include "AssemblyU2DCSharp_InteractiveMama_U3CWaitAndEnableMamaU3Ec__.h"
// Metadata Definition InteractiveMama/<WaitAndEnableMama>c__Iterator7
extern TypeInfo U3CWaitAndEnableMamaU3Ec__Iterator7_t331_il2cpp_TypeInfo;
// InteractiveMama/<WaitAndEnableMama>c__Iterator7
#include "AssemblyU2DCSharp_InteractiveMama_U3CWaitAndEnableMamaU3Ec__MethodDeclarations.h"
static const EncodedMethodIndex U3CWaitAndEnableMamaU3Ec__Iterator7_t331_VTable[9] = 
{
	626,
	601,
	627,
	628,
	751,
	752,
	753,
	754,
	755,
};
static const Il2CppType* U3CWaitAndEnableMamaU3Ec__Iterator7_t331_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitAndEnableMamaU3Ec__Iterator7_t331_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CWaitAndEnableMamaU3Ec__Iterator7_t331_1_0_0;
struct U3CWaitAndEnableMamaU3Ec__Iterator7_t331;
const Il2CppTypeDefinitionMetadata U3CWaitAndEnableMamaU3Ec__Iterator7_t331_DefinitionMetadata = 
{
	&InteractiveMama_t330_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitAndEnableMamaU3Ec__Iterator7_t331_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitAndEnableMamaU3Ec__Iterator7_t331_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitAndEnableMamaU3Ec__Iterator7_t331_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2153/* fieldStart */
	, 1973/* methodStart */
	, -1/* eventStart */
	, 269/* propertyStart */

};
TypeInfo U3CWaitAndEnableMamaU3Ec__Iterator7_t331_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitAndEnableMama>c__Iterator7"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CWaitAndEnableMamaU3Ec__Iterator7_t331_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 812/* custom_attributes_cache */
	, &U3CWaitAndEnableMamaU3Ec__Iterator7_t331_0_0_0/* byval_arg */
	, &U3CWaitAndEnableMamaU3Ec__Iterator7_t331_1_0_0/* this_arg */
	, &U3CWaitAndEnableMamaU3Ec__Iterator7_t331_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitAndEnableMamaU3Ec__Iterator7_t331)/* instance_size */
	, sizeof (U3CWaitAndEnableMamaU3Ec__Iterator7_t331)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// InteractiveMama/<WaitAndMamaPos>c__Iterator8
#include "AssemblyU2DCSharp_InteractiveMama_U3CWaitAndMamaPosU3Ec__Ite.h"
// Metadata Definition InteractiveMama/<WaitAndMamaPos>c__Iterator8
extern TypeInfo U3CWaitAndMamaPosU3Ec__Iterator8_t332_il2cpp_TypeInfo;
// InteractiveMama/<WaitAndMamaPos>c__Iterator8
#include "AssemblyU2DCSharp_InteractiveMama_U3CWaitAndMamaPosU3Ec__IteMethodDeclarations.h"
static const EncodedMethodIndex U3CWaitAndMamaPosU3Ec__Iterator8_t332_VTable[9] = 
{
	626,
	601,
	627,
	628,
	756,
	757,
	758,
	759,
	760,
};
static const Il2CppType* U3CWaitAndMamaPosU3Ec__Iterator8_t332_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitAndMamaPosU3Ec__Iterator8_t332_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CWaitAndMamaPosU3Ec__Iterator8_t332_1_0_0;
struct U3CWaitAndMamaPosU3Ec__Iterator8_t332;
const Il2CppTypeDefinitionMetadata U3CWaitAndMamaPosU3Ec__Iterator8_t332_DefinitionMetadata = 
{
	&InteractiveMama_t330_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitAndMamaPosU3Ec__Iterator8_t332_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitAndMamaPosU3Ec__Iterator8_t332_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitAndMamaPosU3Ec__Iterator8_t332_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2161/* fieldStart */
	, 1979/* methodStart */
	, -1/* eventStart */
	, 271/* propertyStart */

};
TypeInfo U3CWaitAndMamaPosU3Ec__Iterator8_t332_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitAndMamaPos>c__Iterator8"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CWaitAndMamaPosU3Ec__Iterator8_t332_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 817/* custom_attributes_cache */
	, &U3CWaitAndMamaPosU3Ec__Iterator8_t332_0_0_0/* byval_arg */
	, &U3CWaitAndMamaPosU3Ec__Iterator8_t332_1_0_0/* this_arg */
	, &U3CWaitAndMamaPosU3Ec__Iterator8_t332_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitAndMamaPosU3Ec__Iterator8_t332)/* instance_size */
	, sizeof (U3CWaitAndMamaPosU3Ec__Iterator8_t332)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// InteractiveMama/<WaitAndHideMama>c__Iterator9
#include "AssemblyU2DCSharp_InteractiveMama_U3CWaitAndHideMamaU3Ec__It.h"
// Metadata Definition InteractiveMama/<WaitAndHideMama>c__Iterator9
extern TypeInfo U3CWaitAndHideMamaU3Ec__Iterator9_t333_il2cpp_TypeInfo;
// InteractiveMama/<WaitAndHideMama>c__Iterator9
#include "AssemblyU2DCSharp_InteractiveMama_U3CWaitAndHideMamaU3Ec__ItMethodDeclarations.h"
static const EncodedMethodIndex U3CWaitAndHideMamaU3Ec__Iterator9_t333_VTable[9] = 
{
	626,
	601,
	627,
	628,
	761,
	762,
	763,
	764,
	765,
};
static const Il2CppType* U3CWaitAndHideMamaU3Ec__Iterator9_t333_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitAndHideMamaU3Ec__Iterator9_t333_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CWaitAndHideMamaU3Ec__Iterator9_t333_1_0_0;
struct U3CWaitAndHideMamaU3Ec__Iterator9_t333;
const Il2CppTypeDefinitionMetadata U3CWaitAndHideMamaU3Ec__Iterator9_t333_DefinitionMetadata = 
{
	&InteractiveMama_t330_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitAndHideMamaU3Ec__Iterator9_t333_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitAndHideMamaU3Ec__Iterator9_t333_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitAndHideMamaU3Ec__Iterator9_t333_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2168/* fieldStart */
	, 1985/* methodStart */
	, -1/* eventStart */
	, 273/* propertyStart */

};
TypeInfo U3CWaitAndHideMamaU3Ec__Iterator9_t333_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitAndHideMama>c__Iterator9"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CWaitAndHideMamaU3Ec__Iterator9_t333_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 822/* custom_attributes_cache */
	, &U3CWaitAndHideMamaU3Ec__Iterator9_t333_0_0_0/* byval_arg */
	, &U3CWaitAndHideMamaU3Ec__Iterator9_t333_1_0_0/* this_arg */
	, &U3CWaitAndHideMamaU3Ec__Iterator9_t333_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitAndHideMamaU3Ec__Iterator9_t333)/* instance_size */
	, sizeof (U3CWaitAndHideMamaU3Ec__Iterator9_t333)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// InteractiveObject
#include "AssemblyU2DCSharp_InteractiveObject.h"
// Metadata Definition InteractiveObject
extern TypeInfo InteractiveObject_t328_il2cpp_TypeInfo;
// InteractiveObject
#include "AssemblyU2DCSharp_InteractiveObjectMethodDeclarations.h"
static const EncodedMethodIndex InteractiveObject_t328_VTable[5] = 
{
	600,
	601,
	602,
	603,
	749,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType InteractiveObject_t328_1_0_0;
struct InteractiveObject_t328;
const Il2CppTypeDefinitionMetadata InteractiveObject_t328_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, InteractiveObject_t328_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2177/* fieldStart */
	, 1991/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InteractiveObject_t328_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "InteractiveObject"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &InteractiveObject_t328_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 827/* custom_attributes_cache */
	, &InteractiveObject_t328_0_0_0/* byval_arg */
	, &InteractiveObject_t328_1_0_0/* this_arg */
	, &InteractiveObject_t328_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InteractiveObject_t328)/* instance_size */
	, sizeof (InteractiveObject_t328)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// InteractiveParticles
#include "AssemblyU2DCSharp_InteractiveParticles.h"
// Metadata Definition InteractiveParticles
extern TypeInfo InteractiveParticles_t334_il2cpp_TypeInfo;
// InteractiveParticles
#include "AssemblyU2DCSharp_InteractiveParticlesMethodDeclarations.h"
static const EncodedMethodIndex InteractiveParticles_t334_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType InteractiveParticles_t334_0_0_0;
extern const Il2CppType InteractiveParticles_t334_1_0_0;
struct InteractiveParticles_t334;
const Il2CppTypeDefinitionMetadata InteractiveParticles_t334_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, InteractiveParticles_t334_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 1995/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InteractiveParticles_t334_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "InteractiveParticles"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &InteractiveParticles_t334_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InteractiveParticles_t334_0_0_0/* byval_arg */
	, &InteractiveParticles_t334_1_0_0/* this_arg */
	, &InteractiveParticles_t334_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InteractiveParticles_t334)/* instance_size */
	, sizeof (InteractiveParticles_t334)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// InteractivePlasticBag
#include "AssemblyU2DCSharp_InteractivePlasticBag.h"
// Metadata Definition InteractivePlasticBag
extern TypeInfo InteractivePlasticBag_t335_il2cpp_TypeInfo;
// InteractivePlasticBag
#include "AssemblyU2DCSharp_InteractivePlasticBagMethodDeclarations.h"
extern const Il2CppType U3CWaitAndWiggleU3Ec__IteratorA_t336_0_0_0;
static const Il2CppType* InteractivePlasticBag_t335_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CWaitAndWiggleU3Ec__IteratorA_t336_0_0_0,
};
static const EncodedMethodIndex InteractivePlasticBag_t335_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType InteractivePlasticBag_t335_0_0_0;
extern const Il2CppType InteractivePlasticBag_t335_1_0_0;
struct InteractivePlasticBag_t335;
const Il2CppTypeDefinitionMetadata InteractivePlasticBag_t335_DefinitionMetadata = 
{
	NULL/* declaringType */
	, InteractivePlasticBag_t335_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, InteractivePlasticBag_t335_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2179/* fieldStart */
	, 2000/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InteractivePlasticBag_t335_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "InteractivePlasticBag"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &InteractivePlasticBag_t335_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InteractivePlasticBag_t335_0_0_0/* byval_arg */
	, &InteractivePlasticBag_t335_1_0_0/* this_arg */
	, &InteractivePlasticBag_t335_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InteractivePlasticBag_t335)/* instance_size */
	, sizeof (InteractivePlasticBag_t335)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// InteractivePlasticBag/<WaitAndWiggle>c__IteratorA
#include "AssemblyU2DCSharp_InteractivePlasticBag_U3CWaitAndWiggleU3Ec.h"
// Metadata Definition InteractivePlasticBag/<WaitAndWiggle>c__IteratorA
extern TypeInfo U3CWaitAndWiggleU3Ec__IteratorA_t336_il2cpp_TypeInfo;
// InteractivePlasticBag/<WaitAndWiggle>c__IteratorA
#include "AssemblyU2DCSharp_InteractivePlasticBag_U3CWaitAndWiggleU3EcMethodDeclarations.h"
static const EncodedMethodIndex U3CWaitAndWiggleU3Ec__IteratorA_t336_VTable[9] = 
{
	626,
	601,
	627,
	628,
	766,
	767,
	768,
	769,
	770,
};
static const Il2CppType* U3CWaitAndWiggleU3Ec__IteratorA_t336_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitAndWiggleU3Ec__IteratorA_t336_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CWaitAndWiggleU3Ec__IteratorA_t336_1_0_0;
struct U3CWaitAndWiggleU3Ec__IteratorA_t336;
const Il2CppTypeDefinitionMetadata U3CWaitAndWiggleU3Ec__IteratorA_t336_DefinitionMetadata = 
{
	&InteractivePlasticBag_t335_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitAndWiggleU3Ec__IteratorA_t336_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitAndWiggleU3Ec__IteratorA_t336_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitAndWiggleU3Ec__IteratorA_t336_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2181/* fieldStart */
	, 2006/* methodStart */
	, -1/* eventStart */
	, 275/* propertyStart */

};
TypeInfo U3CWaitAndWiggleU3Ec__IteratorA_t336_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitAndWiggle>c__IteratorA"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CWaitAndWiggleU3Ec__IteratorA_t336_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 829/* custom_attributes_cache */
	, &U3CWaitAndWiggleU3Ec__IteratorA_t336_0_0_0/* byval_arg */
	, &U3CWaitAndWiggleU3Ec__IteratorA_t336_1_0_0/* this_arg */
	, &U3CWaitAndWiggleU3Ec__IteratorA_t336_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitAndWiggleU3Ec__IteratorA_t336)/* instance_size */
	, sizeof (U3CWaitAndWiggleU3Ec__IteratorA_t336)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// InteractiveRadio
#include "AssemblyU2DCSharp_InteractiveRadio.h"
// Metadata Definition InteractiveRadio
extern TypeInfo InteractiveRadio_t340_il2cpp_TypeInfo;
// InteractiveRadio
#include "AssemblyU2DCSharp_InteractiveRadioMethodDeclarations.h"
static const EncodedMethodIndex InteractiveRadio_t340_VTable[5] = 
{
	600,
	601,
	602,
	603,
	771,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType InteractiveRadio_t340_0_0_0;
extern const Il2CppType InteractiveRadio_t340_1_0_0;
struct InteractiveRadio_t340;
const Il2CppTypeDefinitionMetadata InteractiveRadio_t340_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &InteractiveObject_t328_0_0_0/* parent */
	, InteractiveRadio_t340_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2186/* fieldStart */
	, 2012/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InteractiveRadio_t340_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "InteractiveRadio"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &InteractiveRadio_t340_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 834/* custom_attributes_cache */
	, &InteractiveRadio_t340_0_0_0/* byval_arg */
	, &InteractiveRadio_t340_1_0_0/* this_arg */
	, &InteractiveRadio_t340_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InteractiveRadio_t340)/* instance_size */
	, sizeof (InteractiveRadio_t340)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// InteractiveShadowMan
#include "AssemblyU2DCSharp_InteractiveShadowMan.h"
// Metadata Definition InteractiveShadowMan
extern TypeInfo InteractiveShadowMan_t341_il2cpp_TypeInfo;
// InteractiveShadowMan
#include "AssemblyU2DCSharp_InteractiveShadowManMethodDeclarations.h"
static const EncodedMethodIndex InteractiveShadowMan_t341_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType InteractiveShadowMan_t341_0_0_0;
extern const Il2CppType InteractiveShadowMan_t341_1_0_0;
struct InteractiveShadowMan_t341;
const Il2CppTypeDefinitionMetadata InteractiveShadowMan_t341_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, InteractiveShadowMan_t341_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2188/* fieldStart */
	, 2017/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InteractiveShadowMan_t341_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "InteractiveShadowMan"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &InteractiveShadowMan_t341_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InteractiveShadowMan_t341_0_0_0/* byval_arg */
	, &InteractiveShadowMan_t341_1_0_0/* this_arg */
	, &InteractiveShadowMan_t341_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InteractiveShadowMan_t341)/* instance_size */
	, sizeof (InteractiveShadowMan_t341)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// InteractiveWheelChair
#include "AssemblyU2DCSharp_InteractiveWheelChair.h"
// Metadata Definition InteractiveWheelChair
extern TypeInfo InteractiveWheelChair_t342_il2cpp_TypeInfo;
// InteractiveWheelChair
#include "AssemblyU2DCSharp_InteractiveWheelChairMethodDeclarations.h"
static const EncodedMethodIndex InteractiveWheelChair_t342_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType InteractiveWheelChair_t342_0_0_0;
extern const Il2CppType InteractiveWheelChair_t342_1_0_0;
struct InteractiveWheelChair_t342;
const Il2CppTypeDefinitionMetadata InteractiveWheelChair_t342_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, InteractiveWheelChair_t342_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2189/* fieldStart */
	, 2021/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InteractiveWheelChair_t342_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "InteractiveWheelChair"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &InteractiveWheelChair_t342_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InteractiveWheelChair_t342_0_0_0/* byval_arg */
	, &InteractiveWheelChair_t342_1_0_0/* this_arg */
	, &InteractiveWheelChair_t342_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InteractiveWheelChair_t342)/* instance_size */
	, sizeof (InteractiveWheelChair_t342)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// LightController
#include "AssemblyU2DCSharp_LightController.h"
// Metadata Definition LightController
extern TypeInfo LightController_t343_il2cpp_TypeInfo;
// LightController
#include "AssemblyU2DCSharp_LightControllerMethodDeclarations.h"
extern const Il2CppType U3CFlickerMeU3Ec__IteratorB_t344_0_0_0;
extern const Il2CppType U3CWaitAndSetFlickerU3Ec__IteratorC_t345_0_0_0;
static const Il2CppType* LightController_t343_il2cpp_TypeInfo__nestedTypes[2] =
{
	&U3CFlickerMeU3Ec__IteratorB_t344_0_0_0,
	&U3CWaitAndSetFlickerU3Ec__IteratorC_t345_0_0_0,
};
static const EncodedMethodIndex LightController_t343_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType LightController_t343_0_0_0;
extern const Il2CppType LightController_t343_1_0_0;
struct LightController_t343;
const Il2CppTypeDefinitionMetadata LightController_t343_DefinitionMetadata = 
{
	NULL/* declaringType */
	, LightController_t343_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, LightController_t343_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2195/* fieldStart */
	, 2026/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LightController_t343_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "LightController"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &LightController_t343_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LightController_t343_0_0_0/* byval_arg */
	, &LightController_t343_1_0_0/* this_arg */
	, &LightController_t343_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LightController_t343)/* instance_size */
	, sizeof (LightController_t343)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// LightController/<FlickerMe>c__IteratorB
#include "AssemblyU2DCSharp_LightController_U3CFlickerMeU3Ec__Iterator.h"
// Metadata Definition LightController/<FlickerMe>c__IteratorB
extern TypeInfo U3CFlickerMeU3Ec__IteratorB_t344_il2cpp_TypeInfo;
// LightController/<FlickerMe>c__IteratorB
#include "AssemblyU2DCSharp_LightController_U3CFlickerMeU3Ec__IteratorMethodDeclarations.h"
static const EncodedMethodIndex U3CFlickerMeU3Ec__IteratorB_t344_VTable[9] = 
{
	626,
	601,
	627,
	628,
	772,
	773,
	774,
	775,
	776,
};
static const Il2CppType* U3CFlickerMeU3Ec__IteratorB_t344_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CFlickerMeU3Ec__IteratorB_t344_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CFlickerMeU3Ec__IteratorB_t344_1_0_0;
struct U3CFlickerMeU3Ec__IteratorB_t344;
const Il2CppTypeDefinitionMetadata U3CFlickerMeU3Ec__IteratorB_t344_DefinitionMetadata = 
{
	&LightController_t343_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CFlickerMeU3Ec__IteratorB_t344_InterfacesTypeInfos/* implementedInterfaces */
	, U3CFlickerMeU3Ec__IteratorB_t344_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CFlickerMeU3Ec__IteratorB_t344_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2209/* fieldStart */
	, 2038/* methodStart */
	, -1/* eventStart */
	, 277/* propertyStart */

};
TypeInfo U3CFlickerMeU3Ec__IteratorB_t344_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<FlickerMe>c__IteratorB"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CFlickerMeU3Ec__IteratorB_t344_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 837/* custom_attributes_cache */
	, &U3CFlickerMeU3Ec__IteratorB_t344_0_0_0/* byval_arg */
	, &U3CFlickerMeU3Ec__IteratorB_t344_1_0_0/* this_arg */
	, &U3CFlickerMeU3Ec__IteratorB_t344_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CFlickerMeU3Ec__IteratorB_t344)/* instance_size */
	, sizeof (U3CFlickerMeU3Ec__IteratorB_t344)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// LightController/<WaitAndSetFlicker>c__IteratorC
#include "AssemblyU2DCSharp_LightController_U3CWaitAndSetFlickerU3Ec__.h"
// Metadata Definition LightController/<WaitAndSetFlicker>c__IteratorC
extern TypeInfo U3CWaitAndSetFlickerU3Ec__IteratorC_t345_il2cpp_TypeInfo;
// LightController/<WaitAndSetFlicker>c__IteratorC
#include "AssemblyU2DCSharp_LightController_U3CWaitAndSetFlickerU3Ec__MethodDeclarations.h"
static const EncodedMethodIndex U3CWaitAndSetFlickerU3Ec__IteratorC_t345_VTable[9] = 
{
	626,
	601,
	627,
	628,
	777,
	778,
	779,
	780,
	781,
};
static const Il2CppType* U3CWaitAndSetFlickerU3Ec__IteratorC_t345_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitAndSetFlickerU3Ec__IteratorC_t345_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CWaitAndSetFlickerU3Ec__IteratorC_t345_1_0_0;
struct U3CWaitAndSetFlickerU3Ec__IteratorC_t345;
const Il2CppTypeDefinitionMetadata U3CWaitAndSetFlickerU3Ec__IteratorC_t345_DefinitionMetadata = 
{
	&LightController_t343_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitAndSetFlickerU3Ec__IteratorC_t345_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitAndSetFlickerU3Ec__IteratorC_t345_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitAndSetFlickerU3Ec__IteratorC_t345_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2216/* fieldStart */
	, 2044/* methodStart */
	, -1/* eventStart */
	, 279/* propertyStart */

};
TypeInfo U3CWaitAndSetFlickerU3Ec__IteratorC_t345_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitAndSetFlicker>c__IteratorC"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CWaitAndSetFlickerU3Ec__IteratorC_t345_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 842/* custom_attributes_cache */
	, &U3CWaitAndSetFlickerU3Ec__IteratorC_t345_0_0_0/* byval_arg */
	, &U3CWaitAndSetFlickerU3Ec__IteratorC_t345_1_0_0/* this_arg */
	, &U3CWaitAndSetFlickerU3Ec__IteratorC_t345_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitAndSetFlickerU3Ec__IteratorC_t345)/* instance_size */
	, sizeof (U3CWaitAndSetFlickerU3Ec__IteratorC_t345)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Loader
#include "AssemblyU2DCSharp_Loader.h"
// Metadata Definition Loader
extern TypeInfo Loader_t346_il2cpp_TypeInfo;
// Loader
#include "AssemblyU2DCSharp_LoaderMethodDeclarations.h"
static const EncodedMethodIndex Loader_t346_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Loader_t346_0_0_0;
extern const Il2CppType Loader_t346_1_0_0;
struct Loader_t346;
const Il2CppTypeDefinitionMetadata Loader_t346_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, Loader_t346_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2223/* fieldStart */
	, 2050/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Loader_t346_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Loader"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Loader_t346_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Loader_t346_0_0_0/* byval_arg */
	, &Loader_t346_1_0_0/* this_arg */
	, &Loader_t346_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Loader_t346)/* instance_size */
	, sizeof (Loader_t346)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// AppEvent
#include "AssemblyU2DCSharp_AppEvent.h"
// Metadata Definition AppEvent
extern TypeInfo AppEvent_t347_il2cpp_TypeInfo;
// AppEvent
#include "AssemblyU2DCSharp_AppEventMethodDeclarations.h"
static const EncodedMethodIndex AppEvent_t347_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair AppEvent_t347_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType AppEvent_t347_0_0_0;
extern const Il2CppType AppEvent_t347_1_0_0;
const Il2CppTypeDefinitionMetadata AppEvent_t347_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AppEvent_t347_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, AppEvent_t347_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2228/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AppEvent_t347_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppEvent"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AppEvent_t347_0_0_0/* byval_arg */
	, &AppEvent_t347_1_0_0/* this_arg */
	, &AppEvent_t347_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppEvent_t347)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AppEvent_t347)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
