﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// Cardboard/DistortionCorrectionMethod
#include "AssemblyU2DCSharp_Cardboard_DistortionCorrectionMethod.h"
// Cardboard/DistortionCorrectionMethod
struct  DistortionCorrectionMethod_t228 
{
	// System.Int32 Cardboard/DistortionCorrectionMethod::value__
	int32_t ___value___1;
};
