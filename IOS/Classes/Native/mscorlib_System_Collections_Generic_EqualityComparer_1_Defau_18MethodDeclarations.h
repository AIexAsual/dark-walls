﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>
struct DefaultComparer_t2959;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::.ctor()
extern "C" void DefaultComparer__ctor_m22999_gshared (DefaultComparer_t2959 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22999(__this, method) (( void (*) (DefaultComparer_t2959 *, const MethodInfo*))DefaultComparer__ctor_m22999_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m23000_gshared (DefaultComparer_t2959 * __this, DateTime_t871  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m23000(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2959 *, DateTime_t871 , const MethodInfo*))DefaultComparer_GetHashCode_m23000_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m23001_gshared (DefaultComparer_t2959 * __this, DateTime_t871  ___x, DateTime_t871  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m23001(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2959 *, DateTime_t871 , DateTime_t871 , const MethodInfo*))DefaultComparer_Equals_m23001_gshared)(__this, ___x, ___y, method)
