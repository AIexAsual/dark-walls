﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vector3OrTransform
struct Vector3OrTransform_t422;

// System.Void Vector3OrTransform::.ctor()
extern "C" void Vector3OrTransform__ctor_m2400 (Vector3OrTransform_t422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vector3OrTransform::.cctor()
extern "C" void Vector3OrTransform__cctor_m2401 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
