﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t269;
// System.Object
#include "mscorlib_System_Object.h"
// ArrayIndexes
struct  ArrayIndexes_t441  : public Object_t
{
	// System.Int32[] ArrayIndexes::indexes
	Int32U5BU5D_t269* ___indexes_0;
};
