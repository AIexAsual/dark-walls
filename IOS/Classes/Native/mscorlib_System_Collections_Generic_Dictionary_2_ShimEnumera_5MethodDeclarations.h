﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ShimEnumerator_t2797;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t2788;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m21563_gshared (ShimEnumerator_t2797 * __this, Dictionary_2_t2788 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m21563(__this, ___host, method) (( void (*) (ShimEnumerator_t2797 *, Dictionary_2_t2788 *, const MethodInfo*))ShimEnumerator__ctor_m21563_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m21564_gshared (ShimEnumerator_t2797 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m21564(__this, method) (( bool (*) (ShimEnumerator_t2797 *, const MethodInfo*))ShimEnumerator_MoveNext_m21564_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Entry()
extern "C" DictionaryEntry_t552  ShimEnumerator_get_Entry_m21565_gshared (ShimEnumerator_t2797 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m21565(__this, method) (( DictionaryEntry_t552  (*) (ShimEnumerator_t2797 *, const MethodInfo*))ShimEnumerator_get_Entry_m21565_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m21566_gshared (ShimEnumerator_t2797 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m21566(__this, method) (( Object_t * (*) (ShimEnumerator_t2797 *, const MethodInfo*))ShimEnumerator_get_Key_m21566_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m21567_gshared (ShimEnumerator_t2797 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m21567(__this, method) (( Object_t * (*) (ShimEnumerator_t2797 *, const MethodInfo*))ShimEnumerator_get_Value_m21567_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m21568_gshared (ShimEnumerator_t2797 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m21568(__this, method) (( Object_t * (*) (ShimEnumerator_t2797 *, const MethodInfo*))ShimEnumerator_get_Current_m21568_gshared)(__this, method)
