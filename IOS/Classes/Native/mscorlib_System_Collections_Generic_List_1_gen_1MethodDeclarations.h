﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<RagdollHelper/BodyPart>
struct List_1_t401;
// System.Object
struct Object_t;
// RagdollHelper/BodyPart
struct BodyPart_t400;
// System.Collections.Generic.IEnumerable`1<RagdollHelper/BodyPart>
struct IEnumerable_1_t2988;
// System.Collections.Generic.IEnumerator`1<RagdollHelper/BodyPart>
struct IEnumerator_1_t2989;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<RagdollHelper/BodyPart>
struct ICollection_1_t2990;
// System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>
struct ReadOnlyCollection_1_t2309;
// RagdollHelper/BodyPart[]
struct BodyPartU5BU5D_t2307;
// System.Predicate`1<RagdollHelper/BodyPart>
struct Predicate_1_t2310;
// System.Comparison`1<RagdollHelper/BodyPart>
struct Comparison_1_t2311;
// System.Collections.Generic.List`1/Enumerator<RagdollHelper/BodyPart>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_30MethodDeclarations.h"
#define List_1__ctor_m3210(__this, method) (( void (*) (List_1_t401 *, const MethodInfo*))List_1__ctor_m6426_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m14530(__this, ___collection, method) (( void (*) (List_1_t401 *, Object_t*, const MethodInfo*))List_1__ctor_m13490_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::.ctor(System.Int32)
#define List_1__ctor_m14531(__this, ___capacity, method) (( void (*) (List_1_t401 *, int32_t, const MethodInfo*))List_1__ctor_m13492_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::.cctor()
#define List_1__cctor_m14532(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13494_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<RagdollHelper/BodyPart>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14533(__this, method) (( Object_t* (*) (List_1_t401 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13496_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m14534(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t401 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m13498_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<RagdollHelper/BodyPart>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m14535(__this, method) (( Object_t * (*) (List_1_t401 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m13500_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<RagdollHelper/BodyPart>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m14536(__this, ___item, method) (( int32_t (*) (List_1_t401 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m13502_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<RagdollHelper/BodyPart>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m14537(__this, ___item, method) (( bool (*) (List_1_t401 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m13504_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<RagdollHelper/BodyPart>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m14538(__this, ___item, method) (( int32_t (*) (List_1_t401 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m13506_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m14539(__this, ___index, ___item, method) (( void (*) (List_1_t401 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m13508_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m14540(__this, ___item, method) (( void (*) (List_1_t401 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m13510_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<RagdollHelper/BodyPart>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14541(__this, method) (( bool (*) (List_1_t401 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13512_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<RagdollHelper/BodyPart>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m14542(__this, method) (( bool (*) (List_1_t401 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m13514_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<RagdollHelper/BodyPart>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m14543(__this, method) (( Object_t * (*) (List_1_t401 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m13516_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<RagdollHelper/BodyPart>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m14544(__this, method) (( bool (*) (List_1_t401 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m13518_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<RagdollHelper/BodyPart>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m14545(__this, method) (( bool (*) (List_1_t401 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m13520_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<RagdollHelper/BodyPart>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m14546(__this, ___index, method) (( Object_t * (*) (List_1_t401 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m13522_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m14547(__this, ___index, ___value, method) (( void (*) (List_1_t401 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m13524_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::Add(T)
#define List_1_Add_m14548(__this, ___item, method) (( void (*) (List_1_t401 *, BodyPart_t400 *, const MethodInfo*))List_1_Add_m13526_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m14549(__this, ___newCount, method) (( void (*) (List_1_t401 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13528_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m14550(__this, ___collection, method) (( void (*) (List_1_t401 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13530_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m14551(__this, ___enumerable, method) (( void (*) (List_1_t401 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13532_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m14552(__this, ___collection, method) (( void (*) (List_1_t401 *, Object_t*, const MethodInfo*))List_1_AddRange_m13534_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<RagdollHelper/BodyPart>::AsReadOnly()
#define List_1_AsReadOnly_m14553(__this, method) (( ReadOnlyCollection_1_t2309 * (*) (List_1_t401 *, const MethodInfo*))List_1_AsReadOnly_m13536_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::Clear()
#define List_1_Clear_m14554(__this, method) (( void (*) (List_1_t401 *, const MethodInfo*))List_1_Clear_m13538_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<RagdollHelper/BodyPart>::Contains(T)
#define List_1_Contains_m14555(__this, ___item, method) (( bool (*) (List_1_t401 *, BodyPart_t400 *, const MethodInfo*))List_1_Contains_m13540_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m14556(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t401 *, BodyPartU5BU5D_t2307*, int32_t, const MethodInfo*))List_1_CopyTo_m13542_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<RagdollHelper/BodyPart>::Find(System.Predicate`1<T>)
#define List_1_Find_m14557(__this, ___match, method) (( BodyPart_t400 * (*) (List_1_t401 *, Predicate_1_t2310 *, const MethodInfo*))List_1_Find_m13544_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m14558(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2310 *, const MethodInfo*))List_1_CheckMatch_m13546_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<RagdollHelper/BodyPart>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m14559(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t401 *, int32_t, int32_t, Predicate_1_t2310 *, const MethodInfo*))List_1_GetIndex_m13548_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<RagdollHelper/BodyPart>::GetEnumerator()
#define List_1_GetEnumerator_m3212(__this, method) (( Enumerator_t537  (*) (List_1_t401 *, const MethodInfo*))List_1_GetEnumerator_m13550_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<RagdollHelper/BodyPart>::IndexOf(T)
#define List_1_IndexOf_m14560(__this, ___item, method) (( int32_t (*) (List_1_t401 *, BodyPart_t400 *, const MethodInfo*))List_1_IndexOf_m13552_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m14561(__this, ___start, ___delta, method) (( void (*) (List_1_t401 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13554_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m14562(__this, ___index, method) (( void (*) (List_1_t401 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13556_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::Insert(System.Int32,T)
#define List_1_Insert_m14563(__this, ___index, ___item, method) (( void (*) (List_1_t401 *, int32_t, BodyPart_t400 *, const MethodInfo*))List_1_Insert_m13558_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m14564(__this, ___collection, method) (( void (*) (List_1_t401 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13560_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<RagdollHelper/BodyPart>::Remove(T)
#define List_1_Remove_m14565(__this, ___item, method) (( bool (*) (List_1_t401 *, BodyPart_t400 *, const MethodInfo*))List_1_Remove_m13562_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<RagdollHelper/BodyPart>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m14566(__this, ___match, method) (( int32_t (*) (List_1_t401 *, Predicate_1_t2310 *, const MethodInfo*))List_1_RemoveAll_m13564_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m14567(__this, ___index, method) (( void (*) (List_1_t401 *, int32_t, const MethodInfo*))List_1_RemoveAt_m13566_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::Reverse()
#define List_1_Reverse_m14568(__this, method) (( void (*) (List_1_t401 *, const MethodInfo*))List_1_Reverse_m13568_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::Sort()
#define List_1_Sort_m14569(__this, method) (( void (*) (List_1_t401 *, const MethodInfo*))List_1_Sort_m13570_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m14570(__this, ___comparison, method) (( void (*) (List_1_t401 *, Comparison_1_t2311 *, const MethodInfo*))List_1_Sort_m13572_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<RagdollHelper/BodyPart>::ToArray()
#define List_1_ToArray_m14571(__this, method) (( BodyPartU5BU5D_t2307* (*) (List_1_t401 *, const MethodInfo*))List_1_ToArray_m13573_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::TrimExcess()
#define List_1_TrimExcess_m14572(__this, method) (( void (*) (List_1_t401 *, const MethodInfo*))List_1_TrimExcess_m13575_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<RagdollHelper/BodyPart>::get_Capacity()
#define List_1_get_Capacity_m14573(__this, method) (( int32_t (*) (List_1_t401 *, const MethodInfo*))List_1_get_Capacity_m13577_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m14574(__this, ___value, method) (( void (*) (List_1_t401 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13579_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<RagdollHelper/BodyPart>::get_Count()
#define List_1_get_Count_m14575(__this, method) (( int32_t (*) (List_1_t401 *, const MethodInfo*))List_1_get_Count_m13581_gshared)(__this, method)
// T System.Collections.Generic.List`1<RagdollHelper/BodyPart>::get_Item(System.Int32)
#define List_1_get_Item_m14576(__this, ___index, method) (( BodyPart_t400 * (*) (List_1_t401 *, int32_t, const MethodInfo*))List_1_get_Item_m13583_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<RagdollHelper/BodyPart>::set_Item(System.Int32,T)
#define List_1_set_Item_m14577(__this, ___index, ___value, method) (( void (*) (List_1_t401 *, int32_t, BodyPart_t400 *, const MethodInfo*))List_1_set_Item_m13585_gshared)(__this, ___index, ___value, method)
