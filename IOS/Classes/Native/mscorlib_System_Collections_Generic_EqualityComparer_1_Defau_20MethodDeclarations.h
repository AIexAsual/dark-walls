﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>
struct DefaultComparer_t2967;
// System.Guid
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::.ctor()
extern "C" void DefaultComparer__ctor_m23037_gshared (DefaultComparer_t2967 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m23037(__this, method) (( void (*) (DefaultComparer_t2967 *, const MethodInfo*))DefaultComparer__ctor_m23037_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m23038_gshared (DefaultComparer_t2967 * __this, Guid_t555  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m23038(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2967 *, Guid_t555 , const MethodInfo*))DefaultComparer_GetHashCode_m23038_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Guid>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m23039_gshared (DefaultComparer_t2967 * __this, Guid_t555  ___x, Guid_t555  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m23039(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2967 *, Guid_t555 , Guid_t555 , const MethodInfo*))DefaultComparer_Equals_m23039_gshared)(__this, ___x, ___y, method)
