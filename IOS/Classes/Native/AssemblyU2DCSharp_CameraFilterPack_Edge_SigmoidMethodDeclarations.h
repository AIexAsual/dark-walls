﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Edge_Sigmoid
struct CameraFilterPack_Edge_Sigmoid_t116;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Edge_Sigmoid::.ctor()
extern "C" void CameraFilterPack_Edge_Sigmoid__ctor_m745 (CameraFilterPack_Edge_Sigmoid_t116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Edge_Sigmoid::get_material()
extern "C" Material_t2 * CameraFilterPack_Edge_Sigmoid_get_material_m746 (CameraFilterPack_Edge_Sigmoid_t116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sigmoid::Start()
extern "C" void CameraFilterPack_Edge_Sigmoid_Start_m747 (CameraFilterPack_Edge_Sigmoid_t116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sigmoid::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Edge_Sigmoid_OnRenderImage_m748 (CameraFilterPack_Edge_Sigmoid_t116 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sigmoid::OnValidate()
extern "C" void CameraFilterPack_Edge_Sigmoid_OnValidate_m749 (CameraFilterPack_Edge_Sigmoid_t116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sigmoid::Update()
extern "C" void CameraFilterPack_Edge_Sigmoid_Update_m750 (CameraFilterPack_Edge_Sigmoid_t116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sigmoid::OnDisable()
extern "C" void CameraFilterPack_Edge_Sigmoid_OnDisable_m751 (CameraFilterPack_Edge_Sigmoid_t116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
