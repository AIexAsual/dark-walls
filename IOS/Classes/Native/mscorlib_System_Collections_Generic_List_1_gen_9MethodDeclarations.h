﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<iTween/LoopType>
struct List_1_t581;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<iTween/LoopType>
struct IEnumerable_1_t3042;
// System.Collections.Generic.IEnumerator`1<iTween/LoopType>
struct IEnumerator_1_t3043;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<iTween/LoopType>
struct ICollection_1_t3044;
// System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>
struct ReadOnlyCollection_1_t2459;
// iTween/LoopType[]
struct LoopTypeU5BU5D_t452;
// System.Predicate`1<iTween/LoopType>
struct Predicate_1_t2463;
// System.Comparison`1<iTween/LoopType>
struct Comparison_1_t2466;
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"
// System.Collections.Generic.List`1/Enumerator<iTween/LoopType>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_17.h"

// System.Void System.Collections.Generic.List`1<iTween/LoopType>::.ctor()
extern "C" void List_1__ctor_m3396_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1__ctor_m3396(__this, method) (( void (*) (List_1_t581 *, const MethodInfo*))List_1__ctor_m3396_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m16500_gshared (List_1_t581 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m16500(__this, ___collection, method) (( void (*) (List_1_t581 *, Object_t*, const MethodInfo*))List_1__ctor_m16500_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::.ctor(System.Int32)
extern "C" void List_1__ctor_m16501_gshared (List_1_t581 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m16501(__this, ___capacity, method) (( void (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1__ctor_m16501_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::.cctor()
extern "C" void List_1__cctor_m16502_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m16502(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m16502_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<iTween/LoopType>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16503_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16503(__this, method) (( Object_t* (*) (List_1_t581 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16503_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m16504_gshared (List_1_t581 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m16504(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t581 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m16504_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<iTween/LoopType>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m16505_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16505(__this, method) (( Object_t * (*) (List_1_t581 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m16505_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<iTween/LoopType>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m16506_gshared (List_1_t581 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m16506(__this, ___item, method) (( int32_t (*) (List_1_t581 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m16506_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<iTween/LoopType>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m16507_gshared (List_1_t581 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m16507(__this, ___item, method) (( bool (*) (List_1_t581 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m16507_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<iTween/LoopType>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m16508_gshared (List_1_t581 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m16508(__this, ___item, method) (( int32_t (*) (List_1_t581 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m16508_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m16509_gshared (List_1_t581 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m16509(__this, ___index, ___item, method) (( void (*) (List_1_t581 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m16509_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m16510_gshared (List_1_t581 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m16510(__this, ___item, method) (( void (*) (List_1_t581 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m16510_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<iTween/LoopType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16511_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16511(__this, method) (( bool (*) (List_1_t581 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16511_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<iTween/LoopType>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m16512_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16512(__this, method) (( bool (*) (List_1_t581 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m16512_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<iTween/LoopType>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m16513_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m16513(__this, method) (( Object_t * (*) (List_1_t581 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m16513_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<iTween/LoopType>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m16514_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m16514(__this, method) (( bool (*) (List_1_t581 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m16514_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<iTween/LoopType>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m16515_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m16515(__this, method) (( bool (*) (List_1_t581 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m16515_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<iTween/LoopType>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m16516_gshared (List_1_t581 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m16516(__this, ___index, method) (( Object_t * (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m16516_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m16517_gshared (List_1_t581 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m16517(__this, ___index, ___value, method) (( void (*) (List_1_t581 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m16517_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::Add(T)
extern "C" void List_1_Add_m16518_gshared (List_1_t581 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Add_m16518(__this, ___item, method) (( void (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1_Add_m16518_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m16519_gshared (List_1_t581 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m16519(__this, ___newCount, method) (( void (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m16519_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m16520_gshared (List_1_t581 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m16520(__this, ___collection, method) (( void (*) (List_1_t581 *, Object_t*, const MethodInfo*))List_1_AddCollection_m16520_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m16521_gshared (List_1_t581 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m16521(__this, ___enumerable, method) (( void (*) (List_1_t581 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m16521_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m16522_gshared (List_1_t581 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m16522(__this, ___collection, method) (( void (*) (List_1_t581 *, Object_t*, const MethodInfo*))List_1_AddRange_m16522_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<iTween/LoopType>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2459 * List_1_AsReadOnly_m16523_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m16523(__this, method) (( ReadOnlyCollection_1_t2459 * (*) (List_1_t581 *, const MethodInfo*))List_1_AsReadOnly_m16523_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::Clear()
extern "C" void List_1_Clear_m16524_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1_Clear_m16524(__this, method) (( void (*) (List_1_t581 *, const MethodInfo*))List_1_Clear_m16524_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<iTween/LoopType>::Contains(T)
extern "C" bool List_1_Contains_m16525_gshared (List_1_t581 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Contains_m16525(__this, ___item, method) (( bool (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1_Contains_m16525_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m16526_gshared (List_1_t581 * __this, LoopTypeU5BU5D_t452* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m16526(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t581 *, LoopTypeU5BU5D_t452*, int32_t, const MethodInfo*))List_1_CopyTo_m16526_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<iTween/LoopType>::Find(System.Predicate`1<T>)
extern "C" int32_t List_1_Find_m16527_gshared (List_1_t581 * __this, Predicate_1_t2463 * ___match, const MethodInfo* method);
#define List_1_Find_m16527(__this, ___match, method) (( int32_t (*) (List_1_t581 *, Predicate_1_t2463 *, const MethodInfo*))List_1_Find_m16527_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m16528_gshared (Object_t * __this /* static, unused */, Predicate_1_t2463 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m16528(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2463 *, const MethodInfo*))List_1_CheckMatch_m16528_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<iTween/LoopType>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m16529_gshared (List_1_t581 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2463 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m16529(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t581 *, int32_t, int32_t, Predicate_1_t2463 *, const MethodInfo*))List_1_GetIndex_m16529_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<iTween/LoopType>::GetEnumerator()
extern "C" Enumerator_t2458  List_1_GetEnumerator_m16530_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m16530(__this, method) (( Enumerator_t2458  (*) (List_1_t581 *, const MethodInfo*))List_1_GetEnumerator_m16530_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<iTween/LoopType>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m16531_gshared (List_1_t581 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m16531(__this, ___item, method) (( int32_t (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1_IndexOf_m16531_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m16532_gshared (List_1_t581 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m16532(__this, ___start, ___delta, method) (( void (*) (List_1_t581 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m16532_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m16533_gshared (List_1_t581 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m16533(__this, ___index, method) (( void (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1_CheckIndex_m16533_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m16534_gshared (List_1_t581 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define List_1_Insert_m16534(__this, ___index, ___item, method) (( void (*) (List_1_t581 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m16534_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m16535_gshared (List_1_t581 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m16535(__this, ___collection, method) (( void (*) (List_1_t581 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m16535_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<iTween/LoopType>::Remove(T)
extern "C" bool List_1_Remove_m16536_gshared (List_1_t581 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Remove_m16536(__this, ___item, method) (( bool (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1_Remove_m16536_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<iTween/LoopType>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m16537_gshared (List_1_t581 * __this, Predicate_1_t2463 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m16537(__this, ___match, method) (( int32_t (*) (List_1_t581 *, Predicate_1_t2463 *, const MethodInfo*))List_1_RemoveAll_m16537_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m16538_gshared (List_1_t581 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m16538(__this, ___index, method) (( void (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1_RemoveAt_m16538_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::Reverse()
extern "C" void List_1_Reverse_m16539_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1_Reverse_m16539(__this, method) (( void (*) (List_1_t581 *, const MethodInfo*))List_1_Reverse_m16539_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::Sort()
extern "C" void List_1_Sort_m16540_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1_Sort_m16540(__this, method) (( void (*) (List_1_t581 *, const MethodInfo*))List_1_Sort_m16540_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m16541_gshared (List_1_t581 * __this, Comparison_1_t2466 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m16541(__this, ___comparison, method) (( void (*) (List_1_t581 *, Comparison_1_t2466 *, const MethodInfo*))List_1_Sort_m16541_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<iTween/LoopType>::ToArray()
extern "C" LoopTypeU5BU5D_t452* List_1_ToArray_m3409_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1_ToArray_m3409(__this, method) (( LoopTypeU5BU5D_t452* (*) (List_1_t581 *, const MethodInfo*))List_1_ToArray_m3409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::TrimExcess()
extern "C" void List_1_TrimExcess_m16542_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m16542(__this, method) (( void (*) (List_1_t581 *, const MethodInfo*))List_1_TrimExcess_m16542_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<iTween/LoopType>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m16543_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m16543(__this, method) (( int32_t (*) (List_1_t581 *, const MethodInfo*))List_1_get_Capacity_m16543_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m16544_gshared (List_1_t581 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m16544(__this, ___value, method) (( void (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1_set_Capacity_m16544_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<iTween/LoopType>::get_Count()
extern "C" int32_t List_1_get_Count_m16545_gshared (List_1_t581 * __this, const MethodInfo* method);
#define List_1_get_Count_m16545(__this, method) (( int32_t (*) (List_1_t581 *, const MethodInfo*))List_1_get_Count_m16545_gshared)(__this, method)
// T System.Collections.Generic.List`1<iTween/LoopType>::get_Item(System.Int32)
extern "C" int32_t List_1_get_Item_m16546_gshared (List_1_t581 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m16546(__this, ___index, method) (( int32_t (*) (List_1_t581 *, int32_t, const MethodInfo*))List_1_get_Item_m16546_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<iTween/LoopType>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m16547_gshared (List_1_t581 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define List_1_set_Item_m16547(__this, ___index, ___value, method) (( void (*) (List_1_t581 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m16547_gshared)(__this, ___index, ___value, method)
