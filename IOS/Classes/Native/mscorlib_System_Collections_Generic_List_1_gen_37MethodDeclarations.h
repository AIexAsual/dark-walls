﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct List_1_t973;
// System.Object
struct Object_t;
// UnityEngine.Networking.Match.MatchDirectConnectInfo
struct MatchDirectConnectInfo_t972;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct IEnumerable_1_t3192;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct IEnumerator_1_t3193;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct ICollection_1_t3194;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct ReadOnlyCollection_1_t2727;
// UnityEngine.Networking.Match.MatchDirectConnectInfo[]
struct MatchDirectConnectInfoU5BU5D_t2725;
// System.Predicate`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct Predicate_1_t2728;
// System.Comparison`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct Comparison_1_t2730;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDirectConnectInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_39.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_30MethodDeclarations.h"
#define List_1__ctor_m20577(__this, method) (( void (*) (List_1_t973 *, const MethodInfo*))List_1__ctor_m6426_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m20578(__this, ___collection, method) (( void (*) (List_1_t973 *, Object_t*, const MethodInfo*))List_1__ctor_m13490_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::.ctor(System.Int32)
#define List_1__ctor_m20579(__this, ___capacity, method) (( void (*) (List_1_t973 *, int32_t, const MethodInfo*))List_1__ctor_m13492_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::.cctor()
#define List_1__cctor_m20580(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13494_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20581(__this, method) (( Object_t* (*) (List_1_t973 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13496_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m20582(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t973 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m13498_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20583(__this, method) (( Object_t * (*) (List_1_t973 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m13500_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m20584(__this, ___item, method) (( int32_t (*) (List_1_t973 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m13502_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m20585(__this, ___item, method) (( bool (*) (List_1_t973 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m13504_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m20586(__this, ___item, method) (( int32_t (*) (List_1_t973 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m13506_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m20587(__this, ___index, ___item, method) (( void (*) (List_1_t973 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m13508_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m20588(__this, ___item, method) (( void (*) (List_1_t973 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m13510_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20589(__this, method) (( bool (*) (List_1_t973 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13512_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20590(__this, method) (( bool (*) (List_1_t973 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m13514_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m20591(__this, method) (( Object_t * (*) (List_1_t973 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m13516_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m20592(__this, method) (( bool (*) (List_1_t973 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m13518_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m20593(__this, method) (( bool (*) (List_1_t973 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m13520_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m20594(__this, ___index, method) (( Object_t * (*) (List_1_t973 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m13522_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m20595(__this, ___index, ___value, method) (( void (*) (List_1_t973 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m13524_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Add(T)
#define List_1_Add_m20596(__this, ___item, method) (( void (*) (List_1_t973 *, MatchDirectConnectInfo_t972 *, const MethodInfo*))List_1_Add_m13526_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m20597(__this, ___newCount, method) (( void (*) (List_1_t973 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13528_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m20598(__this, ___collection, method) (( void (*) (List_1_t973 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13530_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m20599(__this, ___enumerable, method) (( void (*) (List_1_t973 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13532_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m20600(__this, ___collection, method) (( void (*) (List_1_t973 *, Object_t*, const MethodInfo*))List_1_AddRange_m13534_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::AsReadOnly()
#define List_1_AsReadOnly_m20601(__this, method) (( ReadOnlyCollection_1_t2727 * (*) (List_1_t973 *, const MethodInfo*))List_1_AsReadOnly_m13536_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Clear()
#define List_1_Clear_m20602(__this, method) (( void (*) (List_1_t973 *, const MethodInfo*))List_1_Clear_m13538_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Contains(T)
#define List_1_Contains_m20603(__this, ___item, method) (( bool (*) (List_1_t973 *, MatchDirectConnectInfo_t972 *, const MethodInfo*))List_1_Contains_m13540_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m20604(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t973 *, MatchDirectConnectInfoU5BU5D_t2725*, int32_t, const MethodInfo*))List_1_CopyTo_m13542_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Find(System.Predicate`1<T>)
#define List_1_Find_m20605(__this, ___match, method) (( MatchDirectConnectInfo_t972 * (*) (List_1_t973 *, Predicate_1_t2728 *, const MethodInfo*))List_1_Find_m13544_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m20606(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2728 *, const MethodInfo*))List_1_CheckMatch_m13546_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m20607(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t973 *, int32_t, int32_t, Predicate_1_t2728 *, const MethodInfo*))List_1_GetIndex_m13548_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::GetEnumerator()
#define List_1_GetEnumerator_m20608(__this, method) (( Enumerator_t2729  (*) (List_1_t973 *, const MethodInfo*))List_1_GetEnumerator_m13550_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::IndexOf(T)
#define List_1_IndexOf_m20609(__this, ___item, method) (( int32_t (*) (List_1_t973 *, MatchDirectConnectInfo_t972 *, const MethodInfo*))List_1_IndexOf_m13552_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m20610(__this, ___start, ___delta, method) (( void (*) (List_1_t973 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13554_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m20611(__this, ___index, method) (( void (*) (List_1_t973 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13556_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Insert(System.Int32,T)
#define List_1_Insert_m20612(__this, ___index, ___item, method) (( void (*) (List_1_t973 *, int32_t, MatchDirectConnectInfo_t972 *, const MethodInfo*))List_1_Insert_m13558_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m20613(__this, ___collection, method) (( void (*) (List_1_t973 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13560_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Remove(T)
#define List_1_Remove_m20614(__this, ___item, method) (( bool (*) (List_1_t973 *, MatchDirectConnectInfo_t972 *, const MethodInfo*))List_1_Remove_m13562_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m20615(__this, ___match, method) (( int32_t (*) (List_1_t973 *, Predicate_1_t2728 *, const MethodInfo*))List_1_RemoveAll_m13564_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20616(__this, ___index, method) (( void (*) (List_1_t973 *, int32_t, const MethodInfo*))List_1_RemoveAt_m13566_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Reverse()
#define List_1_Reverse_m20617(__this, method) (( void (*) (List_1_t973 *, const MethodInfo*))List_1_Reverse_m13568_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Sort()
#define List_1_Sort_m20618(__this, method) (( void (*) (List_1_t973 *, const MethodInfo*))List_1_Sort_m13570_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20619(__this, ___comparison, method) (( void (*) (List_1_t973 *, Comparison_1_t2730 *, const MethodInfo*))List_1_Sort_m13572_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::ToArray()
#define List_1_ToArray_m20620(__this, method) (( MatchDirectConnectInfoU5BU5D_t2725* (*) (List_1_t973 *, const MethodInfo*))List_1_ToArray_m13573_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::TrimExcess()
#define List_1_TrimExcess_m20621(__this, method) (( void (*) (List_1_t973 *, const MethodInfo*))List_1_TrimExcess_m13575_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::get_Capacity()
#define List_1_get_Capacity_m20622(__this, method) (( int32_t (*) (List_1_t973 *, const MethodInfo*))List_1_get_Capacity_m13577_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m20623(__this, ___value, method) (( void (*) (List_1_t973 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13579_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::get_Count()
#define List_1_get_Count_m20624(__this, method) (( int32_t (*) (List_1_t973 *, const MethodInfo*))List_1_get_Count_m13581_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::get_Item(System.Int32)
#define List_1_get_Item_m20625(__this, ___index, method) (( MatchDirectConnectInfo_t972 * (*) (List_1_t973 *, int32_t, const MethodInfo*))List_1_get_Item_m13583_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::set_Item(System.Int32,T)
#define List_1_set_Item_m20626(__this, ___index, ___value, method) (( void (*) (List_1_t973 *, int32_t, MatchDirectConnectInfo_t972 *, const MethodInfo*))List_1_set_Item_m13585_gshared)(__this, ___index, ___value, method)
