﻿#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "codegen/il2cpp-codegen.h"

// System.Array
#include "mscorlib_System_Array.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Reflection.Emit.ILGenerator/LabelData
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T&)
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.String
#include "mscorlib_System_String.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelData>(T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelData>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisLabelData_t1744_m23986_gshared (Array_t * __this, LabelData_t1744  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisLabelData_t1744_m23986(__this, ___item, method) (( void (*) (Array_t *, LabelData_t1744 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisLabelData_t1744_m23986_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" void Array_InternalArray__ICollection_Add_TisLabelData_t1744_m23986_gshared (Array_t * __this, LabelData_t1744  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.RankException
#include "mscorlib_System_RankException.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
// Locale
#include "mscorlib_LocaleMethodDeclarations.h"
// System.RankException
#include "mscorlib_System_RankExceptionMethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelData>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelData>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisLabelData_t1744_m23987_gshared (Array_t * __this, LabelData_t1744  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisLabelData_t1744_m23987(__this, ___item, method) (( bool (*) (Array_t *, LabelData_t1744 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisLabelData_t1744_m23987_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1933;
extern "C" bool Array_InternalArray__ICollection_Contains_TisLabelData_t1744_m23987_gshared (Array_t * __this, LabelData_t1744  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	LabelData_t1744  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_2 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (LabelData_t1744 *)(&V_2));
		LabelData_t1744  L_5 = ___item;
		goto IL_004d;
	}
	{
		LabelData_t1744  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		LabelData_t1744  L_7 = V_2;
		LabelData_t1744  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
#ifndef _MSC_VER
#else
#endif

#include "mscorlib_ArrayTypes.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelData>(T[],System.Int32)
struct Array_t;
struct LabelDataU5BU5D_t1746;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelData>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelData>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLabelData_t1744_m23988_gshared (Array_t * __this, LabelDataU5BU5D_t1746* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisLabelData_t1744_m23988(__this, ___array, ___index, method) (( void (*) (Array_t *, LabelDataU5BU5D_t1746*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisLabelData_t1744_m23988_gshared)(__this, ___array, ___index, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelData>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t1128_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t556_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1052;
extern Il2CppCodeGenString* _stringLiteral1933;
extern Il2CppCodeGenString* _stringLiteral1970;
extern Il2CppCodeGenString* _stringLiteral1484;
extern Il2CppCodeGenString* _stringLiteral1956;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLabelData_t1744_m23988_gshared (Array_t * __this, LabelDataU5BU5D_t1746* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(706);
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		ArgumentException_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		_stringLiteral1052 = il2cpp_codegen_string_literal_from_index(1052);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		_stringLiteral1970 = il2cpp_codegen_string_literal_from_index(1970);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		_stringLiteral1956 = il2cpp_codegen_string_literal_from_index(1956);
		s_Il2CppMethodIntialized = true;
	}
	{
		LabelDataU5BU5D_t1746* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1128 * L_1 = (ArgumentNullException_t1128 *)il2cpp_codegen_object_new (ArgumentNullException_t1128_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m6429(L_1, (String_t*)_stringLiteral1052, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_4 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m2938((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		LabelDataU5BU5D_t1746* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m9182((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		LabelDataU5BU5D_t1746* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m2938((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t556 * L_11 = (ArgumentException_t556 *)il2cpp_codegen_object_new (ArgumentException_t556_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3362(L_11, (String_t*)_stringLiteral1970, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_0055:
	{
		LabelDataU5BU5D_t1746* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m8449((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_15 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1956, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1130 * L_18 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6436(L_18, (String_t*)_stringLiteral1484, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		LabelDataU5BU5D_t1746* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m2938((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m3325(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelData>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelData>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisLabelData_t1744_m23989_gshared (Array_t * __this, LabelData_t1744  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisLabelData_t1744_m23989(__this, ___item, method) (( bool (*) (Array_t *, LabelData_t1744 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisLabelData_t1744_m23989_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" bool Array_InternalArray__ICollection_Remove_TisLabelData_t1744_m23989_gshared (Array_t * __this, LabelData_t1744  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelData>(T)
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelData>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisLabelData_t1744_m23990_gshared (Array_t * __this, LabelData_t1744  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisLabelData_t1744_m23990(__this, ___item, method) (( int32_t (*) (Array_t *, LabelData_t1744 , const MethodInfo*))Array_InternalArray__IndexOf_TisLabelData_t1744_m23990_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelData>(T)
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1933;
extern "C" int32_t Array_InternalArray__IndexOf_TisLabelData_t1744_m23990_gshared (Array_t * __this, LabelData_t1744  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	LabelData_t1744  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_2 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (LabelData_t1744 *)(&V_2));
		LabelData_t1744  L_5 = ___item;
		goto IL_005d;
	}
	{
		LabelData_t1744  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		LabelData_t1744  L_10 = ___item;
		LabelData_t1744  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisLabelData_t1744_m23991_gshared (Array_t * __this, int32_t ___index, LabelData_t1744  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisLabelData_t1744_m23991(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, LabelData_t1744 , const MethodInfo*))Array_InternalArray__Insert_TisLabelData_t1744_m23991_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" void Array_InternalArray__Insert_TisLabelData_t1744_m23991_gshared (Array_t * __this, int32_t ___index, LabelData_t1744  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisLabelData_t1744_m23993_gshared (Array_t * __this, int32_t ___index, LabelData_t1744  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisLabelData_t1744_m23993(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, LabelData_t1744 , const MethodInfo*))Array_InternalArray__set_Item_TisLabelData_t1744_m23993_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1484;
extern "C" void Array_InternalArray__set_Item_TisLabelData_t1744_m23993_gshared (Array_t * __this, int32_t ___index, LabelData_t1744  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t470* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1130 * L_2 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7443(L_2, (String_t*)_stringLiteral1484, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t470*)((ObjectU5BU5D_t470*)IsInst(__this, ObjectU5BU5D_t470_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t470* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t470* L_4 = V_0;
		int32_t L_5 = ___index;
		LabelData_t1744  L_6 = ___item;
		LabelData_t1744  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5)) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (LabelData_t1744 *)(&___item));
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_102.h"
// System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_102MethodDeclarations.h"
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelData>()
struct Array_t;
struct IEnumerator_1_t3384;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelData>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelData>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisLabelData_t1744_m23994_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisLabelData_t1744_m23994(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisLabelData_t1744_m23994_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelData>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisLabelData_t1744_m23994_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t2933  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t2933 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t2933  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Reflection.Emit.ILGenerator/LabelFixup
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern "C" LabelFixup_t1743  Array_InternalArray__get_Item_TisLabelFixup_t1743_m23996_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelFixup_t1743_m23996(__this, ___index, method) (( LabelFixup_t1743  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelFixup_t1743_m23996_gshared)(__this, ___index, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1484;
extern "C" LabelFixup_t1743  Array_InternalArray__get_Item_TisLabelFixup_t1743_m23996_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		s_Il2CppMethodIntialized = true;
	}
	LabelFixup_t1743  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1130 * L_2 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7443(L_2, (String_t*)_stringLiteral1484, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (LabelFixup_t1743 *)(&V_0));
		LabelFixup_t1743  L_4 = V_0;
		return L_4;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisLabelFixup_t1743_m23997_gshared (Array_t * __this, LabelFixup_t1743  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisLabelFixup_t1743_m23997(__this, ___item, method) (( void (*) (Array_t *, LabelFixup_t1743 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisLabelFixup_t1743_m23997_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" void Array_InternalArray__ICollection_Add_TisLabelFixup_t1743_m23997_gshared (Array_t * __this, LabelFixup_t1743  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisLabelFixup_t1743_m23998_gshared (Array_t * __this, LabelFixup_t1743  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisLabelFixup_t1743_m23998(__this, ___item, method) (( bool (*) (Array_t *, LabelFixup_t1743 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisLabelFixup_t1743_m23998_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1933;
extern "C" bool Array_InternalArray__ICollection_Contains_TisLabelFixup_t1743_m23998_gshared (Array_t * __this, LabelFixup_t1743  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	LabelFixup_t1743  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_2 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (LabelFixup_t1743 *)(&V_2));
		LabelFixup_t1743  L_5 = ___item;
		goto IL_004d;
	}
	{
		LabelFixup_t1743  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		LabelFixup_t1743  L_7 = V_2;
		LabelFixup_t1743  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelFixup>(T[],System.Int32)
struct Array_t;
struct LabelFixupU5BU5D_t1747;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelFixup>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelFixup>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLabelFixup_t1743_m23999_gshared (Array_t * __this, LabelFixupU5BU5D_t1747* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisLabelFixup_t1743_m23999(__this, ___array, ___index, method) (( void (*) (Array_t *, LabelFixupU5BU5D_t1747*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisLabelFixup_t1743_m23999_gshared)(__this, ___array, ___index, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Reflection.Emit.ILGenerator/LabelFixup>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t1128_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t556_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1052;
extern Il2CppCodeGenString* _stringLiteral1933;
extern Il2CppCodeGenString* _stringLiteral1970;
extern Il2CppCodeGenString* _stringLiteral1484;
extern Il2CppCodeGenString* _stringLiteral1956;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisLabelFixup_t1743_m23999_gshared (Array_t * __this, LabelFixupU5BU5D_t1747* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(706);
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		ArgumentException_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		_stringLiteral1052 = il2cpp_codegen_string_literal_from_index(1052);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		_stringLiteral1970 = il2cpp_codegen_string_literal_from_index(1970);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		_stringLiteral1956 = il2cpp_codegen_string_literal_from_index(1956);
		s_Il2CppMethodIntialized = true;
	}
	{
		LabelFixupU5BU5D_t1747* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1128 * L_1 = (ArgumentNullException_t1128 *)il2cpp_codegen_object_new (ArgumentNullException_t1128_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m6429(L_1, (String_t*)_stringLiteral1052, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_4 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m2938((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		LabelFixupU5BU5D_t1747* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m9182((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		LabelFixupU5BU5D_t1747* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m2938((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t556 * L_11 = (ArgumentException_t556 *)il2cpp_codegen_object_new (ArgumentException_t556_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3362(L_11, (String_t*)_stringLiteral1970, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_0055:
	{
		LabelFixupU5BU5D_t1747* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m8449((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_15 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1956, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1130 * L_18 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6436(L_18, (String_t*)_stringLiteral1484, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		LabelFixupU5BU5D_t1747* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m2938((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m3325(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisLabelFixup_t1743_m24000_gshared (Array_t * __this, LabelFixup_t1743  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisLabelFixup_t1743_m24000(__this, ___item, method) (( bool (*) (Array_t *, LabelFixup_t1743 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisLabelFixup_t1743_m24000_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" bool Array_InternalArray__ICollection_Remove_TisLabelFixup_t1743_m24000_gshared (Array_t * __this, LabelFixup_t1743  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisLabelFixup_t1743_m24001_gshared (Array_t * __this, LabelFixup_t1743  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisLabelFixup_t1743_m24001(__this, ___item, method) (( int32_t (*) (Array_t *, LabelFixup_t1743 , const MethodInfo*))Array_InternalArray__IndexOf_TisLabelFixup_t1743_m24001_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Reflection.Emit.ILGenerator/LabelFixup>(T)
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1933;
extern "C" int32_t Array_InternalArray__IndexOf_TisLabelFixup_t1743_m24001_gshared (Array_t * __this, LabelFixup_t1743  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	LabelFixup_t1743  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_2 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (LabelFixup_t1743 *)(&V_2));
		LabelFixup_t1743  L_5 = ___item;
		goto IL_005d;
	}
	{
		LabelFixup_t1743  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		LabelFixup_t1743  L_10 = ___item;
		LabelFixup_t1743  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisLabelFixup_t1743_m24002_gshared (Array_t * __this, int32_t ___index, LabelFixup_t1743  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisLabelFixup_t1743_m24002(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, LabelFixup_t1743 , const MethodInfo*))Array_InternalArray__Insert_TisLabelFixup_t1743_m24002_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" void Array_InternalArray__Insert_TisLabelFixup_t1743_m24002_gshared (Array_t * __this, int32_t ___index, LabelFixup_t1743  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisLabelFixup_t1743_m24004_gshared (Array_t * __this, int32_t ___index, LabelFixup_t1743  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisLabelFixup_t1743_m24004(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, LabelFixup_t1743 , const MethodInfo*))Array_InternalArray__set_Item_TisLabelFixup_t1743_m24004_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1484;
extern "C" void Array_InternalArray__set_Item_TisLabelFixup_t1743_m24004_gshared (Array_t * __this, int32_t ___index, LabelFixup_t1743  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t470* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1130 * L_2 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7443(L_2, (String_t*)_stringLiteral1484, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t470*)((ObjectU5BU5D_t470*)IsInst(__this, ObjectU5BU5D_t470_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t470* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t470* L_4 = V_0;
		int32_t L_5 = ___index;
		LabelFixup_t1743  L_6 = ___item;
		LabelFixup_t1743  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5)) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (LabelFixup_t1743 *)(&___item));
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_103.h"
// System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_103MethodDeclarations.h"
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelFixup>()
struct Array_t;
struct IEnumerator_1_t3385;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelFixup>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelFixup>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisLabelFixup_t1743_m24005_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisLabelFixup_t1743_m24005(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisLabelFixup_t1743_m24005_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Reflection.Emit.ILGenerator/LabelFixup>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisLabelFixup_t1743_m24005_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t2934  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t2934 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t2934  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Reflection.MonoProperty
#include "mscorlib_System_Reflection_MonoProperty.h"
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_gen.h"
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_genMethodDeclarations.h"
// System.Object System.Reflection.MonoProperty::GetterAdapterFrame<System.Object,System.Object>(System.Reflection.MonoProperty/Getter`2<T,R>,System.Object)
struct MonoProperty_t;
struct Object_t;
struct Getter_2_t2941;
// Declaration System.Object System.Reflection.MonoProperty::GetterAdapterFrame<System.Object,System.Object>(System.Reflection.MonoProperty/Getter`2<T,R>,System.Object)
// System.Object System.Reflection.MonoProperty::GetterAdapterFrame<System.Object,System.Object>(System.Reflection.MonoProperty/Getter`2<T,R>,System.Object)
extern "C" Object_t * MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m24006_gshared (Object_t * __this /* static, unused */, Getter_2_t2941 * ___getter, Object_t * ___obj, const MethodInfo* method);
#define MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m24006(__this /* static, unused */, ___getter, ___obj, method) (( Object_t * (*) (Object_t * /* static, unused */, Getter_2_t2941 *, Object_t *, const MethodInfo*))MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m24006_gshared)(__this /* static, unused */, ___getter, ___obj, method)
// System.Object System.Reflection.MonoProperty::GetterAdapterFrame<System.Object,System.Object>(System.Reflection.MonoProperty/Getter`2<T,R>,System.Object)
extern "C" Object_t * MonoProperty_GetterAdapterFrame_TisObject_t_TisObject_t_m24006_gshared (Object_t * __this /* static, unused */, Getter_2_t2941 * ___getter, Object_t * ___obj, const MethodInfo* method)
{
	{
		Getter_2_t2941 * L_0 = ___getter;
		Object_t * L_1 = ___obj;
		NullCheck((Getter_2_t2941 *)L_0);
		Object_t * L_2 = (( Object_t * (*) (Getter_2_t2941 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)((Getter_2_t2941 *)L_0, (Object_t *)((Object_t *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
#include "mscorlib_System_Reflection_MonoProperty_StaticGetter_1_gen.h"
// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
#include "mscorlib_System_Reflection_MonoProperty_StaticGetter_1_genMethodDeclarations.h"
// System.Object System.Reflection.MonoProperty::StaticGetterAdapterFrame<System.Object>(System.Reflection.MonoProperty/StaticGetter`1<R>,System.Object)
struct MonoProperty_t;
struct Object_t;
struct StaticGetter_1_t2942;
// Declaration System.Object System.Reflection.MonoProperty::StaticGetterAdapterFrame<System.Object>(System.Reflection.MonoProperty/StaticGetter`1<R>,System.Object)
// System.Object System.Reflection.MonoProperty::StaticGetterAdapterFrame<System.Object>(System.Reflection.MonoProperty/StaticGetter`1<R>,System.Object)
extern "C" Object_t * MonoProperty_StaticGetterAdapterFrame_TisObject_t_m24007_gshared (Object_t * __this /* static, unused */, StaticGetter_1_t2942 * ___getter, Object_t * ___obj, const MethodInfo* method);
#define MonoProperty_StaticGetterAdapterFrame_TisObject_t_m24007(__this /* static, unused */, ___getter, ___obj, method) (( Object_t * (*) (Object_t * /* static, unused */, StaticGetter_1_t2942 *, Object_t *, const MethodInfo*))MonoProperty_StaticGetterAdapterFrame_TisObject_t_m24007_gshared)(__this /* static, unused */, ___getter, ___obj, method)
// System.Object System.Reflection.MonoProperty::StaticGetterAdapterFrame<System.Object>(System.Reflection.MonoProperty/StaticGetter`1<R>,System.Object)
extern "C" Object_t * MonoProperty_StaticGetterAdapterFrame_TisObject_t_m24007_gshared (Object_t * __this /* static, unused */, StaticGetter_1_t2942 * ___getter, Object_t * ___obj, const MethodInfo* method)
{
	{
		StaticGetter_1_t2942 * L_0 = ___getter;
		NullCheck((StaticGetter_1_t2942 *)L_0);
		Object_t * L_1 = (( Object_t * (*) (StaticGetter_1_t2942 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((StaticGetter_1_t2942 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.DateTime
#include "mscorlib_System_DateTime.h"
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.DateTime>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern "C" DateTime_t871  Array_InternalArray__get_Item_TisDateTime_t871_m24009_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDateTime_t871_m24009(__this, ___index, method) (( DateTime_t871  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDateTime_t871_m24009_gshared)(__this, ___index, method)
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1484;
extern "C" DateTime_t871  Array_InternalArray__get_Item_TisDateTime_t871_m24009_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t871  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1130 * L_2 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7443(L_2, (String_t*)_stringLiteral1484, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (DateTime_t871 *)(&V_0));
		DateTime_t871  L_4 = V_0;
		return L_4;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.DateTime>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.DateTime>(T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.DateTime>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.DateTime>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisDateTime_t871_m24010_gshared (Array_t * __this, DateTime_t871  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisDateTime_t871_m24010(__this, ___item, method) (( void (*) (Array_t *, DateTime_t871 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisDateTime_t871_m24010_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.DateTime>(T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" void Array_InternalArray__ICollection_Add_TisDateTime_t871_m24010_gshared (Array_t * __this, DateTime_t871  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTime>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTime>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTime>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisDateTime_t871_m24011_gshared (Array_t * __this, DateTime_t871  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisDateTime_t871_m24011(__this, ___item, method) (( bool (*) (Array_t *, DateTime_t871 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisDateTime_t871_m24011_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTime>(T)
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1933;
extern "C" bool Array_InternalArray__ICollection_Contains_TisDateTime_t871_m24011_gshared (Array_t * __this, DateTime_t871  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	DateTime_t871  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_2 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (DateTime_t871 *)(&V_2));
		DateTime_t871  L_5 = ___item;
		goto IL_004d;
	}
	{
		DateTime_t871  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		DateTime_t871  L_7 = V_2;
		DateTime_t871  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((DateTime_t871 *)(&___item));
		bool L_10 = DateTime_Equals_m12930((DateTime_t871 *)(&___item), (Object_t *)L_9, NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTime>(T[],System.Int32)
struct Array_t;
struct DateTimeU5BU5D_t2206;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTime>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTime>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDateTime_t871_m24012_gshared (Array_t * __this, DateTimeU5BU5D_t2206* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisDateTime_t871_m24012(__this, ___array, ___index, method) (( void (*) (Array_t *, DateTimeU5BU5D_t2206*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisDateTime_t871_m24012_gshared)(__this, ___array, ___index, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTime>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t1128_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t556_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1052;
extern Il2CppCodeGenString* _stringLiteral1933;
extern Il2CppCodeGenString* _stringLiteral1970;
extern Il2CppCodeGenString* _stringLiteral1484;
extern Il2CppCodeGenString* _stringLiteral1956;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDateTime_t871_m24012_gshared (Array_t * __this, DateTimeU5BU5D_t2206* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(706);
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		ArgumentException_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		_stringLiteral1052 = il2cpp_codegen_string_literal_from_index(1052);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		_stringLiteral1970 = il2cpp_codegen_string_literal_from_index(1970);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		_stringLiteral1956 = il2cpp_codegen_string_literal_from_index(1956);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTimeU5BU5D_t2206* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1128 * L_1 = (ArgumentNullException_t1128 *)il2cpp_codegen_object_new (ArgumentNullException_t1128_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m6429(L_1, (String_t*)_stringLiteral1052, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_4 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m2938((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		DateTimeU5BU5D_t2206* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m9182((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		DateTimeU5BU5D_t2206* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m2938((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t556 * L_11 = (ArgumentException_t556 *)il2cpp_codegen_object_new (ArgumentException_t556_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3362(L_11, (String_t*)_stringLiteral1970, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_0055:
	{
		DateTimeU5BU5D_t2206* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m8449((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_15 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1956, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1130 * L_18 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6436(L_18, (String_t*)_stringLiteral1484, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		DateTimeU5BU5D_t2206* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m2938((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m3325(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTime>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTime>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTime>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisDateTime_t871_m24013_gshared (Array_t * __this, DateTime_t871  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisDateTime_t871_m24013(__this, ___item, method) (( bool (*) (Array_t *, DateTime_t871 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisDateTime_t871_m24013_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTime>(T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" bool Array_InternalArray__ICollection_Remove_TisDateTime_t871_m24013_gshared (Array_t * __this, DateTime_t871  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.DateTime>(T)
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.DateTime>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.DateTime>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisDateTime_t871_m24014_gshared (Array_t * __this, DateTime_t871  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisDateTime_t871_m24014(__this, ___item, method) (( int32_t (*) (Array_t *, DateTime_t871 , const MethodInfo*))Array_InternalArray__IndexOf_TisDateTime_t871_m24014_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.DateTime>(T)
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1933;
extern "C" int32_t Array_InternalArray__IndexOf_TisDateTime_t871_m24014_gshared (Array_t * __this, DateTime_t871  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	DateTime_t871  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_2 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (DateTime_t871 *)(&V_2));
		DateTime_t871  L_5 = ___item;
		goto IL_005d;
	}
	{
		DateTime_t871  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		DateTime_t871  L_10 = ___item;
		DateTime_t871  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((DateTime_t871 *)(&V_2));
		bool L_13 = DateTime_Equals_m12930((DateTime_t871 *)(&V_2), (Object_t *)L_12, NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.DateTime>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.DateTime>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.DateTime>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisDateTime_t871_m24015_gshared (Array_t * __this, int32_t ___index, DateTime_t871  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisDateTime_t871_m24015(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, DateTime_t871 , const MethodInfo*))Array_InternalArray__Insert_TisDateTime_t871_m24015_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.DateTime>(System.Int32,T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" void Array_InternalArray__Insert_TisDateTime_t871_m24015_gshared (Array_t * __this, int32_t ___index, DateTime_t871  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.DateTime>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.DateTime>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.DateTime>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.DateTime>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisDateTime_t871_m24017_gshared (Array_t * __this, int32_t ___index, DateTime_t871  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisDateTime_t871_m24017(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, DateTime_t871 , const MethodInfo*))Array_InternalArray__set_Item_TisDateTime_t871_m24017_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.DateTime>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1484;
extern "C" void Array_InternalArray__set_Item_TisDateTime_t871_m24017_gshared (Array_t * __this, int32_t ___index, DateTime_t871  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t470* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1130 * L_2 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7443(L_2, (String_t*)_stringLiteral1484, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t470*)((ObjectU5BU5D_t470*)IsInst(__this, ObjectU5BU5D_t470_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t470* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t470* L_4 = V_0;
		int32_t L_5 = ___index;
		DateTime_t871  L_6 = ___item;
		DateTime_t871  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5)) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (DateTime_t871 *)(&___item));
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.DateTime>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.DateTime>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_113.h"
// System.Array/InternalEnumerator`1<System.DateTime>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_113MethodDeclarations.h"
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTime>()
struct Array_t;
struct IEnumerator_1_t3386;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTime>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTime>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t871_m24018_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t871_m24018(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t871_m24018_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTime>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDateTime_t871_m24018_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t2946  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t2946 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t2946  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Decimal
#include "mscorlib_System_Decimal.h"
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.Decimal>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern "C" Decimal_t1133  Array_InternalArray__get_Item_TisDecimal_t1133_m24020_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDecimal_t1133_m24020(__this, ___index, method) (( Decimal_t1133  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDecimal_t1133_m24020_gshared)(__this, ___index, method)
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1484;
extern "C" Decimal_t1133  Array_InternalArray__get_Item_TisDecimal_t1133_m24020_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		s_Il2CppMethodIntialized = true;
	}
	Decimal_t1133  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1130 * L_2 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7443(L_2, (String_t*)_stringLiteral1484, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (Decimal_t1133 *)(&V_0));
		Decimal_t1133  L_4 = V_0;
		return L_4;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.Decimal>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.Decimal>(T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.Decimal>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Decimal>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisDecimal_t1133_m24021_gshared (Array_t * __this, Decimal_t1133  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisDecimal_t1133_m24021(__this, ___item, method) (( void (*) (Array_t *, Decimal_t1133 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisDecimal_t1133_m24021_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Decimal>(T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" void Array_InternalArray__ICollection_Add_TisDecimal_t1133_m24021_gshared (Array_t * __this, Decimal_t1133  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Decimal>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.Decimal>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Decimal>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisDecimal_t1133_m24022_gshared (Array_t * __this, Decimal_t1133  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisDecimal_t1133_m24022(__this, ___item, method) (( bool (*) (Array_t *, Decimal_t1133 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisDecimal_t1133_m24022_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Decimal>(T)
// System.Decimal
#include "mscorlib_System_DecimalMethodDeclarations.h"
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1933;
extern "C" bool Array_InternalArray__ICollection_Contains_TisDecimal_t1133_m24022_gshared (Array_t * __this, Decimal_t1133  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Decimal_t1133  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_2 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Decimal_t1133 *)(&V_2));
		Decimal_t1133  L_5 = ___item;
		goto IL_004d;
	}
	{
		Decimal_t1133  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		Decimal_t1133  L_7 = V_2;
		Decimal_t1133  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Decimal_t1133 *)(&___item));
		bool L_10 = Decimal_Equals_m8970((Decimal_t1133 *)(&___item), (Object_t *)L_9, NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Decimal>(T[],System.Int32)
struct Array_t;
struct DecimalU5BU5D_t2207;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.Decimal>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Decimal>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDecimal_t1133_m24023_gshared (Array_t * __this, DecimalU5BU5D_t2207* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisDecimal_t1133_m24023(__this, ___array, ___index, method) (( void (*) (Array_t *, DecimalU5BU5D_t2207*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisDecimal_t1133_m24023_gshared)(__this, ___array, ___index, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Decimal>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t1128_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t556_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1052;
extern Il2CppCodeGenString* _stringLiteral1933;
extern Il2CppCodeGenString* _stringLiteral1970;
extern Il2CppCodeGenString* _stringLiteral1484;
extern Il2CppCodeGenString* _stringLiteral1956;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisDecimal_t1133_m24023_gshared (Array_t * __this, DecimalU5BU5D_t2207* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(706);
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		ArgumentException_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		_stringLiteral1052 = il2cpp_codegen_string_literal_from_index(1052);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		_stringLiteral1970 = il2cpp_codegen_string_literal_from_index(1970);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		_stringLiteral1956 = il2cpp_codegen_string_literal_from_index(1956);
		s_Il2CppMethodIntialized = true;
	}
	{
		DecimalU5BU5D_t2207* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1128 * L_1 = (ArgumentNullException_t1128 *)il2cpp_codegen_object_new (ArgumentNullException_t1128_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m6429(L_1, (String_t*)_stringLiteral1052, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_4 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m2938((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		DecimalU5BU5D_t2207* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m9182((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		DecimalU5BU5D_t2207* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m2938((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t556 * L_11 = (ArgumentException_t556 *)il2cpp_codegen_object_new (ArgumentException_t556_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3362(L_11, (String_t*)_stringLiteral1970, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_0055:
	{
		DecimalU5BU5D_t2207* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m8449((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_15 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1956, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1130 * L_18 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6436(L_18, (String_t*)_stringLiteral1484, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		DecimalU5BU5D_t2207* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m2938((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m3325(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Decimal>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.Decimal>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Decimal>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisDecimal_t1133_m24024_gshared (Array_t * __this, Decimal_t1133  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisDecimal_t1133_m24024(__this, ___item, method) (( bool (*) (Array_t *, Decimal_t1133 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisDecimal_t1133_m24024_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Decimal>(T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" bool Array_InternalArray__ICollection_Remove_TisDecimal_t1133_m24024_gshared (Array_t * __this, Decimal_t1133  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.Decimal>(T)
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.Decimal>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Decimal>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisDecimal_t1133_m24025_gshared (Array_t * __this, Decimal_t1133  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisDecimal_t1133_m24025(__this, ___item, method) (( int32_t (*) (Array_t *, Decimal_t1133 , const MethodInfo*))Array_InternalArray__IndexOf_TisDecimal_t1133_m24025_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Decimal>(T)
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1933;
extern "C" int32_t Array_InternalArray__IndexOf_TisDecimal_t1133_m24025_gshared (Array_t * __this, Decimal_t1133  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Decimal_t1133  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_2 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (Decimal_t1133 *)(&V_2));
		Decimal_t1133  L_5 = ___item;
		goto IL_005d;
	}
	{
		Decimal_t1133  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		Decimal_t1133  L_10 = ___item;
		Decimal_t1133  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Decimal_t1133 *)(&V_2));
		bool L_13 = Decimal_Equals_m8970((Decimal_t1133 *)(&V_2), (Object_t *)L_12, NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.Decimal>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.Decimal>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Decimal>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisDecimal_t1133_m24026_gshared (Array_t * __this, int32_t ___index, Decimal_t1133  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisDecimal_t1133_m24026(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Decimal_t1133 , const MethodInfo*))Array_InternalArray__Insert_TisDecimal_t1133_m24026_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Decimal>(System.Int32,T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" void Array_InternalArray__Insert_TisDecimal_t1133_m24026_gshared (Array_t * __this, int32_t ___index, Decimal_t1133  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.Decimal>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.Decimal>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.Decimal>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Decimal>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisDecimal_t1133_m24028_gshared (Array_t * __this, int32_t ___index, Decimal_t1133  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisDecimal_t1133_m24028(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, Decimal_t1133 , const MethodInfo*))Array_InternalArray__set_Item_TisDecimal_t1133_m24028_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Decimal>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1484;
extern "C" void Array_InternalArray__set_Item_TisDecimal_t1133_m24028_gshared (Array_t * __this, int32_t ___index, Decimal_t1133  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t470* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1130 * L_2 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7443(L_2, (String_t*)_stringLiteral1484, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t470*)((ObjectU5BU5D_t470*)IsInst(__this, ObjectU5BU5D_t470_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t470* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t470* L_4 = V_0;
		int32_t L_5 = ___index;
		Decimal_t1133  L_6 = ___item;
		Decimal_t1133  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5)) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (Decimal_t1133 *)(&___item));
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.Decimal>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.Decimal>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_114.h"
// System.Array/InternalEnumerator`1<System.Decimal>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_114MethodDeclarations.h"
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Decimal>()
struct Array_t;
struct IEnumerator_1_t3387;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Decimal>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Decimal>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1133_m24029_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1133_m24029(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1133_m24029_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Decimal>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisDecimal_t1133_m24029_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t2947  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t2947 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t2947  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.TimeSpan>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C" TimeSpan_t1437  Array_InternalArray__get_Item_TisTimeSpan_t1437_m24031_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeSpan_t1437_m24031(__this, ___index, method) (( TimeSpan_t1437  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeSpan_t1437_m24031_gshared)(__this, ___index, method)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1484;
extern "C" TimeSpan_t1437  Array_InternalArray__get_Item_TisTimeSpan_t1437_m24031_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t1437  V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1130 * L_2 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7443(L_2, (String_t*)_stringLiteral1484, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (TimeSpan_t1437 *)(&V_0));
		TimeSpan_t1437  L_4 = V_0;
		return L_4;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.TimeSpan>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.TimeSpan>(T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.TimeSpan>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.TimeSpan>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisTimeSpan_t1437_m24032_gshared (Array_t * __this, TimeSpan_t1437  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisTimeSpan_t1437_m24032(__this, ___item, method) (( void (*) (Array_t *, TimeSpan_t1437 , const MethodInfo*))Array_InternalArray__ICollection_Add_TisTimeSpan_t1437_m24032_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.TimeSpan>(T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" void Array_InternalArray__ICollection_Add_TisTimeSpan_t1437_m24032_gshared (Array_t * __this, TimeSpan_t1437  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.TimeSpan>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.TimeSpan>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.TimeSpan>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisTimeSpan_t1437_m24033_gshared (Array_t * __this, TimeSpan_t1437  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisTimeSpan_t1437_m24033(__this, ___item, method) (( bool (*) (Array_t *, TimeSpan_t1437 , const MethodInfo*))Array_InternalArray__ICollection_Contains_TisTimeSpan_t1437_m24033_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.TimeSpan>(T)
// System.TimeSpan
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1933;
extern "C" bool Array_InternalArray__ICollection_Contains_TisTimeSpan_t1437_m24033_gshared (Array_t * __this, TimeSpan_t1437  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	TimeSpan_t1437  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_2 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (TimeSpan_t1437 *)(&V_2));
		TimeSpan_t1437  L_5 = ___item;
		goto IL_004d;
	}
	{
		TimeSpan_t1437  L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		TimeSpan_t1437  L_7 = V_2;
		TimeSpan_t1437  L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((TimeSpan_t1437 *)(&___item));
		bool L_10 = TimeSpan_Equals_m13330((TimeSpan_t1437 *)(&___item), (Object_t *)L_9, NULL);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.TimeSpan>(T[],System.Int32)
struct Array_t;
struct TimeSpanU5BU5D_t2208;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.TimeSpan>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.TimeSpan>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t1437_m24034_gshared (Array_t * __this, TimeSpanU5BU5D_t2208* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t1437_m24034(__this, ___array, ___index, method) (( void (*) (Array_t *, TimeSpanU5BU5D_t2208*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t1437_m24034_gshared)(__this, ___array, ___index, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.TimeSpan>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t1128_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t556_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1052;
extern Il2CppCodeGenString* _stringLiteral1933;
extern Il2CppCodeGenString* _stringLiteral1970;
extern Il2CppCodeGenString* _stringLiteral1484;
extern Il2CppCodeGenString* _stringLiteral1956;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTimeSpan_t1437_m24034_gshared (Array_t * __this, TimeSpanU5BU5D_t2208* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(706);
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		ArgumentException_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		_stringLiteral1052 = il2cpp_codegen_string_literal_from_index(1052);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		_stringLiteral1970 = il2cpp_codegen_string_literal_from_index(1970);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		_stringLiteral1956 = il2cpp_codegen_string_literal_from_index(1956);
		s_Il2CppMethodIntialized = true;
	}
	{
		TimeSpanU5BU5D_t2208* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1128 * L_1 = (ArgumentNullException_t1128 *)il2cpp_codegen_object_new (ArgumentNullException_t1128_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m6429(L_1, (String_t*)_stringLiteral1052, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_4 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m2938((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TimeSpanU5BU5D_t2208* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m9182((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		TimeSpanU5BU5D_t2208* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m2938((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t556 * L_11 = (ArgumentException_t556 *)il2cpp_codegen_object_new (ArgumentException_t556_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3362(L_11, (String_t*)_stringLiteral1970, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_0055:
	{
		TimeSpanU5BU5D_t2208* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m8449((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_15 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1956, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1130 * L_18 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6436(L_18, (String_t*)_stringLiteral1484, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TimeSpanU5BU5D_t2208* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m2938((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m3325(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.TimeSpan>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.TimeSpan>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.TimeSpan>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisTimeSpan_t1437_m24035_gshared (Array_t * __this, TimeSpan_t1437  ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisTimeSpan_t1437_m24035(__this, ___item, method) (( bool (*) (Array_t *, TimeSpan_t1437 , const MethodInfo*))Array_InternalArray__ICollection_Remove_TisTimeSpan_t1437_m24035_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.TimeSpan>(T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" bool Array_InternalArray__ICollection_Remove_TisTimeSpan_t1437_m24035_gshared (Array_t * __this, TimeSpan_t1437  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.TimeSpan>(T)
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.TimeSpan>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.TimeSpan>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisTimeSpan_t1437_m24036_gshared (Array_t * __this, TimeSpan_t1437  ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisTimeSpan_t1437_m24036(__this, ___item, method) (( int32_t (*) (Array_t *, TimeSpan_t1437 , const MethodInfo*))Array_InternalArray__IndexOf_TisTimeSpan_t1437_m24036_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.TimeSpan>(T)
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1933;
extern "C" int32_t Array_InternalArray__IndexOf_TisTimeSpan_t1437_m24036_gshared (Array_t * __this, TimeSpan_t1437  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	TimeSpan_t1437  V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_2 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (TimeSpan_t1437 *)(&V_2));
		TimeSpan_t1437  L_5 = ___item;
		goto IL_005d;
	}
	{
		TimeSpan_t1437  L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		TimeSpan_t1437  L_10 = ___item;
		TimeSpan_t1437  L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((TimeSpan_t1437 *)(&V_2));
		bool L_13 = TimeSpan_Equals_m13330((TimeSpan_t1437 *)(&V_2), (Object_t *)L_12, NULL);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.TimeSpan>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.TimeSpan>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.TimeSpan>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisTimeSpan_t1437_m24037_gshared (Array_t * __this, int32_t ___index, TimeSpan_t1437  ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisTimeSpan_t1437_m24037(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, TimeSpan_t1437 , const MethodInfo*))Array_InternalArray__Insert_TisTimeSpan_t1437_m24037_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.TimeSpan>(System.Int32,T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" void Array_InternalArray__Insert_TisTimeSpan_t1437_m24037_gshared (Array_t * __this, int32_t ___index, TimeSpan_t1437  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.TimeSpan>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.TimeSpan>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.TimeSpan>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.TimeSpan>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisTimeSpan_t1437_m24039_gshared (Array_t * __this, int32_t ___index, TimeSpan_t1437  ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisTimeSpan_t1437_m24039(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, TimeSpan_t1437 , const MethodInfo*))Array_InternalArray__set_Item_TisTimeSpan_t1437_m24039_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.TimeSpan>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1484;
extern "C" void Array_InternalArray__set_Item_TisTimeSpan_t1437_m24039_gshared (Array_t * __this, int32_t ___index, TimeSpan_t1437  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t470* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1130 * L_2 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7443(L_2, (String_t*)_stringLiteral1484, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t470*)((ObjectU5BU5D_t470*)IsInst(__this, ObjectU5BU5D_t470_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t470* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t470* L_4 = V_0;
		int32_t L_5 = ___index;
		TimeSpan_t1437  L_6 = ___item;
		TimeSpan_t1437  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5)) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (TimeSpan_t1437 *)(&___item));
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.TimeSpan>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.TimeSpan>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_115.h"
// System.Array/InternalEnumerator`1<System.TimeSpan>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_115MethodDeclarations.h"
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TimeSpan>()
struct Array_t;
struct IEnumerator_1_t3388;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TimeSpan>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TimeSpan>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t1437_m24040_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t1437_m24040(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t1437_m24040_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TimeSpan>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTimeSpan_t1437_m24040_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t2948  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t2948 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t2948  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
struct Array_t;
// Declaration System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,!!0&)
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
struct Array_t;
// Declaration T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern "C" uint8_t Array_InternalArray__get_Item_TisTypeTag_t1930_m24042_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeTag_t1930_m24042(__this, ___index, method) (( uint8_t (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeTag_t1930_m24042_gshared)(__this, ___index, method)
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1484;
extern "C" uint8_t Array_InternalArray__get_Item_TisTypeTag_t1930_m24042_gshared (Array_t * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1130 * L_2 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7443(L_2, (String_t*)_stringLiteral1484, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_3, (uint8_t*)(&V_0));
		uint8_t L_4 = V_0;
		return L_4;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
// System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern "C" void Array_InternalArray__ICollection_Add_TisTypeTag_t1930_m24043_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Add_TisTypeTag_t1930_m24043(__this, ___item, method) (( void (*) (Array_t *, uint8_t, const MethodInfo*))Array_InternalArray__ICollection_Add_TisTypeTag_t1930_m24043_gshared)(__this, ___item, method)
// System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" void Array_InternalArray__ICollection_Add_TisTypeTag_t1930_m24043_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern "C" bool Array_InternalArray__ICollection_Contains_TisTypeTag_t1930_m24044_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Contains_TisTypeTag_t1930_m24044(__this, ___item, method) (( bool (*) (Array_t *, uint8_t, const MethodInfo*))Array_InternalArray__ICollection_Contains_TisTypeTag_t1930_m24044_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1933;
extern "C" bool Array_InternalArray__ICollection_Contains_TisTypeTag_t1930_m24044_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_2 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_006b;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (uint8_t*)(&V_2));
		uint8_t L_5 = ___item;
		goto IL_004d;
	}
	{
		uint8_t L_6 = V_2;
		goto IL_004b;
	}
	{
		return 1;
	}

IL_004b:
	{
		return 0;
	}

IL_004d:
	{
		uint8_t L_7 = V_2;
		uint8_t L_8 = L_7;
		Object_t * L_9 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_8);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)));
		bool L_10 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&___item)), (Object_t *)L_9);
		if (!L_10)
		{
			goto IL_0067;
		}
	}
	{
		return 1;
	}

IL_0067:
	{
		int32_t L_11 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002a;
		}
	}
	{
		return 0;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T[],System.Int32)
struct Array_t;
struct TypeTagU5BU5D_t2209;
// Declaration System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T[],System.Int32)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T[],System.Int32)
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTypeTag_t1930_m24045_gshared (Array_t * __this, TypeTagU5BU5D_t2209* ___array, int32_t ___index, const MethodInfo* method);
#define Array_InternalArray__ICollection_CopyTo_TisTypeTag_t1930_m24045(__this, ___array, ___index, method) (( void (*) (Array_t *, TypeTagU5BU5D_t2209*, int32_t, const MethodInfo*))Array_InternalArray__ICollection_CopyTo_TisTypeTag_t1930_m24045_gshared)(__this, ___array, ___index, method)
// System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T[],System.Int32)
extern TypeInfo* ArgumentNullException_t1128_il2cpp_TypeInfo_var;
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t556_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1052;
extern Il2CppCodeGenString* _stringLiteral1933;
extern Il2CppCodeGenString* _stringLiteral1970;
extern Il2CppCodeGenString* _stringLiteral1484;
extern Il2CppCodeGenString* _stringLiteral1956;
extern "C" void Array_InternalArray__ICollection_CopyTo_TisTypeTag_t1930_m24045_gshared (Array_t * __this, TypeTagU5BU5D_t2209* ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(706);
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		ArgumentException_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		_stringLiteral1052 = il2cpp_codegen_string_literal_from_index(1052);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		_stringLiteral1970 = il2cpp_codegen_string_literal_from_index(1970);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		_stringLiteral1956 = il2cpp_codegen_string_literal_from_index(1956);
		s_Il2CppMethodIntialized = true;
	}
	{
		TypeTagU5BU5D_t2209* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1128 * L_1 = (ArgumentNullException_t1128 *)il2cpp_codegen_object_new (ArgumentNullException_t1128_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m6429(L_1, (String_t*)_stringLiteral1052, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		NullCheck((Array_t *)__this);
		int32_t L_2 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_3 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_4 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_4, (String_t*)L_3, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_002d:
	{
		int32_t L_5 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_6 = Array_GetLength_m2938((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TypeTagU5BU5D_t2209* L_7 = ___array;
		NullCheck((Array_t *)L_7);
		int32_t L_8 = Array_GetLowerBound_m9182((Array_t *)L_7, (int32_t)0, /*hidden argument*/NULL);
		TypeTagU5BU5D_t2209* L_9 = ___array;
		NullCheck((Array_t *)L_9);
		int32_t L_10 = Array_GetLength_m2938((Array_t *)L_9, (int32_t)0, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)((int32_t)((int32_t)L_8+(int32_t)L_10)))))
		{
			goto IL_0055;
		}
	}
	{
		ArgumentException_t556 * L_11 = (ArgumentException_t556 *)il2cpp_codegen_object_new (ArgumentException_t556_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3362(L_11, (String_t*)_stringLiteral1970, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_0055:
	{
		TypeTagU5BU5D_t2209* L_12 = ___array;
		NullCheck((Array_t *)L_12);
		int32_t L_13 = Array_get_Rank_m8449((Array_t *)L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_13) <= ((int32_t)1)))
		{
			goto IL_0071;
		}
	}
	{
		String_t* L_14 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_15 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_15, (String_t*)L_14, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_15);
	}

IL_0071:
	{
		int32_t L_16 = ___index;
		if ((((int32_t)L_16) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		String_t* L_17 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1956, /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t1130 * L_18 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6436(L_18, (String_t*)_stringLiteral1484, (String_t*)L_17, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_008d:
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		TypeTagU5BU5D_t2209* L_20 = ___array;
		int32_t L_21 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_22 = Array_GetLength_m2938((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		Array_Copy_m3325(NULL /*static, unused*/, (Array_t *)__this, (int32_t)L_19, (Array_t *)(Array_t *)L_20, (int32_t)L_21, (int32_t)L_22, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
struct Array_t;
// Declaration System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern "C" bool Array_InternalArray__ICollection_Remove_TisTypeTag_t1930_m24046_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__ICollection_Remove_TisTypeTag_t1930_m24046(__this, ___item, method) (( bool (*) (Array_t *, uint8_t, const MethodInfo*))Array_InternalArray__ICollection_Remove_TisTypeTag_t1930_m24046_gshared)(__this, ___item, method)
// System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" bool Array_InternalArray__ICollection_Remove_TisTypeTag_t1930_m24046_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
struct Array_t;
// Declaration System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
// System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern "C" int32_t Array_InternalArray__IndexOf_TisTypeTag_t1930_m24047_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__IndexOf_TisTypeTag_t1930_m24047(__this, ___item, method) (( int32_t (*) (Array_t *, uint8_t, const MethodInfo*))Array_InternalArray__IndexOf_TisTypeTag_t1930_m24047_gshared)(__this, ___item, method)
// System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern TypeInfo* RankException_t2130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1933;
extern "C" int32_t Array_InternalArray__IndexOf_TisTypeTag_t1930_m24047_gshared (Array_t * __this, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RankException_t2130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		_stringLiteral1933 = il2cpp_codegen_string_literal_from_index(1933);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		NullCheck((Array_t *)__this);
		int32_t L_0 = Array_get_Rank_m8449((Array_t *)__this, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = Locale_GetText_m9372(NULL /*static, unused*/, (String_t*)_stringLiteral1933, /*hidden argument*/NULL);
		RankException_t2130 * L_2 = (RankException_t2130 *)il2cpp_codegen_object_new (RankException_t2130_il2cpp_TypeInfo_var);
		RankException__ctor_m13282(L_2, (String_t*)L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_001c:
	{
		NullCheck((Array_t *)__this);
		int32_t L_3 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		goto IL_0083;
	}

IL_002a:
	{
		int32_t L_4 = V_1;
		NullCheck((Array_t *)__this);
		ArrayGetGenericValueImpl ((Array_t *)__this, (int32_t)L_4, (uint8_t*)(&V_2));
		uint8_t L_5 = ___item;
		goto IL_005d;
	}
	{
		uint8_t L_6 = V_2;
		goto IL_0053;
	}
	{
		int32_t L_7 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_8 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_7+(int32_t)L_8));
	}

IL_0053:
	{
		NullCheck((Array_t *)__this);
		int32_t L_9 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_9-(int32_t)1));
	}

IL_005d:
	{
		uint8_t L_10 = ___item;
		uint8_t L_11 = L_10;
		Object_t * L_12 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_11);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)));
		bool L_13 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (&V_2)), (Object_t *)L_12);
		if (!L_13)
		{
			goto IL_007f;
		}
	}
	{
		int32_t L_14 = V_1;
		NullCheck((Array_t *)__this);
		int32_t L_15 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_14+(int32_t)L_15));
	}

IL_007f:
	{
		int32_t L_16 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0083:
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck((Array_t *)__this);
		int32_t L_19 = Array_GetLowerBound_m9182((Array_t *)__this, (int32_t)0, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_19-(int32_t)1));
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
// System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
extern "C" void Array_InternalArray__Insert_TisTypeTag_t1930_m24048_gshared (Array_t * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__Insert_TisTypeTag_t1930_m24048(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, uint8_t, const MethodInfo*))Array_InternalArray__Insert_TisTypeTag_t1930_m24048_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1505;
extern "C" void Array_InternalArray__Insert_TisTypeTag_t1930_m24048_gshared (Array_t * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		_stringLiteral1505 = il2cpp_codegen_string_literal_from_index(1505);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m7444(L_0, (String_t*)_stringLiteral1505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

struct Array_t;
// Declaration System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,!!0&)
// System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
struct Array_t;
// Declaration System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
// System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
extern "C" void Array_InternalArray__set_Item_TisTypeTag_t1930_m24050_gshared (Array_t * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method);
#define Array_InternalArray__set_Item_TisTypeTag_t1930_m24050(__this, ___index, ___item, method) (( void (*) (Array_t *, int32_t, uint8_t, const MethodInfo*))Array_InternalArray__set_Item_TisTypeTag_t1930_m24050_gshared)(__this, ___index, ___item, method)
// System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1484;
extern "C" void Array_InternalArray__set_Item_TisTypeTag_t1930_m24050_gshared (Array_t * __this, int32_t ___index, uint8_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(712);
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		_stringLiteral1484 = il2cpp_codegen_string_literal_from_index(1484);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t470* V_0 = {0};
	{
		int32_t L_0 = ___index;
		NullCheck((Array_t *)__this);
		int32_t L_1 = Array_get_Length_m8446((Array_t *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t1130 * L_2 = (ArgumentOutOfRangeException_t1130 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m7443(L_2, (String_t*)_stringLiteral1484, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0017:
	{
		V_0 = (ObjectU5BU5D_t470*)((ObjectU5BU5D_t470*)IsInst(__this, ObjectU5BU5D_t470_il2cpp_TypeInfo_var));
		ObjectU5BU5D_t470* L_3 = V_0;
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		ObjectU5BU5D_t470* L_4 = V_0;
		int32_t L_5 = ___index;
		uint8_t L_6 = ___item;
		uint8_t L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_7);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		ArrayElementTypeCheck (L_4, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, L_5)) = (Object_t *)L_8;
		return;
	}

IL_002e:
	{
		int32_t L_9 = ___index;
		NullCheck((Array_t *)__this);
		ArraySetGenericValueImpl ((Array_t *)__this, (int32_t)L_9, (uint8_t*)(&___item));
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T&)
#ifndef _MSC_VER
#else
#endif

// System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_116.h"
// System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_116MethodDeclarations.h"
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.Binary.TypeTag>()
struct Array_t;
struct IEnumerator_1_t3389;
// Declaration System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.Binary.TypeTag>()
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.Binary.TypeTag>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTypeTag_t1930_m24051_gshared (Array_t * __this, const MethodInfo* method);
#define Array_InternalArray__IEnumerable_GetEnumerator_TisTypeTag_t1930_m24051(__this, method) (( Object_t* (*) (Array_t *, const MethodInfo*))Array_InternalArray__IEnumerable_GetEnumerator_TisTypeTag_t1930_m24051_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.Binary.TypeTag>()
extern "C" Object_t* Array_InternalArray__IEnumerable_GetEnumerator_TisTypeTag_t1930_m24051_gshared (Array_t * __this, const MethodInfo* method)
{
	{
		InternalEnumerator_1_t2949  L_0 = {0};
		(( void (*) (InternalEnumerator_1_t2949 *, Array_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(&L_0, (Array_t *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		InternalEnumerator_1_t2949  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), &L_1);
		return (Object_t*)L_2;
	}
}
