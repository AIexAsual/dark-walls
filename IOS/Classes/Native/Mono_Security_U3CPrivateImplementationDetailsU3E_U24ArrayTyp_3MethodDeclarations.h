﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$48
struct U24ArrayTypeU2448_t1327;
struct U24ArrayTypeU2448_t1327_marshaled;

void U24ArrayTypeU2448_t1327_marshal(const U24ArrayTypeU2448_t1327& unmarshaled, U24ArrayTypeU2448_t1327_marshaled& marshaled);
void U24ArrayTypeU2448_t1327_marshal_back(const U24ArrayTypeU2448_t1327_marshaled& marshaled, U24ArrayTypeU2448_t1327& unmarshaled);
void U24ArrayTypeU2448_t1327_marshal_cleanup(U24ArrayTypeU2448_t1327_marshaled& marshaled);
