﻿#pragma once
#include <stdint.h>
// MovingObjectManager
struct MovingObjectManager_t374;
// MovingObjects
struct MovingObjects_t375;
// DoorController
struct DoorController_t311;
// System.Object
#include "mscorlib_System_Object.h"
// MovingObjectManager
struct  MovingObjectManager_t374  : public Object_t
{
	// MovingObjects MovingObjectManager::movingObjects
	MovingObjects_t375 * ___movingObjects_1;
	// DoorController MovingObjectManager::doorController
	DoorController_t311 * ___doorController_2;
};
struct MovingObjectManager_t374_StaticFields{
	// MovingObjectManager MovingObjectManager::_instance
	MovingObjectManager_t374 * ____instance_0;
};
