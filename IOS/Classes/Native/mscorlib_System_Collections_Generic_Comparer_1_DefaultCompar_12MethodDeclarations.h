﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t2701;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m20322_gshared (DefaultComparer_t2701 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m20322(__this, method) (( void (*) (DefaultComparer_t2701 *, const MethodInfo*))DefaultComparer__ctor_m20322_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m20323_gshared (DefaultComparer_t2701 * __this, UILineInfo_t809  ___x, UILineInfo_t809  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m20323(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2701 *, UILineInfo_t809 , UILineInfo_t809 , const MethodInfo*))DefaultComparer_Compare_m20323_gshared)(__this, ___x, ___y, method)
