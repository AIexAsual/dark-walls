﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_80
struct CameraFilterPack_TV_80_t176;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_80::.ctor()
extern "C" void CameraFilterPack_TV_80__ctor_m1139 (CameraFilterPack_TV_80_t176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_80::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_80_get_material_m1140 (CameraFilterPack_TV_80_t176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_80::Start()
extern "C" void CameraFilterPack_TV_80_Start_m1141 (CameraFilterPack_TV_80_t176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_80::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_80_OnRenderImage_m1142 (CameraFilterPack_TV_80_t176 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_80::Update()
extern "C" void CameraFilterPack_TV_80_Update_m1143 (CameraFilterPack_TV_80_t176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_80::OnDisable()
extern "C" void CameraFilterPack_TV_80_OnDisable_m1144 (CameraFilterPack_TV_80_t176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
