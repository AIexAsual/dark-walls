﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// CameraFilterPack_AAA_SuperHexagon
struct  CameraFilterPack_AAA_SuperHexagon_t8  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_AAA_SuperHexagon::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_AAA_SuperHexagon::_AlphaHexa
	float ____AlphaHexa_3;
	// System.Single CameraFilterPack_AAA_SuperHexagon::TimeX
	float ___TimeX_4;
	// UnityEngine.Vector4 CameraFilterPack_AAA_SuperHexagon::ScreenResolution
	Vector4_t5  ___ScreenResolution_5;
	// UnityEngine.Material CameraFilterPack_AAA_SuperHexagon::SCMaterial
	Material_t2 * ___SCMaterial_6;
	// System.Single CameraFilterPack_AAA_SuperHexagon::HexaSize
	float ___HexaSize_7;
	// System.Single CameraFilterPack_AAA_SuperHexagon::_BorderSize
	float ____BorderSize_8;
	// UnityEngine.Color CameraFilterPack_AAA_SuperHexagon::_BorderColor
	Color_t6  ____BorderColor_9;
	// UnityEngine.Color CameraFilterPack_AAA_SuperHexagon::_HexaColor
	Color_t6  ____HexaColor_10;
	// System.Single CameraFilterPack_AAA_SuperHexagon::_SpotSize
	float ____SpotSize_11;
	// UnityEngine.Vector2 CameraFilterPack_AAA_SuperHexagon::center
	Vector2_t7  ___center_18;
	// System.Single CameraFilterPack_AAA_SuperHexagon::Radius
	float ___Radius_19;
};
struct CameraFilterPack_AAA_SuperHexagon_t8_StaticFields{
	// System.Single CameraFilterPack_AAA_SuperHexagon::ChangeBorderSize
	float ___ChangeBorderSize_12;
	// UnityEngine.Color CameraFilterPack_AAA_SuperHexagon::ChangeBorderColor
	Color_t6  ___ChangeBorderColor_13;
	// UnityEngine.Color CameraFilterPack_AAA_SuperHexagon::ChangeHexaColor
	Color_t6  ___ChangeHexaColor_14;
	// System.Single CameraFilterPack_AAA_SuperHexagon::ChangeSpotSize
	float ___ChangeSpotSize_15;
	// System.Single CameraFilterPack_AAA_SuperHexagon::ChangeAlphaHexa
	float ___ChangeAlphaHexa_16;
	// System.Single CameraFilterPack_AAA_SuperHexagon::ChangeValue
	float ___ChangeValue_17;
	// UnityEngine.Vector2 CameraFilterPack_AAA_SuperHexagon::Changecenter
	Vector2_t7  ___Changecenter_20;
	// System.Single CameraFilterPack_AAA_SuperHexagon::ChangeRadius
	float ___ChangeRadius_21;
};
