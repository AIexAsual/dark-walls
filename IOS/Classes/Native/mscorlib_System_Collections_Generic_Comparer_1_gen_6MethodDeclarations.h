﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.Color>
struct Comparer_1_t2437;
// System.Object
struct Object_t;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Color>::.ctor()
extern "C" void Comparer_1__ctor_m16206_gshared (Comparer_1_t2437 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m16206(__this, method) (( void (*) (Comparer_1_t2437 *, const MethodInfo*))Comparer_1__ctor_m16206_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Color>::.cctor()
extern "C" void Comparer_1__cctor_m16207_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m16207(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m16207_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Color>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m16208_gshared (Comparer_1_t2437 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m16208(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t2437 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m16208_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Color>::get_Default()
extern "C" Comparer_1_t2437 * Comparer_1_get_Default_m16209_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m16209(__this /* static, unused */, method) (( Comparer_1_t2437 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m16209_gshared)(__this /* static, unused */, method)
