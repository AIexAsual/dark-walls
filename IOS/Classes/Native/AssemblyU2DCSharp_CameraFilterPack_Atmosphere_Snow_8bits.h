﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Atmosphere_Snow_8bits
struct  CameraFilterPack_Atmosphere_Snow_8bits_t13  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Atmosphere_Snow_8bits::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Atmosphere_Snow_8bits::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Atmosphere_Snow_8bits::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Atmosphere_Snow_8bits::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Atmosphere_Snow_8bits::Threshold
	float ___Threshold_6;
	// System.Single CameraFilterPack_Atmosphere_Snow_8bits::Size
	float ___Size_7;
	// System.Single CameraFilterPack_Atmosphere_Snow_8bits::Value3
	float ___Value3_8;
	// System.Single CameraFilterPack_Atmosphere_Snow_8bits::Value4
	float ___Value4_9;
};
struct CameraFilterPack_Atmosphere_Snow_8bits_t13_StaticFields{
	// System.Single CameraFilterPack_Atmosphere_Snow_8bits::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Atmosphere_Snow_8bits::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Atmosphere_Snow_8bits::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Atmosphere_Snow_8bits::ChangeValue4
	float ___ChangeValue4_13;
};
