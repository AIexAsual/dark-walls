﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Type>
struct Enumerator_t2325;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t545;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Type>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Type>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"
#define Enumerator__ctor_m14806(__this, ___dictionary, method) (( void (*) (Enumerator_t2325 *, Dictionary_2_t545 *, const MethodInfo*))Enumerator__ctor_m14807_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14808(__this, method) (( Object_t * (*) (Enumerator_t2325 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14809_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Type>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14810(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2325 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14811_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Type>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14812(__this, method) (( Object_t * (*) (Enumerator_t2325 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14813_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Type>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14814(__this, method) (( Object_t * (*) (Enumerator_t2325 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14815_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Type>::MoveNext()
#define Enumerator_MoveNext_m14816(__this, method) (( bool (*) (Enumerator_t2325 *, const MethodInfo*))Enumerator_MoveNext_m14817_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Type>::get_Current()
#define Enumerator_get_Current_m14818(__this, method) (( KeyValuePair_2_t2322  (*) (Enumerator_t2325 *, const MethodInfo*))Enumerator_get_Current_m14819_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Type>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m14820(__this, method) (( String_t* (*) (Enumerator_t2325 *, const MethodInfo*))Enumerator_get_CurrentKey_m14821_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Type>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m14822(__this, method) (( Type_t * (*) (Enumerator_t2325 *, const MethodInfo*))Enumerator_get_CurrentValue_m14823_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Type>::VerifyState()
#define Enumerator_VerifyState_m14824(__this, method) (( void (*) (Enumerator_t2325 *, const MethodInfo*))Enumerator_VerifyState_m14825_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Type>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m14826(__this, method) (( void (*) (Enumerator_t2325 *, const MethodInfo*))Enumerator_VerifyCurrent_m14827_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Type>::Dispose()
#define Enumerator_Dispose_m14828(__this, method) (( void (*) (Enumerator_t2325 *, const MethodInfo*))Enumerator_Dispose_m14829_gshared)(__this, method)
