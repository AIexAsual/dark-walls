﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
struct KeyValuePair_2_t2514;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m17534_gshared (KeyValuePair_2_t2514 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m17534(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2514 *, int32_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m17534_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m17535_gshared (KeyValuePair_2_t2514 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m17535(__this, method) (( int32_t (*) (KeyValuePair_2_t2514 *, const MethodInfo*))KeyValuePair_2_get_Key_m17535_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m17536_gshared (KeyValuePair_2_t2514 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m17536(__this, ___value, method) (( void (*) (KeyValuePair_2_t2514 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m17536_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m17537_gshared (KeyValuePair_2_t2514 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m17537(__this, method) (( Object_t * (*) (KeyValuePair_2_t2514 *, const MethodInfo*))KeyValuePair_2_get_Value_m17537_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m17538_gshared (KeyValuePair_2_t2514 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m17538(__this, ___value, method) (( void (*) (KeyValuePair_2_t2514 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m17538_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m17539_gshared (KeyValuePair_2_t2514 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m17539(__this, method) (( String_t* (*) (KeyValuePair_2_t2514 *, const MethodInfo*))KeyValuePair_2_ToString_m17539_gshared)(__this, method)
