﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Navigation
struct Navigation_t690;
// UnityEngine.UI.Selectable
struct Selectable_t636;
// UnityEngine.UI.Navigation/Mode
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"
// UnityEngine.UI.Navigation
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"

// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::get_mode()
extern "C" int32_t Navigation_get_mode_m4020 (Navigation_t690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_mode(UnityEngine.UI.Navigation/Mode)
extern "C" void Navigation_set_mode_m4021 (Navigation_t690 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnUp()
extern "C" Selectable_t636 * Navigation_get_selectOnUp_m4022 (Navigation_t690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnUp(UnityEngine.UI.Selectable)
extern "C" void Navigation_set_selectOnUp_m4023 (Navigation_t690 * __this, Selectable_t636 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnDown()
extern "C" Selectable_t636 * Navigation_get_selectOnDown_m4024 (Navigation_t690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnDown(UnityEngine.UI.Selectable)
extern "C" void Navigation_set_selectOnDown_m4025 (Navigation_t690 * __this, Selectable_t636 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnLeft()
extern "C" Selectable_t636 * Navigation_get_selectOnLeft_m4026 (Navigation_t690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnLeft(UnityEngine.UI.Selectable)
extern "C" void Navigation_set_selectOnLeft_m4027 (Navigation_t690 * __this, Selectable_t636 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnRight()
extern "C" Selectable_t636 * Navigation_get_selectOnRight_m4028 (Navigation_t690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnRight(UnityEngine.UI.Selectable)
extern "C" void Navigation_set_selectOnRight_m4029 (Navigation_t690 * __this, Selectable_t636 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Navigation UnityEngine.UI.Navigation::get_defaultNavigation()
extern "C" Navigation_t690  Navigation_get_defaultNavigation_m4030 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
