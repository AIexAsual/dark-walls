﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>
struct Enumerator_t1112;
// System.Object
struct Object_t;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t877;
// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
struct List_1_t878;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m19540(__this, ___l, method) (( void (*) (Enumerator_t1112 *, List_1_t878 *, const MethodInfo*))Enumerator__ctor_m13586_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19541(__this, method) (( Object_t * (*) (Enumerator_t1112 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13587_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::Dispose()
#define Enumerator_Dispose_m19542(__this, method) (( void (*) (Enumerator_t1112 *, const MethodInfo*))Enumerator_Dispose_m13588_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::VerifyState()
#define Enumerator_VerifyState_m19543(__this, method) (( void (*) (Enumerator_t1112 *, const MethodInfo*))Enumerator_VerifyState_m13589_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::MoveNext()
#define Enumerator_MoveNext_m6365(__this, method) (( bool (*) (Enumerator_t1112 *, const MethodInfo*))Enumerator_MoveNext_m13590_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::get_Current()
#define Enumerator_get_Current_m6364(__this, method) (( GUILayoutEntry_t877 * (*) (Enumerator_t1112 *, const MethodInfo*))Enumerator_get_Current_m13591_gshared)(__this, method)
