﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Colors_Adjust_FullColors
struct CameraFilterPack_Colors_Adjust_FullColors_t70;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Colors_Adjust_FullColors::.ctor()
extern "C" void CameraFilterPack_Colors_Adjust_FullColors__ctor_m440 (CameraFilterPack_Colors_Adjust_FullColors_t70 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_Adjust_FullColors::get_material()
extern "C" Material_t2 * CameraFilterPack_Colors_Adjust_FullColors_get_material_m441 (CameraFilterPack_Colors_Adjust_FullColors_t70 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_FullColors::Start()
extern "C" void CameraFilterPack_Colors_Adjust_FullColors_Start_m442 (CameraFilterPack_Colors_Adjust_FullColors_t70 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_FullColors::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Colors_Adjust_FullColors_OnRenderImage_m443 (CameraFilterPack_Colors_Adjust_FullColors_t70 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_FullColors::Update()
extern "C" void CameraFilterPack_Colors_Adjust_FullColors_Update_m444 (CameraFilterPack_Colors_Adjust_FullColors_t70 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_FullColors::OnDisable()
extern "C" void CameraFilterPack_Colors_Adjust_FullColors_OnDisable_m445 (CameraFilterPack_Colors_Adjust_FullColors_t70 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
