﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Distortion_BigFace
struct  CameraFilterPack_Distortion_BigFace_t80  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Distortion_BigFace::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Distortion_BigFace::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Distortion_BigFace::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Distortion_BigFace::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Distortion_BigFace::_Size
	float ____Size_6;
	// System.Single CameraFilterPack_Distortion_BigFace::Distortion
	float ___Distortion_7;
};
struct CameraFilterPack_Distortion_BigFace_t80_StaticFields{
	// System.Single CameraFilterPack_Distortion_BigFace::ChangeSize
	float ___ChangeSize_8;
	// System.Single CameraFilterPack_Distortion_BigFace::ChangeDistortion
	float ___ChangeDistortion_9;
};
