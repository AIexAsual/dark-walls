﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.Single>
struct Comparer_1_t2407;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.Single>
struct  Comparer_1_t2407  : public Object_t
{
};
struct Comparer_1_t2407_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Single>::_default
	Comparer_1_t2407 * ____default_0;
};
