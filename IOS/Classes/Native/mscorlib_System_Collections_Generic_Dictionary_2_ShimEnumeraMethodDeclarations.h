﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<iTweenEvent/TweenType,System.Object>
struct ShimEnumerator_t2340;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>
struct Dictionary_2_t2327;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<iTweenEvent/TweenType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m14917_gshared (ShimEnumerator_t2340 * __this, Dictionary_2_t2327 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m14917(__this, ___host, method) (( void (*) (ShimEnumerator_t2340 *, Dictionary_2_t2327 *, const MethodInfo*))ShimEnumerator__ctor_m14917_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<iTweenEvent/TweenType,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m14918_gshared (ShimEnumerator_t2340 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m14918(__this, method) (( bool (*) (ShimEnumerator_t2340 *, const MethodInfo*))ShimEnumerator_MoveNext_m14918_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<iTweenEvent/TweenType,System.Object>::get_Entry()
extern "C" DictionaryEntry_t552  ShimEnumerator_get_Entry_m14919_gshared (ShimEnumerator_t2340 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m14919(__this, method) (( DictionaryEntry_t552  (*) (ShimEnumerator_t2340 *, const MethodInfo*))ShimEnumerator_get_Entry_m14919_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<iTweenEvent/TweenType,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m14920_gshared (ShimEnumerator_t2340 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m14920(__this, method) (( Object_t * (*) (ShimEnumerator_t2340 *, const MethodInfo*))ShimEnumerator_get_Key_m14920_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<iTweenEvent/TweenType,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m14921_gshared (ShimEnumerator_t2340 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m14921(__this, method) (( Object_t * (*) (ShimEnumerator_t2340 *, const MethodInfo*))ShimEnumerator_get_Value_m14921_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<iTweenEvent/TweenType,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m14922_gshared (ShimEnumerator_t2340 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m14922(__this, method) (( Object_t * (*) (ShimEnumerator_t2340 *, const MethodInfo*))ShimEnumerator_get_Current_m14922_gshared)(__this, method)
