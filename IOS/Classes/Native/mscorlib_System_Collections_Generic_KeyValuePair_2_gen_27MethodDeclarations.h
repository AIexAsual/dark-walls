﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct KeyValuePair_2_t2825;
// System.Object
struct Object_t;
// System.String
struct String_t;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m21882_gshared (KeyValuePair_2_t2825 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m21882(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2825 *, Object_t *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m21882_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m21883_gshared (KeyValuePair_2_t2825 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m21883(__this, method) (( Object_t * (*) (KeyValuePair_2_t2825 *, const MethodInfo*))KeyValuePair_2_get_Key_m21883_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m21884_gshared (KeyValuePair_2_t2825 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m21884(__this, ___value, method) (( void (*) (KeyValuePair_2_t2825 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m21884_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C" int32_t KeyValuePair_2_get_Value_m21885_gshared (KeyValuePair_2_t2825 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m21885(__this, method) (( int32_t (*) (KeyValuePair_2_t2825 *, const MethodInfo*))KeyValuePair_2_get_Value_m21885_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m21886_gshared (KeyValuePair_2_t2825 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m21886(__this, ___value, method) (( void (*) (KeyValuePair_2_t2825 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m21886_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m21887_gshared (KeyValuePair_2_t2825 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m21887(__this, method) (( String_t* (*) (KeyValuePair_2_t2825 *, const MethodInfo*))KeyValuePair_2_ToString_m21887_gshared)(__this, method)
