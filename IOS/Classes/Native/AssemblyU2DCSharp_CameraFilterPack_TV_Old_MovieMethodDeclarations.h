﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_Old_Movie
struct CameraFilterPack_TV_Old_Movie_t188;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_Old_Movie::.ctor()
extern "C" void CameraFilterPack_TV_Old_Movie__ctor_m1216 (CameraFilterPack_TV_Old_Movie_t188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Old_Movie::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_Old_Movie_get_material_m1217 (CameraFilterPack_TV_Old_Movie_t188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old_Movie::Start()
extern "C" void CameraFilterPack_TV_Old_Movie_Start_m1218 (CameraFilterPack_TV_Old_Movie_t188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old_Movie::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_Old_Movie_OnRenderImage_m1219 (CameraFilterPack_TV_Old_Movie_t188 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old_Movie::OnValidate()
extern "C" void CameraFilterPack_TV_Old_Movie_OnValidate_m1220 (CameraFilterPack_TV_Old_Movie_t188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old_Movie::Update()
extern "C" void CameraFilterPack_TV_Old_Movie_Update_m1221 (CameraFilterPack_TV_Old_Movie_t188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old_Movie::OnDisable()
extern "C" void CameraFilterPack_TV_Old_Movie_OnDisable_m1222 (CameraFilterPack_TV_Old_Movie_t188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
