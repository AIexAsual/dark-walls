﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_TV_Posterize
struct  CameraFilterPack_TV_Posterize_t191  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_TV_Posterize::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_TV_Posterize::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_TV_Posterize::Posterize
	float ___Posterize_4;
	// UnityEngine.Material CameraFilterPack_TV_Posterize::SCMaterial
	Material_t2 * ___SCMaterial_5;
};
struct CameraFilterPack_TV_Posterize_t191_StaticFields{
	// System.Single CameraFilterPack_TV_Posterize::ChangePosterize
	float ___ChangePosterize_6;
};
