﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.OpCodes
struct OpCodes_t1757;

// System.Void System.Reflection.Emit.OpCodes::.cctor()
extern "C" void OpCodes__cctor_m10857 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
