﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// RagdollPartScript
struct RagdollPartScript_t404;
// UnityEngine.Collision
struct Collision_t471;

// System.Void RagdollPartScript::.ctor()
extern "C" void RagdollPartScript__ctor_m2306 (RagdollPartScript_t404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RagdollPartScript::Start()
extern "C" void RagdollPartScript_Start_m2307 (RagdollPartScript_t404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RagdollPartScript::OnCollisionEnter(UnityEngine.Collision)
extern "C" void RagdollPartScript_OnCollisionEnter_m2308 (RagdollPartScript_t404 * __this, Collision_t471 * ___collision, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RagdollPartScript::Update()
extern "C" void RagdollPartScript_Update_m2309 (RagdollPartScript_t404 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
