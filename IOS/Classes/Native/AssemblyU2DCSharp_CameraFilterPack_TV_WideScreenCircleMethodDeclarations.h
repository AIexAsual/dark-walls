﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_WideScreenCircle
struct CameraFilterPack_TV_WideScreenCircle_t200;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_WideScreenCircle::.ctor()
extern "C" void CameraFilterPack_TV_WideScreenCircle__ctor_m1296 (CameraFilterPack_TV_WideScreenCircle_t200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_WideScreenCircle::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_WideScreenCircle_get_material_m1297 (CameraFilterPack_TV_WideScreenCircle_t200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenCircle::Start()
extern "C" void CameraFilterPack_TV_WideScreenCircle_Start_m1298 (CameraFilterPack_TV_WideScreenCircle_t200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenCircle::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_WideScreenCircle_OnRenderImage_m1299 (CameraFilterPack_TV_WideScreenCircle_t200 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenCircle::OnValidate()
extern "C" void CameraFilterPack_TV_WideScreenCircle_OnValidate_m1300 (CameraFilterPack_TV_WideScreenCircle_t200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenCircle::Update()
extern "C" void CameraFilterPack_TV_WideScreenCircle_Update_m1301 (CameraFilterPack_TV_WideScreenCircle_t200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenCircle::OnDisable()
extern "C" void CameraFilterPack_TV_WideScreenCircle_OnDisable_m1302 (CameraFilterPack_TV_WideScreenCircle_t200 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
