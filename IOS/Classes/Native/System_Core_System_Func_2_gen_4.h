﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ILayoutElement
struct ILayoutElement_t776;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct  Func_2_t746  : public MulticastDelegate_t219
{
};
