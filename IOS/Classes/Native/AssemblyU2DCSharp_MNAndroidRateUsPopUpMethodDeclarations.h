﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MNAndroidRateUsPopUp
struct MNAndroidRateUsPopUp_t287;
// System.String
struct String_t;

// System.Void MNAndroidRateUsPopUp::.ctor()
extern "C" void MNAndroidRateUsPopUp__ctor_m1774 (MNAndroidRateUsPopUp_t287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNAndroidRateUsPopUp MNAndroidRateUsPopUp::Create(System.String,System.String,System.String)
extern "C" MNAndroidRateUsPopUp_t287 * MNAndroidRateUsPopUp_Create_m1775 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNAndroidRateUsPopUp MNAndroidRateUsPopUp::Create(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C" MNAndroidRateUsPopUp_t287 * MNAndroidRateUsPopUp_Create_m1776 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___url, String_t* ___yes, String_t* ___later, String_t* ___no, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidRateUsPopUp::init()
extern "C" void MNAndroidRateUsPopUp_init_m1777 (MNAndroidRateUsPopUp_t287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidRateUsPopUp::onPopUpCallBack(System.String)
extern "C" void MNAndroidRateUsPopUp_onPopUpCallBack_m1778 (MNAndroidRateUsPopUp_t287 * __this, String_t* ___buttonIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
