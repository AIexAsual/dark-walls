﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>
struct Enumerator_t2760;
// System.Object
struct Object_t;
// UnityEngine.Networking.Types.NetworkAccessToken
struct NetworkAccessToken_t981;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>
struct Dictionary_2_t982;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__18MethodDeclarations.h"
#define Enumerator__ctor_m20998(__this, ___dictionary, method) (( void (*) (Enumerator_t2760 *, Dictionary_2_t982 *, const MethodInfo*))Enumerator__ctor_m20899_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20999(__this, method) (( Object_t * (*) (Enumerator_t2760 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20900_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21000(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2760 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20901_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21001(__this, method) (( Object_t * (*) (Enumerator_t2760 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20902_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21002(__this, method) (( Object_t * (*) (Enumerator_t2760 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20903_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::MoveNext()
#define Enumerator_MoveNext_m21003(__this, method) (( bool (*) (Enumerator_t2760 *, const MethodInfo*))Enumerator_MoveNext_m20904_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_Current()
#define Enumerator_get_Current_m21004(__this, method) (( KeyValuePair_2_t2757  (*) (Enumerator_t2760 *, const MethodInfo*))Enumerator_get_Current_m20905_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m21005(__this, method) (( uint64_t (*) (Enumerator_t2760 *, const MethodInfo*))Enumerator_get_CurrentKey_m20906_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m21006(__this, method) (( NetworkAccessToken_t981 * (*) (Enumerator_t2760 *, const MethodInfo*))Enumerator_get_CurrentValue_m20907_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::VerifyState()
#define Enumerator_VerifyState_m21007(__this, method) (( void (*) (Enumerator_t2760 *, const MethodInfo*))Enumerator_VerifyState_m20908_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m21008(__this, method) (( void (*) (Enumerator_t2760 *, const MethodInfo*))Enumerator_VerifyCurrent_m20909_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::Dispose()
#define Enumerator_Dispose_m21009(__this, method) (( void (*) (Enumerator_t2760 *, const MethodInfo*))Enumerator_Dispose_m20910_gshared)(__this, method)
