﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Cardboard/<EndOfFrame>c__Iterator1
struct U3CEndOfFrameU3Ec__Iterator1_t234;
// System.Object
struct Object_t;

// System.Void Cardboard/<EndOfFrame>c__Iterator1::.ctor()
extern "C" void U3CEndOfFrameU3Ec__Iterator1__ctor_m1422 (U3CEndOfFrameU3Ec__Iterator1_t234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Cardboard/<EndOfFrame>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CEndOfFrameU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1423 (U3CEndOfFrameU3Ec__Iterator1_t234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Cardboard/<EndOfFrame>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CEndOfFrameU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1424 (U3CEndOfFrameU3Ec__Iterator1_t234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Cardboard/<EndOfFrame>c__Iterator1::MoveNext()
extern "C" bool U3CEndOfFrameU3Ec__Iterator1_MoveNext_m1425 (U3CEndOfFrameU3Ec__Iterator1_t234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cardboard/<EndOfFrame>c__Iterator1::Dispose()
extern "C" void U3CEndOfFrameU3Ec__Iterator1_Dispose_m1426 (U3CEndOfFrameU3Ec__Iterator1_t234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cardboard/<EndOfFrame>c__Iterator1::Reset()
extern "C" void U3CEndOfFrameU3Ec__Iterator1_Reset_m1427 (U3CEndOfFrameU3Ec__Iterator1_t234 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
