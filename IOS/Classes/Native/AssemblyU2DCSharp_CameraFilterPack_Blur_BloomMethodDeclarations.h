﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blur_Bloom
struct CameraFilterPack_Blur_Bloom_t47;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blur_Bloom::.ctor()
extern "C" void CameraFilterPack_Blur_Bloom__ctor_m282 (CameraFilterPack_Blur_Bloom_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Bloom::get_material()
extern "C" Material_t2 * CameraFilterPack_Blur_Bloom_get_material_m283 (CameraFilterPack_Blur_Bloom_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Bloom::Start()
extern "C" void CameraFilterPack_Blur_Bloom_Start_m284 (CameraFilterPack_Blur_Bloom_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Bloom::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blur_Bloom_OnRenderImage_m285 (CameraFilterPack_Blur_Bloom_t47 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Bloom::OnValidate()
extern "C" void CameraFilterPack_Blur_Bloom_OnValidate_m286 (CameraFilterPack_Blur_Bloom_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Bloom::Update()
extern "C" void CameraFilterPack_Blur_Bloom_Update_m287 (CameraFilterPack_Blur_Bloom_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Bloom::OnDisable()
extern "C" void CameraFilterPack_Blur_Bloom_OnDisable_m288 (CameraFilterPack_Blur_Bloom_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
