﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<System.Object>
struct Comparer_1_t2234;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.Comparer`1<System.Object>::.ctor()
extern "C" void Comparer_1__ctor_m13675_gshared (Comparer_1_t2234 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m13675(__this, method) (( void (*) (Comparer_1_t2234 *, const MethodInfo*))Comparer_1__ctor_m13675_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<System.Object>::.cctor()
extern "C" void Comparer_1__cctor_m13676_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m13676(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m13676_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.Object>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m13677_gshared (Comparer_1_t2234 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m13677(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t2234 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m13677_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Object>::get_Default()
extern "C" Comparer_1_t2234 * Comparer_1_get_Default_m13678_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m13678(__this /* static, unused */, method) (( Comparer_1_t2234 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m13678_gshared)(__this /* static, unused */, method)
