﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// iTweenPath
struct iTweenPath_t459;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t317;
// System.String
struct String_t;

// System.Void iTweenPath::.ctor()
extern "C" void iTweenPath__ctor_m2707 (iTweenPath_t459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenPath::.cctor()
extern "C" void iTweenPath__cctor_m2708 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenPath::OnEnable()
extern "C" void iTweenPath_OnEnable_m2709 (iTweenPath_t459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenPath::OnDrawGizmosSelected()
extern "C" void iTweenPath_OnDrawGizmosSelected_m2710 (iTweenPath_t459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] iTweenPath::GetPath(System.String)
extern "C" Vector3U5BU5D_t317* iTweenPath_GetPath_m2711 (Object_t * __this /* static, unused */, String_t* ___requestedName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
