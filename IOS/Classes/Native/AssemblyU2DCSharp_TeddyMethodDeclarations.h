﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Teddy
struct Teddy_t367;

// System.Void Teddy::.ctor()
extern "C" void Teddy__ctor_m2347 (Teddy_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Teddy::Start()
extern "C" void Teddy_Start_m2348 (Teddy_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Teddy::Update()
extern "C" void Teddy_Update_m2349 (Teddy_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Teddy::TeddyLook()
extern "C" void Teddy_TeddyLook_m2350 (Teddy_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Teddy::DoneLook()
extern "C" void Teddy_DoneLook_m2351 (Teddy_t367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
