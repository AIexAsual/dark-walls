﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Colors_DarkColor
struct CameraFilterPack_Colors_DarkColor_t75;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Colors_DarkColor::.ctor()
extern "C" void CameraFilterPack_Colors_DarkColor__ctor_m461 (CameraFilterPack_Colors_DarkColor_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_DarkColor::get_material()
extern "C" Material_t2 * CameraFilterPack_Colors_DarkColor_get_material_m462 (CameraFilterPack_Colors_DarkColor_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_DarkColor::Start()
extern "C" void CameraFilterPack_Colors_DarkColor_Start_m463 (CameraFilterPack_Colors_DarkColor_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_DarkColor::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Colors_DarkColor_OnRenderImage_m464 (CameraFilterPack_Colors_DarkColor_t75 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_DarkColor::OnValidate()
extern "C" void CameraFilterPack_Colors_DarkColor_OnValidate_m465 (CameraFilterPack_Colors_DarkColor_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_DarkColor::Update()
extern "C" void CameraFilterPack_Colors_DarkColor_Update_m466 (CameraFilterPack_Colors_DarkColor_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_DarkColor::OnDisable()
extern "C" void CameraFilterPack_Colors_DarkColor_OnDisable_m467 (CameraFilterPack_Colors_DarkColor_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
