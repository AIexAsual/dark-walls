﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Nullable`1<System.TimeSpan>
struct Nullable_1_t2187;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen_0.h"

// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C" void Nullable_1__ctor_m13467_gshared (Nullable_1_t2187 * __this, TimeSpan_t1437  ___value, const MethodInfo* method);
#define Nullable_1__ctor_m13467(__this, ___value, method) (( void (*) (Nullable_1_t2187 *, TimeSpan_t1437 , const MethodInfo*))Nullable_1__ctor_m13467_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m13468_gshared (Nullable_1_t2187 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m13468(__this, method) (( bool (*) (Nullable_1_t2187 *, const MethodInfo*))Nullable_1_get_HasValue_m13468_gshared)(__this, method)
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern "C" TimeSpan_t1437  Nullable_1_get_Value_m13469_gshared (Nullable_1_t2187 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m13469(__this, method) (( TimeSpan_t1437  (*) (Nullable_1_t2187 *, const MethodInfo*))Nullable_1_get_Value_m13469_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m23019_gshared (Nullable_1_t2187 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m23019(__this, ___other, method) (( bool (*) (Nullable_1_t2187 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m23019_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m23020_gshared (Nullable_1_t2187 * __this, Nullable_1_t2187  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m23020(__this, ___other, method) (( bool (*) (Nullable_1_t2187 *, Nullable_1_t2187 , const MethodInfo*))Nullable_1_Equals_m23020_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m23021_gshared (Nullable_1_t2187 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m23021(__this, method) (( int32_t (*) (Nullable_1_t2187 *, const MethodInfo*))Nullable_1_GetHashCode_m23021_gshared)(__this, method)
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern "C" String_t* Nullable_1_ToString_m23022_gshared (Nullable_1_t2187 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m23022(__this, method) (( String_t* (*) (Nullable_1_t2187 *, const MethodInfo*))Nullable_1_ToString_m23022_gshared)(__this, method)
