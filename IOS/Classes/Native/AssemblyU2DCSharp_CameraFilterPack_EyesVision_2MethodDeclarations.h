﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_EyesVision_2
struct CameraFilterPack_EyesVision_2_t119;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_EyesVision_2::.ctor()
extern "C" void CameraFilterPack_EyesVision_2__ctor_m764 (CameraFilterPack_EyesVision_2_t119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_EyesVision_2::get_material()
extern "C" Material_t2 * CameraFilterPack_EyesVision_2_get_material_m765 (CameraFilterPack_EyesVision_2_t119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EyesVision_2::Start()
extern "C" void CameraFilterPack_EyesVision_2_Start_m766 (CameraFilterPack_EyesVision_2_t119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EyesVision_2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_EyesVision_2_OnRenderImage_m767 (CameraFilterPack_EyesVision_2_t119 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EyesVision_2::Update()
extern "C" void CameraFilterPack_EyesVision_2_Update_m768 (CameraFilterPack_EyesVision_2_t119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EyesVision_2::OnDisable()
extern "C" void CameraFilterPack_EyesVision_2_OnDisable_m769 (CameraFilterPack_EyesVision_2_t119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
