﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// System.String
struct String_t;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Gradients_ElectricGradient
struct  CameraFilterPack_Gradients_ElectricGradient_t147  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Gradients_ElectricGradient::SCShader
	Shader_t1 * ___SCShader_2;
	// System.String CameraFilterPack_Gradients_ElectricGradient::ShaderName
	String_t* ___ShaderName_3;
	// System.Single CameraFilterPack_Gradients_ElectricGradient::TimeX
	float ___TimeX_4;
	// UnityEngine.Vector4 CameraFilterPack_Gradients_ElectricGradient::ScreenResolution
	Vector4_t5  ___ScreenResolution_5;
	// UnityEngine.Material CameraFilterPack_Gradients_ElectricGradient::SCMaterial
	Material_t2 * ___SCMaterial_6;
	// System.Single CameraFilterPack_Gradients_ElectricGradient::Switch
	float ___Switch_7;
	// System.Single CameraFilterPack_Gradients_ElectricGradient::Fade
	float ___Fade_8;
};
