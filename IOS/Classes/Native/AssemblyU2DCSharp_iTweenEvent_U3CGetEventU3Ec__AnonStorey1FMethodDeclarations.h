﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// iTweenEvent/<GetEvent>c__AnonStorey1F
struct U3CGetEventU3Ec__AnonStorey1F_t447;
// iTweenEvent
struct iTweenEvent_t443;

// System.Void iTweenEvent/<GetEvent>c__AnonStorey1F::.ctor()
extern "C" void U3CGetEventU3Ec__AnonStorey1F__ctor_m2694 (U3CGetEventU3Ec__AnonStorey1F_t447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenEvent/<GetEvent>c__AnonStorey1F::<>m__9(iTweenEvent)
extern "C" bool U3CGetEventU3Ec__AnonStorey1F_U3CU3Em__9_m2695 (U3CGetEventU3Ec__AnonStorey1F_t447 * __this, iTweenEvent_t443 * ___tween, const MethodInfo* method) IL2CPP_METHOD_ATTR;
