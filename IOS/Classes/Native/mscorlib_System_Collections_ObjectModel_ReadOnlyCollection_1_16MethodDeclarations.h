﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>
struct ReadOnlyCollection_1_t2471;
// UnityEngine.AudioClip
struct AudioClip_t472;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<UnityEngine.AudioClip>
struct IList_1_t573;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t338;
// System.Collections.Generic.IEnumerator`1<UnityEngine.AudioClip>
struct IEnumerator_1_t3049;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m16782(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2471 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m13592_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m16783(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2471 *, AudioClip_t472 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m13593_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m16784(__this, method) (( void (*) (ReadOnlyCollection_1_t2471 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m13594_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m16785(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2471 *, int32_t, AudioClip_t472 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m13595_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m16786(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2471 *, AudioClip_t472 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m13596_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m16787(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2471 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m13597_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m16788(__this, ___index, method) (( AudioClip_t472 * (*) (ReadOnlyCollection_1_t2471 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m13598_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m16789(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2471 *, int32_t, AudioClip_t472 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m13599_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16790(__this, method) (( bool (*) (ReadOnlyCollection_1_t2471 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13600_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m16791(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2471 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m13601_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m16792(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2471 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m13602_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m16793(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2471 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m13603_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m16794(__this, method) (( void (*) (ReadOnlyCollection_1_t2471 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m13604_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m16795(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2471 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m13605_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m16796(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2471 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m13606_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m16797(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2471 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m13607_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m16798(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2471 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m13608_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m16799(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2471 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m13609_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m16800(__this, method) (( bool (*) (ReadOnlyCollection_1_t2471 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m13610_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m16801(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2471 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m13611_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m16802(__this, method) (( bool (*) (ReadOnlyCollection_1_t2471 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m13612_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m16803(__this, method) (( bool (*) (ReadOnlyCollection_1_t2471 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m13613_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m16804(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2471 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m13614_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m16805(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2471 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m13615_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::Contains(T)
#define ReadOnlyCollection_1_Contains_m16806(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2471 *, AudioClip_t472 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m13616_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m16807(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2471 *, AudioClipU5BU5D_t338*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m13617_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m16808(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2471 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m13618_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m16809(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2471 *, AudioClip_t472 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m13619_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::get_Count()
#define ReadOnlyCollection_1_get_Count_m16810(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2471 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m13620_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m16811(__this, ___index, method) (( AudioClip_t472 * (*) (ReadOnlyCollection_1_t2471 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m13621_gshared)(__this, ___index, method)
