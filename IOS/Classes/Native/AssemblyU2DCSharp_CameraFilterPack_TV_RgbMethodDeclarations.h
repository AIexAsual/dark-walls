﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_Rgb
struct CameraFilterPack_TV_Rgb_t192;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_Rgb::.ctor()
extern "C" void CameraFilterPack_TV_Rgb__ctor_m1243 (CameraFilterPack_TV_Rgb_t192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Rgb::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_Rgb_get_material_m1244 (CameraFilterPack_TV_Rgb_t192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Rgb::Start()
extern "C" void CameraFilterPack_TV_Rgb_Start_m1245 (CameraFilterPack_TV_Rgb_t192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Rgb::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_Rgb_OnRenderImage_m1246 (CameraFilterPack_TV_Rgb_t192 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Rgb::Update()
extern "C" void CameraFilterPack_TV_Rgb_Update_m1247 (CameraFilterPack_TV_Rgb_t192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Rgb::OnDisable()
extern "C" void CameraFilterPack_TV_Rgb_OnDisable_m1248 (CameraFilterPack_TV_Rgb_t192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
