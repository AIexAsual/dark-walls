﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// EventParamMappings
struct EventParamMappings_t421;

// System.Void EventParamMappings::.ctor()
extern "C" void EventParamMappings__ctor_m2398 (EventParamMappings_t421 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventParamMappings::.cctor()
extern "C" void EventParamMappings__cctor_m2399 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
