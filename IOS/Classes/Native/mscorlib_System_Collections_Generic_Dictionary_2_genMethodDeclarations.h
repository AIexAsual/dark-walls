﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct Dictionary_2_t420;
// System.Collections.Generic.ICollection`1<iTweenEvent/TweenType>
struct ICollection_1_t2991;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct ICollection_1_t2992;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t545;
// System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct KeyCollection_t2344;
// System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct ValueCollection_t2345;
// System.Collections.Generic.IEqualityComparer`1<iTweenEvent/TweenType>
struct IEqualityComparer_1_t2318;
// System.Collections.Generic.IDictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct IDictionary_2_t2993;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1104;
// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>[]
struct KeyValuePair_2U5BU5D_t2994;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>>
struct IEnumerator_1_t2995;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1527;
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
// System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::.ctor()
// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_17MethodDeclarations.h"
#define Dictionary_2__ctor_m3287(__this, method) (( void (*) (Dictionary_2_t420 *, const MethodInfo*))Dictionary_2__ctor_m14631_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m14632(__this, ___comparer, method) (( void (*) (Dictionary_2_t420 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14633_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m14634(__this, ___dictionary, method) (( void (*) (Dictionary_2_t420 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14635_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::.ctor(System.Int32)
#define Dictionary_2__ctor_m14636(__this, ___capacity, method) (( void (*) (Dictionary_2_t420 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m14637_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m14638(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t420 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14639_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m14640(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t420 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2__ctor_m14641_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m14642(__this, method) (( Object_t* (*) (Dictionary_2_t420 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m14643_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m14644(__this, method) (( Object_t* (*) (Dictionary_2_t420 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m14645_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m14646(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t420 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m14647_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m14648(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t420 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m14649_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m14650(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t420 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m14651_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m14652(__this, ___key, method) (( bool (*) (Dictionary_2_t420 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m14653_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m14654(__this, ___key, method) (( void (*) (Dictionary_2_t420 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m14655_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14656(__this, method) (( bool (*) (Dictionary_2_t420 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14657_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14658(__this, method) (( Object_t * (*) (Dictionary_2_t420 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14659_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14660(__this, method) (( bool (*) (Dictionary_2_t420 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14661_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14662(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t420 *, KeyValuePair_2_t2343 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14663_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14664(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t420 *, KeyValuePair_2_t2343 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14665_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14666(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t420 *, KeyValuePair_2U5BU5D_t2994*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14667_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14668(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t420 *, KeyValuePair_2_t2343 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14669_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m14670(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t420 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m14671_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14672(__this, method) (( Object_t * (*) (Dictionary_2_t420 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14673_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14674(__this, method) (( Object_t* (*) (Dictionary_2_t420 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14675_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14676(__this, method) (( Object_t * (*) (Dictionary_2_t420 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14677_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::get_Count()
#define Dictionary_2_get_Count_m14678(__this, method) (( int32_t (*) (Dictionary_2_t420 *, const MethodInfo*))Dictionary_2_get_Count_m14679_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::get_Item(TKey)
#define Dictionary_2_get_Item_m14680(__this, ___key, method) (( Dictionary_2_t545 * (*) (Dictionary_2_t420 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m14681_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m14682(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t420 *, int32_t, Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_set_Item_m14683_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m14684(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t420 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m14685_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m14686(__this, ___size, method) (( void (*) (Dictionary_2_t420 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m14687_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m14688(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t420 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m14689_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m14690(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2343  (*) (Object_t * /* static, unused */, int32_t, Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_make_pair_m14691_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m14692(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_pick_key_m14693_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m14694(__this /* static, unused */, ___key, ___value, method) (( Dictionary_2_t545 * (*) (Object_t * /* static, unused */, int32_t, Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_pick_value_m14695_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m14696(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t420 *, KeyValuePair_2U5BU5D_t2994*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m14697_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::Resize()
#define Dictionary_2_Resize_m14698(__this, method) (( void (*) (Dictionary_2_t420 *, const MethodInfo*))Dictionary_2_Resize_m14699_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::Add(TKey,TValue)
#define Dictionary_2_Add_m14700(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t420 *, int32_t, Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_Add_m14701_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::Clear()
#define Dictionary_2_Clear_m14702(__this, method) (( void (*) (Dictionary_2_t420 *, const MethodInfo*))Dictionary_2_Clear_m14703_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m14704(__this, ___key, method) (( bool (*) (Dictionary_2_t420 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m14705_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m14706(__this, ___value, method) (( bool (*) (Dictionary_2_t420 *, Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_ContainsValue_m14707_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m14708(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t420 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2_GetObjectData_m14709_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m14710(__this, ___sender, method) (( void (*) (Dictionary_2_t420 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m14711_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::Remove(TKey)
#define Dictionary_2_Remove_m14712(__this, ___key, method) (( bool (*) (Dictionary_2_t420 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m14713_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m14714(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t420 *, int32_t, Dictionary_2_t545 **, const MethodInfo*))Dictionary_2_TryGetValue_m14715_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::get_Keys()
#define Dictionary_2_get_Keys_m14716(__this, method) (( KeyCollection_t2344 * (*) (Dictionary_2_t420 *, const MethodInfo*))Dictionary_2_get_Keys_m14717_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::get_Values()
#define Dictionary_2_get_Values_m14718(__this, method) (( ValueCollection_t2345 * (*) (Dictionary_2_t420 *, const MethodInfo*))Dictionary_2_get_Values_m14719_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m14720(__this, ___key, method) (( int32_t (*) (Dictionary_2_t420 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m14721_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m14722(__this, ___value, method) (( Dictionary_2_t545 * (*) (Dictionary_2_t420 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m14723_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m14724(__this, ___pair, method) (( bool (*) (Dictionary_2_t420 *, KeyValuePair_2_t2343 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m14725_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m14726(__this, method) (( Enumerator_t2346  (*) (Dictionary_2_t420 *, const MethodInfo*))Dictionary_2_GetEnumerator_m14727_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m14728(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t552  (*) (Object_t * /* static, unused */, int32_t, Dictionary_2_t545 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m14729_gshared)(__this /* static, unused */, ___key, ___value, method)
