﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color>
struct DefaultComparer_t2435;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color>::.ctor()
extern "C" void DefaultComparer__ctor_m16199_gshared (DefaultComparer_t2435 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m16199(__this, method) (( void (*) (DefaultComparer_t2435 *, const MethodInfo*))DefaultComparer__ctor_m16199_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m16200_gshared (DefaultComparer_t2435 * __this, Color_t6  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m16200(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2435 *, Color_t6 , const MethodInfo*))DefaultComparer_GetHashCode_m16200_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m16201_gshared (DefaultComparer_t2435 * __this, Color_t6  ___x, Color_t6  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m16201(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2435 *, Color_t6 , Color_t6 , const MethodInfo*))DefaultComparer_Equals_m16201_gshared)(__this, ___x, ___y, method)
