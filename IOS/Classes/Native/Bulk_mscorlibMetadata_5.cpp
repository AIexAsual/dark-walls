﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.Remoting.FormatterData
#include "mscorlib_System_Runtime_Remoting_FormatterData.h"
// Metadata Definition System.Runtime.Remoting.FormatterData
extern TypeInfo FormatterData_t1915_il2cpp_TypeInfo;
// System.Runtime.Remoting.FormatterData
#include "mscorlib_System_Runtime_Remoting_FormatterDataMethodDeclarations.h"
static const EncodedMethodIndex FormatterData_t1915_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterData_t1915_0_0_0;
extern const Il2CppType FormatterData_t1915_1_0_0;
extern const Il2CppType ProviderData_t1914_0_0_0;
struct FormatterData_t1915;
const Il2CppTypeDefinitionMetadata FormatterData_t1915_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ProviderData_t1914_0_0_0/* parent */
	, FormatterData_t1915_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11931/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FormatterData_t1915_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterData"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &FormatterData_t1915_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FormatterData_t1915_0_0_0/* byval_arg */
	, &FormatterData_t1915_1_0_0/* this_arg */
	, &FormatterData_t1915_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterData_t1915)/* instance_size */
	, sizeof (FormatterData_t1915)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingException
#include "mscorlib_System_Runtime_Remoting_RemotingException.h"
// Metadata Definition System.Runtime.Remoting.RemotingException
extern TypeInfo RemotingException_t1916_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingException
#include "mscorlib_System_Runtime_Remoting_RemotingExceptionMethodDeclarations.h"
static const EncodedMethodIndex RemotingException_t1916_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
extern const Il2CppType ISerializable_t2210_0_0_0;
extern const Il2CppType _Exception_t3443_0_0_0;
static Il2CppInterfaceOffsetPair RemotingException_t1916_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingException_t1916_0_0_0;
extern const Il2CppType RemotingException_t1916_1_0_0;
extern const Il2CppType SystemException_t1536_0_0_0;
struct RemotingException_t1916;
const Il2CppTypeDefinitionMetadata RemotingException_t1916_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RemotingException_t1916_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, RemotingException_t1916_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11932/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RemotingException_t1916_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingException"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &RemotingException_t1916_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3003/* custom_attributes_cache */
	, &RemotingException_t1916_0_0_0/* byval_arg */
	, &RemotingException_t1916_1_0_0/* this_arg */
	, &RemotingException_t1916_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingException_t1916)/* instance_size */
	, sizeof (RemotingException_t1916)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingServices
#include "mscorlib_System_Runtime_Remoting_RemotingServices.h"
// Metadata Definition System.Runtime.Remoting.RemotingServices
extern TypeInfo RemotingServices_t1918_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingServices
#include "mscorlib_System_Runtime_Remoting_RemotingServicesMethodDeclarations.h"
static const EncodedMethodIndex RemotingServices_t1918_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingServices_t1918_0_0_0;
extern const Il2CppType RemotingServices_t1918_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct RemotingServices_t1918;
const Il2CppTypeDefinitionMetadata RemotingServices_t1918_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingServices_t1918_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8386/* fieldStart */
	, 11936/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RemotingServices_t1918_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingServices"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &RemotingServices_t1918_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3004/* custom_attributes_cache */
	, &RemotingServices_t1918_0_0_0/* byval_arg */
	, &RemotingServices_t1918_1_0_0/* this_arg */
	, &RemotingServices_t1918_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingServices_t1918)/* instance_size */
	, sizeof (RemotingServices_t1918)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingServices_t1918_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ServerIdentity
#include "mscorlib_System_Runtime_Remoting_ServerIdentity.h"
// Metadata Definition System.Runtime.Remoting.ServerIdentity
extern TypeInfo ServerIdentity_t1572_il2cpp_TypeInfo;
// System.Runtime.Remoting.ServerIdentity
#include "mscorlib_System_Runtime_Remoting_ServerIdentityMethodDeclarations.h"
static const EncodedMethodIndex ServerIdentity_t1572_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3891,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ServerIdentity_t1572_0_0_0;
extern const Il2CppType ServerIdentity_t1572_1_0_0;
extern const Il2CppType Identity_t1896_0_0_0;
struct ServerIdentity_t1572;
const Il2CppTypeDefinitionMetadata ServerIdentity_t1572_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Identity_t1896_0_0_0/* parent */
	, ServerIdentity_t1572_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8394/* fieldStart */
	, 11960/* methodStart */
	, -1/* eventStart */
	, 2413/* propertyStart */

};
TypeInfo ServerIdentity_t1572_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ServerIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ServerIdentity_t1572_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ServerIdentity_t1572_0_0_0/* byval_arg */
	, &ServerIdentity_t1572_1_0_0/* this_arg */
	, &ServerIdentity_t1572_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ServerIdentity_t1572)/* instance_size */
	, sizeof (ServerIdentity_t1572)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ClientActivatedIdentity
#include "mscorlib_System_Runtime_Remoting_ClientActivatedIdentity.h"
// Metadata Definition System.Runtime.Remoting.ClientActivatedIdentity
extern TypeInfo ClientActivatedIdentity_t1919_il2cpp_TypeInfo;
// System.Runtime.Remoting.ClientActivatedIdentity
#include "mscorlib_System_Runtime_Remoting_ClientActivatedIdentityMethodDeclarations.h"
static const EncodedMethodIndex ClientActivatedIdentity_t1919_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3891,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClientActivatedIdentity_t1919_0_0_0;
extern const Il2CppType ClientActivatedIdentity_t1919_1_0_0;
struct ClientActivatedIdentity_t1919;
const Il2CppTypeDefinitionMetadata ClientActivatedIdentity_t1919_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t1572_0_0_0/* parent */
	, ClientActivatedIdentity_t1919_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11963/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ClientActivatedIdentity_t1919_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientActivatedIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ClientActivatedIdentity_t1919_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientActivatedIdentity_t1919_0_0_0/* byval_arg */
	, &ClientActivatedIdentity_t1919_1_0_0/* this_arg */
	, &ClientActivatedIdentity_t1919_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientActivatedIdentity_t1919)/* instance_size */
	, sizeof (ClientActivatedIdentity_t1919)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.SingletonIdentity
#include "mscorlib_System_Runtime_Remoting_SingletonIdentity.h"
// Metadata Definition System.Runtime.Remoting.SingletonIdentity
extern TypeInfo SingletonIdentity_t1920_il2cpp_TypeInfo;
// System.Runtime.Remoting.SingletonIdentity
#include "mscorlib_System_Runtime_Remoting_SingletonIdentityMethodDeclarations.h"
static const EncodedMethodIndex SingletonIdentity_t1920_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3891,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SingletonIdentity_t1920_0_0_0;
extern const Il2CppType SingletonIdentity_t1920_1_0_0;
struct SingletonIdentity_t1920;
const Il2CppTypeDefinitionMetadata SingletonIdentity_t1920_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t1572_0_0_0/* parent */
	, SingletonIdentity_t1920_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11964/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SingletonIdentity_t1920_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SingletonIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &SingletonIdentity_t1920_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SingletonIdentity_t1920_0_0_0/* byval_arg */
	, &SingletonIdentity_t1920_1_0_0/* this_arg */
	, &SingletonIdentity_t1920_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SingletonIdentity_t1920)/* instance_size */
	, sizeof (SingletonIdentity_t1920)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.SingleCallIdentity
#include "mscorlib_System_Runtime_Remoting_SingleCallIdentity.h"
// Metadata Definition System.Runtime.Remoting.SingleCallIdentity
extern TypeInfo SingleCallIdentity_t1921_il2cpp_TypeInfo;
// System.Runtime.Remoting.SingleCallIdentity
#include "mscorlib_System_Runtime_Remoting_SingleCallIdentityMethodDeclarations.h"
static const EncodedMethodIndex SingleCallIdentity_t1921_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3891,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SingleCallIdentity_t1921_0_0_0;
extern const Il2CppType SingleCallIdentity_t1921_1_0_0;
struct SingleCallIdentity_t1921;
const Il2CppTypeDefinitionMetadata SingleCallIdentity_t1921_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ServerIdentity_t1572_0_0_0/* parent */
	, SingleCallIdentity_t1921_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11965/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SingleCallIdentity_t1921_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SingleCallIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &SingleCallIdentity_t1921_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SingleCallIdentity_t1921_0_0_0/* byval_arg */
	, &SingleCallIdentity_t1921_1_0_0/* this_arg */
	, &SingleCallIdentity_t1921_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SingleCallIdentity_t1921)/* instance_size */
	, sizeof (SingleCallIdentity_t1921)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.SoapServices
#include "mscorlib_System_Runtime_Remoting_SoapServices.h"
// Metadata Definition System.Runtime.Remoting.SoapServices
extern TypeInfo SoapServices_t1923_il2cpp_TypeInfo;
// System.Runtime.Remoting.SoapServices
#include "mscorlib_System_Runtime_Remoting_SoapServicesMethodDeclarations.h"
extern const Il2CppType TypeInfo_t1922_0_0_0;
static const Il2CppType* SoapServices_t1923_il2cpp_TypeInfo__nestedTypes[1] =
{
	&TypeInfo_t1922_0_0_0,
};
static const EncodedMethodIndex SoapServices_t1923_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SoapServices_t1923_0_0_0;
extern const Il2CppType SoapServices_t1923_1_0_0;
struct SoapServices_t1923;
const Il2CppTypeDefinitionMetadata SoapServices_t1923_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SoapServices_t1923_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SoapServices_t1923_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8397/* fieldStart */
	, 11966/* methodStart */
	, -1/* eventStart */
	, 2414/* propertyStart */

};
TypeInfo SoapServices_t1923_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SoapServices"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &SoapServices_t1923_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3007/* custom_attributes_cache */
	, &SoapServices_t1923_0_0_0/* byval_arg */
	, &SoapServices_t1923_1_0_0/* this_arg */
	, &SoapServices_t1923_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SoapServices_t1923)/* instance_size */
	, sizeof (SoapServices_t1923)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SoapServices_t1923_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 3/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.SoapServices/TypeInfo
#include "mscorlib_System_Runtime_Remoting_SoapServices_TypeInfo.h"
// Metadata Definition System.Runtime.Remoting.SoapServices/TypeInfo
extern TypeInfo TypeInfo_t1922_il2cpp_TypeInfo;
// System.Runtime.Remoting.SoapServices/TypeInfo
#include "mscorlib_System_Runtime_Remoting_SoapServices_TypeInfoMethodDeclarations.h"
static const EncodedMethodIndex TypeInfo_t1922_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeInfo_t1922_1_0_0;
struct TypeInfo_t1922;
const Il2CppTypeDefinitionMetadata TypeInfo_t1922_DefinitionMetadata = 
{
	&SoapServices_t1923_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeInfo_t1922_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8402/* fieldStart */
	, 11982/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeInfo_t1922_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &TypeInfo_t1922_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeInfo_t1922_0_0_0/* byval_arg */
	, &TypeInfo_t1922_1_0_0/* this_arg */
	, &TypeInfo_t1922_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInfo_t1922)/* instance_size */
	, sizeof (TypeInfo_t1922)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.TypeEntry
#include "mscorlib_System_Runtime_Remoting_TypeEntry.h"
// Metadata Definition System.Runtime.Remoting.TypeEntry
extern TypeInfo TypeEntry_t1901_il2cpp_TypeInfo;
// System.Runtime.Remoting.TypeEntry
#include "mscorlib_System_Runtime_Remoting_TypeEntryMethodDeclarations.h"
static const EncodedMethodIndex TypeEntry_t1901_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeEntry_t1901_0_0_0;
extern const Il2CppType TypeEntry_t1901_1_0_0;
struct TypeEntry_t1901;
const Il2CppTypeDefinitionMetadata TypeEntry_t1901_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeEntry_t1901_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8404/* fieldStart */
	, 11983/* methodStart */
	, -1/* eventStart */
	, 2417/* propertyStart */

};
TypeInfo TypeEntry_t1901_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &TypeEntry_t1901_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3008/* custom_attributes_cache */
	, &TypeEntry_t1901_0_0_0/* byval_arg */
	, &TypeEntry_t1901_1_0_0/* this_arg */
	, &TypeEntry_t1901_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeEntry_t1901)/* instance_size */
	, sizeof (TypeEntry_t1901)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.TypeInfo
#include "mscorlib_System_Runtime_Remoting_TypeInfo.h"
// Metadata Definition System.Runtime.Remoting.TypeInfo
extern TypeInfo TypeInfo_t1924_il2cpp_TypeInfo;
// System.Runtime.Remoting.TypeInfo
#include "mscorlib_System_Runtime_Remoting_TypeInfoMethodDeclarations.h"
static const EncodedMethodIndex TypeInfo_t1924_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3892,
};
extern const Il2CppType IRemotingTypeInfo_t1909_0_0_0;
static const Il2CppType* TypeInfo_t1924_InterfacesTypeInfos[] = 
{
	&IRemotingTypeInfo_t1909_0_0_0,
};
static Il2CppInterfaceOffsetPair TypeInfo_t1924_InterfacesOffsets[] = 
{
	{ &IRemotingTypeInfo_t1909_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeInfo_t1924_0_0_0;
extern const Il2CppType TypeInfo_t1924_1_0_0;
struct TypeInfo_t1924;
const Il2CppTypeDefinitionMetadata TypeInfo_t1924_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, TypeInfo_t1924_InterfacesTypeInfos/* implementedInterfaces */
	, TypeInfo_t1924_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeInfo_t1924_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8406/* fieldStart */
	, 11988/* methodStart */
	, -1/* eventStart */
	, 2419/* propertyStart */

};
TypeInfo TypeInfo_t1924_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &TypeInfo_t1924_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeInfo_t1924_0_0_0/* byval_arg */
	, &TypeInfo_t1924_1_0_0/* this_arg */
	, &TypeInfo_t1924_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInfo_t1924)/* instance_size */
	, sizeof (TypeInfo_t1924)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.WellKnownClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_WellKnownClientTypeEntry.h"
// Metadata Definition System.Runtime.Remoting.WellKnownClientTypeEntry
extern TypeInfo WellKnownClientTypeEntry_t1925_il2cpp_TypeInfo;
// System.Runtime.Remoting.WellKnownClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_WellKnownClientTypeEntryMethodDeclarations.h"
static const EncodedMethodIndex WellKnownClientTypeEntry_t1925_VTable[4] = 
{
	626,
	601,
	627,
	3893,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WellKnownClientTypeEntry_t1925_0_0_0;
extern const Il2CppType WellKnownClientTypeEntry_t1925_1_0_0;
struct WellKnownClientTypeEntry_t1925;
const Il2CppTypeDefinitionMetadata WellKnownClientTypeEntry_t1925_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeEntry_t1901_0_0_0/* parent */
	, WellKnownClientTypeEntry_t1925_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8409/* fieldStart */
	, 11990/* methodStart */
	, -1/* eventStart */
	, 2420/* propertyStart */

};
TypeInfo WellKnownClientTypeEntry_t1925_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WellKnownClientTypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &WellKnownClientTypeEntry_t1925_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3009/* custom_attributes_cache */
	, &WellKnownClientTypeEntry_t1925_0_0_0/* byval_arg */
	, &WellKnownClientTypeEntry_t1925_1_0_0/* this_arg */
	, &WellKnownClientTypeEntry_t1925_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WellKnownClientTypeEntry_t1925)/* instance_size */
	, sizeof (WellKnownClientTypeEntry_t1925)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
// Metadata Definition System.Runtime.Remoting.WellKnownObjectMode
extern TypeInfo WellKnownObjectMode_t1926_il2cpp_TypeInfo;
// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectModeMethodDeclarations.h"
static const EncodedMethodIndex WellKnownObjectMode_t1926_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair WellKnownObjectMode_t1926_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WellKnownObjectMode_t1926_0_0_0;
extern const Il2CppType WellKnownObjectMode_t1926_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata WellKnownObjectMode_t1926_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WellKnownObjectMode_t1926_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, WellKnownObjectMode_t1926_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8412/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WellKnownObjectMode_t1926_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WellKnownObjectMode"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3010/* custom_attributes_cache */
	, &WellKnownObjectMode_t1926_0_0_0/* byval_arg */
	, &WellKnownObjectMode_t1926_1_0_0/* this_arg */
	, &WellKnownObjectMode_t1926_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WellKnownObjectMode_t1926)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (WellKnownObjectMode_t1926)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.WellKnownServiceTypeEntry
#include "mscorlib_System_Runtime_Remoting_WellKnownServiceTypeEntry.h"
// Metadata Definition System.Runtime.Remoting.WellKnownServiceTypeEntry
extern TypeInfo WellKnownServiceTypeEntry_t1927_il2cpp_TypeInfo;
// System.Runtime.Remoting.WellKnownServiceTypeEntry
#include "mscorlib_System_Runtime_Remoting_WellKnownServiceTypeEntryMethodDeclarations.h"
static const EncodedMethodIndex WellKnownServiceTypeEntry_t1927_VTable[4] = 
{
	626,
	601,
	627,
	3894,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WellKnownServiceTypeEntry_t1927_0_0_0;
extern const Il2CppType WellKnownServiceTypeEntry_t1927_1_0_0;
struct WellKnownServiceTypeEntry_t1927;
const Il2CppTypeDefinitionMetadata WellKnownServiceTypeEntry_t1927_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeEntry_t1901_0_0_0/* parent */
	, WellKnownServiceTypeEntry_t1927_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8415/* fieldStart */
	, 11995/* methodStart */
	, -1/* eventStart */
	, 2423/* propertyStart */

};
TypeInfo WellKnownServiceTypeEntry_t1927_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WellKnownServiceTypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &WellKnownServiceTypeEntry_t1927_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3011/* custom_attributes_cache */
	, &WellKnownServiceTypeEntry_t1927_0_0_0/* byval_arg */
	, &WellKnownServiceTypeEntry_t1927_1_0_0/* this_arg */
	, &WellKnownServiceTypeEntry_t1927_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WellKnownServiceTypeEntry_t1927)/* instance_size */
	, sizeof (WellKnownServiceTypeEntry_t1927)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryCommon
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryCommon
extern TypeInfo BinaryCommon_t1928_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryCommon
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_BinaMethodDeclarations.h"
static const EncodedMethodIndex BinaryCommon_t1928_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryCommon_t1928_0_0_0;
extern const Il2CppType BinaryCommon_t1928_1_0_0;
struct BinaryCommon_t1928;
const Il2CppTypeDefinitionMetadata BinaryCommon_t1928_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BinaryCommon_t1928_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8418/* fieldStart */
	, 12000/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BinaryCommon_t1928_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryCommon"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, NULL/* methods */
	, &BinaryCommon_t1928_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BinaryCommon_t1928_0_0_0/* byval_arg */
	, &BinaryCommon_t1928_1_0_0/* this_arg */
	, &BinaryCommon_t1928_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryCommon_t1928)/* instance_size */
	, sizeof (BinaryCommon_t1928)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BinaryCommon_t1928_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryElement
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_0.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryElement
extern TypeInfo BinaryElement_t1929_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryElement
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_0MethodDeclarations.h"
static const EncodedMethodIndex BinaryElement_t1929_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair BinaryElement_t1929_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryElement_t1929_0_0_0;
extern const Il2CppType BinaryElement_t1929_1_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t1117_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata BinaryElement_t1929_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BinaryElement_t1929_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, BinaryElement_t1929_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8422/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BinaryElement_t1929_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryElement"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, NULL/* methods */
	, &Byte_t1117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BinaryElement_t1929_0_0_0/* byval_arg */
	, &BinaryElement_t1929_1_0_0/* this_arg */
	, &BinaryElement_t1929_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryElement_t1929)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BinaryElement_t1929)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.TypeTag
extern TypeInfo TypeTag_t1930_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_TypeMethodDeclarations.h"
static const EncodedMethodIndex TypeTag_t1930_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair TypeTag_t1930_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeTag_t1930_0_0_0;
extern const Il2CppType TypeTag_t1930_1_0_0;
const Il2CppTypeDefinitionMetadata TypeTag_t1930_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeTag_t1930_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, TypeTag_t1930_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8446/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeTag_t1930_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeTag"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, NULL/* methods */
	, &Byte_t1117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeTag_t1930_0_0_0/* byval_arg */
	, &TypeTag_t1930_1_0_0/* this_arg */
	, &TypeTag_t1930_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeTag_t1930)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeTag_t1930)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.MethodFlags
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Meth.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.MethodFlags
extern TypeInfo MethodFlags_t1931_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.MethodFlags
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_MethMethodDeclarations.h"
static const EncodedMethodIndex MethodFlags_t1931_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair MethodFlags_t1931_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodFlags_t1931_0_0_0;
extern const Il2CppType MethodFlags_t1931_1_0_0;
const Il2CppTypeDefinitionMetadata MethodFlags_t1931_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodFlags_t1931_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, MethodFlags_t1931_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8455/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MethodFlags_t1931_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodFlags"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodFlags_t1931_0_0_0/* byval_arg */
	, &MethodFlags_t1931_1_0_0/* this_arg */
	, &MethodFlags_t1931_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodFlags_t1931)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MethodFlags_t1931)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Retu.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
extern TypeInfo ReturnTypeTag_t1932_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_RetuMethodDeclarations.h"
static const EncodedMethodIndex ReturnTypeTag_t1932_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ReturnTypeTag_t1932_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReturnTypeTag_t1932_0_0_0;
extern const Il2CppType ReturnTypeTag_t1932_1_0_0;
const Il2CppTypeDefinitionMetadata ReturnTypeTag_t1932_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReturnTypeTag_t1932_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ReturnTypeTag_t1932_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8466/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ReturnTypeTag_t1932_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReturnTypeTag"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, NULL/* methods */
	, &Byte_t1117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReturnTypeTag_t1932_0_0_0/* byval_arg */
	, &ReturnTypeTag_t1932_1_0_0/* this_arg */
	, &ReturnTypeTag_t1932_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReturnTypeTag_t1932)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ReturnTypeTag_t1932)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_1.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
extern TypeInfo BinaryFormatter_t1917_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Bina_1MethodDeclarations.h"
static const EncodedMethodIndex BinaryFormatter_t1917_VTable[8] = 
{
	626,
	601,
	627,
	628,
	3895,
	3896,
	3897,
	3898,
};
extern const Il2CppType IRemotingFormatter_t3490_0_0_0;
extern const Il2CppType IFormatter_t3492_0_0_0;
static const Il2CppType* BinaryFormatter_t1917_InterfacesTypeInfos[] = 
{
	&IRemotingFormatter_t3490_0_0_0,
	&IFormatter_t3492_0_0_0,
};
static Il2CppInterfaceOffsetPair BinaryFormatter_t1917_InterfacesOffsets[] = 
{
	{ &IRemotingFormatter_t3490_0_0_0, 4},
	{ &IFormatter_t3492_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryFormatter_t1917_0_0_0;
extern const Il2CppType BinaryFormatter_t1917_1_0_0;
struct BinaryFormatter_t1917;
const Il2CppTypeDefinitionMetadata BinaryFormatter_t1917_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, BinaryFormatter_t1917_InterfacesTypeInfos/* implementedInterfaces */
	, BinaryFormatter_t1917_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BinaryFormatter_t1917_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8471/* fieldStart */
	, 12004/* methodStart */
	, -1/* eventStart */
	, 2426/* propertyStart */

};
TypeInfo BinaryFormatter_t1917_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryFormatter"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, NULL/* methods */
	, &BinaryFormatter_t1917_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3012/* custom_attributes_cache */
	, &BinaryFormatter_t1917_0_0_0/* byval_arg */
	, &BinaryFormatter_t1917_1_0_0/* this_arg */
	, &BinaryFormatter_t1917_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryFormatter_t1917)/* instance_size */
	, sizeof (BinaryFormatter_t1917)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BinaryFormatter_t1917_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.MessageFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Mess.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.MessageFormatter
extern TypeInfo MessageFormatter_t1934_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.MessageFormatter
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_MessMethodDeclarations.h"
static const EncodedMethodIndex MessageFormatter_t1934_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MessageFormatter_t1934_0_0_0;
extern const Il2CppType MessageFormatter_t1934_1_0_0;
struct MessageFormatter_t1934;
const Il2CppTypeDefinitionMetadata MessageFormatter_t1934_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MessageFormatter_t1934_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12015/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MessageFormatter_t1934_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MessageFormatter"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, NULL/* methods */
	, &MessageFormatter_t1934_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MessageFormatter_t1934_0_0_0/* byval_arg */
	, &MessageFormatter_t1934_1_0_0/* this_arg */
	, &MessageFormatter_t1934_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MessageFormatter_t1934)/* instance_size */
	, sizeof (MessageFormatter_t1934)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_1.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader
extern TypeInfo ObjectReader_t1939_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ObjectReader
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_1MethodDeclarations.h"
extern const Il2CppType TypeMetadata_t1936_0_0_0;
extern const Il2CppType ArrayNullFiller_t1937_0_0_0;
static const Il2CppType* ObjectReader_t1939_il2cpp_TypeInfo__nestedTypes[2] =
{
	&TypeMetadata_t1936_0_0_0,
	&ArrayNullFiller_t1937_0_0_0,
};
static const EncodedMethodIndex ObjectReader_t1939_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectReader_t1939_0_0_0;
extern const Il2CppType ObjectReader_t1939_1_0_0;
struct ObjectReader_t1939;
const Il2CppTypeDefinitionMetadata ObjectReader_t1939_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ObjectReader_t1939_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectReader_t1939_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8478/* fieldStart */
	, 12017/* methodStart */
	, -1/* eventStart */
	, 2432/* propertyStart */

};
TypeInfo ObjectReader_t1939_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectReader"/* name */
	, "System.Runtime.Serialization.Formatters.Binary"/* namespaze */
	, NULL/* methods */
	, &ObjectReader_t1939_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectReader_t1939_0_0_0/* byval_arg */
	, &ObjectReader_t1939_1_0_0/* this_arg */
	, &ObjectReader_t1939_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectReader_t1939)/* instance_size */
	, sizeof (ObjectReader_t1939)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 27/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
extern TypeInfo TypeMetadata_t1936_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/TypeMetadata
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_ObjeMethodDeclarations.h"
static const EncodedMethodIndex TypeMetadata_t1936_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeMetadata_t1936_1_0_0;
struct TypeMetadata_t1936;
const Il2CppTypeDefinitionMetadata TypeMetadata_t1936_DefinitionMetadata = 
{
	&ObjectReader_t1939_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeMetadata_t1936_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8490/* fieldStart */
	, 12044/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeMetadata_t1936_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeMetadata"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &TypeMetadata_t1936_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeMetadata_t1936_0_0_0/* byval_arg */
	, &TypeMetadata_t1936_1_0_0/* this_arg */
	, &TypeMetadata_t1936_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeMetadata_t1936)/* instance_size */
	, sizeof (TypeMetadata_t1936)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_0.h"
// Metadata Definition System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
extern TypeInfo ArrayNullFiller_t1937_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.Binary.ObjectReader/ArrayNullFiller
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Obje_0MethodDeclarations.h"
static const EncodedMethodIndex ArrayNullFiller_t1937_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArrayNullFiller_t1937_1_0_0;
struct ArrayNullFiller_t1937;
const Il2CppTypeDefinitionMetadata ArrayNullFiller_t1937_DefinitionMetadata = 
{
	&ObjectReader_t1939_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArrayNullFiller_t1937_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8496/* fieldStart */
	, 12045/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArrayNullFiller_t1937_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayNullFiller"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ArrayNullFiller_t1937_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArrayNullFiller_t1937_0_0_0/* byval_arg */
	, &ArrayNullFiller_t1937_1_0_0/* this_arg */
	, &ArrayNullFiller_t1937_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArrayNullFiller_t1937)/* instance_size */
	, sizeof (ArrayNullFiller_t1937)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterAs.h"
// Metadata Definition System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
extern TypeInfo FormatterAssemblyStyle_t1940_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterAsMethodDeclarations.h"
static const EncodedMethodIndex FormatterAssemblyStyle_t1940_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair FormatterAssemblyStyle_t1940_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterAssemblyStyle_t1940_0_0_0;
extern const Il2CppType FormatterAssemblyStyle_t1940_1_0_0;
const Il2CppTypeDefinitionMetadata FormatterAssemblyStyle_t1940_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatterAssemblyStyle_t1940_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, FormatterAssemblyStyle_t1940_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8497/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FormatterAssemblyStyle_t1940_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterAssemblyStyle"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3015/* custom_attributes_cache */
	, &FormatterAssemblyStyle_t1940_0_0_0/* byval_arg */
	, &FormatterAssemblyStyle_t1940_1_0_0/* this_arg */
	, &FormatterAssemblyStyle_t1940_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterAssemblyStyle_t1940)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FormatterAssemblyStyle_t1940)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.FormatterTypeStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterTy.h"
// Metadata Definition System.Runtime.Serialization.Formatters.FormatterTypeStyle
extern TypeInfo FormatterTypeStyle_t1941_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.FormatterTypeStyle
#include "mscorlib_System_Runtime_Serialization_Formatters_FormatterTyMethodDeclarations.h"
static const EncodedMethodIndex FormatterTypeStyle_t1941_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair FormatterTypeStyle_t1941_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterTypeStyle_t1941_0_0_0;
extern const Il2CppType FormatterTypeStyle_t1941_1_0_0;
const Il2CppTypeDefinitionMetadata FormatterTypeStyle_t1941_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatterTypeStyle_t1941_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, FormatterTypeStyle_t1941_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8500/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FormatterTypeStyle_t1941_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterTypeStyle"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3016/* custom_attributes_cache */
	, &FormatterTypeStyle_t1941_0_0_0/* byval_arg */
	, &FormatterTypeStyle_t1941_1_0_0/* this_arg */
	, &FormatterTypeStyle_t1941_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterTypeStyle_t1941)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FormatterTypeStyle_t1941)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
// Metadata Definition System.Runtime.Serialization.Formatters.TypeFilterLevel
extern TypeInfo TypeFilterLevel_t1942_il2cpp_TypeInfo;
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterLMethodDeclarations.h"
static const EncodedMethodIndex TypeFilterLevel_t1942_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair TypeFilterLevel_t1942_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeFilterLevel_t1942_0_0_0;
extern const Il2CppType TypeFilterLevel_t1942_1_0_0;
const Il2CppTypeDefinitionMetadata TypeFilterLevel_t1942_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeFilterLevel_t1942_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, TypeFilterLevel_t1942_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8504/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeFilterLevel_t1942_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeFilterLevel"/* name */
	, "System.Runtime.Serialization.Formatters"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3017/* custom_attributes_cache */
	, &TypeFilterLevel_t1942_0_0_0/* byval_arg */
	, &TypeFilterLevel_t1942_1_0_0/* this_arg */
	, &TypeFilterLevel_t1942_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeFilterLevel_t1942)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeFilterLevel_t1942)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.FormatterConverter
#include "mscorlib_System_Runtime_Serialization_FormatterConverter.h"
// Metadata Definition System.Runtime.Serialization.FormatterConverter
extern TypeInfo FormatterConverter_t1943_il2cpp_TypeInfo;
// System.Runtime.Serialization.FormatterConverter
#include "mscorlib_System_Runtime_Serialization_FormatterConverterMethodDeclarations.h"
static const EncodedMethodIndex FormatterConverter_t1943_VTable[10] = 
{
	626,
	601,
	627,
	628,
	3899,
	3900,
	3901,
	3902,
	3903,
	3904,
};
extern const Il2CppType IFormatterConverter_t1960_0_0_0;
static const Il2CppType* FormatterConverter_t1943_InterfacesTypeInfos[] = 
{
	&IFormatterConverter_t1960_0_0_0,
};
static Il2CppInterfaceOffsetPair FormatterConverter_t1943_InterfacesOffsets[] = 
{
	{ &IFormatterConverter_t1960_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterConverter_t1943_0_0_0;
extern const Il2CppType FormatterConverter_t1943_1_0_0;
struct FormatterConverter_t1943;
const Il2CppTypeDefinitionMetadata FormatterConverter_t1943_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FormatterConverter_t1943_InterfacesTypeInfos/* implementedInterfaces */
	, FormatterConverter_t1943_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FormatterConverter_t1943_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12046/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FormatterConverter_t1943_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterConverter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &FormatterConverter_t1943_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3018/* custom_attributes_cache */
	, &FormatterConverter_t1943_0_0_0/* byval_arg */
	, &FormatterConverter_t1943_1_0_0/* this_arg */
	, &FormatterConverter_t1943_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterConverter_t1943)/* instance_size */
	, sizeof (FormatterConverter_t1943)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.FormatterServices
#include "mscorlib_System_Runtime_Serialization_FormatterServices.h"
// Metadata Definition System.Runtime.Serialization.FormatterServices
extern TypeInfo FormatterServices_t1944_il2cpp_TypeInfo;
// System.Runtime.Serialization.FormatterServices
#include "mscorlib_System_Runtime_Serialization_FormatterServicesMethodDeclarations.h"
static const EncodedMethodIndex FormatterServices_t1944_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatterServices_t1944_0_0_0;
extern const Il2CppType FormatterServices_t1944_1_0_0;
struct FormatterServices_t1944;
const Il2CppTypeDefinitionMetadata FormatterServices_t1944_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FormatterServices_t1944_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12053/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FormatterServices_t1944_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatterServices"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &FormatterServices_t1944_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3019/* custom_attributes_cache */
	, &FormatterServices_t1944_0_0_0/* byval_arg */
	, &FormatterServices_t1944_1_0_0/* this_arg */
	, &FormatterServices_t1944_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatterServices_t1944)/* instance_size */
	, sizeof (FormatterServices_t1944)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IDeserializationCallback
extern TypeInfo IDeserializationCallback_t2213_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IDeserializationCallback_t2213_0_0_0;
extern const Il2CppType IDeserializationCallback_t2213_1_0_0;
struct IDeserializationCallback_t2213;
const Il2CppTypeDefinitionMetadata IDeserializationCallback_t2213_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12055/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IDeserializationCallback_t2213_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDeserializationCallback"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &IDeserializationCallback_t2213_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3020/* custom_attributes_cache */
	, &IDeserializationCallback_t2213_0_0_0/* byval_arg */
	, &IDeserializationCallback_t2213_1_0_0/* this_arg */
	, &IDeserializationCallback_t2213_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IFormatter
extern TypeInfo IFormatter_t3492_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormatter_t3492_1_0_0;
struct IFormatter_t3492;
const Il2CppTypeDefinitionMetadata IFormatter_t3492_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IFormatter_t3492_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &IFormatter_t3492_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3021/* custom_attributes_cache */
	, &IFormatter_t3492_0_0_0/* byval_arg */
	, &IFormatter_t3492_1_0_0/* this_arg */
	, &IFormatter_t3492_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IFormatterConverter
extern TypeInfo IFormatterConverter_t1960_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormatterConverter_t1960_1_0_0;
struct IFormatterConverter_t1960;
const Il2CppTypeDefinitionMetadata IFormatterConverter_t1960_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12056/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IFormatterConverter_t1960_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatterConverter"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &IFormatterConverter_t1960_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3022/* custom_attributes_cache */
	, &IFormatterConverter_t1960_0_0_0/* byval_arg */
	, &IFormatterConverter_t1960_1_0_0/* this_arg */
	, &IFormatterConverter_t1960_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.IObjectReference
extern TypeInfo IObjectReference_t2211_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IObjectReference_t2211_0_0_0;
extern const Il2CppType IObjectReference_t2211_1_0_0;
struct IObjectReference_t2211;
const Il2CppTypeDefinitionMetadata IObjectReference_t2211_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12062/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IObjectReference_t2211_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IObjectReference"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &IObjectReference_t2211_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3023/* custom_attributes_cache */
	, &IObjectReference_t2211_0_0_0/* byval_arg */
	, &IObjectReference_t2211_1_0_0/* this_arg */
	, &IObjectReference_t2211_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.ISerializationSurrogate
extern TypeInfo ISerializationSurrogate_t1952_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISerializationSurrogate_t1952_0_0_0;
extern const Il2CppType ISerializationSurrogate_t1952_1_0_0;
struct ISerializationSurrogate_t1952;
const Il2CppTypeDefinitionMetadata ISerializationSurrogate_t1952_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12063/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ISerializationSurrogate_t1952_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISerializationSurrogate"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &ISerializationSurrogate_t1952_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3024/* custom_attributes_cache */
	, &ISerializationSurrogate_t1952_0_0_0/* byval_arg */
	, &ISerializationSurrogate_t1952_1_0_0/* this_arg */
	, &ISerializationSurrogate_t1952_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.ISurrogateSelector
extern TypeInfo ISurrogateSelector_t1885_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISurrogateSelector_t1885_0_0_0;
extern const Il2CppType ISurrogateSelector_t1885_1_0_0;
struct ISurrogateSelector_t1885;
const Il2CppTypeDefinitionMetadata ISurrogateSelector_t1885_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12064/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ISurrogateSelector_t1885_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISurrogateSelector"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &ISurrogateSelector_t1885_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3025/* custom_attributes_cache */
	, &ISurrogateSelector_t1885_0_0_0/* byval_arg */
	, &ISurrogateSelector_t1885_1_0_0/* this_arg */
	, &ISurrogateSelector_t1885_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.ObjectManager
#include "mscorlib_System_Runtime_Serialization_ObjectManager.h"
// Metadata Definition System.Runtime.Serialization.ObjectManager
extern TypeInfo ObjectManager_t1938_il2cpp_TypeInfo;
// System.Runtime.Serialization.ObjectManager
#include "mscorlib_System_Runtime_Serialization_ObjectManagerMethodDeclarations.h"
static const EncodedMethodIndex ObjectManager_t1938_VTable[11] = 
{
	626,
	601,
	627,
	628,
	3905,
	3906,
	3907,
	3908,
	3909,
	3910,
	3911,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectManager_t1938_0_0_0;
extern const Il2CppType ObjectManager_t1938_1_0_0;
struct ObjectManager_t1938;
const Il2CppTypeDefinitionMetadata ObjectManager_t1938_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectManager_t1938_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8507/* fieldStart */
	, 12065/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ObjectManager_t1938_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectManager"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &ObjectManager_t1938_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3026/* custom_attributes_cache */
	, &ObjectManager_t1938_0_0_0/* byval_arg */
	, &ObjectManager_t1938_1_0_0/* this_arg */
	, &ObjectManager_t1938_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectManager_t1938)/* instance_size */
	, sizeof (ObjectManager_t1938)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.BaseFixupRecord
#include "mscorlib_System_Runtime_Serialization_BaseFixupRecord.h"
// Metadata Definition System.Runtime.Serialization.BaseFixupRecord
extern TypeInfo BaseFixupRecord_t1946_il2cpp_TypeInfo;
// System.Runtime.Serialization.BaseFixupRecord
#include "mscorlib_System_Runtime_Serialization_BaseFixupRecordMethodDeclarations.h"
static const EncodedMethodIndex BaseFixupRecord_t1946_VTable[5] = 
{
	626,
	601,
	627,
	628,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BaseFixupRecord_t1946_0_0_0;
extern const Il2CppType BaseFixupRecord_t1946_1_0_0;
struct BaseFixupRecord_t1946;
const Il2CppTypeDefinitionMetadata BaseFixupRecord_t1946_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BaseFixupRecord_t1946_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8516/* fieldStart */
	, 12079/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BaseFixupRecord_t1946_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseFixupRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &BaseFixupRecord_t1946_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseFixupRecord_t1946_0_0_0/* byval_arg */
	, &BaseFixupRecord_t1946_1_0_0/* this_arg */
	, &BaseFixupRecord_t1946_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseFixupRecord_t1946)/* instance_size */
	, sizeof (BaseFixupRecord_t1946)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.ArrayFixupRecord
#include "mscorlib_System_Runtime_Serialization_ArrayFixupRecord.h"
// Metadata Definition System.Runtime.Serialization.ArrayFixupRecord
extern TypeInfo ArrayFixupRecord_t1947_il2cpp_TypeInfo;
// System.Runtime.Serialization.ArrayFixupRecord
#include "mscorlib_System_Runtime_Serialization_ArrayFixupRecordMethodDeclarations.h"
static const EncodedMethodIndex ArrayFixupRecord_t1947_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3912,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArrayFixupRecord_t1947_0_0_0;
extern const Il2CppType ArrayFixupRecord_t1947_1_0_0;
struct ArrayFixupRecord_t1947;
const Il2CppTypeDefinitionMetadata ArrayFixupRecord_t1947_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseFixupRecord_t1946_0_0_0/* parent */
	, ArrayFixupRecord_t1947_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8520/* fieldStart */
	, 12082/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArrayFixupRecord_t1947_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayFixupRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &ArrayFixupRecord_t1947_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArrayFixupRecord_t1947_0_0_0/* byval_arg */
	, &ArrayFixupRecord_t1947_1_0_0/* this_arg */
	, &ArrayFixupRecord_t1947_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArrayFixupRecord_t1947)/* instance_size */
	, sizeof (ArrayFixupRecord_t1947)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.MultiArrayFixupRecord
#include "mscorlib_System_Runtime_Serialization_MultiArrayFixupRecord.h"
// Metadata Definition System.Runtime.Serialization.MultiArrayFixupRecord
extern TypeInfo MultiArrayFixupRecord_t1948_il2cpp_TypeInfo;
// System.Runtime.Serialization.MultiArrayFixupRecord
#include "mscorlib_System_Runtime_Serialization_MultiArrayFixupRecordMethodDeclarations.h"
static const EncodedMethodIndex MultiArrayFixupRecord_t1948_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3913,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MultiArrayFixupRecord_t1948_0_0_0;
extern const Il2CppType MultiArrayFixupRecord_t1948_1_0_0;
struct MultiArrayFixupRecord_t1948;
const Il2CppTypeDefinitionMetadata MultiArrayFixupRecord_t1948_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseFixupRecord_t1946_0_0_0/* parent */
	, MultiArrayFixupRecord_t1948_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8521/* fieldStart */
	, 12084/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MultiArrayFixupRecord_t1948_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MultiArrayFixupRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &MultiArrayFixupRecord_t1948_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MultiArrayFixupRecord_t1948_0_0_0/* byval_arg */
	, &MultiArrayFixupRecord_t1948_1_0_0/* this_arg */
	, &MultiArrayFixupRecord_t1948_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MultiArrayFixupRecord_t1948)/* instance_size */
	, sizeof (MultiArrayFixupRecord_t1948)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.FixupRecord
#include "mscorlib_System_Runtime_Serialization_FixupRecord.h"
// Metadata Definition System.Runtime.Serialization.FixupRecord
extern TypeInfo FixupRecord_t1949_il2cpp_TypeInfo;
// System.Runtime.Serialization.FixupRecord
#include "mscorlib_System_Runtime_Serialization_FixupRecordMethodDeclarations.h"
static const EncodedMethodIndex FixupRecord_t1949_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3914,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FixupRecord_t1949_0_0_0;
extern const Il2CppType FixupRecord_t1949_1_0_0;
struct FixupRecord_t1949;
const Il2CppTypeDefinitionMetadata FixupRecord_t1949_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseFixupRecord_t1946_0_0_0/* parent */
	, FixupRecord_t1949_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8522/* fieldStart */
	, 12086/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FixupRecord_t1949_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FixupRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &FixupRecord_t1949_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FixupRecord_t1949_0_0_0/* byval_arg */
	, &FixupRecord_t1949_1_0_0/* this_arg */
	, &FixupRecord_t1949_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FixupRecord_t1949)/* instance_size */
	, sizeof (FixupRecord_t1949)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.DelayedFixupRecord
#include "mscorlib_System_Runtime_Serialization_DelayedFixupRecord.h"
// Metadata Definition System.Runtime.Serialization.DelayedFixupRecord
extern TypeInfo DelayedFixupRecord_t1950_il2cpp_TypeInfo;
// System.Runtime.Serialization.DelayedFixupRecord
#include "mscorlib_System_Runtime_Serialization_DelayedFixupRecordMethodDeclarations.h"
static const EncodedMethodIndex DelayedFixupRecord_t1950_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3915,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DelayedFixupRecord_t1950_0_0_0;
extern const Il2CppType DelayedFixupRecord_t1950_1_0_0;
struct DelayedFixupRecord_t1950;
const Il2CppTypeDefinitionMetadata DelayedFixupRecord_t1950_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseFixupRecord_t1946_0_0_0/* parent */
	, DelayedFixupRecord_t1950_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8523/* fieldStart */
	, 12088/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DelayedFixupRecord_t1950_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DelayedFixupRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &DelayedFixupRecord_t1950_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DelayedFixupRecord_t1950_0_0_0/* byval_arg */
	, &DelayedFixupRecord_t1950_1_0_0/* this_arg */
	, &DelayedFixupRecord_t1950_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DelayedFixupRecord_t1950)/* instance_size */
	, sizeof (DelayedFixupRecord_t1950)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.ObjectRecordStatus
#include "mscorlib_System_Runtime_Serialization_ObjectRecordStatus.h"
// Metadata Definition System.Runtime.Serialization.ObjectRecordStatus
extern TypeInfo ObjectRecordStatus_t1951_il2cpp_TypeInfo;
// System.Runtime.Serialization.ObjectRecordStatus
#include "mscorlib_System_Runtime_Serialization_ObjectRecordStatusMethodDeclarations.h"
static const EncodedMethodIndex ObjectRecordStatus_t1951_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ObjectRecordStatus_t1951_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectRecordStatus_t1951_0_0_0;
extern const Il2CppType ObjectRecordStatus_t1951_1_0_0;
const Il2CppTypeDefinitionMetadata ObjectRecordStatus_t1951_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ObjectRecordStatus_t1951_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ObjectRecordStatus_t1951_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8524/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ObjectRecordStatus_t1951_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectRecordStatus"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &Byte_t1117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectRecordStatus_t1951_0_0_0/* byval_arg */
	, &ObjectRecordStatus_t1951_1_0_0/* this_arg */
	, &ObjectRecordStatus_t1951_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectRecordStatus_t1951)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ObjectRecordStatus_t1951)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Serialization.ObjectRecord
#include "mscorlib_System_Runtime_Serialization_ObjectRecord.h"
// Metadata Definition System.Runtime.Serialization.ObjectRecord
extern TypeInfo ObjectRecord_t1945_il2cpp_TypeInfo;
// System.Runtime.Serialization.ObjectRecord
#include "mscorlib_System_Runtime_Serialization_ObjectRecordMethodDeclarations.h"
static const EncodedMethodIndex ObjectRecord_t1945_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectRecord_t1945_0_0_0;
extern const Il2CppType ObjectRecord_t1945_1_0_0;
struct ObjectRecord_t1945;
const Il2CppTypeDefinitionMetadata ObjectRecord_t1945_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectRecord_t1945_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8529/* fieldStart */
	, 12090/* methodStart */
	, -1/* eventStart */
	, 2433/* propertyStart */

};
TypeInfo ObjectRecord_t1945_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectRecord"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &ObjectRecord_t1945_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectRecord_t1945_0_0_0/* byval_arg */
	, &ObjectRecord_t1945_1_0_0/* this_arg */
	, &ObjectRecord_t1945_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectRecord_t1945)/* instance_size */
	, sizeof (ObjectRecord_t1945)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 4/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.OnDeserializedAttribute
#include "mscorlib_System_Runtime_Serialization_OnDeserializedAttribut.h"
// Metadata Definition System.Runtime.Serialization.OnDeserializedAttribute
extern TypeInfo OnDeserializedAttribute_t1953_il2cpp_TypeInfo;
// System.Runtime.Serialization.OnDeserializedAttribute
#include "mscorlib_System_Runtime_Serialization_OnDeserializedAttributMethodDeclarations.h"
static const EncodedMethodIndex OnDeserializedAttribute_t1953_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
extern const Il2CppType _Attribute_t3429_0_0_0;
static Il2CppInterfaceOffsetPair OnDeserializedAttribute_t1953_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OnDeserializedAttribute_t1953_0_0_0;
extern const Il2CppType OnDeserializedAttribute_t1953_1_0_0;
extern const Il2CppType Attribute_t903_0_0_0;
struct OnDeserializedAttribute_t1953;
const Il2CppTypeDefinitionMetadata OnDeserializedAttribute_t1953_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnDeserializedAttribute_t1953_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, OnDeserializedAttribute_t1953_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnDeserializedAttribute_t1953_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnDeserializedAttribute"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &OnDeserializedAttribute_t1953_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3027/* custom_attributes_cache */
	, &OnDeserializedAttribute_t1953_0_0_0/* byval_arg */
	, &OnDeserializedAttribute_t1953_1_0_0/* this_arg */
	, &OnDeserializedAttribute_t1953_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnDeserializedAttribute_t1953)/* instance_size */
	, sizeof (OnDeserializedAttribute_t1953)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.OnDeserializingAttribute
#include "mscorlib_System_Runtime_Serialization_OnDeserializingAttribu.h"
// Metadata Definition System.Runtime.Serialization.OnDeserializingAttribute
extern TypeInfo OnDeserializingAttribute_t1954_il2cpp_TypeInfo;
// System.Runtime.Serialization.OnDeserializingAttribute
#include "mscorlib_System_Runtime_Serialization_OnDeserializingAttribuMethodDeclarations.h"
static const EncodedMethodIndex OnDeserializingAttribute_t1954_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair OnDeserializingAttribute_t1954_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OnDeserializingAttribute_t1954_0_0_0;
extern const Il2CppType OnDeserializingAttribute_t1954_1_0_0;
struct OnDeserializingAttribute_t1954;
const Il2CppTypeDefinitionMetadata OnDeserializingAttribute_t1954_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnDeserializingAttribute_t1954_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, OnDeserializingAttribute_t1954_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnDeserializingAttribute_t1954_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnDeserializingAttribute"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &OnDeserializingAttribute_t1954_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3028/* custom_attributes_cache */
	, &OnDeserializingAttribute_t1954_0_0_0/* byval_arg */
	, &OnDeserializingAttribute_t1954_1_0_0/* this_arg */
	, &OnDeserializingAttribute_t1954_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnDeserializingAttribute_t1954)/* instance_size */
	, sizeof (OnDeserializingAttribute_t1954)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.OnSerializedAttribute
#include "mscorlib_System_Runtime_Serialization_OnSerializedAttribute.h"
// Metadata Definition System.Runtime.Serialization.OnSerializedAttribute
extern TypeInfo OnSerializedAttribute_t1955_il2cpp_TypeInfo;
// System.Runtime.Serialization.OnSerializedAttribute
#include "mscorlib_System_Runtime_Serialization_OnSerializedAttributeMethodDeclarations.h"
static const EncodedMethodIndex OnSerializedAttribute_t1955_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair OnSerializedAttribute_t1955_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OnSerializedAttribute_t1955_0_0_0;
extern const Il2CppType OnSerializedAttribute_t1955_1_0_0;
struct OnSerializedAttribute_t1955;
const Il2CppTypeDefinitionMetadata OnSerializedAttribute_t1955_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnSerializedAttribute_t1955_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, OnSerializedAttribute_t1955_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnSerializedAttribute_t1955_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnSerializedAttribute"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &OnSerializedAttribute_t1955_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3029/* custom_attributes_cache */
	, &OnSerializedAttribute_t1955_0_0_0/* byval_arg */
	, &OnSerializedAttribute_t1955_1_0_0/* this_arg */
	, &OnSerializedAttribute_t1955_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnSerializedAttribute_t1955)/* instance_size */
	, sizeof (OnSerializedAttribute_t1955)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.OnSerializingAttribute
#include "mscorlib_System_Runtime_Serialization_OnSerializingAttribute.h"
// Metadata Definition System.Runtime.Serialization.OnSerializingAttribute
extern TypeInfo OnSerializingAttribute_t1956_il2cpp_TypeInfo;
// System.Runtime.Serialization.OnSerializingAttribute
#include "mscorlib_System_Runtime_Serialization_OnSerializingAttributeMethodDeclarations.h"
static const EncodedMethodIndex OnSerializingAttribute_t1956_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair OnSerializingAttribute_t1956_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OnSerializingAttribute_t1956_0_0_0;
extern const Il2CppType OnSerializingAttribute_t1956_1_0_0;
struct OnSerializingAttribute_t1956;
const Il2CppTypeDefinitionMetadata OnSerializingAttribute_t1956_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnSerializingAttribute_t1956_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, OnSerializingAttribute_t1956_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnSerializingAttribute_t1956_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnSerializingAttribute"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &OnSerializingAttribute_t1956_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3030/* custom_attributes_cache */
	, &OnSerializingAttribute_t1956_0_0_0/* byval_arg */
	, &OnSerializingAttribute_t1956_1_0_0/* this_arg */
	, &OnSerializingAttribute_t1956_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnSerializingAttribute_t1956)/* instance_size */
	, sizeof (OnSerializingAttribute_t1956)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationBinder
#include "mscorlib_System_Runtime_Serialization_SerializationBinder.h"
// Metadata Definition System.Runtime.Serialization.SerializationBinder
extern TypeInfo SerializationBinder_t1933_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationBinder
#include "mscorlib_System_Runtime_Serialization_SerializationBinderMethodDeclarations.h"
static const EncodedMethodIndex SerializationBinder_t1933_VTable[5] = 
{
	626,
	601,
	627,
	628,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationBinder_t1933_0_0_0;
extern const Il2CppType SerializationBinder_t1933_1_0_0;
struct SerializationBinder_t1933;
const Il2CppTypeDefinitionMetadata SerializationBinder_t1933_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SerializationBinder_t1933_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12103/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SerializationBinder_t1933_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationBinder"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &SerializationBinder_t1933_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3031/* custom_attributes_cache */
	, &SerializationBinder_t1933_0_0_0/* byval_arg */
	, &SerializationBinder_t1933_1_0_0/* this_arg */
	, &SerializationBinder_t1933_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationBinder_t1933)/* instance_size */
	, sizeof (SerializationBinder_t1933)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationCallbacks
#include "mscorlib_System_Runtime_Serialization_SerializationCallbacks_0.h"
// Metadata Definition System.Runtime.Serialization.SerializationCallbacks
extern TypeInfo SerializationCallbacks_t1958_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationCallbacks
#include "mscorlib_System_Runtime_Serialization_SerializationCallbacks_0MethodDeclarations.h"
extern const Il2CppType CallbackHandler_t1957_0_0_0;
static const Il2CppType* SerializationCallbacks_t1958_il2cpp_TypeInfo__nestedTypes[1] =
{
	&CallbackHandler_t1957_0_0_0,
};
static const EncodedMethodIndex SerializationCallbacks_t1958_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationCallbacks_t1958_0_0_0;
extern const Il2CppType SerializationCallbacks_t1958_1_0_0;
struct SerializationCallbacks_t1958;
const Il2CppTypeDefinitionMetadata SerializationCallbacks_t1958_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SerializationCallbacks_t1958_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SerializationCallbacks_t1958_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8542/* fieldStart */
	, 12105/* methodStart */
	, -1/* eventStart */
	, 2437/* propertyStart */

};
TypeInfo SerializationCallbacks_t1958_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationCallbacks"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &SerializationCallbacks_t1958_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SerializationCallbacks_t1958_0_0_0/* byval_arg */
	, &SerializationCallbacks_t1958_1_0_0/* this_arg */
	, &SerializationCallbacks_t1958_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationCallbacks_t1958)/* instance_size */
	, sizeof (SerializationCallbacks_t1958)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SerializationCallbacks_t1958_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationCallbacks/CallbackHandler
#include "mscorlib_System_Runtime_Serialization_SerializationCallbacks.h"
// Metadata Definition System.Runtime.Serialization.SerializationCallbacks/CallbackHandler
extern TypeInfo CallbackHandler_t1957_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationCallbacks/CallbackHandler
#include "mscorlib_System_Runtime_Serialization_SerializationCallbacksMethodDeclarations.h"
static const EncodedMethodIndex CallbackHandler_t1957_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	3916,
	3917,
	3918,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
static Il2CppInterfaceOffsetPair CallbackHandler_t1957_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CallbackHandler_t1957_1_0_0;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
struct CallbackHandler_t1957;
const Il2CppTypeDefinitionMetadata CallbackHandler_t1957_DefinitionMetadata = 
{
	&SerializationCallbacks_t1958_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CallbackHandler_t1957_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, CallbackHandler_t1957_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12113/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CallbackHandler_t1957_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CallbackHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CallbackHandler_t1957_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CallbackHandler_t1957_0_0_0/* byval_arg */
	, &CallbackHandler_t1957_1_0_0/* this_arg */
	, &CallbackHandler_t1957_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CallbackHandler_t1957/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CallbackHandler_t1957)/* instance_size */
	, sizeof (CallbackHandler_t1957)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationEntry
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
// Metadata Definition System.Runtime.Serialization.SerializationEntry
extern TypeInfo SerializationEntry_t1959_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationEntry
#include "mscorlib_System_Runtime_Serialization_SerializationEntryMethodDeclarations.h"
static const EncodedMethodIndex SerializationEntry_t1959_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationEntry_t1959_0_0_0;
extern const Il2CppType SerializationEntry_t1959_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata SerializationEntry_t1959_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, SerializationEntry_t1959_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8548/* fieldStart */
	, 12117/* methodStart */
	, -1/* eventStart */
	, 2438/* propertyStart */

};
TypeInfo SerializationEntry_t1959_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationEntry"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &SerializationEntry_t1959_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3032/* custom_attributes_cache */
	, &SerializationEntry_t1959_0_0_0/* byval_arg */
	, &SerializationEntry_t1959_1_0_0/* this_arg */
	, &SerializationEntry_t1959_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationEntry_t1959)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SerializationEntry_t1959)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationException
#include "mscorlib_System_Runtime_Serialization_SerializationException.h"
// Metadata Definition System.Runtime.Serialization.SerializationException
extern TypeInfo SerializationException_t1532_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationException
#include "mscorlib_System_Runtime_Serialization_SerializationExceptionMethodDeclarations.h"
static const EncodedMethodIndex SerializationException_t1532_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair SerializationException_t1532_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationException_t1532_0_0_0;
extern const Il2CppType SerializationException_t1532_1_0_0;
struct SerializationException_t1532;
const Il2CppTypeDefinitionMetadata SerializationException_t1532_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SerializationException_t1532_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, SerializationException_t1532_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12120/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SerializationException_t1532_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationException"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &SerializationException_t1532_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3033/* custom_attributes_cache */
	, &SerializationException_t1532_0_0_0/* byval_arg */
	, &SerializationException_t1532_1_0_0/* this_arg */
	, &SerializationException_t1532_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationException_t1532)/* instance_size */
	, sizeof (SerializationException_t1532)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
// Metadata Definition System.Runtime.Serialization.SerializationInfo
extern TypeInfo SerializationInfo_t1104_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfoMethodDeclarations.h"
static const EncodedMethodIndex SerializationInfo_t1104_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationInfo_t1104_0_0_0;
extern const Il2CppType SerializationInfo_t1104_1_0_0;
struct SerializationInfo_t1104;
const Il2CppTypeDefinitionMetadata SerializationInfo_t1104_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SerializationInfo_t1104_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8551/* fieldStart */
	, 12123/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SerializationInfo_t1104_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationInfo"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &SerializationInfo_t1104_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3034/* custom_attributes_cache */
	, &SerializationInfo_t1104_0_0_0/* byval_arg */
	, &SerializationInfo_t1104_1_0_0/* this_arg */
	, &SerializationInfo_t1104_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationInfo_t1104)/* instance_size */
	, sizeof (SerializationInfo_t1104)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.SerializationInfoEnumerator
#include "mscorlib_System_Runtime_Serialization_SerializationInfoEnume.h"
// Metadata Definition System.Runtime.Serialization.SerializationInfoEnumerator
extern TypeInfo SerializationInfoEnumerator_t1961_il2cpp_TypeInfo;
// System.Runtime.Serialization.SerializationInfoEnumerator
#include "mscorlib_System_Runtime_Serialization_SerializationInfoEnumeMethodDeclarations.h"
static const EncodedMethodIndex SerializationInfoEnumerator_t1961_VTable[6] = 
{
	626,
	601,
	627,
	628,
	3919,
	3920,
};
extern const Il2CppType IEnumerator_t464_0_0_0;
static const Il2CppType* SerializationInfoEnumerator_t1961_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
};
static Il2CppInterfaceOffsetPair SerializationInfoEnumerator_t1961_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializationInfoEnumerator_t1961_0_0_0;
extern const Il2CppType SerializationInfoEnumerator_t1961_1_0_0;
struct SerializationInfoEnumerator_t1961;
const Il2CppTypeDefinitionMetadata SerializationInfoEnumerator_t1961_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SerializationInfoEnumerator_t1961_InterfacesTypeInfos/* implementedInterfaces */
	, SerializationInfoEnumerator_t1961_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SerializationInfoEnumerator_t1961_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8556/* fieldStart */
	, 12141/* methodStart */
	, -1/* eventStart */
	, 2440/* propertyStart */

};
TypeInfo SerializationInfoEnumerator_t1961_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializationInfoEnumerator"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &SerializationInfoEnumerator_t1961_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3037/* custom_attributes_cache */
	, &SerializationInfoEnumerator_t1961_0_0_0/* byval_arg */
	, &SerializationInfoEnumerator_t1961_1_0_0/* this_arg */
	, &SerializationInfoEnumerator_t1961_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializationInfoEnumerator_t1961)/* instance_size */
	, sizeof (SerializationInfoEnumerator_t1961)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 4/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// Metadata Definition System.Runtime.Serialization.StreamingContext
extern TypeInfo StreamingContext_t1105_il2cpp_TypeInfo;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContextMethodDeclarations.h"
static const EncodedMethodIndex StreamingContext_t1105_VTable[4] = 
{
	3921,
	601,
	3922,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StreamingContext_t1105_0_0_0;
extern const Il2CppType StreamingContext_t1105_1_0_0;
const Il2CppTypeDefinitionMetadata StreamingContext_t1105_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, StreamingContext_t1105_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8557/* fieldStart */
	, 12147/* methodStart */
	, -1/* eventStart */
	, 2444/* propertyStart */

};
TypeInfo StreamingContext_t1105_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StreamingContext"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &StreamingContext_t1105_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3038/* custom_attributes_cache */
	, &StreamingContext_t1105_0_0_0/* byval_arg */
	, &StreamingContext_t1105_1_0_0/* this_arg */
	, &StreamingContext_t1105_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StreamingContext_t1105)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StreamingContext_t1105)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Serialization.StreamingContextStates
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
// Metadata Definition System.Runtime.Serialization.StreamingContextStates
extern TypeInfo StreamingContextStates_t1962_il2cpp_TypeInfo;
// System.Runtime.Serialization.StreamingContextStates
#include "mscorlib_System_Runtime_Serialization_StreamingContextStatesMethodDeclarations.h"
static const EncodedMethodIndex StreamingContextStates_t1962_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair StreamingContextStates_t1962_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StreamingContextStates_t1962_0_0_0;
extern const Il2CppType StreamingContextStates_t1962_1_0_0;
const Il2CppTypeDefinitionMetadata StreamingContextStates_t1962_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StreamingContextStates_t1962_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, StreamingContextStates_t1962_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8559/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StreamingContextStates_t1962_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StreamingContextStates"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3039/* custom_attributes_cache */
	, &StreamingContextStates_t1962_0_0_0/* byval_arg */
	, &StreamingContextStates_t1962_1_0_0/* this_arg */
	, &StreamingContextStates_t1962_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StreamingContextStates_t1962)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StreamingContextStates_t1962)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509Certificate
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509C.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509Certificate
extern TypeInfo X509Certificate_t1304_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509Certificate
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509CMethodDeclarations.h"
static const EncodedMethodIndex X509Certificate_t1304_VTable[18] = 
{
	2055,
	601,
	2056,
	3923,
	2058,
	2059,
	2060,
	2061,
	2062,
	2063,
	2064,
	2065,
	2066,
	2067,
	2068,
	3924,
	3925,
	3926,
};
static const Il2CppType* X509Certificate_t1304_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
	&IDeserializationCallback_t2213_0_0_0,
};
static Il2CppInterfaceOffsetPair X509Certificate_t1304_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &IDeserializationCallback_t2213_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X509Certificate_t1304_0_0_0;
extern const Il2CppType X509Certificate_t1304_1_0_0;
struct X509Certificate_t1304;
const Il2CppTypeDefinitionMetadata X509Certificate_t1304_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509Certificate_t1304_InterfacesTypeInfos/* implementedInterfaces */
	, X509Certificate_t1304_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Certificate_t1304_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8569/* fieldStart */
	, 12152/* methodStart */
	, -1/* eventStart */
	, 2445/* propertyStart */

};
TypeInfo X509Certificate_t1304_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Certificate"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509Certificate_t1304_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3040/* custom_attributes_cache */
	, &X509Certificate_t1304_0_0_0/* byval_arg */
	, &X509Certificate_t1304_1_0_0/* this_arg */
	, &X509Certificate_t1304_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Certificate_t1304)/* instance_size */
	, sizeof (X509Certificate_t1304)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509KeyStorageFlags
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509K.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509KeyStorageFlags
extern TypeInfo X509KeyStorageFlags_t1963_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509KeyStorageFlags
#include "mscorlib_System_Security_Cryptography_X509Certificates_X509KMethodDeclarations.h"
static const EncodedMethodIndex X509KeyStorageFlags_t1963_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair X509KeyStorageFlags_t1963_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X509KeyStorageFlags_t1963_0_0_0;
extern const Il2CppType X509KeyStorageFlags_t1963_1_0_0;
const Il2CppTypeDefinitionMetadata X509KeyStorageFlags_t1963_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509KeyStorageFlags_t1963_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, X509KeyStorageFlags_t1963_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8574/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509KeyStorageFlags_t1963_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509KeyStorageFlags"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3046/* custom_attributes_cache */
	, &X509KeyStorageFlags_t1963_0_0_0/* byval_arg */
	, &X509KeyStorageFlags_t1963_1_0_0/* this_arg */
	, &X509KeyStorageFlags_t1963_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509KeyStorageFlags_t1963)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509KeyStorageFlags_t1963)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.AsymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_AsymmetricAlgorithm.h"
// Metadata Definition System.Security.Cryptography.AsymmetricAlgorithm
extern TypeInfo AsymmetricAlgorithm_t1322_il2cpp_TypeInfo;
// System.Security.Cryptography.AsymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_AsymmetricAlgorithmMethodDeclarations.h"
static const EncodedMethodIndex AsymmetricAlgorithm_t1322_VTable[10] = 
{
	626,
	601,
	627,
	628,
	1704,
	3927,
	1706,
	0,
	0,
	0,
};
extern const Il2CppType IDisposable_t538_0_0_0;
static const Il2CppType* AsymmetricAlgorithm_t1322_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair AsymmetricAlgorithm_t1322_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsymmetricAlgorithm_t1322_0_0_0;
extern const Il2CppType AsymmetricAlgorithm_t1322_1_0_0;
struct AsymmetricAlgorithm_t1322;
const Il2CppTypeDefinitionMetadata AsymmetricAlgorithm_t1322_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AsymmetricAlgorithm_t1322_InterfacesTypeInfos/* implementedInterfaces */
	, AsymmetricAlgorithm_t1322_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsymmetricAlgorithm_t1322_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8581/* fieldStart */
	, 12176/* methodStart */
	, -1/* eventStart */
	, 2447/* propertyStart */

};
TypeInfo AsymmetricAlgorithm_t1322_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsymmetricAlgorithm"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &AsymmetricAlgorithm_t1322_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3047/* custom_attributes_cache */
	, &AsymmetricAlgorithm_t1322_0_0_0/* byval_arg */
	, &AsymmetricAlgorithm_t1322_1_0_0/* this_arg */
	, &AsymmetricAlgorithm_t1322_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsymmetricAlgorithm_t1322)/* instance_size */
	, sizeof (AsymmetricAlgorithm_t1322)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.AsymmetricKeyExchangeFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricKeyExchangeF.h"
// Metadata Definition System.Security.Cryptography.AsymmetricKeyExchangeFormatter
extern TypeInfo AsymmetricKeyExchangeFormatter_t1964_il2cpp_TypeInfo;
// System.Security.Cryptography.AsymmetricKeyExchangeFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricKeyExchangeFMethodDeclarations.h"
static const EncodedMethodIndex AsymmetricKeyExchangeFormatter_t1964_VTable[5] = 
{
	626,
	601,
	627,
	628,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsymmetricKeyExchangeFormatter_t1964_0_0_0;
extern const Il2CppType AsymmetricKeyExchangeFormatter_t1964_1_0_0;
struct AsymmetricKeyExchangeFormatter_t1964;
const Il2CppTypeDefinitionMetadata AsymmetricKeyExchangeFormatter_t1964_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsymmetricKeyExchangeFormatter_t1964_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12185/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AsymmetricKeyExchangeFormatter_t1964_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsymmetricKeyExchangeFormatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &AsymmetricKeyExchangeFormatter_t1964_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3048/* custom_attributes_cache */
	, &AsymmetricKeyExchangeFormatter_t1964_0_0_0/* byval_arg */
	, &AsymmetricKeyExchangeFormatter_t1964_1_0_0/* this_arg */
	, &AsymmetricKeyExchangeFormatter_t1964_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsymmetricKeyExchangeFormatter_t1964)/* instance_size */
	, sizeof (AsymmetricKeyExchangeFormatter_t1964)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.AsymmetricSignatureDeformatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureDef.h"
// Metadata Definition System.Security.Cryptography.AsymmetricSignatureDeformatter
extern TypeInfo AsymmetricSignatureDeformatter_t1289_il2cpp_TypeInfo;
// System.Security.Cryptography.AsymmetricSignatureDeformatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureDefMethodDeclarations.h"
static const EncodedMethodIndex AsymmetricSignatureDeformatter_t1289_VTable[7] = 
{
	626,
	601,
	627,
	628,
	0,
	0,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsymmetricSignatureDeformatter_t1289_0_0_0;
extern const Il2CppType AsymmetricSignatureDeformatter_t1289_1_0_0;
struct AsymmetricSignatureDeformatter_t1289;
const Il2CppTypeDefinitionMetadata AsymmetricSignatureDeformatter_t1289_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsymmetricSignatureDeformatter_t1289_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12187/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AsymmetricSignatureDeformatter_t1289_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsymmetricSignatureDeformatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &AsymmetricSignatureDeformatter_t1289_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3049/* custom_attributes_cache */
	, &AsymmetricSignatureDeformatter_t1289_0_0_0/* byval_arg */
	, &AsymmetricSignatureDeformatter_t1289_1_0_0/* this_arg */
	, &AsymmetricSignatureDeformatter_t1289_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsymmetricSignatureDeformatter_t1289)/* instance_size */
	, sizeof (AsymmetricSignatureDeformatter_t1289)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.AsymmetricSignatureFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureFor.h"
// Metadata Definition System.Security.Cryptography.AsymmetricSignatureFormatter
extern TypeInfo AsymmetricSignatureFormatter_t1291_il2cpp_TypeInfo;
// System.Security.Cryptography.AsymmetricSignatureFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureForMethodDeclarations.h"
static const EncodedMethodIndex AsymmetricSignatureFormatter_t1291_VTable[7] = 
{
	626,
	601,
	627,
	628,
	0,
	0,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsymmetricSignatureFormatter_t1291_0_0_0;
extern const Il2CppType AsymmetricSignatureFormatter_t1291_1_0_0;
struct AsymmetricSignatureFormatter_t1291;
const Il2CppTypeDefinitionMetadata AsymmetricSignatureFormatter_t1291_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsymmetricSignatureFormatter_t1291_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12191/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AsymmetricSignatureFormatter_t1291_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsymmetricSignatureFormatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &AsymmetricSignatureFormatter_t1291_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3050/* custom_attributes_cache */
	, &AsymmetricSignatureFormatter_t1291_0_0_0/* byval_arg */
	, &AsymmetricSignatureFormatter_t1291_1_0_0/* this_arg */
	, &AsymmetricSignatureFormatter_t1291_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsymmetricSignatureFormatter_t1291)/* instance_size */
	, sizeof (AsymmetricSignatureFormatter_t1291)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.Base64Constants
#include "mscorlib_System_Security_Cryptography_Base64Constants.h"
// Metadata Definition System.Security.Cryptography.Base64Constants
extern TypeInfo Base64Constants_t1965_il2cpp_TypeInfo;
// System.Security.Cryptography.Base64Constants
#include "mscorlib_System_Security_Cryptography_Base64ConstantsMethodDeclarations.h"
static const EncodedMethodIndex Base64Constants_t1965_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Base64Constants_t1965_0_0_0;
extern const Il2CppType Base64Constants_t1965_1_0_0;
struct Base64Constants_t1965;
const Il2CppTypeDefinitionMetadata Base64Constants_t1965_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Base64Constants_t1965_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8583/* fieldStart */
	, 12195/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Base64Constants_t1965_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Base64Constants"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &Base64Constants_t1965_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Base64Constants_t1965_0_0_0/* byval_arg */
	, &Base64Constants_t1965_1_0_0/* this_arg */
	, &Base64Constants_t1965_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Base64Constants_t1965)/* instance_size */
	, sizeof (Base64Constants_t1965)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Base64Constants_t1965_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.CipherMode
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
// Metadata Definition System.Security.Cryptography.CipherMode
extern TypeInfo CipherMode_t1190_il2cpp_TypeInfo;
// System.Security.Cryptography.CipherMode
#include "mscorlib_System_Security_Cryptography_CipherModeMethodDeclarations.h"
static const EncodedMethodIndex CipherMode_t1190_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair CipherMode_t1190_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CipherMode_t1190_0_0_0;
extern const Il2CppType CipherMode_t1190_1_0_0;
const Il2CppTypeDefinitionMetadata CipherMode_t1190_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CipherMode_t1190_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, CipherMode_t1190_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8585/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CipherMode_t1190_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CipherMode"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3051/* custom_attributes_cache */
	, &CipherMode_t1190_0_0_0/* byval_arg */
	, &CipherMode_t1190_1_0_0/* this_arg */
	, &CipherMode_t1190_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CipherMode_t1190)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CipherMode_t1190)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.CryptoConfig
#include "mscorlib_System_Security_Cryptography_CryptoConfig.h"
// Metadata Definition System.Security.Cryptography.CryptoConfig
extern TypeInfo CryptoConfig_t1337_il2cpp_TypeInfo;
// System.Security.Cryptography.CryptoConfig
#include "mscorlib_System_Security_Cryptography_CryptoConfigMethodDeclarations.h"
static const EncodedMethodIndex CryptoConfig_t1337_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CryptoConfig_t1337_0_0_0;
extern const Il2CppType CryptoConfig_t1337_1_0_0;
struct CryptoConfig_t1337;
const Il2CppTypeDefinitionMetadata CryptoConfig_t1337_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CryptoConfig_t1337_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8591/* fieldStart */
	, 12196/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CryptoConfig_t1337_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CryptoConfig"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &CryptoConfig_t1337_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3052/* custom_attributes_cache */
	, &CryptoConfig_t1337_0_0_0/* byval_arg */
	, &CryptoConfig_t1337_1_0_0/* this_arg */
	, &CryptoConfig_t1337_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CryptoConfig_t1337)/* instance_size */
	, sizeof (CryptoConfig_t1337)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CryptoConfig_t1337_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.CryptographicException
#include "mscorlib_System_Security_Cryptography_CryptographicException.h"
// Metadata Definition System.Security.Cryptography.CryptographicException
extern TypeInfo CryptographicException_t1189_il2cpp_TypeInfo;
// System.Security.Cryptography.CryptographicException
#include "mscorlib_System_Security_Cryptography_CryptographicExceptionMethodDeclarations.h"
static const EncodedMethodIndex CryptographicException_t1189_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static const Il2CppType* CryptographicException_t1189_InterfacesTypeInfos[] = 
{
	&_Exception_t3443_0_0_0,
};
static Il2CppInterfaceOffsetPair CryptographicException_t1189_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CryptographicException_t1189_0_0_0;
extern const Il2CppType CryptographicException_t1189_1_0_0;
struct CryptographicException_t1189;
const Il2CppTypeDefinitionMetadata CryptographicException_t1189_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CryptographicException_t1189_InterfacesTypeInfos/* implementedInterfaces */
	, CryptographicException_t1189_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, CryptographicException_t1189_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12203/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CryptographicException_t1189_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CryptographicException"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &CryptographicException_t1189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3054/* custom_attributes_cache */
	, &CryptographicException_t1189_0_0_0/* byval_arg */
	, &CryptographicException_t1189_1_0_0/* this_arg */
	, &CryptographicException_t1189_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CryptographicException_t1189)/* instance_size */
	, sizeof (CryptographicException_t1189)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.CryptographicUnexpectedOperationException
#include "mscorlib_System_Security_Cryptography_CryptographicUnexpecte.h"
// Metadata Definition System.Security.Cryptography.CryptographicUnexpectedOperationException
extern TypeInfo CryptographicUnexpectedOperationException_t1352_il2cpp_TypeInfo;
// System.Security.Cryptography.CryptographicUnexpectedOperationException
#include "mscorlib_System_Security_Cryptography_CryptographicUnexpecteMethodDeclarations.h"
static const EncodedMethodIndex CryptographicUnexpectedOperationException_t1352_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair CryptographicUnexpectedOperationException_t1352_InterfacesOffsets[] = 
{
	{ &_Exception_t3443_0_0_0, 5},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CryptographicUnexpectedOperationException_t1352_0_0_0;
extern const Il2CppType CryptographicUnexpectedOperationException_t1352_1_0_0;
struct CryptographicUnexpectedOperationException_t1352;
const Il2CppTypeDefinitionMetadata CryptographicUnexpectedOperationException_t1352_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CryptographicUnexpectedOperationException_t1352_InterfacesOffsets/* interfaceOffsets */
	, &CryptographicException_t1189_0_0_0/* parent */
	, CryptographicUnexpectedOperationException_t1352_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12208/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CryptographicUnexpectedOperationException_t1352_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CryptographicUnexpectedOperationException"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &CryptographicUnexpectedOperationException_t1352_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3055/* custom_attributes_cache */
	, &CryptographicUnexpectedOperationException_t1352_0_0_0/* byval_arg */
	, &CryptographicUnexpectedOperationException_t1352_1_0_0/* this_arg */
	, &CryptographicUnexpectedOperationException_t1352_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CryptographicUnexpectedOperationException_t1352)/* instance_size */
	, sizeof (CryptographicUnexpectedOperationException_t1352)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.CspParameters
#include "mscorlib_System_Security_Cryptography_CspParameters.h"
// Metadata Definition System.Security.Cryptography.CspParameters
extern TypeInfo CspParameters_t1339_il2cpp_TypeInfo;
// System.Security.Cryptography.CspParameters
#include "mscorlib_System_Security_Cryptography_CspParametersMethodDeclarations.h"
static const EncodedMethodIndex CspParameters_t1339_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CspParameters_t1339_0_0_0;
extern const Il2CppType CspParameters_t1339_1_0_0;
struct CspParameters_t1339;
const Il2CppTypeDefinitionMetadata CspParameters_t1339_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CspParameters_t1339_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8594/* fieldStart */
	, 12211/* methodStart */
	, -1/* eventStart */
	, 2448/* propertyStart */

};
TypeInfo CspParameters_t1339_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CspParameters"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &CspParameters_t1339_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3056/* custom_attributes_cache */
	, &CspParameters_t1339_0_0_0/* byval_arg */
	, &CspParameters_t1339_1_0_0/* this_arg */
	, &CspParameters_t1339_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CspParameters_t1339)/* instance_size */
	, sizeof (CspParameters_t1339)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.CspProviderFlags
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
// Metadata Definition System.Security.Cryptography.CspProviderFlags
extern TypeInfo CspProviderFlags_t1966_il2cpp_TypeInfo;
// System.Security.Cryptography.CspProviderFlags
#include "mscorlib_System_Security_Cryptography_CspProviderFlagsMethodDeclarations.h"
static const EncodedMethodIndex CspProviderFlags_t1966_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair CspProviderFlags_t1966_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CspProviderFlags_t1966_0_0_0;
extern const Il2CppType CspProviderFlags_t1966_1_0_0;
const Il2CppTypeDefinitionMetadata CspProviderFlags_t1966_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CspProviderFlags_t1966_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, CspProviderFlags_t1966_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8599/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CspProviderFlags_t1966_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CspProviderFlags"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3057/* custom_attributes_cache */
	, &CspProviderFlags_t1966_0_0_0/* byval_arg */
	, &CspProviderFlags_t1966_1_0_0/* this_arg */
	, &CspProviderFlags_t1966_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CspProviderFlags_t1966)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CspProviderFlags_t1966)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.DES
#include "mscorlib_System_Security_Cryptography_DES.h"
// Metadata Definition System.Security.Cryptography.DES
extern TypeInfo DES_t1354_il2cpp_TypeInfo;
// System.Security.Cryptography.DES
#include "mscorlib_System_Security_Cryptography_DESMethodDeclarations.h"
static const EncodedMethodIndex DES_t1354_VTable[26] = 
{
	626,
	1629,
	627,
	628,
	1630,
	1631,
	1632,
	1633,
	1634,
	1635,
	1636,
	3928,
	3929,
	1639,
	1640,
	1641,
	1642,
	1643,
	1644,
	1645,
	1646,
	0,
	1647,
	0,
	0,
	0,
};
static Il2CppInterfaceOffsetPair DES_t1354_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DES_t1354_0_0_0;
extern const Il2CppType DES_t1354_1_0_0;
extern const Il2CppType SymmetricAlgorithm_t1176_0_0_0;
struct DES_t1354;
const Il2CppTypeDefinitionMetadata DES_t1354_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DES_t1354_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricAlgorithm_t1176_0_0_0/* parent */
	, DES_t1354_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8608/* fieldStart */
	, 12217/* methodStart */
	, -1/* eventStart */
	, 2449/* propertyStart */

};
TypeInfo DES_t1354_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DES"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DES_t1354_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3058/* custom_attributes_cache */
	, &DES_t1354_0_0_0/* byval_arg */
	, &DES_t1354_1_0_0/* this_arg */
	, &DES_t1354_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DES_t1354)/* instance_size */
	, sizeof (DES_t1354)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DES_t1354_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.DESTransform
#include "mscorlib_System_Security_Cryptography_DESTransform.h"
// Metadata Definition System.Security.Cryptography.DESTransform
extern TypeInfo DESTransform_t1968_il2cpp_TypeInfo;
// System.Security.Cryptography.DESTransform
#include "mscorlib_System_Security_Cryptography_DESTransformMethodDeclarations.h"
static const EncodedMethodIndex DESTransform_t1968_VTable[18] = 
{
	626,
	2740,
	627,
	628,
	2741,
	2742,
	2743,
	2744,
	2745,
	2742,
	2746,
	3930,
	2747,
	2748,
	2749,
	2750,
	2743,
	2744,
};
extern const Il2CppType ICryptoTransform_t1188_0_0_0;
static Il2CppInterfaceOffsetPair DESTransform_t1968_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DESTransform_t1968_0_0_0;
extern const Il2CppType DESTransform_t1968_1_0_0;
extern const Il2CppType SymmetricTransform_t1622_0_0_0;
struct DESTransform_t1968;
const Il2CppTypeDefinitionMetadata DESTransform_t1968_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DESTransform_t1968_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricTransform_t1622_0_0_0/* parent */
	, DESTransform_t1968_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8610/* fieldStart */
	, 12225/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DESTransform_t1968_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DESTransform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DESTransform_t1968_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DESTransform_t1968_0_0_0/* byval_arg */
	, &DESTransform_t1968_1_0_0/* this_arg */
	, &DESTransform_t1968_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DESTransform_t1968)/* instance_size */
	, sizeof (DESTransform_t1968)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DESTransform_t1968_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.DESCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_DESCryptoServiceProvid.h"
// Metadata Definition System.Security.Cryptography.DESCryptoServiceProvider
extern TypeInfo DESCryptoServiceProvider_t1969_il2cpp_TypeInfo;
// System.Security.Cryptography.DESCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_DESCryptoServiceProvidMethodDeclarations.h"
static const EncodedMethodIndex DESCryptoServiceProvider_t1969_VTable[26] = 
{
	626,
	1629,
	627,
	628,
	1630,
	1631,
	1632,
	1633,
	1634,
	1635,
	1636,
	3928,
	3929,
	1639,
	1640,
	1641,
	1642,
	1643,
	1644,
	1645,
	1646,
	3931,
	1647,
	3932,
	3933,
	3934,
};
static Il2CppInterfaceOffsetPair DESCryptoServiceProvider_t1969_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DESCryptoServiceProvider_t1969_0_0_0;
extern const Il2CppType DESCryptoServiceProvider_t1969_1_0_0;
struct DESCryptoServiceProvider_t1969;
const Il2CppTypeDefinitionMetadata DESCryptoServiceProvider_t1969_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DESCryptoServiceProvider_t1969_InterfacesOffsets/* interfaceOffsets */
	, &DES_t1354_0_0_0/* parent */
	, DESCryptoServiceProvider_t1969_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12234/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DESCryptoServiceProvider_t1969_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DESCryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DESCryptoServiceProvider_t1969_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3059/* custom_attributes_cache */
	, &DESCryptoServiceProvider_t1969_0_0_0/* byval_arg */
	, &DESCryptoServiceProvider_t1969_1_0_0/* this_arg */
	, &DESCryptoServiceProvider_t1969_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DESCryptoServiceProvider_t1969)/* instance_size */
	, sizeof (DESCryptoServiceProvider_t1969)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.DSA
#include "mscorlib_System_Security_Cryptography_DSA.h"
// Metadata Definition System.Security.Cryptography.DSA
extern TypeInfo DSA_t1232_il2cpp_TypeInfo;
// System.Security.Cryptography.DSA
#include "mscorlib_System_Security_Cryptography_DSAMethodDeclarations.h"
static const EncodedMethodIndex DSA_t1232_VTable[14] = 
{
	626,
	601,
	627,
	628,
	1704,
	3927,
	1706,
	0,
	2720,
	2721,
	0,
	0,
	0,
	0,
};
static Il2CppInterfaceOffsetPair DSA_t1232_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSA_t1232_0_0_0;
extern const Il2CppType DSA_t1232_1_0_0;
struct DSA_t1232;
const Il2CppTypeDefinitionMetadata DSA_t1232_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DSA_t1232_InterfacesOffsets/* interfaceOffsets */
	, &AsymmetricAlgorithm_t1322_0_0_0/* parent */
	, DSA_t1232_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12239/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DSA_t1232_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSA"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DSA_t1232_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3060/* custom_attributes_cache */
	, &DSA_t1232_0_0_0/* byval_arg */
	, &DSA_t1232_1_0_0/* this_arg */
	, &DSA_t1232_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DSA_t1232)/* instance_size */
	, sizeof (DSA_t1232)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.DSACryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_DSACryptoServiceProvid.h"
// Metadata Definition System.Security.Cryptography.DSACryptoServiceProvider
extern TypeInfo DSACryptoServiceProvider_t1343_il2cpp_TypeInfo;
// System.Security.Cryptography.DSACryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_DSACryptoServiceProvidMethodDeclarations.h"
static const EncodedMethodIndex DSACryptoServiceProvider_t1343_VTable[14] = 
{
	626,
	3935,
	627,
	628,
	1704,
	3936,
	1706,
	3937,
	2720,
	2721,
	3938,
	3939,
	3940,
	3941,
};
extern const Il2CppType ICspAsymmetricAlgorithm_t3493_0_0_0;
static const Il2CppType* DSACryptoServiceProvider_t1343_InterfacesTypeInfos[] = 
{
	&ICspAsymmetricAlgorithm_t3493_0_0_0,
};
static Il2CppInterfaceOffsetPair DSACryptoServiceProvider_t1343_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICspAsymmetricAlgorithm_t3493_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSACryptoServiceProvider_t1343_0_0_0;
extern const Il2CppType DSACryptoServiceProvider_t1343_1_0_0;
struct DSACryptoServiceProvider_t1343;
const Il2CppTypeDefinitionMetadata DSACryptoServiceProvider_t1343_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, DSACryptoServiceProvider_t1343_InterfacesTypeInfos/* implementedInterfaces */
	, DSACryptoServiceProvider_t1343_InterfacesOffsets/* interfaceOffsets */
	, &DSA_t1232_0_0_0/* parent */
	, DSACryptoServiceProvider_t1343_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8623/* fieldStart */
	, 12249/* methodStart */
	, -1/* eventStart */
	, 2450/* propertyStart */

};
TypeInfo DSACryptoServiceProvider_t1343_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSACryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DSACryptoServiceProvider_t1343_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3061/* custom_attributes_cache */
	, &DSACryptoServiceProvider_t1343_0_0_0/* byval_arg */
	, &DSACryptoServiceProvider_t1343_1_0_0/* this_arg */
	, &DSACryptoServiceProvider_t1343_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DSACryptoServiceProvider_t1343)/* instance_size */
	, sizeof (DSACryptoServiceProvider_t1343)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DSACryptoServiceProvider_t1343_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.DSAParameters
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
// Metadata Definition System.Security.Cryptography.DSAParameters
extern TypeInfo DSAParameters_t1334_il2cpp_TypeInfo;
// System.Security.Cryptography.DSAParameters
#include "mscorlib_System_Security_Cryptography_DSAParametersMethodDeclarations.h"
static const EncodedMethodIndex DSAParameters_t1334_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSAParameters_t1334_0_0_0;
extern const Il2CppType DSAParameters_t1334_1_0_0;
const Il2CppTypeDefinitionMetadata DSAParameters_t1334_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, DSAParameters_t1334_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8630/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DSAParameters_t1334_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSAParameters"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DSAParameters_t1334_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3063/* custom_attributes_cache */
	, &DSAParameters_t1334_0_0_0/* byval_arg */
	, &DSAParameters_t1334_1_0_0/* this_arg */
	, &DSAParameters_t1334_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)DSAParameters_t1334_marshal/* marshal_to_native_func */
	, (methodPointerType)DSAParameters_t1334_marshal_back/* marshal_from_native_func */
	, (methodPointerType)DSAParameters_t1334_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (DSAParameters_t1334)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DSAParameters_t1334)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(DSAParameters_t1334_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.DSASignatureDeformatter
#include "mscorlib_System_Security_Cryptography_DSASignatureDeformatte.h"
// Metadata Definition System.Security.Cryptography.DSASignatureDeformatter
extern TypeInfo DSASignatureDeformatter_t1347_il2cpp_TypeInfo;
// System.Security.Cryptography.DSASignatureDeformatter
#include "mscorlib_System_Security_Cryptography_DSASignatureDeformatteMethodDeclarations.h"
static const EncodedMethodIndex DSASignatureDeformatter_t1347_VTable[7] = 
{
	626,
	601,
	627,
	628,
	3942,
	3943,
	3944,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSASignatureDeformatter_t1347_0_0_0;
extern const Il2CppType DSASignatureDeformatter_t1347_1_0_0;
struct DSASignatureDeformatter_t1347;
const Il2CppTypeDefinitionMetadata DSASignatureDeformatter_t1347_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsymmetricSignatureDeformatter_t1289_0_0_0/* parent */
	, DSASignatureDeformatter_t1347_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8638/* fieldStart */
	, 12262/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DSASignatureDeformatter_t1347_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSASignatureDeformatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DSASignatureDeformatter_t1347_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3064/* custom_attributes_cache */
	, &DSASignatureDeformatter_t1347_0_0_0/* byval_arg */
	, &DSASignatureDeformatter_t1347_1_0_0/* this_arg */
	, &DSASignatureDeformatter_t1347_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DSASignatureDeformatter_t1347)/* instance_size */
	, sizeof (DSASignatureDeformatter_t1347)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.DSASignatureFormatter
#include "mscorlib_System_Security_Cryptography_DSASignatureFormatter.h"
// Metadata Definition System.Security.Cryptography.DSASignatureFormatter
extern TypeInfo DSASignatureFormatter_t1970_il2cpp_TypeInfo;
// System.Security.Cryptography.DSASignatureFormatter
#include "mscorlib_System_Security_Cryptography_DSASignatureFormatterMethodDeclarations.h"
static const EncodedMethodIndex DSASignatureFormatter_t1970_VTable[7] = 
{
	626,
	601,
	627,
	628,
	3945,
	3946,
	3947,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSASignatureFormatter_t1970_0_0_0;
extern const Il2CppType DSASignatureFormatter_t1970_1_0_0;
struct DSASignatureFormatter_t1970;
const Il2CppTypeDefinitionMetadata DSASignatureFormatter_t1970_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsymmetricSignatureFormatter_t1291_0_0_0/* parent */
	, DSASignatureFormatter_t1970_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8639/* fieldStart */
	, 12267/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DSASignatureFormatter_t1970_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSASignatureFormatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DSASignatureFormatter_t1970_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3065/* custom_attributes_cache */
	, &DSASignatureFormatter_t1970_0_0_0/* byval_arg */
	, &DSASignatureFormatter_t1970_1_0_0/* this_arg */
	, &DSASignatureFormatter_t1970_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DSASignatureFormatter_t1970)/* instance_size */
	, sizeof (DSASignatureFormatter_t1970)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.HMAC
#include "mscorlib_System_Security_Cryptography_HMAC.h"
// Metadata Definition System.Security.Cryptography.HMAC
extern TypeInfo HMAC_t1342_il2cpp_TypeInfo;
// System.Security.Cryptography.HMAC
#include "mscorlib_System_Security_Cryptography_HMACMethodDeclarations.h"
static const EncodedMethodIndex HMAC_t1342_VTable[17] = 
{
	626,
	1784,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	3948,
	3949,
	1698,
	3950,
	3951,
	3952,
	3953,
};
static Il2CppInterfaceOffsetPair HMAC_t1342_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMAC_t1342_0_0_0;
extern const Il2CppType HMAC_t1342_1_0_0;
extern const Il2CppType KeyedHashAlgorithm_t1255_0_0_0;
struct HMAC_t1342;
const Il2CppTypeDefinitionMetadata HMAC_t1342_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMAC_t1342_InterfacesOffsets/* interfaceOffsets */
	, &KeyedHashAlgorithm_t1255_0_0_0/* parent */
	, HMAC_t1342_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8640/* fieldStart */
	, 12271/* methodStart */
	, -1/* eventStart */
	, 2452/* propertyStart */

};
TypeInfo HMAC_t1342_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMAC"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HMAC_t1342_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3066/* custom_attributes_cache */
	, &HMAC_t1342_0_0_0/* byval_arg */
	, &HMAC_t1342_1_0_0/* this_arg */
	, &HMAC_t1342_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMAC_t1342)/* instance_size */
	, sizeof (HMAC_t1342)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACMD5
#include "mscorlib_System_Security_Cryptography_HMACMD5.h"
// Metadata Definition System.Security.Cryptography.HMACMD5
extern TypeInfo HMACMD5_t1971_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACMD5
#include "mscorlib_System_Security_Cryptography_HMACMD5MethodDeclarations.h"
static const EncodedMethodIndex HMACMD5_t1971_VTable[17] = 
{
	626,
	1784,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	3948,
	3949,
	1698,
	3950,
	3951,
	3952,
	3953,
};
static Il2CppInterfaceOffsetPair HMACMD5_t1971_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACMD5_t1971_0_0_0;
extern const Il2CppType HMACMD5_t1971_1_0_0;
struct HMACMD5_t1971;
const Il2CppTypeDefinitionMetadata HMACMD5_t1971_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACMD5_t1971_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t1342_0_0_0/* parent */
	, HMACMD5_t1971_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12285/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HMACMD5_t1971_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACMD5"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HMACMD5_t1971_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3067/* custom_attributes_cache */
	, &HMACMD5_t1971_0_0_0/* byval_arg */
	, &HMACMD5_t1971_1_0_0/* this_arg */
	, &HMACMD5_t1971_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACMD5_t1971)/* instance_size */
	, sizeof (HMACMD5_t1971)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACRIPEMD160
#include "mscorlib_System_Security_Cryptography_HMACRIPEMD160.h"
// Metadata Definition System.Security.Cryptography.HMACRIPEMD160
extern TypeInfo HMACRIPEMD160_t1972_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACRIPEMD160
#include "mscorlib_System_Security_Cryptography_HMACRIPEMD160MethodDeclarations.h"
static const EncodedMethodIndex HMACRIPEMD160_t1972_VTable[17] = 
{
	626,
	1784,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	3948,
	3949,
	1698,
	3950,
	3951,
	3952,
	3953,
};
static Il2CppInterfaceOffsetPair HMACRIPEMD160_t1972_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACRIPEMD160_t1972_0_0_0;
extern const Il2CppType HMACRIPEMD160_t1972_1_0_0;
struct HMACRIPEMD160_t1972;
const Il2CppTypeDefinitionMetadata HMACRIPEMD160_t1972_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACRIPEMD160_t1972_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t1342_0_0_0/* parent */
	, HMACRIPEMD160_t1972_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12287/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HMACRIPEMD160_t1972_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACRIPEMD160"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HMACRIPEMD160_t1972_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3068/* custom_attributes_cache */
	, &HMACRIPEMD160_t1972_0_0_0/* byval_arg */
	, &HMACRIPEMD160_t1972_1_0_0/* this_arg */
	, &HMACRIPEMD160_t1972_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACRIPEMD160_t1972)/* instance_size */
	, sizeof (HMACRIPEMD160_t1972)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACSHA1
#include "mscorlib_System_Security_Cryptography_HMACSHA1.h"
// Metadata Definition System.Security.Cryptography.HMACSHA1
extern TypeInfo HMACSHA1_t1341_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACSHA1
#include "mscorlib_System_Security_Cryptography_HMACSHA1MethodDeclarations.h"
static const EncodedMethodIndex HMACSHA1_t1341_VTable[17] = 
{
	626,
	1784,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	3948,
	3949,
	1698,
	3950,
	3951,
	3952,
	3953,
};
static Il2CppInterfaceOffsetPair HMACSHA1_t1341_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACSHA1_t1341_0_0_0;
extern const Il2CppType HMACSHA1_t1341_1_0_0;
struct HMACSHA1_t1341;
const Il2CppTypeDefinitionMetadata HMACSHA1_t1341_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACSHA1_t1341_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t1342_0_0_0/* parent */
	, HMACSHA1_t1341_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12289/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HMACSHA1_t1341_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACSHA1"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HMACSHA1_t1341_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3069/* custom_attributes_cache */
	, &HMACSHA1_t1341_0_0_0/* byval_arg */
	, &HMACSHA1_t1341_1_0_0/* this_arg */
	, &HMACSHA1_t1341_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACSHA1_t1341)/* instance_size */
	, sizeof (HMACSHA1_t1341)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACSHA256
#include "mscorlib_System_Security_Cryptography_HMACSHA256.h"
// Metadata Definition System.Security.Cryptography.HMACSHA256
extern TypeInfo HMACSHA256_t1973_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACSHA256
#include "mscorlib_System_Security_Cryptography_HMACSHA256MethodDeclarations.h"
static const EncodedMethodIndex HMACSHA256_t1973_VTable[17] = 
{
	626,
	1784,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	3948,
	3949,
	1698,
	3950,
	3951,
	3952,
	3953,
};
static Il2CppInterfaceOffsetPair HMACSHA256_t1973_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACSHA256_t1973_0_0_0;
extern const Il2CppType HMACSHA256_t1973_1_0_0;
struct HMACSHA256_t1973;
const Il2CppTypeDefinitionMetadata HMACSHA256_t1973_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACSHA256_t1973_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t1342_0_0_0/* parent */
	, HMACSHA256_t1973_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12291/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HMACSHA256_t1973_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACSHA256"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HMACSHA256_t1973_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3070/* custom_attributes_cache */
	, &HMACSHA256_t1973_0_0_0/* byval_arg */
	, &HMACSHA256_t1973_1_0_0/* this_arg */
	, &HMACSHA256_t1973_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACSHA256_t1973)/* instance_size */
	, sizeof (HMACSHA256_t1973)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACSHA384
#include "mscorlib_System_Security_Cryptography_HMACSHA384.h"
// Metadata Definition System.Security.Cryptography.HMACSHA384
extern TypeInfo HMACSHA384_t1974_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACSHA384
#include "mscorlib_System_Security_Cryptography_HMACSHA384MethodDeclarations.h"
static const EncodedMethodIndex HMACSHA384_t1974_VTable[17] = 
{
	626,
	1784,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	3948,
	3949,
	1698,
	3950,
	3951,
	3952,
	3953,
};
static Il2CppInterfaceOffsetPair HMACSHA384_t1974_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACSHA384_t1974_0_0_0;
extern const Il2CppType HMACSHA384_t1974_1_0_0;
struct HMACSHA384_t1974;
const Il2CppTypeDefinitionMetadata HMACSHA384_t1974_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACSHA384_t1974_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t1342_0_0_0/* parent */
	, HMACSHA384_t1974_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8645/* fieldStart */
	, 12293/* methodStart */
	, -1/* eventStart */
	, 2456/* propertyStart */

};
TypeInfo HMACSHA384_t1974_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACSHA384"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HMACSHA384_t1974_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3071/* custom_attributes_cache */
	, &HMACSHA384_t1974_0_0_0/* byval_arg */
	, &HMACSHA384_t1974_1_0_0/* this_arg */
	, &HMACSHA384_t1974_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACSHA384_t1974)/* instance_size */
	, sizeof (HMACSHA384_t1974)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(HMACSHA384_t1974_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HMACSHA512
#include "mscorlib_System_Security_Cryptography_HMACSHA512.h"
// Metadata Definition System.Security.Cryptography.HMACSHA512
extern TypeInfo HMACSHA512_t1975_il2cpp_TypeInfo;
// System.Security.Cryptography.HMACSHA512
#include "mscorlib_System_Security_Cryptography_HMACSHA512MethodDeclarations.h"
static const EncodedMethodIndex HMACSHA512_t1975_VTable[17] = 
{
	626,
	1784,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	3948,
	3949,
	1698,
	3950,
	3951,
	3952,
	3953,
};
static Il2CppInterfaceOffsetPair HMACSHA512_t1975_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HMACSHA512_t1975_0_0_0;
extern const Il2CppType HMACSHA512_t1975_1_0_0;
struct HMACSHA512_t1975;
const Il2CppTypeDefinitionMetadata HMACSHA512_t1975_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMACSHA512_t1975_InterfacesOffsets/* interfaceOffsets */
	, &HMAC_t1342_0_0_0/* parent */
	, HMACSHA512_t1975_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8647/* fieldStart */
	, 12297/* methodStart */
	, -1/* eventStart */
	, 2457/* propertyStart */

};
TypeInfo HMACSHA512_t1975_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMACSHA512"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HMACSHA512_t1975_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3072/* custom_attributes_cache */
	, &HMACSHA512_t1975_0_0_0/* byval_arg */
	, &HMACSHA512_t1975_1_0_0/* this_arg */
	, &HMACSHA512_t1975_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMACSHA512_t1975)/* instance_size */
	, sizeof (HMACSHA512_t1975)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(HMACSHA512_t1975_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.HashAlgorithm
#include "mscorlib_System_Security_Cryptography_HashAlgorithm.h"
// Metadata Definition System.Security.Cryptography.HashAlgorithm
extern TypeInfo HashAlgorithm_t1217_il2cpp_TypeInfo;
// System.Security.Cryptography.HashAlgorithm
#include "mscorlib_System_Security_Cryptography_HashAlgorithmMethodDeclarations.h"
static const EncodedMethodIndex HashAlgorithm_t1217_VTable[15] = 
{
	626,
	601,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	0,
	0,
	1698,
	0,
	1699,
};
static const Il2CppType* HashAlgorithm_t1217_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
	&ICryptoTransform_t1188_0_0_0,
};
static Il2CppInterfaceOffsetPair HashAlgorithm_t1217_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HashAlgorithm_t1217_0_0_0;
extern const Il2CppType HashAlgorithm_t1217_1_0_0;
struct HashAlgorithm_t1217;
const Il2CppTypeDefinitionMetadata HashAlgorithm_t1217_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, HashAlgorithm_t1217_InterfacesTypeInfos/* implementedInterfaces */
	, HashAlgorithm_t1217_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, HashAlgorithm_t1217_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8649/* fieldStart */
	, 12301/* methodStart */
	, -1/* eventStart */
	, 2458/* propertyStart */

};
TypeInfo HashAlgorithm_t1217_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HashAlgorithm"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HashAlgorithm_t1217_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3073/* custom_attributes_cache */
	, &HashAlgorithm_t1217_0_0_0/* byval_arg */
	, &HashAlgorithm_t1217_1_0_0/* this_arg */
	, &HashAlgorithm_t1217_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HashAlgorithm_t1217)/* instance_size */
	, sizeof (HashAlgorithm_t1217)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Security.Cryptography.ICryptoTransform
extern TypeInfo ICryptoTransform_t1188_il2cpp_TypeInfo;
static const Il2CppType* ICryptoTransform_t1188_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICryptoTransform_t1188_1_0_0;
struct ICryptoTransform_t1188;
const Il2CppTypeDefinitionMetadata ICryptoTransform_t1188_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ICryptoTransform_t1188_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12315/* methodStart */
	, -1/* eventStart */
	, 2461/* propertyStart */

};
TypeInfo ICryptoTransform_t1188_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICryptoTransform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &ICryptoTransform_t1188_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3074/* custom_attributes_cache */
	, &ICryptoTransform_t1188_0_0_0/* byval_arg */
	, &ICryptoTransform_t1188_1_0_0/* this_arg */
	, &ICryptoTransform_t1188_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Security.Cryptography.ICspAsymmetricAlgorithm
extern TypeInfo ICspAsymmetricAlgorithm_t3493_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICspAsymmetricAlgorithm_t3493_1_0_0;
struct ICspAsymmetricAlgorithm_t3493;
const Il2CppTypeDefinitionMetadata ICspAsymmetricAlgorithm_t3493_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ICspAsymmetricAlgorithm_t3493_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICspAsymmetricAlgorithm"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &ICspAsymmetricAlgorithm_t3493_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3075/* custom_attributes_cache */
	, &ICspAsymmetricAlgorithm_t3493_0_0_0/* byval_arg */
	, &ICspAsymmetricAlgorithm_t3493_1_0_0/* this_arg */
	, &ICspAsymmetricAlgorithm_t3493_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.KeySizes
#include "mscorlib_System_Security_Cryptography_KeySizes.h"
// Metadata Definition System.Security.Cryptography.KeySizes
extern TypeInfo KeySizes_t1195_il2cpp_TypeInfo;
// System.Security.Cryptography.KeySizes
#include "mscorlib_System_Security_Cryptography_KeySizesMethodDeclarations.h"
static const EncodedMethodIndex KeySizes_t1195_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeySizes_t1195_0_0_0;
extern const Il2CppType KeySizes_t1195_1_0_0;
struct KeySizes_t1195;
const Il2CppTypeDefinitionMetadata KeySizes_t1195_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, KeySizes_t1195_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8653/* fieldStart */
	, 12318/* methodStart */
	, -1/* eventStart */
	, 2462/* propertyStart */

};
TypeInfo KeySizes_t1195_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeySizes"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &KeySizes_t1195_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeySizes_t1195_0_0_0/* byval_arg */
	, &KeySizes_t1195_1_0_0/* this_arg */
	, &KeySizes_t1195_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeySizes_t1195)/* instance_size */
	, sizeof (KeySizes_t1195)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.KeyedHashAlgorithm
#include "mscorlib_System_Security_Cryptography_KeyedHashAlgorithm.h"
// Metadata Definition System.Security.Cryptography.KeyedHashAlgorithm
extern TypeInfo KeyedHashAlgorithm_t1255_il2cpp_TypeInfo;
// System.Security.Cryptography.KeyedHashAlgorithm
#include "mscorlib_System_Security_Cryptography_KeyedHashAlgorithmMethodDeclarations.h"
static const EncodedMethodIndex KeyedHashAlgorithm_t1255_VTable[17] = 
{
	626,
	1784,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	0,
	0,
	1698,
	0,
	1788,
	3954,
	3955,
};
static Il2CppInterfaceOffsetPair KeyedHashAlgorithm_t1255_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyedHashAlgorithm_t1255_1_0_0;
struct KeyedHashAlgorithm_t1255;
const Il2CppTypeDefinitionMetadata KeyedHashAlgorithm_t1255_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, KeyedHashAlgorithm_t1255_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t1217_0_0_0/* parent */
	, KeyedHashAlgorithm_t1255_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8656/* fieldStart */
	, 12324/* methodStart */
	, -1/* eventStart */
	, 2465/* propertyStart */

};
TypeInfo KeyedHashAlgorithm_t1255_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyedHashAlgorithm"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &KeyedHashAlgorithm_t1255_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3076/* custom_attributes_cache */
	, &KeyedHashAlgorithm_t1255_0_0_0/* byval_arg */
	, &KeyedHashAlgorithm_t1255_1_0_0/* this_arg */
	, &KeyedHashAlgorithm_t1255_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyedHashAlgorithm_t1255)/* instance_size */
	, sizeof (KeyedHashAlgorithm_t1255)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.MACTripleDES
#include "mscorlib_System_Security_Cryptography_MACTripleDES.h"
// Metadata Definition System.Security.Cryptography.MACTripleDES
extern TypeInfo MACTripleDES_t1976_il2cpp_TypeInfo;
// System.Security.Cryptography.MACTripleDES
#include "mscorlib_System_Security_Cryptography_MACTripleDESMethodDeclarations.h"
static const EncodedMethodIndex MACTripleDES_t1976_VTable[17] = 
{
	626,
	3956,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	3957,
	3958,
	1698,
	3959,
	3960,
	3954,
	3955,
};
static Il2CppInterfaceOffsetPair MACTripleDES_t1976_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MACTripleDES_t1976_0_0_0;
extern const Il2CppType MACTripleDES_t1976_1_0_0;
struct MACTripleDES_t1976;
const Il2CppTypeDefinitionMetadata MACTripleDES_t1976_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MACTripleDES_t1976_InterfacesOffsets/* interfaceOffsets */
	, &KeyedHashAlgorithm_t1255_0_0_0/* parent */
	, MACTripleDES_t1976_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8657/* fieldStart */
	, 12330/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MACTripleDES_t1976_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MACTripleDES"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &MACTripleDES_t1976_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3077/* custom_attributes_cache */
	, &MACTripleDES_t1976_0_0_0/* byval_arg */
	, &MACTripleDES_t1976_1_0_0/* this_arg */
	, &MACTripleDES_t1976_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MACTripleDES_t1976)/* instance_size */
	, sizeof (MACTripleDES_t1976)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.MD5
#include "mscorlib_System_Security_Cryptography_MD5.h"
// Metadata Definition System.Security.Cryptography.MD5
extern TypeInfo MD5_t1344_il2cpp_TypeInfo;
// System.Security.Cryptography.MD5
#include "mscorlib_System_Security_Cryptography_MD5MethodDeclarations.h"
static const EncodedMethodIndex MD5_t1344_VTable[15] = 
{
	626,
	601,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	0,
	0,
	1698,
	0,
	1699,
};
static Il2CppInterfaceOffsetPair MD5_t1344_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MD5_t1344_0_0_0;
extern const Il2CppType MD5_t1344_1_0_0;
struct MD5_t1344;
const Il2CppTypeDefinitionMetadata MD5_t1344_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MD5_t1344_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t1217_0_0_0/* parent */
	, MD5_t1344_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12337/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MD5_t1344_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MD5"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &MD5_t1344_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3078/* custom_attributes_cache */
	, &MD5_t1344_0_0_0/* byval_arg */
	, &MD5_t1344_1_0_0/* this_arg */
	, &MD5_t1344_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MD5_t1344)/* instance_size */
	, sizeof (MD5_t1344)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.MD5CryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_MD5CryptoServiceProvid.h"
// Metadata Definition System.Security.Cryptography.MD5CryptoServiceProvider
extern TypeInfo MD5CryptoServiceProvider_t1977_il2cpp_TypeInfo;
// System.Security.Cryptography.MD5CryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_MD5CryptoServiceProvidMethodDeclarations.h"
static const EncodedMethodIndex MD5CryptoServiceProvider_t1977_VTable[15] = 
{
	626,
	3961,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	3962,
	3963,
	1698,
	3964,
	3965,
};
static Il2CppInterfaceOffsetPair MD5CryptoServiceProvider_t1977_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MD5CryptoServiceProvider_t1977_0_0_0;
extern const Il2CppType MD5CryptoServiceProvider_t1977_1_0_0;
struct MD5CryptoServiceProvider_t1977;
const Il2CppTypeDefinitionMetadata MD5CryptoServiceProvider_t1977_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MD5CryptoServiceProvider_t1977_InterfacesOffsets/* interfaceOffsets */
	, &MD5_t1344_0_0_0/* parent */
	, MD5CryptoServiceProvider_t1977_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8660/* fieldStart */
	, 12340/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MD5CryptoServiceProvider_t1977_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MD5CryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &MD5CryptoServiceProvider_t1977_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3079/* custom_attributes_cache */
	, &MD5CryptoServiceProvider_t1977_0_0_0/* byval_arg */
	, &MD5CryptoServiceProvider_t1977_1_0_0/* this_arg */
	, &MD5CryptoServiceProvider_t1977_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MD5CryptoServiceProvider_t1977)/* instance_size */
	, sizeof (MD5CryptoServiceProvider_t1977)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MD5CryptoServiceProvider_t1977_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.PaddingMode
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"
// Metadata Definition System.Security.Cryptography.PaddingMode
extern TypeInfo PaddingMode_t1193_il2cpp_TypeInfo;
// System.Security.Cryptography.PaddingMode
#include "mscorlib_System_Security_Cryptography_PaddingModeMethodDeclarations.h"
static const EncodedMethodIndex PaddingMode_t1193_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair PaddingMode_t1193_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PaddingMode_t1193_0_0_0;
extern const Il2CppType PaddingMode_t1193_1_0_0;
const Il2CppTypeDefinitionMetadata PaddingMode_t1193_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PaddingMode_t1193_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, PaddingMode_t1193_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8666/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PaddingMode_t1193_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PaddingMode"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3080/* custom_attributes_cache */
	, &PaddingMode_t1193_0_0_0/* byval_arg */
	, &PaddingMode_t1193_1_0_0/* this_arg */
	, &PaddingMode_t1193_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PaddingMode_t1193)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PaddingMode_t1193)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.RC2
#include "mscorlib_System_Security_Cryptography_RC2.h"
// Metadata Definition System.Security.Cryptography.RC2
extern TypeInfo RC2_t1355_il2cpp_TypeInfo;
// System.Security.Cryptography.RC2
#include "mscorlib_System_Security_Cryptography_RC2MethodDeclarations.h"
static const EncodedMethodIndex RC2_t1355_VTable[27] = 
{
	626,
	1629,
	627,
	628,
	1630,
	1631,
	1632,
	1633,
	1634,
	1635,
	1636,
	1637,
	1638,
	3966,
	3967,
	1641,
	1642,
	1643,
	1644,
	1645,
	1646,
	0,
	1647,
	0,
	0,
	0,
	3968,
};
static Il2CppInterfaceOffsetPair RC2_t1355_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RC2_t1355_0_0_0;
extern const Il2CppType RC2_t1355_1_0_0;
struct RC2_t1355;
const Il2CppTypeDefinitionMetadata RC2_t1355_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RC2_t1355_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricAlgorithm_t1176_0_0_0/* parent */
	, RC2_t1355_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8672/* fieldStart */
	, 12350/* methodStart */
	, -1/* eventStart */
	, 2466/* propertyStart */

};
TypeInfo RC2_t1355_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RC2"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RC2_t1355_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3081/* custom_attributes_cache */
	, &RC2_t1355_0_0_0/* byval_arg */
	, &RC2_t1355_1_0_0/* this_arg */
	, &RC2_t1355_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RC2_t1355)/* instance_size */
	, sizeof (RC2_t1355)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 27/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.RC2CryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_RC2CryptoServiceProvid.h"
// Metadata Definition System.Security.Cryptography.RC2CryptoServiceProvider
extern TypeInfo RC2CryptoServiceProvider_t1978_il2cpp_TypeInfo;
// System.Security.Cryptography.RC2CryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_RC2CryptoServiceProvidMethodDeclarations.h"
static const EncodedMethodIndex RC2CryptoServiceProvider_t1978_VTable[27] = 
{
	626,
	1629,
	627,
	628,
	1630,
	1631,
	1632,
	1633,
	1634,
	1635,
	1636,
	1637,
	1638,
	3966,
	3967,
	1641,
	1642,
	1643,
	1644,
	1645,
	1646,
	3969,
	1647,
	3970,
	3971,
	3972,
	3973,
};
static Il2CppInterfaceOffsetPair RC2CryptoServiceProvider_t1978_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RC2CryptoServiceProvider_t1978_0_0_0;
extern const Il2CppType RC2CryptoServiceProvider_t1978_1_0_0;
struct RC2CryptoServiceProvider_t1978;
const Il2CppTypeDefinitionMetadata RC2CryptoServiceProvider_t1978_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RC2CryptoServiceProvider_t1978_InterfacesOffsets/* interfaceOffsets */
	, &RC2_t1355_0_0_0/* parent */
	, RC2CryptoServiceProvider_t1978_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12356/* methodStart */
	, -1/* eventStart */
	, 2468/* propertyStart */

};
TypeInfo RC2CryptoServiceProvider_t1978_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RC2CryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RC2CryptoServiceProvider_t1978_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3082/* custom_attributes_cache */
	, &RC2CryptoServiceProvider_t1978_0_0_0/* byval_arg */
	, &RC2CryptoServiceProvider_t1978_1_0_0/* this_arg */
	, &RC2CryptoServiceProvider_t1978_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RC2CryptoServiceProvider_t1978)/* instance_size */
	, sizeof (RC2CryptoServiceProvider_t1978)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 27/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.RC2Transform
#include "mscorlib_System_Security_Cryptography_RC2Transform.h"
// Metadata Definition System.Security.Cryptography.RC2Transform
extern TypeInfo RC2Transform_t1979_il2cpp_TypeInfo;
// System.Security.Cryptography.RC2Transform
#include "mscorlib_System_Security_Cryptography_RC2TransformMethodDeclarations.h"
static const EncodedMethodIndex RC2Transform_t1979_VTable[18] = 
{
	626,
	2740,
	627,
	628,
	2741,
	2742,
	2743,
	2744,
	2745,
	2742,
	2746,
	3974,
	2747,
	2748,
	2749,
	2750,
	2743,
	2744,
};
static Il2CppInterfaceOffsetPair RC2Transform_t1979_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RC2Transform_t1979_0_0_0;
extern const Il2CppType RC2Transform_t1979_1_0_0;
struct RC2Transform_t1979;
const Il2CppTypeDefinitionMetadata RC2Transform_t1979_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RC2Transform_t1979_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricTransform_t1622_0_0_0/* parent */
	, RC2Transform_t1979_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8673/* fieldStart */
	, 12362/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RC2Transform_t1979_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RC2Transform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RC2Transform_t1979_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RC2Transform_t1979_0_0_0/* byval_arg */
	, &RC2Transform_t1979_1_0_0/* this_arg */
	, &RC2Transform_t1979_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RC2Transform_t1979)/* instance_size */
	, sizeof (RC2Transform_t1979)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RC2Transform_t1979_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.RIPEMD160
#include "mscorlib_System_Security_Cryptography_RIPEMD160.h"
// Metadata Definition System.Security.Cryptography.RIPEMD160
extern TypeInfo RIPEMD160_t1980_il2cpp_TypeInfo;
// System.Security.Cryptography.RIPEMD160
#include "mscorlib_System_Security_Cryptography_RIPEMD160MethodDeclarations.h"
static const EncodedMethodIndex RIPEMD160_t1980_VTable[15] = 
{
	626,
	601,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	0,
	0,
	1698,
	0,
	1699,
};
static Il2CppInterfaceOffsetPair RIPEMD160_t1980_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RIPEMD160_t1980_0_0_0;
extern const Il2CppType RIPEMD160_t1980_1_0_0;
struct RIPEMD160_t1980;
const Il2CppTypeDefinitionMetadata RIPEMD160_t1980_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RIPEMD160_t1980_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t1217_0_0_0/* parent */
	, RIPEMD160_t1980_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12365/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RIPEMD160_t1980_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RIPEMD160"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RIPEMD160_t1980_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3083/* custom_attributes_cache */
	, &RIPEMD160_t1980_0_0_0/* byval_arg */
	, &RIPEMD160_t1980_1_0_0/* this_arg */
	, &RIPEMD160_t1980_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RIPEMD160_t1980)/* instance_size */
	, sizeof (RIPEMD160_t1980)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.RIPEMD160Managed
#include "mscorlib_System_Security_Cryptography_RIPEMD160Managed.h"
// Metadata Definition System.Security.Cryptography.RIPEMD160Managed
extern TypeInfo RIPEMD160Managed_t1981_il2cpp_TypeInfo;
// System.Security.Cryptography.RIPEMD160Managed
#include "mscorlib_System_Security_Cryptography_RIPEMD160ManagedMethodDeclarations.h"
static const EncodedMethodIndex RIPEMD160Managed_t1981_VTable[15] = 
{
	626,
	3975,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	3976,
	3977,
	1698,
	3978,
	1699,
};
static Il2CppInterfaceOffsetPair RIPEMD160Managed_t1981_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RIPEMD160Managed_t1981_0_0_0;
extern const Il2CppType RIPEMD160Managed_t1981_1_0_0;
struct RIPEMD160Managed_t1981;
const Il2CppTypeDefinitionMetadata RIPEMD160Managed_t1981_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RIPEMD160Managed_t1981_InterfacesOffsets/* interfaceOffsets */
	, &RIPEMD160_t1980_0_0_0/* parent */
	, RIPEMD160Managed_t1981_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8680/* fieldStart */
	, 12366/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RIPEMD160Managed_t1981_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RIPEMD160Managed"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RIPEMD160Managed_t1981_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3084/* custom_attributes_cache */
	, &RIPEMD160Managed_t1981_0_0_0/* byval_arg */
	, &RIPEMD160Managed_t1981_1_0_0/* this_arg */
	, &RIPEMD160Managed_t1981_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RIPEMD160Managed_t1981)/* instance_size */
	, sizeof (RIPEMD160Managed_t1981)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.RNGCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_RNGCryptoServiceProvid.h"
// Metadata Definition System.Security.Cryptography.RNGCryptoServiceProvider
extern TypeInfo RNGCryptoServiceProvider_t1982_il2cpp_TypeInfo;
// System.Security.Cryptography.RNGCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_RNGCryptoServiceProvidMethodDeclarations.h"
static const EncodedMethodIndex RNGCryptoServiceProvider_t1982_VTable[6] = 
{
	626,
	3979,
	627,
	628,
	3980,
	3981,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RNGCryptoServiceProvider_t1982_0_0_0;
extern const Il2CppType RNGCryptoServiceProvider_t1982_1_0_0;
extern const Il2CppType RandomNumberGenerator_t1174_0_0_0;
struct RNGCryptoServiceProvider_t1982;
const Il2CppTypeDefinitionMetadata RNGCryptoServiceProvider_t1982_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &RandomNumberGenerator_t1174_0_0_0/* parent */
	, RNGCryptoServiceProvider_t1982_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8685/* fieldStart */
	, 12390/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RNGCryptoServiceProvider_t1982_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RNGCryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RNGCryptoServiceProvider_t1982_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RNGCryptoServiceProvider_t1982_0_0_0/* byval_arg */
	, &RNGCryptoServiceProvider_t1982_1_0_0/* this_arg */
	, &RNGCryptoServiceProvider_t1982_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RNGCryptoServiceProvider_t1982)/* instance_size */
	, sizeof (RNGCryptoServiceProvider_t1982)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RNGCryptoServiceProvider_t1982_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.RSA
#include "mscorlib_System_Security_Cryptography_RSA.h"
// Metadata Definition System.Security.Cryptography.RSA
extern TypeInfo RSA_t1226_il2cpp_TypeInfo;
// System.Security.Cryptography.RSA
#include "mscorlib_System_Security_Cryptography_RSAMethodDeclarations.h"
static const EncodedMethodIndex RSA_t1226_VTable[14] = 
{
	626,
	601,
	627,
	628,
	1704,
	3927,
	1706,
	0,
	1708,
	3982,
	0,
	0,
	0,
	0,
};
static Il2CppInterfaceOffsetPair RSA_t1226_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RSA_t1226_0_0_0;
extern const Il2CppType RSA_t1226_1_0_0;
struct RSA_t1226;
const Il2CppTypeDefinitionMetadata RSA_t1226_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RSA_t1226_InterfacesOffsets/* interfaceOffsets */
	, &AsymmetricAlgorithm_t1322_0_0_0/* parent */
	, RSA_t1226_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12400/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RSA_t1226_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSA"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSA_t1226_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3085/* custom_attributes_cache */
	, &RSA_t1226_0_0_0/* byval_arg */
	, &RSA_t1226_1_0_0/* this_arg */
	, &RSA_t1226_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSA_t1226)/* instance_size */
	, sizeof (RSA_t1226)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
