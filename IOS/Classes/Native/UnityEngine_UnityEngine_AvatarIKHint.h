﻿#pragma once
#include <stdint.h>
// System.Enum
#include "mscorlib_System_Enum.h"
// UnityEngine.AvatarIKHint
#include "UnityEngine_UnityEngine_AvatarIKHint.h"
// UnityEngine.AvatarIKHint
struct  AvatarIKHint_t947 
{
	// System.Int32 UnityEngine.AvatarIKHint::value__
	int32_t ___value___1;
};
