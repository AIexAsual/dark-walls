﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Colors_Adjust_PreFilters
struct CameraFilterPack_Colors_Adjust_PreFilters_t73;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Colors_Adjust_PreFilters::.ctor()
extern "C" void CameraFilterPack_Colors_Adjust_PreFilters__ctor_m446 (CameraFilterPack_Colors_Adjust_PreFilters_t73 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_Adjust_PreFilters::get_material()
extern "C" Material_t2 * CameraFilterPack_Colors_Adjust_PreFilters_get_material_m447 (CameraFilterPack_Colors_Adjust_PreFilters_t73 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_PreFilters::ChangeFilters()
extern "C" void CameraFilterPack_Colors_Adjust_PreFilters_ChangeFilters_m448 (CameraFilterPack_Colors_Adjust_PreFilters_t73 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_PreFilters::Start()
extern "C" void CameraFilterPack_Colors_Adjust_PreFilters_Start_m449 (CameraFilterPack_Colors_Adjust_PreFilters_t73 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_PreFilters::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Colors_Adjust_PreFilters_OnRenderImage_m450 (CameraFilterPack_Colors_Adjust_PreFilters_t73 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_PreFilters::OnValidate()
extern "C" void CameraFilterPack_Colors_Adjust_PreFilters_OnValidate_m451 (CameraFilterPack_Colors_Adjust_PreFilters_t73 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_PreFilters::Update()
extern "C" void CameraFilterPack_Colors_Adjust_PreFilters_Update_m452 (CameraFilterPack_Colors_Adjust_PreFilters_t73 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_PreFilters::OnDisable()
extern "C" void CameraFilterPack_Colors_Adjust_PreFilters_OnDisable_m453 (CameraFilterPack_Colors_Adjust_PreFilters_t73 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
