﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// UnityEngine.SpriteRenderer
#include "UnityEngine_UnityEngine_SpriteRenderer.h"
// Metadata Definition UnityEngine.SpriteRenderer
extern TypeInfo SpriteRenderer_t786_il2cpp_TypeInfo;
// UnityEngine.SpriteRenderer
#include "UnityEngine_UnityEngine_SpriteRendererMethodDeclarations.h"
static const EncodedMethodIndex SpriteRenderer_t786_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SpriteRenderer_t786_0_0_0;
extern const Il2CppType SpriteRenderer_t786_1_0_0;
extern const Il2CppType Renderer_t312_0_0_0;
struct SpriteRenderer_t786;
const Il2CppTypeDefinitionMetadata SpriteRenderer_t786_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Renderer_t312_0_0_0/* parent */
	, SpriteRenderer_t786_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SpriteRenderer_t786_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpriteRenderer"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SpriteRenderer_t786_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SpriteRenderer_t786_0_0_0/* byval_arg */
	, &SpriteRenderer_t786_1_0_0/* this_arg */
	, &SpriteRenderer_t786_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpriteRenderer_t786)/* instance_size */
	, sizeof (SpriteRenderer_t786)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Sprites.DataUtility
#include "UnityEngine_UnityEngine_Sprites_DataUtility.h"
// Metadata Definition UnityEngine.Sprites.DataUtility
extern TypeInfo DataUtility_t906_il2cpp_TypeInfo;
// UnityEngine.Sprites.DataUtility
#include "UnityEngine_UnityEngine_Sprites_DataUtilityMethodDeclarations.h"
static const EncodedMethodIndex DataUtility_t906_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DataUtility_t906_0_0_0;
extern const Il2CppType DataUtility_t906_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct DataUtility_t906;
const Il2CppTypeDefinitionMetadata DataUtility_t906_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DataUtility_t906_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4894/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DataUtility_t906_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DataUtility"/* name */
	, "UnityEngine.Sprites"/* namespaze */
	, NULL/* methods */
	, &DataUtility_t906_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DataUtility_t906_0_0_0/* byval_arg */
	, &DataUtility_t906_1_0_0/* this_arg */
	, &DataUtility_t906_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DataUtility_t906)/* instance_size */
	, sizeof (DataUtility_t906)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WWW
#include "UnityEngine_UnityEngine_WWW.h"
// Metadata Definition UnityEngine.WWW
extern TypeInfo WWW_t304_il2cpp_TypeInfo;
// UnityEngine.WWW
#include "UnityEngine_UnityEngine_WWWMethodDeclarations.h"
static const EncodedMethodIndex WWW_t304_VTable[5] = 
{
	626,
	1370,
	627,
	628,
	1371,
};
extern const Il2CppType IDisposable_t538_0_0_0;
static const Il2CppType* WWW_t304_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair WWW_t304_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WWW_t304_0_0_0;
extern const Il2CppType WWW_t304_1_0_0;
struct WWW_t304;
const Il2CppTypeDefinitionMetadata WWW_t304_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WWW_t304_InterfacesTypeInfos/* implementedInterfaces */
	, WWW_t304_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WWW_t304_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4899/* methodStart */
	, -1/* eventStart */
	, 868/* propertyStart */

};
TypeInfo WWW_t304_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WWW"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WWW_t304_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WWW_t304_0_0_0/* byval_arg */
	, &WWW_t304_1_0_0/* this_arg */
	, &WWW_t304_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WWW_t304)/* instance_size */
	, sizeof (WWW_t304)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 8/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.WWWForm
#include "UnityEngine_UnityEngine_WWWForm.h"
// Metadata Definition UnityEngine.WWWForm
extern TypeInfo WWWForm_t908_il2cpp_TypeInfo;
// UnityEngine.WWWForm
#include "UnityEngine_UnityEngine_WWWFormMethodDeclarations.h"
static const EncodedMethodIndex WWWForm_t908_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WWWForm_t908_0_0_0;
extern const Il2CppType WWWForm_t908_1_0_0;
struct WWWForm_t908;
const Il2CppTypeDefinitionMetadata WWWForm_t908_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WWWForm_t908_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4318/* fieldStart */
	, 4917/* methodStart */
	, -1/* eventStart */
	, 876/* propertyStart */

};
TypeInfo WWWForm_t908_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WWWForm"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WWWForm_t908_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WWWForm_t908_0_0_0/* byval_arg */
	, &WWWForm_t908_1_0_0/* this_arg */
	, &WWWForm_t908_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WWWForm_t908)/* instance_size */
	, sizeof (WWWForm_t908)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WWWTranscoder
#include "UnityEngine_UnityEngine_WWWTranscoder.h"
// Metadata Definition UnityEngine.WWWTranscoder
extern TypeInfo WWWTranscoder_t909_il2cpp_TypeInfo;
// UnityEngine.WWWTranscoder
#include "UnityEngine_UnityEngine_WWWTranscoderMethodDeclarations.h"
static const EncodedMethodIndex WWWTranscoder_t909_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WWWTranscoder_t909_0_0_0;
extern const Il2CppType WWWTranscoder_t909_1_0_0;
struct WWWTranscoder_t909;
const Il2CppTypeDefinitionMetadata WWWTranscoder_t909_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WWWTranscoder_t909_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4324/* fieldStart */
	, 4923/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WWWTranscoder_t909_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WWWTranscoder"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WWWTranscoder_t909_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WWWTranscoder_t909_0_0_0/* byval_arg */
	, &WWWTranscoder_t909_1_0_0/* this_arg */
	, &WWWTranscoder_t909_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WWWTranscoder_t909)/* instance_size */
	, sizeof (WWWTranscoder_t909)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WWWTranscoder_t909_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CacheIndex
#include "UnityEngine_UnityEngine_CacheIndex.h"
// Metadata Definition UnityEngine.CacheIndex
extern TypeInfo CacheIndex_t910_il2cpp_TypeInfo;
// UnityEngine.CacheIndex
#include "UnityEngine_UnityEngine_CacheIndexMethodDeclarations.h"
static const EncodedMethodIndex CacheIndex_t910_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CacheIndex_t910_0_0_0;
extern const Il2CppType CacheIndex_t910_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata CacheIndex_t910_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, CacheIndex_t910_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4332/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CacheIndex_t910_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CacheIndex"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &CacheIndex_t910_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1694/* custom_attributes_cache */
	, &CacheIndex_t910_0_0_0/* byval_arg */
	, &CacheIndex_t910_1_0_0/* this_arg */
	, &CacheIndex_t910_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)CacheIndex_t910_marshal/* marshal_to_native_func */
	, (methodPointerType)CacheIndex_t910_marshal_back/* marshal_from_native_func */
	, (methodPointerType)CacheIndex_t910_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (CacheIndex_t910)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CacheIndex_t910)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(CacheIndex_t910_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UnityString
#include "UnityEngine_UnityEngine_UnityString.h"
// Metadata Definition UnityEngine.UnityString
extern TypeInfo UnityString_t911_il2cpp_TypeInfo;
// UnityEngine.UnityString
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"
static const EncodedMethodIndex UnityString_t911_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityString_t911_0_0_0;
extern const Il2CppType UnityString_t911_1_0_0;
struct UnityString_t911;
const Il2CppTypeDefinitionMetadata UnityString_t911_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnityString_t911_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4931/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityString_t911_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityString"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &UnityString_t911_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityString_t911_0_0_0/* byval_arg */
	, &UnityString_t911_1_0_0/* this_arg */
	, &UnityString_t911_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityString_t911)/* instance_size */
	, sizeof (UnityString_t911)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AsyncOperation
#include "UnityEngine_UnityEngine_AsyncOperation.h"
// Metadata Definition UnityEngine.AsyncOperation
extern TypeInfo AsyncOperation_t829_il2cpp_TypeInfo;
// UnityEngine.AsyncOperation
#include "UnityEngine_UnityEngine_AsyncOperationMethodDeclarations.h"
static const EncodedMethodIndex AsyncOperation_t829_VTable[4] = 
{
	626,
	1262,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AsyncOperation_t829_0_0_0;
extern const Il2CppType AsyncOperation_t829_1_0_0;
extern const Il2CppType YieldInstruction_t836_0_0_0;
struct AsyncOperation_t829;
const Il2CppTypeDefinitionMetadata AsyncOperation_t829_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &YieldInstruction_t836_0_0_0/* parent */
	, AsyncOperation_t829_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4335/* fieldStart */
	, 4932/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AsyncOperation_t829_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsyncOperation"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AsyncOperation_t829_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AsyncOperation_t829_0_0_0/* byval_arg */
	, &AsyncOperation_t829_1_0_0/* this_arg */
	, &AsyncOperation_t829_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)AsyncOperation_t829_marshal/* marshal_to_native_func */
	, (methodPointerType)AsyncOperation_t829_marshal_back/* marshal_from_native_func */
	, (methodPointerType)AsyncOperation_t829_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (AsyncOperation_t829)/* instance_size */
	, sizeof (AsyncOperation_t829)/* actualSize */
	, 0/* element_size */
	, sizeof(AsyncOperation_t829_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Application
#include "UnityEngine_UnityEngine_Application.h"
// Metadata Definition UnityEngine.Application
extern TypeInfo Application_t913_il2cpp_TypeInfo;
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
extern const Il2CppType LogCallback_t912_0_0_0;
static const Il2CppType* Application_t913_il2cpp_TypeInfo__nestedTypes[1] =
{
	&LogCallback_t912_0_0_0,
};
static const EncodedMethodIndex Application_t913_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Application_t913_0_0_0;
extern const Il2CppType Application_t913_1_0_0;
struct Application_t913;
const Il2CppTypeDefinitionMetadata Application_t913_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Application_t913_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Application_t913_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4336/* fieldStart */
	, 4935/* methodStart */
	, -1/* eventStart */
	, 878/* propertyStart */

};
TypeInfo Application_t913_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Application"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Application_t913_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Application_t913_0_0_0/* byval_arg */
	, &Application_t913_1_0_0/* this_arg */
	, &Application_t913_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Application_t913)/* instance_size */
	, sizeof (Application_t913)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Application_t913_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 7/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Application/LogCallback
#include "UnityEngine_UnityEngine_Application_LogCallback.h"
// Metadata Definition UnityEngine.Application/LogCallback
extern TypeInfo LogCallback_t912_il2cpp_TypeInfo;
// UnityEngine.Application/LogCallback
#include "UnityEngine_UnityEngine_Application_LogCallbackMethodDeclarations.h"
static const EncodedMethodIndex LogCallback_t912_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1372,
	1373,
	1374,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
extern const Il2CppType ISerializable_t2210_0_0_0;
static Il2CppInterfaceOffsetPair LogCallback_t912_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType LogCallback_t912_1_0_0;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
struct LogCallback_t912;
const Il2CppTypeDefinitionMetadata LogCallback_t912_DefinitionMetadata = 
{
	&Application_t913_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LogCallback_t912_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, LogCallback_t912_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4948/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LogCallback_t912_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LogCallback"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &LogCallback_t912_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LogCallback_t912_0_0_0/* byval_arg */
	, &LogCallback_t912_1_0_0/* this_arg */
	, &LogCallback_t912_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_LogCallback_t912/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LogCallback_t912)/* instance_size */
	, sizeof (LogCallback_t912)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"
// Metadata Definition UnityEngine.Behaviour
extern TypeInfo Behaviour_t825_il2cpp_TypeInfo;
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
static const EncodedMethodIndex Behaviour_t825_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Behaviour_t825_0_0_0;
extern const Il2CppType Behaviour_t825_1_0_0;
extern const Il2CppType Component_t477_0_0_0;
struct Behaviour_t825;
const Il2CppTypeDefinitionMetadata Behaviour_t825_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Component_t477_0_0_0/* parent */
	, Behaviour_t825_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4952/* methodStart */
	, -1/* eventStart */
	, 885/* propertyStart */

};
TypeInfo Behaviour_t825_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Behaviour"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Behaviour_t825_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Behaviour_t825_0_0_0/* byval_arg */
	, &Behaviour_t825_1_0_0/* this_arg */
	, &Behaviour_t825_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Behaviour_t825)/* instance_size */
	, sizeof (Behaviour_t825)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// Metadata Definition UnityEngine.Camera
extern TypeInfo Camera_t14_il2cpp_TypeInfo;
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
extern const Il2CppType CameraCallback_t914_0_0_0;
static const Il2CppType* Camera_t14_il2cpp_TypeInfo__nestedTypes[1] =
{
	&CameraCallback_t914_0_0_0,
};
static const EncodedMethodIndex Camera_t14_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Camera_t14_0_0_0;
extern const Il2CppType Camera_t14_1_0_0;
struct Camera_t14;
const Il2CppTypeDefinitionMetadata Camera_t14_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Camera_t14_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t825_0_0_0/* parent */
	, Camera_t14_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4338/* fieldStart */
	, 4956/* methodStart */
	, -1/* eventStart */
	, 887/* propertyStart */

};
TypeInfo Camera_t14_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Camera"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Camera_t14_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Camera_t14_0_0_0/* byval_arg */
	, &Camera_t14_1_0_0/* this_arg */
	, &Camera_t14_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Camera_t14)/* instance_size */
	, sizeof (Camera_t14)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Camera_t14_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 45/* method_count */
	, 19/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Camera/CameraCallback
#include "UnityEngine_UnityEngine_Camera_CameraCallback.h"
// Metadata Definition UnityEngine.Camera/CameraCallback
extern TypeInfo CameraCallback_t914_il2cpp_TypeInfo;
// UnityEngine.Camera/CameraCallback
#include "UnityEngine_UnityEngine_Camera_CameraCallbackMethodDeclarations.h"
static const EncodedMethodIndex CameraCallback_t914_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1375,
	1376,
	1377,
};
static Il2CppInterfaceOffsetPair CameraCallback_t914_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CameraCallback_t914_1_0_0;
struct CameraCallback_t914;
const Il2CppTypeDefinitionMetadata CameraCallback_t914_DefinitionMetadata = 
{
	&Camera_t14_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CameraCallback_t914_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, CameraCallback_t914_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5001/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CameraCallback_t914_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraCallback"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraCallback_t914_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1740/* custom_attributes_cache */
	, &CameraCallback_t914_0_0_0/* byval_arg */
	, &CameraCallback_t914_1_0_0/* this_arg */
	, &CameraCallback_t914_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CameraCallback_t914/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraCallback_t914)/* instance_size */
	, sizeof (CameraCallback_t914)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_Debug.h"
// Metadata Definition UnityEngine.Debug
extern TypeInfo Debug_t915_il2cpp_TypeInfo;
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
static const EncodedMethodIndex Debug_t915_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Debug_t915_0_0_0;
extern const Il2CppType Debug_t915_1_0_0;
struct Debug_t915;
const Il2CppTypeDefinitionMetadata Debug_t915_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Debug_t915_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5005/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Debug_t915_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Debug"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Debug_t915_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Debug_t915_0_0_0/* byval_arg */
	, &Debug_t915_1_0_0/* this_arg */
	, &Debug_t915_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Debug_t915)/* instance_size */
	, sizeof (Debug_t915)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Display
#include "UnityEngine_UnityEngine_Display.h"
// Metadata Definition UnityEngine.Display
extern TypeInfo Display_t918_il2cpp_TypeInfo;
// UnityEngine.Display
#include "UnityEngine_UnityEngine_DisplayMethodDeclarations.h"
extern const Il2CppType DisplaysUpdatedDelegate_t916_0_0_0;
static const Il2CppType* Display_t918_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DisplaysUpdatedDelegate_t916_0_0_0,
};
static const EncodedMethodIndex Display_t918_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Display_t918_0_0_0;
extern const Il2CppType Display_t918_1_0_0;
struct Display_t918;
const Il2CppTypeDefinitionMetadata Display_t918_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Display_t918_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Display_t918_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4341/* fieldStart */
	, 5014/* methodStart */
	, 14/* eventStart */
	, 906/* propertyStart */

};
TypeInfo Display_t918_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Display"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Display_t918_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Display_t918_0_0_0/* byval_arg */
	, &Display_t918_1_0_0/* this_arg */
	, &Display_t918_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Display_t918)/* instance_size */
	, sizeof (Display_t918)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Display_t918_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 7/* property_count */
	, 4/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Display/DisplaysUpdatedDelegate
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDelegate.h"
// Metadata Definition UnityEngine.Display/DisplaysUpdatedDelegate
extern TypeInfo DisplaysUpdatedDelegate_t916_il2cpp_TypeInfo;
// UnityEngine.Display/DisplaysUpdatedDelegate
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDelegateMethodDeclarations.h"
static const EncodedMethodIndex DisplaysUpdatedDelegate_t916_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1378,
	1379,
	1380,
};
static Il2CppInterfaceOffsetPair DisplaysUpdatedDelegate_t916_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DisplaysUpdatedDelegate_t916_1_0_0;
struct DisplaysUpdatedDelegate_t916;
const Il2CppTypeDefinitionMetadata DisplaysUpdatedDelegate_t916_DefinitionMetadata = 
{
	&Display_t918_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DisplaysUpdatedDelegate_t916_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, DisplaysUpdatedDelegate_t916_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5042/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DisplaysUpdatedDelegate_t916_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DisplaysUpdatedDelegate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DisplaysUpdatedDelegate_t916_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DisplaysUpdatedDelegate_t916_0_0_0/* byval_arg */
	, &DisplaysUpdatedDelegate_t916_1_0_0/* this_arg */
	, &DisplaysUpdatedDelegate_t916_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t916/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DisplaysUpdatedDelegate_t916)/* instance_size */
	, sizeof (DisplaysUpdatedDelegate_t916)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Metadata Definition UnityEngine.MonoBehaviour
extern TypeInfo MonoBehaviour_t4_il2cpp_TypeInfo;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
static const EncodedMethodIndex MonoBehaviour_t4_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType MonoBehaviour_t4_0_0_0;
extern const Il2CppType MonoBehaviour_t4_1_0_0;
struct MonoBehaviour_t4;
const Il2CppTypeDefinitionMetadata MonoBehaviour_t4_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t825_0_0_0/* parent */
	, MonoBehaviour_t4_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5046/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoBehaviour_t4_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoBehaviour"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &MonoBehaviour_t4_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoBehaviour_t4_0_0_0/* byval_arg */
	, &MonoBehaviour_t4_1_0_0/* this_arg */
	, &MonoBehaviour_t4_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoBehaviour_t4)/* instance_size */
	, sizeof (MonoBehaviour_t4)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"
// Metadata Definition UnityEngine.TouchPhase
extern TypeInfo TouchPhase_t919_il2cpp_TypeInfo;
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhaseMethodDeclarations.h"
static const EncodedMethodIndex TouchPhase_t919_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair TouchPhase_t919_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TouchPhase_t919_0_0_0;
extern const Il2CppType TouchPhase_t919_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata TouchPhase_t919_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TouchPhase_t919_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, TouchPhase_t919_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4345/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TouchPhase_t919_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TouchPhase"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TouchPhase_t919_0_0_0/* byval_arg */
	, &TouchPhase_t919_1_0_0/* this_arg */
	, &TouchPhase_t919_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TouchPhase_t919)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TouchPhase_t919)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.IMECompositionMode
#include "UnityEngine_UnityEngine_IMECompositionMode.h"
// Metadata Definition UnityEngine.IMECompositionMode
extern TypeInfo IMECompositionMode_t920_il2cpp_TypeInfo;
// UnityEngine.IMECompositionMode
#include "UnityEngine_UnityEngine_IMECompositionModeMethodDeclarations.h"
static const EncodedMethodIndex IMECompositionMode_t920_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair IMECompositionMode_t920_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IMECompositionMode_t920_0_0_0;
extern const Il2CppType IMECompositionMode_t920_1_0_0;
const Il2CppTypeDefinitionMetadata IMECompositionMode_t920_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IMECompositionMode_t920_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, IMECompositionMode_t920_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4351/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IMECompositionMode_t920_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMECompositionMode"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IMECompositionMode_t920_0_0_0/* byval_arg */
	, &IMECompositionMode_t920_1_0_0/* this_arg */
	, &IMECompositionMode_t920_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IMECompositionMode_t920)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (IMECompositionMode_t920)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
// Metadata Definition UnityEngine.Touch
extern TypeInfo Touch_t768_il2cpp_TypeInfo;
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"
static const EncodedMethodIndex Touch_t768_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Touch_t768_0_0_0;
extern const Il2CppType Touch_t768_1_0_0;
const Il2CppTypeDefinitionMetadata Touch_t768_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Touch_t768_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4355/* fieldStart */
	, 5058/* methodStart */
	, -1/* eventStart */
	, 913/* propertyStart */

};
TypeInfo Touch_t768_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Touch"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Touch_t768_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Touch_t768_0_0_0/* byval_arg */
	, &Touch_t768_1_0_0/* this_arg */
	, &Touch_t768_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Touch_t768_marshal/* marshal_to_native_func */
	, (methodPointerType)Touch_t768_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Touch_t768_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Touch_t768)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Touch_t768)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Touch_t768_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Input
#include "UnityEngine_UnityEngine_Input.h"
// Metadata Definition UnityEngine.Input
extern TypeInfo Input_t486_il2cpp_TypeInfo;
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
static const EncodedMethodIndex Input_t486_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Input_t486_0_0_0;
extern const Il2CppType Input_t486_1_0_0;
struct Input_t486;
const Il2CppTypeDefinitionMetadata Input_t486_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Input_t486_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5061/* methodStart */
	, -1/* eventStart */
	, 916/* propertyStart */

};
TypeInfo Input_t486_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Input"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Input_t486_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Input_t486_0_0_0/* byval_arg */
	, &Input_t486_1_0_0/* this_arg */
	, &Input_t486_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Input_t486)/* instance_size */
	, sizeof (Input_t486)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 25/* method_count */
	, 9/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"
// Metadata Definition UnityEngine.HideFlags
extern TypeInfo HideFlags_t921_il2cpp_TypeInfo;
// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlagsMethodDeclarations.h"
static const EncodedMethodIndex HideFlags_t921_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair HideFlags_t921_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HideFlags_t921_0_0_0;
extern const Il2CppType HideFlags_t921_1_0_0;
const Il2CppTypeDefinitionMetadata HideFlags_t921_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HideFlags_t921_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, HideFlags_t921_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4362/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HideFlags_t921_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HideFlags"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1778/* custom_attributes_cache */
	, &HideFlags_t921_0_0_0/* byval_arg */
	, &HideFlags_t921_1_0_0/* this_arg */
	, &HideFlags_t921_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HideFlags_t921)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HideFlags_t921)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// Metadata Definition UnityEngine.Object
extern TypeInfo Object_t473_il2cpp_TypeInfo;
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
extern const Il2CppType Object_Instantiate_m24179_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Object_Instantiate_m24179_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4058 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType Object_FindObjectOfType_m24180_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Object_FindObjectOfType_m24180_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 4059 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4059 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
static const EncodedMethodIndex Object_t473_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Object_t473_0_0_0;
extern const Il2CppType Object_t473_1_0_0;
struct Object_t473;
const Il2CppTypeDefinitionMetadata Object_t473_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Object_t473_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4372/* fieldStart */
	, 5086/* methodStart */
	, -1/* eventStart */
	, 925/* propertyStart */

};
TypeInfo Object_t473_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Object"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Object_t473_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Object_t473_0_0_0/* byval_arg */
	, &Object_t473_1_0_0/* this_arg */
	, &Object_t473_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Object_t473_marshal/* marshal_to_native_func */
	, (methodPointerType)Object_t473_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Object_t473_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Object_t473)/* instance_size */
	, sizeof (Object_t473)/* actualSize */
	, 0/* element_size */
	, sizeof(Object_t473_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 25/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
// Metadata Definition UnityEngine.Component
extern TypeInfo Component_t477_il2cpp_TypeInfo;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
extern const Il2CppType Component_GetComponent_m24181_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Component_GetComponent_m24181_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 4060 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType Component_GetComponentInChildren_m24182_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Component_GetComponentInChildren_m24182_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 4061 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4061 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Component_GetComponentsInChildren_m24183_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5447 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Component_GetComponentsInChildren_m24184_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5448 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Component_GetComponentsInChildren_m24185_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5449 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Component_GetComponentsInChildren_m24186_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5450 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType Component_GetComponentInParent_m24187_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Component_GetComponentInParent_m24187_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 4070 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4070 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType Component_GetComponents_m24188_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Component_GetComponents_m24188_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 4072 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
static const EncodedMethodIndex Component_t477_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Component_t477_1_0_0;
struct Component_t477;
const Il2CppTypeDefinitionMetadata Component_t477_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t473_0_0_0/* parent */
	, Component_t477_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5111/* methodStart */
	, -1/* eventStart */
	, 927/* propertyStart */

};
TypeInfo Component_t477_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Component"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Component_t477_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Component_t477_0_0_0/* byval_arg */
	, &Component_t477_1_0_0/* this_arg */
	, &Component_t477_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Component_t477)/* instance_size */
	, sizeof (Component_t477)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 20/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Light
#include "UnityEngine_UnityEngine_Light.h"
// Metadata Definition UnityEngine.Light
extern TypeInfo Light_t318_il2cpp_TypeInfo;
// UnityEngine.Light
#include "UnityEngine_UnityEngine_LightMethodDeclarations.h"
static const EncodedMethodIndex Light_t318_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Light_t318_0_0_0;
extern const Il2CppType Light_t318_1_0_0;
struct Light_t318;
const Il2CppTypeDefinitionMetadata Light_t318_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t825_0_0_0/* parent */
	, Light_t318_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5131/* methodStart */
	, -1/* eventStart */
	, 929/* propertyStart */

};
TypeInfo Light_t318_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Light"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Light_t318_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Light_t318_0_0_0/* byval_arg */
	, &Light_t318_1_0_0/* this_arg */
	, &Light_t318_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Light_t318)/* instance_size */
	, sizeof (Light_t318)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// Metadata Definition UnityEngine.GameObject
extern TypeInfo GameObject_t256_il2cpp_TypeInfo;
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
extern const Il2CppType GameObject_GetComponent_m24189_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition GameObject_GetComponent_m24189_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 4073 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType GameObject_GetComponents_m24190_gp_0_0_0_0;
extern const Il2CppType TU5BU5D_t3643_0_0_0;
extern const Il2CppRGCTXDefinition GameObject_GetComponents_m24190_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 4075 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4074 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType GameObject_GetComponents_m24191_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition GameObject_GetComponents_m24191_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 4077 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType GameObject_GetComponentsInChildren_m24192_gp_0_0_0_0;
extern const Il2CppType TU5BU5D_t3646_0_0_0;
extern const Il2CppRGCTXDefinition GameObject_GetComponentsInChildren_m24192_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 4079 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4078 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType GameObject_GetComponentsInChildren_m24193_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition GameObject_GetComponentsInChildren_m24193_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 4081 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType GameObject_GetComponentsInParent_m24194_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition GameObject_GetComponentsInParent_m24194_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 4083 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType GameObject_AddComponent_m24195_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition GameObject_AddComponent_m24195_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 4086 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4086 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
static const EncodedMethodIndex GameObject_t256_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GameObject_t256_0_0_0;
extern const Il2CppType GameObject_t256_1_0_0;
struct GameObject_t256;
const Il2CppTypeDefinitionMetadata GameObject_t256_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t473_0_0_0/* parent */
	, GameObject_t256_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5140/* methodStart */
	, -1/* eventStart */
	, 934/* propertyStart */

};
TypeInfo GameObject_t256_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GameObject"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GameObject_t256_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GameObject_t256_0_0_0/* byval_arg */
	, &GameObject_t256_1_0_0/* this_arg */
	, &GameObject_t256_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GameObject_t256)/* instance_size */
	, sizeof (GameObject_t256)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// Metadata Definition UnityEngine.Transform
extern TypeInfo Transform_t243_il2cpp_TypeInfo;
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
extern const Il2CppType Enumerator_t922_0_0_0;
static const Il2CppType* Transform_t243_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Enumerator_t922_0_0_0,
};
static const EncodedMethodIndex Transform_t243_VTable[5] = 
{
	600,
	601,
	602,
	603,
	1362,
};
extern const Il2CppType IEnumerable_t1097_0_0_0;
static const Il2CppType* Transform_t243_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
};
static Il2CppInterfaceOffsetPair Transform_t243_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Transform_t243_0_0_0;
extern const Il2CppType Transform_t243_1_0_0;
struct Transform_t243;
const Il2CppTypeDefinitionMetadata Transform_t243_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Transform_t243_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Transform_t243_InterfacesTypeInfos/* implementedInterfaces */
	, Transform_t243_InterfacesOffsets/* interfaceOffsets */
	, &Component_t477_0_0_0/* parent */
	, Transform_t243_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5170/* methodStart */
	, -1/* eventStart */
	, 938/* propertyStart */

};
TypeInfo Transform_t243_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Transform"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Transform_t243_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Transform_t243_0_0_0/* byval_arg */
	, &Transform_t243_1_0_0/* this_arg */
	, &Transform_t243_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Transform_t243)/* instance_size */
	, sizeof (Transform_t243)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 63/* method_count */
	, 14/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Transform/Enumerator
#include "UnityEngine_UnityEngine_Transform_Enumerator.h"
// Metadata Definition UnityEngine.Transform/Enumerator
extern TypeInfo Enumerator_t922_il2cpp_TypeInfo;
// UnityEngine.Transform/Enumerator
#include "UnityEngine_UnityEngine_Transform_EnumeratorMethodDeclarations.h"
static const EncodedMethodIndex Enumerator_t922_VTable[6] = 
{
	626,
	601,
	627,
	628,
	1381,
	1382,
};
extern const Il2CppType IEnumerator_t464_0_0_0;
static const Il2CppType* Enumerator_t922_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t922_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Enumerator_t922_1_0_0;
struct Enumerator_t922;
const Il2CppTypeDefinitionMetadata Enumerator_t922_DefinitionMetadata = 
{
	&Transform_t243_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t922_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t922_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerator_t922_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4374/* fieldStart */
	, 5233/* methodStart */
	, -1/* eventStart */
	, 952/* propertyStart */

};
TypeInfo Enumerator_t922_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t922_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t922_0_0_0/* byval_arg */
	, &Enumerator_t922_1_0_0/* this_arg */
	, &Enumerator_t922_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t922)/* instance_size */
	, sizeof (Enumerator_t922)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Time
#include "UnityEngine_UnityEngine_Time.h"
// Metadata Definition UnityEngine.Time
extern TypeInfo Time_t923_il2cpp_TypeInfo;
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
static const EncodedMethodIndex Time_t923_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Time_t923_0_0_0;
extern const Il2CppType Time_t923_1_0_0;
struct Time_t923;
const Il2CppTypeDefinitionMetadata Time_t923_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Time_t923_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5236/* methodStart */
	, -1/* eventStart */
	, 953/* propertyStart */

};
TypeInfo Time_t923_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Time"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Time_t923_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Time_t923_0_0_0/* byval_arg */
	, &Time_t923_1_0_0/* this_arg */
	, &Time_t923_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Time_t923)/* instance_size */
	, sizeof (Time_t923)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 6/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Random
#include "UnityEngine_UnityEngine_Random.h"
// Metadata Definition UnityEngine.Random
extern TypeInfo Random_t924_il2cpp_TypeInfo;
// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
static const EncodedMethodIndex Random_t924_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Random_t924_0_0_0;
extern const Il2CppType Random_t924_1_0_0;
struct Random_t924;
const Il2CppTypeDefinitionMetadata Random_t924_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Random_t924_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5242/* methodStart */
	, -1/* eventStart */
	, 959/* propertyStart */

};
TypeInfo Random_t924_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Random"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Random_t924_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Random_t924_0_0_0/* byval_arg */
	, &Random_t924_1_0_0/* this_arg */
	, &Random_t924_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Random_t924)/* instance_size */
	, sizeof (Random_t924)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstruction.h"
// Metadata Definition UnityEngine.YieldInstruction
extern TypeInfo YieldInstruction_t836_il2cpp_TypeInfo;
// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstructionMethodDeclarations.h"
static const EncodedMethodIndex YieldInstruction_t836_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType YieldInstruction_t836_1_0_0;
struct YieldInstruction_t836;
const Il2CppTypeDefinitionMetadata YieldInstruction_t836_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, YieldInstruction_t836_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5248/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo YieldInstruction_t836_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "YieldInstruction"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &YieldInstruction_t836_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &YieldInstruction_t836_0_0_0/* byval_arg */
	, &YieldInstruction_t836_1_0_0/* this_arg */
	, &YieldInstruction_t836_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)YieldInstruction_t836_marshal/* marshal_to_native_func */
	, (methodPointerType)YieldInstruction_t836_marshal_back/* marshal_from_native_func */
	, (methodPointerType)YieldInstruction_t836_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (YieldInstruction_t836)/* instance_size */
	, sizeof (YieldInstruction_t836)/* actualSize */
	, 0/* element_size */
	, sizeof(YieldInstruction_t836_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.PlayerPrefsException
#include "UnityEngine_UnityEngine_PlayerPrefsException.h"
// Metadata Definition UnityEngine.PlayerPrefsException
extern TypeInfo PlayerPrefsException_t925_il2cpp_TypeInfo;
// UnityEngine.PlayerPrefsException
#include "UnityEngine_UnityEngine_PlayerPrefsExceptionMethodDeclarations.h"
static const EncodedMethodIndex PlayerPrefsException_t925_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
extern const Il2CppType _Exception_t3443_0_0_0;
static Il2CppInterfaceOffsetPair PlayerPrefsException_t925_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PlayerPrefsException_t925_0_0_0;
extern const Il2CppType PlayerPrefsException_t925_1_0_0;
extern const Il2CppType Exception_t520_0_0_0;
struct PlayerPrefsException_t925;
const Il2CppTypeDefinitionMetadata PlayerPrefsException_t925_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PlayerPrefsException_t925_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t520_0_0_0/* parent */
	, PlayerPrefsException_t925_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5249/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PlayerPrefsException_t925_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PlayerPrefsException"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &PlayerPrefsException_t925_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PlayerPrefsException_t925_0_0_0/* byval_arg */
	, &PlayerPrefsException_t925_1_0_0/* this_arg */
	, &PlayerPrefsException_t925_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PlayerPrefsException_t925)/* instance_size */
	, sizeof (PlayerPrefsException_t925)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.PlayerPrefs
#include "UnityEngine_UnityEngine_PlayerPrefs.h"
// Metadata Definition UnityEngine.PlayerPrefs
extern TypeInfo PlayerPrefs_t926_il2cpp_TypeInfo;
// UnityEngine.PlayerPrefs
#include "UnityEngine_UnityEngine_PlayerPrefsMethodDeclarations.h"
static const EncodedMethodIndex PlayerPrefs_t926_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PlayerPrefs_t926_0_0_0;
extern const Il2CppType PlayerPrefs_t926_1_0_0;
struct PlayerPrefs_t926;
const Il2CppTypeDefinitionMetadata PlayerPrefs_t926_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PlayerPrefs_t926_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5250/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PlayerPrefs_t926_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PlayerPrefs"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &PlayerPrefs_t926_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PlayerPrefs_t926_0_0_0/* byval_arg */
	, &PlayerPrefs_t926_1_0_0/* this_arg */
	, &PlayerPrefs_t926_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PlayerPrefs_t926)/* instance_size */
	, sizeof (PlayerPrefs_t926)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.ParticleSystem
#include "UnityEngine_UnityEngine_ParticleSystem.h"
// Metadata Definition UnityEngine.ParticleSystem
extern TypeInfo ParticleSystem_t534_il2cpp_TypeInfo;
// UnityEngine.ParticleSystem
#include "UnityEngine_UnityEngine_ParticleSystemMethodDeclarations.h"
static const EncodedMethodIndex ParticleSystem_t534_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ParticleSystem_t534_0_0_0;
extern const Il2CppType ParticleSystem_t534_1_0_0;
struct ParticleSystem_t534;
const Il2CppTypeDefinitionMetadata ParticleSystem_t534_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Component_t477_0_0_0/* parent */
	, ParticleSystem_t534_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5257/* methodStart */
	, -1/* eventStart */
	, 961/* propertyStart */

};
TypeInfo ParticleSystem_t534_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParticleSystem"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &ParticleSystem_t534_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ParticleSystem_t534_0_0_0/* byval_arg */
	, &ParticleSystem_t534_1_0_0/* this_arg */
	, &ParticleSystem_t534_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ParticleSystem_t534)/* instance_size */
	, sizeof (ParticleSystem_t534)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Particle
#include "UnityEngine_UnityEngine_Particle.h"
// Metadata Definition UnityEngine.Particle
extern TypeInfo Particle_t927_il2cpp_TypeInfo;
// UnityEngine.Particle
#include "UnityEngine_UnityEngine_ParticleMethodDeclarations.h"
static const EncodedMethodIndex Particle_t927_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Particle_t927_0_0_0;
extern const Il2CppType Particle_t927_1_0_0;
const Il2CppTypeDefinitionMetadata Particle_t927_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Particle_t927_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4376/* fieldStart */
	, 5258/* methodStart */
	, -1/* eventStart */
	, 962/* propertyStart */

};
TypeInfo Particle_t927_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Particle"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Particle_t927_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Particle_t927_0_0_0/* byval_arg */
	, &Particle_t927_1_0_0/* this_arg */
	, &Particle_t927_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Particle_t927)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Particle_t927)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Particle_t927 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 16/* method_count */
	, 8/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.ForceMode
#include "UnityEngine_UnityEngine_ForceMode.h"
// Metadata Definition UnityEngine.ForceMode
extern TypeInfo ForceMode_t928_il2cpp_TypeInfo;
// UnityEngine.ForceMode
#include "UnityEngine_UnityEngine_ForceModeMethodDeclarations.h"
static const EncodedMethodIndex ForceMode_t928_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ForceMode_t928_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ForceMode_t928_0_0_0;
extern const Il2CppType ForceMode_t928_1_0_0;
const Il2CppTypeDefinitionMetadata ForceMode_t928_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ForceMode_t928_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ForceMode_t928_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4384/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ForceMode_t928_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ForceMode"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ForceMode_t928_0_0_0/* byval_arg */
	, &ForceMode_t928_1_0_0/* this_arg */
	, &ForceMode_t928_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ForceMode_t928)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ForceMode_t928)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Physics
#include "UnityEngine_UnityEngine_Physics.h"
// Metadata Definition UnityEngine.Physics
extern TypeInfo Physics_t929_il2cpp_TypeInfo;
// UnityEngine.Physics
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
static const EncodedMethodIndex Physics_t929_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Physics_t929_0_0_0;
extern const Il2CppType Physics_t929_1_0_0;
struct Physics_t929;
const Il2CppTypeDefinitionMetadata Physics_t929_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Physics_t929_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5274/* methodStart */
	, -1/* eventStart */
	, 970/* propertyStart */

};
TypeInfo Physics_t929_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Physics"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Physics_t929_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Physics_t929_0_0_0/* byval_arg */
	, &Physics_t929_1_0_0/* this_arg */
	, &Physics_t929_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Physics_t929)/* instance_size */
	, sizeof (Physics_t929)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_Rigidbody.h"
// Metadata Definition UnityEngine.Rigidbody
extern TypeInfo Rigidbody_t325_il2cpp_TypeInfo;
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_RigidbodyMethodDeclarations.h"
static const EncodedMethodIndex Rigidbody_t325_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Rigidbody_t325_0_0_0;
extern const Il2CppType Rigidbody_t325_1_0_0;
struct Rigidbody_t325;
const Il2CppTypeDefinitionMetadata Rigidbody_t325_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Component_t477_0_0_0/* parent */
	, Rigidbody_t325_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5288/* methodStart */
	, -1/* eventStart */
	, 971/* propertyStart */

};
TypeInfo Rigidbody_t325_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Rigidbody"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Rigidbody_t325_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Rigidbody_t325_0_0_0/* byval_arg */
	, &Rigidbody_t325_1_0_0/* this_arg */
	, &Rigidbody_t325_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Rigidbody_t325)/* instance_size */
	, sizeof (Rigidbody_t325)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
// Metadata Definition UnityEngine.Collider
extern TypeInfo Collider_t319_il2cpp_TypeInfo;
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
static const EncodedMethodIndex Collider_t319_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Collider_t319_0_0_0;
extern const Il2CppType Collider_t319_1_0_0;
struct Collider_t319;
const Il2CppTypeDefinitionMetadata Collider_t319_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Component_t477_0_0_0/* parent */
	, Collider_t319_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5309/* methodStart */
	, -1/* eventStart */
	, 976/* propertyStart */

};
TypeInfo Collider_t319_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Collider"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Collider_t319_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Collider_t319_0_0_0/* byval_arg */
	, &Collider_t319_1_0_0/* this_arg */
	, &Collider_t319_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Collider_t319)/* instance_size */
	, sizeof (Collider_t319)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.BoxCollider
#include "UnityEngine_UnityEngine_BoxCollider.h"
// Metadata Definition UnityEngine.BoxCollider
extern TypeInfo BoxCollider_t535_il2cpp_TypeInfo;
// UnityEngine.BoxCollider
#include "UnityEngine_UnityEngine_BoxColliderMethodDeclarations.h"
static const EncodedMethodIndex BoxCollider_t535_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType BoxCollider_t535_0_0_0;
extern const Il2CppType BoxCollider_t535_1_0_0;
struct BoxCollider_t535;
const Il2CppTypeDefinitionMetadata BoxCollider_t535_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Collider_t319_0_0_0/* parent */
	, BoxCollider_t535_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BoxCollider_t535_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "BoxCollider"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &BoxCollider_t535_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BoxCollider_t535_0_0_0/* byval_arg */
	, &BoxCollider_t535_1_0_0/* this_arg */
	, &BoxCollider_t535_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BoxCollider_t535)/* instance_size */
	, sizeof (BoxCollider_t535)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CapsuleCollider
#include "UnityEngine_UnityEngine_CapsuleCollider.h"
// Metadata Definition UnityEngine.CapsuleCollider
extern TypeInfo CapsuleCollider_t415_il2cpp_TypeInfo;
// UnityEngine.CapsuleCollider
#include "UnityEngine_UnityEngine_CapsuleColliderMethodDeclarations.h"
static const EncodedMethodIndex CapsuleCollider_t415_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CapsuleCollider_t415_0_0_0;
extern const Il2CppType CapsuleCollider_t415_1_0_0;
struct CapsuleCollider_t415;
const Il2CppTypeDefinitionMetadata CapsuleCollider_t415_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Collider_t319_0_0_0/* parent */
	, CapsuleCollider_t415_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5316/* methodStart */
	, -1/* eventStart */
	, 980/* propertyStart */

};
TypeInfo CapsuleCollider_t415_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CapsuleCollider"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &CapsuleCollider_t415_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CapsuleCollider_t415_0_0_0/* byval_arg */
	, &CapsuleCollider_t415_1_0_0/* this_arg */
	, &CapsuleCollider_t415_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CapsuleCollider_t415)/* instance_size */
	, sizeof (CapsuleCollider_t415)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// Metadata Definition UnityEngine.RaycastHit
extern TypeInfo RaycastHit_t482_il2cpp_TypeInfo;
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"
static const EncodedMethodIndex RaycastHit_t482_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RaycastHit_t482_0_0_0;
extern const Il2CppType RaycastHit_t482_1_0_0;
const Il2CppTypeDefinitionMetadata RaycastHit_t482_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, RaycastHit_t482_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4389/* fieldStart */
	, 5323/* methodStart */
	, -1/* eventStart */
	, 983/* propertyStart */

};
TypeInfo RaycastHit_t482_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RaycastHit"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &RaycastHit_t482_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RaycastHit_t482_0_0_0/* byval_arg */
	, &RaycastHit_t482_1_0_0/* this_arg */
	, &RaycastHit_t482_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RaycastHit_t482)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RaycastHit_t482)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 7/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.PhysicMaterial
#include "UnityEngine_UnityEngine_PhysicMaterial.h"
// Metadata Definition UnityEngine.PhysicMaterial
extern TypeInfo PhysicMaterial_t410_il2cpp_TypeInfo;
// UnityEngine.PhysicMaterial
#include "UnityEngine_UnityEngine_PhysicMaterialMethodDeclarations.h"
static const EncodedMethodIndex PhysicMaterial_t410_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PhysicMaterial_t410_0_0_0;
extern const Il2CppType PhysicMaterial_t410_1_0_0;
struct PhysicMaterial_t410;
const Il2CppTypeDefinitionMetadata PhysicMaterial_t410_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t473_0_0_0/* parent */
	, PhysicMaterial_t410_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PhysicMaterial_t410_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PhysicMaterial"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &PhysicMaterial_t410_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PhysicMaterial_t410_0_0_0/* byval_arg */
	, &PhysicMaterial_t410_1_0_0/* this_arg */
	, &PhysicMaterial_t410_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PhysicMaterial_t410)/* instance_size */
	, sizeof (PhysicMaterial_t410)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.ContactPoint
#include "UnityEngine_UnityEngine_ContactPoint.h"
// Metadata Definition UnityEngine.ContactPoint
extern TypeInfo ContactPoint_t930_il2cpp_TypeInfo;
// UnityEngine.ContactPoint
#include "UnityEngine_UnityEngine_ContactPointMethodDeclarations.h"
static const EncodedMethodIndex ContactPoint_t930_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ContactPoint_t930_0_0_0;
extern const Il2CppType ContactPoint_t930_1_0_0;
const Il2CppTypeDefinitionMetadata ContactPoint_t930_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, ContactPoint_t930_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4395/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ContactPoint_t930_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContactPoint"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &ContactPoint_t930_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ContactPoint_t930_0_0_0/* byval_arg */
	, &ContactPoint_t930_1_0_0/* this_arg */
	, &ContactPoint_t930_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContactPoint_t930)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ContactPoint_t930)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(ContactPoint_t930 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Collision
#include "UnityEngine_UnityEngine_Collision.h"
// Metadata Definition UnityEngine.Collision
extern TypeInfo Collision_t471_il2cpp_TypeInfo;
// UnityEngine.Collision
#include "UnityEngine_UnityEngine_CollisionMethodDeclarations.h"
static const EncodedMethodIndex Collision_t471_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Collision_t471_0_0_0;
extern const Il2CppType Collision_t471_1_0_0;
struct Collision_t471;
const Il2CppTypeDefinitionMetadata Collision_t471_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Collision_t471_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4399/* fieldStart */
	, 5332/* methodStart */
	, -1/* eventStart */
	, 990/* propertyStart */

};
TypeInfo Collision_t471_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Collision"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Collision_t471_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Collision_t471_0_0_0/* byval_arg */
	, &Collision_t471_1_0_0/* this_arg */
	, &Collision_t471_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Collision_t471)/* instance_size */
	, sizeof (Collision_t471)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 5/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Cloth
#include "UnityEngine_UnityEngine_Cloth.h"
// Metadata Definition UnityEngine.Cloth
extern TypeInfo Cloth_t337_il2cpp_TypeInfo;
// UnityEngine.Cloth
#include "UnityEngine_UnityEngine_ClothMethodDeclarations.h"
static const EncodedMethodIndex Cloth_t337_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Cloth_t337_0_0_0;
extern const Il2CppType Cloth_t337_1_0_0;
struct Cloth_t337;
const Il2CppTypeDefinitionMetadata Cloth_t337_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Component_t477_0_0_0/* parent */
	, Cloth_t337_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5337/* methodStart */
	, -1/* eventStart */
	, 995/* propertyStart */

};
TypeInfo Cloth_t337_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Cloth"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Cloth_t337_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Cloth_t337_0_0_0/* byval_arg */
	, &Cloth_t337_1_0_0/* this_arg */
	, &Cloth_t337_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Cloth_t337)/* instance_size */
	, sizeof (Cloth_t337)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Physics2D
#include "UnityEngine_UnityEngine_Physics2D.h"
// Metadata Definition UnityEngine.Physics2D
extern TypeInfo Physics2D_t788_il2cpp_TypeInfo;
// UnityEngine.Physics2D
#include "UnityEngine_UnityEngine_Physics2DMethodDeclarations.h"
static const EncodedMethodIndex Physics2D_t788_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Physics2D_t788_0_0_0;
extern const Il2CppType Physics2D_t788_1_0_0;
struct Physics2D_t788;
const Il2CppTypeDefinitionMetadata Physics2D_t788_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Physics2D_t788_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4403/* fieldStart */
	, 5342/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Physics2D_t788_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Physics2D"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Physics2D_t788_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Physics2D_t788_0_0_0/* byval_arg */
	, &Physics2D_t788_1_0_0/* this_arg */
	, &Physics2D_t788_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Physics2D_t788)/* instance_size */
	, sizeof (Physics2D_t788)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Physics2D_t788_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
// Metadata Definition UnityEngine.RaycastHit2D
extern TypeInfo RaycastHit2D_t789_il2cpp_TypeInfo;
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2DMethodDeclarations.h"
static const EncodedMethodIndex RaycastHit2D_t789_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RaycastHit2D_t789_0_0_0;
extern const Il2CppType RaycastHit2D_t789_1_0_0;
const Il2CppTypeDefinitionMetadata RaycastHit2D_t789_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, RaycastHit2D_t789_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4404/* fieldStart */
	, 5349/* methodStart */
	, -1/* eventStart */
	, 998/* propertyStart */

};
TypeInfo RaycastHit2D_t789_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RaycastHit2D"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &RaycastHit2D_t789_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RaycastHit2D_t789_0_0_0/* byval_arg */
	, &RaycastHit2D_t789_1_0_0/* this_arg */
	, &RaycastHit2D_t789_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RaycastHit2D_t789)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RaycastHit2D_t789)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Rigidbody2D
#include "UnityEngine_UnityEngine_Rigidbody2D.h"
// Metadata Definition UnityEngine.Rigidbody2D
extern TypeInfo Rigidbody2D_t933_il2cpp_TypeInfo;
// UnityEngine.Rigidbody2D
#include "UnityEngine_UnityEngine_Rigidbody2DMethodDeclarations.h"
static const EncodedMethodIndex Rigidbody2D_t933_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Rigidbody2D_t933_0_0_0;
extern const Il2CppType Rigidbody2D_t933_1_0_0;
struct Rigidbody2D_t933;
const Il2CppTypeDefinitionMetadata Rigidbody2D_t933_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Component_t477_0_0_0/* parent */
	, Rigidbody2D_t933_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Rigidbody2D_t933_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Rigidbody2D"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Rigidbody2D_t933_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Rigidbody2D_t933_0_0_0/* byval_arg */
	, &Rigidbody2D_t933_1_0_0/* this_arg */
	, &Rigidbody2D_t933_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Rigidbody2D_t933)/* instance_size */
	, sizeof (Rigidbody2D_t933)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Collider2D
#include "UnityEngine_UnityEngine_Collider2D.h"
// Metadata Definition UnityEngine.Collider2D
extern TypeInfo Collider2D_t790_il2cpp_TypeInfo;
// UnityEngine.Collider2D
#include "UnityEngine_UnityEngine_Collider2DMethodDeclarations.h"
static const EncodedMethodIndex Collider2D_t790_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Collider2D_t790_0_0_0;
extern const Il2CppType Collider2D_t790_1_0_0;
struct Collider2D_t790;
const Il2CppTypeDefinitionMetadata Collider2D_t790_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t825_0_0_0/* parent */
	, Collider2D_t790_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5355/* methodStart */
	, -1/* eventStart */
	, 1004/* propertyStart */

};
TypeInfo Collider2D_t790_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Collider2D"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Collider2D_t790_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Collider2D_t790_0_0_0/* byval_arg */
	, &Collider2D_t790_1_0_0/* this_arg */
	, &Collider2D_t790_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Collider2D_t790)/* instance_size */
	, sizeof (Collider2D_t790)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.NavMeshAgent
#include "UnityEngine_UnityEngine_NavMeshAgent.h"
// Metadata Definition UnityEngine.NavMeshAgent
extern TypeInfo NavMeshAgent_t307_il2cpp_TypeInfo;
// UnityEngine.NavMeshAgent
#include "UnityEngine_UnityEngine_NavMeshAgentMethodDeclarations.h"
static const EncodedMethodIndex NavMeshAgent_t307_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType NavMeshAgent_t307_0_0_0;
extern const Il2CppType NavMeshAgent_t307_1_0_0;
struct NavMeshAgent_t307;
const Il2CppTypeDefinitionMetadata NavMeshAgent_t307_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t825_0_0_0/* parent */
	, NavMeshAgent_t307_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5356/* methodStart */
	, -1/* eventStart */
	, 1005/* propertyStart */

};
TypeInfo NavMeshAgent_t307_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "NavMeshAgent"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &NavMeshAgent_t307_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NavMeshAgent_t307_0_0_0/* byval_arg */
	, &NavMeshAgent_t307_1_0_0/* this_arg */
	, &NavMeshAgent_t307_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NavMeshAgent_t307)/* instance_size */
	, sizeof (NavMeshAgent_t307)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AudioSettings
#include "UnityEngine_UnityEngine_AudioSettings.h"
// Metadata Definition UnityEngine.AudioSettings
extern TypeInfo AudioSettings_t935_il2cpp_TypeInfo;
// UnityEngine.AudioSettings
#include "UnityEngine_UnityEngine_AudioSettingsMethodDeclarations.h"
extern const Il2CppType AudioConfigurationChangeHandler_t934_0_0_0;
static const Il2CppType* AudioSettings_t935_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AudioConfigurationChangeHandler_t934_0_0_0,
};
static const EncodedMethodIndex AudioSettings_t935_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AudioSettings_t935_0_0_0;
extern const Il2CppType AudioSettings_t935_1_0_0;
struct AudioSettings_t935;
const Il2CppTypeDefinitionMetadata AudioSettings_t935_DefinitionMetadata = 
{
	NULL/* declaringType */
	, AudioSettings_t935_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AudioSettings_t935_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4410/* fieldStart */
	, 5361/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AudioSettings_t935_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AudioSettings"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AudioSettings_t935_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AudioSettings_t935_0_0_0/* byval_arg */
	, &AudioSettings_t935_1_0_0/* this_arg */
	, &AudioSettings_t935_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AudioSettings_t935)/* instance_size */
	, sizeof (AudioSettings_t935)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AudioSettings_t935_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChan.h"
// Metadata Definition UnityEngine.AudioSettings/AudioConfigurationChangeHandler
extern TypeInfo AudioConfigurationChangeHandler_t934_il2cpp_TypeInfo;
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChanMethodDeclarations.h"
static const EncodedMethodIndex AudioConfigurationChangeHandler_t934_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1383,
	1384,
	1385,
};
static Il2CppInterfaceOffsetPair AudioConfigurationChangeHandler_t934_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AudioConfigurationChangeHandler_t934_1_0_0;
struct AudioConfigurationChangeHandler_t934;
const Il2CppTypeDefinitionMetadata AudioConfigurationChangeHandler_t934_DefinitionMetadata = 
{
	&AudioSettings_t935_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AudioConfigurationChangeHandler_t934_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, AudioConfigurationChangeHandler_t934_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5362/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AudioConfigurationChangeHandler_t934_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AudioConfigurationChangeHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &AudioConfigurationChangeHandler_t934_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AudioConfigurationChangeHandler_t934_0_0_0/* byval_arg */
	, &AudioConfigurationChangeHandler_t934_1_0_0/* this_arg */
	, &AudioConfigurationChangeHandler_t934_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t934/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AudioConfigurationChangeHandler_t934)/* instance_size */
	, sizeof (AudioConfigurationChangeHandler_t934)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClip.h"
// Metadata Definition UnityEngine.AudioClip
extern TypeInfo AudioClip_t472_il2cpp_TypeInfo;
// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClipMethodDeclarations.h"
extern const Il2CppType PCMReaderCallback_t936_0_0_0;
extern const Il2CppType PCMSetPositionCallback_t937_0_0_0;
static const Il2CppType* AudioClip_t472_il2cpp_TypeInfo__nestedTypes[2] =
{
	&PCMReaderCallback_t936_0_0_0,
	&PCMSetPositionCallback_t937_0_0_0,
};
static const EncodedMethodIndex AudioClip_t472_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AudioClip_t472_0_0_0;
extern const Il2CppType AudioClip_t472_1_0_0;
struct AudioClip_t472;
const Il2CppTypeDefinitionMetadata AudioClip_t472_DefinitionMetadata = 
{
	NULL/* declaringType */
	, AudioClip_t472_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t473_0_0_0/* parent */
	, AudioClip_t472_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4411/* fieldStart */
	, 5366/* methodStart */
	, -1/* eventStart */
	, 1007/* propertyStart */

};
TypeInfo AudioClip_t472_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AudioClip"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AudioClip_t472_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AudioClip_t472_0_0_0/* byval_arg */
	, &AudioClip_t472_1_0_0/* this_arg */
	, &AudioClip_t472_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AudioClip_t472)/* instance_size */
	, sizeof (AudioClip_t472)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AudioClip/PCMReaderCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallback.h"
// Metadata Definition UnityEngine.AudioClip/PCMReaderCallback
extern TypeInfo PCMReaderCallback_t936_il2cpp_TypeInfo;
// UnityEngine.AudioClip/PCMReaderCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbackMethodDeclarations.h"
static const EncodedMethodIndex PCMReaderCallback_t936_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1386,
	1387,
	1388,
};
static Il2CppInterfaceOffsetPair PCMReaderCallback_t936_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PCMReaderCallback_t936_1_0_0;
struct PCMReaderCallback_t936;
const Il2CppTypeDefinitionMetadata PCMReaderCallback_t936_DefinitionMetadata = 
{
	&AudioClip_t472_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PCMReaderCallback_t936_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, PCMReaderCallback_t936_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5369/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PCMReaderCallback_t936_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PCMReaderCallback"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &PCMReaderCallback_t936_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PCMReaderCallback_t936_0_0_0/* byval_arg */
	, &PCMReaderCallback_t936_1_0_0/* this_arg */
	, &PCMReaderCallback_t936_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PCMReaderCallback_t936/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PCMReaderCallback_t936)/* instance_size */
	, sizeof (PCMReaderCallback_t936)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.AudioClip/PCMSetPositionCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallback.h"
// Metadata Definition UnityEngine.AudioClip/PCMSetPositionCallback
extern TypeInfo PCMSetPositionCallback_t937_il2cpp_TypeInfo;
// UnityEngine.AudioClip/PCMSetPositionCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallbackMethodDeclarations.h"
static const EncodedMethodIndex PCMSetPositionCallback_t937_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1389,
	1390,
	1391,
};
static Il2CppInterfaceOffsetPair PCMSetPositionCallback_t937_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PCMSetPositionCallback_t937_1_0_0;
struct PCMSetPositionCallback_t937;
const Il2CppTypeDefinitionMetadata PCMSetPositionCallback_t937_DefinitionMetadata = 
{
	&AudioClip_t472_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PCMSetPositionCallback_t937_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, PCMSetPositionCallback_t937_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5373/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PCMSetPositionCallback_t937_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PCMSetPositionCallback"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &PCMSetPositionCallback_t937_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PCMSetPositionCallback_t937_0_0_0/* byval_arg */
	, &PCMSetPositionCallback_t937_1_0_0/* this_arg */
	, &PCMSetPositionCallback_t937_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PCMSetPositionCallback_t937/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PCMSetPositionCallback_t937)/* instance_size */
	, sizeof (PCMSetPositionCallback_t937)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSource.h"
// Metadata Definition UnityEngine.AudioSource
extern TypeInfo AudioSource_t339_il2cpp_TypeInfo;
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSourceMethodDeclarations.h"
static const EncodedMethodIndex AudioSource_t339_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AudioSource_t339_0_0_0;
extern const Il2CppType AudioSource_t339_1_0_0;
struct AudioSource_t339;
const Il2CppTypeDefinitionMetadata AudioSource_t339_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t825_0_0_0/* parent */
	, AudioSource_t339_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5377/* methodStart */
	, -1/* eventStart */
	, 1008/* propertyStart */

};
TypeInfo AudioSource_t339_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AudioSource"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AudioSource_t339_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AudioSource_t339_0_0_0/* byval_arg */
	, &AudioSource_t339_1_0_0/* this_arg */
	, &AudioSource_t339_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AudioSource_t339)/* instance_size */
	, sizeof (AudioSource_t339)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDevice.h"
// Metadata Definition UnityEngine.WebCamDevice
extern TypeInfo WebCamDevice_t938_il2cpp_TypeInfo;
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDeviceMethodDeclarations.h"
static const EncodedMethodIndex WebCamDevice_t938_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WebCamDevice_t938_0_0_0;
extern const Il2CppType WebCamDevice_t938_1_0_0;
const Il2CppTypeDefinitionMetadata WebCamDevice_t938_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, WebCamDevice_t938_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4413/* fieldStart */
	, 5390/* methodStart */
	, -1/* eventStart */
	, 1013/* propertyStart */

};
TypeInfo WebCamDevice_t938_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebCamDevice"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WebCamDevice_t938_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WebCamDevice_t938_0_0_0/* byval_arg */
	, &WebCamDevice_t938_1_0_0/* this_arg */
	, &WebCamDevice_t938_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)WebCamDevice_t938_marshal/* marshal_to_native_func */
	, (methodPointerType)WebCamDevice_t938_marshal_back/* marshal_from_native_func */
	, (methodPointerType)WebCamDevice_t938_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (WebCamDevice_t938)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (WebCamDevice_t938)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(WebCamDevice_t938_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AnimationEventSource
#include "UnityEngine_UnityEngine_AnimationEventSource.h"
// Metadata Definition UnityEngine.AnimationEventSource
extern TypeInfo AnimationEventSource_t939_il2cpp_TypeInfo;
// UnityEngine.AnimationEventSource
#include "UnityEngine_UnityEngine_AnimationEventSourceMethodDeclarations.h"
static const EncodedMethodIndex AnimationEventSource_t939_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair AnimationEventSource_t939_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AnimationEventSource_t939_0_0_0;
extern const Il2CppType AnimationEventSource_t939_1_0_0;
const Il2CppTypeDefinitionMetadata AnimationEventSource_t939_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AnimationEventSource_t939_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, AnimationEventSource_t939_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4415/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AnimationEventSource_t939_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimationEventSource"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnimationEventSource_t939_0_0_0/* byval_arg */
	, &AnimationEventSource_t939_1_0_0/* this_arg */
	, &AnimationEventSource_t939_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnimationEventSource_t939)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AnimationEventSource_t939)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.AnimationEvent
#include "UnityEngine_UnityEngine_AnimationEvent.h"
// Metadata Definition UnityEngine.AnimationEvent
extern TypeInfo AnimationEvent_t941_il2cpp_TypeInfo;
// UnityEngine.AnimationEvent
#include "UnityEngine_UnityEngine_AnimationEventMethodDeclarations.h"
static const EncodedMethodIndex AnimationEvent_t941_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AnimationEvent_t941_0_0_0;
extern const Il2CppType AnimationEvent_t941_1_0_0;
struct AnimationEvent_t941;
const Il2CppTypeDefinitionMetadata AnimationEvent_t941_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AnimationEvent_t941_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4419/* fieldStart */
	, 5392/* methodStart */
	, -1/* eventStart */
	, 1015/* propertyStart */

};
TypeInfo AnimationEvent_t941_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimationEvent"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AnimationEvent_t941_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnimationEvent_t941_0_0_0/* byval_arg */
	, &AnimationEvent_t941_1_0_0/* this_arg */
	, &AnimationEvent_t941_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnimationEvent_t941)/* instance_size */
	, sizeof (AnimationEvent_t941)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 23/* method_count */
	, 13/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"
// Metadata Definition UnityEngine.Keyframe
extern TypeInfo Keyframe_t943_il2cpp_TypeInfo;
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_KeyframeMethodDeclarations.h"
static const EncodedMethodIndex Keyframe_t943_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Keyframe_t943_0_0_0;
extern const Il2CppType Keyframe_t943_1_0_0;
const Il2CppTypeDefinitionMetadata Keyframe_t943_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Keyframe_t943_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4430/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Keyframe_t943_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Keyframe"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Keyframe_t943_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Keyframe_t943_0_0_0/* byval_arg */
	, &Keyframe_t943_1_0_0/* this_arg */
	, &Keyframe_t943_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Keyframe_t943)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Keyframe_t943)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Keyframe_t943 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AnimationCurve
#include "UnityEngine_UnityEngine_AnimationCurve.h"
// Metadata Definition UnityEngine.AnimationCurve
extern TypeInfo AnimationCurve_t944_il2cpp_TypeInfo;
// UnityEngine.AnimationCurve
#include "UnityEngine_UnityEngine_AnimationCurveMethodDeclarations.h"
static const EncodedMethodIndex AnimationCurve_t944_VTable[4] = 
{
	626,
	1392,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AnimationCurve_t944_0_0_0;
extern const Il2CppType AnimationCurve_t944_1_0_0;
struct AnimationCurve_t944;
const Il2CppTypeDefinitionMetadata AnimationCurve_t944_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AnimationCurve_t944_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4434/* fieldStart */
	, 5415/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AnimationCurve_t944_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimationCurve"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AnimationCurve_t944_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1963/* custom_attributes_cache */
	, &AnimationCurve_t944_0_0_0/* byval_arg */
	, &AnimationCurve_t944_1_0_0/* this_arg */
	, &AnimationCurve_t944_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)AnimationCurve_t944_marshal/* marshal_to_native_func */
	, (methodPointerType)AnimationCurve_t944_marshal_back/* marshal_from_native_func */
	, (methodPointerType)AnimationCurve_t944_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (AnimationCurve_t944)/* instance_size */
	, sizeof (AnimationCurve_t944)/* actualSize */
	, 0/* element_size */
	, sizeof(AnimationCurve_t944_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AnimationState
#include "UnityEngine_UnityEngine_AnimationState.h"
// Metadata Definition UnityEngine.AnimationState
extern TypeInfo AnimationState_t940_il2cpp_TypeInfo;
// UnityEngine.AnimationState
#include "UnityEngine_UnityEngine_AnimationStateMethodDeclarations.h"
static const EncodedMethodIndex AnimationState_t940_VTable[4] = 
{
	1393,
	601,
	1394,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AnimationState_t940_0_0_0;
extern const Il2CppType AnimationState_t940_1_0_0;
extern const Il2CppType TrackedReference_t945_0_0_0;
struct AnimationState_t940;
const Il2CppTypeDefinitionMetadata AnimationState_t940_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TrackedReference_t945_0_0_0/* parent */
	, AnimationState_t940_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AnimationState_t940_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimationState"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AnimationState_t940_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnimationState_t940_0_0_0/* byval_arg */
	, &AnimationState_t940_1_0_0/* this_arg */
	, &AnimationState_t940_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnimationState_t940)/* instance_size */
	, sizeof (AnimationState_t940)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AvatarIKGoal
#include "UnityEngine_UnityEngine_AvatarIKGoal.h"
// Metadata Definition UnityEngine.AvatarIKGoal
extern TypeInfo AvatarIKGoal_t946_il2cpp_TypeInfo;
// UnityEngine.AvatarIKGoal
#include "UnityEngine_UnityEngine_AvatarIKGoalMethodDeclarations.h"
static const EncodedMethodIndex AvatarIKGoal_t946_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair AvatarIKGoal_t946_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AvatarIKGoal_t946_0_0_0;
extern const Il2CppType AvatarIKGoal_t946_1_0_0;
const Il2CppTypeDefinitionMetadata AvatarIKGoal_t946_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AvatarIKGoal_t946_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, AvatarIKGoal_t946_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4435/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AvatarIKGoal_t946_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AvatarIKGoal"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AvatarIKGoal_t946_0_0_0/* byval_arg */
	, &AvatarIKGoal_t946_1_0_0/* this_arg */
	, &AvatarIKGoal_t946_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AvatarIKGoal_t946)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AvatarIKGoal_t946)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.AvatarIKHint
#include "UnityEngine_UnityEngine_AvatarIKHint.h"
// Metadata Definition UnityEngine.AvatarIKHint
extern TypeInfo AvatarIKHint_t947_il2cpp_TypeInfo;
// UnityEngine.AvatarIKHint
#include "UnityEngine_UnityEngine_AvatarIKHintMethodDeclarations.h"
static const EncodedMethodIndex AvatarIKHint_t947_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair AvatarIKHint_t947_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AvatarIKHint_t947_0_0_0;
extern const Il2CppType AvatarIKHint_t947_1_0_0;
const Il2CppTypeDefinitionMetadata AvatarIKHint_t947_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AvatarIKHint_t947_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, AvatarIKHint_t947_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4440/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AvatarIKHint_t947_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AvatarIKHint"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AvatarIKHint_t947_0_0_0/* byval_arg */
	, &AvatarIKHint_t947_1_0_0/* this_arg */
	, &AvatarIKHint_t947_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AvatarIKHint_t947)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AvatarIKHint_t947)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
// Metadata Definition UnityEngine.AnimatorClipInfo
extern TypeInfo AnimatorClipInfo_t942_il2cpp_TypeInfo;
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfoMethodDeclarations.h"
static const EncodedMethodIndex AnimatorClipInfo_t942_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AnimatorClipInfo_t942_0_0_0;
extern const Il2CppType AnimatorClipInfo_t942_1_0_0;
const Il2CppTypeDefinitionMetadata AnimatorClipInfo_t942_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, AnimatorClipInfo_t942_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4445/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AnimatorClipInfo_t942_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimatorClipInfo"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AnimatorClipInfo_t942_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnimatorClipInfo_t942_0_0_0/* byval_arg */
	, &AnimatorClipInfo_t942_1_0_0/* this_arg */
	, &AnimatorClipInfo_t942_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnimatorClipInfo_t942)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AnimatorClipInfo_t942)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(AnimatorClipInfo_t942 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
// Metadata Definition UnityEngine.AnimatorStateInfo
extern TypeInfo AnimatorStateInfo_t542_il2cpp_TypeInfo;
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfoMethodDeclarations.h"
static const EncodedMethodIndex AnimatorStateInfo_t542_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AnimatorStateInfo_t542_0_0_0;
extern const Il2CppType AnimatorStateInfo_t542_1_0_0;
const Il2CppTypeDefinitionMetadata AnimatorStateInfo_t542_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, AnimatorStateInfo_t542_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4447/* fieldStart */
	, 5420/* methodStart */
	, -1/* eventStart */
	, 1028/* propertyStart */

};
TypeInfo AnimatorStateInfo_t542_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimatorStateInfo"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AnimatorStateInfo_t542_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnimatorStateInfo_t542_0_0_0/* byval_arg */
	, &AnimatorStateInfo_t542_1_0_0/* this_arg */
	, &AnimatorStateInfo_t542_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnimatorStateInfo_t542)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AnimatorStateInfo_t542)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(AnimatorStateInfo_t542 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 9/* method_count */
	, 7/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AnimatorTransitionInfo
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo.h"
// Metadata Definition UnityEngine.AnimatorTransitionInfo
extern TypeInfo AnimatorTransitionInfo_t948_il2cpp_TypeInfo;
// UnityEngine.AnimatorTransitionInfo
#include "UnityEngine_UnityEngine_AnimatorTransitionInfoMethodDeclarations.h"
static const EncodedMethodIndex AnimatorTransitionInfo_t948_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AnimatorTransitionInfo_t948_0_0_0;
extern const Il2CppType AnimatorTransitionInfo_t948_1_0_0;
const Il2CppTypeDefinitionMetadata AnimatorTransitionInfo_t948_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, AnimatorTransitionInfo_t948_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4454/* fieldStart */
	, 5429/* methodStart */
	, -1/* eventStart */
	, 1035/* propertyStart */

};
TypeInfo AnimatorTransitionInfo_t948_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimatorTransitionInfo"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AnimatorTransitionInfo_t948_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnimatorTransitionInfo_t948_0_0_0/* byval_arg */
	, &AnimatorTransitionInfo_t948_1_0_0/* this_arg */
	, &AnimatorTransitionInfo_t948_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)AnimatorTransitionInfo_t948_marshal/* marshal_to_native_func */
	, (methodPointerType)AnimatorTransitionInfo_t948_marshal_back/* marshal_from_native_func */
	, (methodPointerType)AnimatorTransitionInfo_t948_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (AnimatorTransitionInfo_t948)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AnimatorTransitionInfo_t948)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(AnimatorTransitionInfo_t948_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 7/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Animator
#include "UnityEngine_UnityEngine_Animator.h"
// Metadata Definition UnityEngine.Animator
extern TypeInfo Animator_t321_il2cpp_TypeInfo;
// UnityEngine.Animator
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"
static const EncodedMethodIndex Animator_t321_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Animator_t321_0_0_0;
extern const Il2CppType Animator_t321_1_0_0;
struct Animator_t321;
const Il2CppTypeDefinitionMetadata Animator_t321_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t825_0_0_0/* parent */
	, Animator_t321_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5438/* methodStart */
	, -1/* eventStart */
	, 1042/* propertyStart */

};
TypeInfo Animator_t321_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Animator"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Animator_t321_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Animator_t321_0_0_0/* byval_arg */
	, &Animator_t321_1_0_0/* this_arg */
	, &Animator_t321_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Animator_t321)/* instance_size */
	, sizeof (Animator_t321)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 38/* method_count */
	, 7/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SkeletonBone
#include "UnityEngine_UnityEngine_SkeletonBone.h"
// Metadata Definition UnityEngine.SkeletonBone
extern TypeInfo SkeletonBone_t949_il2cpp_TypeInfo;
// UnityEngine.SkeletonBone
#include "UnityEngine_UnityEngine_SkeletonBoneMethodDeclarations.h"
static const EncodedMethodIndex SkeletonBone_t949_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SkeletonBone_t949_0_0_0;
extern const Il2CppType SkeletonBone_t949_1_0_0;
const Il2CppTypeDefinitionMetadata SkeletonBone_t949_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, SkeletonBone_t949_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4460/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SkeletonBone_t949_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SkeletonBone"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SkeletonBone_t949_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SkeletonBone_t949_0_0_0/* byval_arg */
	, &SkeletonBone_t949_1_0_0/* this_arg */
	, &SkeletonBone_t949_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)SkeletonBone_t949_marshal/* marshal_to_native_func */
	, (methodPointerType)SkeletonBone_t949_marshal_back/* marshal_from_native_func */
	, (methodPointerType)SkeletonBone_t949_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (SkeletonBone_t949)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SkeletonBone_t949)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(SkeletonBone_t949_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.HumanLimit
#include "UnityEngine_UnityEngine_HumanLimit.h"
// Metadata Definition UnityEngine.HumanLimit
extern TypeInfo HumanLimit_t950_il2cpp_TypeInfo;
// UnityEngine.HumanLimit
#include "UnityEngine_UnityEngine_HumanLimitMethodDeclarations.h"
static const EncodedMethodIndex HumanLimit_t950_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HumanLimit_t950_0_0_0;
extern const Il2CppType HumanLimit_t950_1_0_0;
const Il2CppTypeDefinitionMetadata HumanLimit_t950_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, HumanLimit_t950_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4465/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HumanLimit_t950_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HumanLimit"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &HumanLimit_t950_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HumanLimit_t950_0_0_0/* byval_arg */
	, &HumanLimit_t950_1_0_0/* this_arg */
	, &HumanLimit_t950_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HumanLimit_t950)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HumanLimit_t950)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(HumanLimit_t950 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.HumanBone
#include "UnityEngine_UnityEngine_HumanBone.h"
// Metadata Definition UnityEngine.HumanBone
extern TypeInfo HumanBone_t951_il2cpp_TypeInfo;
// UnityEngine.HumanBone
#include "UnityEngine_UnityEngine_HumanBoneMethodDeclarations.h"
static const EncodedMethodIndex HumanBone_t951_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HumanBone_t951_0_0_0;
extern const Il2CppType HumanBone_t951_1_0_0;
const Il2CppTypeDefinitionMetadata HumanBone_t951_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, HumanBone_t951_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4470/* fieldStart */
	, 5476/* methodStart */
	, -1/* eventStart */
	, 1049/* propertyStart */

};
TypeInfo HumanBone_t951_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HumanBone"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &HumanBone_t951_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HumanBone_t951_0_0_0/* byval_arg */
	, &HumanBone_t951_1_0_0/* this_arg */
	, &HumanBone_t951_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)HumanBone_t951_marshal/* marshal_to_native_func */
	, (methodPointerType)HumanBone_t951_marshal_back/* marshal_from_native_func */
	, (methodPointerType)HumanBone_t951_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (HumanBone_t951)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HumanBone_t951)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(HumanBone_t951_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RuntimeAnimatorController
#include "UnityEngine_UnityEngine_RuntimeAnimatorController.h"
// Metadata Definition UnityEngine.RuntimeAnimatorController
extern TypeInfo RuntimeAnimatorController_t816_il2cpp_TypeInfo;
// UnityEngine.RuntimeAnimatorController
#include "UnityEngine_UnityEngine_RuntimeAnimatorControllerMethodDeclarations.h"
static const EncodedMethodIndex RuntimeAnimatorController_t816_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RuntimeAnimatorController_t816_0_0_0;
extern const Il2CppType RuntimeAnimatorController_t816_1_0_0;
struct RuntimeAnimatorController_t816;
const Il2CppTypeDefinitionMetadata RuntimeAnimatorController_t816_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t473_0_0_0/* parent */
	, RuntimeAnimatorController_t816_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RuntimeAnimatorController_t816_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeAnimatorController"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &RuntimeAnimatorController_t816_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RuntimeAnimatorController_t816_0_0_0/* byval_arg */
	, &RuntimeAnimatorController_t816_1_0_0/* this_arg */
	, &RuntimeAnimatorController_t816_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeAnimatorController_t816)/* instance_size */
	, sizeof (RuntimeAnimatorController_t816)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.HumanBodyBones
#include "UnityEngine_UnityEngine_HumanBodyBones.h"
// Metadata Definition UnityEngine.HumanBodyBones
extern TypeInfo HumanBodyBones_t952_il2cpp_TypeInfo;
// UnityEngine.HumanBodyBones
#include "UnityEngine_UnityEngine_HumanBodyBonesMethodDeclarations.h"
static const EncodedMethodIndex HumanBodyBones_t952_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair HumanBodyBones_t952_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HumanBodyBones_t952_0_0_0;
extern const Il2CppType HumanBodyBones_t952_1_0_0;
const Il2CppTypeDefinitionMetadata HumanBodyBones_t952_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HumanBodyBones_t952_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, HumanBodyBones_t952_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4473/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HumanBodyBones_t952_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HumanBodyBones"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HumanBodyBones_t952_0_0_0/* byval_arg */
	, &HumanBodyBones_t952_1_0_0/* this_arg */
	, &HumanBodyBones_t952_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HumanBodyBones_t952)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HumanBodyBones_t952)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 56/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Avatar
#include "UnityEngine_UnityEngine_Avatar.h"
// Metadata Definition UnityEngine.Avatar
extern TypeInfo Avatar_t543_il2cpp_TypeInfo;
// UnityEngine.Avatar
#include "UnityEngine_UnityEngine_AvatarMethodDeclarations.h"
static const EncodedMethodIndex Avatar_t543_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Avatar_t543_0_0_0;
extern const Il2CppType Avatar_t543_1_0_0;
struct Avatar_t543;
const Il2CppTypeDefinitionMetadata Avatar_t543_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t473_0_0_0/* parent */
	, Avatar_t543_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Avatar_t543_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Avatar"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Avatar_t543_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Avatar_t543_0_0_0/* byval_arg */
	, &Avatar_t543_1_0_0/* this_arg */
	, &Avatar_t543_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Avatar_t543)/* instance_size */
	, sizeof (Avatar_t543)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
// Metadata Definition UnityEngine.TextAnchor
extern TypeInfo TextAnchor_t819_il2cpp_TypeInfo;
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchorMethodDeclarations.h"
static const EncodedMethodIndex TextAnchor_t819_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair TextAnchor_t819_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextAnchor_t819_0_0_0;
extern const Il2CppType TextAnchor_t819_1_0_0;
const Il2CppTypeDefinitionMetadata TextAnchor_t819_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextAnchor_t819_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, TextAnchor_t819_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4529/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TextAnchor_t819_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextAnchor"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextAnchor_t819_0_0_0/* byval_arg */
	, &TextAnchor_t819_1_0_0/* this_arg */
	, &TextAnchor_t819_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextAnchor_t819)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextAnchor_t819)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
// Metadata Definition UnityEngine.HorizontalWrapMode
extern TypeInfo HorizontalWrapMode_t953_il2cpp_TypeInfo;
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapModeMethodDeclarations.h"
static const EncodedMethodIndex HorizontalWrapMode_t953_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair HorizontalWrapMode_t953_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HorizontalWrapMode_t953_0_0_0;
extern const Il2CppType HorizontalWrapMode_t953_1_0_0;
const Il2CppTypeDefinitionMetadata HorizontalWrapMode_t953_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HorizontalWrapMode_t953_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, HorizontalWrapMode_t953_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4539/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HorizontalWrapMode_t953_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HorizontalWrapMode"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HorizontalWrapMode_t953_0_0_0/* byval_arg */
	, &HorizontalWrapMode_t953_1_0_0/* this_arg */
	, &HorizontalWrapMode_t953_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HorizontalWrapMode_t953)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HorizontalWrapMode_t953)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
// Metadata Definition UnityEngine.VerticalWrapMode
extern TypeInfo VerticalWrapMode_t954_il2cpp_TypeInfo;
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapModeMethodDeclarations.h"
static const EncodedMethodIndex VerticalWrapMode_t954_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair VerticalWrapMode_t954_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType VerticalWrapMode_t954_0_0_0;
extern const Il2CppType VerticalWrapMode_t954_1_0_0;
const Il2CppTypeDefinitionMetadata VerticalWrapMode_t954_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, VerticalWrapMode_t954_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, VerticalWrapMode_t954_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4542/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo VerticalWrapMode_t954_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "VerticalWrapMode"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VerticalWrapMode_t954_0_0_0/* byval_arg */
	, &VerticalWrapMode_t954_1_0_0/* this_arg */
	, &VerticalWrapMode_t954_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VerticalWrapMode_t954)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (VerticalWrapMode_t954)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.GUIText
#include "UnityEngine_UnityEngine_GUIText.h"
// Metadata Definition UnityEngine.GUIText
extern TypeInfo GUIText_t549_il2cpp_TypeInfo;
// UnityEngine.GUIText
#include "UnityEngine_UnityEngine_GUITextMethodDeclarations.h"
static const EncodedMethodIndex GUIText_t549_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUIText_t549_0_0_0;
extern const Il2CppType GUIText_t549_1_0_0;
extern const Il2CppType GUIElement_t861_0_0_0;
struct GUIText_t549;
const Il2CppTypeDefinitionMetadata GUIText_t549_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &GUIElement_t861_0_0_0/* parent */
	, GUIText_t549_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5480/* methodStart */
	, -1/* eventStart */
	, 1051/* propertyStart */

};
TypeInfo GUIText_t549_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIText"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUIText_t549_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUIText_t549_0_0_0/* byval_arg */
	, &GUIText_t549_1_0_0/* this_arg */
	, &GUIText_t549_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUIText_t549)/* instance_size */
	, sizeof (GUIText_t549)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextMesh
#include "UnityEngine_UnityEngine_TextMesh.h"
// Metadata Definition UnityEngine.TextMesh
extern TypeInfo TextMesh_t397_il2cpp_TypeInfo;
// UnityEngine.TextMesh
#include "UnityEngine_UnityEngine_TextMeshMethodDeclarations.h"
static const EncodedMethodIndex TextMesh_t397_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextMesh_t397_0_0_0;
extern const Il2CppType TextMesh_t397_1_0_0;
struct TextMesh_t397;
const Il2CppTypeDefinitionMetadata TextMesh_t397_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Component_t477_0_0_0/* parent */
	, TextMesh_t397_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5481/* methodStart */
	, -1/* eventStart */
	, 1052/* propertyStart */

};
TypeInfo TextMesh_t397_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextMesh"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &TextMesh_t397_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextMesh_t397_0_0_0/* byval_arg */
	, &TextMesh_t397_1_0_0/* this_arg */
	, &TextMesh_t397_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextMesh_t397)/* instance_size */
	, sizeof (TextMesh_t397)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CharacterInfo
#include "UnityEngine_UnityEngine_CharacterInfo.h"
// Metadata Definition UnityEngine.CharacterInfo
extern TypeInfo CharacterInfo_t955_il2cpp_TypeInfo;
// UnityEngine.CharacterInfo
#include "UnityEngine_UnityEngine_CharacterInfoMethodDeclarations.h"
static const EncodedMethodIndex CharacterInfo_t955_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CharacterInfo_t955_0_0_0;
extern const Il2CppType CharacterInfo_t955_1_0_0;
const Il2CppTypeDefinitionMetadata CharacterInfo_t955_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, CharacterInfo_t955_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4545/* fieldStart */
	, 5484/* methodStart */
	, -1/* eventStart */
	, 1054/* propertyStart */

};
TypeInfo CharacterInfo_t955_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CharacterInfo"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &CharacterInfo_t955_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CharacterInfo_t955_0_0_0/* byval_arg */
	, &CharacterInfo_t955_1_0_0/* this_arg */
	, &CharacterInfo_t955_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)CharacterInfo_t955_marshal/* marshal_to_native_func */
	, (methodPointerType)CharacterInfo_t955_marshal_back/* marshal_from_native_func */
	, (methodPointerType)CharacterInfo_t955_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (CharacterInfo_t955)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CharacterInfo_t955)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(CharacterInfo_t955_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 16/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Font
#include "UnityEngine_UnityEngine_Font.h"
// Metadata Definition UnityEngine.Font
extern TypeInfo Font_t643_il2cpp_TypeInfo;
// UnityEngine.Font
#include "UnityEngine_UnityEngine_FontMethodDeclarations.h"
extern const Il2CppType FontTextureRebuildCallback_t956_0_0_0;
static const Il2CppType* Font_t643_il2cpp_TypeInfo__nestedTypes[1] =
{
	&FontTextureRebuildCallback_t956_0_0_0,
};
static const EncodedMethodIndex Font_t643_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Font_t643_0_0_0;
extern const Il2CppType Font_t643_1_0_0;
struct Font_t643;
const Il2CppTypeDefinitionMetadata Font_t643_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Font_t643_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t473_0_0_0/* parent */
	, Font_t643_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4553/* fieldStart */
	, 5500/* methodStart */
	, 15/* eventStart */
	, 1070/* propertyStart */

};
TypeInfo Font_t643_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Font"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Font_t643_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Font_t643_0_0_0/* byval_arg */
	, &Font_t643_1_0_0/* this_arg */
	, &Font_t643_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Font_t643)/* instance_size */
	, sizeof (Font_t643)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Font_t643_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Font/FontTextureRebuildCallback
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallback.h"
// Metadata Definition UnityEngine.Font/FontTextureRebuildCallback
extern TypeInfo FontTextureRebuildCallback_t956_il2cpp_TypeInfo;
// UnityEngine.Font/FontTextureRebuildCallback
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallbackMethodDeclarations.h"
static const EncodedMethodIndex FontTextureRebuildCallback_t956_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1395,
	1396,
	1397,
};
static Il2CppInterfaceOffsetPair FontTextureRebuildCallback_t956_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType FontTextureRebuildCallback_t956_1_0_0;
struct FontTextureRebuildCallback_t956;
const Il2CppTypeDefinitionMetadata FontTextureRebuildCallback_t956_DefinitionMetadata = 
{
	&Font_t643_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FontTextureRebuildCallback_t956_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, FontTextureRebuildCallback_t956_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5507/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FontTextureRebuildCallback_t956_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FontTextureRebuildCallback"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &FontTextureRebuildCallback_t956_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2010/* custom_attributes_cache */
	, &FontTextureRebuildCallback_t956_0_0_0/* byval_arg */
	, &FontTextureRebuildCallback_t956_1_0_0/* this_arg */
	, &FontTextureRebuildCallback_t956_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_FontTextureRebuildCallback_t956/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FontTextureRebuildCallback_t956)/* instance_size */
	, sizeof (FontTextureRebuildCallback_t956)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
// Metadata Definition UnityEngine.UICharInfo
extern TypeInfo UICharInfo_t811_il2cpp_TypeInfo;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfoMethodDeclarations.h"
static const EncodedMethodIndex UICharInfo_t811_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UICharInfo_t811_0_0_0;
extern const Il2CppType UICharInfo_t811_1_0_0;
const Il2CppTypeDefinitionMetadata UICharInfo_t811_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, UICharInfo_t811_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4555/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UICharInfo_t811_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UICharInfo"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &UICharInfo_t811_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UICharInfo_t811_0_0_0/* byval_arg */
	, &UICharInfo_t811_1_0_0/* this_arg */
	, &UICharInfo_t811_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UICharInfo_t811)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UICharInfo_t811)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(UICharInfo_t811 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
// Metadata Definition UnityEngine.UILineInfo
extern TypeInfo UILineInfo_t809_il2cpp_TypeInfo;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfoMethodDeclarations.h"
static const EncodedMethodIndex UILineInfo_t809_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UILineInfo_t809_0_0_0;
extern const Il2CppType UILineInfo_t809_1_0_0;
const Il2CppTypeDefinitionMetadata UILineInfo_t809_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, UILineInfo_t809_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4557/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UILineInfo_t809_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UILineInfo"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &UILineInfo_t809_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UILineInfo_t809_0_0_0/* byval_arg */
	, &UILineInfo_t809_1_0_0/* this_arg */
	, &UILineInfo_t809_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UILineInfo_t809)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UILineInfo_t809)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(UILineInfo_t809 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextGenerator
#include "UnityEngine_UnityEngine_TextGenerator.h"
// Metadata Definition UnityEngine.TextGenerator
extern TypeInfo TextGenerator_t686_il2cpp_TypeInfo;
// UnityEngine.TextGenerator
#include "UnityEngine_UnityEngine_TextGeneratorMethodDeclarations.h"
static const EncodedMethodIndex TextGenerator_t686_VTable[5] = 
{
	626,
	1398,
	627,
	628,
	1399,
};
static const Il2CppType* TextGenerator_t686_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair TextGenerator_t686_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextGenerator_t686_0_0_0;
extern const Il2CppType TextGenerator_t686_1_0_0;
struct TextGenerator_t686;
const Il2CppTypeDefinitionMetadata TextGenerator_t686_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, TextGenerator_t686_InterfacesTypeInfos/* implementedInterfaces */
	, TextGenerator_t686_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TextGenerator_t686_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4559/* fieldStart */
	, 5511/* methodStart */
	, -1/* eventStart */
	, 1073/* propertyStart */

};
TypeInfo TextGenerator_t686_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextGenerator"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &TextGenerator_t686_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextGenerator_t686_0_0_0/* byval_arg */
	, &TextGenerator_t686_1_0_0/* this_arg */
	, &TextGenerator_t686_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextGenerator_t686)/* instance_size */
	, sizeof (TextGenerator_t686)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 34/* method_count */
	, 9/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderMode.h"
// Metadata Definition UnityEngine.RenderMode
extern TypeInfo RenderMode_t959_il2cpp_TypeInfo;
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderModeMethodDeclarations.h"
static const EncodedMethodIndex RenderMode_t959_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair RenderMode_t959_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderMode_t959_0_0_0;
extern const Il2CppType RenderMode_t959_1_0_0;
const Il2CppTypeDefinitionMetadata RenderMode_t959_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RenderMode_t959_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, RenderMode_t959_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4570/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RenderMode_t959_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderMode"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderMode_t959_0_0_0/* byval_arg */
	, &RenderMode_t959_1_0_0/* this_arg */
	, &RenderMode_t959_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderMode_t959)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderMode_t959)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
// Metadata Definition UnityEngine.Canvas
extern TypeInfo Canvas_t650_il2cpp_TypeInfo;
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_CanvasMethodDeclarations.h"
extern const Il2CppType WillRenderCanvases_t791_0_0_0;
static const Il2CppType* Canvas_t650_il2cpp_TypeInfo__nestedTypes[1] =
{
	&WillRenderCanvases_t791_0_0_0,
};
static const EncodedMethodIndex Canvas_t650_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Canvas_t650_0_0_0;
extern const Il2CppType Canvas_t650_1_0_0;
struct Canvas_t650;
const Il2CppTypeDefinitionMetadata Canvas_t650_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Canvas_t650_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t825_0_0_0/* parent */
	, Canvas_t650_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4574/* fieldStart */
	, 5545/* methodStart */
	, 16/* eventStart */
	, 1082/* propertyStart */

};
TypeInfo Canvas_t650_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Canvas"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Canvas_t650_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Canvas_t650_0_0_0/* byval_arg */
	, &Canvas_t650_1_0_0/* this_arg */
	, &Canvas_t650_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Canvas_t650)/* instance_size */
	, sizeof (Canvas_t650)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Canvas_t650_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 9/* property_count */
	, 1/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Canvas/WillRenderCanvases
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases.h"
// Metadata Definition UnityEngine.Canvas/WillRenderCanvases
extern TypeInfo WillRenderCanvases_t791_il2cpp_TypeInfo;
// UnityEngine.Canvas/WillRenderCanvases
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvasesMethodDeclarations.h"
static const EncodedMethodIndex WillRenderCanvases_t791_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1400,
	1401,
	1402,
};
static Il2CppInterfaceOffsetPair WillRenderCanvases_t791_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WillRenderCanvases_t791_1_0_0;
struct WillRenderCanvases_t791;
const Il2CppTypeDefinitionMetadata WillRenderCanvases_t791_DefinitionMetadata = 
{
	&Canvas_t650_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WillRenderCanvases_t791_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, WillRenderCanvases_t791_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5562/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WillRenderCanvases_t791_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WillRenderCanvases"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &WillRenderCanvases_t791_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WillRenderCanvases_t791_0_0_0/* byval_arg */
	, &WillRenderCanvases_t791_1_0_0/* this_arg */
	, &WillRenderCanvases_t791_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_WillRenderCanvases_t791/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WillRenderCanvases_t791)/* instance_size */
	, sizeof (WillRenderCanvases_t791)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.ICanvasRaycastFilter
extern TypeInfo ICanvasRaycastFilter_t798_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ICanvasRaycastFilter_t798_0_0_0;
extern const Il2CppType ICanvasRaycastFilter_t798_1_0_0;
struct ICanvasRaycastFilter_t798;
const Il2CppTypeDefinitionMetadata ICanvasRaycastFilter_t798_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5566/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ICanvasRaycastFilter_t798_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICanvasRaycastFilter"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &ICanvasRaycastFilter_t798_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ICanvasRaycastFilter_t798_0_0_0/* byval_arg */
	, &ICanvasRaycastFilter_t798_1_0_0/* this_arg */
	, &ICanvasRaycastFilter_t798_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CanvasGroup
#include "UnityEngine_UnityEngine_CanvasGroup.h"
// Metadata Definition UnityEngine.CanvasGroup
extern TypeInfo CanvasGroup_t797_il2cpp_TypeInfo;
// UnityEngine.CanvasGroup
#include "UnityEngine_UnityEngine_CanvasGroupMethodDeclarations.h"
static const EncodedMethodIndex CanvasGroup_t797_VTable[5] = 
{
	600,
	601,
	602,
	603,
	1403,
};
static const Il2CppType* CanvasGroup_t797_InterfacesTypeInfos[] = 
{
	&ICanvasRaycastFilter_t798_0_0_0,
};
static Il2CppInterfaceOffsetPair CanvasGroup_t797_InterfacesOffsets[] = 
{
	{ &ICanvasRaycastFilter_t798_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CanvasGroup_t797_0_0_0;
extern const Il2CppType CanvasGroup_t797_1_0_0;
struct CanvasGroup_t797;
const Il2CppTypeDefinitionMetadata CanvasGroup_t797_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CanvasGroup_t797_InterfacesTypeInfos/* implementedInterfaces */
	, CanvasGroup_t797_InterfacesOffsets/* interfaceOffsets */
	, &Component_t477_0_0_0/* parent */
	, CanvasGroup_t797_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5567/* methodStart */
	, -1/* eventStart */
	, 1091/* propertyStart */

};
TypeInfo CanvasGroup_t797_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasGroup"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &CanvasGroup_t797_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CanvasGroup_t797_0_0_0/* byval_arg */
	, &CanvasGroup_t797_1_0_0/* this_arg */
	, &CanvasGroup_t797_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasGroup_t797)/* instance_size */
	, sizeof (CanvasGroup_t797)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
// Metadata Definition UnityEngine.UIVertex
extern TypeInfo UIVertex_t685_il2cpp_TypeInfo;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertexMethodDeclarations.h"
static const EncodedMethodIndex UIVertex_t685_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UIVertex_t685_0_0_0;
extern const Il2CppType UIVertex_t685_1_0_0;
const Il2CppTypeDefinitionMetadata UIVertex_t685_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, UIVertex_t685_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4575/* fieldStart */
	, 5571/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UIVertex_t685_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UIVertex"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &UIVertex_t685_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UIVertex_t685_0_0_0/* byval_arg */
	, &UIVertex_t685_1_0_0/* this_arg */
	, &UIVertex_t685_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UIVertex_t685)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UIVertex_t685)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(UIVertex_t685 )/* native_size */
	, sizeof(UIVertex_t685_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRenderer.h"
// Metadata Definition UnityEngine.CanvasRenderer
extern TypeInfo CanvasRenderer_t649_il2cpp_TypeInfo;
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRendererMethodDeclarations.h"
static const EncodedMethodIndex CanvasRenderer_t649_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CanvasRenderer_t649_0_0_0;
extern const Il2CppType CanvasRenderer_t649_1_0_0;
struct CanvasRenderer_t649;
const Il2CppTypeDefinitionMetadata CanvasRenderer_t649_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Component_t477_0_0_0/* parent */
	, CanvasRenderer_t649_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5572/* methodStart */
	, -1/* eventStart */
	, 1094/* propertyStart */

};
TypeInfo CanvasRenderer_t649_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasRenderer"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &CanvasRenderer_t649_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CanvasRenderer_t649_0_0_0/* byval_arg */
	, &CanvasRenderer_t649_1_0_0/* this_arg */
	, &CanvasRenderer_t649_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasRenderer_t649)/* instance_size */
	, sizeof (CanvasRenderer_t649)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RectTransformUtility
#include "UnityEngine_UnityEngine_RectTransformUtility.h"
// Metadata Definition UnityEngine.RectTransformUtility
extern TypeInfo RectTransformUtility_t799_il2cpp_TypeInfo;
// UnityEngine.RectTransformUtility
#include "UnityEngine_UnityEngine_RectTransformUtilityMethodDeclarations.h"
static const EncodedMethodIndex RectTransformUtility_t799_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RectTransformUtility_t799_0_0_0;
extern const Il2CppType RectTransformUtility_t799_1_0_0;
struct RectTransformUtility_t799;
const Il2CppTypeDefinitionMetadata RectTransformUtility_t799_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RectTransformUtility_t799_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4584/* fieldStart */
	, 5583/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RectTransformUtility_t799_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RectTransformUtility"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &RectTransformUtility_t799_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RectTransformUtility_t799_0_0_0/* byval_arg */
	, &RectTransformUtility_t799_1_0_0/* this_arg */
	, &RectTransformUtility_t799_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RectTransformUtility_t799)/* instance_size */
	, sizeof (RectTransformUtility_t799)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RectTransformUtility_t799_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.Request
#include "UnityEngine_UnityEngine_Networking_Match_Request.h"
// Metadata Definition UnityEngine.Networking.Match.Request
extern TypeInfo Request_t960_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.Request
#include "UnityEngine_UnityEngine_Networking_Match_RequestMethodDeclarations.h"
static const EncodedMethodIndex Request_t960_VTable[4] = 
{
	626,
	601,
	627,
	1404,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Request_t960_0_0_0;
extern const Il2CppType Request_t960_1_0_0;
struct Request_t960;
const Il2CppTypeDefinitionMetadata Request_t960_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Request_t960_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4585/* fieldStart */
	, 5596/* methodStart */
	, -1/* eventStart */
	, 1096/* propertyStart */

};
TypeInfo Request_t960_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Request"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &Request_t960_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Request_t960_0_0_0/* byval_arg */
	, &Request_t960_1_0_0/* this_arg */
	, &Request_t960_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Request_t960)/* instance_size */
	, sizeof (Request_t960)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.ResponseBase
#include "UnityEngine_UnityEngine_Networking_Match_ResponseBase.h"
// Metadata Definition UnityEngine.Networking.Match.ResponseBase
extern TypeInfo ResponseBase_t961_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.ResponseBase
#include "UnityEngine_UnityEngine_Networking_Match_ResponseBaseMethodDeclarations.h"
extern const Il2CppType List_1_t3650_0_0_0;
extern const Il2CppType ResponseBase_ParseJSONList_m24198_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition ResponseBase_ParseJSONList_m24198_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4143 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5451 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5452 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4144 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5453 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
static const EncodedMethodIndex ResponseBase_t961_VTable[5] = 
{
	626,
	601,
	627,
	628,
	0,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ResponseBase_t961_0_0_0;
extern const Il2CppType ResponseBase_t961_1_0_0;
struct ResponseBase_t961;
const Il2CppTypeDefinitionMetadata ResponseBase_t961_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ResponseBase_t961_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5601/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ResponseBase_t961_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResponseBase"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &ResponseBase_t961_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ResponseBase_t961_0_0_0/* byval_arg */
	, &ResponseBase_t961_1_0_0/* this_arg */
	, &ResponseBase_t961_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResponseBase_t961)/* instance_size */
	, sizeof (ResponseBase_t961)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Networking.Match.IResponse
extern TypeInfo IResponse_t3400_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IResponse_t3400_0_0_0;
extern const Il2CppType IResponse_t3400_1_0_0;
struct IResponse_t3400;
const Il2CppTypeDefinitionMetadata IResponse_t3400_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IResponse_t3400_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IResponse"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &IResponse_t3400_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IResponse_t3400_0_0_0/* byval_arg */
	, &IResponse_t3400_1_0_0/* this_arg */
	, &IResponse_t3400_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.Response
#include "UnityEngine_UnityEngine_Networking_Match_Response.h"
// Metadata Definition UnityEngine.Networking.Match.Response
extern TypeInfo Response_t962_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.Response
#include "UnityEngine_UnityEngine_Networking_Match_ResponseMethodDeclarations.h"
static const EncodedMethodIndex Response_t962_VTable[5] = 
{
	626,
	601,
	627,
	1405,
	1406,
};
static const Il2CppType* Response_t962_InterfacesTypeInfos[] = 
{
	&IResponse_t3400_0_0_0,
};
static Il2CppInterfaceOffsetPair Response_t962_InterfacesOffsets[] = 
{
	{ &IResponse_t3400_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Response_t962_0_0_0;
extern const Il2CppType Response_t962_1_0_0;
struct Response_t962;
const Il2CppTypeDefinitionMetadata Response_t962_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Response_t962_InterfacesTypeInfos/* implementedInterfaces */
	, Response_t962_InterfacesOffsets/* interfaceOffsets */
	, &ResponseBase_t961_0_0_0/* parent */
	, Response_t962_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4589/* fieldStart */
	, 5609/* methodStart */
	, -1/* eventStart */
	, 1099/* propertyStart */

};
TypeInfo Response_t962_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Response"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &Response_t962_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Response_t962_0_0_0/* byval_arg */
	, &Response_t962_1_0_0/* this_arg */
	, &Response_t962_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Response_t962)/* instance_size */
	, sizeof (Response_t962)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Networking.Match.BasicResponse
#include "UnityEngine_UnityEngine_Networking_Match_BasicResponse.h"
// Metadata Definition UnityEngine.Networking.Match.BasicResponse
extern TypeInfo BasicResponse_t963_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.BasicResponse
#include "UnityEngine_UnityEngine_Networking_Match_BasicResponseMethodDeclarations.h"
static const EncodedMethodIndex BasicResponse_t963_VTable[5] = 
{
	626,
	601,
	627,
	1405,
	1406,
};
static Il2CppInterfaceOffsetPair BasicResponse_t963_InterfacesOffsets[] = 
{
	{ &IResponse_t3400_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType BasicResponse_t963_0_0_0;
extern const Il2CppType BasicResponse_t963_1_0_0;
struct BasicResponse_t963;
const Il2CppTypeDefinitionMetadata BasicResponse_t963_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BasicResponse_t963_InterfacesOffsets/* interfaceOffsets */
	, &Response_t962_0_0_0/* parent */
	, BasicResponse_t963_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5616/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BasicResponse_t963_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "BasicResponse"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &BasicResponse_t963_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BasicResponse_t963_0_0_0/* byval_arg */
	, &BasicResponse_t963_1_0_0/* this_arg */
	, &BasicResponse_t963_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BasicResponse_t963)/* instance_size */
	, sizeof (BasicResponse_t963)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Networking.Match.CreateMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchRequest.h"
// Metadata Definition UnityEngine.Networking.Match.CreateMatchRequest
extern TypeInfo CreateMatchRequest_t965_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.CreateMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchRequestMethodDeclarations.h"
static const EncodedMethodIndex CreateMatchRequest_t965_VTable[4] = 
{
	626,
	601,
	627,
	1407,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CreateMatchRequest_t965_0_0_0;
extern const Il2CppType CreateMatchRequest_t965_1_0_0;
struct CreateMatchRequest_t965;
const Il2CppTypeDefinitionMetadata CreateMatchRequest_t965_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Request_t960_0_0_0/* parent */
	, CreateMatchRequest_t965_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4591/* fieldStart */
	, 5617/* methodStart */
	, -1/* eventStart */
	, 1101/* propertyStart */

};
TypeInfo CreateMatchRequest_t965_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CreateMatchRequest"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &CreateMatchRequest_t965_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CreateMatchRequest_t965_0_0_0/* byval_arg */
	, &CreateMatchRequest_t965_1_0_0/* this_arg */
	, &CreateMatchRequest_t965_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CreateMatchRequest_t965)/* instance_size */
	, sizeof (CreateMatchRequest_t965)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 5/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
