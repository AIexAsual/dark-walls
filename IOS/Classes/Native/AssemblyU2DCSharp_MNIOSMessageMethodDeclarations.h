﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MNIOSMessage
struct MNIOSMessage_t289;
// System.String
struct String_t;

// System.Void MNIOSMessage::.ctor()
extern "C" void MNIOSMessage__ctor_m1784 (MNIOSMessage_t289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNIOSMessage MNIOSMessage::Create(System.String,System.String)
extern "C" MNIOSMessage_t289 * MNIOSMessage_Create_m1785 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNIOSMessage MNIOSMessage::Create(System.String,System.String,System.String)
extern "C" MNIOSMessage_t289 * MNIOSMessage_Create_m1786 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSMessage::init()
extern "C" void MNIOSMessage_init_m1787 (MNIOSMessage_t289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSMessage::onPopUpCallBack(System.String)
extern "C" void MNIOSMessage_onPopUpCallBack_m1788 (MNIOSMessage_t289 * __this, String_t* ___buttonIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
