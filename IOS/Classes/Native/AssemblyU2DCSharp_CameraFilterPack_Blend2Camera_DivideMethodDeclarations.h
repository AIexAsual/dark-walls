﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_Divide
struct CameraFilterPack_Blend2Camera_Divide_t24;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_Divide::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_Divide__ctor_m106 (CameraFilterPack_Blend2Camera_Divide_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Divide::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_Divide_get_material_m107 (CameraFilterPack_Blend2Camera_Divide_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Divide::Start()
extern "C" void CameraFilterPack_Blend2Camera_Divide_Start_m108 (CameraFilterPack_Blend2Camera_Divide_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Divide::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_Divide_OnRenderImage_m109 (CameraFilterPack_Blend2Camera_Divide_t24 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Divide::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_Divide_OnValidate_m110 (CameraFilterPack_Blend2Camera_Divide_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Divide::Update()
extern "C" void CameraFilterPack_Blend2Camera_Divide_Update_m111 (CameraFilterPack_Blend2Camera_Divide_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Divide::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_Divide_OnEnable_m112 (CameraFilterPack_Blend2Camera_Divide_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Divide::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_Divide_OnDisable_m113 (CameraFilterPack_Blend2Camera_Divide_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
