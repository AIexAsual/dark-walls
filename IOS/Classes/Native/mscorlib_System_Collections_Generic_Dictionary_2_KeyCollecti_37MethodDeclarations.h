﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>
struct Enumerator_t2899;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1463;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22620_gshared (Enumerator_t2899 * __this, Dictionary_2_t1463 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m22620(__this, ___host, method) (( void (*) (Enumerator_t2899 *, Dictionary_2_t1463 *, const MethodInfo*))Enumerator__ctor_m22620_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22621_gshared (Enumerator_t2899 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22621(__this, method) (( Object_t * (*) (Enumerator_t2899 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22621_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m22622_gshared (Enumerator_t2899 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22622(__this, method) (( void (*) (Enumerator_t2899 *, const MethodInfo*))Enumerator_Dispose_m22622_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22623_gshared (Enumerator_t2899 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22623(__this, method) (( bool (*) (Enumerator_t2899 *, const MethodInfo*))Enumerator_MoveNext_m22623_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m22624_gshared (Enumerator_t2899 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22624(__this, method) (( int32_t (*) (Enumerator_t2899 *, const MethodInfo*))Enumerator_get_Current_m22624_gshared)(__this, method)
