﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Light_Rainbow
struct  CameraFilterPack_Light_Rainbow_t155  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Light_Rainbow::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Light_Rainbow::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Light_Rainbow::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Light_Rainbow::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Light_Rainbow::Value
	float ___Value_6;
};
struct CameraFilterPack_Light_Rainbow_t155_StaticFields{
	// System.Single CameraFilterPack_Light_Rainbow::ChangeValue
	float ___ChangeValue_7;
};
