﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_Colors_Brightness
struct  CameraFilterPack_Colors_Brightness_t74  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Colors_Brightness::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Colors_Brightness::_Brightness
	float ____Brightness_3;
	// UnityEngine.Material CameraFilterPack_Colors_Brightness::SCMaterial
	Material_t2 * ___SCMaterial_4;
};
struct CameraFilterPack_Colors_Brightness_t74_StaticFields{
	// System.Single CameraFilterPack_Colors_Brightness::ChangeBrightness
	float ___ChangeBrightness_5;
};
