﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<iTween/EaseType>
struct DefaultComparer_t2453;
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<iTween/EaseType>::.ctor()
extern "C" void DefaultComparer__ctor_m16483_gshared (DefaultComparer_t2453 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m16483(__this, method) (( void (*) (DefaultComparer_t2453 *, const MethodInfo*))DefaultComparer__ctor_m16483_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<iTween/EaseType>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m16484_gshared (DefaultComparer_t2453 * __this, int32_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m16484(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2453 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m16484_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<iTween/EaseType>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m16485_gshared (DefaultComparer_t2453 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m16485(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2453 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m16485_gshared)(__this, ___x, ___y, method)
