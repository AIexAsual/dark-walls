﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle
struct Toggle_t721;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Toggle>
struct  Predicate_1_t723  : public MulticastDelegate_t219
{
};
