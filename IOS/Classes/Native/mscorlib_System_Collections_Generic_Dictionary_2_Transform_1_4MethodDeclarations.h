﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>,System.Collections.DictionaryEntry>
struct Transform_1_t2319;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t545;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
// System.Collections.Generic.Dictionary`2/Transform`1<iTweenEvent/TweenType,System.Object,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_2MethodDeclarations.h"
#define Transform_1__ctor_m14931(__this, ___object, ___method, method) (( void (*) (Transform_1_t2319 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m14904_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m14932(__this, ___key, ___value, method) (( DictionaryEntry_t552  (*) (Transform_1_t2319 *, int32_t, Dictionary_2_t545 *, const MethodInfo*))Transform_1_Invoke_m14905_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m14933(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2319 *, int32_t, Dictionary_2_t545 *, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m14906_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m14934(__this, ___result, method) (( DictionaryEntry_t552  (*) (Transform_1_t2319 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m14907_gshared)(__this, ___result, method)
