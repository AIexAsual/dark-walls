﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
struct KeyValuePair_2_t2652;
// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_t874;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11MethodDeclarations.h"
#define KeyValuePair_2__ctor_m19494(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2652 *, int32_t, LayoutCache_t874 *, const MethodInfo*))KeyValuePair_2__ctor_m17534_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Key()
#define KeyValuePair_2_get_Key_m19495(__this, method) (( int32_t (*) (KeyValuePair_2_t2652 *, const MethodInfo*))KeyValuePair_2_get_Key_m17535_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m19496(__this, ___value, method) (( void (*) (KeyValuePair_2_t2652 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m17536_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Value()
#define KeyValuePair_2_get_Value_m19497(__this, method) (( LayoutCache_t874 * (*) (KeyValuePair_2_t2652 *, const MethodInfo*))KeyValuePair_2_get_Value_m17537_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m19498(__this, ___value, method) (( void (*) (KeyValuePair_2_t2652 *, LayoutCache_t874 *, const MethodInfo*))KeyValuePair_2_set_Value_m17538_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::ToString()
#define KeyValuePair_2_ToString_m19499(__this, method) (( String_t* (*) (KeyValuePair_2_t2652 *, const MethodInfo*))KeyValuePair_2_ToString_m17539_gshared)(__this, method)
