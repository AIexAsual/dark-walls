﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_8bits
struct CameraFilterPack_FX_8bits_t120;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_8bits::.ctor()
extern "C" void CameraFilterPack_FX_8bits__ctor_m770 (CameraFilterPack_FX_8bits_t120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_8bits::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_8bits_get_material_m771 (CameraFilterPack_FX_8bits_t120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits::Start()
extern "C" void CameraFilterPack_FX_8bits_Start_m772 (CameraFilterPack_FX_8bits_t120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_8bits_OnRenderImage_m773 (CameraFilterPack_FX_8bits_t120 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits::OnValidate()
extern "C" void CameraFilterPack_FX_8bits_OnValidate_m774 (CameraFilterPack_FX_8bits_t120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits::Update()
extern "C" void CameraFilterPack_FX_8bits_Update_m775 (CameraFilterPack_FX_8bits_t120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits::OnDisable()
extern "C" void CameraFilterPack_FX_8bits_OnDisable_m776 (CameraFilterPack_FX_8bits_t120 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
