﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.Syntax.ExpressionCollection
struct ExpressionCollection_t1493;
// System.Text.RegularExpressions.Syntax.Expression
#include "System_System_Text_RegularExpressions_Syntax_Expression.h"
// System.Text.RegularExpressions.Syntax.CompositeExpression
struct  CompositeExpression_t1495  : public Expression_t1494
{
	// System.Text.RegularExpressions.Syntax.ExpressionCollection System.Text.RegularExpressions.Syntax.CompositeExpression::expressions
	ExpressionCollection_t1493 * ___expressions_0;
};
