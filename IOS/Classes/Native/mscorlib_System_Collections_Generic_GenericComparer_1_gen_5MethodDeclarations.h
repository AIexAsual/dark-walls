﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.Single>
struct GenericComparer_1_t2408;

// System.Void System.Collections.Generic.GenericComparer`1<System.Single>::.ctor()
extern "C" void GenericComparer_1__ctor_m15777_gshared (GenericComparer_1_t2408 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m15777(__this, method) (( void (*) (GenericComparer_1_t2408 *, const MethodInfo*))GenericComparer_1__ctor_m15777_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Single>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m15778_gshared (GenericComparer_1_t2408 * __this, float ___x, float ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m15778(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2408 *, float, float, const MethodInfo*))GenericComparer_1_Compare_m15778_gshared)(__this, ___x, ___y, method)
