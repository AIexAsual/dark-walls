﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// MNPopup
#include "AssemblyU2DCSharp_MNPopup.h"
// MNAndroidDialog
struct  MNAndroidDialog_t285  : public MNPopup_t278
{
	// System.String MNAndroidDialog::yes
	String_t* ___yes_6;
	// System.String MNAndroidDialog::no
	String_t* ___no_7;
};
