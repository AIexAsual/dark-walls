﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// MNDialogResult
#include "AssemblyU2DCSharp_MNDialogResult.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<MNDialogResult>
struct  Action_1_t277  : public MulticastDelegate_t219
{
};
