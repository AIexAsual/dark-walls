﻿#pragma once
#include <stdint.h>
// UnityEngine.Camera
struct Camera_t14;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Camera/CameraCallback
struct  CameraCallback_t914  : public MulticastDelegate_t219
{
};
