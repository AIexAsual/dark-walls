﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>
struct Collection_1_t2696;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1087;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t3181;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t810;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void Collection_1__ctor_m20270_gshared (Collection_1_t2696 * __this, const MethodInfo* method);
#define Collection_1__ctor_m20270(__this, method) (( void (*) (Collection_1_t2696 *, const MethodInfo*))Collection_1__ctor_m20270_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20271_gshared (Collection_1_t2696 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20271(__this, method) (( bool (*) (Collection_1_t2696 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20271_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m20272_gshared (Collection_1_t2696 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m20272(__this, ___array, ___index, method) (( void (*) (Collection_1_t2696 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m20272_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m20273_gshared (Collection_1_t2696 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m20273(__this, method) (( Object_t * (*) (Collection_1_t2696 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m20273_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m20274_gshared (Collection_1_t2696 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m20274(__this, ___value, method) (( int32_t (*) (Collection_1_t2696 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m20274_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m20275_gshared (Collection_1_t2696 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m20275(__this, ___value, method) (( bool (*) (Collection_1_t2696 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m20275_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m20276_gshared (Collection_1_t2696 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m20276(__this, ___value, method) (( int32_t (*) (Collection_1_t2696 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m20276_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m20277_gshared (Collection_1_t2696 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m20277(__this, ___index, ___value, method) (( void (*) (Collection_1_t2696 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m20277_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m20278_gshared (Collection_1_t2696 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m20278(__this, ___value, method) (( void (*) (Collection_1_t2696 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m20278_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m20279_gshared (Collection_1_t2696 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m20279(__this, method) (( bool (*) (Collection_1_t2696 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m20279_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m20280_gshared (Collection_1_t2696 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m20280(__this, method) (( Object_t * (*) (Collection_1_t2696 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m20280_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m20281_gshared (Collection_1_t2696 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m20281(__this, method) (( bool (*) (Collection_1_t2696 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m20281_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m20282_gshared (Collection_1_t2696 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m20282(__this, method) (( bool (*) (Collection_1_t2696 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m20282_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m20283_gshared (Collection_1_t2696 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m20283(__this, ___index, method) (( Object_t * (*) (Collection_1_t2696 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m20283_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m20284_gshared (Collection_1_t2696 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m20284(__this, ___index, ___value, method) (( void (*) (Collection_1_t2696 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m20284_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void Collection_1_Add_m20285_gshared (Collection_1_t2696 * __this, UILineInfo_t809  ___item, const MethodInfo* method);
#define Collection_1_Add_m20285(__this, ___item, method) (( void (*) (Collection_1_t2696 *, UILineInfo_t809 , const MethodInfo*))Collection_1_Add_m20285_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Clear()
extern "C" void Collection_1_Clear_m20286_gshared (Collection_1_t2696 * __this, const MethodInfo* method);
#define Collection_1_Clear_m20286(__this, method) (( void (*) (Collection_1_t2696 *, const MethodInfo*))Collection_1_Clear_m20286_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m20287_gshared (Collection_1_t2696 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m20287(__this, method) (( void (*) (Collection_1_t2696 *, const MethodInfo*))Collection_1_ClearItems_m20287_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m20288_gshared (Collection_1_t2696 * __this, UILineInfo_t809  ___item, const MethodInfo* method);
#define Collection_1_Contains_m20288(__this, ___item, method) (( bool (*) (Collection_1_t2696 *, UILineInfo_t809 , const MethodInfo*))Collection_1_Contains_m20288_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m20289_gshared (Collection_1_t2696 * __this, UILineInfoU5BU5D_t1087* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m20289(__this, ___array, ___index, method) (( void (*) (Collection_1_t2696 *, UILineInfoU5BU5D_t1087*, int32_t, const MethodInfo*))Collection_1_CopyTo_m20289_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m20290_gshared (Collection_1_t2696 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m20290(__this, method) (( Object_t* (*) (Collection_1_t2696 *, const MethodInfo*))Collection_1_GetEnumerator_m20290_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m20291_gshared (Collection_1_t2696 * __this, UILineInfo_t809  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m20291(__this, ___item, method) (( int32_t (*) (Collection_1_t2696 *, UILineInfo_t809 , const MethodInfo*))Collection_1_IndexOf_m20291_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m20292_gshared (Collection_1_t2696 * __this, int32_t ___index, UILineInfo_t809  ___item, const MethodInfo* method);
#define Collection_1_Insert_m20292(__this, ___index, ___item, method) (( void (*) (Collection_1_t2696 *, int32_t, UILineInfo_t809 , const MethodInfo*))Collection_1_Insert_m20292_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m20293_gshared (Collection_1_t2696 * __this, int32_t ___index, UILineInfo_t809  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m20293(__this, ___index, ___item, method) (( void (*) (Collection_1_t2696 *, int32_t, UILineInfo_t809 , const MethodInfo*))Collection_1_InsertItem_m20293_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m20294_gshared (Collection_1_t2696 * __this, UILineInfo_t809  ___item, const MethodInfo* method);
#define Collection_1_Remove_m20294(__this, ___item, method) (( bool (*) (Collection_1_t2696 *, UILineInfo_t809 , const MethodInfo*))Collection_1_Remove_m20294_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m20295_gshared (Collection_1_t2696 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m20295(__this, ___index, method) (( void (*) (Collection_1_t2696 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m20295_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m20296_gshared (Collection_1_t2696 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m20296(__this, ___index, method) (( void (*) (Collection_1_t2696 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m20296_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m20297_gshared (Collection_1_t2696 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m20297(__this, method) (( int32_t (*) (Collection_1_t2696 *, const MethodInfo*))Collection_1_get_Count_m20297_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t809  Collection_1_get_Item_m20298_gshared (Collection_1_t2696 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m20298(__this, ___index, method) (( UILineInfo_t809  (*) (Collection_1_t2696 *, int32_t, const MethodInfo*))Collection_1_get_Item_m20298_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m20299_gshared (Collection_1_t2696 * __this, int32_t ___index, UILineInfo_t809  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m20299(__this, ___index, ___value, method) (( void (*) (Collection_1_t2696 *, int32_t, UILineInfo_t809 , const MethodInfo*))Collection_1_set_Item_m20299_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m20300_gshared (Collection_1_t2696 * __this, int32_t ___index, UILineInfo_t809  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m20300(__this, ___index, ___item, method) (( void (*) (Collection_1_t2696 *, int32_t, UILineInfo_t809 , const MethodInfo*))Collection_1_SetItem_m20300_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m20301_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m20301(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m20301_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ConvertItem(System.Object)
extern "C" UILineInfo_t809  Collection_1_ConvertItem_m20302_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m20302(__this /* static, unused */, ___item, method) (( UILineInfo_t809  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m20302_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m20303_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m20303(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m20303_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m20304_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m20304(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m20304_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m20305_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m20305(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m20305_gshared)(__this /* static, unused */, ___list, method)
