﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.FormatterData
struct FormatterData_t1915;

// System.Void System.Runtime.Remoting.FormatterData::.ctor()
extern "C" void FormatterData__ctor_m11611 (FormatterData_t1915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
