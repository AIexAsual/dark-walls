﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t545;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"
// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct  KeyValuePair_2_t2343 
{
	// TKey System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::value
	Dictionary_2_t545 * ___value_1;
};
