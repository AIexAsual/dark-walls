﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// TestEventManager
struct TestEventManager_t388;
// TestEventManager/OnStateChangeHandler
struct OnStateChangeHandler_t387;
// testClass
#include "AssemblyU2DCSharp_testClass.h"

// System.Void TestEventManager::.ctor()
extern "C" void TestEventManager__ctor_m2208 (TestEventManager_t388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestEventManager::.cctor()
extern "C" void TestEventManager__cctor_m2209 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestEventManager::add_OnStateChangeEvent(TestEventManager/OnStateChangeHandler)
extern "C" void TestEventManager_add_OnStateChangeEvent_m2210 (TestEventManager_t388 * __this, OnStateChangeHandler_t387 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestEventManager::remove_OnStateChangeEvent(TestEventManager/OnStateChangeHandler)
extern "C" void TestEventManager_remove_OnStateChangeEvent_m2211 (TestEventManager_t388 * __this, OnStateChangeHandler_t387 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TestEventManager TestEventManager::get_Instance()
extern "C" TestEventManager_t388 * TestEventManager_get_Instance_m2212 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestEventManager::dispatchEvent(testClass)
extern "C" void TestEventManager_dispatchEvent_m2213 (TestEventManager_t388 * __this, int32_t ___stateEvent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestEventManager::Start()
extern "C" void TestEventManager_Start_m2214 (TestEventManager_t388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestEventManager::Update()
extern "C" void TestEventManager_Update_m2215 (TestEventManager_t388 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
