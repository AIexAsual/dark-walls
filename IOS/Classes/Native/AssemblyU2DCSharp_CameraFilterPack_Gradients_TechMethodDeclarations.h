﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Gradients_Tech
struct CameraFilterPack_Gradients_Tech_t153;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Gradients_Tech::.ctor()
extern "C" void CameraFilterPack_Gradients_Tech__ctor_m989 (CameraFilterPack_Gradients_Tech_t153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_Tech::get_material()
extern "C" Material_t2 * CameraFilterPack_Gradients_Tech_get_material_m990 (CameraFilterPack_Gradients_Tech_t153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Tech::Start()
extern "C" void CameraFilterPack_Gradients_Tech_Start_m991 (CameraFilterPack_Gradients_Tech_t153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Tech::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Gradients_Tech_OnRenderImage_m992 (CameraFilterPack_Gradients_Tech_t153 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Tech::Update()
extern "C" void CameraFilterPack_Gradients_Tech_Update_m993 (CameraFilterPack_Gradients_Tech_t153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Tech::OnDisable()
extern "C" void CameraFilterPack_Gradients_Tech_OnDisable_m994 (CameraFilterPack_Gradients_Tech_t153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
