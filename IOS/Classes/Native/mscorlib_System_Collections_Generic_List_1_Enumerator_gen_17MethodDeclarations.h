﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<iTween/LoopType>
struct Enumerator_t2458;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<iTween/LoopType>
struct List_1_t581;
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"

// System.Void System.Collections.Generic.List`1/Enumerator<iTween/LoopType>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m16548_gshared (Enumerator_t2458 * __this, List_1_t581 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m16548(__this, ___l, method) (( void (*) (Enumerator_t2458 *, List_1_t581 *, const MethodInfo*))Enumerator__ctor_m16548_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<iTween/LoopType>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16549_gshared (Enumerator_t2458 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16549(__this, method) (( Object_t * (*) (Enumerator_t2458 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16549_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<iTween/LoopType>::Dispose()
extern "C" void Enumerator_Dispose_m16550_gshared (Enumerator_t2458 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16550(__this, method) (( void (*) (Enumerator_t2458 *, const MethodInfo*))Enumerator_Dispose_m16550_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<iTween/LoopType>::VerifyState()
extern "C" void Enumerator_VerifyState_m16551_gshared (Enumerator_t2458 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m16551(__this, method) (( void (*) (Enumerator_t2458 *, const MethodInfo*))Enumerator_VerifyState_m16551_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<iTween/LoopType>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16552_gshared (Enumerator_t2458 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16552(__this, method) (( bool (*) (Enumerator_t2458 *, const MethodInfo*))Enumerator_MoveNext_m16552_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<iTween/LoopType>::get_Current()
extern "C" int32_t Enumerator_get_Current_m16553_gshared (Enumerator_t2458 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16553(__this, method) (( int32_t (*) (Enumerator_t2458 *, const MethodInfo*))Enumerator_get_Current_m16553_gshared)(__this, method)
