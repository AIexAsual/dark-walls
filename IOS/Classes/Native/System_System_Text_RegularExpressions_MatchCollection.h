﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.Match
struct Match_t1371;
// System.Collections.ArrayList
struct ArrayList_t436;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.RegularExpressions.MatchCollection
struct  MatchCollection_t1370  : public Object_t
{
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.MatchCollection::current
	Match_t1371 * ___current_0;
	// System.Collections.ArrayList System.Text.RegularExpressions.MatchCollection::list
	ArrayList_t436 * ___list_1;
};
