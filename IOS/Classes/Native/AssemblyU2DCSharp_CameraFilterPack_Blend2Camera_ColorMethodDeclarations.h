﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_Color
struct CameraFilterPack_Blend2Camera_Color_t18;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_Color::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_Color__ctor_m58 (CameraFilterPack_Blend2Camera_Color_t18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Color::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_Color_get_material_m59 (CameraFilterPack_Blend2Camera_Color_t18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Color::Start()
extern "C" void CameraFilterPack_Blend2Camera_Color_Start_m60 (CameraFilterPack_Blend2Camera_Color_t18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Color::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_Color_OnRenderImage_m61 (CameraFilterPack_Blend2Camera_Color_t18 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Color::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_Color_OnValidate_m62 (CameraFilterPack_Blend2Camera_Color_t18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Color::Update()
extern "C" void CameraFilterPack_Blend2Camera_Color_Update_m63 (CameraFilterPack_Blend2Camera_Color_t18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Color::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_Color_OnEnable_m64 (CameraFilterPack_Blend2Camera_Color_t18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Color::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_Color_OnDisable_m65 (CameraFilterPack_Blend2Camera_Color_t18 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
