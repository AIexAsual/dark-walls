﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>
struct ReadOnlyCollection_1_t2459;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<iTween/LoopType>
struct IList_1_t570;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// iTween/LoopType[]
struct LoopTypeU5BU5D_t452;
// System.Collections.Generic.IEnumerator`1<iTween/LoopType>
struct IEnumerator_1_t3043;
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m16554_gshared (ReadOnlyCollection_1_t2459 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m16554(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2459 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m16554_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m16555_gshared (ReadOnlyCollection_1_t2459 * __this, int32_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m16555(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2459 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m16555_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m16556_gshared (ReadOnlyCollection_1_t2459 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m16556(__this, method) (( void (*) (ReadOnlyCollection_1_t2459 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m16556_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m16557_gshared (ReadOnlyCollection_1_t2459 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m16557(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2459 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m16557_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m16558_gshared (ReadOnlyCollection_1_t2459 * __this, int32_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m16558(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2459 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m16558_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m16559_gshared (ReadOnlyCollection_1_t2459 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m16559(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2459 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m16559_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m16560_gshared (ReadOnlyCollection_1_t2459 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m16560(__this, ___index, method) (( int32_t (*) (ReadOnlyCollection_1_t2459 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m16560_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m16561_gshared (ReadOnlyCollection_1_t2459 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m16561(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2459 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m16561_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16562_gshared (ReadOnlyCollection_1_t2459 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16562(__this, method) (( bool (*) (ReadOnlyCollection_1_t2459 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16562_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m16563_gshared (ReadOnlyCollection_1_t2459 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m16563(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2459 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m16563_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m16564_gshared (ReadOnlyCollection_1_t2459 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m16564(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2459 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m16564_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m16565_gshared (ReadOnlyCollection_1_t2459 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m16565(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2459 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m16565_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m16566_gshared (ReadOnlyCollection_1_t2459 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m16566(__this, method) (( void (*) (ReadOnlyCollection_1_t2459 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m16566_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m16567_gshared (ReadOnlyCollection_1_t2459 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m16567(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2459 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m16567_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m16568_gshared (ReadOnlyCollection_1_t2459 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m16568(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2459 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m16568_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m16569_gshared (ReadOnlyCollection_1_t2459 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m16569(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2459 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m16569_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m16570_gshared (ReadOnlyCollection_1_t2459 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m16570(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2459 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m16570_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m16571_gshared (ReadOnlyCollection_1_t2459 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m16571(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2459 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m16571_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m16572_gshared (ReadOnlyCollection_1_t2459 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m16572(__this, method) (( bool (*) (ReadOnlyCollection_1_t2459 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m16572_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m16573_gshared (ReadOnlyCollection_1_t2459 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m16573(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2459 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m16573_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m16574_gshared (ReadOnlyCollection_1_t2459 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m16574(__this, method) (( bool (*) (ReadOnlyCollection_1_t2459 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m16574_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m16575_gshared (ReadOnlyCollection_1_t2459 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m16575(__this, method) (( bool (*) (ReadOnlyCollection_1_t2459 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m16575_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m16576_gshared (ReadOnlyCollection_1_t2459 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m16576(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2459 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m16576_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m16577_gshared (ReadOnlyCollection_1_t2459 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m16577(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2459 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m16577_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m16578_gshared (ReadOnlyCollection_1_t2459 * __this, int32_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m16578(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2459 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m16578_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m16579_gshared (ReadOnlyCollection_1_t2459 * __this, LoopTypeU5BU5D_t452* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m16579(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2459 *, LoopTypeU5BU5D_t452*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m16579_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m16580_gshared (ReadOnlyCollection_1_t2459 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m16580(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2459 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m16580_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m16581_gshared (ReadOnlyCollection_1_t2459 * __this, int32_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m16581(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2459 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m16581_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m16582_gshared (ReadOnlyCollection_1_t2459 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m16582(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2459 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m16582_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::get_Item(System.Int32)
extern "C" int32_t ReadOnlyCollection_1_get_Item_m16583_gshared (ReadOnlyCollection_1_t2459 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m16583(__this, ___index, method) (( int32_t (*) (ReadOnlyCollection_1_t2459 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m16583_gshared)(__this, ___index, method)
