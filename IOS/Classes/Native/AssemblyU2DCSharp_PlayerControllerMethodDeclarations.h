﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PlayerController
struct PlayerController_t356;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.Collider
struct Collider_t319;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void PlayerController::.ctor()
extern "C" void PlayerController__ctor_m2272 (PlayerController_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::.cctor()
extern "C" void PlayerController__cctor_m2273 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayerController PlayerController::get_Instance()
extern "C" PlayerController_t356 * PlayerController_get_Instance_m2274 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::Start()
extern "C" void PlayerController_Start_m2275 (PlayerController_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::Update()
extern "C" void PlayerController_Update_m2276 (PlayerController_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::goTo()
extern "C" void PlayerController_goTo_m2277 (PlayerController_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::StartPos(System.Single)
extern "C" void PlayerController_StartPos_m2278 (PlayerController_t356 * __this, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerController::WaitAndStartPos(System.Single)
extern "C" Object_t * PlayerController_WaitAndStartPos_m2279 (PlayerController_t356 * __this, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::MoveTo(System.Single,UnityEngine.Vector3,System.Boolean)
extern "C" void PlayerController_MoveTo_m2280 (PlayerController_t356 * __this, float ___delay, Vector3_t215  ___val, bool ___isReady, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerController::WaitAndMoveTo(System.Single,UnityEngine.Vector3,System.Boolean)
extern "C" Object_t * PlayerController_WaitAndMoveTo_m2281 (PlayerController_t356 * __this, float ___delay, Vector3_t215  ___val, bool ___isReady, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerController::StandUp(System.Single)
extern "C" Object_t * PlayerController_StandUp_m2282 (PlayerController_t356 * __this, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::checkDestination()
extern "C" void PlayerController_checkDestination_m2283 (PlayerController_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::GetFlashLight(System.Single,System.Single)
extern "C" void PlayerController_GetFlashLight_m2284 (PlayerController_t356 * __this, float ___time, float ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerController::WaitAndGetFlashLight(System.Single,System.Single)
extern "C" Object_t * PlayerController_WaitAndGetFlashLight_m2285 (PlayerController_t356 * __this, float ___time, float ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::OffFlashLight()
extern "C" void PlayerController_OffFlashLight_m2286 (PlayerController_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::flashLightFlicker(System.Boolean)
extern "C" void PlayerController_flashLightFlicker_m2287 (PlayerController_t356 * __this, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::ChangeDialog(System.Int32)
extern "C" void PlayerController_ChangeDialog_m2288 (PlayerController_t356 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::Unload()
extern "C" void PlayerController_Unload_m2289 (PlayerController_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::ReadToGO(System.Boolean)
extern "C" void PlayerController_ReadToGO_m2290 (PlayerController_t356 * __this, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::DeadState()
extern "C" void PlayerController_DeadState_m2291 (PlayerController_t356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::MyMonologue(System.Single,System.Int32)
extern "C" void PlayerController_MyMonologue_m2292 (PlayerController_t356 * __this, float ___time, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerController::WaitAndTalk(System.Single,System.Int32)
extern "C" Object_t * PlayerController_WaitAndTalk_m2293 (PlayerController_t356 * __this, float ___time, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerController::WaitAndSpace(System.Single)
extern "C" Object_t * PlayerController_WaitAndSpace_m2294 (PlayerController_t356 * __this, float ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerController::WaitAndRestartApp(System.Single)
extern "C" Object_t * PlayerController_WaitAndRestartApp_m2295 (PlayerController_t356 * __this, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::CallRestart(System.Single)
extern "C" void PlayerController_CallRestart_m2296 (PlayerController_t356 * __this, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::OnTriggerEnter(UnityEngine.Collider)
extern "C" void PlayerController_OnTriggerEnter_m2297 (PlayerController_t356 * __this, Collider_t319 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
