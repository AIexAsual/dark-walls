﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CardboardProfile/Distortion
struct Distortion_t251;

// System.Single CardboardProfile/Distortion::distort(System.Single)
extern "C" float Distortion_distort_m1555 (Distortion_t251 * __this, float ___r, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CardboardProfile/Distortion::distortInv(System.Single)
extern "C" float Distortion_distortInv_m1556 (Distortion_t251 * __this, float ___radius, const MethodInfo* method) IL2CPP_METHOD_ATTR;
