﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<iTween/LoopType>
struct Comparison_1_t2466;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"

// System.Void System.Comparison`1<iTween/LoopType>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m16638_gshared (Comparison_1_t2466 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m16638(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2466 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m16638_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<iTween/LoopType>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m16639_gshared (Comparison_1_t2466 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m16639(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2466 *, int32_t, int32_t, const MethodInfo*))Comparison_1_Invoke_m16639_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<iTween/LoopType>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m16640_gshared (Comparison_1_t2466 * __this, int32_t ___x, int32_t ___y, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m16640(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2466 *, int32_t, int32_t, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m16640_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<iTween/LoopType>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m16641_gshared (Comparison_1_t2466 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m16641(__this, ___result, method) (( int32_t (*) (Comparison_1_t2466 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m16641_gshared)(__this, ___result, method)
