﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MNWP8Dialog
struct MNWP8Dialog_t292;
// System.String
struct String_t;

// System.Void MNWP8Dialog::.ctor()
extern "C" void MNWP8Dialog__ctor_m1812 (MNWP8Dialog_t292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNWP8Dialog MNWP8Dialog::Create(System.String,System.String)
extern "C" MNWP8Dialog_t292 * MNWP8Dialog_Create_m1813 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNWP8Dialog::init()
extern "C" void MNWP8Dialog_init_m1814 (MNWP8Dialog_t292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNWP8Dialog::OnOkDel()
extern "C" void MNWP8Dialog_OnOkDel_m1815 (MNWP8Dialog_t292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNWP8Dialog::OnCancelDel()
extern "C" void MNWP8Dialog_OnCancelDel_m1816 (MNWP8Dialog_t292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
