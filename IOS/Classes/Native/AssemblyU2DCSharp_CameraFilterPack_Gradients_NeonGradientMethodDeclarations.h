﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Gradients_NeonGradient
struct CameraFilterPack_Gradients_NeonGradient_t150;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Gradients_NeonGradient::.ctor()
extern "C" void CameraFilterPack_Gradients_NeonGradient__ctor_m971 (CameraFilterPack_Gradients_NeonGradient_t150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_NeonGradient::get_material()
extern "C" Material_t2 * CameraFilterPack_Gradients_NeonGradient_get_material_m972 (CameraFilterPack_Gradients_NeonGradient_t150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_NeonGradient::Start()
extern "C" void CameraFilterPack_Gradients_NeonGradient_Start_m973 (CameraFilterPack_Gradients_NeonGradient_t150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_NeonGradient::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Gradients_NeonGradient_OnRenderImage_m974 (CameraFilterPack_Gradients_NeonGradient_t150 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_NeonGradient::Update()
extern "C" void CameraFilterPack_Gradients_NeonGradient_Update_m975 (CameraFilterPack_Gradients_NeonGradient_t150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_NeonGradient::OnDisable()
extern "C" void CameraFilterPack_Gradients_NeonGradient_OnDisable_m976 (CameraFilterPack_Gradients_NeonGradient_t150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
