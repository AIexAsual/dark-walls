﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$124
struct U24ArrayTypeU24124_t2164;
struct U24ArrayTypeU24124_t2164_marshaled;

void U24ArrayTypeU24124_t2164_marshal(const U24ArrayTypeU24124_t2164& unmarshaled, U24ArrayTypeU24124_t2164_marshaled& marshaled);
void U24ArrayTypeU24124_t2164_marshal_back(const U24ArrayTypeU24124_t2164_marshaled& marshaled, U24ArrayTypeU24124_t2164& unmarshaled);
void U24ArrayTypeU24124_t2164_marshal_cleanup(U24ArrayTypeU24124_t2164_marshaled& marshaled);
