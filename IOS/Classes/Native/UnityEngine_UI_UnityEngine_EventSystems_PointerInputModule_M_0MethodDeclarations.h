﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
struct MouseButtonEventData_t615;

// System.Void UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::.ctor()
extern "C" void MouseButtonEventData__ctor_m3570 (MouseButtonEventData_t615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::PressedThisFrame()
extern "C" bool MouseButtonEventData_PressedThisFrame_m3571 (MouseButtonEventData_t615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::ReleasedThisFrame()
extern "C" bool MouseButtonEventData_ReleasedThisFrame_m3572 (MouseButtonEventData_t615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
