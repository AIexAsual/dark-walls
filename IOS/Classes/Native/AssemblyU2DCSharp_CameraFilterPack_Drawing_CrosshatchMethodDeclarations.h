﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Drawing_Crosshatch
struct CameraFilterPack_Drawing_Crosshatch_t98;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Drawing_Crosshatch::.ctor()
extern "C" void CameraFilterPack_Drawing_Crosshatch__ctor_m623 (CameraFilterPack_Drawing_Crosshatch_t98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Crosshatch::get_material()
extern "C" Material_t2 * CameraFilterPack_Drawing_Crosshatch_get_material_m624 (CameraFilterPack_Drawing_Crosshatch_t98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Crosshatch::Start()
extern "C" void CameraFilterPack_Drawing_Crosshatch_Start_m625 (CameraFilterPack_Drawing_Crosshatch_t98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Crosshatch::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Drawing_Crosshatch_OnRenderImage_m626 (CameraFilterPack_Drawing_Crosshatch_t98 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Crosshatch::OnValidate()
extern "C" void CameraFilterPack_Drawing_Crosshatch_OnValidate_m627 (CameraFilterPack_Drawing_Crosshatch_t98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Crosshatch::Update()
extern "C" void CameraFilterPack_Drawing_Crosshatch_Update_m628 (CameraFilterPack_Drawing_Crosshatch_t98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Crosshatch::OnDisable()
extern "C" void CameraFilterPack_Drawing_Crosshatch_OnDisable_m629 (CameraFilterPack_Drawing_Crosshatch_t98 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
