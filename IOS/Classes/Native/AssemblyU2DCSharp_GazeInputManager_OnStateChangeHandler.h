﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// GazeInputEvent
#include "AssemblyU2DCSharp_GazeInputEvent.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// GazeInputManager/OnStateChangeHandler
struct  OnStateChangeHandler_t358  : public MulticastDelegate_t219
{
};
