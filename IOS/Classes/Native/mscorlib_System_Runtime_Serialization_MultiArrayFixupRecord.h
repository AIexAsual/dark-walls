﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t269;
// System.Runtime.Serialization.BaseFixupRecord
#include "mscorlib_System_Runtime_Serialization_BaseFixupRecord.h"
// System.Runtime.Serialization.MultiArrayFixupRecord
struct  MultiArrayFixupRecord_t1948  : public BaseFixupRecord_t1946
{
	// System.Int32[] System.Runtime.Serialization.MultiArrayFixupRecord::_indices
	Int32U5BU5D_t269* ____indices_4;
};
