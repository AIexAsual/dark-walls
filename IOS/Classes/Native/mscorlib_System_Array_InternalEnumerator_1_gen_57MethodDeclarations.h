﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>>
struct InternalEnumerator_1_t2744;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m20864_gshared (InternalEnumerator_1_t2744 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m20864(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2744 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m20864_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20865_gshared (InternalEnumerator_1_t2744 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20865(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2744 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20865_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m20866_gshared (InternalEnumerator_1_t2744 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m20866(__this, method) (( void (*) (InternalEnumerator_1_t2744 *, const MethodInfo*))InternalEnumerator_1_Dispose_m20866_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m20867_gshared (InternalEnumerator_1_t2744 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m20867(__this, method) (( bool (*) (InternalEnumerator_1_t2744 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m20867_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t2743  InternalEnumerator_1_get_Current_m20868_gshared (InternalEnumerator_1_t2744 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m20868(__this, method) (( KeyValuePair_2_t2743  (*) (InternalEnumerator_1_t2744 *, const MethodInfo*))InternalEnumerator_1_get_Current_m20868_gshared)(__this, method)
