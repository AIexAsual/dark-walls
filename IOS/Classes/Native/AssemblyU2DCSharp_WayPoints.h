﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// WayPoints
struct  WayPoints_t364  : public Object_t
{
};
struct WayPoints_t364_StaticFields{
	// System.String WayPoints::MAMAWAY_POINT
	String_t* ___MAMAWAY_POINT_0;
	// System.String WayPoints::MAMA
	String_t* ___MAMA_1;
	// System.String WayPoints::PLAYER
	String_t* ___PLAYER_2;
};
