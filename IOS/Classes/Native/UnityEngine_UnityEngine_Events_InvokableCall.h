﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction
struct UnityAction_t651;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall
struct  InvokableCall_t1062  : public BaseInvokableCall_t1061
{
	// UnityEngine.Events.UnityAction UnityEngine.Events.InvokableCall::Delegate
	UnityAction_t651 * ___Delegate_0;
};
