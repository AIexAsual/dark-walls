﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct KeyValuePair_2_t2762;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m21045_gshared (KeyValuePair_2_t2762 * __this, Object_t * ___key, KeyValuePair_2_t2350  ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m21045(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2762 *, Object_t *, KeyValuePair_2_t2350 , const MethodInfo*))KeyValuePair_2__ctor_m21045_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m21046_gshared (KeyValuePair_2_t2762 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m21046(__this, method) (( Object_t * (*) (KeyValuePair_2_t2762 *, const MethodInfo*))KeyValuePair_2_get_Key_m21046_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m21047_gshared (KeyValuePair_2_t2762 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m21047(__this, ___value, method) (( void (*) (KeyValuePair_2_t2762 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m21047_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Value()
extern "C" KeyValuePair_2_t2350  KeyValuePair_2_get_Value_m21048_gshared (KeyValuePair_2_t2762 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m21048(__this, method) (( KeyValuePair_2_t2350  (*) (KeyValuePair_2_t2762 *, const MethodInfo*))KeyValuePair_2_get_Value_m21048_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m21049_gshared (KeyValuePair_2_t2762 * __this, KeyValuePair_2_t2350  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m21049(__this, ___value, method) (( void (*) (KeyValuePair_2_t2762 *, KeyValuePair_2_t2350 , const MethodInfo*))KeyValuePair_2_set_Value_m21049_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m21050_gshared (KeyValuePair_2_t2762 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m21050(__this, method) (( String_t* (*) (KeyValuePair_2_t2762 *, const MethodInfo*))KeyValuePair_2_ToString_m21050_gshared)(__this, method)
