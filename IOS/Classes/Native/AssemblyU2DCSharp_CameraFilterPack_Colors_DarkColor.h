﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Colors_DarkColor
struct  CameraFilterPack_Colors_DarkColor_t75  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Colors_DarkColor::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Colors_DarkColor::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Colors_DarkColor::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Colors_DarkColor::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Colors_DarkColor::Alpha
	float ___Alpha_6;
	// System.Single CameraFilterPack_Colors_DarkColor::Colors
	float ___Colors_7;
	// System.Single CameraFilterPack_Colors_DarkColor::Green_Mod
	float ___Green_Mod_8;
	// System.Single CameraFilterPack_Colors_DarkColor::Value4
	float ___Value4_9;
};
struct CameraFilterPack_Colors_DarkColor_t75_StaticFields{
	// System.Single CameraFilterPack_Colors_DarkColor::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Colors_DarkColor::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Colors_DarkColor::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Colors_DarkColor::ChangeValue4
	float ___ChangeValue4_13;
};
