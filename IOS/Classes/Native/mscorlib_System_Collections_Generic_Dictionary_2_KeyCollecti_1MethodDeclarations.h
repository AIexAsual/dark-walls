﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>
struct KeyCollection_t2331;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>
struct Dictionary_2_t2327;
// System.Collections.Generic.IEnumerator`1<iTweenEvent/TweenType>
struct IEnumerator_1_t3002;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// iTweenEvent/TweenType[]
struct TweenTypeU5BU5D_t2316;
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<iTweenEvent/TweenType,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_2.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void KeyCollection__ctor_m14846_gshared (KeyCollection_t2331 * __this, Dictionary_2_t2327 * ___dictionary, const MethodInfo* method);
#define KeyCollection__ctor_m14846(__this, ___dictionary, method) (( void (*) (KeyCollection_t2331 *, Dictionary_2_t2327 *, const MethodInfo*))KeyCollection__ctor_m14846_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m14847_gshared (KeyCollection_t2331 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m14847(__this, ___item, method) (( void (*) (KeyCollection_t2331 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m14847_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C" void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m14848_gshared (KeyCollection_t2331 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m14848(__this, method) (( void (*) (KeyCollection_t2331 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m14848_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m14849_gshared (KeyCollection_t2331 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m14849(__this, ___item, method) (( bool (*) (KeyCollection_t2331 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m14849_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m14850_gshared (KeyCollection_t2331 * __this, int32_t ___item, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m14850(__this, ___item, method) (( bool (*) (KeyCollection_t2331 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m14850_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C" Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m14851_gshared (KeyCollection_t2331 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m14851(__this, method) (( Object_t* (*) (KeyCollection_t2331 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m14851_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void KeyCollection_System_Collections_ICollection_CopyTo_m14852_gshared (KeyCollection_t2331 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m14852(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2331 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m14852_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m14853_gshared (KeyCollection_t2331 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m14853(__this, method) (( Object_t * (*) (KeyCollection_t2331 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m14853_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C" bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m14854_gshared (KeyCollection_t2331 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m14854(__this, method) (( bool (*) (KeyCollection_t2331 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m14854_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m14855_gshared (KeyCollection_t2331 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m14855(__this, method) (( bool (*) (KeyCollection_t2331 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m14855_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m14856_gshared (KeyCollection_t2331 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m14856(__this, method) (( Object_t * (*) (KeyCollection_t2331 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m14856_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>::CopyTo(TKey[],System.Int32)
extern "C" void KeyCollection_CopyTo_m14857_gshared (KeyCollection_t2331 * __this, TweenTypeU5BU5D_t2316* ___array, int32_t ___index, const MethodInfo* method);
#define KeyCollection_CopyTo_m14857(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2331 *, TweenTypeU5BU5D_t2316*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m14857_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>::GetEnumerator()
extern "C" Enumerator_t2332  KeyCollection_GetEnumerator_m14858_gshared (KeyCollection_t2331 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m14858(__this, method) (( Enumerator_t2332  (*) (KeyCollection_t2331 *, const MethodInfo*))KeyCollection_GetEnumerator_m14858_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>::get_Count()
extern "C" int32_t KeyCollection_get_Count_m14859_gshared (KeyCollection_t2331 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m14859(__this, method) (( int32_t (*) (KeyCollection_t2331 *, const MethodInfo*))KeyCollection_get_Count_m14859_gshared)(__this, method)
