﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// CameraFilterPack_AAA_SuperComputer
struct  CameraFilterPack_AAA_SuperComputer_t3  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_AAA_SuperComputer::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_AAA_SuperComputer::_AlphaHexa
	float ____AlphaHexa_3;
	// System.Single CameraFilterPack_AAA_SuperComputer::TimeX
	float ___TimeX_4;
	// UnityEngine.Vector4 CameraFilterPack_AAA_SuperComputer::ScreenResolution
	Vector4_t5  ___ScreenResolution_5;
	// UnityEngine.Material CameraFilterPack_AAA_SuperComputer::SCMaterial
	Material_t2 * ___SCMaterial_6;
	// System.Single CameraFilterPack_AAA_SuperComputer::ShapeFormula
	float ___ShapeFormula_7;
	// System.Single CameraFilterPack_AAA_SuperComputer::Shape
	float ___Shape_8;
	// System.Single CameraFilterPack_AAA_SuperComputer::_BorderSize
	float ____BorderSize_9;
	// UnityEngine.Color CameraFilterPack_AAA_SuperComputer::_BorderColor
	Color_t6  ____BorderColor_10;
	// System.Single CameraFilterPack_AAA_SuperComputer::_SpotSize
	float ____SpotSize_11;
	// UnityEngine.Vector2 CameraFilterPack_AAA_SuperComputer::center
	Vector2_t7  ___center_18;
	// System.Single CameraFilterPack_AAA_SuperComputer::Radius
	float ___Radius_19;
};
struct CameraFilterPack_AAA_SuperComputer_t3_StaticFields{
	// System.Single CameraFilterPack_AAA_SuperComputer::ChangeBorderSize
	float ___ChangeBorderSize_12;
	// UnityEngine.Color CameraFilterPack_AAA_SuperComputer::ChangeBorderColor
	Color_t6  ___ChangeBorderColor_13;
	// UnityEngine.Color CameraFilterPack_AAA_SuperComputer::ChangeHexaColor
	Color_t6  ___ChangeHexaColor_14;
	// System.Single CameraFilterPack_AAA_SuperComputer::ChangeSpotSize
	float ___ChangeSpotSize_15;
	// System.Single CameraFilterPack_AAA_SuperComputer::ChangeAlphaHexa
	float ___ChangeAlphaHexa_16;
	// System.Single CameraFilterPack_AAA_SuperComputer::ChangeValue
	float ___ChangeValue_17;
	// UnityEngine.Vector2 CameraFilterPack_AAA_SuperComputer::Changecenter
	Vector2_t7  ___Changecenter_20;
	// System.Single CameraFilterPack_AAA_SuperComputer::ChangeRadius
	float ___ChangeRadius_21;
};
