﻿#pragma once
#include <stdint.h>
// GameManager
struct GameManager_t354;
// InteractiveObject[]
struct InteractiveObjectU5BU5D_t355;
// UnityEngine.GameObject
struct GameObject_t256;
// System.String
struct String_t;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t323;
// UnityEngine.Collider
struct Collider_t319;
// GoTo
struct GoTo_t315;
// UnityEngine.Animator
struct Animator_t321;
// PlayerController
struct PlayerController_t356;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// InteractiveEvent
#include "AssemblyU2DCSharp_InteractiveEvent.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// GameManager
struct  GameManager_t354  : public MonoBehaviour_t4
{
	// InteractiveObject[] GameManager::interactiveObject
	InteractiveObjectU5BU5D_t355* ___interactiveObject_3;
	// InteractiveEvent GameManager::IE
	int32_t ___IE_4;
	// System.String GameManager::apdroidAppUrl
	String_t* ___apdroidAppUrl_22;
	// System.Boolean GameManager::hasChildLaugh
	bool ___hasChildLaugh_23;
	// System.Boolean GameManager::hwLights
	bool ___hwLights_24;
	// System.Boolean GameManager::isStarring
	bool ___isStarring_25;
	// System.Boolean GameManager::isReady
	bool ___isReady_26;
	// System.Boolean GameManager::isRestarting
	bool ___isRestarting_27;
	// System.Boolean GameManager::isFlicker
	bool ___isFlicker_28;
	// System.Boolean GameManager::isQuitting
	bool ___isQuitting_29;
	// System.String GameManager::appPackageName
	String_t* ___appPackageName_30;
	// System.String GameManager::oldHitObject
	String_t* ___oldHitObject_31;
	// System.String GameManager::state
	String_t* ___state_32;
	// System.String GameManager::DebugText
	String_t* ___DebugText_33;
	// UnityEngine.GameObject GameManager::thisObject
	GameObject_t256 * ___thisObject_34;
	// UnityEngine.GameObject GameManager::Cursor
	GameObject_t256 * ___Cursor_35;
	// UnityEngine.GameObject GameManager::hidingObjects
	GameObject_t256 * ___hidingObjects_36;
	// UnityEngine.GameObject GameManager::behindUser
	GameObject_t256 * ___behindUser_37;
	// UnityEngine.GameObject GameManager::myFlashLight
	GameObject_t256 * ___myFlashLight_38;
	// UnityEngine.GameObject GameManager::Line
	GameObject_t256 * ___Line_39;
	// UnityEngine.GameObject GameManager::mamawaypoint
	GameObject_t256 * ___mamawaypoint_40;
	// UnityEngine.GameObject GameManager::myLightIndicator
	GameObject_t256 * ___myLightIndicator_41;
	// UnityEngine.GameObject GameManager::myDestination
	GameObject_t256 * ___myDestination_42;
	// UnityEngine.GameObject[] GameManager::IntroOBjects
	GameObjectU5BU5D_t323* ___IntroOBjects_43;
	// UnityEngine.Collider GameManager::col
	Collider_t319 * ___col_44;
	// UnityEngine.Vector3 GameManager::targetPos
	Vector3_t215  ___targetPos_45;
	// GoTo GameManager::GoTo
	GoTo_t315 * ___GoTo_46;
	// UnityEngine.Animator GameManager::Cursor_Anim
	Animator_t321 * ___Cursor_Anim_47;
	// PlayerController GameManager::PlayerController
	PlayerController_t356 * ___PlayerController_48;
	// System.Single GameManager::t
	float ___t_49;
	// System.Single GameManager::tStare
	float ___tStare_50;
	// System.Single GameManager::deltaTime
	float ___deltaTime_51;
};
struct GameManager_t354_StaticFields{
	// GameManager GameManager::_instance
	GameManager_t354 * ____instance_2;
	// UnityEngine.GameObject GameManager::INTRO
	GameObject_t256 * ___INTRO_5;
	// UnityEngine.GameObject GameManager::EMERGENCY_ROOM
	GameObject_t256 * ___EMERGENCY_ROOM_6;
	// UnityEngine.GameObject GameManager::HALLWAY_ROOM
	GameObject_t256 * ___HALLWAY_ROOM_7;
	// UnityEngine.GameObject GameManager::COMFORT_ROOM
	GameObject_t256 * ___COMFORT_ROOM_8;
	// UnityEngine.GameObject GameManager::LOBBY_ROOM
	GameObject_t256 * ___LOBBY_ROOM_9;
	// UnityEngine.GameObject GameManager::NURSERY_ROOM
	GameObject_t256 * ___NURSERY_ROOM_10;
	// UnityEngine.GameObject GameManager::MORGUE_ROOM
	GameObject_t256 * ___MORGUE_ROOM_11;
	// UnityEngine.GameObject GameManager::WARD_ROOM
	GameObject_t256 * ___WARD_ROOM_12;
	// UnityEngine.GameObject GameManager::EMERGENCY_OBJECTS
	GameObject_t256 * ___EMERGENCY_OBJECTS_13;
	// UnityEngine.GameObject GameManager::HALLWAY_OBJECTS
	GameObject_t256 * ___HALLWAY_OBJECTS_14;
	// UnityEngine.GameObject GameManager::COMFORT_OBJECTS
	GameObject_t256 * ___COMFORT_OBJECTS_15;
	// UnityEngine.GameObject GameManager::LOBBY_OBJECTS
	GameObject_t256 * ___LOBBY_OBJECTS_16;
	// UnityEngine.GameObject GameManager::NURSERY_OBJECTS
	GameObject_t256 * ___NURSERY_OBJECTS_17;
	// UnityEngine.GameObject GameManager::MORGUE_OBJECTS
	GameObject_t256 * ___MORGUE_OBJECTS_18;
	// UnityEngine.GameObject GameManager::WARD_OBJECTS
	GameObject_t256 * ___WARD_OBJECTS_19;
	// System.String GameManager::debugger
	String_t* ___debugger_20;
	// System.Int32 GameManager::countRestart
	int32_t ___countRestart_21;
};
