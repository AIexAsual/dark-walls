﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Vision_Plasma
struct CameraFilterPack_Vision_Plasma_t207;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Vision_Plasma::.ctor()
extern "C" void CameraFilterPack_Vision_Plasma__ctor_m1345 (CameraFilterPack_Vision_Plasma_t207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Vision_Plasma::get_material()
extern "C" Material_t2 * CameraFilterPack_Vision_Plasma_get_material_m1346 (CameraFilterPack_Vision_Plasma_t207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Plasma::Start()
extern "C" void CameraFilterPack_Vision_Plasma_Start_m1347 (CameraFilterPack_Vision_Plasma_t207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Plasma::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Vision_Plasma_OnRenderImage_m1348 (CameraFilterPack_Vision_Plasma_t207 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Plasma::OnValidate()
extern "C" void CameraFilterPack_Vision_Plasma_OnValidate_m1349 (CameraFilterPack_Vision_Plasma_t207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Plasma::Update()
extern "C" void CameraFilterPack_Vision_Plasma_Update_m1350 (CameraFilterPack_Vision_Plasma_t207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Plasma::OnDisable()
extern "C" void CameraFilterPack_Vision_Plasma_OnDisable_m1351 (CameraFilterPack_Vision_Plasma_t207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
