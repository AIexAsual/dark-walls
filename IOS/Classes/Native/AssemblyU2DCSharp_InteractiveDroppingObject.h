﻿#pragma once
#include <stdint.h>
// UnityEngine.Rigidbody
struct Rigidbody_t325;
// InteractiveObject
#include "AssemblyU2DCSharp_InteractiveObject.h"
// InteractiveDroppingObject
struct  InteractiveDroppingObject_t327  : public InteractiveObject_t328
{
	// UnityEngine.Rigidbody InteractiveDroppingObject::rigBod
	Rigidbody_t325 * ___rigBod_4;
};
