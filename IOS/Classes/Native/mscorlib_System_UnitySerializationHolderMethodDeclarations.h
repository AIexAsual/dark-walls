﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.UnitySerializationHolder
struct UnitySerializationHolder_t2145;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1104;
// System.Type
struct Type_t;
// System.DBNull
struct DBNull_t2087;
// System.Reflection.Module
struct Module_t1748;
// System.Object
struct Object_t;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.UnitySerializationHolder::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void UnitySerializationHolder__ctor_m13379 (UnitySerializationHolder_t2145 * __this, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___ctx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UnitySerializationHolder::GetTypeData(System.Type,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void UnitySerializationHolder_GetTypeData_m13380 (Object_t * __this /* static, unused */, Type_t * ___instance, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___ctx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UnitySerializationHolder::GetDBNullData(System.DBNull,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void UnitySerializationHolder_GetDBNullData_m13381 (Object_t * __this /* static, unused */, DBNull_t2087 * ___instance, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___ctx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UnitySerializationHolder::GetModuleData(System.Reflection.Module,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void UnitySerializationHolder_GetModuleData_m13382 (Object_t * __this /* static, unused */, Module_t1748 * ___instance, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___ctx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UnitySerializationHolder::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void UnitySerializationHolder_GetObjectData_m13383 (UnitySerializationHolder_t2145 * __this, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.UnitySerializationHolder::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern "C" Object_t * UnitySerializationHolder_GetRealObject_m13384 (UnitySerializationHolder_t2145 * __this, StreamingContext_t1105  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
