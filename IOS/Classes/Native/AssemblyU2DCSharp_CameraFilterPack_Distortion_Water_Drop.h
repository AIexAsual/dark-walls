﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Distortion_Water_Drop
struct  CameraFilterPack_Distortion_Water_Drop_t93  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Distortion_Water_Drop::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Distortion_Water_Drop::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Distortion_Water_Drop::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Distortion_Water_Drop::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Distortion_Water_Drop::CenterX
	float ___CenterX_6;
	// System.Single CameraFilterPack_Distortion_Water_Drop::CenterY
	float ___CenterY_7;
	// System.Single CameraFilterPack_Distortion_Water_Drop::WaveIntensity
	float ___WaveIntensity_8;
	// System.Int32 CameraFilterPack_Distortion_Water_Drop::NumberOfWaves
	int32_t ___NumberOfWaves_9;
};
struct CameraFilterPack_Distortion_Water_Drop_t93_StaticFields{
	// System.Single CameraFilterPack_Distortion_Water_Drop::ChangeCenterX
	float ___ChangeCenterX_10;
	// System.Single CameraFilterPack_Distortion_Water_Drop::ChangeCenterY
	float ___ChangeCenterY_11;
	// System.Single CameraFilterPack_Distortion_Water_Drop::ChangeWaveIntensity
	float ___ChangeWaveIntensity_12;
	// System.Int32 CameraFilterPack_Distortion_Water_Drop::ChangeNumberOfWaves
	int32_t ___ChangeNumberOfWaves_13;
};
