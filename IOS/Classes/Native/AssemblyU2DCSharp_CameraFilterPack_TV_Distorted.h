﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_TV_Distorted
struct  CameraFilterPack_TV_Distorted_t184  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_TV_Distorted::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_TV_Distorted::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_TV_Distorted::Distortion
	float ___Distortion_4;
	// System.Single CameraFilterPack_TV_Distorted::RGB
	float ___RGB_5;
	// UnityEngine.Material CameraFilterPack_TV_Distorted::SCMaterial
	Material_t2 * ___SCMaterial_6;
};
struct CameraFilterPack_TV_Distorted_t184_StaticFields{
	// System.Single CameraFilterPack_TV_Distorted::ChangeDistortion
	float ___ChangeDistortion_7;
};
