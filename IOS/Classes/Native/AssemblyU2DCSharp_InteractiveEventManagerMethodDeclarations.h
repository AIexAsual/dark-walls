﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractiveEventManager
struct InteractiveEventManager_t362;
// InteractiveEventManager/OnStateChangeHandler
struct OnStateChangeHandler_t361;
// InteractiveEvent
#include "AssemblyU2DCSharp_InteractiveEvent.h"

// System.Void InteractiveEventManager::.ctor()
extern "C" void InteractiveEventManager__ctor_m2098 (InteractiveEventManager_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveEventManager::.cctor()
extern "C" void InteractiveEventManager__cctor_m2099 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveEventManager::add_OnStateChangeEvent(InteractiveEventManager/OnStateChangeHandler)
extern "C" void InteractiveEventManager_add_OnStateChangeEvent_m2100 (InteractiveEventManager_t362 * __this, OnStateChangeHandler_t361 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveEventManager::remove_OnStateChangeEvent(InteractiveEventManager/OnStateChangeHandler)
extern "C" void InteractiveEventManager_remove_OnStateChangeEvent_m2101 (InteractiveEventManager_t362 * __this, OnStateChangeHandler_t361 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// InteractiveEventManager InteractiveEventManager::get_Instance()
extern "C" InteractiveEventManager_t362 * InteractiveEventManager_get_Instance_m2102 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveEventManager::dispatchEvent(InteractiveEvent)
extern "C" void InteractiveEventManager_dispatchEvent_m2103 (InteractiveEventManager_t362 * __this, int32_t ___stateEvent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveEventManager::Start()
extern "C" void InteractiveEventManager_Start_m2104 (InteractiveEventManager_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveEventManager::Update()
extern "C" void InteractiveEventManager_Update_m2105 (InteractiveEventManager_t362 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
