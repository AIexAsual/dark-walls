﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.DebuggerStepThroughAttribute
struct DebuggerStepThroughAttribute_t1675;

// System.Void System.Diagnostics.DebuggerStepThroughAttribute::.ctor()
extern "C" void DebuggerStepThroughAttribute__ctor_m10152 (DebuggerStepThroughAttribute_t1675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
