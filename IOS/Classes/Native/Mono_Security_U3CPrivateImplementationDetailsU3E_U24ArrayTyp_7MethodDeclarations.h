﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$4
struct U24ArrayTypeU244_t1331;
struct U24ArrayTypeU244_t1331_marshaled;

void U24ArrayTypeU244_t1331_marshal(const U24ArrayTypeU244_t1331& unmarshaled, U24ArrayTypeU244_t1331_marshaled& marshaled);
void U24ArrayTypeU244_t1331_marshal_back(const U24ArrayTypeU244_t1331_marshaled& marshaled, U24ArrayTypeU244_t1331& unmarshaled);
void U24ArrayTypeU244_t1331_marshal_cleanup(U24ArrayTypeU244_t1331_marshaled& marshaled);
