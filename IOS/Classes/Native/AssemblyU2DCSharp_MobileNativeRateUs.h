﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Action`1<MNDialogResult>
struct Action_1_t277;
// System.Object
#include "mscorlib_System_Object.h"
// MobileNativeRateUs
struct  MobileNativeRateUs_t281  : public Object_t
{
	// System.String MobileNativeRateUs::title
	String_t* ___title_0;
	// System.String MobileNativeRateUs::message
	String_t* ___message_1;
	// System.String MobileNativeRateUs::yes
	String_t* ___yes_2;
	// System.String MobileNativeRateUs::later
	String_t* ___later_3;
	// System.String MobileNativeRateUs::no
	String_t* ___no_4;
	// System.String MobileNativeRateUs::url
	String_t* ___url_5;
	// System.String MobileNativeRateUs::appleId
	String_t* ___appleId_6;
	// System.Action`1<MNDialogResult> MobileNativeRateUs::OnComplete
	Action_1_t277 * ___OnComplete_7;
};
struct MobileNativeRateUs_t281_StaticFields{
	// System.Action`1<MNDialogResult> MobileNativeRateUs::<>f__am$cache8
	Action_1_t277 * ___U3CU3Ef__amU24cache8_8;
};
