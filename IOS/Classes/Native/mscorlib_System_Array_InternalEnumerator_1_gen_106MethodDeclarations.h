﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>
struct InternalEnumerator_1_t2937;
// System.Object
struct Object_t;
// System.Reflection.Emit.MethodBuilder
struct MethodBuilder_t1740;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m22824(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2937 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13479_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22825(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2937 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13480_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>::Dispose()
#define InternalEnumerator_1_Dispose_m22826(__this, method) (( void (*) (InternalEnumerator_1_t2937 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13481_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>::MoveNext()
#define InternalEnumerator_1_MoveNext_m22827(__this, method) (( bool (*) (InternalEnumerator_1_t2937 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13482_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>::get_Current()
#define InternalEnumerator_1_get_Current_m22828(__this, method) (( MethodBuilder_t1740 * (*) (InternalEnumerator_1_t2937 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13483_gshared)(__this, method)
