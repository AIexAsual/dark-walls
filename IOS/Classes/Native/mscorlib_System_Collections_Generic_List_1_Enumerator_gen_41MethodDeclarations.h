﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Type>
struct Enumerator_t2815;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t1159;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Type>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m21757(__this, ___l, method) (( void (*) (Enumerator_t2815 *, List_1_t1159 *, const MethodInfo*))Enumerator__ctor_m13586_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21758(__this, method) (( Object_t * (*) (Enumerator_t2815 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13587_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Type>::Dispose()
#define Enumerator_Dispose_m21759(__this, method) (( void (*) (Enumerator_t2815 *, const MethodInfo*))Enumerator_Dispose_m13588_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Type>::VerifyState()
#define Enumerator_VerifyState_m21760(__this, method) (( void (*) (Enumerator_t2815 *, const MethodInfo*))Enumerator_VerifyState_m13589_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Type>::MoveNext()
#define Enumerator_MoveNext_m21761(__this, method) (( bool (*) (Enumerator_t2815 *, const MethodInfo*))Enumerator_MoveNext_m13590_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Type>::get_Current()
#define Enumerator_get_Current_m21762(__this, method) (( Type_t * (*) (Enumerator_t2815 *, const MethodInfo*))Enumerator_get_Current_m13591_gshared)(__this, method)
