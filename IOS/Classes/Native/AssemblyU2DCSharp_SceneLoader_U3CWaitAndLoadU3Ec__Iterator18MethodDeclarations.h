﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SceneLoader/<WaitAndLoad>c__Iterator18
struct U3CWaitAndLoadU3Ec__Iterator18_t408;
// System.Object
struct Object_t;

// System.Void SceneLoader/<WaitAndLoad>c__Iterator18::.ctor()
extern "C" void U3CWaitAndLoadU3Ec__Iterator18__ctor_m2330 (U3CWaitAndLoadU3Ec__Iterator18_t408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SceneLoader/<WaitAndLoad>c__Iterator18::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndLoadU3Ec__Iterator18_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2331 (U3CWaitAndLoadU3Ec__Iterator18_t408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SceneLoader/<WaitAndLoad>c__Iterator18::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndLoadU3Ec__Iterator18_System_Collections_IEnumerator_get_Current_m2332 (U3CWaitAndLoadU3Ec__Iterator18_t408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneLoader/<WaitAndLoad>c__Iterator18::MoveNext()
extern "C" bool U3CWaitAndLoadU3Ec__Iterator18_MoveNext_m2333 (U3CWaitAndLoadU3Ec__Iterator18_t408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneLoader/<WaitAndLoad>c__Iterator18::Dispose()
extern "C" void U3CWaitAndLoadU3Ec__Iterator18_Dispose_m2334 (U3CWaitAndLoadU3Ec__Iterator18_t408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneLoader/<WaitAndLoad>c__Iterator18::Reset()
extern "C" void U3CWaitAndLoadU3Ec__Iterator18_Reset_m2335 (U3CWaitAndLoadU3Ec__Iterator18_t408 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
