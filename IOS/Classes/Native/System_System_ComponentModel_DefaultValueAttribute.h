﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.ComponentModel.DefaultValueAttribute
struct  DefaultValueAttribute_t1389  : public Attribute_t903
{
	// System.Object System.ComponentModel.DefaultValueAttribute::DefaultValue
	Object_t * ___DefaultValue_0;
};
