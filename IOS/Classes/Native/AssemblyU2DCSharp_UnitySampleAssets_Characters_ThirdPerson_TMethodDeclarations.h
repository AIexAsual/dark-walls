﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings
struct AdvancedSettings_t411;

// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings::.ctor()
extern "C" void AdvancedSettings__ctor_m2358 (AdvancedSettings_t411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
