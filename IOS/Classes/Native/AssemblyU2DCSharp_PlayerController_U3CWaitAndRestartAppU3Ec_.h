﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// PlayerController/<WaitAndRestartApp>c__Iterator16
struct  U3CWaitAndRestartAppU3Ec__Iterator16_t396  : public Object_t
{
	// System.Single PlayerController/<WaitAndRestartApp>c__Iterator16::delay
	float ___delay_0;
	// System.Int32 PlayerController/<WaitAndRestartApp>c__Iterator16::$PC
	int32_t ___U24PC_1;
	// System.Object PlayerController/<WaitAndRestartApp>c__Iterator16::$current
	Object_t * ___U24current_2;
	// System.Single PlayerController/<WaitAndRestartApp>c__Iterator16::<$>delay
	float ___U3CU24U3Edelay_3;
};
