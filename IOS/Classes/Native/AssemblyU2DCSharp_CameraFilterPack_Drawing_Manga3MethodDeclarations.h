﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Drawing_Manga3
struct CameraFilterPack_Drawing_Manga3_t104;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Drawing_Manga3::.ctor()
extern "C" void CameraFilterPack_Drawing_Manga3__ctor_m664 (CameraFilterPack_Drawing_Manga3_t104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Manga3::get_material()
extern "C" Material_t2 * CameraFilterPack_Drawing_Manga3_get_material_m665 (CameraFilterPack_Drawing_Manga3_t104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga3::Start()
extern "C" void CameraFilterPack_Drawing_Manga3_Start_m666 (CameraFilterPack_Drawing_Manga3_t104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga3::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Drawing_Manga3_OnRenderImage_m667 (CameraFilterPack_Drawing_Manga3_t104 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga3::OnValidate()
extern "C" void CameraFilterPack_Drawing_Manga3_OnValidate_m668 (CameraFilterPack_Drawing_Manga3_t104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga3::Update()
extern "C" void CameraFilterPack_Drawing_Manga3_Update_m669 (CameraFilterPack_Drawing_Manga3_t104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga3::OnDisable()
extern "C" void CameraFilterPack_Drawing_Manga3_OnDisable_m670 (CameraFilterPack_Drawing_Manga3_t104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
