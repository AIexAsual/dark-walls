﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// iTween
struct iTween_t432;
// System.Object
#include "mscorlib_System_Object.h"
// iTween/<TweenRestart>c__Iterator1B
struct  U3CTweenRestartU3Ec__Iterator1B_t434  : public Object_t
{
	// System.Int32 iTween/<TweenRestart>c__Iterator1B::$PC
	int32_t ___U24PC_0;
	// System.Object iTween/<TweenRestart>c__Iterator1B::$current
	Object_t * ___U24current_1;
	// iTween iTween/<TweenRestart>c__Iterator1B::<>f__this
	iTween_t432 * ___U3CU3Ef__this_2;
};
