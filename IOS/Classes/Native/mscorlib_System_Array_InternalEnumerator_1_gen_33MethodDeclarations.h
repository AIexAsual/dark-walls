﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<iTweenEvent>
struct InternalEnumerator_1_t2391;
// System.Object
struct Object_t;
// iTweenEvent
struct iTweenEvent_t443;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<iTweenEvent>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15494(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2391 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13479_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<iTweenEvent>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15495(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2391 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13480_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<iTweenEvent>::Dispose()
#define InternalEnumerator_1_Dispose_m15496(__this, method) (( void (*) (InternalEnumerator_1_t2391 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13481_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<iTweenEvent>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15497(__this, method) (( bool (*) (InternalEnumerator_1_t2391 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13482_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<iTweenEvent>::get_Current()
#define InternalEnumerator_1_get_Current_m15498(__this, method) (( iTweenEvent_t443 * (*) (InternalEnumerator_1_t2391 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13483_gshared)(__this, method)
