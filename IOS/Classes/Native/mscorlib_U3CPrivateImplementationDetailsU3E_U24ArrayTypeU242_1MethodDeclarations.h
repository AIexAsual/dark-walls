﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$2048
struct U24ArrayTypeU242048_t2166;
struct U24ArrayTypeU242048_t2166_marshaled;

void U24ArrayTypeU242048_t2166_marshal(const U24ArrayTypeU242048_t2166& unmarshaled, U24ArrayTypeU242048_t2166_marshaled& marshaled);
void U24ArrayTypeU242048_t2166_marshal_back(const U24ArrayTypeU242048_t2166_marshaled& marshaled, U24ArrayTypeU242048_t2166& unmarshaled);
void U24ArrayTypeU242048_t2166_marshal_cleanup(U24ArrayTypeU242048_t2166_marshaled& marshaled);
