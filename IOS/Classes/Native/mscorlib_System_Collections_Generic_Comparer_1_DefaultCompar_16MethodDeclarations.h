﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>
struct DefaultComparer_t2969;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::.ctor()
extern "C" void DefaultComparer__ctor_m23045_gshared (DefaultComparer_t2969 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m23045(__this, method) (( void (*) (DefaultComparer_t2969 *, const MethodInfo*))DefaultComparer__ctor_m23045_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.TimeSpan>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m23046_gshared (DefaultComparer_t2969 * __this, TimeSpan_t1437  ___x, TimeSpan_t1437  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m23046(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2969 *, TimeSpan_t1437 , TimeSpan_t1437 , const MethodInfo*))DefaultComparer_Compare_m23046_gshared)(__this, ___x, ___y, method)
