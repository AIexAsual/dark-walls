﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t2039;

// System.Void System.Text.EncoderFallbackBuffer::.ctor()
extern "C" void EncoderFallbackBuffer__ctor_m12318 (EncoderFallbackBuffer_t2039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
