﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t578;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Color>
struct IEnumerable_1_t3033;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color>
struct IEnumerator_1_t3034;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<UnityEngine.Color>
struct ICollection_1_t3035;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>
struct ReadOnlyCollection_1_t2432;
// UnityEngine.Color[]
struct ColorU5BU5D_t449;
// System.Predicate`1<UnityEngine.Color>
struct Predicate_1_t2436;
// System.Comparison`1<UnityEngine.Color>
struct Comparison_1_t2439;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor()
extern "C" void List_1__ctor_m3393_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1__ctor_m3393(__this, method) (( void (*) (List_1_t578 *, const MethodInfo*))List_1__ctor_m3393_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m16074_gshared (List_1_t578 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m16074(__this, ___collection, method) (( void (*) (List_1_t578 *, Object_t*, const MethodInfo*))List_1__ctor_m16074_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor(System.Int32)
extern "C" void List_1__ctor_m16075_gshared (List_1_t578 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m16075(__this, ___capacity, method) (( void (*) (List_1_t578 *, int32_t, const MethodInfo*))List_1__ctor_m16075_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.cctor()
extern "C" void List_1__cctor_m16076_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m16076(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m16076_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Color>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16077_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16077(__this, method) (( Object_t* (*) (List_1_t578 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16077_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m16078_gshared (List_1_t578 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m16078(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t578 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m16078_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Color>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m16079_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16079(__this, method) (( Object_t * (*) (List_1_t578 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m16079_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m16080_gshared (List_1_t578 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m16080(__this, ___item, method) (( int32_t (*) (List_1_t578 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m16080_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m16081_gshared (List_1_t578 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m16081(__this, ___item, method) (( bool (*) (List_1_t578 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m16081_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m16082_gshared (List_1_t578 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m16082(__this, ___item, method) (( int32_t (*) (List_1_t578 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m16082_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m16083_gshared (List_1_t578 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m16083(__this, ___index, ___item, method) (( void (*) (List_1_t578 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m16083_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m16084_gshared (List_1_t578 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m16084(__this, ___item, method) (( void (*) (List_1_t578 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m16084_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16085_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16085(__this, method) (( bool (*) (List_1_t578 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16085_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m16086_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16086(__this, method) (( bool (*) (List_1_t578 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m16086_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Color>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m16087_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m16087(__this, method) (( Object_t * (*) (List_1_t578 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m16087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m16088_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m16088(__this, method) (( bool (*) (List_1_t578 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m16088_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m16089_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m16089(__this, method) (( bool (*) (List_1_t578 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m16089_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Color>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m16090_gshared (List_1_t578 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m16090(__this, ___index, method) (( Object_t * (*) (List_1_t578 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m16090_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m16091_gshared (List_1_t578 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m16091(__this, ___index, ___value, method) (( void (*) (List_1_t578 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m16091_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Add(T)
extern "C" void List_1_Add_m16092_gshared (List_1_t578 * __this, Color_t6  ___item, const MethodInfo* method);
#define List_1_Add_m16092(__this, ___item, method) (( void (*) (List_1_t578 *, Color_t6 , const MethodInfo*))List_1_Add_m16092_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m16093_gshared (List_1_t578 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m16093(__this, ___newCount, method) (( void (*) (List_1_t578 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m16093_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m16094_gshared (List_1_t578 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m16094(__this, ___collection, method) (( void (*) (List_1_t578 *, Object_t*, const MethodInfo*))List_1_AddCollection_m16094_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m16095_gshared (List_1_t578 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m16095(__this, ___enumerable, method) (( void (*) (List_1_t578 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m16095_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m16096_gshared (List_1_t578 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m16096(__this, ___collection, method) (( void (*) (List_1_t578 *, Object_t*, const MethodInfo*))List_1_AddRange_m16096_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Color>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2432 * List_1_AsReadOnly_m16097_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m16097(__this, method) (( ReadOnlyCollection_1_t2432 * (*) (List_1_t578 *, const MethodInfo*))List_1_AsReadOnly_m16097_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Clear()
extern "C" void List_1_Clear_m16098_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1_Clear_m16098(__this, method) (( void (*) (List_1_t578 *, const MethodInfo*))List_1_Clear_m16098_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color>::Contains(T)
extern "C" bool List_1_Contains_m16099_gshared (List_1_t578 * __this, Color_t6  ___item, const MethodInfo* method);
#define List_1_Contains_m16099(__this, ___item, method) (( bool (*) (List_1_t578 *, Color_t6 , const MethodInfo*))List_1_Contains_m16099_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m16100_gshared (List_1_t578 * __this, ColorU5BU5D_t449* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m16100(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t578 *, ColorU5BU5D_t449*, int32_t, const MethodInfo*))List_1_CopyTo_m16100_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Color>::Find(System.Predicate`1<T>)
extern "C" Color_t6  List_1_Find_m16101_gshared (List_1_t578 * __this, Predicate_1_t2436 * ___match, const MethodInfo* method);
#define List_1_Find_m16101(__this, ___match, method) (( Color_t6  (*) (List_1_t578 *, Predicate_1_t2436 *, const MethodInfo*))List_1_Find_m16101_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m16102_gshared (Object_t * __this /* static, unused */, Predicate_1_t2436 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m16102(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2436 *, const MethodInfo*))List_1_CheckMatch_m16102_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m16103_gshared (List_1_t578 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2436 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m16103(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t578 *, int32_t, int32_t, Predicate_1_t2436 *, const MethodInfo*))List_1_GetIndex_m16103_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Color>::GetEnumerator()
extern "C" Enumerator_t2431  List_1_GetEnumerator_m16104_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m16104(__this, method) (( Enumerator_t2431  (*) (List_1_t578 *, const MethodInfo*))List_1_GetEnumerator_m16104_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m16105_gshared (List_1_t578 * __this, Color_t6  ___item, const MethodInfo* method);
#define List_1_IndexOf_m16105(__this, ___item, method) (( int32_t (*) (List_1_t578 *, Color_t6 , const MethodInfo*))List_1_IndexOf_m16105_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m16106_gshared (List_1_t578 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m16106(__this, ___start, ___delta, method) (( void (*) (List_1_t578 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m16106_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m16107_gshared (List_1_t578 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m16107(__this, ___index, method) (( void (*) (List_1_t578 *, int32_t, const MethodInfo*))List_1_CheckIndex_m16107_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m16108_gshared (List_1_t578 * __this, int32_t ___index, Color_t6  ___item, const MethodInfo* method);
#define List_1_Insert_m16108(__this, ___index, ___item, method) (( void (*) (List_1_t578 *, int32_t, Color_t6 , const MethodInfo*))List_1_Insert_m16108_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m16109_gshared (List_1_t578 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m16109(__this, ___collection, method) (( void (*) (List_1_t578 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m16109_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Color>::Remove(T)
extern "C" bool List_1_Remove_m16110_gshared (List_1_t578 * __this, Color_t6  ___item, const MethodInfo* method);
#define List_1_Remove_m16110(__this, ___item, method) (( bool (*) (List_1_t578 *, Color_t6 , const MethodInfo*))List_1_Remove_m16110_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m16111_gshared (List_1_t578 * __this, Predicate_1_t2436 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m16111(__this, ___match, method) (( int32_t (*) (List_1_t578 *, Predicate_1_t2436 *, const MethodInfo*))List_1_RemoveAll_m16111_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m16112_gshared (List_1_t578 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m16112(__this, ___index, method) (( void (*) (List_1_t578 *, int32_t, const MethodInfo*))List_1_RemoveAt_m16112_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Reverse()
extern "C" void List_1_Reverse_m16113_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1_Reverse_m16113(__this, method) (( void (*) (List_1_t578 *, const MethodInfo*))List_1_Reverse_m16113_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Sort()
extern "C" void List_1_Sort_m16114_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1_Sort_m16114(__this, method) (( void (*) (List_1_t578 *, const MethodInfo*))List_1_Sort_m16114_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m16115_gshared (List_1_t578 * __this, Comparison_1_t2439 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m16115(__this, ___comparison, method) (( void (*) (List_1_t578 *, Comparison_1_t2439 *, const MethodInfo*))List_1_Sort_m16115_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Color>::ToArray()
extern "C" ColorU5BU5D_t449* List_1_ToArray_m3406_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1_ToArray_m3406(__this, method) (( ColorU5BU5D_t449* (*) (List_1_t578 *, const MethodInfo*))List_1_ToArray_m3406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::TrimExcess()
extern "C" void List_1_TrimExcess_m16116_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m16116(__this, method) (( void (*) (List_1_t578 *, const MethodInfo*))List_1_TrimExcess_m16116_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m16117_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m16117(__this, method) (( int32_t (*) (List_1_t578 *, const MethodInfo*))List_1_get_Capacity_m16117_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m16118_gshared (List_1_t578 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m16118(__this, ___value, method) (( void (*) (List_1_t578 *, int32_t, const MethodInfo*))List_1_set_Capacity_m16118_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color>::get_Count()
extern "C" int32_t List_1_get_Count_m16119_gshared (List_1_t578 * __this, const MethodInfo* method);
#define List_1_get_Count_m16119(__this, method) (( int32_t (*) (List_1_t578 *, const MethodInfo*))List_1_get_Count_m16119_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Color>::get_Item(System.Int32)
extern "C" Color_t6  List_1_get_Item_m16120_gshared (List_1_t578 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m16120(__this, ___index, method) (( Color_t6  (*) (List_1_t578 *, int32_t, const MethodInfo*))List_1_get_Item_m16120_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m16121_gshared (List_1_t578 * __this, int32_t ___index, Color_t6  ___value, const MethodInfo* method);
#define List_1_set_Item_m16121(__this, ___index, ___value, method) (( void (*) (List_1_t578 *, int32_t, Color_t6 , const MethodInfo*))List_1_set_Item_m16121_gshared)(__this, ___index, ___value, method)
