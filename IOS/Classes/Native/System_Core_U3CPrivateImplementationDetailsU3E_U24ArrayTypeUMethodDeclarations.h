﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$120
struct U24ArrayTypeU24120_t1184;
struct U24ArrayTypeU24120_t1184_marshaled;

void U24ArrayTypeU24120_t1184_marshal(const U24ArrayTypeU24120_t1184& unmarshaled, U24ArrayTypeU24120_t1184_marshaled& marshaled);
void U24ArrayTypeU24120_t1184_marshal_back(const U24ArrayTypeU24120_t1184_marshaled& marshaled, U24ArrayTypeU24120_t1184& unmarshaled);
void U24ArrayTypeU24120_t1184_marshal_cleanup(U24ArrayTypeU24120_t1184_marshaled& marshaled);
