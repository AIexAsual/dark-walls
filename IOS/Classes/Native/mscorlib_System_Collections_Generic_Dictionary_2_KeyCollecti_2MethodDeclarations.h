﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<iTweenEvent/TweenType,System.Object>
struct Enumerator_t2332;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>
struct Dictionary_2_t2327;
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<iTweenEvent/TweenType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m14860_gshared (Enumerator_t2332 * __this, Dictionary_2_t2327 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m14860(__this, ___host, method) (( void (*) (Enumerator_t2332 *, Dictionary_2_t2327 *, const MethodInfo*))Enumerator__ctor_m14860_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<iTweenEvent/TweenType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m14861_gshared (Enumerator_t2332 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m14861(__this, method) (( Object_t * (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14861_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<iTweenEvent/TweenType,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m14862_gshared (Enumerator_t2332 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m14862(__this, method) (( void (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_Dispose_m14862_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<iTweenEvent/TweenType,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m14863_gshared (Enumerator_t2332 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m14863(__this, method) (( bool (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_MoveNext_m14863_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<iTweenEvent/TweenType,System.Object>::get_Current()
extern "C" int32_t Enumerator_get_Current_m14864_gshared (Enumerator_t2332 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m14864(__this, method) (( int32_t (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_get_Current_m14864_gshared)(__this, method)
