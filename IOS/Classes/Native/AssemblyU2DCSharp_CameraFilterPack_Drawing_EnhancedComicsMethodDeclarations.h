﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Drawing_EnhancedComics
struct CameraFilterPack_Drawing_EnhancedComics_t99;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Drawing_EnhancedComics::.ctor()
extern "C" void CameraFilterPack_Drawing_EnhancedComics__ctor_m630 (CameraFilterPack_Drawing_EnhancedComics_t99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_EnhancedComics::get_material()
extern "C" Material_t2 * CameraFilterPack_Drawing_EnhancedComics_get_material_m631 (CameraFilterPack_Drawing_EnhancedComics_t99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_EnhancedComics::Start()
extern "C" void CameraFilterPack_Drawing_EnhancedComics_Start_m632 (CameraFilterPack_Drawing_EnhancedComics_t99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_EnhancedComics::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Drawing_EnhancedComics_OnRenderImage_m633 (CameraFilterPack_Drawing_EnhancedComics_t99 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_EnhancedComics::OnValidate()
extern "C" void CameraFilterPack_Drawing_EnhancedComics_OnValidate_m634 (CameraFilterPack_Drawing_EnhancedComics_t99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_EnhancedComics::Update()
extern "C" void CameraFilterPack_Drawing_EnhancedComics_Update_m635 (CameraFilterPack_Drawing_EnhancedComics_t99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_EnhancedComics::OnDisable()
extern "C" void CameraFilterPack_Drawing_EnhancedComics_OnDisable_m636 (CameraFilterPack_Drawing_EnhancedComics_t99 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
