﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>
struct Enumerator_t2903;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1463;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22655_gshared (Enumerator_t2903 * __this, Dictionary_2_t1463 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m22655(__this, ___host, method) (( void (*) (Enumerator_t2903 *, Dictionary_2_t1463 *, const MethodInfo*))Enumerator__ctor_m22655_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22656_gshared (Enumerator_t2903 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22656(__this, method) (( Object_t * (*) (Enumerator_t2903 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22656_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m22657_gshared (Enumerator_t2903 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22657(__this, method) (( void (*) (Enumerator_t2903 *, const MethodInfo*))Enumerator_Dispose_m22657_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22658_gshared (Enumerator_t2903 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22658(__this, method) (( bool (*) (Enumerator_t2903 *, const MethodInfo*))Enumerator_MoveNext_m22658_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m22659_gshared (Enumerator_t2903 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22659(__this, method) (( int32_t (*) (Enumerator_t2903 *, const MethodInfo*))Enumerator_get_Current_m22659_gshared)(__this, method)
