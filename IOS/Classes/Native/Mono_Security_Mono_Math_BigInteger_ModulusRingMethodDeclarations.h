﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Math.BigInteger/ModulusRing
struct ModulusRing_t1200;
// Mono.Math.BigInteger
struct BigInteger_t1199;

// System.Void Mono.Math.BigInteger/ModulusRing::.ctor(Mono.Math.BigInteger)
extern "C" void ModulusRing__ctor_m6573 (ModulusRing_t1200 * __this, BigInteger_t1199 * ___modulus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/ModulusRing::BarrettReduction(Mono.Math.BigInteger)
extern "C" void ModulusRing_BarrettReduction_m6574 (ModulusRing_t1200 * __this, BigInteger_t1199 * ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Multiply(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1199 * ModulusRing_Multiply_m6575 (ModulusRing_t1200 * __this, BigInteger_t1199 * ___a, BigInteger_t1199 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Difference(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1199 * ModulusRing_Difference_m6576 (ModulusRing_t1200 * __this, BigInteger_t1199 * ___a, BigInteger_t1199 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t1199 * ModulusRing_Pow_m6577 (ModulusRing_t1200 * __this, BigInteger_t1199 * ___a, BigInteger_t1199 * ___k, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(System.UInt32,Mono.Math.BigInteger)
extern "C" BigInteger_t1199 * ModulusRing_Pow_m6578 (ModulusRing_t1200 * __this, uint32_t ___b, BigInteger_t1199 * ___exp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
