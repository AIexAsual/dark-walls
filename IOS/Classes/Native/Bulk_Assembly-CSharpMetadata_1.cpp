﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// CameraFilterPack_EXTRA_SHOWFPS
#include "AssemblyU2DCSharp_CameraFilterPack_EXTRA_SHOWFPS.h"
// Metadata Definition CameraFilterPack_EXTRA_SHOWFPS
extern TypeInfo CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo;
// CameraFilterPack_EXTRA_SHOWFPS
#include "AssemblyU2DCSharp_CameraFilterPack_EXTRA_SHOWFPSMethodDeclarations.h"
extern const Il2CppType U3CFPSXU3Ec__Iterator0_t111_0_0_0;
static const Il2CppType* CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CFPSXU3Ec__Iterator0_t111_0_0_0,
};
static const EncodedMethodIndex CameraFilterPack_EXTRA_SHOWFPS_t110_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_EXTRA_SHOWFPS_t110_0_0_0;
extern const Il2CppType CameraFilterPack_EXTRA_SHOWFPS_t110_1_0_0;
extern const Il2CppType MonoBehaviour_t4_0_0_0;
struct CameraFilterPack_EXTRA_SHOWFPS_t110;
const Il2CppTypeDefinitionMetadata CameraFilterPack_EXTRA_SHOWFPS_t110_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_EXTRA_SHOWFPS_t110_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 971/* fieldStart */
	, 705/* methodStart */
	, -1/* eventStart */
	, 97/* propertyStart */

};
TypeInfo CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_EXTRA_SHOWFPS"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 322/* custom_attributes_cache */
	, &CameraFilterPack_EXTRA_SHOWFPS_t110_0_0_0/* byval_arg */
	, &CameraFilterPack_EXTRA_SHOWFPS_t110_1_0_0/* this_arg */
	, &CameraFilterPack_EXTRA_SHOWFPS_t110_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_EXTRA_SHOWFPS_t110)/* instance_size */
	, sizeof (CameraFilterPack_EXTRA_SHOWFPS_t110)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_EXTRA_SHOWFPS_t110_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0
#include "AssemblyU2DCSharp_CameraFilterPack_EXTRA_SHOWFPS_U3CFPSXU3Ec.h"
// Metadata Definition CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0
extern TypeInfo U3CFPSXU3Ec__Iterator0_t111_il2cpp_TypeInfo;
// CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0
#include "AssemblyU2DCSharp_CameraFilterPack_EXTRA_SHOWFPS_U3CFPSXU3EcMethodDeclarations.h"
static const EncodedMethodIndex U3CFPSXU3Ec__Iterator0_t111_VTable[9] = 
{
	626,
	601,
	627,
	628,
	629,
	630,
	631,
	632,
	633,
};
extern const Il2CppType IEnumerator_t464_0_0_0;
extern const Il2CppType IEnumerator_1_t2280_0_0_0;
extern const Il2CppType IDisposable_t538_0_0_0;
static const Il2CppType* U3CFPSXU3Ec__Iterator0_t111_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CFPSXU3Ec__Iterator0_t111_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CFPSXU3Ec__Iterator0_t111_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct U3CFPSXU3Ec__Iterator0_t111;
const Il2CppTypeDefinitionMetadata U3CFPSXU3Ec__Iterator0_t111_DefinitionMetadata = 
{
	&CameraFilterPack_EXTRA_SHOWFPS_t110_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CFPSXU3Ec__Iterator0_t111_InterfacesTypeInfos/* implementedInterfaces */
	, U3CFPSXU3Ec__Iterator0_t111_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CFPSXU3Ec__Iterator0_t111_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 987/* fieldStart */
	, 713/* methodStart */
	, -1/* eventStart */
	, 98/* propertyStart */

};
TypeInfo U3CFPSXU3Ec__Iterator0_t111_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<FPSX>c__Iterator0"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CFPSXU3Ec__Iterator0_t111_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 328/* custom_attributes_cache */
	, &U3CFPSXU3Ec__Iterator0_t111_0_0_0/* byval_arg */
	, &U3CFPSXU3Ec__Iterator0_t111_1_0_0/* this_arg */
	, &U3CFPSXU3Ec__Iterator0_t111_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CFPSXU3Ec__Iterator0_t111)/* instance_size */
	, sizeof (U3CFPSXU3Ec__Iterator0_t111)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// CameraFilterPack_Edge_BlackLine
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_BlackLine.h"
// Metadata Definition CameraFilterPack_Edge_BlackLine
extern TypeInfo CameraFilterPack_Edge_BlackLine_t112_il2cpp_TypeInfo;
// CameraFilterPack_Edge_BlackLine
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_BlackLineMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Edge_BlackLine_t112_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Edge_BlackLine_t112_0_0_0;
extern const Il2CppType CameraFilterPack_Edge_BlackLine_t112_1_0_0;
struct CameraFilterPack_Edge_BlackLine_t112;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Edge_BlackLine_t112_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Edge_BlackLine_t112_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 991/* fieldStart */
	, 719/* methodStart */
	, -1/* eventStart */
	, 100/* propertyStart */

};
TypeInfo CameraFilterPack_Edge_BlackLine_t112_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Edge_BlackLine"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Edge_BlackLine_t112_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 333/* custom_attributes_cache */
	, &CameraFilterPack_Edge_BlackLine_t112_0_0_0/* byval_arg */
	, &CameraFilterPack_Edge_BlackLine_t112_1_0_0/* this_arg */
	, &CameraFilterPack_Edge_BlackLine_t112_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Edge_BlackLine_t112)/* instance_size */
	, sizeof (CameraFilterPack_Edge_BlackLine_t112)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Edge_Edge_filter
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Edge_filter.h"
// Metadata Definition CameraFilterPack_Edge_Edge_filter
extern TypeInfo CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo;
// CameraFilterPack_Edge_Edge_filter
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Edge_filterMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Edge_Edge_filter_t113_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Edge_Edge_filter_t113_0_0_0;
extern const Il2CppType CameraFilterPack_Edge_Edge_filter_t113_1_0_0;
struct CameraFilterPack_Edge_Edge_filter_t113;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Edge_Edge_filter_t113_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Edge_Edge_filter_t113_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 995/* fieldStart */
	, 725/* methodStart */
	, -1/* eventStart */
	, 101/* propertyStart */

};
TypeInfo CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Edge_Edge_filter"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 334/* custom_attributes_cache */
	, &CameraFilterPack_Edge_Edge_filter_t113_0_0_0/* byval_arg */
	, &CameraFilterPack_Edge_Edge_filter_t113_1_0_0/* this_arg */
	, &CameraFilterPack_Edge_Edge_filter_t113_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Edge_Edge_filter_t113)/* instance_size */
	, sizeof (CameraFilterPack_Edge_Edge_filter_t113)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Edge_Edge_filter_t113_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Edge_Golden
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Golden.h"
// Metadata Definition CameraFilterPack_Edge_Golden
extern TypeInfo CameraFilterPack_Edge_Golden_t114_il2cpp_TypeInfo;
// CameraFilterPack_Edge_Golden
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_GoldenMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Edge_Golden_t114_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Edge_Golden_t114_0_0_0;
extern const Il2CppType CameraFilterPack_Edge_Golden_t114_1_0_0;
struct CameraFilterPack_Edge_Golden_t114;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Edge_Golden_t114_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Edge_Golden_t114_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1005/* fieldStart */
	, 732/* methodStart */
	, -1/* eventStart */
	, 102/* propertyStart */

};
TypeInfo CameraFilterPack_Edge_Golden_t114_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Edge_Golden"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Edge_Golden_t114_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 338/* custom_attributes_cache */
	, &CameraFilterPack_Edge_Golden_t114_0_0_0/* byval_arg */
	, &CameraFilterPack_Edge_Golden_t114_1_0_0/* this_arg */
	, &CameraFilterPack_Edge_Golden_t114_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Edge_Golden_t114)/* instance_size */
	, sizeof (CameraFilterPack_Edge_Golden_t114)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Edge_Neon
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Neon.h"
// Metadata Definition CameraFilterPack_Edge_Neon
extern TypeInfo CameraFilterPack_Edge_Neon_t115_il2cpp_TypeInfo;
// CameraFilterPack_Edge_Neon
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_NeonMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Edge_Neon_t115_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Edge_Neon_t115_0_0_0;
extern const Il2CppType CameraFilterPack_Edge_Neon_t115_1_0_0;
struct CameraFilterPack_Edge_Neon_t115;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Edge_Neon_t115_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Edge_Neon_t115_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1009/* fieldStart */
	, 738/* methodStart */
	, -1/* eventStart */
	, 103/* propertyStart */

};
TypeInfo CameraFilterPack_Edge_Neon_t115_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Edge_Neon"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Edge_Neon_t115_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 339/* custom_attributes_cache */
	, &CameraFilterPack_Edge_Neon_t115_0_0_0/* byval_arg */
	, &CameraFilterPack_Edge_Neon_t115_1_0_0/* this_arg */
	, &CameraFilterPack_Edge_Neon_t115_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Edge_Neon_t115)/* instance_size */
	, sizeof (CameraFilterPack_Edge_Neon_t115)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Edge_Neon_t115_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Edge_Sigmoid
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Sigmoid.h"
// Metadata Definition CameraFilterPack_Edge_Sigmoid
extern TypeInfo CameraFilterPack_Edge_Sigmoid_t116_il2cpp_TypeInfo;
// CameraFilterPack_Edge_Sigmoid
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_SigmoidMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Edge_Sigmoid_t116_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Edge_Sigmoid_t116_0_0_0;
extern const Il2CppType CameraFilterPack_Edge_Sigmoid_t116_1_0_0;
struct CameraFilterPack_Edge_Sigmoid_t116;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Edge_Sigmoid_t116_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Edge_Sigmoid_t116_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1015/* fieldStart */
	, 745/* methodStart */
	, -1/* eventStart */
	, 104/* propertyStart */

};
TypeInfo CameraFilterPack_Edge_Sigmoid_t116_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Edge_Sigmoid"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Edge_Sigmoid_t116_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 341/* custom_attributes_cache */
	, &CameraFilterPack_Edge_Sigmoid_t116_0_0_0/* byval_arg */
	, &CameraFilterPack_Edge_Sigmoid_t116_1_0_0/* this_arg */
	, &CameraFilterPack_Edge_Sigmoid_t116_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Edge_Sigmoid_t116)/* instance_size */
	, sizeof (CameraFilterPack_Edge_Sigmoid_t116)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Edge_Sigmoid_t116_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Edge_Sobel
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Sobel.h"
// Metadata Definition CameraFilterPack_Edge_Sobel
extern TypeInfo CameraFilterPack_Edge_Sobel_t117_il2cpp_TypeInfo;
// CameraFilterPack_Edge_Sobel
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_SobelMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Edge_Sobel_t117_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Edge_Sobel_t117_0_0_0;
extern const Il2CppType CameraFilterPack_Edge_Sobel_t117_1_0_0;
struct CameraFilterPack_Edge_Sobel_t117;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Edge_Sobel_t117_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Edge_Sobel_t117_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1021/* fieldStart */
	, 752/* methodStart */
	, -1/* eventStart */
	, 105/* propertyStart */

};
TypeInfo CameraFilterPack_Edge_Sobel_t117_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Edge_Sobel"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Edge_Sobel_t117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 343/* custom_attributes_cache */
	, &CameraFilterPack_Edge_Sobel_t117_0_0_0/* byval_arg */
	, &CameraFilterPack_Edge_Sobel_t117_1_0_0/* this_arg */
	, &CameraFilterPack_Edge_Sobel_t117_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Edge_Sobel_t117)/* instance_size */
	, sizeof (CameraFilterPack_Edge_Sobel_t117)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_EyesVision_1
#include "AssemblyU2DCSharp_CameraFilterPack_EyesVision_1.h"
// Metadata Definition CameraFilterPack_EyesVision_1
extern TypeInfo CameraFilterPack_EyesVision_1_t118_il2cpp_TypeInfo;
// CameraFilterPack_EyesVision_1
#include "AssemblyU2DCSharp_CameraFilterPack_EyesVision_1MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_EyesVision_1_t118_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_EyesVision_1_t118_0_0_0;
extern const Il2CppType CameraFilterPack_EyesVision_1_t118_1_0_0;
struct CameraFilterPack_EyesVision_1_t118;
const Il2CppTypeDefinitionMetadata CameraFilterPack_EyesVision_1_t118_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_EyesVision_1_t118_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1025/* fieldStart */
	, 758/* methodStart */
	, -1/* eventStart */
	, 106/* propertyStart */

};
TypeInfo CameraFilterPack_EyesVision_1_t118_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_EyesVision_1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_EyesVision_1_t118_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 344/* custom_attributes_cache */
	, &CameraFilterPack_EyesVision_1_t118_0_0_0/* byval_arg */
	, &CameraFilterPack_EyesVision_1_t118_1_0_0/* this_arg */
	, &CameraFilterPack_EyesVision_1_t118_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_EyesVision_1_t118)/* instance_size */
	, sizeof (CameraFilterPack_EyesVision_1_t118)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_EyesVision_2
#include "AssemblyU2DCSharp_CameraFilterPack_EyesVision_2.h"
// Metadata Definition CameraFilterPack_EyesVision_2
extern TypeInfo CameraFilterPack_EyesVision_2_t119_il2cpp_TypeInfo;
// CameraFilterPack_EyesVision_2
#include "AssemblyU2DCSharp_CameraFilterPack_EyesVision_2MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_EyesVision_2_t119_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_EyesVision_2_t119_0_0_0;
extern const Il2CppType CameraFilterPack_EyesVision_2_t119_1_0_0;
struct CameraFilterPack_EyesVision_2_t119;
const Il2CppTypeDefinitionMetadata CameraFilterPack_EyesVision_2_t119_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_EyesVision_2_t119_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1033/* fieldStart */
	, 764/* methodStart */
	, -1/* eventStart */
	, 107/* propertyStart */

};
TypeInfo CameraFilterPack_EyesVision_2_t119_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_EyesVision_2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_EyesVision_2_t119_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 349/* custom_attributes_cache */
	, &CameraFilterPack_EyesVision_2_t119_0_0_0/* byval_arg */
	, &CameraFilterPack_EyesVision_2_t119_1_0_0/* this_arg */
	, &CameraFilterPack_EyesVision_2_t119_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_EyesVision_2_t119)/* instance_size */
	, sizeof (CameraFilterPack_EyesVision_2_t119)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_8bits
#include "AssemblyU2DCSharp_CameraFilterPack_FX_8bits.h"
// Metadata Definition CameraFilterPack_FX_8bits
extern TypeInfo CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo;
// CameraFilterPack_FX_8bits
#include "AssemblyU2DCSharp_CameraFilterPack_FX_8bitsMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_8bits_t120_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_8bits_t120_0_0_0;
extern const Il2CppType CameraFilterPack_FX_8bits_t120_1_0_0;
struct CameraFilterPack_FX_8bits_t120;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_8bits_t120_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_8bits_t120_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1041/* fieldStart */
	, 770/* methodStart */
	, -1/* eventStart */
	, 108/* propertyStart */

};
TypeInfo CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_8bits"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 354/* custom_attributes_cache */
	, &CameraFilterPack_FX_8bits_t120_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_8bits_t120_1_0_0/* this_arg */
	, &CameraFilterPack_FX_8bits_t120_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_8bits_t120)/* instance_size */
	, sizeof (CameraFilterPack_FX_8bits_t120)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_8bits_t120_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_8bits_gb
#include "AssemblyU2DCSharp_CameraFilterPack_FX_8bits_gb.h"
// Metadata Definition CameraFilterPack_FX_8bits_gb
extern TypeInfo CameraFilterPack_FX_8bits_gb_t121_il2cpp_TypeInfo;
// CameraFilterPack_FX_8bits_gb
#include "AssemblyU2DCSharp_CameraFilterPack_FX_8bits_gbMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_8bits_gb_t121_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_8bits_gb_t121_0_0_0;
extern const Il2CppType CameraFilterPack_FX_8bits_gb_t121_1_0_0;
struct CameraFilterPack_FX_8bits_gb_t121;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_8bits_gb_t121_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_8bits_gb_t121_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1050/* fieldStart */
	, 777/* methodStart */
	, -1/* eventStart */
	, 109/* propertyStart */

};
TypeInfo CameraFilterPack_FX_8bits_gb_t121_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_8bits_gb"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_8bits_gb_t121_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 358/* custom_attributes_cache */
	, &CameraFilterPack_FX_8bits_gb_t121_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_8bits_gb_t121_1_0_0/* this_arg */
	, &CameraFilterPack_FX_8bits_gb_t121_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_8bits_gb_t121)/* instance_size */
	, sizeof (CameraFilterPack_FX_8bits_gb_t121)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_8bits_gb_t121_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Ascii
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Ascii.h"
// Metadata Definition CameraFilterPack_FX_Ascii
extern TypeInfo CameraFilterPack_FX_Ascii_t122_il2cpp_TypeInfo;
// CameraFilterPack_FX_Ascii
#include "AssemblyU2DCSharp_CameraFilterPack_FX_AsciiMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Ascii_t122_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Ascii_t122_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Ascii_t122_1_0_0;
struct CameraFilterPack_FX_Ascii_t122;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Ascii_t122_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Ascii_t122_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1055/* fieldStart */
	, 784/* methodStart */
	, -1/* eventStart */
	, 110/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Ascii_t122_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Ascii"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Ascii_t122_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 360/* custom_attributes_cache */
	, &CameraFilterPack_FX_Ascii_t122_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Ascii_t122_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Ascii_t122_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Ascii_t122)/* instance_size */
	, sizeof (CameraFilterPack_FX_Ascii_t122)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Dot_Circle
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Dot_Circle.h"
// Metadata Definition CameraFilterPack_FX_Dot_Circle
extern TypeInfo CameraFilterPack_FX_Dot_Circle_t123_il2cpp_TypeInfo;
// CameraFilterPack_FX_Dot_Circle
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Dot_CircleMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Dot_Circle_t123_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Dot_Circle_t123_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Dot_Circle_t123_1_0_0;
struct CameraFilterPack_FX_Dot_Circle_t123;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Dot_Circle_t123_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Dot_Circle_t123_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1059/* fieldStart */
	, 790/* methodStart */
	, -1/* eventStart */
	, 111/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Dot_Circle_t123_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Dot_Circle"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Dot_Circle_t123_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 361/* custom_attributes_cache */
	, &CameraFilterPack_FX_Dot_Circle_t123_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Dot_Circle_t123_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Dot_Circle_t123_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Dot_Circle_t123)/* instance_size */
	, sizeof (CameraFilterPack_FX_Dot_Circle_t123)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_Dot_Circle_t123_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Drunk
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Drunk.h"
// Metadata Definition CameraFilterPack_FX_Drunk
extern TypeInfo CameraFilterPack_FX_Drunk_t124_il2cpp_TypeInfo;
// CameraFilterPack_FX_Drunk
#include "AssemblyU2DCSharp_CameraFilterPack_FX_DrunkMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Drunk_t124_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Drunk_t124_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Drunk_t124_1_0_0;
struct CameraFilterPack_FX_Drunk_t124;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Drunk_t124_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Drunk_t124_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1065/* fieldStart */
	, 797/* methodStart */
	, -1/* eventStart */
	, 112/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Drunk_t124_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Drunk"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Drunk_t124_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 363/* custom_attributes_cache */
	, &CameraFilterPack_FX_Drunk_t124_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Drunk_t124_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Drunk_t124_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Drunk_t124)/* instance_size */
	, sizeof (CameraFilterPack_FX_Drunk_t124)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_Drunk_t124_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Drunk2
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Drunk2.h"
// Metadata Definition CameraFilterPack_FX_Drunk2
extern TypeInfo CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo;
// CameraFilterPack_FX_Drunk2
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Drunk2MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Drunk2_t125_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Drunk2_t125_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Drunk2_t125_1_0_0;
struct CameraFilterPack_FX_Drunk2_t125;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Drunk2_t125_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Drunk2_t125_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1072/* fieldStart */
	, 806/* methodStart */
	, -1/* eventStart */
	, 114/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Drunk2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 365/* custom_attributes_cache */
	, &CameraFilterPack_FX_Drunk2_t125_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Drunk2_t125_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Drunk2_t125_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Drunk2_t125)/* instance_size */
	, sizeof (CameraFilterPack_FX_Drunk2_t125)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_Drunk2_t125_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_EarthQuake
#include "AssemblyU2DCSharp_CameraFilterPack_FX_EarthQuake.h"
// Metadata Definition CameraFilterPack_FX_EarthQuake
extern TypeInfo CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo;
// CameraFilterPack_FX_EarthQuake
#include "AssemblyU2DCSharp_CameraFilterPack_FX_EarthQuakeMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_EarthQuake_t126_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_EarthQuake_t126_0_0_0;
extern const Il2CppType CameraFilterPack_FX_EarthQuake_t126_1_0_0;
struct CameraFilterPack_FX_EarthQuake_t126;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_EarthQuake_t126_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_EarthQuake_t126_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1084/* fieldStart */
	, 813/* methodStart */
	, -1/* eventStart */
	, 115/* propertyStart */

};
TypeInfo CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_EarthQuake"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 370/* custom_attributes_cache */
	, &CameraFilterPack_FX_EarthQuake_t126_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_EarthQuake_t126_1_0_0/* this_arg */
	, &CameraFilterPack_FX_EarthQuake_t126_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_EarthQuake_t126)/* instance_size */
	, sizeof (CameraFilterPack_FX_EarthQuake_t126)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_EarthQuake_t126_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Funk
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Funk.h"
// Metadata Definition CameraFilterPack_FX_Funk
extern TypeInfo CameraFilterPack_FX_Funk_t127_il2cpp_TypeInfo;
// CameraFilterPack_FX_Funk
#include "AssemblyU2DCSharp_CameraFilterPack_FX_FunkMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Funk_t127_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Funk_t127_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Funk_t127_1_0_0;
struct CameraFilterPack_FX_Funk_t127;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Funk_t127_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Funk_t127_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1096/* fieldStart */
	, 820/* methodStart */
	, -1/* eventStart */
	, 116/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Funk_t127_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Funk"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Funk_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 375/* custom_attributes_cache */
	, &CameraFilterPack_FX_Funk_t127_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Funk_t127_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Funk_t127_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Funk_t127)/* instance_size */
	, sizeof (CameraFilterPack_FX_Funk_t127)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Glitch1
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Glitch1.h"
// Metadata Definition CameraFilterPack_FX_Glitch1
extern TypeInfo CameraFilterPack_FX_Glitch1_t128_il2cpp_TypeInfo;
// CameraFilterPack_FX_Glitch1
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Glitch1MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Glitch1_t128_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Glitch1_t128_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Glitch1_t128_1_0_0;
struct CameraFilterPack_FX_Glitch1_t128;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Glitch1_t128_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Glitch1_t128_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1100/* fieldStart */
	, 826/* methodStart */
	, -1/* eventStart */
	, 117/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Glitch1_t128_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Glitch1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Glitch1_t128_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 376/* custom_attributes_cache */
	, &CameraFilterPack_FX_Glitch1_t128_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Glitch1_t128_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Glitch1_t128_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Glitch1_t128)/* instance_size */
	, sizeof (CameraFilterPack_FX_Glitch1_t128)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_Glitch1_t128_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Glitch2
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Glitch2.h"
// Metadata Definition CameraFilterPack_FX_Glitch2
extern TypeInfo CameraFilterPack_FX_Glitch2_t129_il2cpp_TypeInfo;
// CameraFilterPack_FX_Glitch2
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Glitch2MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Glitch2_t129_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Glitch2_t129_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Glitch2_t129_1_0_0;
struct CameraFilterPack_FX_Glitch2_t129;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Glitch2_t129_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Glitch2_t129_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1106/* fieldStart */
	, 833/* methodStart */
	, -1/* eventStart */
	, 118/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Glitch2_t129_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Glitch2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Glitch2_t129_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 378/* custom_attributes_cache */
	, &CameraFilterPack_FX_Glitch2_t129_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Glitch2_t129_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Glitch2_t129_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Glitch2_t129)/* instance_size */
	, sizeof (CameraFilterPack_FX_Glitch2_t129)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_Glitch2_t129_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Glitch3
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Glitch3.h"
// Metadata Definition CameraFilterPack_FX_Glitch3
extern TypeInfo CameraFilterPack_FX_Glitch3_t130_il2cpp_TypeInfo;
// CameraFilterPack_FX_Glitch3
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Glitch3MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Glitch3_t130_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Glitch3_t130_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Glitch3_t130_1_0_0;
struct CameraFilterPack_FX_Glitch3_t130;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Glitch3_t130_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Glitch3_t130_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1112/* fieldStart */
	, 840/* methodStart */
	, -1/* eventStart */
	, 119/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Glitch3_t130_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Glitch3"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Glitch3_t130_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 380/* custom_attributes_cache */
	, &CameraFilterPack_FX_Glitch3_t130_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Glitch3_t130_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Glitch3_t130_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Glitch3_t130)/* instance_size */
	, sizeof (CameraFilterPack_FX_Glitch3_t130)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_Glitch3_t130_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Grid
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Grid.h"
// Metadata Definition CameraFilterPack_FX_Grid
extern TypeInfo CameraFilterPack_FX_Grid_t131_il2cpp_TypeInfo;
// CameraFilterPack_FX_Grid
#include "AssemblyU2DCSharp_CameraFilterPack_FX_GridMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Grid_t131_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Grid_t131_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Grid_t131_1_0_0;
struct CameraFilterPack_FX_Grid_t131;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Grid_t131_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Grid_t131_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1118/* fieldStart */
	, 847/* methodStart */
	, -1/* eventStart */
	, 120/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Grid_t131_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Grid"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Grid_t131_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 382/* custom_attributes_cache */
	, &CameraFilterPack_FX_Grid_t131_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Grid_t131_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Grid_t131_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Grid_t131)/* instance_size */
	, sizeof (CameraFilterPack_FX_Grid_t131)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_Grid_t131_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Hexagon
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Hexagon.h"
// Metadata Definition CameraFilterPack_FX_Hexagon
extern TypeInfo CameraFilterPack_FX_Hexagon_t132_il2cpp_TypeInfo;
// CameraFilterPack_FX_Hexagon
#include "AssemblyU2DCSharp_CameraFilterPack_FX_HexagonMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Hexagon_t132_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Hexagon_t132_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Hexagon_t132_1_0_0;
struct CameraFilterPack_FX_Hexagon_t132;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Hexagon_t132_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Hexagon_t132_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1123/* fieldStart */
	, 854/* methodStart */
	, -1/* eventStart */
	, 121/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Hexagon_t132_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Hexagon"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Hexagon_t132_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 384/* custom_attributes_cache */
	, &CameraFilterPack_FX_Hexagon_t132_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Hexagon_t132_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Hexagon_t132_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Hexagon_t132)/* instance_size */
	, sizeof (CameraFilterPack_FX_Hexagon_t132)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Hexagon_Black
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Hexagon_Black.h"
// Metadata Definition CameraFilterPack_FX_Hexagon_Black
extern TypeInfo CameraFilterPack_FX_Hexagon_Black_t133_il2cpp_TypeInfo;
// CameraFilterPack_FX_Hexagon_Black
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Hexagon_BlackMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Hexagon_Black_t133_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Hexagon_Black_t133_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Hexagon_Black_t133_1_0_0;
struct CameraFilterPack_FX_Hexagon_Black_t133;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Hexagon_Black_t133_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Hexagon_Black_t133_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1127/* fieldStart */
	, 860/* methodStart */
	, -1/* eventStart */
	, 122/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Hexagon_Black_t133_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Hexagon_Black"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Hexagon_Black_t133_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 385/* custom_attributes_cache */
	, &CameraFilterPack_FX_Hexagon_Black_t133_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Hexagon_Black_t133_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Hexagon_Black_t133_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Hexagon_Black_t133)/* instance_size */
	, sizeof (CameraFilterPack_FX_Hexagon_Black_t133)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_Hexagon_Black_t133_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Hypno
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Hypno.h"
// Metadata Definition CameraFilterPack_FX_Hypno
extern TypeInfo CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo;
// CameraFilterPack_FX_Hypno
#include "AssemblyU2DCSharp_CameraFilterPack_FX_HypnoMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Hypno_t134_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Hypno_t134_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Hypno_t134_1_0_0;
struct CameraFilterPack_FX_Hypno_t134;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Hypno_t134_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Hypno_t134_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1133/* fieldStart */
	, 867/* methodStart */
	, -1/* eventStart */
	, 123/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Hypno"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 387/* custom_attributes_cache */
	, &CameraFilterPack_FX_Hypno_t134_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Hypno_t134_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Hypno_t134_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Hypno_t134)/* instance_size */
	, sizeof (CameraFilterPack_FX_Hypno_t134)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_Hypno_t134_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_InverChromiLum
#include "AssemblyU2DCSharp_CameraFilterPack_FX_InverChromiLum.h"
// Metadata Definition CameraFilterPack_FX_InverChromiLum
extern TypeInfo CameraFilterPack_FX_InverChromiLum_t135_il2cpp_TypeInfo;
// CameraFilterPack_FX_InverChromiLum
#include "AssemblyU2DCSharp_CameraFilterPack_FX_InverChromiLumMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_InverChromiLum_t135_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_InverChromiLum_t135_0_0_0;
extern const Il2CppType CameraFilterPack_FX_InverChromiLum_t135_1_0_0;
struct CameraFilterPack_FX_InverChromiLum_t135;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_InverChromiLum_t135_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_InverChromiLum_t135_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1145/* fieldStart */
	, 874/* methodStart */
	, -1/* eventStart */
	, 124/* propertyStart */

};
TypeInfo CameraFilterPack_FX_InverChromiLum_t135_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_InverChromiLum"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_InverChromiLum_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 392/* custom_attributes_cache */
	, &CameraFilterPack_FX_InverChromiLum_t135_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_InverChromiLum_t135_1_0_0/* this_arg */
	, &CameraFilterPack_FX_InverChromiLum_t135_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_InverChromiLum_t135)/* instance_size */
	, sizeof (CameraFilterPack_FX_InverChromiLum_t135)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Mirror
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Mirror.h"
// Metadata Definition CameraFilterPack_FX_Mirror
extern TypeInfo CameraFilterPack_FX_Mirror_t136_il2cpp_TypeInfo;
// CameraFilterPack_FX_Mirror
#include "AssemblyU2DCSharp_CameraFilterPack_FX_MirrorMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Mirror_t136_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Mirror_t136_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Mirror_t136_1_0_0;
struct CameraFilterPack_FX_Mirror_t136;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Mirror_t136_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Mirror_t136_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1149/* fieldStart */
	, 880/* methodStart */
	, -1/* eventStart */
	, 125/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Mirror_t136_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Mirror"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Mirror_t136_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 393/* custom_attributes_cache */
	, &CameraFilterPack_FX_Mirror_t136_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Mirror_t136_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Mirror_t136_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Mirror_t136)/* instance_size */
	, sizeof (CameraFilterPack_FX_Mirror_t136)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Plasma
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Plasma.h"
// Metadata Definition CameraFilterPack_FX_Plasma
extern TypeInfo CameraFilterPack_FX_Plasma_t137_il2cpp_TypeInfo;
// CameraFilterPack_FX_Plasma
#include "AssemblyU2DCSharp_CameraFilterPack_FX_PlasmaMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Plasma_t137_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Plasma_t137_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Plasma_t137_1_0_0;
struct CameraFilterPack_FX_Plasma_t137;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Plasma_t137_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Plasma_t137_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1153/* fieldStart */
	, 886/* methodStart */
	, -1/* eventStart */
	, 126/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Plasma_t137_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Plasma"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Plasma_t137_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 394/* custom_attributes_cache */
	, &CameraFilterPack_FX_Plasma_t137_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Plasma_t137_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Plasma_t137_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Plasma_t137)/* instance_size */
	, sizeof (CameraFilterPack_FX_Plasma_t137)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_Plasma_t137_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Psycho
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Psycho.h"
// Metadata Definition CameraFilterPack_FX_Psycho
extern TypeInfo CameraFilterPack_FX_Psycho_t138_il2cpp_TypeInfo;
// CameraFilterPack_FX_Psycho
#include "AssemblyU2DCSharp_CameraFilterPack_FX_PsychoMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Psycho_t138_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Psycho_t138_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Psycho_t138_1_0_0;
struct CameraFilterPack_FX_Psycho_t138;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Psycho_t138_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Psycho_t138_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1159/* fieldStart */
	, 893/* methodStart */
	, -1/* eventStart */
	, 127/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Psycho_t138_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Psycho"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Psycho_t138_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 396/* custom_attributes_cache */
	, &CameraFilterPack_FX_Psycho_t138_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Psycho_t138_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Psycho_t138_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Psycho_t138)/* instance_size */
	, sizeof (CameraFilterPack_FX_Psycho_t138)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_Psycho_t138_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Screens
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Screens.h"
// Metadata Definition CameraFilterPack_FX_Screens
extern TypeInfo CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo;
// CameraFilterPack_FX_Screens
#include "AssemblyU2DCSharp_CameraFilterPack_FX_ScreensMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Screens_t139_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Screens_t139_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Screens_t139_1_0_0;
struct CameraFilterPack_FX_Screens_t139;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Screens_t139_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Screens_t139_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1164/* fieldStart */
	, 900/* methodStart */
	, -1/* eventStart */
	, 128/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Screens"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 398/* custom_attributes_cache */
	, &CameraFilterPack_FX_Screens_t139_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Screens_t139_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Screens_t139_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Screens_t139)/* instance_size */
	, sizeof (CameraFilterPack_FX_Screens_t139)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_Screens_t139_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_Spot
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Spot.h"
// Metadata Definition CameraFilterPack_FX_Spot
extern TypeInfo CameraFilterPack_FX_Spot_t140_il2cpp_TypeInfo;
// CameraFilterPack_FX_Spot
#include "AssemblyU2DCSharp_CameraFilterPack_FX_SpotMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_Spot_t140_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_Spot_t140_0_0_0;
extern const Il2CppType CameraFilterPack_FX_Spot_t140_1_0_0;
struct CameraFilterPack_FX_Spot_t140;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_Spot_t140_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_Spot_t140_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1176/* fieldStart */
	, 907/* methodStart */
	, -1/* eventStart */
	, 129/* propertyStart */

};
TypeInfo CameraFilterPack_FX_Spot_t140_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_Spot"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_Spot_t140_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 403/* custom_attributes_cache */
	, &CameraFilterPack_FX_Spot_t140_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_Spot_t140_1_0_0/* this_arg */
	, &CameraFilterPack_FX_Spot_t140_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_Spot_t140)/* instance_size */
	, sizeof (CameraFilterPack_FX_Spot_t140)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_Spot_t140_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_ZebraColor
#include "AssemblyU2DCSharp_CameraFilterPack_FX_ZebraColor.h"
// Metadata Definition CameraFilterPack_FX_ZebraColor
extern TypeInfo CameraFilterPack_FX_ZebraColor_t141_il2cpp_TypeInfo;
// CameraFilterPack_FX_ZebraColor
#include "AssemblyU2DCSharp_CameraFilterPack_FX_ZebraColorMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_ZebraColor_t141_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_ZebraColor_t141_0_0_0;
extern const Il2CppType CameraFilterPack_FX_ZebraColor_t141_1_0_0;
struct CameraFilterPack_FX_ZebraColor_t141;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_ZebraColor_t141_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_ZebraColor_t141_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1184/* fieldStart */
	, 914/* methodStart */
	, -1/* eventStart */
	, 130/* propertyStart */

};
TypeInfo CameraFilterPack_FX_ZebraColor_t141_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_ZebraColor"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_ZebraColor_t141_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 404/* custom_attributes_cache */
	, &CameraFilterPack_FX_ZebraColor_t141_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_ZebraColor_t141_1_0_0/* this_arg */
	, &CameraFilterPack_FX_ZebraColor_t141_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_ZebraColor_t141)/* instance_size */
	, sizeof (CameraFilterPack_FX_ZebraColor_t141)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_FX_ZebraColor_t141_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_FX_superDot
#include "AssemblyU2DCSharp_CameraFilterPack_FX_superDot.h"
// Metadata Definition CameraFilterPack_FX_superDot
extern TypeInfo CameraFilterPack_FX_superDot_t142_il2cpp_TypeInfo;
// CameraFilterPack_FX_superDot
#include "AssemblyU2DCSharp_CameraFilterPack_FX_superDotMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_FX_superDot_t142_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_FX_superDot_t142_0_0_0;
extern const Il2CppType CameraFilterPack_FX_superDot_t142_1_0_0;
struct CameraFilterPack_FX_superDot_t142;
const Il2CppTypeDefinitionMetadata CameraFilterPack_FX_superDot_t142_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_FX_superDot_t142_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1190/* fieldStart */
	, 921/* methodStart */
	, -1/* eventStart */
	, 131/* propertyStart */

};
TypeInfo CameraFilterPack_FX_superDot_t142_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_FX_superDot"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_FX_superDot_t142_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 406/* custom_attributes_cache */
	, &CameraFilterPack_FX_superDot_t142_0_0_0/* byval_arg */
	, &CameraFilterPack_FX_superDot_t142_1_0_0/* this_arg */
	, &CameraFilterPack_FX_superDot_t142_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_FX_superDot_t142)/* instance_size */
	, sizeof (CameraFilterPack_FX_superDot_t142)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Film_ColorPerfection
#include "AssemblyU2DCSharp_CameraFilterPack_Film_ColorPerfection.h"
// Metadata Definition CameraFilterPack_Film_ColorPerfection
extern TypeInfo CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo;
// CameraFilterPack_Film_ColorPerfection
#include "AssemblyU2DCSharp_CameraFilterPack_Film_ColorPerfectionMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Film_ColorPerfection_t143_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Film_ColorPerfection_t143_0_0_0;
extern const Il2CppType CameraFilterPack_Film_ColorPerfection_t143_1_0_0;
struct CameraFilterPack_Film_ColorPerfection_t143;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Film_ColorPerfection_t143_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Film_ColorPerfection_t143_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1194/* fieldStart */
	, 927/* methodStart */
	, -1/* eventStart */
	, 132/* propertyStart */

};
TypeInfo CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Film_ColorPerfection"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 407/* custom_attributes_cache */
	, &CameraFilterPack_Film_ColorPerfection_t143_0_0_0/* byval_arg */
	, &CameraFilterPack_Film_ColorPerfection_t143_1_0_0/* this_arg */
	, &CameraFilterPack_Film_ColorPerfection_t143_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Film_ColorPerfection_t143)/* instance_size */
	, sizeof (CameraFilterPack_Film_ColorPerfection_t143)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Film_ColorPerfection_t143_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Film_Grain
#include "AssemblyU2DCSharp_CameraFilterPack_Film_Grain.h"
// Metadata Definition CameraFilterPack_Film_Grain
extern TypeInfo CameraFilterPack_Film_Grain_t144_il2cpp_TypeInfo;
// CameraFilterPack_Film_Grain
#include "AssemblyU2DCSharp_CameraFilterPack_Film_GrainMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Film_Grain_t144_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Film_Grain_t144_0_0_0;
extern const Il2CppType CameraFilterPack_Film_Grain_t144_1_0_0;
struct CameraFilterPack_Film_Grain_t144;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Film_Grain_t144_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Film_Grain_t144_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1206/* fieldStart */
	, 934/* methodStart */
	, -1/* eventStart */
	, 133/* propertyStart */

};
TypeInfo CameraFilterPack_Film_Grain_t144_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Film_Grain"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Film_Grain_t144_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 412/* custom_attributes_cache */
	, &CameraFilterPack_Film_Grain_t144_0_0_0/* byval_arg */
	, &CameraFilterPack_Film_Grain_t144_1_0_0/* this_arg */
	, &CameraFilterPack_Film_Grain_t144_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Film_Grain_t144)/* instance_size */
	, sizeof (CameraFilterPack_Film_Grain_t144)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Film_Grain_t144_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Gradients_Ansi
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Ansi.h"
// Metadata Definition CameraFilterPack_Gradients_Ansi
extern TypeInfo CameraFilterPack_Gradients_Ansi_t145_il2cpp_TypeInfo;
// CameraFilterPack_Gradients_Ansi
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_AnsiMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Gradients_Ansi_t145_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Gradients_Ansi_t145_0_0_0;
extern const Il2CppType CameraFilterPack_Gradients_Ansi_t145_1_0_0;
struct CameraFilterPack_Gradients_Ansi_t145;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Gradients_Ansi_t145_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Gradients_Ansi_t145_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1212/* fieldStart */
	, 941/* methodStart */
	, -1/* eventStart */
	, 134/* propertyStart */

};
TypeInfo CameraFilterPack_Gradients_Ansi_t145_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Gradients_Ansi"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Gradients_Ansi_t145_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 414/* custom_attributes_cache */
	, &CameraFilterPack_Gradients_Ansi_t145_0_0_0/* byval_arg */
	, &CameraFilterPack_Gradients_Ansi_t145_1_0_0/* this_arg */
	, &CameraFilterPack_Gradients_Ansi_t145_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Gradients_Ansi_t145)/* instance_size */
	, sizeof (CameraFilterPack_Gradients_Ansi_t145)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Gradients_Desert
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Desert.h"
// Metadata Definition CameraFilterPack_Gradients_Desert
extern TypeInfo CameraFilterPack_Gradients_Desert_t146_il2cpp_TypeInfo;
// CameraFilterPack_Gradients_Desert
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_DesertMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Gradients_Desert_t146_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Gradients_Desert_t146_0_0_0;
extern const Il2CppType CameraFilterPack_Gradients_Desert_t146_1_0_0;
struct CameraFilterPack_Gradients_Desert_t146;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Gradients_Desert_t146_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Gradients_Desert_t146_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1219/* fieldStart */
	, 947/* methodStart */
	, -1/* eventStart */
	, 135/* propertyStart */

};
TypeInfo CameraFilterPack_Gradients_Desert_t146_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Gradients_Desert"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Gradients_Desert_t146_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 417/* custom_attributes_cache */
	, &CameraFilterPack_Gradients_Desert_t146_0_0_0/* byval_arg */
	, &CameraFilterPack_Gradients_Desert_t146_1_0_0/* this_arg */
	, &CameraFilterPack_Gradients_Desert_t146_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Gradients_Desert_t146)/* instance_size */
	, sizeof (CameraFilterPack_Gradients_Desert_t146)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Gradients_ElectricGradient
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_ElectricGradien.h"
// Metadata Definition CameraFilterPack_Gradients_ElectricGradient
extern TypeInfo CameraFilterPack_Gradients_ElectricGradient_t147_il2cpp_TypeInfo;
// CameraFilterPack_Gradients_ElectricGradient
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_ElectricGradienMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Gradients_ElectricGradient_t147_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Gradients_ElectricGradient_t147_0_0_0;
extern const Il2CppType CameraFilterPack_Gradients_ElectricGradient_t147_1_0_0;
struct CameraFilterPack_Gradients_ElectricGradient_t147;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Gradients_ElectricGradient_t147_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Gradients_ElectricGradient_t147_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1226/* fieldStart */
	, 953/* methodStart */
	, -1/* eventStart */
	, 136/* propertyStart */

};
TypeInfo CameraFilterPack_Gradients_ElectricGradient_t147_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Gradients_ElectricGradient"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Gradients_ElectricGradient_t147_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 420/* custom_attributes_cache */
	, &CameraFilterPack_Gradients_ElectricGradient_t147_0_0_0/* byval_arg */
	, &CameraFilterPack_Gradients_ElectricGradient_t147_1_0_0/* this_arg */
	, &CameraFilterPack_Gradients_ElectricGradient_t147_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Gradients_ElectricGradient_t147)/* instance_size */
	, sizeof (CameraFilterPack_Gradients_ElectricGradient_t147)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Gradients_FireGradient
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_FireGradient.h"
// Metadata Definition CameraFilterPack_Gradients_FireGradient
extern TypeInfo CameraFilterPack_Gradients_FireGradient_t148_il2cpp_TypeInfo;
// CameraFilterPack_Gradients_FireGradient
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_FireGradientMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Gradients_FireGradient_t148_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Gradients_FireGradient_t148_0_0_0;
extern const Il2CppType CameraFilterPack_Gradients_FireGradient_t148_1_0_0;
struct CameraFilterPack_Gradients_FireGradient_t148;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Gradients_FireGradient_t148_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Gradients_FireGradient_t148_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1233/* fieldStart */
	, 959/* methodStart */
	, -1/* eventStart */
	, 137/* propertyStart */

};
TypeInfo CameraFilterPack_Gradients_FireGradient_t148_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Gradients_FireGradient"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Gradients_FireGradient_t148_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 423/* custom_attributes_cache */
	, &CameraFilterPack_Gradients_FireGradient_t148_0_0_0/* byval_arg */
	, &CameraFilterPack_Gradients_FireGradient_t148_1_0_0/* this_arg */
	, &CameraFilterPack_Gradients_FireGradient_t148_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Gradients_FireGradient_t148)/* instance_size */
	, sizeof (CameraFilterPack_Gradients_FireGradient_t148)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Gradients_Hue
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Hue.h"
// Metadata Definition CameraFilterPack_Gradients_Hue
extern TypeInfo CameraFilterPack_Gradients_Hue_t149_il2cpp_TypeInfo;
// CameraFilterPack_Gradients_Hue
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_HueMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Gradients_Hue_t149_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Gradients_Hue_t149_0_0_0;
extern const Il2CppType CameraFilterPack_Gradients_Hue_t149_1_0_0;
struct CameraFilterPack_Gradients_Hue_t149;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Gradients_Hue_t149_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Gradients_Hue_t149_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1240/* fieldStart */
	, 965/* methodStart */
	, -1/* eventStart */
	, 138/* propertyStart */

};
TypeInfo CameraFilterPack_Gradients_Hue_t149_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Gradients_Hue"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Gradients_Hue_t149_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 426/* custom_attributes_cache */
	, &CameraFilterPack_Gradients_Hue_t149_0_0_0/* byval_arg */
	, &CameraFilterPack_Gradients_Hue_t149_1_0_0/* this_arg */
	, &CameraFilterPack_Gradients_Hue_t149_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Gradients_Hue_t149)/* instance_size */
	, sizeof (CameraFilterPack_Gradients_Hue_t149)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Gradients_NeonGradient
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_NeonGradient.h"
// Metadata Definition CameraFilterPack_Gradients_NeonGradient
extern TypeInfo CameraFilterPack_Gradients_NeonGradient_t150_il2cpp_TypeInfo;
// CameraFilterPack_Gradients_NeonGradient
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_NeonGradientMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Gradients_NeonGradient_t150_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Gradients_NeonGradient_t150_0_0_0;
extern const Il2CppType CameraFilterPack_Gradients_NeonGradient_t150_1_0_0;
struct CameraFilterPack_Gradients_NeonGradient_t150;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Gradients_NeonGradient_t150_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Gradients_NeonGradient_t150_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1247/* fieldStart */
	, 971/* methodStart */
	, -1/* eventStart */
	, 139/* propertyStart */

};
TypeInfo CameraFilterPack_Gradients_NeonGradient_t150_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Gradients_NeonGradient"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Gradients_NeonGradient_t150_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 429/* custom_attributes_cache */
	, &CameraFilterPack_Gradients_NeonGradient_t150_0_0_0/* byval_arg */
	, &CameraFilterPack_Gradients_NeonGradient_t150_1_0_0/* this_arg */
	, &CameraFilterPack_Gradients_NeonGradient_t150_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Gradients_NeonGradient_t150)/* instance_size */
	, sizeof (CameraFilterPack_Gradients_NeonGradient_t150)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Gradients_Rainbow
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Rainbow.h"
// Metadata Definition CameraFilterPack_Gradients_Rainbow
extern TypeInfo CameraFilterPack_Gradients_Rainbow_t151_il2cpp_TypeInfo;
// CameraFilterPack_Gradients_Rainbow
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_RainbowMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Gradients_Rainbow_t151_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Gradients_Rainbow_t151_0_0_0;
extern const Il2CppType CameraFilterPack_Gradients_Rainbow_t151_1_0_0;
struct CameraFilterPack_Gradients_Rainbow_t151;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Gradients_Rainbow_t151_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Gradients_Rainbow_t151_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1254/* fieldStart */
	, 977/* methodStart */
	, -1/* eventStart */
	, 140/* propertyStart */

};
TypeInfo CameraFilterPack_Gradients_Rainbow_t151_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Gradients_Rainbow"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Gradients_Rainbow_t151_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 432/* custom_attributes_cache */
	, &CameraFilterPack_Gradients_Rainbow_t151_0_0_0/* byval_arg */
	, &CameraFilterPack_Gradients_Rainbow_t151_1_0_0/* this_arg */
	, &CameraFilterPack_Gradients_Rainbow_t151_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Gradients_Rainbow_t151)/* instance_size */
	, sizeof (CameraFilterPack_Gradients_Rainbow_t151)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Gradients_Stripe
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Stripe.h"
// Metadata Definition CameraFilterPack_Gradients_Stripe
extern TypeInfo CameraFilterPack_Gradients_Stripe_t152_il2cpp_TypeInfo;
// CameraFilterPack_Gradients_Stripe
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_StripeMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Gradients_Stripe_t152_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Gradients_Stripe_t152_0_0_0;
extern const Il2CppType CameraFilterPack_Gradients_Stripe_t152_1_0_0;
struct CameraFilterPack_Gradients_Stripe_t152;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Gradients_Stripe_t152_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Gradients_Stripe_t152_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1261/* fieldStart */
	, 983/* methodStart */
	, -1/* eventStart */
	, 141/* propertyStart */

};
TypeInfo CameraFilterPack_Gradients_Stripe_t152_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Gradients_Stripe"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Gradients_Stripe_t152_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 435/* custom_attributes_cache */
	, &CameraFilterPack_Gradients_Stripe_t152_0_0_0/* byval_arg */
	, &CameraFilterPack_Gradients_Stripe_t152_1_0_0/* this_arg */
	, &CameraFilterPack_Gradients_Stripe_t152_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Gradients_Stripe_t152)/* instance_size */
	, sizeof (CameraFilterPack_Gradients_Stripe_t152)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Gradients_Tech
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Tech.h"
// Metadata Definition CameraFilterPack_Gradients_Tech
extern TypeInfo CameraFilterPack_Gradients_Tech_t153_il2cpp_TypeInfo;
// CameraFilterPack_Gradients_Tech
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_TechMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Gradients_Tech_t153_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Gradients_Tech_t153_0_0_0;
extern const Il2CppType CameraFilterPack_Gradients_Tech_t153_1_0_0;
struct CameraFilterPack_Gradients_Tech_t153;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Gradients_Tech_t153_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Gradients_Tech_t153_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1268/* fieldStart */
	, 989/* methodStart */
	, -1/* eventStart */
	, 142/* propertyStart */

};
TypeInfo CameraFilterPack_Gradients_Tech_t153_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Gradients_Tech"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Gradients_Tech_t153_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 438/* custom_attributes_cache */
	, &CameraFilterPack_Gradients_Tech_t153_0_0_0/* byval_arg */
	, &CameraFilterPack_Gradients_Tech_t153_1_0_0/* this_arg */
	, &CameraFilterPack_Gradients_Tech_t153_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Gradients_Tech_t153)/* instance_size */
	, sizeof (CameraFilterPack_Gradients_Tech_t153)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Gradients_Therma
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Therma.h"
// Metadata Definition CameraFilterPack_Gradients_Therma
extern TypeInfo CameraFilterPack_Gradients_Therma_t154_il2cpp_TypeInfo;
// CameraFilterPack_Gradients_Therma
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_ThermaMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Gradients_Therma_t154_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Gradients_Therma_t154_0_0_0;
extern const Il2CppType CameraFilterPack_Gradients_Therma_t154_1_0_0;
struct CameraFilterPack_Gradients_Therma_t154;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Gradients_Therma_t154_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Gradients_Therma_t154_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1275/* fieldStart */
	, 995/* methodStart */
	, -1/* eventStart */
	, 143/* propertyStart */

};
TypeInfo CameraFilterPack_Gradients_Therma_t154_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Gradients_Therma"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Gradients_Therma_t154_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 441/* custom_attributes_cache */
	, &CameraFilterPack_Gradients_Therma_t154_0_0_0/* byval_arg */
	, &CameraFilterPack_Gradients_Therma_t154_1_0_0/* this_arg */
	, &CameraFilterPack_Gradients_Therma_t154_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Gradients_Therma_t154)/* instance_size */
	, sizeof (CameraFilterPack_Gradients_Therma_t154)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Light_Rainbow
#include "AssemblyU2DCSharp_CameraFilterPack_Light_Rainbow.h"
// Metadata Definition CameraFilterPack_Light_Rainbow
extern TypeInfo CameraFilterPack_Light_Rainbow_t155_il2cpp_TypeInfo;
// CameraFilterPack_Light_Rainbow
#include "AssemblyU2DCSharp_CameraFilterPack_Light_RainbowMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Light_Rainbow_t155_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Light_Rainbow_t155_0_0_0;
extern const Il2CppType CameraFilterPack_Light_Rainbow_t155_1_0_0;
struct CameraFilterPack_Light_Rainbow_t155;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Light_Rainbow_t155_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Light_Rainbow_t155_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1282/* fieldStart */
	, 1001/* methodStart */
	, -1/* eventStart */
	, 144/* propertyStart */

};
TypeInfo CameraFilterPack_Light_Rainbow_t155_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Light_Rainbow"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Light_Rainbow_t155_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 444/* custom_attributes_cache */
	, &CameraFilterPack_Light_Rainbow_t155_0_0_0/* byval_arg */
	, &CameraFilterPack_Light_Rainbow_t155_1_0_0/* this_arg */
	, &CameraFilterPack_Light_Rainbow_t155_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Light_Rainbow_t155)/* instance_size */
	, sizeof (CameraFilterPack_Light_Rainbow_t155)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Light_Rainbow_t155_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Light_Rainbow2
#include "AssemblyU2DCSharp_CameraFilterPack_Light_Rainbow2.h"
// Metadata Definition CameraFilterPack_Light_Rainbow2
extern TypeInfo CameraFilterPack_Light_Rainbow2_t156_il2cpp_TypeInfo;
// CameraFilterPack_Light_Rainbow2
#include "AssemblyU2DCSharp_CameraFilterPack_Light_Rainbow2MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Light_Rainbow2_t156_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Light_Rainbow2_t156_0_0_0;
extern const Il2CppType CameraFilterPack_Light_Rainbow2_t156_1_0_0;
struct CameraFilterPack_Light_Rainbow2_t156;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Light_Rainbow2_t156_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Light_Rainbow2_t156_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1288/* fieldStart */
	, 1008/* methodStart */
	, -1/* eventStart */
	, 145/* propertyStart */

};
TypeInfo CameraFilterPack_Light_Rainbow2_t156_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Light_Rainbow2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Light_Rainbow2_t156_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 446/* custom_attributes_cache */
	, &CameraFilterPack_Light_Rainbow2_t156_0_0_0/* byval_arg */
	, &CameraFilterPack_Light_Rainbow2_t156_1_0_0/* this_arg */
	, &CameraFilterPack_Light_Rainbow2_t156_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Light_Rainbow2_t156)/* instance_size */
	, sizeof (CameraFilterPack_Light_Rainbow2_t156)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Light_Rainbow2_t156_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Light_Water
#include "AssemblyU2DCSharp_CameraFilterPack_Light_Water.h"
// Metadata Definition CameraFilterPack_Light_Water
extern TypeInfo CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo;
// CameraFilterPack_Light_Water
#include "AssemblyU2DCSharp_CameraFilterPack_Light_WaterMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Light_Water_t157_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Light_Water_t157_0_0_0;
extern const Il2CppType CameraFilterPack_Light_Water_t157_1_0_0;
struct CameraFilterPack_Light_Water_t157;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Light_Water_t157_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Light_Water_t157_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1294/* fieldStart */
	, 1015/* methodStart */
	, -1/* eventStart */
	, 146/* propertyStart */

};
TypeInfo CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Light_Water"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 448/* custom_attributes_cache */
	, &CameraFilterPack_Light_Water_t157_0_0_0/* byval_arg */
	, &CameraFilterPack_Light_Water_t157_1_0_0/* this_arg */
	, &CameraFilterPack_Light_Water_t157_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Light_Water_t157)/* instance_size */
	, sizeof (CameraFilterPack_Light_Water_t157)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Light_Water_t157_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Light_Water2
#include "AssemblyU2DCSharp_CameraFilterPack_Light_Water2.h"
// Metadata Definition CameraFilterPack_Light_Water2
extern TypeInfo CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo;
// CameraFilterPack_Light_Water2
#include "AssemblyU2DCSharp_CameraFilterPack_Light_Water2MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Light_Water2_t158_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Light_Water2_t158_0_0_0;
extern const Il2CppType CameraFilterPack_Light_Water2_t158_1_0_0;
struct CameraFilterPack_Light_Water2_t158;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Light_Water2_t158_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Light_Water2_t158_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1305/* fieldStart */
	, 1022/* methodStart */
	, -1/* eventStart */
	, 147/* propertyStart */

};
TypeInfo CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Light_Water2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 453/* custom_attributes_cache */
	, &CameraFilterPack_Light_Water2_t158_0_0_0/* byval_arg */
	, &CameraFilterPack_Light_Water2_t158_1_0_0/* this_arg */
	, &CameraFilterPack_Light_Water2_t158_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Light_Water2_t158)/* instance_size */
	, sizeof (CameraFilterPack_Light_Water2_t158)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Light_Water2_t158_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_NightVisionFX
#include "AssemblyU2DCSharp_CameraFilterPack_NightVisionFX.h"
// Metadata Definition CameraFilterPack_NightVisionFX
extern TypeInfo CameraFilterPack_NightVisionFX_t160_il2cpp_TypeInfo;
// CameraFilterPack_NightVisionFX
#include "AssemblyU2DCSharp_CameraFilterPack_NightVisionFXMethodDeclarations.h"
extern const Il2CppType preset_t159_0_0_0;
static const Il2CppType* CameraFilterPack_NightVisionFX_t160_il2cpp_TypeInfo__nestedTypes[1] =
{
	&preset_t159_0_0_0,
};
static const EncodedMethodIndex CameraFilterPack_NightVisionFX_t160_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_NightVisionFX_t160_0_0_0;
extern const Il2CppType CameraFilterPack_NightVisionFX_t160_1_0_0;
struct CameraFilterPack_NightVisionFX_t160;
const Il2CppTypeDefinitionMetadata CameraFilterPack_NightVisionFX_t160_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CameraFilterPack_NightVisionFX_t160_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_NightVisionFX_t160_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1317/* fieldStart */
	, 1029/* methodStart */
	, -1/* eventStart */
	, 148/* propertyStart */

};
TypeInfo CameraFilterPack_NightVisionFX_t160_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_NightVisionFX"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_NightVisionFX_t160_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 458/* custom_attributes_cache */
	, &CameraFilterPack_NightVisionFX_t160_0_0_0/* byval_arg */
	, &CameraFilterPack_NightVisionFX_t160_1_0_0/* this_arg */
	, &CameraFilterPack_NightVisionFX_t160_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_NightVisionFX_t160)/* instance_size */
	, sizeof (CameraFilterPack_NightVisionFX_t160)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 21/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_NightVisionFX/preset
#include "AssemblyU2DCSharp_CameraFilterPack_NightVisionFX_preset.h"
// Metadata Definition CameraFilterPack_NightVisionFX/preset
extern TypeInfo preset_t159_il2cpp_TypeInfo;
// CameraFilterPack_NightVisionFX/preset
#include "AssemblyU2DCSharp_CameraFilterPack_NightVisionFX_presetMethodDeclarations.h"
static const EncodedMethodIndex preset_t159_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair preset_t159_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType preset_t159_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata preset_t159_DefinitionMetadata = 
{
	&CameraFilterPack_NightVisionFX_t160_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, preset_t159_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, preset_t159_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1338/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo preset_t159_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "preset"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &preset_t159_0_0_0/* byval_arg */
	, &preset_t159_1_0_0/* this_arg */
	, &preset_t159_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (preset_t159)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (preset_t159)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// CameraFilterPack_NightVision_4
#include "AssemblyU2DCSharp_CameraFilterPack_NightVision_4.h"
// Metadata Definition CameraFilterPack_NightVision_4
extern TypeInfo CameraFilterPack_NightVision_4_t161_il2cpp_TypeInfo;
// CameraFilterPack_NightVision_4
#include "AssemblyU2DCSharp_CameraFilterPack_NightVision_4MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_NightVision_4_t161_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_NightVision_4_t161_0_0_0;
extern const Il2CppType CameraFilterPack_NightVision_4_t161_1_0_0;
struct CameraFilterPack_NightVision_4_t161;
const Il2CppTypeDefinitionMetadata CameraFilterPack_NightVision_4_t161_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_NightVision_4_t161_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1350/* fieldStart */
	, 1035/* methodStart */
	, -1/* eventStart */
	, 149/* propertyStart */

};
TypeInfo CameraFilterPack_NightVision_4_t161_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_NightVision_4"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_NightVision_4_t161_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 474/* custom_attributes_cache */
	, &CameraFilterPack_NightVision_4_t161_0_0_0/* byval_arg */
	, &CameraFilterPack_NightVision_4_t161_1_0_0/* this_arg */
	, &CameraFilterPack_NightVision_4_t161_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_NightVision_4_t161)/* instance_size */
	, sizeof (CameraFilterPack_NightVision_4_t161)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Oculus_NightVision1
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVision1.h"
// Metadata Definition CameraFilterPack_Oculus_NightVision1
extern TypeInfo CameraFilterPack_Oculus_NightVision1_t162_il2cpp_TypeInfo;
// CameraFilterPack_Oculus_NightVision1
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVision1MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Oculus_NightVision1_t162_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Oculus_NightVision1_t162_0_0_0;
extern const Il2CppType CameraFilterPack_Oculus_NightVision1_t162_1_0_0;
struct CameraFilterPack_Oculus_NightVision1_t162;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Oculus_NightVision1_t162_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Oculus_NightVision1_t162_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1358/* fieldStart */
	, 1043/* methodStart */
	, -1/* eventStart */
	, 150/* propertyStart */

};
TypeInfo CameraFilterPack_Oculus_NightVision1_t162_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Oculus_NightVision1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Oculus_NightVision1_t162_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 476/* custom_attributes_cache */
	, &CameraFilterPack_Oculus_NightVision1_t162_0_0_0/* byval_arg */
	, &CameraFilterPack_Oculus_NightVision1_t162_1_0_0/* this_arg */
	, &CameraFilterPack_Oculus_NightVision1_t162_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Oculus_NightVision1_t162)/* instance_size */
	, sizeof (CameraFilterPack_Oculus_NightVision1_t162)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Oculus_NightVision1_t162_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Oculus_NightVision2
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVision2.h"
// Metadata Definition CameraFilterPack_Oculus_NightVision2
extern TypeInfo CameraFilterPack_Oculus_NightVision2_t163_il2cpp_TypeInfo;
// CameraFilterPack_Oculus_NightVision2
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVision2MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Oculus_NightVision2_t163_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Oculus_NightVision2_t163_0_0_0;
extern const Il2CppType CameraFilterPack_Oculus_NightVision2_t163_1_0_0;
struct CameraFilterPack_Oculus_NightVision2_t163;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Oculus_NightVision2_t163_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Oculus_NightVision2_t163_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1367/* fieldStart */
	, 1049/* methodStart */
	, -1/* eventStart */
	, 151/* propertyStart */

};
TypeInfo CameraFilterPack_Oculus_NightVision2_t163_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Oculus_NightVision2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Oculus_NightVision2_t163_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 479/* custom_attributes_cache */
	, &CameraFilterPack_Oculus_NightVision2_t163_0_0_0/* byval_arg */
	, &CameraFilterPack_Oculus_NightVision2_t163_1_0_0/* this_arg */
	, &CameraFilterPack_Oculus_NightVision2_t163_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Oculus_NightVision2_t163)/* instance_size */
	, sizeof (CameraFilterPack_Oculus_NightVision2_t163)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Oculus_NightVision3
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVision3.h"
// Metadata Definition CameraFilterPack_Oculus_NightVision3
extern TypeInfo CameraFilterPack_Oculus_NightVision3_t164_il2cpp_TypeInfo;
// CameraFilterPack_Oculus_NightVision3
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVision3MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Oculus_NightVision3_t164_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Oculus_NightVision3_t164_0_0_0;
extern const Il2CppType CameraFilterPack_Oculus_NightVision3_t164_1_0_0;
struct CameraFilterPack_Oculus_NightVision3_t164;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Oculus_NightVision3_t164_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Oculus_NightVision3_t164_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1375/* fieldStart */
	, 1057/* methodStart */
	, -1/* eventStart */
	, 152/* propertyStart */

};
TypeInfo CameraFilterPack_Oculus_NightVision3_t164_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Oculus_NightVision3"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Oculus_NightVision3_t164_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 481/* custom_attributes_cache */
	, &CameraFilterPack_Oculus_NightVision3_t164_0_0_0/* byval_arg */
	, &CameraFilterPack_Oculus_NightVision3_t164_1_0_0/* this_arg */
	, &CameraFilterPack_Oculus_NightVision3_t164_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Oculus_NightVision3_t164)/* instance_size */
	, sizeof (CameraFilterPack_Oculus_NightVision3_t164)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Oculus_NightVision3_t164_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Oculus_NightVision5
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVision5.h"
// Metadata Definition CameraFilterPack_Oculus_NightVision5
extern TypeInfo CameraFilterPack_Oculus_NightVision5_t165_il2cpp_TypeInfo;
// CameraFilterPack_Oculus_NightVision5
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVision5MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Oculus_NightVision5_t165_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Oculus_NightVision5_t165_0_0_0;
extern const Il2CppType CameraFilterPack_Oculus_NightVision5_t165_1_0_0;
struct CameraFilterPack_Oculus_NightVision5_t165;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Oculus_NightVision5_t165_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Oculus_NightVision5_t165_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1383/* fieldStart */
	, 1064/* methodStart */
	, -1/* eventStart */
	, 153/* propertyStart */

};
TypeInfo CameraFilterPack_Oculus_NightVision5_t165_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Oculus_NightVision5"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Oculus_NightVision5_t165_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 483/* custom_attributes_cache */
	, &CameraFilterPack_Oculus_NightVision5_t165_0_0_0/* byval_arg */
	, &CameraFilterPack_Oculus_NightVision5_t165_1_0_0/* this_arg */
	, &CameraFilterPack_Oculus_NightVision5_t165_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Oculus_NightVision5_t165)/* instance_size */
	, sizeof (CameraFilterPack_Oculus_NightVision5_t165)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Oculus_ThermaVision
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_ThermaVision.h"
// Metadata Definition CameraFilterPack_Oculus_ThermaVision
extern TypeInfo CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo;
// CameraFilterPack_Oculus_ThermaVision
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_ThermaVisionMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Oculus_ThermaVision_t166_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Oculus_ThermaVision_t166_0_0_0;
extern const Il2CppType CameraFilterPack_Oculus_ThermaVision_t166_1_0_0;
struct CameraFilterPack_Oculus_ThermaVision_t166;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Oculus_ThermaVision_t166_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Oculus_ThermaVision_t166_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1394/* fieldStart */
	, 1072/* methodStart */
	, -1/* eventStart */
	, 154/* propertyStart */

};
TypeInfo CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Oculus_ThermaVision"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 488/* custom_attributes_cache */
	, &CameraFilterPack_Oculus_ThermaVision_t166_0_0_0/* byval_arg */
	, &CameraFilterPack_Oculus_ThermaVision_t166_1_0_0/* this_arg */
	, &CameraFilterPack_Oculus_ThermaVision_t166_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Oculus_ThermaVision_t166)/* instance_size */
	, sizeof (CameraFilterPack_Oculus_ThermaVision_t166)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Oculus_ThermaVision_t166_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_OldFilm_Cutting1
#include "AssemblyU2DCSharp_CameraFilterPack_OldFilm_Cutting1.h"
// Metadata Definition CameraFilterPack_OldFilm_Cutting1
extern TypeInfo CameraFilterPack_OldFilm_Cutting1_t167_il2cpp_TypeInfo;
// CameraFilterPack_OldFilm_Cutting1
#include "AssemblyU2DCSharp_CameraFilterPack_OldFilm_Cutting1MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_OldFilm_Cutting1_t167_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_OldFilm_Cutting1_t167_0_0_0;
extern const Il2CppType CameraFilterPack_OldFilm_Cutting1_t167_1_0_0;
struct CameraFilterPack_OldFilm_Cutting1_t167;
const Il2CppTypeDefinitionMetadata CameraFilterPack_OldFilm_Cutting1_t167_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_OldFilm_Cutting1_t167_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1406/* fieldStart */
	, 1079/* methodStart */
	, -1/* eventStart */
	, 155/* propertyStart */

};
TypeInfo CameraFilterPack_OldFilm_Cutting1_t167_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_OldFilm_Cutting1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_OldFilm_Cutting1_t167_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 493/* custom_attributes_cache */
	, &CameraFilterPack_OldFilm_Cutting1_t167_0_0_0/* byval_arg */
	, &CameraFilterPack_OldFilm_Cutting1_t167_1_0_0/* this_arg */
	, &CameraFilterPack_OldFilm_Cutting1_t167_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_OldFilm_Cutting1_t167)/* instance_size */
	, sizeof (CameraFilterPack_OldFilm_Cutting1_t167)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_OldFilm_Cutting2
#include "AssemblyU2DCSharp_CameraFilterPack_OldFilm_Cutting2.h"
// Metadata Definition CameraFilterPack_OldFilm_Cutting2
extern TypeInfo CameraFilterPack_OldFilm_Cutting2_t168_il2cpp_TypeInfo;
// CameraFilterPack_OldFilm_Cutting2
#include "AssemblyU2DCSharp_CameraFilterPack_OldFilm_Cutting2MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_OldFilm_Cutting2_t168_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_OldFilm_Cutting2_t168_0_0_0;
extern const Il2CppType CameraFilterPack_OldFilm_Cutting2_t168_1_0_0;
struct CameraFilterPack_OldFilm_Cutting2_t168;
const Il2CppTypeDefinitionMetadata CameraFilterPack_OldFilm_Cutting2_t168_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_OldFilm_Cutting2_t168_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1414/* fieldStart */
	, 1085/* methodStart */
	, -1/* eventStart */
	, 156/* propertyStart */

};
TypeInfo CameraFilterPack_OldFilm_Cutting2_t168_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_OldFilm_Cutting2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_OldFilm_Cutting2_t168_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 498/* custom_attributes_cache */
	, &CameraFilterPack_OldFilm_Cutting2_t168_0_0_0/* byval_arg */
	, &CameraFilterPack_OldFilm_Cutting2_t168_1_0_0/* this_arg */
	, &CameraFilterPack_OldFilm_Cutting2_t168_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_OldFilm_Cutting2_t168)/* instance_size */
	, sizeof (CameraFilterPack_OldFilm_Cutting2_t168)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Pixel_Pixelisation
#include "AssemblyU2DCSharp_CameraFilterPack_Pixel_Pixelisation.h"
// Metadata Definition CameraFilterPack_Pixel_Pixelisation
extern TypeInfo CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo;
// CameraFilterPack_Pixel_Pixelisation
#include "AssemblyU2DCSharp_CameraFilterPack_Pixel_PixelisationMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Pixel_Pixelisation_t169_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Pixel_Pixelisation_t169_0_0_0;
extern const Il2CppType CameraFilterPack_Pixel_Pixelisation_t169_1_0_0;
struct CameraFilterPack_Pixel_Pixelisation_t169;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Pixel_Pixelisation_t169_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Pixel_Pixelisation_t169_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1422/* fieldStart */
	, 1091/* methodStart */
	, -1/* eventStart */
	, 157/* propertyStart */

};
TypeInfo CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Pixel_Pixelisation"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 503/* custom_attributes_cache */
	, &CameraFilterPack_Pixel_Pixelisation_t169_0_0_0/* byval_arg */
	, &CameraFilterPack_Pixel_Pixelisation_t169_1_0_0/* this_arg */
	, &CameraFilterPack_Pixel_Pixelisation_t169_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Pixel_Pixelisation_t169)/* instance_size */
	, sizeof (CameraFilterPack_Pixel_Pixelisation_t169)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Pixel_Pixelisation_t169_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Pixelisation_Dot
#include "AssemblyU2DCSharp_CameraFilterPack_Pixelisation_Dot.h"
// Metadata Definition CameraFilterPack_Pixelisation_Dot
extern TypeInfo CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo;
// CameraFilterPack_Pixelisation_Dot
#include "AssemblyU2DCSharp_CameraFilterPack_Pixelisation_DotMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Pixelisation_Dot_t170_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Pixelisation_Dot_t170_0_0_0;
extern const Il2CppType CameraFilterPack_Pixelisation_Dot_t170_1_0_0;
struct CameraFilterPack_Pixelisation_Dot_t170;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Pixelisation_Dot_t170_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Pixelisation_Dot_t170_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1430/* fieldStart */
	, 1098/* methodStart */
	, -1/* eventStart */
	, 158/* propertyStart */

};
TypeInfo CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Pixelisation_Dot"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 507/* custom_attributes_cache */
	, &CameraFilterPack_Pixelisation_Dot_t170_0_0_0/* byval_arg */
	, &CameraFilterPack_Pixelisation_Dot_t170_1_0_0/* this_arg */
	, &CameraFilterPack_Pixelisation_Dot_t170_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Pixelisation_Dot_t170)/* instance_size */
	, sizeof (CameraFilterPack_Pixelisation_Dot_t170)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Pixelisation_Dot_t170_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Pixelisation_OilPaint
#include "AssemblyU2DCSharp_CameraFilterPack_Pixelisation_OilPaint.h"
// Metadata Definition CameraFilterPack_Pixelisation_OilPaint
extern TypeInfo CameraFilterPack_Pixelisation_OilPaint_t171_il2cpp_TypeInfo;
// CameraFilterPack_Pixelisation_OilPaint
#include "AssemblyU2DCSharp_CameraFilterPack_Pixelisation_OilPaintMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Pixelisation_OilPaint_t171_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Pixelisation_OilPaint_t171_0_0_0;
extern const Il2CppType CameraFilterPack_Pixelisation_OilPaint_t171_1_0_0;
struct CameraFilterPack_Pixelisation_OilPaint_t171;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Pixelisation_OilPaint_t171_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Pixelisation_OilPaint_t171_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1442/* fieldStart */
	, 1105/* methodStart */
	, -1/* eventStart */
	, 159/* propertyStart */

};
TypeInfo CameraFilterPack_Pixelisation_OilPaint_t171_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Pixelisation_OilPaint"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Pixelisation_OilPaint_t171_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 512/* custom_attributes_cache */
	, &CameraFilterPack_Pixelisation_OilPaint_t171_0_0_0/* byval_arg */
	, &CameraFilterPack_Pixelisation_OilPaint_t171_1_0_0/* this_arg */
	, &CameraFilterPack_Pixelisation_OilPaint_t171_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Pixelisation_OilPaint_t171)/* instance_size */
	, sizeof (CameraFilterPack_Pixelisation_OilPaint_t171)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Pixelisation_OilPaint_t171_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Pixelisation_OilPaintHQ
#include "AssemblyU2DCSharp_CameraFilterPack_Pixelisation_OilPaintHQ.h"
// Metadata Definition CameraFilterPack_Pixelisation_OilPaintHQ
extern TypeInfo CameraFilterPack_Pixelisation_OilPaintHQ_t172_il2cpp_TypeInfo;
// CameraFilterPack_Pixelisation_OilPaintHQ
#include "AssemblyU2DCSharp_CameraFilterPack_Pixelisation_OilPaintHQMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Pixelisation_OilPaintHQ_t172_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Pixelisation_OilPaintHQ_t172_0_0_0;
extern const Il2CppType CameraFilterPack_Pixelisation_OilPaintHQ_t172_1_0_0;
struct CameraFilterPack_Pixelisation_OilPaintHQ_t172;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Pixelisation_OilPaintHQ_t172_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Pixelisation_OilPaintHQ_t172_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1448/* fieldStart */
	, 1112/* methodStart */
	, -1/* eventStart */
	, 160/* propertyStart */

};
TypeInfo CameraFilterPack_Pixelisation_OilPaintHQ_t172_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Pixelisation_OilPaintHQ"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Pixelisation_OilPaintHQ_t172_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 514/* custom_attributes_cache */
	, &CameraFilterPack_Pixelisation_OilPaintHQ_t172_0_0_0/* byval_arg */
	, &CameraFilterPack_Pixelisation_OilPaintHQ_t172_1_0_0/* this_arg */
	, &CameraFilterPack_Pixelisation_OilPaintHQ_t172_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Pixelisation_OilPaintHQ_t172)/* instance_size */
	, sizeof (CameraFilterPack_Pixelisation_OilPaintHQ_t172)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Pixelisation_OilPaintHQ_t172_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Sharpen_Sharpen
#include "AssemblyU2DCSharp_CameraFilterPack_Sharpen_Sharpen.h"
// Metadata Definition CameraFilterPack_Sharpen_Sharpen
extern TypeInfo CameraFilterPack_Sharpen_Sharpen_t173_il2cpp_TypeInfo;
// CameraFilterPack_Sharpen_Sharpen
#include "AssemblyU2DCSharp_CameraFilterPack_Sharpen_SharpenMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Sharpen_Sharpen_t173_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Sharpen_Sharpen_t173_0_0_0;
extern const Il2CppType CameraFilterPack_Sharpen_Sharpen_t173_1_0_0;
struct CameraFilterPack_Sharpen_Sharpen_t173;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Sharpen_Sharpen_t173_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Sharpen_Sharpen_t173_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1454/* fieldStart */
	, 1119/* methodStart */
	, -1/* eventStart */
	, 161/* propertyStart */

};
TypeInfo CameraFilterPack_Sharpen_Sharpen_t173_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Sharpen_Sharpen"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Sharpen_Sharpen_t173_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 516/* custom_attributes_cache */
	, &CameraFilterPack_Sharpen_Sharpen_t173_0_0_0/* byval_arg */
	, &CameraFilterPack_Sharpen_Sharpen_t173_1_0_0/* this_arg */
	, &CameraFilterPack_Sharpen_Sharpen_t173_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Sharpen_Sharpen_t173)/* instance_size */
	, sizeof (CameraFilterPack_Sharpen_Sharpen_t173)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Sharpen_Sharpen_t173_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Special_Bubble
#include "AssemblyU2DCSharp_CameraFilterPack_Special_Bubble.h"
// Metadata Definition CameraFilterPack_Special_Bubble
extern TypeInfo CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo;
// CameraFilterPack_Special_Bubble
#include "AssemblyU2DCSharp_CameraFilterPack_Special_BubbleMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Special_Bubble_t174_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Special_Bubble_t174_0_0_0;
extern const Il2CppType CameraFilterPack_Special_Bubble_t174_1_0_0;
struct CameraFilterPack_Special_Bubble_t174;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Special_Bubble_t174_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Special_Bubble_t174_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1460/* fieldStart */
	, 1126/* methodStart */
	, -1/* eventStart */
	, 162/* propertyStart */

};
TypeInfo CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Special_Bubble"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 518/* custom_attributes_cache */
	, &CameraFilterPack_Special_Bubble_t174_0_0_0/* byval_arg */
	, &CameraFilterPack_Special_Bubble_t174_1_0_0/* this_arg */
	, &CameraFilterPack_Special_Bubble_t174_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Special_Bubble_t174)/* instance_size */
	, sizeof (CameraFilterPack_Special_Bubble_t174)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Special_Bubble_t174_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_50
#include "AssemblyU2DCSharp_CameraFilterPack_TV_50.h"
// Metadata Definition CameraFilterPack_TV_50
extern TypeInfo CameraFilterPack_TV_50_t175_il2cpp_TypeInfo;
// CameraFilterPack_TV_50
#include "AssemblyU2DCSharp_CameraFilterPack_TV_50MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_50_t175_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_50_t175_0_0_0;
extern const Il2CppType CameraFilterPack_TV_50_t175_1_0_0;
struct CameraFilterPack_TV_50_t175;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_50_t175_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_50_t175_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1472/* fieldStart */
	, 1133/* methodStart */
	, -1/* eventStart */
	, 163/* propertyStart */

};
TypeInfo CameraFilterPack_TV_50_t175_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_50"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_50_t175_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 523/* custom_attributes_cache */
	, &CameraFilterPack_TV_50_t175_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_50_t175_1_0_0/* this_arg */
	, &CameraFilterPack_TV_50_t175_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_50_t175)/* instance_size */
	, sizeof (CameraFilterPack_TV_50_t175)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_80
#include "AssemblyU2DCSharp_CameraFilterPack_TV_80.h"
// Metadata Definition CameraFilterPack_TV_80
extern TypeInfo CameraFilterPack_TV_80_t176_il2cpp_TypeInfo;
// CameraFilterPack_TV_80
#include "AssemblyU2DCSharp_CameraFilterPack_TV_80MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_80_t176_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_80_t176_0_0_0;
extern const Il2CppType CameraFilterPack_TV_80_t176_1_0_0;
struct CameraFilterPack_TV_80_t176;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_80_t176_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_80_t176_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1476/* fieldStart */
	, 1139/* methodStart */
	, -1/* eventStart */
	, 164/* propertyStart */

};
TypeInfo CameraFilterPack_TV_80_t176_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_80"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_80_t176_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 524/* custom_attributes_cache */
	, &CameraFilterPack_TV_80_t176_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_80_t176_1_0_0/* this_arg */
	, &CameraFilterPack_TV_80_t176_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_80_t176)/* instance_size */
	, sizeof (CameraFilterPack_TV_80_t176)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_ARCADE
#include "AssemblyU2DCSharp_CameraFilterPack_TV_ARCADE.h"
// Metadata Definition CameraFilterPack_TV_ARCADE
extern TypeInfo CameraFilterPack_TV_ARCADE_t177_il2cpp_TypeInfo;
// CameraFilterPack_TV_ARCADE
#include "AssemblyU2DCSharp_CameraFilterPack_TV_ARCADEMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_ARCADE_t177_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_ARCADE_t177_0_0_0;
extern const Il2CppType CameraFilterPack_TV_ARCADE_t177_1_0_0;
struct CameraFilterPack_TV_ARCADE_t177;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_ARCADE_t177_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_ARCADE_t177_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1480/* fieldStart */
	, 1145/* methodStart */
	, -1/* eventStart */
	, 165/* propertyStart */

};
TypeInfo CameraFilterPack_TV_ARCADE_t177_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_ARCADE"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_ARCADE_t177_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 525/* custom_attributes_cache */
	, &CameraFilterPack_TV_ARCADE_t177_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_ARCADE_t177_1_0_0/* this_arg */
	, &CameraFilterPack_TV_ARCADE_t177_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_ARCADE_t177)/* instance_size */
	, sizeof (CameraFilterPack_TV_ARCADE_t177)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_ARCADE_2
#include "AssemblyU2DCSharp_CameraFilterPack_TV_ARCADE_2.h"
// Metadata Definition CameraFilterPack_TV_ARCADE_2
extern TypeInfo CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo;
// CameraFilterPack_TV_ARCADE_2
#include "AssemblyU2DCSharp_CameraFilterPack_TV_ARCADE_2MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_ARCADE_2_t178_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_ARCADE_2_t178_0_0_0;
extern const Il2CppType CameraFilterPack_TV_ARCADE_2_t178_1_0_0;
struct CameraFilterPack_TV_ARCADE_2_t178;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_ARCADE_2_t178_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_ARCADE_2_t178_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1484/* fieldStart */
	, 1151/* methodStart */
	, -1/* eventStart */
	, 166/* propertyStart */

};
TypeInfo CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_ARCADE_2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 526/* custom_attributes_cache */
	, &CameraFilterPack_TV_ARCADE_2_t178_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_ARCADE_2_t178_1_0_0/* this_arg */
	, &CameraFilterPack_TV_ARCADE_2_t178_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_ARCADE_2_t178)/* instance_size */
	, sizeof (CameraFilterPack_TV_ARCADE_2_t178)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_ARCADE_2_t178_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_Artefact
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Artefact.h"
// Metadata Definition CameraFilterPack_TV_Artefact
extern TypeInfo CameraFilterPack_TV_Artefact_t179_il2cpp_TypeInfo;
// CameraFilterPack_TV_Artefact
#include "AssemblyU2DCSharp_CameraFilterPack_TV_ArtefactMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_Artefact_t179_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_Artefact_t179_0_0_0;
extern const Il2CppType CameraFilterPack_TV_Artefact_t179_1_0_0;
struct CameraFilterPack_TV_Artefact_t179;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_Artefact_t179_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_Artefact_t179_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1496/* fieldStart */
	, 1158/* methodStart */
	, -1/* eventStart */
	, 167/* propertyStart */

};
TypeInfo CameraFilterPack_TV_Artefact_t179_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_Artefact"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_Artefact_t179_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 531/* custom_attributes_cache */
	, &CameraFilterPack_TV_Artefact_t179_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_Artefact_t179_1_0_0/* this_arg */
	, &CameraFilterPack_TV_Artefact_t179_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_Artefact_t179)/* instance_size */
	, sizeof (CameraFilterPack_TV_Artefact_t179)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_BrokenGlass
#include "AssemblyU2DCSharp_CameraFilterPack_TV_BrokenGlass.h"
// Metadata Definition CameraFilterPack_TV_BrokenGlass
extern TypeInfo CameraFilterPack_TV_BrokenGlass_t180_il2cpp_TypeInfo;
// CameraFilterPack_TV_BrokenGlass
#include "AssemblyU2DCSharp_CameraFilterPack_TV_BrokenGlassMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_BrokenGlass_t180_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_BrokenGlass_t180_0_0_0;
extern const Il2CppType CameraFilterPack_TV_BrokenGlass_t180_1_0_0;
struct CameraFilterPack_TV_BrokenGlass_t180;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_BrokenGlass_t180_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_BrokenGlass_t180_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1503/* fieldStart */
	, 1164/* methodStart */
	, -1/* eventStart */
	, 168/* propertyStart */

};
TypeInfo CameraFilterPack_TV_BrokenGlass_t180_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_BrokenGlass"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_BrokenGlass_t180_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 535/* custom_attributes_cache */
	, &CameraFilterPack_TV_BrokenGlass_t180_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_BrokenGlass_t180_1_0_0/* this_arg */
	, &CameraFilterPack_TV_BrokenGlass_t180_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_BrokenGlass_t180)/* instance_size */
	, sizeof (CameraFilterPack_TV_BrokenGlass_t180)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_Chromatical
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Chromatical.h"
// Metadata Definition CameraFilterPack_TV_Chromatical
extern TypeInfo CameraFilterPack_TV_Chromatical_t181_il2cpp_TypeInfo;
// CameraFilterPack_TV_Chromatical
#include "AssemblyU2DCSharp_CameraFilterPack_TV_ChromaticalMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_Chromatical_t181_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_Chromatical_t181_0_0_0;
extern const Il2CppType CameraFilterPack_TV_Chromatical_t181_1_0_0;
struct CameraFilterPack_TV_Chromatical_t181;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_Chromatical_t181_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_Chromatical_t181_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1512/* fieldStart */
	, 1170/* methodStart */
	, -1/* eventStart */
	, 169/* propertyStart */

};
TypeInfo CameraFilterPack_TV_Chromatical_t181_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_Chromatical"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_Chromatical_t181_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 541/* custom_attributes_cache */
	, &CameraFilterPack_TV_Chromatical_t181_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_Chromatical_t181_1_0_0/* this_arg */
	, &CameraFilterPack_TV_Chromatical_t181_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_Chromatical_t181)/* instance_size */
	, sizeof (CameraFilterPack_TV_Chromatical_t181)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_Chromatical2
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Chromatical2.h"
// Metadata Definition CameraFilterPack_TV_Chromatical2
extern TypeInfo CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo;
// CameraFilterPack_TV_Chromatical2
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Chromatical2MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_Chromatical2_t182_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_Chromatical2_t182_0_0_0;
extern const Il2CppType CameraFilterPack_TV_Chromatical2_t182_1_0_0;
struct CameraFilterPack_TV_Chromatical2_t182;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_Chromatical2_t182_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_Chromatical2_t182_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1516/* fieldStart */
	, 1176/* methodStart */
	, -1/* eventStart */
	, 170/* propertyStart */

};
TypeInfo CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_Chromatical2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 542/* custom_attributes_cache */
	, &CameraFilterPack_TV_Chromatical2_t182_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_Chromatical2_t182_1_0_0/* this_arg */
	, &CameraFilterPack_TV_Chromatical2_t182_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_Chromatical2_t182)/* instance_size */
	, sizeof (CameraFilterPack_TV_Chromatical2_t182)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_Chromatical2_t182_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_CompressionFX
#include "AssemblyU2DCSharp_CameraFilterPack_TV_CompressionFX.h"
// Metadata Definition CameraFilterPack_TV_CompressionFX
extern TypeInfo CameraFilterPack_TV_CompressionFX_t183_il2cpp_TypeInfo;
// CameraFilterPack_TV_CompressionFX
#include "AssemblyU2DCSharp_CameraFilterPack_TV_CompressionFXMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_CompressionFX_t183_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_CompressionFX_t183_0_0_0;
extern const Il2CppType CameraFilterPack_TV_CompressionFX_t183_1_0_0;
struct CameraFilterPack_TV_CompressionFX_t183;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_CompressionFX_t183_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_CompressionFX_t183_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1528/* fieldStart */
	, 1183/* methodStart */
	, -1/* eventStart */
	, 171/* propertyStart */

};
TypeInfo CameraFilterPack_TV_CompressionFX_t183_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_CompressionFX"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_CompressionFX_t183_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 547/* custom_attributes_cache */
	, &CameraFilterPack_TV_CompressionFX_t183_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_CompressionFX_t183_1_0_0/* this_arg */
	, &CameraFilterPack_TV_CompressionFX_t183_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_CompressionFX_t183)/* instance_size */
	, sizeof (CameraFilterPack_TV_CompressionFX_t183)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_Distorted
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Distorted.h"
// Metadata Definition CameraFilterPack_TV_Distorted
extern TypeInfo CameraFilterPack_TV_Distorted_t184_il2cpp_TypeInfo;
// CameraFilterPack_TV_Distorted
#include "AssemblyU2DCSharp_CameraFilterPack_TV_DistortedMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_Distorted_t184_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_Distorted_t184_0_0_0;
extern const Il2CppType CameraFilterPack_TV_Distorted_t184_1_0_0;
struct CameraFilterPack_TV_Distorted_t184;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_Distorted_t184_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_Distorted_t184_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1533/* fieldStart */
	, 1189/* methodStart */
	, -1/* eventStart */
	, 172/* propertyStart */

};
TypeInfo CameraFilterPack_TV_Distorted_t184_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_Distorted"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_Distorted_t184_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 549/* custom_attributes_cache */
	, &CameraFilterPack_TV_Distorted_t184_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_Distorted_t184_1_0_0/* this_arg */
	, &CameraFilterPack_TV_Distorted_t184_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_Distorted_t184)/* instance_size */
	, sizeof (CameraFilterPack_TV_Distorted_t184)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_Distorted_t184_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_LED
#include "AssemblyU2DCSharp_CameraFilterPack_TV_LED.h"
// Metadata Definition CameraFilterPack_TV_LED
extern TypeInfo CameraFilterPack_TV_LED_t185_il2cpp_TypeInfo;
// CameraFilterPack_TV_LED
#include "AssemblyU2DCSharp_CameraFilterPack_TV_LEDMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_LED_t185_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_LED_t185_0_0_0;
extern const Il2CppType CameraFilterPack_TV_LED_t185_1_0_0;
struct CameraFilterPack_TV_LED_t185;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_LED_t185_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_LED_t185_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1539/* fieldStart */
	, 1196/* methodStart */
	, -1/* eventStart */
	, 173/* propertyStart */

};
TypeInfo CameraFilterPack_TV_LED_t185_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_LED"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_LED_t185_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 552/* custom_attributes_cache */
	, &CameraFilterPack_TV_LED_t185_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_LED_t185_1_0_0/* this_arg */
	, &CameraFilterPack_TV_LED_t185_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_LED_t185)/* instance_size */
	, sizeof (CameraFilterPack_TV_LED_t185)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_Noise
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Noise.h"
// Metadata Definition CameraFilterPack_TV_Noise
extern TypeInfo CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo;
// CameraFilterPack_TV_Noise
#include "AssemblyU2DCSharp_CameraFilterPack_TV_NoiseMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_Noise_t186_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_Noise_t186_0_0_0;
extern const Il2CppType CameraFilterPack_TV_Noise_t186_1_0_0;
struct CameraFilterPack_TV_Noise_t186;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_Noise_t186_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_Noise_t186_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1545/* fieldStart */
	, 1202/* methodStart */
	, -1/* eventStart */
	, 174/* propertyStart */

};
TypeInfo CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_Noise"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 555/* custom_attributes_cache */
	, &CameraFilterPack_TV_Noise_t186_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_Noise_t186_1_0_0/* this_arg */
	, &CameraFilterPack_TV_Noise_t186_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_Noise_t186)/* instance_size */
	, sizeof (CameraFilterPack_TV_Noise_t186)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_Noise_t186_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_Old
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Old.h"
// Metadata Definition CameraFilterPack_TV_Old
extern TypeInfo CameraFilterPack_TV_Old_t187_il2cpp_TypeInfo;
// CameraFilterPack_TV_Old
#include "AssemblyU2DCSharp_CameraFilterPack_TV_OldMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_Old_t187_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_Old_t187_0_0_0;
extern const Il2CppType CameraFilterPack_TV_Old_t187_1_0_0;
struct CameraFilterPack_TV_Old_t187;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_Old_t187_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_Old_t187_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1557/* fieldStart */
	, 1209/* methodStart */
	, -1/* eventStart */
	, 175/* propertyStart */

};
TypeInfo CameraFilterPack_TV_Old_t187_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_Old"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_Old_t187_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 560/* custom_attributes_cache */
	, &CameraFilterPack_TV_Old_t187_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_Old_t187_1_0_0/* this_arg */
	, &CameraFilterPack_TV_Old_t187_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_Old_t187)/* instance_size */
	, sizeof (CameraFilterPack_TV_Old_t187)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_Old_t187_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_Old_Movie
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Old_Movie.h"
// Metadata Definition CameraFilterPack_TV_Old_Movie
extern TypeInfo CameraFilterPack_TV_Old_Movie_t188_il2cpp_TypeInfo;
// CameraFilterPack_TV_Old_Movie
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Old_MovieMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_Old_Movie_t188_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_Old_Movie_t188_0_0_0;
extern const Il2CppType CameraFilterPack_TV_Old_Movie_t188_1_0_0;
struct CameraFilterPack_TV_Old_Movie_t188;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_Old_Movie_t188_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_Old_Movie_t188_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1562/* fieldStart */
	, 1216/* methodStart */
	, -1/* eventStart */
	, 176/* propertyStart */

};
TypeInfo CameraFilterPack_TV_Old_Movie_t188_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_Old_Movie"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_Old_Movie_t188_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 562/* custom_attributes_cache */
	, &CameraFilterPack_TV_Old_Movie_t188_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_Old_Movie_t188_1_0_0/* this_arg */
	, &CameraFilterPack_TV_Old_Movie_t188_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_Old_Movie_t188)/* instance_size */
	, sizeof (CameraFilterPack_TV_Old_Movie_t188)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_Old_Movie_t188_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_Old_Movie_2
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Old_Movie_2.h"
// Metadata Definition CameraFilterPack_TV_Old_Movie_2
extern TypeInfo CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo;
// CameraFilterPack_TV_Old_Movie_2
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Old_Movie_2MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_Old_Movie_2_t189_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_Old_Movie_2_t189_0_0_0;
extern const Il2CppType CameraFilterPack_TV_Old_Movie_2_t189_1_0_0;
struct CameraFilterPack_TV_Old_Movie_2_t189;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_Old_Movie_2_t189_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_Old_Movie_2_t189_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1567/* fieldStart */
	, 1223/* methodStart */
	, -1/* eventStart */
	, 177/* propertyStart */

};
TypeInfo CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_Old_Movie_2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 564/* custom_attributes_cache */
	, &CameraFilterPack_TV_Old_Movie_2_t189_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_Old_Movie_2_t189_1_0_0/* this_arg */
	, &CameraFilterPack_TV_Old_Movie_2_t189_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_Old_Movie_2_t189)/* instance_size */
	, sizeof (CameraFilterPack_TV_Old_Movie_2_t189)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_Old_Movie_2_t189_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_PlanetMars
#include "AssemblyU2DCSharp_CameraFilterPack_TV_PlanetMars.h"
// Metadata Definition CameraFilterPack_TV_PlanetMars
extern TypeInfo CameraFilterPack_TV_PlanetMars_t190_il2cpp_TypeInfo;
// CameraFilterPack_TV_PlanetMars
#include "AssemblyU2DCSharp_CameraFilterPack_TV_PlanetMarsMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_PlanetMars_t190_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_PlanetMars_t190_0_0_0;
extern const Il2CppType CameraFilterPack_TV_PlanetMars_t190_1_0_0;
struct CameraFilterPack_TV_PlanetMars_t190;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_PlanetMars_t190_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_PlanetMars_t190_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1579/* fieldStart */
	, 1230/* methodStart */
	, -1/* eventStart */
	, 178/* propertyStart */

};
TypeInfo CameraFilterPack_TV_PlanetMars_t190_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_PlanetMars"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_PlanetMars_t190_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 569/* custom_attributes_cache */
	, &CameraFilterPack_TV_PlanetMars_t190_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_PlanetMars_t190_1_0_0/* this_arg */
	, &CameraFilterPack_TV_PlanetMars_t190_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_PlanetMars_t190)/* instance_size */
	, sizeof (CameraFilterPack_TV_PlanetMars_t190)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_Posterize
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Posterize.h"
// Metadata Definition CameraFilterPack_TV_Posterize
extern TypeInfo CameraFilterPack_TV_Posterize_t191_il2cpp_TypeInfo;
// CameraFilterPack_TV_Posterize
#include "AssemblyU2DCSharp_CameraFilterPack_TV_PosterizeMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_Posterize_t191_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_Posterize_t191_0_0_0;
extern const Il2CppType CameraFilterPack_TV_Posterize_t191_1_0_0;
struct CameraFilterPack_TV_Posterize_t191;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_Posterize_t191_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_Posterize_t191_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1584/* fieldStart */
	, 1236/* methodStart */
	, -1/* eventStart */
	, 179/* propertyStart */

};
TypeInfo CameraFilterPack_TV_Posterize_t191_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_Posterize"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_Posterize_t191_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 571/* custom_attributes_cache */
	, &CameraFilterPack_TV_Posterize_t191_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_Posterize_t191_1_0_0/* this_arg */
	, &CameraFilterPack_TV_Posterize_t191_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_Posterize_t191)/* instance_size */
	, sizeof (CameraFilterPack_TV_Posterize_t191)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_Posterize_t191_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_Rgb
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Rgb.h"
// Metadata Definition CameraFilterPack_TV_Rgb
extern TypeInfo CameraFilterPack_TV_Rgb_t192_il2cpp_TypeInfo;
// CameraFilterPack_TV_Rgb
#include "AssemblyU2DCSharp_CameraFilterPack_TV_RgbMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_Rgb_t192_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_Rgb_t192_0_0_0;
extern const Il2CppType CameraFilterPack_TV_Rgb_t192_1_0_0;
struct CameraFilterPack_TV_Rgb_t192;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_Rgb_t192_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_Rgb_t192_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1589/* fieldStart */
	, 1243/* methodStart */
	, -1/* eventStart */
	, 180/* propertyStart */

};
TypeInfo CameraFilterPack_TV_Rgb_t192_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_Rgb"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_Rgb_t192_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 573/* custom_attributes_cache */
	, &CameraFilterPack_TV_Rgb_t192_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_Rgb_t192_1_0_0/* this_arg */
	, &CameraFilterPack_TV_Rgb_t192_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_Rgb_t192)/* instance_size */
	, sizeof (CameraFilterPack_TV_Rgb_t192)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_Tiles
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Tiles.h"
// Metadata Definition CameraFilterPack_TV_Tiles
extern TypeInfo CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo;
// CameraFilterPack_TV_Tiles
#include "AssemblyU2DCSharp_CameraFilterPack_TV_TilesMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_Tiles_t193_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_Tiles_t193_0_0_0;
extern const Il2CppType CameraFilterPack_TV_Tiles_t193_1_0_0;
struct CameraFilterPack_TV_Tiles_t193;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_Tiles_t193_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_Tiles_t193_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1594/* fieldStart */
	, 1249/* methodStart */
	, -1/* eventStart */
	, 181/* propertyStart */

};
TypeInfo CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_Tiles"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 575/* custom_attributes_cache */
	, &CameraFilterPack_TV_Tiles_t193_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_Tiles_t193_1_0_0/* this_arg */
	, &CameraFilterPack_TV_Tiles_t193_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_Tiles_t193)/* instance_size */
	, sizeof (CameraFilterPack_TV_Tiles_t193)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_Tiles_t193_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_VHS
#include "AssemblyU2DCSharp_CameraFilterPack_TV_VHS.h"
// Metadata Definition CameraFilterPack_TV_VHS
extern TypeInfo CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo;
// CameraFilterPack_TV_VHS
#include "AssemblyU2DCSharp_CameraFilterPack_TV_VHSMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_VHS_t194_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_VHS_t194_0_0_0;
extern const Il2CppType CameraFilterPack_TV_VHS_t194_1_0_0;
struct CameraFilterPack_TV_VHS_t194;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_VHS_t194_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_VHS_t194_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1606/* fieldStart */
	, 1256/* methodStart */
	, -1/* eventStart */
	, 182/* propertyStart */

};
TypeInfo CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_VHS"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 580/* custom_attributes_cache */
	, &CameraFilterPack_TV_VHS_t194_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_VHS_t194_1_0_0/* this_arg */
	, &CameraFilterPack_TV_VHS_t194_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_VHS_t194)/* instance_size */
	, sizeof (CameraFilterPack_TV_VHS_t194)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_VHS_t194_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_VHS_Rewind
#include "AssemblyU2DCSharp_CameraFilterPack_TV_VHS_Rewind.h"
// Metadata Definition CameraFilterPack_TV_VHS_Rewind
extern TypeInfo CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo;
// CameraFilterPack_TV_VHS_Rewind
#include "AssemblyU2DCSharp_CameraFilterPack_TV_VHS_RewindMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_VHS_Rewind_t195_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_VHS_Rewind_t195_0_0_0;
extern const Il2CppType CameraFilterPack_TV_VHS_Rewind_t195_1_0_0;
struct CameraFilterPack_TV_VHS_Rewind_t195;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_VHS_Rewind_t195_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_VHS_Rewind_t195_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1618/* fieldStart */
	, 1263/* methodStart */
	, -1/* eventStart */
	, 183/* propertyStart */

};
TypeInfo CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_VHS_Rewind"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 585/* custom_attributes_cache */
	, &CameraFilterPack_TV_VHS_Rewind_t195_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_VHS_Rewind_t195_1_0_0/* this_arg */
	, &CameraFilterPack_TV_VHS_Rewind_t195_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_VHS_Rewind_t195)/* instance_size */
	, sizeof (CameraFilterPack_TV_VHS_Rewind_t195)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_VHS_Rewind_t195_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_Vcr
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Vcr.h"
// Metadata Definition CameraFilterPack_TV_Vcr
extern TypeInfo CameraFilterPack_TV_Vcr_t196_il2cpp_TypeInfo;
// CameraFilterPack_TV_Vcr
#include "AssemblyU2DCSharp_CameraFilterPack_TV_VcrMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_Vcr_t196_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_Vcr_t196_0_0_0;
extern const Il2CppType CameraFilterPack_TV_Vcr_t196_1_0_0;
struct CameraFilterPack_TV_Vcr_t196;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_Vcr_t196_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_Vcr_t196_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1630/* fieldStart */
	, 1270/* methodStart */
	, -1/* eventStart */
	, 184/* propertyStart */

};
TypeInfo CameraFilterPack_TV_Vcr_t196_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_Vcr"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_Vcr_t196_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 590/* custom_attributes_cache */
	, &CameraFilterPack_TV_Vcr_t196_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_Vcr_t196_1_0_0/* this_arg */
	, &CameraFilterPack_TV_Vcr_t196_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_Vcr_t196)/* instance_size */
	, sizeof (CameraFilterPack_TV_Vcr_t196)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_Vcr_t196_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_Video3D
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Video3D.h"
// Metadata Definition CameraFilterPack_TV_Video3D
extern TypeInfo CameraFilterPack_TV_Video3D_t197_il2cpp_TypeInfo;
// CameraFilterPack_TV_Video3D
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Video3DMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_Video3D_t197_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_Video3D_t197_0_0_0;
extern const Il2CppType CameraFilterPack_TV_Video3D_t197_1_0_0;
struct CameraFilterPack_TV_Video3D_t197;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_Video3D_t197_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_Video3D_t197_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1635/* fieldStart */
	, 1277/* methodStart */
	, -1/* eventStart */
	, 185/* propertyStart */

};
TypeInfo CameraFilterPack_TV_Video3D_t197_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_Video3D"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_Video3D_t197_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 592/* custom_attributes_cache */
	, &CameraFilterPack_TV_Video3D_t197_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_Video3D_t197_1_0_0/* this_arg */
	, &CameraFilterPack_TV_Video3D_t197_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_Video3D_t197)/* instance_size */
	, sizeof (CameraFilterPack_TV_Video3D_t197)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_Videoflip
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Videoflip.h"
// Metadata Definition CameraFilterPack_TV_Videoflip
extern TypeInfo CameraFilterPack_TV_Videoflip_t198_il2cpp_TypeInfo;
// CameraFilterPack_TV_Videoflip
#include "AssemblyU2DCSharp_CameraFilterPack_TV_VideoflipMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_Videoflip_t198_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_Videoflip_t198_0_0_0;
extern const Il2CppType CameraFilterPack_TV_Videoflip_t198_1_0_0;
struct CameraFilterPack_TV_Videoflip_t198;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_Videoflip_t198_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_Videoflip_t198_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1639/* fieldStart */
	, 1283/* methodStart */
	, -1/* eventStart */
	, 186/* propertyStart */

};
TypeInfo CameraFilterPack_TV_Videoflip_t198_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_Videoflip"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_Videoflip_t198_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 593/* custom_attributes_cache */
	, &CameraFilterPack_TV_Videoflip_t198_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_Videoflip_t198_1_0_0/* this_arg */
	, &CameraFilterPack_TV_Videoflip_t198_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_Videoflip_t198)/* instance_size */
	, sizeof (CameraFilterPack_TV_Videoflip_t198)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_Vintage
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Vintage.h"
// Metadata Definition CameraFilterPack_TV_Vintage
extern TypeInfo CameraFilterPack_TV_Vintage_t199_il2cpp_TypeInfo;
// CameraFilterPack_TV_Vintage
#include "AssemblyU2DCSharp_CameraFilterPack_TV_VintageMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_Vintage_t199_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_Vintage_t199_0_0_0;
extern const Il2CppType CameraFilterPack_TV_Vintage_t199_1_0_0;
struct CameraFilterPack_TV_Vintage_t199;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_Vintage_t199_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_Vintage_t199_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1643/* fieldStart */
	, 1289/* methodStart */
	, -1/* eventStart */
	, 187/* propertyStart */

};
TypeInfo CameraFilterPack_TV_Vintage_t199_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_Vintage"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_Vintage_t199_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 594/* custom_attributes_cache */
	, &CameraFilterPack_TV_Vintage_t199_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_Vintage_t199_1_0_0/* this_arg */
	, &CameraFilterPack_TV_Vintage_t199_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_Vintage_t199)/* instance_size */
	, sizeof (CameraFilterPack_TV_Vintage_t199)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_Vintage_t199_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_WideScreenCircle
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenCircle.h"
// Metadata Definition CameraFilterPack_TV_WideScreenCircle
extern TypeInfo CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo;
// CameraFilterPack_TV_WideScreenCircle
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenCircleMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_WideScreenCircle_t200_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_WideScreenCircle_t200_0_0_0;
extern const Il2CppType CameraFilterPack_TV_WideScreenCircle_t200_1_0_0;
struct CameraFilterPack_TV_WideScreenCircle_t200;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_WideScreenCircle_t200_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_WideScreenCircle_t200_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1648/* fieldStart */
	, 1296/* methodStart */
	, -1/* eventStart */
	, 188/* propertyStart */

};
TypeInfo CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_WideScreenCircle"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 596/* custom_attributes_cache */
	, &CameraFilterPack_TV_WideScreenCircle_t200_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_WideScreenCircle_t200_1_0_0/* this_arg */
	, &CameraFilterPack_TV_WideScreenCircle_t200_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_WideScreenCircle_t200)/* instance_size */
	, sizeof (CameraFilterPack_TV_WideScreenCircle_t200)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_WideScreenCircle_t200_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_WideScreenHV
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenHV.h"
// Metadata Definition CameraFilterPack_TV_WideScreenHV
extern TypeInfo CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo;
// CameraFilterPack_TV_WideScreenHV
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenHVMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_WideScreenHV_t201_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_WideScreenHV_t201_0_0_0;
extern const Il2CppType CameraFilterPack_TV_WideScreenHV_t201_1_0_0;
struct CameraFilterPack_TV_WideScreenHV_t201;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_WideScreenHV_t201_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_WideScreenHV_t201_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1660/* fieldStart */
	, 1303/* methodStart */
	, -1/* eventStart */
	, 189/* propertyStart */

};
TypeInfo CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_WideScreenHV"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 601/* custom_attributes_cache */
	, &CameraFilterPack_TV_WideScreenHV_t201_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_WideScreenHV_t201_1_0_0/* this_arg */
	, &CameraFilterPack_TV_WideScreenHV_t201_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_WideScreenHV_t201)/* instance_size */
	, sizeof (CameraFilterPack_TV_WideScreenHV_t201)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_WideScreenHV_t201_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_WideScreenHorizontal
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenHorizontal.h"
// Metadata Definition CameraFilterPack_TV_WideScreenHorizontal
extern TypeInfo CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo;
// CameraFilterPack_TV_WideScreenHorizontal
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenHorizontalMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_WideScreenHorizontal_t202_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_WideScreenHorizontal_t202_0_0_0;
extern const Il2CppType CameraFilterPack_TV_WideScreenHorizontal_t202_1_0_0;
struct CameraFilterPack_TV_WideScreenHorizontal_t202;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_WideScreenHorizontal_t202_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_WideScreenHorizontal_t202_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1672/* fieldStart */
	, 1310/* methodStart */
	, -1/* eventStart */
	, 190/* propertyStart */

};
TypeInfo CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_WideScreenHorizontal"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 606/* custom_attributes_cache */
	, &CameraFilterPack_TV_WideScreenHorizontal_t202_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_WideScreenHorizontal_t202_1_0_0/* this_arg */
	, &CameraFilterPack_TV_WideScreenHorizontal_t202_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_WideScreenHorizontal_t202)/* instance_size */
	, sizeof (CameraFilterPack_TV_WideScreenHorizontal_t202)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_WideScreenHorizontal_t202_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_TV_WideScreenVertical
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenVertical.h"
// Metadata Definition CameraFilterPack_TV_WideScreenVertical
extern TypeInfo CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo;
// CameraFilterPack_TV_WideScreenVertical
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenVerticalMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_TV_WideScreenVertical_t203_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_TV_WideScreenVertical_t203_0_0_0;
extern const Il2CppType CameraFilterPack_TV_WideScreenVertical_t203_1_0_0;
struct CameraFilterPack_TV_WideScreenVertical_t203;
const Il2CppTypeDefinitionMetadata CameraFilterPack_TV_WideScreenVertical_t203_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_TV_WideScreenVertical_t203_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1684/* fieldStart */
	, 1317/* methodStart */
	, -1/* eventStart */
	, 191/* propertyStart */

};
TypeInfo CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_TV_WideScreenVertical"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 611/* custom_attributes_cache */
	, &CameraFilterPack_TV_WideScreenVertical_t203_0_0_0/* byval_arg */
	, &CameraFilterPack_TV_WideScreenVertical_t203_1_0_0/* this_arg */
	, &CameraFilterPack_TV_WideScreenVertical_t203_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_TV_WideScreenVertical_t203)/* instance_size */
	, sizeof (CameraFilterPack_TV_WideScreenVertical_t203)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_TV_WideScreenVertical_t203_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_VHS_Tracking
#include "AssemblyU2DCSharp_CameraFilterPack_VHS_Tracking.h"
// Metadata Definition CameraFilterPack_VHS_Tracking
extern TypeInfo CameraFilterPack_VHS_Tracking_t204_il2cpp_TypeInfo;
// CameraFilterPack_VHS_Tracking
#include "AssemblyU2DCSharp_CameraFilterPack_VHS_TrackingMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_VHS_Tracking_t204_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_VHS_Tracking_t204_0_0_0;
extern const Il2CppType CameraFilterPack_VHS_Tracking_t204_1_0_0;
struct CameraFilterPack_VHS_Tracking_t204;
const Il2CppTypeDefinitionMetadata CameraFilterPack_VHS_Tracking_t204_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_VHS_Tracking_t204_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1696/* fieldStart */
	, 1324/* methodStart */
	, -1/* eventStart */
	, 192/* propertyStart */

};
TypeInfo CameraFilterPack_VHS_Tracking_t204_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_VHS_Tracking"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_VHS_Tracking_t204_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 616/* custom_attributes_cache */
	, &CameraFilterPack_VHS_Tracking_t204_0_0_0/* byval_arg */
	, &CameraFilterPack_VHS_Tracking_t204_1_0_0/* this_arg */
	, &CameraFilterPack_VHS_Tracking_t204_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_VHS_Tracking_t204)/* instance_size */
	, sizeof (CameraFilterPack_VHS_Tracking_t204)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_VHS_Tracking_t204_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Vision_Blood
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Blood.h"
// Metadata Definition CameraFilterPack_Vision_Blood
extern TypeInfo CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo;
// CameraFilterPack_Vision_Blood
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_BloodMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Vision_Blood_t205_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Vision_Blood_t205_0_0_0;
extern const Il2CppType CameraFilterPack_Vision_Blood_t205_1_0_0;
struct CameraFilterPack_Vision_Blood_t205;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Vision_Blood_t205_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Vision_Blood_t205_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1702/* fieldStart */
	, 1331/* methodStart */
	, -1/* eventStart */
	, 193/* propertyStart */

};
TypeInfo CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Vision_Blood"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 618/* custom_attributes_cache */
	, &CameraFilterPack_Vision_Blood_t205_0_0_0/* byval_arg */
	, &CameraFilterPack_Vision_Blood_t205_1_0_0/* this_arg */
	, &CameraFilterPack_Vision_Blood_t205_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Vision_Blood_t205)/* instance_size */
	, sizeof (CameraFilterPack_Vision_Blood_t205)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Vision_Blood_t205_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Vision_Crystal
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Crystal.h"
// Metadata Definition CameraFilterPack_Vision_Crystal
extern TypeInfo CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo;
// CameraFilterPack_Vision_Crystal
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_CrystalMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Vision_Crystal_t206_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Vision_Crystal_t206_0_0_0;
extern const Il2CppType CameraFilterPack_Vision_Crystal_t206_1_0_0;
struct CameraFilterPack_Vision_Crystal_t206;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Vision_Crystal_t206_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Vision_Crystal_t206_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1714/* fieldStart */
	, 1338/* methodStart */
	, -1/* eventStart */
	, 194/* propertyStart */

};
TypeInfo CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Vision_Crystal"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 623/* custom_attributes_cache */
	, &CameraFilterPack_Vision_Crystal_t206_0_0_0/* byval_arg */
	, &CameraFilterPack_Vision_Crystal_t206_1_0_0/* this_arg */
	, &CameraFilterPack_Vision_Crystal_t206_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Vision_Crystal_t206)/* instance_size */
	, sizeof (CameraFilterPack_Vision_Crystal_t206)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Vision_Crystal_t206_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Vision_Plasma
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Plasma.h"
// Metadata Definition CameraFilterPack_Vision_Plasma
extern TypeInfo CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo;
// CameraFilterPack_Vision_Plasma
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_PlasmaMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Vision_Plasma_t207_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Vision_Plasma_t207_0_0_0;
extern const Il2CppType CameraFilterPack_Vision_Plasma_t207_1_0_0;
struct CameraFilterPack_Vision_Plasma_t207;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Vision_Plasma_t207_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Vision_Plasma_t207_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1726/* fieldStart */
	, 1345/* methodStart */
	, -1/* eventStart */
	, 195/* propertyStart */

};
TypeInfo CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Vision_Plasma"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 628/* custom_attributes_cache */
	, &CameraFilterPack_Vision_Plasma_t207_0_0_0/* byval_arg */
	, &CameraFilterPack_Vision_Plasma_t207_1_0_0/* this_arg */
	, &CameraFilterPack_Vision_Plasma_t207_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Vision_Plasma_t207)/* instance_size */
	, sizeof (CameraFilterPack_Vision_Plasma_t207)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Vision_Plasma_t207_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Vision_Psycho
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Psycho.h"
// Metadata Definition CameraFilterPack_Vision_Psycho
extern TypeInfo CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo;
// CameraFilterPack_Vision_Psycho
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_PsychoMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Vision_Psycho_t208_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Vision_Psycho_t208_0_0_0;
extern const Il2CppType CameraFilterPack_Vision_Psycho_t208_1_0_0;
struct CameraFilterPack_Vision_Psycho_t208;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Vision_Psycho_t208_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Vision_Psycho_t208_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1738/* fieldStart */
	, 1352/* methodStart */
	, -1/* eventStart */
	, 196/* propertyStart */

};
TypeInfo CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Vision_Psycho"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 633/* custom_attributes_cache */
	, &CameraFilterPack_Vision_Psycho_t208_0_0_0/* byval_arg */
	, &CameraFilterPack_Vision_Psycho_t208_1_0_0/* this_arg */
	, &CameraFilterPack_Vision_Psycho_t208_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Vision_Psycho_t208)/* instance_size */
	, sizeof (CameraFilterPack_Vision_Psycho_t208)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Vision_Psycho_t208_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Vision_Tunnel
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Tunnel.h"
// Metadata Definition CameraFilterPack_Vision_Tunnel
extern TypeInfo CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo;
// CameraFilterPack_Vision_Tunnel
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_TunnelMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Vision_Tunnel_t209_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Vision_Tunnel_t209_0_0_0;
extern const Il2CppType CameraFilterPack_Vision_Tunnel_t209_1_0_0;
struct CameraFilterPack_Vision_Tunnel_t209;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Vision_Tunnel_t209_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Vision_Tunnel_t209_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 1750/* fieldStart */
	, 1359/* methodStart */
	, -1/* eventStart */
	, 197/* propertyStart */

};
TypeInfo CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Vision_Tunnel"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 638/* custom_attributes_cache */
	, &CameraFilterPack_Vision_Tunnel_t209_0_0_0/* byval_arg */
	, &CameraFilterPack_Vision_Tunnel_t209_1_0_0/* this_arg */
	, &CameraFilterPack_Vision_Tunnel_t209_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Vision_Tunnel_t209)/* instance_size */
	, sizeof (CameraFilterPack_Vision_Tunnel_t209)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Vision_Tunnel_t209_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
