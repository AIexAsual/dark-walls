﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GoTo/<WaitAndEnable>c__Iterator6
struct U3CWaitAndEnableU3Ec__Iterator6_t316;
// System.Object
struct Object_t;

// System.Void GoTo/<WaitAndEnable>c__Iterator6::.ctor()
extern "C" void U3CWaitAndEnableU3Ec__Iterator6__ctor_m1889 (U3CWaitAndEnableU3Ec__Iterator6_t316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GoTo/<WaitAndEnable>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndEnableU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1890 (U3CWaitAndEnableU3Ec__Iterator6_t316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GoTo/<WaitAndEnable>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndEnableU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1891 (U3CWaitAndEnableU3Ec__Iterator6_t316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GoTo/<WaitAndEnable>c__Iterator6::MoveNext()
extern "C" bool U3CWaitAndEnableU3Ec__Iterator6_MoveNext_m1892 (U3CWaitAndEnableU3Ec__Iterator6_t316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoTo/<WaitAndEnable>c__Iterator6::Dispose()
extern "C" void U3CWaitAndEnableU3Ec__Iterator6_Dispose_m1893 (U3CWaitAndEnableU3Ec__Iterator6_t316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoTo/<WaitAndEnable>c__Iterator6::Reset()
extern "C" void U3CWaitAndEnableU3Ec__Iterator6_Reset_m1894 (U3CWaitAndEnableU3Ec__Iterator6_t316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
