﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_LinearLight
struct CameraFilterPack_Blend2Camera_LinearLight_t34;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_LinearLight::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_LinearLight__ctor_m185 (CameraFilterPack_Blend2Camera_LinearLight_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_LinearLight::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_LinearLight_get_material_m186 (CameraFilterPack_Blend2Camera_LinearLight_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearLight::Start()
extern "C" void CameraFilterPack_Blend2Camera_LinearLight_Start_m187 (CameraFilterPack_Blend2Camera_LinearLight_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearLight::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_LinearLight_OnRenderImage_m188 (CameraFilterPack_Blend2Camera_LinearLight_t34 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearLight::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_LinearLight_OnValidate_m189 (CameraFilterPack_Blend2Camera_LinearLight_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearLight::Update()
extern "C" void CameraFilterPack_Blend2Camera_LinearLight_Update_m190 (CameraFilterPack_Blend2Camera_LinearLight_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearLight::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_LinearLight_OnEnable_m191 (CameraFilterPack_Blend2Camera_LinearLight_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearLight::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_LinearLight_OnDisable_m192 (CameraFilterPack_Blend2Camera_LinearLight_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
