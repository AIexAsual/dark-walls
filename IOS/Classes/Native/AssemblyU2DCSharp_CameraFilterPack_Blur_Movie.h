﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Blur_Movie
struct  CameraFilterPack_Blur_Movie_t54  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Blur_Movie::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Blur_Movie::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Blur_Movie::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Blur_Movie::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Blur_Movie::Radius
	float ___Radius_6;
	// System.Single CameraFilterPack_Blur_Movie::Factor
	float ___Factor_7;
	// System.Int32 CameraFilterPack_Blur_Movie::FastFilter
	int32_t ___FastFilter_8;
	// System.Single CameraFilterPack_Blur_Movie::time
	float ___time_12;
};
struct CameraFilterPack_Blur_Movie_t54_StaticFields{
	// System.Single CameraFilterPack_Blur_Movie::ChangeRadius
	float ___ChangeRadius_9;
	// System.Single CameraFilterPack_Blur_Movie::ChangeFactor
	float ___ChangeFactor_10;
	// System.Int32 CameraFilterPack_Blur_Movie::ChangeFastFilter
	int32_t ___ChangeFastFilter_11;
};
