﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SA_ScreenShotMaker
struct SA_ScreenShotMaker_t300;
// System.Collections.IEnumerator
struct IEnumerator_t464;

// System.Void SA_ScreenShotMaker::.ctor()
extern "C" void SA_ScreenShotMaker__ctor_m1841 (SA_ScreenShotMaker_t300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_ScreenShotMaker::GetScreenshot()
extern "C" void SA_ScreenShotMaker_GetScreenshot_m1842 (SA_ScreenShotMaker_t300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SA_ScreenShotMaker::SaveScreenshot()
extern "C" Object_t * SA_ScreenShotMaker_SaveScreenshot_m1843 (SA_ScreenShotMaker_t300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
