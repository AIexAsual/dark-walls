﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractiveMama/<WaitAndMamaPos>c__Iterator8
struct U3CWaitAndMamaPosU3Ec__Iterator8_t332;
// System.Object
struct Object_t;

// System.Void InteractiveMama/<WaitAndMamaPos>c__Iterator8::.ctor()
extern "C" void U3CWaitAndMamaPosU3Ec__Iterator8__ctor_m1941 (U3CWaitAndMamaPosU3Ec__Iterator8_t332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InteractiveMama/<WaitAndMamaPos>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndMamaPosU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1942 (U3CWaitAndMamaPosU3Ec__Iterator8_t332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InteractiveMama/<WaitAndMamaPos>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndMamaPosU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1943 (U3CWaitAndMamaPosU3Ec__Iterator8_t332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InteractiveMama/<WaitAndMamaPos>c__Iterator8::MoveNext()
extern "C" bool U3CWaitAndMamaPosU3Ec__Iterator8_MoveNext_m1944 (U3CWaitAndMamaPosU3Ec__Iterator8_t332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveMama/<WaitAndMamaPos>c__Iterator8::Dispose()
extern "C" void U3CWaitAndMamaPosU3Ec__Iterator8_Dispose_m1945 (U3CWaitAndMamaPosU3Ec__Iterator8_t332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveMama/<WaitAndMamaPos>c__Iterator8::Reset()
extern "C" void U3CWaitAndMamaPosU3Ec__Iterator8_Reset_m1946 (U3CWaitAndMamaPosU3Ec__Iterator8_t332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
