﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t398;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t317;
// UnityEngine.Transform[]
struct TransformU5BU5D_t423;
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// Vector3OrTransformArray
struct  Vector3OrTransformArray_t424  : public Object_t
{
	// System.Int32 Vector3OrTransformArray::selected
	int32_t ___selected_4;
	// UnityEngine.Vector3[] Vector3OrTransformArray::vectorArray
	Vector3U5BU5D_t317* ___vectorArray_5;
	// UnityEngine.Transform[] Vector3OrTransformArray::transformArray
	TransformU5BU5D_t423* ___transformArray_6;
	// System.String Vector3OrTransformArray::pathName
	String_t* ___pathName_7;
};
struct Vector3OrTransformArray_t424_StaticFields{
	// System.String[] Vector3OrTransformArray::choices
	StringU5BU5D_t398* ___choices_0;
	// System.Int32 Vector3OrTransformArray::vector3Selected
	int32_t ___vector3Selected_1;
	// System.Int32 Vector3OrTransformArray::transformSelected
	int32_t ___transformSelected_2;
	// System.Int32 Vector3OrTransformArray::iTweenPathSelected
	int32_t ___iTweenPathSelected_3;
};
