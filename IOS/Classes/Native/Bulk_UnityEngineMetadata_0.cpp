﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// <Module>
#include "UnityEngine_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t827_il2cpp_TypeInfo;
// <Module>
#include "UnityEngine_U3CModuleU3EMethodDeclarations.h"
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CModuleU3E_t827_0_0_0;
extern const Il2CppType U3CModuleU3E_t827_1_0_0;
struct U3CModuleU3E_t827;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t827_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CModuleU3E_t827_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CModuleU3E_t827_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t827_0_0_0/* byval_arg */
	, &U3CModuleU3E_t827_1_0_0/* this_arg */
	, &U3CModuleU3E_t827_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t827)/* instance_size */
	, sizeof (U3CModuleU3E_t827)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AssetBundleCreateRequest
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest.h"
// Metadata Definition UnityEngine.AssetBundleCreateRequest
extern TypeInfo AssetBundleCreateRequest_t828_il2cpp_TypeInfo;
// UnityEngine.AssetBundleCreateRequest
#include "UnityEngine_UnityEngine_AssetBundleCreateRequestMethodDeclarations.h"
static const EncodedMethodIndex AssetBundleCreateRequest_t828_VTable[4] = 
{
	626,
	1262,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AssetBundleCreateRequest_t828_0_0_0;
extern const Il2CppType AssetBundleCreateRequest_t828_1_0_0;
extern const Il2CppType AsyncOperation_t829_0_0_0;
struct AssetBundleCreateRequest_t828;
const Il2CppTypeDefinitionMetadata AssetBundleCreateRequest_t828_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsyncOperation_t829_0_0_0/* parent */
	, AssetBundleCreateRequest_t828_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4025/* methodStart */
	, -1/* eventStart */
	, 674/* propertyStart */

};
TypeInfo AssetBundleCreateRequest_t828_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssetBundleCreateRequest"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AssetBundleCreateRequest_t828_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AssetBundleCreateRequest_t828_0_0_0/* byval_arg */
	, &AssetBundleCreateRequest_t828_1_0_0/* this_arg */
	, &AssetBundleCreateRequest_t828_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssetBundleCreateRequest_t828)/* instance_size */
	, sizeof (AssetBundleCreateRequest_t828)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AssetBundleRequest
#include "UnityEngine_UnityEngine_AssetBundleRequest.h"
// Metadata Definition UnityEngine.AssetBundleRequest
extern TypeInfo AssetBundleRequest_t831_il2cpp_TypeInfo;
// UnityEngine.AssetBundleRequest
#include "UnityEngine_UnityEngine_AssetBundleRequestMethodDeclarations.h"
static const EncodedMethodIndex AssetBundleRequest_t831_VTable[4] = 
{
	626,
	1262,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AssetBundleRequest_t831_0_0_0;
extern const Il2CppType AssetBundleRequest_t831_1_0_0;
struct AssetBundleRequest_t831;
const Il2CppTypeDefinitionMetadata AssetBundleRequest_t831_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsyncOperation_t829_0_0_0/* parent */
	, AssetBundleRequest_t831_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3565/* fieldStart */
	, 4028/* methodStart */
	, -1/* eventStart */
	, 675/* propertyStart */

};
TypeInfo AssetBundleRequest_t831_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssetBundleRequest"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AssetBundleRequest_t831_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AssetBundleRequest_t831_0_0_0/* byval_arg */
	, &AssetBundleRequest_t831_1_0_0/* this_arg */
	, &AssetBundleRequest_t831_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssetBundleRequest_t831)/* instance_size */
	, sizeof (AssetBundleRequest_t831)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.AssetBundle
#include "UnityEngine_UnityEngine_AssetBundle.h"
// Metadata Definition UnityEngine.AssetBundle
extern TypeInfo AssetBundle_t830_il2cpp_TypeInfo;
// UnityEngine.AssetBundle
#include "UnityEngine_UnityEngine_AssetBundleMethodDeclarations.h"
static const EncodedMethodIndex AssetBundle_t830_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AssetBundle_t830_0_0_0;
extern const Il2CppType AssetBundle_t830_1_0_0;
extern const Il2CppType Object_t473_0_0_0;
struct AssetBundle_t830;
const Il2CppTypeDefinitionMetadata AssetBundle_t830_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t473_0_0_0/* parent */
	, AssetBundle_t830_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4031/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssetBundle_t830_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssetBundle"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AssetBundle_t830_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AssetBundle_t830_0_0_0/* byval_arg */
	, &AssetBundle_t830_1_0_0/* this_arg */
	, &AssetBundle_t830_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssetBundle_t830)/* instance_size */
	, sizeof (AssetBundle_t830)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
// Metadata Definition UnityEngine.SendMessageOptions
extern TypeInfo SendMessageOptions_t832_il2cpp_TypeInfo;
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptionsMethodDeclarations.h"
static const EncodedMethodIndex SendMessageOptions_t832_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair SendMessageOptions_t832_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SendMessageOptions_t832_0_0_0;
extern const Il2CppType SendMessageOptions_t832_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata SendMessageOptions_t832_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SendMessageOptions_t832_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, SendMessageOptions_t832_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3568/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SendMessageOptions_t832_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SendMessageOptions"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SendMessageOptions_t832_0_0_0/* byval_arg */
	, &SendMessageOptions_t832_1_0_0/* this_arg */
	, &SendMessageOptions_t832_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SendMessageOptions_t832)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SendMessageOptions_t832)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
// Metadata Definition UnityEngine.Space
extern TypeInfo Space_t546_il2cpp_TypeInfo;
// UnityEngine.Space
#include "UnityEngine_UnityEngine_SpaceMethodDeclarations.h"
static const EncodedMethodIndex Space_t546_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Space_t546_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Space_t546_0_0_0;
extern const Il2CppType Space_t546_1_0_0;
const Il2CppTypeDefinitionMetadata Space_t546_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Space_t546_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Space_t546_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3571/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Space_t546_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Space"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Space_t546_0_0_0/* byval_arg */
	, &Space_t546_1_0_0/* this_arg */
	, &Space_t546_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Space_t546)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Space_t546)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
// Metadata Definition UnityEngine.LayerMask
extern TypeInfo LayerMask_t241_il2cpp_TypeInfo;
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMaskMethodDeclarations.h"
static const EncodedMethodIndex LayerMask_t241_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType LayerMask_t241_0_0_0;
extern const Il2CppType LayerMask_t241_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata LayerMask_t241_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, LayerMask_t241_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3574/* fieldStart */
	, 4034/* methodStart */
	, -1/* eventStart */
	, 677/* propertyStart */

};
TypeInfo LayerMask_t241_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayerMask"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &LayerMask_t241_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LayerMask_t241_0_0_0/* byval_arg */
	, &LayerMask_t241_1_0_0/* this_arg */
	, &LayerMask_t241_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayerMask_t241)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LayerMask_t241)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(LayerMask_t241 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
// Metadata Definition UnityEngine.RuntimePlatform
extern TypeInfo RuntimePlatform_t833_il2cpp_TypeInfo;
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatformMethodDeclarations.h"
static const EncodedMethodIndex RuntimePlatform_t833_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair RuntimePlatform_t833_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RuntimePlatform_t833_0_0_0;
extern const Il2CppType RuntimePlatform_t833_1_0_0;
const Il2CppTypeDefinitionMetadata RuntimePlatform_t833_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RuntimePlatform_t833_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, RuntimePlatform_t833_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3575/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RuntimePlatform_t833_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimePlatform"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RuntimePlatform_t833_0_0_0/* byval_arg */
	, &RuntimePlatform_t833_1_0_0/* this_arg */
	, &RuntimePlatform_t833_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimePlatform_t833)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RuntimePlatform_t833)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 30/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.LogType
#include "UnityEngine_UnityEngine_LogType.h"
// Metadata Definition UnityEngine.LogType
extern TypeInfo LogType_t834_il2cpp_TypeInfo;
// UnityEngine.LogType
#include "UnityEngine_UnityEngine_LogTypeMethodDeclarations.h"
static const EncodedMethodIndex LogType_t834_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair LogType_t834_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType LogType_t834_0_0_0;
extern const Il2CppType LogType_t834_1_0_0;
const Il2CppTypeDefinitionMetadata LogType_t834_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LogType_t834_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, LogType_t834_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3605/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LogType_t834_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LogType"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LogType_t834_0_0_0/* byval_arg */
	, &LogType_t834_1_0_0/* this_arg */
	, &LogType_t834_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LogType_t834)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LogType_t834)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SystemInfo
#include "UnityEngine_UnityEngine_SystemInfo.h"
// Metadata Definition UnityEngine.SystemInfo
extern TypeInfo SystemInfo_t835_il2cpp_TypeInfo;
// UnityEngine.SystemInfo
#include "UnityEngine_UnityEngine_SystemInfoMethodDeclarations.h"
static const EncodedMethodIndex SystemInfo_t835_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SystemInfo_t835_0_0_0;
extern const Il2CppType SystemInfo_t835_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct SystemInfo_t835;
const Il2CppTypeDefinitionMetadata SystemInfo_t835_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SystemInfo_t835_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4041/* methodStart */
	, -1/* eventStart */
	, 678/* propertyStart */

};
TypeInfo SystemInfo_t835_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SystemInfo"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SystemInfo_t835_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SystemInfo_t835_0_0_0/* byval_arg */
	, &SystemInfo_t835_1_0_0/* this_arg */
	, &SystemInfo_t835_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SystemInfo_t835)/* instance_size */
	, sizeof (SystemInfo_t835)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
// Metadata Definition UnityEngine.WaitForSeconds
extern TypeInfo WaitForSeconds_t475_il2cpp_TypeInfo;
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
static const EncodedMethodIndex WaitForSeconds_t475_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WaitForSeconds_t475_0_0_0;
extern const Il2CppType WaitForSeconds_t475_1_0_0;
extern const Il2CppType YieldInstruction_t836_0_0_0;
struct WaitForSeconds_t475;
const Il2CppTypeDefinitionMetadata WaitForSeconds_t475_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &YieldInstruction_t836_0_0_0/* parent */
	, WaitForSeconds_t475_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3611/* fieldStart */
	, 4044/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WaitForSeconds_t475_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WaitForSeconds"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WaitForSeconds_t475_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WaitForSeconds_t475_0_0_0/* byval_arg */
	, &WaitForSeconds_t475_1_0_0/* this_arg */
	, &WaitForSeconds_t475_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)WaitForSeconds_t475_marshal/* marshal_to_native_func */
	, (methodPointerType)WaitForSeconds_t475_marshal_back/* marshal_from_native_func */
	, (methodPointerType)WaitForSeconds_t475_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (WaitForSeconds_t475)/* instance_size */
	, sizeof (WaitForSeconds_t475)/* actualSize */
	, 0/* element_size */
	, sizeof(WaitForSeconds_t475_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WaitForFixedUpdate
#include "UnityEngine_UnityEngine_WaitForFixedUpdate.h"
// Metadata Definition UnityEngine.WaitForFixedUpdate
extern TypeInfo WaitForFixedUpdate_t837_il2cpp_TypeInfo;
// UnityEngine.WaitForFixedUpdate
#include "UnityEngine_UnityEngine_WaitForFixedUpdateMethodDeclarations.h"
static const EncodedMethodIndex WaitForFixedUpdate_t837_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WaitForFixedUpdate_t837_0_0_0;
extern const Il2CppType WaitForFixedUpdate_t837_1_0_0;
struct WaitForFixedUpdate_t837;
const Il2CppTypeDefinitionMetadata WaitForFixedUpdate_t837_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &YieldInstruction_t836_0_0_0/* parent */
	, WaitForFixedUpdate_t837_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4045/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WaitForFixedUpdate_t837_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WaitForFixedUpdate"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WaitForFixedUpdate_t837_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WaitForFixedUpdate_t837_0_0_0/* byval_arg */
	, &WaitForFixedUpdate_t837_1_0_0/* this_arg */
	, &WaitForFixedUpdate_t837_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WaitForFixedUpdate_t837)/* instance_size */
	, sizeof (WaitForFixedUpdate_t837)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WaitForEndOfFrame
#include "UnityEngine_UnityEngine_WaitForEndOfFrame.h"
// Metadata Definition UnityEngine.WaitForEndOfFrame
extern TypeInfo WaitForEndOfFrame_t484_il2cpp_TypeInfo;
// UnityEngine.WaitForEndOfFrame
#include "UnityEngine_UnityEngine_WaitForEndOfFrameMethodDeclarations.h"
static const EncodedMethodIndex WaitForEndOfFrame_t484_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WaitForEndOfFrame_t484_0_0_0;
extern const Il2CppType WaitForEndOfFrame_t484_1_0_0;
struct WaitForEndOfFrame_t484;
const Il2CppTypeDefinitionMetadata WaitForEndOfFrame_t484_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &YieldInstruction_t836_0_0_0/* parent */
	, WaitForEndOfFrame_t484_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4046/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WaitForEndOfFrame_t484_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WaitForEndOfFrame"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WaitForEndOfFrame_t484_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WaitForEndOfFrame_t484_0_0_0/* byval_arg */
	, &WaitForEndOfFrame_t484_1_0_0/* this_arg */
	, &WaitForEndOfFrame_t484_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WaitForEndOfFrame_t484)/* instance_size */
	, sizeof (WaitForEndOfFrame_t484)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Coroutine
#include "UnityEngine_UnityEngine_Coroutine.h"
// Metadata Definition UnityEngine.Coroutine
extern TypeInfo Coroutine_t547_il2cpp_TypeInfo;
// UnityEngine.Coroutine
#include "UnityEngine_UnityEngine_CoroutineMethodDeclarations.h"
static const EncodedMethodIndex Coroutine_t547_VTable[4] = 
{
	626,
	1263,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Coroutine_t547_0_0_0;
extern const Il2CppType Coroutine_t547_1_0_0;
struct Coroutine_t547;
const Il2CppTypeDefinitionMetadata Coroutine_t547_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &YieldInstruction_t836_0_0_0/* parent */
	, Coroutine_t547_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3612/* fieldStart */
	, 4047/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Coroutine_t547_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Coroutine"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Coroutine_t547_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Coroutine_t547_0_0_0/* byval_arg */
	, &Coroutine_t547_1_0_0/* this_arg */
	, &Coroutine_t547_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Coroutine_t547_marshal/* marshal_to_native_func */
	, (methodPointerType)Coroutine_t547_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Coroutine_t547_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Coroutine_t547)/* instance_size */
	, sizeof (Coroutine_t547)/* actualSize */
	, 0/* element_size */
	, sizeof(Coroutine_t547_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.ScriptableObject
#include "UnityEngine_UnityEngine_ScriptableObject.h"
// Metadata Definition UnityEngine.ScriptableObject
extern TypeInfo ScriptableObject_t838_il2cpp_TypeInfo;
// UnityEngine.ScriptableObject
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
extern const Il2CppType ScriptableObject_CreateInstance_m24176_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition ScriptableObject_CreateInstance_m24176_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 3932 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3932 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
static const EncodedMethodIndex ScriptableObject_t838_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ScriptableObject_t838_0_0_0;
extern const Il2CppType ScriptableObject_t838_1_0_0;
struct ScriptableObject_t838;
const Il2CppTypeDefinitionMetadata ScriptableObject_t838_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t473_0_0_0/* parent */
	, ScriptableObject_t838_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4050/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ScriptableObject_t838_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScriptableObject"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &ScriptableObject_t838_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScriptableObject_t838_0_0_0/* byval_arg */
	, &ScriptableObject_t838_1_0_0/* this_arg */
	, &ScriptableObject_t838_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)ScriptableObject_t838_marshal/* marshal_to_native_func */
	, (methodPointerType)ScriptableObject_t838_marshal_back/* marshal_from_native_func */
	, (methodPointerType)ScriptableObject_t838_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (ScriptableObject_t838)/* instance_size */
	, sizeof (ScriptableObject_t838)/* actualSize */
	, 0/* element_size */
	, sizeof(ScriptableObject_t838_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GameCente.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
extern TypeInfo GameCenterPlatform_t848_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GameCenteMethodDeclarations.h"
static const EncodedMethodIndex GameCenterPlatform_t848_VTable[19] = 
{
	626,
	601,
	627,
	628,
	1264,
	1265,
	1266,
	1267,
	1268,
	1269,
	1270,
	1271,
	1272,
	1273,
	1274,
	1275,
	1276,
	1277,
	1278,
};
extern const Il2CppType ISocialPlatform_t3406_0_0_0;
static const Il2CppType* GameCenterPlatform_t848_InterfacesTypeInfos[] = 
{
	&ISocialPlatform_t3406_0_0_0,
};
static Il2CppInterfaceOffsetPair GameCenterPlatform_t848_InterfacesOffsets[] = 
{
	{ &ISocialPlatform_t3406_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GameCenterPlatform_t848_0_0_0;
extern const Il2CppType GameCenterPlatform_t848_1_0_0;
struct GameCenterPlatform_t848;
const Il2CppTypeDefinitionMetadata GameCenterPlatform_t848_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, GameCenterPlatform_t848_InterfacesTypeInfos/* implementedInterfaces */
	, GameCenterPlatform_t848_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GameCenterPlatform_t848_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3613/* fieldStart */
	, 4056/* methodStart */
	, -1/* eventStart */
	, 681/* propertyStart */

};
TypeInfo GameCenterPlatform_t848_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GameCenterPlatform"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, NULL/* methods */
	, &GameCenterPlatform_t848_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GameCenterPlatform_t848_0_0_0/* byval_arg */
	, &GameCenterPlatform_t848_1_0_0/* this_arg */
	, &GameCenterPlatform_t848_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GameCenterPlatform_t848)/* instance_size */
	, sizeof (GameCenterPlatform_t848)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GameCenterPlatform_t848_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 61/* method_count */
	, 1/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcLeaderb.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
extern TypeInfo GcLeaderboard_t850_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcLeaderbMethodDeclarations.h"
static const EncodedMethodIndex GcLeaderboard_t850_VTable[4] = 
{
	626,
	1279,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcLeaderboard_t850_0_0_0;
extern const Il2CppType GcLeaderboard_t850_1_0_0;
struct GcLeaderboard_t850;
const Il2CppTypeDefinitionMetadata GcLeaderboard_t850_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GcLeaderboard_t850_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3628/* fieldStart */
	, 4117/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GcLeaderboard_t850_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcLeaderboard"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, NULL/* methods */
	, &GcLeaderboard_t850_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcLeaderboard_t850_0_0_0/* byval_arg */
	, &GcLeaderboard_t850_1_0_0/* this_arg */
	, &GcLeaderboard_t850_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GcLeaderboard_t850)/* instance_size */
	, sizeof (GcLeaderboard_t850)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RenderSettings
#include "UnityEngine_UnityEngine_RenderSettings.h"
// Metadata Definition UnityEngine.RenderSettings
extern TypeInfo RenderSettings_t851_il2cpp_TypeInfo;
// UnityEngine.RenderSettings
#include "UnityEngine_UnityEngine_RenderSettingsMethodDeclarations.h"
static const EncodedMethodIndex RenderSettings_t851_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderSettings_t851_0_0_0;
extern const Il2CppType RenderSettings_t851_1_0_0;
struct RenderSettings_t851;
const Il2CppTypeDefinitionMetadata RenderSettings_t851_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t473_0_0_0/* parent */
	, RenderSettings_t851_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4128/* methodStart */
	, -1/* eventStart */
	, 682/* propertyStart */

};
TypeInfo RenderSettings_t851_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderSettings"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &RenderSettings_t851_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderSettings_t851_0_0_0/* byval_arg */
	, &RenderSettings_t851_1_0_0/* this_arg */
	, &RenderSettings_t851_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderSettings_t851)/* instance_size */
	, sizeof (RenderSettings_t851)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.QualitySettings
#include "UnityEngine_UnityEngine_QualitySettings.h"
// Metadata Definition UnityEngine.QualitySettings
extern TypeInfo QualitySettings_t852_il2cpp_TypeInfo;
// UnityEngine.QualitySettings
#include "UnityEngine_UnityEngine_QualitySettingsMethodDeclarations.h"
static const EncodedMethodIndex QualitySettings_t852_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType QualitySettings_t852_0_0_0;
extern const Il2CppType QualitySettings_t852_1_0_0;
struct QualitySettings_t852;
const Il2CppTypeDefinitionMetadata QualitySettings_t852_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t473_0_0_0/* parent */
	, QualitySettings_t852_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4129/* methodStart */
	, -1/* eventStart */
	, 683/* propertyStart */

};
TypeInfo QualitySettings_t852_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "QualitySettings"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &QualitySettings_t852_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &QualitySettings_t852_0_0_0/* byval_arg */
	, &QualitySettings_t852_1_0_0/* this_arg */
	, &QualitySettings_t852_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (QualitySettings_t852)/* instance_size */
	, sizeof (QualitySettings_t852)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilter.h"
// Metadata Definition UnityEngine.MeshFilter
extern TypeInfo MeshFilter_t853_il2cpp_TypeInfo;
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"
static const EncodedMethodIndex MeshFilter_t853_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType MeshFilter_t853_0_0_0;
extern const Il2CppType MeshFilter_t853_1_0_0;
extern const Il2CppType Component_t477_0_0_0;
struct MeshFilter_t853;
const Il2CppTypeDefinitionMetadata MeshFilter_t853_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Component_t477_0_0_0/* parent */
	, MeshFilter_t853_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MeshFilter_t853_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "MeshFilter"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &MeshFilter_t853_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MeshFilter_t853_0_0_0/* byval_arg */
	, &MeshFilter_t853_1_0_0/* this_arg */
	, &MeshFilter_t853_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MeshFilter_t853)/* instance_size */
	, sizeof (MeshFilter_t853)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// Metadata Definition UnityEngine.Mesh
extern TypeInfo Mesh_t245_il2cpp_TypeInfo;
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
static const EncodedMethodIndex Mesh_t245_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Mesh_t245_0_0_0;
extern const Il2CppType Mesh_t245_1_0_0;
struct Mesh_t245;
const Il2CppTypeDefinitionMetadata Mesh_t245_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t473_0_0_0/* parent */
	, Mesh_t245_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4131/* methodStart */
	, -1/* eventStart */
	, 685/* propertyStart */

};
TypeInfo Mesh_t245_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mesh"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Mesh_t245_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mesh_t245_0_0_0/* byval_arg */
	, &Mesh_t245_1_0_0/* this_arg */
	, &Mesh_t245_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mesh_t245)/* instance_size */
	, sizeof (Mesh_t245)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeight.h"
// Metadata Definition UnityEngine.BoneWeight
extern TypeInfo BoneWeight_t854_il2cpp_TypeInfo;
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeightMethodDeclarations.h"
static const EncodedMethodIndex BoneWeight_t854_VTable[4] = 
{
	1280,
	601,
	1281,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType BoneWeight_t854_0_0_0;
extern const Il2CppType BoneWeight_t854_1_0_0;
const Il2CppTypeDefinitionMetadata BoneWeight_t854_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, BoneWeight_t854_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3630/* fieldStart */
	, 4139/* methodStart */
	, -1/* eventStart */
	, 689/* propertyStart */

};
TypeInfo BoneWeight_t854_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "BoneWeight"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &BoneWeight_t854_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BoneWeight_t854_0_0_0/* byval_arg */
	, &BoneWeight_t854_1_0_0/* this_arg */
	, &BoneWeight_t854_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BoneWeight_t854)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BoneWeight_t854)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(BoneWeight_t854 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 20/* method_count */
	, 8/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SkinnedMeshRenderer
#include "UnityEngine_UnityEngine_SkinnedMeshRenderer.h"
// Metadata Definition UnityEngine.SkinnedMeshRenderer
extern TypeInfo SkinnedMeshRenderer_t532_il2cpp_TypeInfo;
// UnityEngine.SkinnedMeshRenderer
#include "UnityEngine_UnityEngine_SkinnedMeshRendererMethodDeclarations.h"
static const EncodedMethodIndex SkinnedMeshRenderer_t532_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SkinnedMeshRenderer_t532_0_0_0;
extern const Il2CppType SkinnedMeshRenderer_t532_1_0_0;
extern const Il2CppType Renderer_t312_0_0_0;
struct SkinnedMeshRenderer_t532;
const Il2CppTypeDefinitionMetadata SkinnedMeshRenderer_t532_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Renderer_t312_0_0_0/* parent */
	, SkinnedMeshRenderer_t532_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SkinnedMeshRenderer_t532_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SkinnedMeshRenderer"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SkinnedMeshRenderer_t532_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SkinnedMeshRenderer_t532_0_0_0/* byval_arg */
	, &SkinnedMeshRenderer_t532_1_0_0/* this_arg */
	, &SkinnedMeshRenderer_t532_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SkinnedMeshRenderer_t532)/* instance_size */
	, sizeof (SkinnedMeshRenderer_t532)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.LensFlare
#include "UnityEngine_UnityEngine_LensFlare.h"
// Metadata Definition UnityEngine.LensFlare
extern TypeInfo LensFlare_t855_il2cpp_TypeInfo;
// UnityEngine.LensFlare
#include "UnityEngine_UnityEngine_LensFlareMethodDeclarations.h"
static const EncodedMethodIndex LensFlare_t855_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType LensFlare_t855_0_0_0;
extern const Il2CppType LensFlare_t855_1_0_0;
extern const Il2CppType Behaviour_t825_0_0_0;
struct LensFlare_t855;
const Il2CppTypeDefinitionMetadata LensFlare_t855_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t825_0_0_0/* parent */
	, LensFlare_t855_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LensFlare_t855_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LensFlare"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &LensFlare_t855_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LensFlare_t855_0_0_0/* byval_arg */
	, &LensFlare_t855_1_0_0/* this_arg */
	, &LensFlare_t855_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LensFlare_t855)/* instance_size */
	, sizeof (LensFlare_t855)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"
// Metadata Definition UnityEngine.Renderer
extern TypeInfo Renderer_t312_il2cpp_TypeInfo;
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
static const EncodedMethodIndex Renderer_t312_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Renderer_t312_1_0_0;
struct Renderer_t312;
const Il2CppTypeDefinitionMetadata Renderer_t312_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Component_t477_0_0_0/* parent */
	, Renderer_t312_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4159/* methodStart */
	, -1/* eventStart */
	, 697/* propertyStart */

};
TypeInfo Renderer_t312_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Renderer"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Renderer_t312_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Renderer_t312_0_0_0/* byval_arg */
	, &Renderer_t312_1_0_0/* this_arg */
	, &Renderer_t312_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Renderer_t312)/* instance_size */
	, sizeof (Renderer_t312)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.InternalDrawTextureArguments
#include "UnityEngine_UnityEngine_InternalDrawTextureArguments.h"
// Metadata Definition UnityEngine.InternalDrawTextureArguments
extern TypeInfo InternalDrawTextureArguments_t856_il2cpp_TypeInfo;
// UnityEngine.InternalDrawTextureArguments
#include "UnityEngine_UnityEngine_InternalDrawTextureArgumentsMethodDeclarations.h"
static const EncodedMethodIndex InternalDrawTextureArguments_t856_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InternalDrawTextureArguments_t856_0_0_0;
extern const Il2CppType InternalDrawTextureArguments_t856_1_0_0;
const Il2CppTypeDefinitionMetadata InternalDrawTextureArguments_t856_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, InternalDrawTextureArguments_t856_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3638/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InternalDrawTextureArguments_t856_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalDrawTextureArguments"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &InternalDrawTextureArguments_t856_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InternalDrawTextureArguments_t856_0_0_0/* byval_arg */
	, &InternalDrawTextureArguments_t856_1_0_0/* this_arg */
	, &InternalDrawTextureArguments_t856_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalDrawTextureArguments_t856)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (InternalDrawTextureArguments_t856)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Graphics
#include "UnityEngine_UnityEngine_Graphics.h"
// Metadata Definition UnityEngine.Graphics
extern TypeInfo Graphics_t857_il2cpp_TypeInfo;
// UnityEngine.Graphics
#include "UnityEngine_UnityEngine_GraphicsMethodDeclarations.h"
static const EncodedMethodIndex Graphics_t857_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Graphics_t857_0_0_0;
extern const Il2CppType Graphics_t857_1_0_0;
struct Graphics_t857;
const Il2CppTypeDefinitionMetadata Graphics_t857_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Graphics_t857_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4165/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Graphics_t857_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Graphics"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Graphics_t857_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Graphics_t857_0_0_0/* byval_arg */
	, &Graphics_t857_1_0_0/* this_arg */
	, &Graphics_t857_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Graphics_t857)/* instance_size */
	, sizeof (Graphics_t857)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_Screen.h"
// Metadata Definition UnityEngine.Screen
extern TypeInfo Screen_t858_il2cpp_TypeInfo;
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
static const EncodedMethodIndex Screen_t858_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Screen_t858_0_0_0;
extern const Il2CppType Screen_t858_1_0_0;
struct Screen_t858;
const Il2CppTypeDefinitionMetadata Screen_t858_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Screen_t858_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4176/* methodStart */
	, -1/* eventStart */
	, 702/* propertyStart */

};
TypeInfo Screen_t858_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Screen"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Screen_t858_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Screen_t858_0_0_0/* byval_arg */
	, &Screen_t858_1_0_0/* this_arg */
	, &Screen_t858_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Screen_t858)/* instance_size */
	, sizeof (Screen_t858)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GL
#include "UnityEngine_UnityEngine_GL.h"
// Metadata Definition UnityEngine.GL
extern TypeInfo GL_t859_il2cpp_TypeInfo;
// UnityEngine.GL
#include "UnityEngine_UnityEngine_GLMethodDeclarations.h"
static const EncodedMethodIndex GL_t859_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GL_t859_0_0_0;
extern const Il2CppType GL_t859_1_0_0;
struct GL_t859;
const Il2CppTypeDefinitionMetadata GL_t859_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GL_t859_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4180/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GL_t859_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GL"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GL_t859_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GL_t859_0_0_0/* byval_arg */
	, &GL_t859_1_0_0/* this_arg */
	, &GL_t859_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GL_t859)/* instance_size */
	, sizeof (GL_t859)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.MeshRenderer
#include "UnityEngine_UnityEngine_MeshRenderer.h"
// Metadata Definition UnityEngine.MeshRenderer
extern TypeInfo MeshRenderer_t223_il2cpp_TypeInfo;
// UnityEngine.MeshRenderer
#include "UnityEngine_UnityEngine_MeshRendererMethodDeclarations.h"
static const EncodedMethodIndex MeshRenderer_t223_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType MeshRenderer_t223_0_0_0;
extern const Il2CppType MeshRenderer_t223_1_0_0;
struct MeshRenderer_t223;
const Il2CppTypeDefinitionMetadata MeshRenderer_t223_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Renderer_t312_0_0_0/* parent */
	, MeshRenderer_t223_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MeshRenderer_t223_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "MeshRenderer"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &MeshRenderer_t223_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MeshRenderer_t223_0_0_0/* byval_arg */
	, &MeshRenderer_t223_1_0_0/* this_arg */
	, &MeshRenderer_t223_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MeshRenderer_t223)/* instance_size */
	, sizeof (MeshRenderer_t223)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// Metadata Definition UnityEngine.Texture
extern TypeInfo Texture_t221_il2cpp_TypeInfo;
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"
static const EncodedMethodIndex Texture_t221_VTable[8] = 
{
	600,
	601,
	602,
	603,
	1282,
	1283,
	1284,
	1285,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Texture_t221_0_0_0;
extern const Il2CppType Texture_t221_1_0_0;
struct Texture_t221;
const Il2CppTypeDefinitionMetadata Texture_t221_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t473_0_0_0/* parent */
	, Texture_t221_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4195/* methodStart */
	, -1/* eventStart */
	, 706/* propertyStart */

};
TypeInfo Texture_t221_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Texture"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Texture_t221_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Texture_t221_0_0_0/* byval_arg */
	, &Texture_t221_1_0_0/* this_arg */
	, &Texture_t221_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Texture_t221)/* instance_size */
	, sizeof (Texture_t221)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"
// Metadata Definition UnityEngine.Texture2D
extern TypeInfo Texture2D_t9_il2cpp_TypeInfo;
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
static const EncodedMethodIndex Texture2D_t9_VTable[8] = 
{
	600,
	601,
	602,
	603,
	1282,
	1283,
	1284,
	1285,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Texture2D_t9_0_0_0;
extern const Il2CppType Texture2D_t9_1_0_0;
struct Texture2D_t9;
const Il2CppTypeDefinitionMetadata Texture2D_t9_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Texture_t221_0_0_0/* parent */
	, Texture2D_t9_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4205/* methodStart */
	, -1/* eventStart */
	, 710/* propertyStart */

};
TypeInfo Texture2D_t9_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Texture2D"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Texture2D_t9_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Texture2D_t9_0_0_0/* byval_arg */
	, &Texture2D_t9_1_0_0/* this_arg */
	, &Texture2D_t9_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Texture2D_t9)/* instance_size */
	, sizeof (Texture2D_t9)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTexture.h"
// Metadata Definition UnityEngine.RenderTexture
extern TypeInfo RenderTexture_t15_il2cpp_TypeInfo;
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTextureMethodDeclarations.h"
static const EncodedMethodIndex RenderTexture_t15_VTable[8] = 
{
	600,
	601,
	602,
	603,
	1286,
	1287,
	1288,
	1289,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderTexture_t15_0_0_0;
extern const Il2CppType RenderTexture_t15_1_0_0;
struct RenderTexture_t15;
const Il2CppTypeDefinitionMetadata RenderTexture_t15_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Texture_t221_0_0_0/* parent */
	, RenderTexture_t15_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4217/* methodStart */
	, -1/* eventStart */
	, 711/* propertyStart */

};
TypeInfo RenderTexture_t15_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderTexture"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &RenderTexture_t15_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderTexture_t15_0_0_0/* byval_arg */
	, &RenderTexture_t15_1_0_0/* this_arg */
	, &RenderTexture_t15_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderTexture_t15)/* instance_size */
	, sizeof (RenderTexture_t15)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 6/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.ReflectionProbe
#include "UnityEngine_UnityEngine_ReflectionProbe.h"
// Metadata Definition UnityEngine.ReflectionProbe
extern TypeInfo ReflectionProbe_t860_il2cpp_TypeInfo;
// UnityEngine.ReflectionProbe
#include "UnityEngine_UnityEngine_ReflectionProbeMethodDeclarations.h"
static const EncodedMethodIndex ReflectionProbe_t860_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ReflectionProbe_t860_0_0_0;
extern const Il2CppType ReflectionProbe_t860_1_0_0;
struct ReflectionProbe_t860;
const Il2CppTypeDefinitionMetadata ReflectionProbe_t860_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t825_0_0_0/* parent */
	, ReflectionProbe_t860_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ReflectionProbe_t860_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReflectionProbe"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &ReflectionProbe_t860_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReflectionProbe_t860_0_0_0/* byval_arg */
	, &ReflectionProbe_t860_1_0_0/* this_arg */
	, &ReflectionProbe_t860_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReflectionProbe_t860)/* instance_size */
	, sizeof (ReflectionProbe_t860)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUIElement
#include "UnityEngine_UnityEngine_GUIElement.h"
// Metadata Definition UnityEngine.GUIElement
extern TypeInfo GUIElement_t861_il2cpp_TypeInfo;
// UnityEngine.GUIElement
#include "UnityEngine_UnityEngine_GUIElementMethodDeclarations.h"
static const EncodedMethodIndex GUIElement_t861_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUIElement_t861_0_0_0;
extern const Il2CppType GUIElement_t861_1_0_0;
struct GUIElement_t861;
const Il2CppTypeDefinitionMetadata GUIElement_t861_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t825_0_0_0/* parent */
	, GUIElement_t861_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GUIElement_t861_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIElement"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUIElement_t861_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUIElement_t861_0_0_0/* byval_arg */
	, &GUIElement_t861_1_0_0/* this_arg */
	, &GUIElement_t861_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUIElement_t861)/* instance_size */
	, sizeof (GUIElement_t861)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUITexture
#include "UnityEngine_UnityEngine_GUITexture.h"
// Metadata Definition UnityEngine.GUITexture
extern TypeInfo GUITexture_t548_il2cpp_TypeInfo;
// UnityEngine.GUITexture
#include "UnityEngine_UnityEngine_GUITextureMethodDeclarations.h"
static const EncodedMethodIndex GUITexture_t548_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUITexture_t548_0_0_0;
extern const Il2CppType GUITexture_t548_1_0_0;
struct GUITexture_t548;
const Il2CppTypeDefinitionMetadata GUITexture_t548_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &GUIElement_t861_0_0_0/* parent */
	, GUITexture_t548_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4245/* methodStart */
	, -1/* eventStart */
	, 717/* propertyStart */

};
TypeInfo GUITexture_t548_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUITexture"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUITexture_t548_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUITexture_t548_0_0_0/* byval_arg */
	, &GUITexture_t548_1_0_0/* this_arg */
	, &GUITexture_t548_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUITexture_t548)/* instance_size */
	, sizeof (GUITexture_t548)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUILayer
#include "UnityEngine_UnityEngine_GUILayer.h"
// Metadata Definition UnityEngine.GUILayer
extern TypeInfo GUILayer_t862_il2cpp_TypeInfo;
// UnityEngine.GUILayer
#include "UnityEngine_UnityEngine_GUILayerMethodDeclarations.h"
static const EncodedMethodIndex GUILayer_t862_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUILayer_t862_0_0_0;
extern const Il2CppType GUILayer_t862_1_0_0;
struct GUILayer_t862;
const Il2CppTypeDefinitionMetadata GUILayer_t862_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Behaviour_t825_0_0_0/* parent */
	, GUILayer_t862_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4250/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GUILayer_t862_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUILayer"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUILayer_t862_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUILayer_t862_0_0_0/* byval_arg */
	, &GUILayer_t862_1_0_0/* this_arg */
	, &GUILayer_t862_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUILayer_t862)/* instance_size */
	, sizeof (GUILayer_t862)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GradientColorKey
#include "UnityEngine_UnityEngine_GradientColorKey.h"
// Metadata Definition UnityEngine.GradientColorKey
extern TypeInfo GradientColorKey_t863_il2cpp_TypeInfo;
// UnityEngine.GradientColorKey
#include "UnityEngine_UnityEngine_GradientColorKeyMethodDeclarations.h"
static const EncodedMethodIndex GradientColorKey_t863_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GradientColorKey_t863_0_0_0;
extern const Il2CppType GradientColorKey_t863_1_0_0;
const Il2CppTypeDefinitionMetadata GradientColorKey_t863_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, GradientColorKey_t863_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3647/* fieldStart */
	, 4252/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GradientColorKey_t863_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GradientColorKey"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GradientColorKey_t863_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GradientColorKey_t863_0_0_0/* byval_arg */
	, &GradientColorKey_t863_1_0_0/* this_arg */
	, &GradientColorKey_t863_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GradientColorKey_t863)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GradientColorKey_t863)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GradientColorKey_t863 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GradientAlphaKey
#include "UnityEngine_UnityEngine_GradientAlphaKey.h"
// Metadata Definition UnityEngine.GradientAlphaKey
extern TypeInfo GradientAlphaKey_t864_il2cpp_TypeInfo;
// UnityEngine.GradientAlphaKey
#include "UnityEngine_UnityEngine_GradientAlphaKeyMethodDeclarations.h"
static const EncodedMethodIndex GradientAlphaKey_t864_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GradientAlphaKey_t864_0_0_0;
extern const Il2CppType GradientAlphaKey_t864_1_0_0;
const Il2CppTypeDefinitionMetadata GradientAlphaKey_t864_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, GradientAlphaKey_t864_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3649/* fieldStart */
	, 4253/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GradientAlphaKey_t864_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GradientAlphaKey"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GradientAlphaKey_t864_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GradientAlphaKey_t864_0_0_0/* byval_arg */
	, &GradientAlphaKey_t864_1_0_0/* this_arg */
	, &GradientAlphaKey_t864_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GradientAlphaKey_t864)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GradientAlphaKey_t864)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GradientAlphaKey_t864 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Gradient
#include "UnityEngine_UnityEngine_Gradient.h"
// Metadata Definition UnityEngine.Gradient
extern TypeInfo Gradient_t865_il2cpp_TypeInfo;
// UnityEngine.Gradient
#include "UnityEngine_UnityEngine_GradientMethodDeclarations.h"
static const EncodedMethodIndex Gradient_t865_VTable[4] = 
{
	626,
	1290,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Gradient_t865_0_0_0;
extern const Il2CppType Gradient_t865_1_0_0;
struct Gradient_t865;
const Il2CppTypeDefinitionMetadata Gradient_t865_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Gradient_t865_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3651/* fieldStart */
	, 4254/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Gradient_t865_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Gradient"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Gradient_t865_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Gradient_t865_0_0_0/* byval_arg */
	, &Gradient_t865_1_0_0/* this_arg */
	, &Gradient_t865_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Gradient_t865_marshal/* marshal_to_native_func */
	, (methodPointerType)Gradient_t865_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Gradient_t865_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Gradient_t865)/* instance_size */
	, sizeof (Gradient_t865)/* actualSize */
	, 0/* element_size */
	, sizeof(Gradient_t865_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.ScaleMode
#include "UnityEngine_UnityEngine_ScaleMode.h"
// Metadata Definition UnityEngine.ScaleMode
extern TypeInfo ScaleMode_t866_il2cpp_TypeInfo;
// UnityEngine.ScaleMode
#include "UnityEngine_UnityEngine_ScaleModeMethodDeclarations.h"
static const EncodedMethodIndex ScaleMode_t866_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ScaleMode_t866_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ScaleMode_t866_0_0_0;
extern const Il2CppType ScaleMode_t866_1_0_0;
const Il2CppTypeDefinitionMetadata ScaleMode_t866_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScaleMode_t866_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ScaleMode_t866_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3652/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ScaleMode_t866_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScaleMode"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScaleMode_t866_0_0_0/* byval_arg */
	, &ScaleMode_t866_1_0_0/* this_arg */
	, &ScaleMode_t866_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScaleMode_t866)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScaleMode_t866)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUI.h"
// Metadata Definition UnityEngine.GUI
extern TypeInfo GUI_t483_il2cpp_TypeInfo;
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
extern const Il2CppType ScrollViewState_t867_0_0_0;
extern const Il2CppType WindowFunction_t868_0_0_0;
static const Il2CppType* GUI_t483_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ScrollViewState_t867_0_0_0,
	&WindowFunction_t868_0_0_0,
};
static const EncodedMethodIndex GUI_t483_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUI_t483_0_0_0;
extern const Il2CppType GUI_t483_1_0_0;
struct GUI_t483;
const Il2CppTypeDefinitionMetadata GUI_t483_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GUI_t483_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUI_t483_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3656/* fieldStart */
	, 4258/* methodStart */
	, -1/* eventStart */
	, 719/* propertyStart */

};
TypeInfo GUI_t483_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUI"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUI_t483_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUI_t483_0_0_0/* byval_arg */
	, &GUI_t483_1_0_0/* this_arg */
	, &GUI_t483_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUI_t483)/* instance_size */
	, sizeof (GUI_t483)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GUI_t483_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 20/* method_count */
	, 6/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUI/ScrollViewState
#include "UnityEngine_UnityEngine_GUI_ScrollViewState.h"
// Metadata Definition UnityEngine.GUI/ScrollViewState
extern TypeInfo ScrollViewState_t867_il2cpp_TypeInfo;
// UnityEngine.GUI/ScrollViewState
#include "UnityEngine_UnityEngine_GUI_ScrollViewStateMethodDeclarations.h"
static const EncodedMethodIndex ScrollViewState_t867_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ScrollViewState_t867_1_0_0;
struct ScrollViewState_t867;
const Il2CppTypeDefinitionMetadata ScrollViewState_t867_DefinitionMetadata = 
{
	&GUI_t483_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ScrollViewState_t867_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3671/* fieldStart */
	, 4278/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ScrollViewState_t867_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrollViewState"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ScrollViewState_t867_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScrollViewState_t867_0_0_0/* byval_arg */
	, &ScrollViewState_t867_1_0_0/* this_arg */
	, &ScrollViewState_t867_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrollViewState_t867)/* instance_size */
	, sizeof (ScrollViewState_t867)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048837/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
// Metadata Definition UnityEngine.GUI/WindowFunction
extern TypeInfo WindowFunction_t868_il2cpp_TypeInfo;
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunctionMethodDeclarations.h"
static const EncodedMethodIndex WindowFunction_t868_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1291,
	1292,
	1293,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
extern const Il2CppType ISerializable_t2210_0_0_0;
static Il2CppInterfaceOffsetPair WindowFunction_t868_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WindowFunction_t868_1_0_0;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
struct WindowFunction_t868;
const Il2CppTypeDefinitionMetadata WindowFunction_t868_DefinitionMetadata = 
{
	&GUI_t483_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WindowFunction_t868_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, WindowFunction_t868_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4279/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WindowFunction_t868_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WindowFunction"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &WindowFunction_t868_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WindowFunction_t868_0_0_0/* byval_arg */
	, &WindowFunction_t868_1_0_0/* this_arg */
	, &WindowFunction_t868_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_WindowFunction_t868/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WindowFunction_t868)/* instance_size */
	, sizeof (WindowFunction_t868)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.GUILayout
#include "UnityEngine_UnityEngine_GUILayout.h"
// Metadata Definition UnityEngine.GUILayout
extern TypeInfo GUILayout_t872_il2cpp_TypeInfo;
// UnityEngine.GUILayout
#include "UnityEngine_UnityEngine_GUILayoutMethodDeclarations.h"
static const EncodedMethodIndex GUILayout_t872_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUILayout_t872_0_0_0;
extern const Il2CppType GUILayout_t872_1_0_0;
struct GUILayout_t872;
const Il2CppTypeDefinitionMetadata GUILayout_t872_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUILayout_t872_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4283/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GUILayout_t872_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUILayout"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUILayout_t872_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUILayout_t872_0_0_0/* byval_arg */
	, &GUILayout_t872_1_0_0/* this_arg */
	, &GUILayout_t872_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUILayout_t872)/* instance_size */
	, sizeof (GUILayout_t872)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtility.h"
// Metadata Definition UnityEngine.GUILayoutUtility
extern TypeInfo GUILayoutUtility_t876_il2cpp_TypeInfo;
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtilityMethodDeclarations.h"
extern const Il2CppType LayoutCache_t874_0_0_0;
static const Il2CppType* GUILayoutUtility_t876_il2cpp_TypeInfo__nestedTypes[1] =
{
	&LayoutCache_t874_0_0_0,
};
static const EncodedMethodIndex GUILayoutUtility_t876_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUILayoutUtility_t876_0_0_0;
extern const Il2CppType GUILayoutUtility_t876_1_0_0;
struct GUILayoutUtility_t876;
const Il2CppTypeDefinitionMetadata GUILayoutUtility_t876_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GUILayoutUtility_t876_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUILayoutUtility_t876_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3677/* fieldStart */
	, 4287/* methodStart */
	, -1/* eventStart */
	, 725/* propertyStart */

};
TypeInfo GUILayoutUtility_t876_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUILayoutUtility"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUILayoutUtility_t876_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUILayoutUtility_t876_0_0_0/* byval_arg */
	, &GUILayoutUtility_t876_1_0_0/* this_arg */
	, &GUILayoutUtility_t876_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUILayoutUtility_t876)/* instance_size */
	, sizeof (GUILayoutUtility_t876)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GUILayoutUtility_t876_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUILayoutUtility/LayoutCache
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCache.h"
// Metadata Definition UnityEngine.GUILayoutUtility/LayoutCache
extern TypeInfo LayoutCache_t874_il2cpp_TypeInfo;
// UnityEngine.GUILayoutUtility/LayoutCache
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCacheMethodDeclarations.h"
static const EncodedMethodIndex LayoutCache_t874_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType LayoutCache_t874_1_0_0;
struct LayoutCache_t874;
const Il2CppTypeDefinitionMetadata LayoutCache_t874_DefinitionMetadata = 
{
	&GUILayoutUtility_t876_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LayoutCache_t874_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3682/* fieldStart */
	, 4301/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LayoutCache_t874_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutCache"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &LayoutCache_t874_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LayoutCache_t874_0_0_0/* byval_arg */
	, &LayoutCache_t874_1_0_0/* this_arg */
	, &LayoutCache_t874_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutCache_t874)/* instance_size */
	, sizeof (LayoutCache_t874)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048837/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntry.h"
// Metadata Definition UnityEngine.GUILayoutEntry
extern TypeInfo GUILayoutEntry_t877_il2cpp_TypeInfo;
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntryMethodDeclarations.h"
static const EncodedMethodIndex GUILayoutEntry_t877_VTable[11] = 
{
	626,
	601,
	627,
	1294,
	1295,
	1296,
	1297,
	1298,
	1299,
	1300,
	1301,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUILayoutEntry_t877_0_0_0;
extern const Il2CppType GUILayoutEntry_t877_1_0_0;
struct GUILayoutEntry_t877;
const Il2CppTypeDefinitionMetadata GUILayoutEntry_t877_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUILayoutEntry_t877_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3685/* fieldStart */
	, 4302/* methodStart */
	, -1/* eventStart */
	, 726/* propertyStart */

};
TypeInfo GUILayoutEntry_t877_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUILayoutEntry"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUILayoutEntry_t877_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUILayoutEntry_t877_0_0_0/* byval_arg */
	, &GUILayoutEntry_t877_1_0_0/* this_arg */
	, &GUILayoutEntry_t877_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUILayoutEntry_t877)/* instance_size */
	, sizeof (GUILayoutEntry_t877)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GUILayoutEntry_t877_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 2/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUILayoutGroup
#include "UnityEngine_UnityEngine_GUILayoutGroup.h"
// Metadata Definition UnityEngine.GUILayoutGroup
extern TypeInfo GUILayoutGroup_t873_il2cpp_TypeInfo;
// UnityEngine.GUILayoutGroup
#include "UnityEngine_UnityEngine_GUILayoutGroupMethodDeclarations.h"
static const EncodedMethodIndex GUILayoutGroup_t873_VTable[11] = 
{
	626,
	601,
	627,
	1302,
	1303,
	1304,
	1305,
	1306,
	1307,
	1308,
	1309,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUILayoutGroup_t873_0_0_0;
extern const Il2CppType GUILayoutGroup_t873_1_0_0;
struct GUILayoutGroup_t873;
const Il2CppTypeDefinitionMetadata GUILayoutGroup_t873_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &GUILayoutEntry_t877_0_0_0/* parent */
	, GUILayoutGroup_t873_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3695/* fieldStart */
	, 4315/* methodStart */
	, -1/* eventStart */
	, 728/* propertyStart */

};
TypeInfo GUILayoutGroup_t873_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUILayoutGroup"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUILayoutGroup_t873_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUILayoutGroup_t873_0_0_0/* byval_arg */
	, &GUILayoutGroup_t873_1_0_0/* this_arg */
	, &GUILayoutGroup_t873_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUILayoutGroup_t873)/* instance_size */
	, sizeof (GUILayoutGroup_t873)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 1/* property_count */
	, 17/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUIScrollGroup
#include "UnityEngine_UnityEngine_GUIScrollGroup.h"
// Metadata Definition UnityEngine.GUIScrollGroup
extern TypeInfo GUIScrollGroup_t879_il2cpp_TypeInfo;
// UnityEngine.GUIScrollGroup
#include "UnityEngine_UnityEngine_GUIScrollGroupMethodDeclarations.h"
static const EncodedMethodIndex GUIScrollGroup_t879_VTable[11] = 
{
	626,
	601,
	627,
	1302,
	1303,
	1310,
	1311,
	1312,
	1313,
	1308,
	1309,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUIScrollGroup_t879_0_0_0;
extern const Il2CppType GUIScrollGroup_t879_1_0_0;
struct GUIScrollGroup_t879;
const Il2CppTypeDefinitionMetadata GUIScrollGroup_t879_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &GUILayoutGroup_t873_0_0_0/* parent */
	, GUIScrollGroup_t879_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3712/* fieldStart */
	, 4327/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GUIScrollGroup_t879_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIScrollGroup"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUIScrollGroup_t879_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUIScrollGroup_t879_0_0_0/* byval_arg */
	, &GUIScrollGroup_t879_1_0_0/* this_arg */
	, &GUIScrollGroup_t879_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUIScrollGroup_t879)/* instance_size */
	, sizeof (GUIScrollGroup_t879)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUIWordWrapSizer
#include "UnityEngine_UnityEngine_GUIWordWrapSizer.h"
// Metadata Definition UnityEngine.GUIWordWrapSizer
extern TypeInfo GUIWordWrapSizer_t880_il2cpp_TypeInfo;
// UnityEngine.GUIWordWrapSizer
#include "UnityEngine_UnityEngine_GUIWordWrapSizerMethodDeclarations.h"
static const EncodedMethodIndex GUIWordWrapSizer_t880_VTable[11] = 
{
	626,
	601,
	627,
	1294,
	1295,
	1314,
	1315,
	1298,
	1299,
	1300,
	1301,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUIWordWrapSizer_t880_0_0_0;
extern const Il2CppType GUIWordWrapSizer_t880_1_0_0;
struct GUIWordWrapSizer_t880;
const Il2CppTypeDefinitionMetadata GUIWordWrapSizer_t880_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &GUILayoutEntry_t877_0_0_0/* parent */
	, GUIWordWrapSizer_t880_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3724/* fieldStart */
	, 4332/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GUIWordWrapSizer_t880_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIWordWrapSizer"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUIWordWrapSizer_t880_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUIWordWrapSizer_t880_0_0_0/* byval_arg */
	, &GUIWordWrapSizer_t880_1_0_0/* this_arg */
	, &GUIWordWrapSizer_t880_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUIWordWrapSizer_t880)/* instance_size */
	, sizeof (GUIWordWrapSizer_t880)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUILayoutOption
#include "UnityEngine_UnityEngine_GUILayoutOption.h"
// Metadata Definition UnityEngine.GUILayoutOption
extern TypeInfo GUILayoutOption_t882_il2cpp_TypeInfo;
// UnityEngine.GUILayoutOption
#include "UnityEngine_UnityEngine_GUILayoutOptionMethodDeclarations.h"
extern const Il2CppType Type_t881_0_0_0;
static const Il2CppType* GUILayoutOption_t882_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Type_t881_0_0_0,
};
static const EncodedMethodIndex GUILayoutOption_t882_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUILayoutOption_t882_0_0_0;
extern const Il2CppType GUILayoutOption_t882_1_0_0;
struct GUILayoutOption_t882;
const Il2CppTypeDefinitionMetadata GUILayoutOption_t882_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GUILayoutOption_t882_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUILayoutOption_t882_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3727/* fieldStart */
	, 4335/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GUILayoutOption_t882_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUILayoutOption"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUILayoutOption_t882_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUILayoutOption_t882_0_0_0/* byval_arg */
	, &GUILayoutOption_t882_1_0_0/* this_arg */
	, &GUILayoutOption_t882_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUILayoutOption_t882)/* instance_size */
	, sizeof (GUILayoutOption_t882)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUILayoutOption/Type
#include "UnityEngine_UnityEngine_GUILayoutOption_Type.h"
// Metadata Definition UnityEngine.GUILayoutOption/Type
extern TypeInfo Type_t881_il2cpp_TypeInfo;
// UnityEngine.GUILayoutOption/Type
#include "UnityEngine_UnityEngine_GUILayoutOption_TypeMethodDeclarations.h"
static const EncodedMethodIndex Type_t881_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Type_t881_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Type_t881_1_0_0;
const Il2CppTypeDefinitionMetadata Type_t881_DefinitionMetadata = 
{
	&GUILayoutOption_t882_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Type_t881_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Type_t881_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3729/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Type_t881_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Type"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Type_t881_0_0_0/* byval_arg */
	, &Type_t881_1_0_0/* this_arg */
	, &Type_t881_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Type_t881)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Type_t881)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 261/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.ExitGUIException
#include "UnityEngine_UnityEngine_ExitGUIException.h"
// Metadata Definition UnityEngine.ExitGUIException
extern TypeInfo ExitGUIException_t883_il2cpp_TypeInfo;
// UnityEngine.ExitGUIException
#include "UnityEngine_UnityEngine_ExitGUIExceptionMethodDeclarations.h"
static const EncodedMethodIndex ExitGUIException_t883_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
extern const Il2CppType _Exception_t3443_0_0_0;
static Il2CppInterfaceOffsetPair ExitGUIException_t883_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ExitGUIException_t883_0_0_0;
extern const Il2CppType ExitGUIException_t883_1_0_0;
extern const Il2CppType Exception_t520_0_0_0;
struct ExitGUIException_t883;
const Il2CppTypeDefinitionMetadata ExitGUIException_t883_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExitGUIException_t883_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t520_0_0_0/* parent */
	, ExitGUIException_t883_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ExitGUIException_t883_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExitGUIException"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &ExitGUIException_t883_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExitGUIException_t883_0_0_0/* byval_arg */
	, &ExitGUIException_t883_1_0_0/* this_arg */
	, &ExitGUIException_t883_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExitGUIException_t883)/* instance_size */
	, sizeof (ExitGUIException_t883)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.GUIUtility
#include "UnityEngine_UnityEngine_GUIUtility.h"
// Metadata Definition UnityEngine.GUIUtility
extern TypeInfo GUIUtility_t884_il2cpp_TypeInfo;
// UnityEngine.GUIUtility
#include "UnityEngine_UnityEngine_GUIUtilityMethodDeclarations.h"
static const EncodedMethodIndex GUIUtility_t884_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUIUtility_t884_0_0_0;
extern const Il2CppType GUIUtility_t884_1_0_0;
struct GUIUtility_t884;
const Il2CppTypeDefinitionMetadata GUIUtility_t884_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUIUtility_t884_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3744/* fieldStart */
	, 4336/* methodStart */
	, -1/* eventStart */
	, 729/* propertyStart */

};
TypeInfo GUIUtility_t884_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIUtility"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUIUtility_t884_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUIUtility_t884_0_0_0/* byval_arg */
	, &GUIUtility_t884_1_0_0/* this_arg */
	, &GUIUtility_t884_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUIUtility_t884)/* instance_size */
	, sizeof (GUIUtility_t884)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GUIUtility_t884_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUISettings
#include "UnityEngine_UnityEngine_GUISettings.h"
// Metadata Definition UnityEngine.GUISettings
extern TypeInfo GUISettings_t885_il2cpp_TypeInfo;
// UnityEngine.GUISettings
#include "UnityEngine_UnityEngine_GUISettingsMethodDeclarations.h"
static const EncodedMethodIndex GUISettings_t885_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUISettings_t885_0_0_0;
extern const Il2CppType GUISettings_t885_1_0_0;
struct GUISettings_t885;
const Il2CppTypeDefinitionMetadata GUISettings_t885_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUISettings_t885_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3748/* fieldStart */
	, 4347/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GUISettings_t885_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUISettings"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUISettings_t885_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUISettings_t885_0_0_0/* byval_arg */
	, &GUISettings_t885_1_0_0/* this_arg */
	, &GUISettings_t885_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUISettings_t885)/* instance_size */
	, sizeof (GUISettings_t885)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkin.h"
// Metadata Definition UnityEngine.GUISkin
extern TypeInfo GUISkin_t869_il2cpp_TypeInfo;
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
extern const Il2CppType SkinChangedDelegate_t886_0_0_0;
static const Il2CppType* GUISkin_t869_il2cpp_TypeInfo__nestedTypes[1] =
{
	&SkinChangedDelegate_t886_0_0_0,
};
static const EncodedMethodIndex GUISkin_t869_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUISkin_t869_0_0_0;
extern const Il2CppType GUISkin_t869_1_0_0;
struct GUISkin_t869;
const Il2CppTypeDefinitionMetadata GUISkin_t869_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GUISkin_t869_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ScriptableObject_t838_0_0_0/* parent */
	, GUISkin_t869_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3753/* fieldStart */
	, 4348/* methodStart */
	, -1/* eventStart */
	, 730/* propertyStart */

};
TypeInfo GUISkin_t869_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUISkin"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUISkin_t869_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1505/* custom_attributes_cache */
	, &GUISkin_t869_0_0_0/* byval_arg */
	, &GUISkin_t869_1_0_0/* this_arg */
	, &GUISkin_t869_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUISkin_t869)/* instance_size */
	, sizeof (GUISkin_t869)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GUISkin_t869_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 54/* method_count */
	, 24/* property_count */
	, 27/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUISkin/SkinChangedDelegate
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegate.h"
// Metadata Definition UnityEngine.GUISkin/SkinChangedDelegate
extern TypeInfo SkinChangedDelegate_t886_il2cpp_TypeInfo;
// UnityEngine.GUISkin/SkinChangedDelegate
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegateMethodDeclarations.h"
static const EncodedMethodIndex SkinChangedDelegate_t886_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1323,
	1324,
	1325,
};
static Il2CppInterfaceOffsetPair SkinChangedDelegate_t886_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SkinChangedDelegate_t886_1_0_0;
struct SkinChangedDelegate_t886;
const Il2CppTypeDefinitionMetadata SkinChangedDelegate_t886_DefinitionMetadata = 
{
	&GUISkin_t869_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SkinChangedDelegate_t886_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, SkinChangedDelegate_t886_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4402/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SkinChangedDelegate_t886_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SkinChangedDelegate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SkinChangedDelegate_t886_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SkinChangedDelegate_t886_0_0_0/* byval_arg */
	, &SkinChangedDelegate_t886_1_0_0/* this_arg */
	, &SkinChangedDelegate_t886_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_SkinChangedDelegate_t886/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SkinChangedDelegate_t886)/* instance_size */
	, sizeof (SkinChangedDelegate_t886)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 261/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContent.h"
// Metadata Definition UnityEngine.GUIContent
extern TypeInfo GUIContent_t807_il2cpp_TypeInfo;
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
static const EncodedMethodIndex GUIContent_t807_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUIContent_t807_0_0_0;
extern const Il2CppType GUIContent_t807_1_0_0;
struct GUIContent_t807;
const Il2CppTypeDefinitionMetadata GUIContent_t807_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUIContent_t807_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3780/* fieldStart */
	, 4406/* methodStart */
	, -1/* eventStart */
	, 754/* propertyStart */

};
TypeInfo GUIContent_t807_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIContent"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUIContent_t807_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUIContent_t807_0_0_0/* byval_arg */
	, &GUIContent_t807_1_0_0/* this_arg */
	, &GUIContent_t807_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUIContent_t807)/* instance_size */
	, sizeof (GUIContent_t807)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GUIContent_t807_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056777/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.GUIStyleState
#include "UnityEngine_UnityEngine_GUIStyleState.h"
// Metadata Definition UnityEngine.GUIStyleState
extern TypeInfo GUIStyleState_t523_il2cpp_TypeInfo;
// UnityEngine.GUIStyleState
#include "UnityEngine_UnityEngine_GUIStyleStateMethodDeclarations.h"
static const EncodedMethodIndex GUIStyleState_t523_VTable[4] = 
{
	626,
	1326,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUIStyleState_t523_0_0_0;
extern const Il2CppType GUIStyleState_t523_1_0_0;
struct GUIStyleState_t523;
const Il2CppTypeDefinitionMetadata GUIStyleState_t523_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUIStyleState_t523_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3787/* fieldStart */
	, 4414/* methodStart */
	, -1/* eventStart */
	, 755/* propertyStart */

};
TypeInfo GUIStyleState_t523_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIStyleState"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUIStyleState_t523_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUIStyleState_t523_0_0_0/* byval_arg */
	, &GUIStyleState_t523_1_0_0/* this_arg */
	, &GUIStyleState_t523_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUIStyleState_t523)/* instance_size */
	, sizeof (GUIStyleState_t523)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffset.h"
// Metadata Definition UnityEngine.RectOffset
extern TypeInfo RectOffset_t741_il2cpp_TypeInfo;
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
static const EncodedMethodIndex RectOffset_t741_VTable[4] = 
{
	626,
	1327,
	627,
	1328,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RectOffset_t741_0_0_0;
extern const Il2CppType RectOffset_t741_1_0_0;
struct RectOffset_t741;
const Il2CppTypeDefinitionMetadata RectOffset_t741_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RectOffset_t741_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3790/* fieldStart */
	, 4422/* methodStart */
	, -1/* eventStart */
	, 756/* propertyStart */

};
TypeInfo RectOffset_t741_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RectOffset"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &RectOffset_t741_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RectOffset_t741_0_0_0/* byval_arg */
	, &RectOffset_t741_1_0_0/* this_arg */
	, &RectOffset_t741_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RectOffset_t741)/* instance_size */
	, sizeof (RectOffset_t741)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 6/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
// Metadata Definition UnityEngine.FontStyle
extern TypeInfo FontStyle_t889_il2cpp_TypeInfo;
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyleMethodDeclarations.h"
static const EncodedMethodIndex FontStyle_t889_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair FontStyle_t889_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType FontStyle_t889_0_0_0;
extern const Il2CppType FontStyle_t889_1_0_0;
const Il2CppTypeDefinitionMetadata FontStyle_t889_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FontStyle_t889_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, FontStyle_t889_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3792/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FontStyle_t889_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FontStyle"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FontStyle_t889_0_0_0/* byval_arg */
	, &FontStyle_t889_1_0_0/* this_arg */
	, &FontStyle_t889_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FontStyle_t889)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FontStyle_t889)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.ImagePosition
#include "UnityEngine_UnityEngine_ImagePosition.h"
// Metadata Definition UnityEngine.ImagePosition
extern TypeInfo ImagePosition_t890_il2cpp_TypeInfo;
// UnityEngine.ImagePosition
#include "UnityEngine_UnityEngine_ImagePositionMethodDeclarations.h"
static const EncodedMethodIndex ImagePosition_t890_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ImagePosition_t890_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ImagePosition_t890_0_0_0;
extern const Il2CppType ImagePosition_t890_1_0_0;
const Il2CppTypeDefinitionMetadata ImagePosition_t890_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ImagePosition_t890_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ImagePosition_t890_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3797/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ImagePosition_t890_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ImagePosition"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ImagePosition_t890_0_0_0/* byval_arg */
	, &ImagePosition_t890_1_0_0/* this_arg */
	, &ImagePosition_t890_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ImagePosition_t890)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ImagePosition_t890)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
// Metadata Definition UnityEngine.GUIStyle
extern TypeInfo GUIStyle_t273_il2cpp_TypeInfo;
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
static const EncodedMethodIndex GUIStyle_t273_VTable[4] = 
{
	626,
	1329,
	627,
	1330,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GUIStyle_t273_0_0_0;
extern const Il2CppType GUIStyle_t273_1_0_0;
struct GUIStyle_t273;
const Il2CppTypeDefinitionMetadata GUIStyle_t273_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GUIStyle_t273_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3802/* fieldStart */
	, 4441/* methodStart */
	, -1/* eventStart */
	, 762/* propertyStart */

};
TypeInfo GUIStyle_t273_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIStyle"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &GUIStyle_t273_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GUIStyle_t273_0_0_0/* byval_arg */
	, &GUIStyle_t273_1_0_0/* this_arg */
	, &GUIStyle_t273_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GUIStyle_t273)/* instance_size */
	, sizeof (GUIStyle_t273)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GUIStyle_t273_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 39/* method_count */
	, 16/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
// Metadata Definition UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
extern TypeInfo TouchScreenKeyboard_InternalConstructorHelperArguments_t891_il2cpp_TypeInfo;
// UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstrucMethodDeclarations.h"
static const EncodedMethodIndex TouchScreenKeyboard_InternalConstructorHelperArguments_t891_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TouchScreenKeyboard_InternalConstructorHelperArguments_t891_0_0_0;
extern const Il2CppType TouchScreenKeyboard_InternalConstructorHelperArguments_t891_1_0_0;
const Il2CppTypeDefinitionMetadata TouchScreenKeyboard_InternalConstructorHelperArguments_t891_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, TouchScreenKeyboard_InternalConstructorHelperArguments_t891_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3818/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TouchScreenKeyboard_InternalConstructorHelperArguments_t891_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TouchScreenKeyboard_InternalConstructorHelperArguments"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &TouchScreenKeyboard_InternalConstructorHelperArguments_t891_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TouchScreenKeyboard_InternalConstructorHelperArguments_t891_0_0_0/* byval_arg */
	, &TouchScreenKeyboard_InternalConstructorHelperArguments_t891_1_0_0/* this_arg */
	, &TouchScreenKeyboard_InternalConstructorHelperArguments_t891_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TouchScreenKeyboard_InternalConstructorHelperArguments_t891)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TouchScreenKeyboard_InternalConstructorHelperArguments_t891)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(TouchScreenKeyboard_InternalConstructorHelperArguments_t891 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TouchScreenKeyboardType
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
// Metadata Definition UnityEngine.TouchScreenKeyboardType
extern TypeInfo TouchScreenKeyboardType_t805_il2cpp_TypeInfo;
// UnityEngine.TouchScreenKeyboardType
#include "UnityEngine_UnityEngine_TouchScreenKeyboardTypeMethodDeclarations.h"
static const EncodedMethodIndex TouchScreenKeyboardType_t805_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair TouchScreenKeyboardType_t805_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TouchScreenKeyboardType_t805_0_0_0;
extern const Il2CppType TouchScreenKeyboardType_t805_1_0_0;
const Il2CppTypeDefinitionMetadata TouchScreenKeyboardType_t805_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TouchScreenKeyboardType_t805_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, TouchScreenKeyboardType_t805_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3823/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TouchScreenKeyboardType_t805_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TouchScreenKeyboardType"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TouchScreenKeyboardType_t805_0_0_0/* byval_arg */
	, &TouchScreenKeyboardType_t805_1_0_0/* this_arg */
	, &TouchScreenKeyboardType_t805_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TouchScreenKeyboardType_t805)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TouchScreenKeyboardType_t805)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TouchScreenKeyboard
#include "UnityEngine_UnityEngine_TouchScreenKeyboard.h"
// Metadata Definition UnityEngine.TouchScreenKeyboard
extern TypeInfo TouchScreenKeyboard_t683_il2cpp_TypeInfo;
// UnityEngine.TouchScreenKeyboard
#include "UnityEngine_UnityEngine_TouchScreenKeyboardMethodDeclarations.h"
static const EncodedMethodIndex TouchScreenKeyboard_t683_VTable[4] = 
{
	626,
	1331,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TouchScreenKeyboard_t683_0_0_0;
extern const Il2CppType TouchScreenKeyboard_t683_1_0_0;
struct TouchScreenKeyboard_t683;
const Il2CppTypeDefinitionMetadata TouchScreenKeyboard_t683_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TouchScreenKeyboard_t683_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3832/* fieldStart */
	, 4480/* methodStart */
	, -1/* eventStart */
	, 778/* propertyStart */

};
TypeInfo TouchScreenKeyboard_t683_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TouchScreenKeyboard"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &TouchScreenKeyboard_t683_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TouchScreenKeyboard_t683_0_0_0/* byval_arg */
	, &TouchScreenKeyboard_t683_1_0_0/* this_arg */
	, &TouchScreenKeyboard_t683_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TouchScreenKeyboard_t683)/* instance_size */
	, sizeof (TouchScreenKeyboard_t683)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 6/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Event
#include "UnityEngine_UnityEngine_Event.h"
// Metadata Definition UnityEngine.Event
extern TypeInfo Event_t481_il2cpp_TypeInfo;
// UnityEngine.Event
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
static const EncodedMethodIndex Event_t481_VTable[4] = 
{
	1332,
	1333,
	1334,
	1335,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Event_t481_0_0_0;
extern const Il2CppType Event_t481_1_0_0;
struct Event_t481;
const Il2CppTypeDefinitionMetadata Event_t481_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Event_t481_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3833/* fieldStart */
	, 4495/* methodStart */
	, -1/* eventStart */
	, 784/* propertyStart */

};
TypeInfo Event_t481_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Event"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Event_t481_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Event_t481_0_0_0/* byval_arg */
	, &Event_t481_1_0_0/* this_arg */
	, &Event_t481_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Event_t481_marshal/* marshal_to_native_func */
	, (methodPointerType)Event_t481_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Event_t481_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Event_t481)/* instance_size */
	, sizeof (Event_t481)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Event_t481_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 10/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
// Metadata Definition UnityEngine.KeyCode
extern TypeInfo KeyCode_t892_il2cpp_TypeInfo;
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCodeMethodDeclarations.h"
static const EncodedMethodIndex KeyCode_t892_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair KeyCode_t892_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType KeyCode_t892_0_0_0;
extern const Il2CppType KeyCode_t892_1_0_0;
const Il2CppTypeDefinitionMetadata KeyCode_t892_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, KeyCode_t892_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, KeyCode_t892_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3837/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo KeyCode_t892_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyCode"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyCode_t892_0_0_0/* byval_arg */
	, &KeyCode_t892_1_0_0/* this_arg */
	, &KeyCode_t892_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyCode_t892)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (KeyCode_t892)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 322/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventType.h"
// Metadata Definition UnityEngine.EventType
extern TypeInfo EventType_t893_il2cpp_TypeInfo;
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventTypeMethodDeclarations.h"
static const EncodedMethodIndex EventType_t893_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair EventType_t893_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType EventType_t893_0_0_0;
extern const Il2CppType EventType_t893_1_0_0;
const Il2CppTypeDefinitionMetadata EventType_t893_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventType_t893_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, EventType_t893_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4159/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EventType_t893_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventType"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EventType_t893_0_0_0/* byval_arg */
	, &EventType_t893_1_0_0/* this_arg */
	, &EventType_t893_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventType_t893)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EventType_t893)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 31/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiers.h"
// Metadata Definition UnityEngine.EventModifiers
extern TypeInfo EventModifiers_t894_il2cpp_TypeInfo;
// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiersMethodDeclarations.h"
static const EncodedMethodIndex EventModifiers_t894_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair EventModifiers_t894_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType EventModifiers_t894_0_0_0;
extern const Il2CppType EventModifiers_t894_1_0_0;
const Il2CppTypeDefinitionMetadata EventModifiers_t894_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventModifiers_t894_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, EventModifiers_t894_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4190/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EventModifiers_t894_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventModifiers"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1602/* custom_attributes_cache */
	, &EventModifiers_t894_0_0_0/* byval_arg */
	, &EventModifiers_t894_1_0_0/* this_arg */
	, &EventModifiers_t894_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventModifiers_t894)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EventModifiers_t894)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Gizmos
#include "UnityEngine_UnityEngine_Gizmos.h"
// Metadata Definition UnityEngine.Gizmos
extern TypeInfo Gizmos_t895_il2cpp_TypeInfo;
// UnityEngine.Gizmos
#include "UnityEngine_UnityEngine_GizmosMethodDeclarations.h"
static const EncodedMethodIndex Gizmos_t895_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Gizmos_t895_0_0_0;
extern const Il2CppType Gizmos_t895_1_0_0;
struct Gizmos_t895;
const Il2CppTypeDefinitionMetadata Gizmos_t895_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Gizmos_t895_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4516/* methodStart */
	, -1/* eventStart */
	, 794/* propertyStart */

};
TypeInfo Gizmos_t895_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Gizmos"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Gizmos_t895_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Gizmos_t895_0_0_0/* byval_arg */
	, &Gizmos_t895_1_0_0/* this_arg */
	, &Gizmos_t895_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Gizmos_t895)/* instance_size */
	, sizeof (Gizmos_t895)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// Metadata Definition UnityEngine.Vector2
extern TypeInfo Vector2_t7_il2cpp_TypeInfo;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
static const EncodedMethodIndex Vector2_t7_VTable[4] = 
{
	1336,
	601,
	1337,
	1338,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Vector2_t7_0_0_0;
extern const Il2CppType Vector2_t7_1_0_0;
const Il2CppTypeDefinitionMetadata Vector2_t7_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Vector2_t7_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4199/* fieldStart */
	, 4522/* methodStart */
	, -1/* eventStart */
	, 795/* propertyStart */

};
TypeInfo Vector2_t7_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector2"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Vector2_t7_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1607/* custom_attributes_cache */
	, &Vector2_t7_0_0_0/* byval_arg */
	, &Vector2_t7_1_0_0/* this_arg */
	, &Vector2_t7_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector2_t7)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Vector2_t7)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Vector2_t7 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 21/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Metadata Definition UnityEngine.Vector3
extern TypeInfo Vector3_t215_il2cpp_TypeInfo;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
static const EncodedMethodIndex Vector3_t215_VTable[4] = 
{
	1339,
	601,
	1340,
	1341,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Vector3_t215_0_0_0;
extern const Il2CppType Vector3_t215_1_0_0;
const Il2CppTypeDefinitionMetadata Vector3_t215_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Vector3_t215_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4202/* fieldStart */
	, 4543/* methodStart */
	, -1/* eventStart */
	, 800/* propertyStart */

};
TypeInfo Vector3_t215_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector3"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Vector3_t215_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1608/* custom_attributes_cache */
	, &Vector3_t215_0_0_0/* byval_arg */
	, &Vector3_t215_1_0_0/* this_arg */
	, &Vector3_t215_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector3_t215)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Vector3_t215)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Vector3_t215 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 38/* method_count */
	, 12/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// Metadata Definition UnityEngine.Color
extern TypeInfo Color_t6_il2cpp_TypeInfo;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
static const EncodedMethodIndex Color_t6_VTable[4] = 
{
	1342,
	601,
	1343,
	1344,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Color_t6_0_0_0;
extern const Il2CppType Color_t6_1_0_0;
const Il2CppTypeDefinitionMetadata Color_t6_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Color_t6_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4206/* fieldStart */
	, 4581/* methodStart */
	, -1/* eventStart */
	, 812/* propertyStart */

};
TypeInfo Color_t6_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Color"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Color_t6_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1609/* custom_attributes_cache */
	, &Color_t6_0_0_0/* byval_arg */
	, &Color_t6_1_0_0/* this_arg */
	, &Color_t6_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Color_t6)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Color_t6)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Color_t6 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 16/* method_count */
	, 7/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// Metadata Definition UnityEngine.Color32
extern TypeInfo Color32_t778_il2cpp_TypeInfo;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
static const EncodedMethodIndex Color32_t778_VTable[4] = 
{
	652,
	601,
	653,
	1345,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Color32_t778_0_0_0;
extern const Il2CppType Color32_t778_1_0_0;
const Il2CppTypeDefinitionMetadata Color32_t778_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Color32_t778_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4210/* fieldStart */
	, 4597/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Color32_t778_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Color32"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Color32_t778_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1610/* custom_attributes_cache */
	, &Color32_t778_0_0_0/* byval_arg */
	, &Color32_t778_1_0_0/* this_arg */
	, &Color32_t778_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Color32_t778)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Color32_t778)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Color32_t778 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// Metadata Definition UnityEngine.Quaternion
extern TypeInfo Quaternion_t261_il2cpp_TypeInfo;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
static const EncodedMethodIndex Quaternion_t261_VTable[4] = 
{
	1346,
	601,
	1347,
	1348,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Quaternion_t261_0_0_0;
extern const Il2CppType Quaternion_t261_1_0_0;
const Il2CppTypeDefinitionMetadata Quaternion_t261_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Quaternion_t261_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4214/* fieldStart */
	, 4601/* methodStart */
	, -1/* eventStart */
	, 819/* propertyStart */

};
TypeInfo Quaternion_t261_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Quaternion"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Quaternion_t261_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1611/* custom_attributes_cache */
	, &Quaternion_t261_0_0_0/* byval_arg */
	, &Quaternion_t261_1_0_0/* this_arg */
	, &Quaternion_t261_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Quaternion_t261)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Quaternion_t261)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Quaternion_t261 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 24/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// Metadata Definition UnityEngine.Rect
extern TypeInfo Rect_t225_il2cpp_TypeInfo;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
static const EncodedMethodIndex Rect_t225_VTable[4] = 
{
	1349,
	601,
	1350,
	1351,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Rect_t225_0_0_0;
extern const Il2CppType Rect_t225_1_0_0;
const Il2CppTypeDefinitionMetadata Rect_t225_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Rect_t225_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4219/* fieldStart */
	, 4625/* methodStart */
	, -1/* eventStart */
	, 821/* propertyStart */

};
TypeInfo Rect_t225_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Rect"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Rect_t225_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Rect_t225_0_0_0/* byval_arg */
	, &Rect_t225_1_0_0/* this_arg */
	, &Rect_t225_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Rect_t225)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Rect_t225)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Rect_t225 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 23/* method_count */
	, 11/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// Metadata Definition UnityEngine.Matrix4x4
extern TypeInfo Matrix4x4_t242_il2cpp_TypeInfo;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
static const EncodedMethodIndex Matrix4x4_t242_VTable[4] = 
{
	1352,
	601,
	1353,
	1354,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Matrix4x4_t242_0_0_0;
extern const Il2CppType Matrix4x4_t242_1_0_0;
const Il2CppTypeDefinitionMetadata Matrix4x4_t242_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Matrix4x4_t242_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4223/* fieldStart */
	, 4648/* methodStart */
	, -1/* eventStart */
	, 832/* propertyStart */

};
TypeInfo Matrix4x4_t242_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Matrix4x4"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Matrix4x4_t242_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1619/* custom_attributes_cache */
	, &Matrix4x4_t242_0_0_0/* byval_arg */
	, &Matrix4x4_t242_1_0_0/* this_arg */
	, &Matrix4x4_t242_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Matrix4x4_t242)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Matrix4x4_t242)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Matrix4x4_t242 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 36/* method_count */
	, 7/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
// Metadata Definition UnityEngine.Bounds
extern TypeInfo Bounds_t703_il2cpp_TypeInfo;
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_BoundsMethodDeclarations.h"
static const EncodedMethodIndex Bounds_t703_VTable[4] = 
{
	1355,
	601,
	1356,
	1357,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Bounds_t703_0_0_0;
extern const Il2CppType Bounds_t703_1_0_0;
const Il2CppTypeDefinitionMetadata Bounds_t703_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Bounds_t703_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4239/* fieldStart */
	, 4684/* methodStart */
	, -1/* eventStart */
	, 839/* propertyStart */

};
TypeInfo Bounds_t703_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Bounds"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Bounds_t703_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Bounds_t703_0_0_0/* byval_arg */
	, &Bounds_t703_1_0_0/* this_arg */
	, &Bounds_t703_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Bounds_t703)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Bounds_t703)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Bounds_t703 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 36/* method_count */
	, 5/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// Metadata Definition UnityEngine.Vector4
extern TypeInfo Vector4_t5_il2cpp_TypeInfo;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
static const EncodedMethodIndex Vector4_t5_VTable[4] = 
{
	1358,
	601,
	1359,
	1360,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Vector4_t5_0_0_0;
extern const Il2CppType Vector4_t5_1_0_0;
const Il2CppTypeDefinitionMetadata Vector4_t5_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Vector4_t5_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4241/* fieldStart */
	, 4720/* methodStart */
	, -1/* eventStart */
	, 844/* propertyStart */

};
TypeInfo Vector4_t5_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector4"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Vector4_t5_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1631/* custom_attributes_cache */
	, &Vector4_t5_0_0_0/* byval_arg */
	, &Vector4_t5_1_0_0/* this_arg */
	, &Vector4_t5_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector4_t5)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Vector4_t5)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Vector4_t5 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 16/* method_count */
	, 3/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// Metadata Definition UnityEngine.Ray
extern TypeInfo Ray_t465_il2cpp_TypeInfo;
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
static const EncodedMethodIndex Ray_t465_VTable[4] = 
{
	652,
	601,
	653,
	1361,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Ray_t465_0_0_0;
extern const Il2CppType Ray_t465_1_0_0;
const Il2CppTypeDefinitionMetadata Ray_t465_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Ray_t465_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4246/* fieldStart */
	, 4736/* methodStart */
	, -1/* eventStart */
	, 847/* propertyStart */

};
TypeInfo Ray_t465_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Ray"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Ray_t465_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Ray_t465_0_0_0/* byval_arg */
	, &Ray_t465_1_0_0/* this_arg */
	, &Ray_t465_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Ray_t465)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Ray_t465)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Ray_t465 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Plane
#include "UnityEngine_UnityEngine_Plane.h"
// Metadata Definition UnityEngine.Plane
extern TypeInfo Plane_t808_il2cpp_TypeInfo;
// UnityEngine.Plane
#include "UnityEngine_UnityEngine_PlaneMethodDeclarations.h"
static const EncodedMethodIndex Plane_t808_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Plane_t808_0_0_0;
extern const Il2CppType Plane_t808_1_0_0;
const Il2CppTypeDefinitionMetadata Plane_t808_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Plane_t808_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4248/* fieldStart */
	, 4741/* methodStart */
	, -1/* eventStart */
	, 849/* propertyStart */

};
TypeInfo Plane_t808_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Plane"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Plane_t808_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Plane_t808_0_0_0/* byval_arg */
	, &Plane_t808_1_0_0/* this_arg */
	, &Plane_t808_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Plane_t808)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Plane_t808)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Plane_t808 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngineInternal.MathfInternal
#include "UnityEngine_UnityEngineInternal_MathfInternal.h"
// Metadata Definition UnityEngineInternal.MathfInternal
extern TypeInfo MathfInternal_t896_il2cpp_TypeInfo;
// UnityEngineInternal.MathfInternal
#include "UnityEngine_UnityEngineInternal_MathfInternalMethodDeclarations.h"
static const EncodedMethodIndex MathfInternal_t896_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType MathfInternal_t896_0_0_0;
extern const Il2CppType MathfInternal_t896_1_0_0;
const Il2CppTypeDefinitionMetadata MathfInternal_t896_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, MathfInternal_t896_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4250/* fieldStart */
	, 4745/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MathfInternal_t896_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "MathfInternal"/* name */
	, "UnityEngineInternal"/* namespaze */
	, NULL/* methods */
	, &MathfInternal_t896_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MathfInternal_t896_0_0_0/* byval_arg */
	, &MathfInternal_t896_1_0_0/* this_arg */
	, &MathfInternal_t896_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MathfInternal_t896)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MathfInternal_t896)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(MathfInternal_t896 )/* native_size */
	, sizeof(MathfInternal_t896_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_Mathf.h"
// Metadata Definition UnityEngine.Mathf
extern TypeInfo Mathf_t474_il2cpp_TypeInfo;
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
static const EncodedMethodIndex Mathf_t474_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Mathf_t474_0_0_0;
extern const Il2CppType Mathf_t474_1_0_0;
const Il2CppTypeDefinitionMetadata Mathf_t474_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Mathf_t474_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4253/* fieldStart */
	, 4746/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Mathf_t474_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mathf"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Mathf_t474_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Mathf_t474_0_0_0/* byval_arg */
	, &Mathf_t474_1_0_0/* this_arg */
	, &Mathf_t474_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mathf_t474)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Mathf_t474)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Mathf_t474 )/* native_size */
	, sizeof(Mathf_t474_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 37/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.DrivenTransformProperties
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"
// Metadata Definition UnityEngine.DrivenTransformProperties
extern TypeInfo DrivenTransformProperties_t897_il2cpp_TypeInfo;
// UnityEngine.DrivenTransformProperties
#include "UnityEngine_UnityEngine_DrivenTransformPropertiesMethodDeclarations.h"
static const EncodedMethodIndex DrivenTransformProperties_t897_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair DrivenTransformProperties_t897_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DrivenTransformProperties_t897_0_0_0;
extern const Il2CppType DrivenTransformProperties_t897_1_0_0;
const Il2CppTypeDefinitionMetadata DrivenTransformProperties_t897_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DrivenTransformProperties_t897_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, DrivenTransformProperties_t897_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4254/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DrivenTransformProperties_t897_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DrivenTransformProperties"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1638/* custom_attributes_cache */
	, &DrivenTransformProperties_t897_0_0_0/* byval_arg */
	, &DrivenTransformProperties_t897_1_0_0/* this_arg */
	, &DrivenTransformProperties_t897_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DrivenTransformProperties_t897)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DrivenTransformProperties_t897)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 26/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.DrivenRectTransformTracker
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker.h"
// Metadata Definition UnityEngine.DrivenRectTransformTracker
extern TypeInfo DrivenRectTransformTracker_t698_il2cpp_TypeInfo;
// UnityEngine.DrivenRectTransformTracker
#include "UnityEngine_UnityEngine_DrivenRectTransformTrackerMethodDeclarations.h"
static const EncodedMethodIndex DrivenRectTransformTracker_t698_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DrivenRectTransformTracker_t698_0_0_0;
extern const Il2CppType DrivenRectTransformTracker_t698_1_0_0;
const Il2CppTypeDefinitionMetadata DrivenRectTransformTracker_t698_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, DrivenRectTransformTracker_t698_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4783/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DrivenRectTransformTracker_t698_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DrivenRectTransformTracker"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &DrivenRectTransformTracker_t698_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DrivenRectTransformTracker_t698_0_0_0/* byval_arg */
	, &DrivenRectTransformTracker_t698_1_0_0/* this_arg */
	, &DrivenRectTransformTracker_t698_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DrivenRectTransformTracker_t698)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DrivenRectTransformTracker_t698)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(DrivenRectTransformTracker_t698 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
// Metadata Definition UnityEngine.RectTransform
extern TypeInfo RectTransform_t648_il2cpp_TypeInfo;
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
extern const Il2CppType Edge_t898_0_0_0;
extern const Il2CppType Axis_t899_0_0_0;
extern const Il2CppType ReapplyDrivenProperties_t822_0_0_0;
static const Il2CppType* RectTransform_t648_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Edge_t898_0_0_0,
	&Axis_t899_0_0_0,
	&ReapplyDrivenProperties_t822_0_0_0,
};
static const EncodedMethodIndex RectTransform_t648_VTable[5] = 
{
	600,
	601,
	602,
	603,
	1362,
};
extern const Il2CppType IEnumerable_t1097_0_0_0;
static Il2CppInterfaceOffsetPair RectTransform_t648_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RectTransform_t648_0_0_0;
extern const Il2CppType RectTransform_t648_1_0_0;
extern const Il2CppType Transform_t243_0_0_0;
struct RectTransform_t648;
const Il2CppTypeDefinitionMetadata RectTransform_t648_DefinitionMetadata = 
{
	NULL/* declaringType */
	, RectTransform_t648_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RectTransform_t648_InterfacesOffsets/* interfaceOffsets */
	, &Transform_t243_0_0_0/* parent */
	, RectTransform_t648_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4280/* fieldStart */
	, 4785/* methodStart */
	, 13/* eventStart */
	, 851/* propertyStart */

};
TypeInfo RectTransform_t648_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RectTransform"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &RectTransform_t648_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RectTransform_t648_0_0_0/* byval_arg */
	, &RectTransform_t648_1_0_0/* this_arg */
	, &RectTransform_t648_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RectTransform_t648)/* instance_size */
	, sizeof (RectTransform_t648)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RectTransform_t648_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 6/* property_count */
	, 1/* field_count */
	, 1/* event_count */
	, 3/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.RectTransform/Edge
#include "UnityEngine_UnityEngine_RectTransform_Edge.h"
// Metadata Definition UnityEngine.RectTransform/Edge
extern TypeInfo Edge_t898_il2cpp_TypeInfo;
// UnityEngine.RectTransform/Edge
#include "UnityEngine_UnityEngine_RectTransform_EdgeMethodDeclarations.h"
static const EncodedMethodIndex Edge_t898_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Edge_t898_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Edge_t898_1_0_0;
const Il2CppTypeDefinitionMetadata Edge_t898_DefinitionMetadata = 
{
	&RectTransform_t648_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Edge_t898_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Edge_t898_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4281/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Edge_t898_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Edge"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Edge_t898_0_0_0/* byval_arg */
	, &Edge_t898_1_0_0/* this_arg */
	, &Edge_t898_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Edge_t898)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Edge_t898)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.RectTransform/Axis
#include "UnityEngine_UnityEngine_RectTransform_Axis.h"
// Metadata Definition UnityEngine.RectTransform/Axis
extern TypeInfo Axis_t899_il2cpp_TypeInfo;
// UnityEngine.RectTransform/Axis
#include "UnityEngine_UnityEngine_RectTransform_AxisMethodDeclarations.h"
static const EncodedMethodIndex Axis_t899_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Axis_t899_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Axis_t899_1_0_0;
const Il2CppTypeDefinitionMetadata Axis_t899_DefinitionMetadata = 
{
	&RectTransform_t648_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Axis_t899_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Axis_t899_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4286/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Axis_t899_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Axis"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Axis_t899_0_0_0/* byval_arg */
	, &Axis_t899_1_0_0/* this_arg */
	, &Axis_t899_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Axis_t899)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Axis_t899)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.RectTransform/ReapplyDrivenProperties
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertie.h"
// Metadata Definition UnityEngine.RectTransform/ReapplyDrivenProperties
extern TypeInfo ReapplyDrivenProperties_t822_il2cpp_TypeInfo;
// UnityEngine.RectTransform/ReapplyDrivenProperties
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertieMethodDeclarations.h"
static const EncodedMethodIndex ReapplyDrivenProperties_t822_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1363,
	1364,
	1365,
};
static Il2CppInterfaceOffsetPair ReapplyDrivenProperties_t822_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ReapplyDrivenProperties_t822_1_0_0;
struct ReapplyDrivenProperties_t822;
const Il2CppTypeDefinitionMetadata ReapplyDrivenProperties_t822_DefinitionMetadata = 
{
	&RectTransform_t648_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReapplyDrivenProperties_t822_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, ReapplyDrivenProperties_t822_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4815/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ReapplyDrivenProperties_t822_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReapplyDrivenProperties"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ReapplyDrivenProperties_t822_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReapplyDrivenProperties_t822_0_0_0/* byval_arg */
	, &ReapplyDrivenProperties_t822_1_0_0/* this_arg */
	, &ReapplyDrivenProperties_t822_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ReapplyDrivenProperties_t822/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReapplyDrivenProperties_t822)/* instance_size */
	, sizeof (ReapplyDrivenProperties_t822)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.ResourceRequest
#include "UnityEngine_UnityEngine_ResourceRequest.h"
// Metadata Definition UnityEngine.ResourceRequest
extern TypeInfo ResourceRequest_t900_il2cpp_TypeInfo;
// UnityEngine.ResourceRequest
#include "UnityEngine_UnityEngine_ResourceRequestMethodDeclarations.h"
static const EncodedMethodIndex ResourceRequest_t900_VTable[4] = 
{
	626,
	1262,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ResourceRequest_t900_0_0_0;
extern const Il2CppType ResourceRequest_t900_1_0_0;
struct ResourceRequest_t900;
const Il2CppTypeDefinitionMetadata ResourceRequest_t900_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsyncOperation_t829_0_0_0/* parent */
	, ResourceRequest_t900_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4289/* fieldStart */
	, 4819/* methodStart */
	, -1/* eventStart */
	, 857/* propertyStart */

};
TypeInfo ResourceRequest_t900_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResourceRequest"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &ResourceRequest_t900_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ResourceRequest_t900_0_0_0/* byval_arg */
	, &ResourceRequest_t900_1_0_0/* this_arg */
	, &ResourceRequest_t900_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResourceRequest_t900)/* instance_size */
	, sizeof (ResourceRequest_t900)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Resources
#include "UnityEngine_UnityEngine_Resources.h"
// Metadata Definition UnityEngine.Resources
extern TypeInfo Resources_t901_il2cpp_TypeInfo;
// UnityEngine.Resources
#include "UnityEngine_UnityEngine_ResourcesMethodDeclarations.h"
static const EncodedMethodIndex Resources_t901_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Resources_t901_0_0_0;
extern const Il2CppType Resources_t901_1_0_0;
struct Resources_t901;
const Il2CppTypeDefinitionMetadata Resources_t901_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Resources_t901_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4821/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Resources_t901_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Resources"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Resources_t901_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Resources_t901_0_0_0/* byval_arg */
	, &Resources_t901_1_0_0/* this_arg */
	, &Resources_t901_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Resources_t901)/* instance_size */
	, sizeof (Resources_t901)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SerializePrivateVariables
#include "UnityEngine_UnityEngine_SerializePrivateVariables.h"
// Metadata Definition UnityEngine.SerializePrivateVariables
extern TypeInfo SerializePrivateVariables_t902_il2cpp_TypeInfo;
// UnityEngine.SerializePrivateVariables
#include "UnityEngine_UnityEngine_SerializePrivateVariablesMethodDeclarations.h"
static const EncodedMethodIndex SerializePrivateVariables_t902_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
extern const Il2CppType _Attribute_t3429_0_0_0;
static Il2CppInterfaceOffsetPair SerializePrivateVariables_t902_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SerializePrivateVariables_t902_0_0_0;
extern const Il2CppType SerializePrivateVariables_t902_1_0_0;
extern const Il2CppType Attribute_t903_0_0_0;
struct SerializePrivateVariables_t902;
const Il2CppTypeDefinitionMetadata SerializePrivateVariables_t902_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SerializePrivateVariables_t902_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, SerializePrivateVariables_t902_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4823/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SerializePrivateVariables_t902_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializePrivateVariables"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SerializePrivateVariables_t902_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1651/* custom_attributes_cache */
	, &SerializePrivateVariables_t902_0_0_0/* byval_arg */
	, &SerializePrivateVariables_t902_1_0_0/* this_arg */
	, &SerializePrivateVariables_t902_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializePrivateVariables_t902)/* instance_size */
	, sizeof (SerializePrivateVariables_t902)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// Metadata Definition UnityEngine.SerializeField
extern TypeInfo SerializeField_t904_il2cpp_TypeInfo;
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
static const EncodedMethodIndex SerializeField_t904_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair SerializeField_t904_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SerializeField_t904_0_0_0;
extern const Il2CppType SerializeField_t904_1_0_0;
struct SerializeField_t904;
const Il2CppTypeDefinitionMetadata SerializeField_t904_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SerializeField_t904_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, SerializeField_t904_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4824/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SerializeField_t904_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializeField"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SerializeField_t904_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SerializeField_t904_0_0_0/* byval_arg */
	, &SerializeField_t904_1_0_0/* this_arg */
	, &SerializeField_t904_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializeField_t904)/* instance_size */
	, sizeof (SerializeField_t904)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.ISerializationCallbackReceiver
extern TypeInfo ISerializationCallbackReceiver_t3399_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ISerializationCallbackReceiver_t3399_0_0_0;
extern const Il2CppType ISerializationCallbackReceiver_t3399_1_0_0;
struct ISerializationCallbackReceiver_t3399;
const Il2CppTypeDefinitionMetadata ISerializationCallbackReceiver_t3399_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4825/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ISerializationCallbackReceiver_t3399_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISerializationCallbackReceiver"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &ISerializationCallbackReceiver_t3399_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISerializationCallbackReceiver_t3399_0_0_0/* byval_arg */
	, &ISerializationCallbackReceiver_t3399_1_0_0/* this_arg */
	, &ISerializationCallbackReceiver_t3399_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_Shader.h"
// Metadata Definition UnityEngine.Shader
extern TypeInfo Shader_t1_il2cpp_TypeInfo;
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_ShaderMethodDeclarations.h"
static const EncodedMethodIndex Shader_t1_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Shader_t1_0_0_0;
extern const Il2CppType Shader_t1_1_0_0;
struct Shader_t1;
const Il2CppTypeDefinitionMetadata Shader_t1_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t473_0_0_0/* parent */
	, Shader_t1_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4827/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Shader_t1_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Shader"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Shader_t1_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Shader_t1_0_0_0/* byval_arg */
	, &Shader_t1_1_0_0/* this_arg */
	, &Shader_t1_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Shader_t1)/* instance_size */
	, sizeof (Shader_t1)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// Metadata Definition UnityEngine.Material
extern TypeInfo Material_t2_il2cpp_TypeInfo;
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
static const EncodedMethodIndex Material_t2_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Material_t2_0_0_0;
extern const Il2CppType Material_t2_1_0_0;
struct Material_t2;
const Il2CppTypeDefinitionMetadata Material_t2_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t473_0_0_0/* parent */
	, Material_t2_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4838/* methodStart */
	, -1/* eventStart */
	, 858/* propertyStart */

};
TypeInfo Material_t2_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Material"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Material_t2_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Material_t2_0_0_0/* byval_arg */
	, &Material_t2_1_0_0/* this_arg */
	, &Material_t2_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Material_t2)/* instance_size */
	, sizeof (Material_t2)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Rendering.SphericalHarmonicsL2
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
// Metadata Definition UnityEngine.Rendering.SphericalHarmonicsL2
extern TypeInfo SphericalHarmonicsL2_t905_il2cpp_TypeInfo;
// UnityEngine.Rendering.SphericalHarmonicsL2
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2MethodDeclarations.h"
static const EncodedMethodIndex SphericalHarmonicsL2_t905_VTable[4] = 
{
	1368,
	601,
	1369,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SphericalHarmonicsL2_t905_0_0_0;
extern const Il2CppType SphericalHarmonicsL2_t905_1_0_0;
const Il2CppTypeDefinitionMetadata SphericalHarmonicsL2_t905_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, SphericalHarmonicsL2_t905_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4291/* fieldStart */
	, 4868/* methodStart */
	, -1/* eventStart */
	, 862/* propertyStart */

};
TypeInfo SphericalHarmonicsL2_t905_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SphericalHarmonicsL2"/* name */
	, "UnityEngine.Rendering"/* namespaze */
	, NULL/* methods */
	, &SphericalHarmonicsL2_t905_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1670/* custom_attributes_cache */
	, &SphericalHarmonicsL2_t905_0_0_0/* byval_arg */
	, &SphericalHarmonicsL2_t905_1_0_0/* this_arg */
	, &SphericalHarmonicsL2_t905_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SphericalHarmonicsL2_t905)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SphericalHarmonicsL2_t905)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(SphericalHarmonicsL2_t905 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 18/* method_count */
	, 1/* property_count */
	, 27/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Sprite
#include "UnityEngine_UnityEngine_Sprite.h"
// Metadata Definition UnityEngine.Sprite
extern TypeInfo Sprite_t668_il2cpp_TypeInfo;
// UnityEngine.Sprite
#include "UnityEngine_UnityEngine_SpriteMethodDeclarations.h"
static const EncodedMethodIndex Sprite_t668_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Sprite_t668_0_0_0;
extern const Il2CppType Sprite_t668_1_0_0;
struct Sprite_t668;
const Il2CppTypeDefinitionMetadata Sprite_t668_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t473_0_0_0/* parent */
	, Sprite_t668_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4886/* methodStart */
	, -1/* eventStart */
	, 863/* propertyStart */

};
TypeInfo Sprite_t668_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Sprite"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Sprite_t668_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Sprite_t668_0_0_0/* byval_arg */
	, &Sprite_t668_1_0_0/* this_arg */
	, &Sprite_t668_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Sprite_t668)/* instance_size */
	, sizeof (Sprite_t668)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
