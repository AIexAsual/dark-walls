﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// StartAndStopTween
struct StartAndStopTween_t419;

// System.Void StartAndStopTween::.ctor()
extern "C" void StartAndStopTween__ctor_m2396 (StartAndStopTween_t419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StartAndStopTween::OnGUI()
extern "C" void StartAndStopTween_OnGUI_m2397 (StartAndStopTween_t419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
