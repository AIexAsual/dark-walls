﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$52
struct U24ArrayTypeU2452_t2171;
struct U24ArrayTypeU2452_t2171_marshaled;

void U24ArrayTypeU2452_t2171_marshal(const U24ArrayTypeU2452_t2171& unmarshaled, U24ArrayTypeU2452_t2171_marshaled& marshaled);
void U24ArrayTypeU2452_t2171_marshal_back(const U24ArrayTypeU2452_t2171_marshaled& marshaled, U24ArrayTypeU2452_t2171& unmarshaled);
void U24ArrayTypeU2452_t2171_marshal_cleanup(U24ArrayTypeU2452_t2171_marshaled& marshaled);
