﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_OldFilm_Cutting2
struct CameraFilterPack_OldFilm_Cutting2_t168;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_OldFilm_Cutting2::.ctor()
extern "C" void CameraFilterPack_OldFilm_Cutting2__ctor_m1085 (CameraFilterPack_OldFilm_Cutting2_t168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_OldFilm_Cutting2::get_material()
extern "C" Material_t2 * CameraFilterPack_OldFilm_Cutting2_get_material_m1086 (CameraFilterPack_OldFilm_Cutting2_t168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_OldFilm_Cutting2::Start()
extern "C" void CameraFilterPack_OldFilm_Cutting2_Start_m1087 (CameraFilterPack_OldFilm_Cutting2_t168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_OldFilm_Cutting2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_OldFilm_Cutting2_OnRenderImage_m1088 (CameraFilterPack_OldFilm_Cutting2_t168 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_OldFilm_Cutting2::Update()
extern "C" void CameraFilterPack_OldFilm_Cutting2_Update_m1089 (CameraFilterPack_OldFilm_Cutting2_t168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_OldFilm_Cutting2::OnDisable()
extern "C" void CameraFilterPack_OldFilm_Cutting2_OnDisable_m1090 (CameraFilterPack_OldFilm_Cutting2_t168 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
