﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Object>
struct InternalEnumerator_1_t2224;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m13479_gshared (InternalEnumerator_1_t2224 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m13479(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2224 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13479_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13480_gshared (InternalEnumerator_1_t2224 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13480(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2224 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13480_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m13481_gshared (InternalEnumerator_1_t2224 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m13481(__this, method) (( void (*) (InternalEnumerator_1_t2224 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13481_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m13482_gshared (InternalEnumerator_1_t2224 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m13482(__this, method) (( bool (*) (InternalEnumerator_1_t2224 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13482_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern "C" Object_t * InternalEnumerator_1_get_Current_m13483_gshared (InternalEnumerator_1_t2224 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m13483(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2224 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13483_gshared)(__this, method)
