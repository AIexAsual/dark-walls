﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DroppingObjects
struct DroppingObjects_t363;

// System.Void DroppingObjects::.ctor()
extern "C" void DroppingObjects__ctor_m2106 (DroppingObjects_t363 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DroppingObjects::.cctor()
extern "C" void DroppingObjects__cctor_m2107 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
