﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Cardboard/StereoScreenChangeDelegate
struct StereoScreenChangeDelegate_t232;
// System.Object
struct Object_t;
// UnityEngine.RenderTexture
struct RenderTexture_t15;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void Cardboard/StereoScreenChangeDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void StereoScreenChangeDelegate__ctor_m1418 (StereoScreenChangeDelegate_t232 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cardboard/StereoScreenChangeDelegate::Invoke(UnityEngine.RenderTexture)
extern "C" void StereoScreenChangeDelegate_Invoke_m1419 (StereoScreenChangeDelegate_t232 * __this, RenderTexture_t15 * ___newStereoScreen, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_StereoScreenChangeDelegate_t232(Il2CppObject* delegate, RenderTexture_t15 * ___newStereoScreen);
// System.IAsyncResult Cardboard/StereoScreenChangeDelegate::BeginInvoke(UnityEngine.RenderTexture,System.AsyncCallback,System.Object)
extern "C" Object_t * StereoScreenChangeDelegate_BeginInvoke_m1420 (StereoScreenChangeDelegate_t232 * __this, RenderTexture_t15 * ___newStereoScreen, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Cardboard/StereoScreenChangeDelegate::EndInvoke(System.IAsyncResult)
extern "C" void StereoScreenChangeDelegate_EndInvoke_m1421 (StereoScreenChangeDelegate_t232 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
