﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForSeconds
struct WaitForSeconds_t475;
struct WaitForSeconds_t475_marshaled;

// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" void WaitForSeconds__ctor_m2750 (WaitForSeconds_t475 * __this, float ___seconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void WaitForSeconds_t475_marshal(const WaitForSeconds_t475& unmarshaled, WaitForSeconds_t475_marshaled& marshaled);
void WaitForSeconds_t475_marshal_back(const WaitForSeconds_t475_marshaled& marshaled, WaitForSeconds_t475& unmarshaled);
void WaitForSeconds_t475_marshal_cleanup(WaitForSeconds_t475_marshaled& marshaled);
