﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t630;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t769;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"

// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::get_startColor()
extern "C" Color_t6  ColorTween_get_startColor_m3646 (ColorTween_t630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_startColor(UnityEngine.Color)
extern "C" void ColorTween_set_startColor_m3647 (ColorTween_t630 * __this, Color_t6  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::get_targetColor()
extern "C" Color_t6  ColorTween_get_targetColor_m3648 (ColorTween_t630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_targetColor(UnityEngine.Color)
extern "C" void ColorTween_set_targetColor_m3649 (ColorTween_t630 * __this, Color_t6  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode UnityEngine.UI.CoroutineTween.ColorTween::get_tweenMode()
extern "C" int32_t ColorTween_get_tweenMode_m3650 (ColorTween_t630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_tweenMode(UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode)
extern "C" void ColorTween_set_tweenMode_m3651 (ColorTween_t630 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.ColorTween::get_duration()
extern "C" float ColorTween_get_duration_m3652 (ColorTween_t630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_duration(System.Single)
extern "C" void ColorTween_set_duration_m3653 (ColorTween_t630 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::get_ignoreTimeScale()
extern "C" bool ColorTween_get_ignoreTimeScale_m3654 (ColorTween_t630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_ignoreTimeScale(System.Boolean)
extern "C" void ColorTween_set_ignoreTimeScale_m3655 (ColorTween_t630 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::TweenValue(System.Single)
extern "C" void ColorTween_TweenValue_m3656 (ColorTween_t630 * __this, float ___floatPercentage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::AddOnChangedCallback(UnityEngine.Events.UnityAction`1<UnityEngine.Color>)
extern "C" void ColorTween_AddOnChangedCallback_m3657 (ColorTween_t630 * __this, UnityAction_1_t769 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::GetIgnoreTimescale()
extern "C" bool ColorTween_GetIgnoreTimescale_m3658 (ColorTween_t630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.ColorTween::GetDuration()
extern "C" float ColorTween_GetDuration_m3659 (ColorTween_t630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::ValidTarget()
extern "C" bool ColorTween_ValidTarget_m3660 (ColorTween_t630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
