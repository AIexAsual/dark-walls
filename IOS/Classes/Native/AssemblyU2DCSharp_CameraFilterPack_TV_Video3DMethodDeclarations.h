﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_Video3D
struct CameraFilterPack_TV_Video3D_t197;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_Video3D::.ctor()
extern "C" void CameraFilterPack_TV_Video3D__ctor_m1277 (CameraFilterPack_TV_Video3D_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Video3D::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_Video3D_get_material_m1278 (CameraFilterPack_TV_Video3D_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Video3D::Start()
extern "C" void CameraFilterPack_TV_Video3D_Start_m1279 (CameraFilterPack_TV_Video3D_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Video3D::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_Video3D_OnRenderImage_m1280 (CameraFilterPack_TV_Video3D_t197 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Video3D::Update()
extern "C" void CameraFilterPack_TV_Video3D_Update_m1281 (CameraFilterPack_TV_Video3D_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Video3D::OnDisable()
extern "C" void CameraFilterPack_TV_Video3D_OnDisable_m1282 (CameraFilterPack_TV_Video3D_t197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
