﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Color>
struct InternalEnumerator_1_t2246;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m13754_gshared (InternalEnumerator_1_t2246 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m13754(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2246 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13754_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13755_gshared (InternalEnumerator_1_t2246 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13755(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2246 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13755_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m13756_gshared (InternalEnumerator_1_t2246 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m13756(__this, method) (( void (*) (InternalEnumerator_1_t2246 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13756_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m13757_gshared (InternalEnumerator_1_t2246 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m13757(__this, method) (( bool (*) (InternalEnumerator_1_t2246 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13757_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Color>::get_Current()
extern "C" Color_t6  InternalEnumerator_1_get_Current_m13758_gshared (InternalEnumerator_1_t2246 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m13758(__this, method) (( Color_t6  (*) (InternalEnumerator_1_t2246 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13758_gshared)(__this, method)
