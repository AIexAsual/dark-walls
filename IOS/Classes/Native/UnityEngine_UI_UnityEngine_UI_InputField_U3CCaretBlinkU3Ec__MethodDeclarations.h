﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.InputField/<CaretBlink>c__Iterator2
struct U3CCaretBlinkU3Ec__Iterator2_t681;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.InputField/<CaretBlink>c__Iterator2::.ctor()
extern "C" void U3CCaretBlinkU3Ec__Iterator2__ctor_m3868 (U3CCaretBlinkU3Ec__Iterator2_t681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.InputField/<CaretBlink>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CCaretBlinkU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3869 (U3CCaretBlinkU3Ec__Iterator2_t681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.InputField/<CaretBlink>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCaretBlinkU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m3870 (U3CCaretBlinkU3Ec__Iterator2_t681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.InputField/<CaretBlink>c__Iterator2::MoveNext()
extern "C" bool U3CCaretBlinkU3Ec__Iterator2_MoveNext_m3871 (U3CCaretBlinkU3Ec__Iterator2_t681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.InputField/<CaretBlink>c__Iterator2::Dispose()
extern "C" void U3CCaretBlinkU3Ec__Iterator2_Dispose_m3872 (U3CCaretBlinkU3Ec__Iterator2_t681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.InputField/<CaretBlink>c__Iterator2::Reset()
extern "C" void U3CCaretBlinkU3Ec__Iterator2_Reset_m3873 (U3CCaretBlinkU3Ec__Iterator2_t681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
