﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<iTween/EaseType>
struct InternalEnumerator_1_t2386;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"

// System.Void System.Array/InternalEnumerator`1<iTween/EaseType>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15469_gshared (InternalEnumerator_1_t2386 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15469(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2386 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15469_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<iTween/EaseType>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15470_gshared (InternalEnumerator_1_t2386 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15470(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2386 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15470_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<iTween/EaseType>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15471_gshared (InternalEnumerator_1_t2386 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15471(__this, method) (( void (*) (InternalEnumerator_1_t2386 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15471_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<iTween/EaseType>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15472_gshared (InternalEnumerator_1_t2386 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15472(__this, method) (( bool (*) (InternalEnumerator_1_t2386 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15472_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<iTween/EaseType>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m15473_gshared (InternalEnumerator_1_t2386 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15473(__this, method) (( int32_t (*) (InternalEnumerator_1_t2386 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15473_gshared)(__this, method)
