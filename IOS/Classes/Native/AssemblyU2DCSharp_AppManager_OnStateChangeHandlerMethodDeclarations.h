﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AppManager/OnStateChangeHandler
struct OnStateChangeHandler_t349;
// System.Object
struct Object_t;
// System.Collections.Hashtable
struct Hashtable_t348;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// AppEvent
#include "AssemblyU2DCSharp_AppEvent.h"

// System.Void AppManager/OnStateChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C" void OnStateChangeHandler__ctor_m2033 (OnStateChangeHandler_t349 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager/OnStateChangeHandler::Invoke(AppEvent,System.Collections.Hashtable)
extern "C" void OnStateChangeHandler_Invoke_m2034 (OnStateChangeHandler_t349 * __this, int32_t ___ev, Hashtable_t348 * ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OnStateChangeHandler_t349(Il2CppObject* delegate, int32_t ___ev, Hashtable_t348 * ___i);
// System.IAsyncResult AppManager/OnStateChangeHandler::BeginInvoke(AppEvent,System.Collections.Hashtable,System.AsyncCallback,System.Object)
extern "C" Object_t * OnStateChangeHandler_BeginInvoke_m2035 (OnStateChangeHandler_t349 * __this, int32_t ___ev, Hashtable_t348 * ___i, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager/OnStateChangeHandler::EndInvoke(System.IAsyncResult)
extern "C" void OnStateChangeHandler_EndInvoke_m2036 (OnStateChangeHandler_t349 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
