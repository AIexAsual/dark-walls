﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.GuidAttribute
struct GuidAttribute_t1559;
// System.String
struct String_t;

// System.Void System.Runtime.InteropServices.GuidAttribute::.ctor(System.String)
extern "C" void GuidAttribute__ctor_m9352 (GuidAttribute_t1559 * __this, String_t* ___guid, const MethodInfo* method) IL2CPP_METHOD_ATTR;
