﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_8bits_gb
struct CameraFilterPack_FX_8bits_gb_t121;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_8bits_gb::.ctor()
extern "C" void CameraFilterPack_FX_8bits_gb__ctor_m777 (CameraFilterPack_FX_8bits_gb_t121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_8bits_gb::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_8bits_gb_get_material_m778 (CameraFilterPack_FX_8bits_gb_t121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits_gb::Start()
extern "C" void CameraFilterPack_FX_8bits_gb_Start_m779 (CameraFilterPack_FX_8bits_gb_t121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits_gb::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_8bits_gb_OnRenderImage_m780 (CameraFilterPack_FX_8bits_gb_t121 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits_gb::OnValidate()
extern "C" void CameraFilterPack_FX_8bits_gb_OnValidate_m781 (CameraFilterPack_FX_8bits_gb_t121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits_gb::Update()
extern "C" void CameraFilterPack_FX_8bits_gb_Update_m782 (CameraFilterPack_FX_8bits_gb_t121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_8bits_gb::OnDisable()
extern "C" void CameraFilterPack_FX_8bits_gb_OnDisable_m783 (CameraFilterPack_FX_8bits_gb_t121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
