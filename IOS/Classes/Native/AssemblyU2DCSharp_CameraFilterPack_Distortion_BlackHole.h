﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Distortion_BlackHole
struct  CameraFilterPack_Distortion_BlackHole_t81  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Distortion_BlackHole::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Distortion_BlackHole::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Distortion_BlackHole::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Distortion_BlackHole::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Distortion_BlackHole::PositionX
	float ___PositionX_6;
	// System.Single CameraFilterPack_Distortion_BlackHole::PositionY
	float ___PositionY_7;
	// System.Single CameraFilterPack_Distortion_BlackHole::Size
	float ___Size_8;
	// System.Single CameraFilterPack_Distortion_BlackHole::Distortion
	float ___Distortion_9;
};
struct CameraFilterPack_Distortion_BlackHole_t81_StaticFields{
	// System.Single CameraFilterPack_Distortion_BlackHole::ChangePositionX
	float ___ChangePositionX_10;
	// System.Single CameraFilterPack_Distortion_BlackHole::ChangePositionY
	float ___ChangePositionY_11;
	// System.Single CameraFilterPack_Distortion_BlackHole::ChangeSize
	float ___ChangeSize_12;
	// System.Single CameraFilterPack_Distortion_BlackHole::ChangeDistortion
	float ___ChangeDistortion_13;
};
