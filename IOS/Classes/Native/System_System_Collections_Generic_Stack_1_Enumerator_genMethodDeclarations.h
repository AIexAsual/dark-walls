﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1/Enumerator<System.Object>
struct Enumerator_t2269;
// System.Object
struct Object_t;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t2266;

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Stack`1<T>)
extern "C" void Enumerator__ctor_m14023_gshared (Enumerator_t2269 * __this, Stack_1_t2266 * ___t, const MethodInfo* method);
#define Enumerator__ctor_m14023(__this, ___t, method) (( void (*) (Enumerator_t2269 *, Stack_1_t2266 *, const MethodInfo*))Enumerator__ctor_m14023_gshared)(__this, ___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m14024_gshared (Enumerator_t2269 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m14024(__this, method) (( Object_t * (*) (Enumerator_t2269 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14024_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m14025_gshared (Enumerator_t2269 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m14025(__this, method) (( void (*) (Enumerator_t2269 *, const MethodInfo*))Enumerator_Dispose_m14025_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m14026_gshared (Enumerator_t2269 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m14026(__this, method) (( bool (*) (Enumerator_t2269 *, const MethodInfo*))Enumerator_MoveNext_m14026_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m14027_gshared (Enumerator_t2269 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m14027(__this, method) (( Object_t * (*) (Enumerator_t2269 *, const MethodInfo*))Enumerator_get_Current_m14027_gshared)(__this, method)
