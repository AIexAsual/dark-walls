﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct KeyValuePair_2_t2549;
// UnityEngine.Font
struct Font_t643;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t792;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m18054(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2549 *, Font_t643 *, List_1_t792 *, const MethodInfo*))KeyValuePair_2__ctor_m14739_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Key()
#define KeyValuePair_2_get_Key_m18055(__this, method) (( Font_t643 * (*) (KeyValuePair_2_t2549 *, const MethodInfo*))KeyValuePair_2_get_Key_m14741_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m18056(__this, ___value, method) (( void (*) (KeyValuePair_2_t2549 *, Font_t643 *, const MethodInfo*))KeyValuePair_2_set_Key_m14743_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Value()
#define KeyValuePair_2_get_Value_m18057(__this, method) (( List_1_t792 * (*) (KeyValuePair_2_t2549 *, const MethodInfo*))KeyValuePair_2_get_Value_m14745_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m18058(__this, ___value, method) (( void (*) (KeyValuePair_2_t2549 *, List_1_t792 *, const MethodInfo*))KeyValuePair_2_set_Value_m14747_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::ToString()
#define KeyValuePair_2_ToString_m18059(__this, method) (( String_t* (*) (KeyValuePair_2_t2549 *, const MethodInfo*))KeyValuePair_2_ToString_m14749_gshared)(__this, method)
