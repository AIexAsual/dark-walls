﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Alien_Vision
struct CameraFilterPack_Alien_Vision_t11;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Alien_Vision::.ctor()
extern "C" void CameraFilterPack_Alien_Vision__ctor_m23 (CameraFilterPack_Alien_Vision_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Alien_Vision::get_material()
extern "C" Material_t2 * CameraFilterPack_Alien_Vision_get_material_m24 (CameraFilterPack_Alien_Vision_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Alien_Vision::Start()
extern "C" void CameraFilterPack_Alien_Vision_Start_m25 (CameraFilterPack_Alien_Vision_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Alien_Vision::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Alien_Vision_OnRenderImage_m26 (CameraFilterPack_Alien_Vision_t11 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Alien_Vision::OnValidate()
extern "C" void CameraFilterPack_Alien_Vision_OnValidate_m27 (CameraFilterPack_Alien_Vision_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Alien_Vision::Update()
extern "C" void CameraFilterPack_Alien_Vision_Update_m28 (CameraFilterPack_Alien_Vision_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Alien_Vision::OnDisable()
extern "C" void CameraFilterPack_Alien_Vision_OnDisable_m29 (CameraFilterPack_Alien_Vision_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
