﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Type>
struct KeyValuePair_2_t2322;
// System.String
struct String_t;
// System.Type
struct Type_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Type>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m14738(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2322 *, String_t*, Type_t *, const MethodInfo*))KeyValuePair_2__ctor_m14739_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Type>::get_Key()
#define KeyValuePair_2_get_Key_m14740(__this, method) (( String_t* (*) (KeyValuePair_2_t2322 *, const MethodInfo*))KeyValuePair_2_get_Key_m14741_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Type>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m14742(__this, ___value, method) (( void (*) (KeyValuePair_2_t2322 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m14743_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Type>::get_Value()
#define KeyValuePair_2_get_Value_m14744(__this, method) (( Type_t * (*) (KeyValuePair_2_t2322 *, const MethodInfo*))KeyValuePair_2_get_Value_m14745_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Type>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m14746(__this, ___value, method) (( void (*) (KeyValuePair_2_t2322 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Value_m14747_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Type>::ToString()
#define KeyValuePair_2_ToString_m14748(__this, method) (( String_t* (*) (KeyValuePair_2_t2322 *, const MethodInfo*))KeyValuePair_2_ToString_m14749_gshared)(__this, method)
