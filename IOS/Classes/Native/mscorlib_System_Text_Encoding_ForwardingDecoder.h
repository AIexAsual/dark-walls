﻿#pragma once
#include <stdint.h>
// System.Text.Encoding
struct Encoding_t519;
// System.Text.Decoder
#include "mscorlib_System_Text_Decoder.h"
// System.Text.Encoding/ForwardingDecoder
struct  ForwardingDecoder_t2043  : public Decoder_t1695
{
	// System.Text.Encoding System.Text.Encoding/ForwardingDecoder::encoding
	Encoding_t519 * ___encoding_2;
};
