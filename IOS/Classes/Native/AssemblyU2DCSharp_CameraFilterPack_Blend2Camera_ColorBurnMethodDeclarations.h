﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_ColorBurn
struct CameraFilterPack_Blend2Camera_ColorBurn_t19;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_ColorBurn::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_ColorBurn__ctor_m66 (CameraFilterPack_Blend2Camera_ColorBurn_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_ColorBurn::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_ColorBurn_get_material_m67 (CameraFilterPack_Blend2Camera_ColorBurn_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorBurn::Start()
extern "C" void CameraFilterPack_Blend2Camera_ColorBurn_Start_m68 (CameraFilterPack_Blend2Camera_ColorBurn_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorBurn::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_ColorBurn_OnRenderImage_m69 (CameraFilterPack_Blend2Camera_ColorBurn_t19 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorBurn::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_ColorBurn_OnValidate_m70 (CameraFilterPack_Blend2Camera_ColorBurn_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorBurn::Update()
extern "C" void CameraFilterPack_Blend2Camera_ColorBurn_Update_m71 (CameraFilterPack_Blend2Camera_ColorBurn_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorBurn::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_ColorBurn_OnEnable_m72 (CameraFilterPack_Blend2Camera_ColorBurn_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorBurn::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_ColorBurn_OnDisable_m73 (CameraFilterPack_Blend2Camera_ColorBurn_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
