﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.DecoderExceptionFallback
struct DecoderExceptionFallback_t2031;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2030;
// System.Object
struct Object_t;

// System.Void System.Text.DecoderExceptionFallback::.ctor()
extern "C" void DecoderExceptionFallback__ctor_m12275 (DecoderExceptionFallback_t2031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallbackBuffer System.Text.DecoderExceptionFallback::CreateFallbackBuffer()
extern "C" DecoderFallbackBuffer_t2030 * DecoderExceptionFallback_CreateFallbackBuffer_m12276 (DecoderExceptionFallback_t2031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.DecoderExceptionFallback::Equals(System.Object)
extern "C" bool DecoderExceptionFallback_Equals_m12277 (DecoderExceptionFallback_t2031 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.DecoderExceptionFallback::GetHashCode()
extern "C" int32_t DecoderExceptionFallback_GetHashCode_m12278 (DecoderExceptionFallback_t2031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
