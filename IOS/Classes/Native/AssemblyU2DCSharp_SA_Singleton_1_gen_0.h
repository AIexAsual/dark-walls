﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SA_Singleton`1<System.Object>
struct  SA_Singleton_1_t2301  : public MonoBehaviour_t4
{
};
struct SA_Singleton_1_t2301_StaticFields{
	// T SA_Singleton`1<System.Object>::_instance
	Object_t * ____instance_2;
	// System.Boolean SA_Singleton`1<System.Object>::applicationIsQuitting
	bool ___applicationIsQuitting_3;
};
