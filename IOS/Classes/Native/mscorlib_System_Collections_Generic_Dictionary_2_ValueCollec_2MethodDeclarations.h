﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>
struct Enumerator_t1115;
// System.Object
struct Object_t;
// UnityEngine.GUIStyle
struct GUIStyle_t273;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t888;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_9MethodDeclarations.h"
#define Enumerator__ctor_m19732(__this, ___host, method) (( void (*) (Enumerator_t1115 *, Dictionary_2_t888 *, const MethodInfo*))Enumerator__ctor_m15094_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19733(__this, method) (( Object_t * (*) (Enumerator_t1115 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15095_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m19734(__this, method) (( void (*) (Enumerator_t1115 *, const MethodInfo*))Enumerator_Dispose_m15096_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m19735(__this, method) (( bool (*) (Enumerator_t1115 *, const MethodInfo*))Enumerator_MoveNext_m15097_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m19736(__this, method) (( GUIStyle_t273 * (*) (Enumerator_t1115 *, const MethodInfo*))Enumerator_get_Current_m15098_gshared)(__this, method)
