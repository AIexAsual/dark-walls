﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>
struct ShimEnumerator_t2374;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2363;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m15290_gshared (ShimEnumerator_t2374 * __this, Dictionary_2_t2363 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m15290(__this, ___host, method) (( void (*) (ShimEnumerator_t2374 *, Dictionary_2_t2363 *, const MethodInfo*))ShimEnumerator__ctor_m15290_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m15291_gshared (ShimEnumerator_t2374 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m15291(__this, method) (( bool (*) (ShimEnumerator_t2374 *, const MethodInfo*))ShimEnumerator_MoveNext_m15291_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Entry()
extern "C" DictionaryEntry_t552  ShimEnumerator_get_Entry_m15292_gshared (ShimEnumerator_t2374 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m15292(__this, method) (( DictionaryEntry_t552  (*) (ShimEnumerator_t2374 *, const MethodInfo*))ShimEnumerator_get_Entry_m15292_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m15293_gshared (ShimEnumerator_t2374 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m15293(__this, method) (( Object_t * (*) (ShimEnumerator_t2374 *, const MethodInfo*))ShimEnumerator_get_Key_m15293_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m15294_gshared (ShimEnumerator_t2374 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m15294(__this, method) (( Object_t * (*) (ShimEnumerator_t2374 *, const MethodInfo*))ShimEnumerator_get_Value_m15294_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m15295_gshared (ShimEnumerator_t2374 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m15295(__this, method) (( Object_t * (*) (ShimEnumerator_t2374 *, const MethodInfo*))ShimEnumerator_get_Current_m15295_gshared)(__this, method)
