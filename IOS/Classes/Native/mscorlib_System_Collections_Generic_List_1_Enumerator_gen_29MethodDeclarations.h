﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>
struct Enumerator_t2574;
// System.Object
struct Object_t;
// UnityEngine.UI.Graphic
struct Graphic_t654;
// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct List_1_t656;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m18442(__this, ___l, method) (( void (*) (Enumerator_t2574 *, List_1_t656 *, const MethodInfo*))Enumerator__ctor_m13586_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18443(__this, method) (( Object_t * (*) (Enumerator_t2574 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13587_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::Dispose()
#define Enumerator_Dispose_m18444(__this, method) (( void (*) (Enumerator_t2574 *, const MethodInfo*))Enumerator_Dispose_m13588_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::VerifyState()
#define Enumerator_VerifyState_m18445(__this, method) (( void (*) (Enumerator_t2574 *, const MethodInfo*))Enumerator_VerifyState_m13589_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::MoveNext()
#define Enumerator_MoveNext_m18446(__this, method) (( bool (*) (Enumerator_t2574 *, const MethodInfo*))Enumerator_MoveNext_m13590_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>::get_Current()
#define Enumerator_get_Current_m18447(__this, method) (( Graphic_t654 * (*) (Enumerator_t2574 *, const MethodInfo*))Enumerator_get_Current_m13591_gshared)(__this, method)
