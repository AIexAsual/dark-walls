﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GazeInputManager/OnStateChangeHandler
struct OnStateChangeHandler_t358;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// GazeInputEvent
#include "AssemblyU2DCSharp_GazeInputEvent.h"

// System.Void GazeInputManager/OnStateChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C" void OnStateChangeHandler__ctor_m2082 (OnStateChangeHandler_t358 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GazeInputManager/OnStateChangeHandler::Invoke(GazeInputEvent,System.String)
extern "C" void OnStateChangeHandler_Invoke_m2083 (OnStateChangeHandler_t358 * __this, int32_t ___ev, String_t* ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String
#include "mscorlib_System_String.h"
extern "C" void pinvoke_delegate_wrapper_OnStateChangeHandler_t358(Il2CppObject* delegate, int32_t ___ev, String_t* ___i);
// System.IAsyncResult GazeInputManager/OnStateChangeHandler::BeginInvoke(GazeInputEvent,System.String,System.AsyncCallback,System.Object)
extern "C" Object_t * OnStateChangeHandler_BeginInvoke_m2084 (OnStateChangeHandler_t358 * __this, int32_t ___ev, String_t* ___i, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GazeInputManager/OnStateChangeHandler::EndInvoke(System.IAsyncResult)
extern "C" void OnStateChangeHandler_EndInvoke_m2085 (OnStateChangeHandler_t358 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
