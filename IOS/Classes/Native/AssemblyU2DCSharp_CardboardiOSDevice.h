﻿#pragma once
#include <stdint.h>
// BaseCardboardDevice
#include "AssemblyU2DCSharp_BaseCardboardDevice.h"
// CardboardiOSDevice
struct  CardboardiOSDevice_t272  : public BaseCardboardDevice_t270
{
	// System.Boolean CardboardiOSDevice::isOpenGL
	bool ___isOpenGL_35;
	// System.Boolean CardboardiOSDevice::debugOnboarding
	bool ___debugOnboarding_36;
};
