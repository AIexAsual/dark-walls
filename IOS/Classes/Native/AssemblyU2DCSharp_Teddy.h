﻿#pragma once
#include <stdint.h>
// UnityEngine.Rigidbody
struct Rigidbody_t325;
// RagdollScript
#include "AssemblyU2DCSharp_RagdollScript.h"
// Teddy
struct  Teddy_t367  : public RagdollScript_t405
{
	// UnityEngine.Rigidbody Teddy::rb
	Rigidbody_t325 * ___rb_2;
};
