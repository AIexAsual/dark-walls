﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_FX_Drunk2
struct  CameraFilterPack_FX_Drunk2_t125  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_FX_Drunk2::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_FX_Drunk2::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_FX_Drunk2::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_FX_Drunk2::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_FX_Drunk2::Value
	float ___Value_6;
	// System.Single CameraFilterPack_FX_Drunk2::Value2
	float ___Value2_7;
	// System.Single CameraFilterPack_FX_Drunk2::Value3
	float ___Value3_8;
	// System.Single CameraFilterPack_FX_Drunk2::Value4
	float ___Value4_9;
};
struct CameraFilterPack_FX_Drunk2_t125_StaticFields{
	// System.Single CameraFilterPack_FX_Drunk2::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_FX_Drunk2::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_FX_Drunk2::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_FX_Drunk2::ChangeValue4
	float ___ChangeValue4_13;
};
