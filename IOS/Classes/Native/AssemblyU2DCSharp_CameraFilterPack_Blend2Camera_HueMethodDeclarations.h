﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_Hue
struct CameraFilterPack_Blend2Camera_Hue_t29;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_Hue::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_Hue__ctor_m145 (CameraFilterPack_Blend2Camera_Hue_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Hue::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_Hue_get_material_m146 (CameraFilterPack_Blend2Camera_Hue_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Hue::Start()
extern "C" void CameraFilterPack_Blend2Camera_Hue_Start_m147 (CameraFilterPack_Blend2Camera_Hue_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Hue::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_Hue_OnRenderImage_m148 (CameraFilterPack_Blend2Camera_Hue_t29 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Hue::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_Hue_OnValidate_m149 (CameraFilterPack_Blend2Camera_Hue_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Hue::Update()
extern "C" void CameraFilterPack_Blend2Camera_Hue_Update_m150 (CameraFilterPack_Blend2Camera_Hue_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Hue::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_Hue_OnEnable_m151 (CameraFilterPack_Blend2Camera_Hue_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Hue::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_Hue_OnDisable_m152 (CameraFilterPack_Blend2Camera_Hue_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
