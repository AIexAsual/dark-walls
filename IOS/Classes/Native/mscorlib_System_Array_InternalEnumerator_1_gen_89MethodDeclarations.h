﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>
struct InternalEnumerator_1_t2916;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Mono.Globalization.Unicode.CodePointIndexer/TableRange
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22734_gshared (InternalEnumerator_1_t2916 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22734(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2916 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22734_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22735_gshared (InternalEnumerator_1_t2916 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22735(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2916 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22735_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22736_gshared (InternalEnumerator_1_t2916 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22736(__this, method) (( void (*) (InternalEnumerator_1_t2916 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22736_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22737_gshared (InternalEnumerator_1_t2916 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22737(__this, method) (( bool (*) (InternalEnumerator_1_t2916 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22737_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern "C" TableRange_t1580  InternalEnumerator_1_get_Current_m22738_gshared (InternalEnumerator_1_t2916 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22738(__this, method) (( TableRange_t1580  (*) (InternalEnumerator_1_t2916 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22738_gshared)(__this, method)
