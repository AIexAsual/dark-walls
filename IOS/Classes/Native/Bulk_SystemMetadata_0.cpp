﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// <Module>
#include "System_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t1374_il2cpp_TypeInfo;
// <Module>
#include "System_U3CModuleU3EMethodDeclarations.h"
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType U3CModuleU3E_t1374_0_0_0;
extern const Il2CppType U3CModuleU3E_t1374_1_0_0;
struct U3CModuleU3E_t1374;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t1374_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CModuleU3E_t1374_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CModuleU3E_t1374_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t1374_0_0_0/* byval_arg */
	, &U3CModuleU3E_t1374_1_0_0/* this_arg */
	, &U3CModuleU3E_t1374_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t1374)/* instance_size */
	, sizeof (U3CModuleU3E_t1374)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Locale
#include "System_Locale.h"
// Metadata Definition Locale
extern TypeInfo Locale_t1375_il2cpp_TypeInfo;
// Locale
#include "System_LocaleMethodDeclarations.h"
static const EncodedMethodIndex Locale_t1375_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Locale_t1375_0_0_0;
extern const Il2CppType Locale_t1375_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct Locale_t1375;
const Il2CppTypeDefinitionMetadata Locale_t1375_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Locale_t1375_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7102/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Locale_t1375_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Locale"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Locale_t1375_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Locale_t1375_0_0_0/* byval_arg */
	, &Locale_t1375_1_0_0/* this_arg */
	, &Locale_t1375_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Locale_t1375)/* instance_size */
	, sizeof (Locale_t1375)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoTODOAttribute
#include "System_System_MonoTODOAttribute.h"
// Metadata Definition System.MonoTODOAttribute
extern TypeInfo MonoTODOAttribute_t1376_il2cpp_TypeInfo;
// System.MonoTODOAttribute
#include "System_System_MonoTODOAttributeMethodDeclarations.h"
static const EncodedMethodIndex MonoTODOAttribute_t1376_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
extern const Il2CppType _Attribute_t3429_0_0_0;
static Il2CppInterfaceOffsetPair MonoTODOAttribute_t1376_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType MonoTODOAttribute_t1376_0_0_0;
extern const Il2CppType MonoTODOAttribute_t1376_1_0_0;
extern const Il2CppType Attribute_t903_0_0_0;
struct MonoTODOAttribute_t1376;
const Il2CppTypeDefinitionMetadata MonoTODOAttribute_t1376_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoTODOAttribute_t1376_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, MonoTODOAttribute_t1376_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5571/* fieldStart */
	, 7104/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoTODOAttribute_t1376_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTODOAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoTODOAttribute_t1376_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2349/* custom_attributes_cache */
	, &MonoTODOAttribute_t1376_0_0_0/* byval_arg */
	, &MonoTODOAttribute_t1376_1_0_0/* this_arg */
	, &MonoTODOAttribute_t1376_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTODOAttribute_t1376)/* instance_size */
	, sizeof (MonoTODOAttribute_t1376)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.CodeDom.Compiler.GeneratedCodeAttribute
#include "System_System_CodeDom_Compiler_GeneratedCodeAttribute.h"
// Metadata Definition System.CodeDom.Compiler.GeneratedCodeAttribute
extern TypeInfo GeneratedCodeAttribute_t1377_il2cpp_TypeInfo;
// System.CodeDom.Compiler.GeneratedCodeAttribute
#include "System_System_CodeDom_Compiler_GeneratedCodeAttributeMethodDeclarations.h"
static const EncodedMethodIndex GeneratedCodeAttribute_t1377_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair GeneratedCodeAttribute_t1377_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType GeneratedCodeAttribute_t1377_0_0_0;
extern const Il2CppType GeneratedCodeAttribute_t1377_1_0_0;
struct GeneratedCodeAttribute_t1377;
const Il2CppTypeDefinitionMetadata GeneratedCodeAttribute_t1377_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GeneratedCodeAttribute_t1377_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, GeneratedCodeAttribute_t1377_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5572/* fieldStart */
	, 7106/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GeneratedCodeAttribute_t1377_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "GeneratedCodeAttribute"/* name */
	, "System.CodeDom.Compiler"/* namespaze */
	, NULL/* methods */
	, &GeneratedCodeAttribute_t1377_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2350/* custom_attributes_cache */
	, &GeneratedCodeAttribute_t1377_0_0_0/* byval_arg */
	, &GeneratedCodeAttribute_t1377_1_0_0/* this_arg */
	, &GeneratedCodeAttribute_t1377_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GeneratedCodeAttribute_t1377)/* instance_size */
	, sizeof (GeneratedCodeAttribute_t1377)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Queue`1
extern TypeInfo Queue_1_t3424_il2cpp_TypeInfo;
extern const Il2CppType Enumerator_t3425_0_0_0;
static const Il2CppType* Queue_1_t3424_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Enumerator_t3425_0_0_0,
};
static const EncodedMethodIndex Queue_1_t3424_VTable[10] = 
{
	626,
	601,
	627,
	628,
	1944,
	1945,
	1946,
	1947,
	1948,
	1949,
};
extern const Il2CppType IEnumerable_1_t3720_0_0_0;
extern const Il2CppType ICollection_t1528_0_0_0;
extern const Il2CppType IEnumerable_t1097_0_0_0;
static const Il2CppType* Queue_1_t3424_InterfacesTypeInfos[] = 
{
	&IEnumerable_1_t3720_0_0_0,
	&ICollection_t1528_0_0_0,
	&IEnumerable_t1097_0_0_0,
};
static Il2CppInterfaceOffsetPair Queue_1_t3424_InterfacesOffsets[] = 
{
	{ &IEnumerable_1_t3720_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IEnumerable_t1097_0_0_0, 9},
};
extern const Il2CppType TU5BU5D_t3721_0_0_0;
extern const Il2CppType Enumerator_t3722_0_0_0;
extern const Il2CppRGCTXDefinition Queue_1_t3424_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4494 }/* Array */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5513 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4495 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5514 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5515 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5516 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Queue_1_t3424_0_0_0;
extern const Il2CppType Queue_1_t3424_1_0_0;
struct Queue_1_t3424;
const Il2CppTypeDefinitionMetadata Queue_1_t3424_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Queue_1_t3424_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Queue_1_t3424_InterfacesTypeInfos/* implementedInterfaces */
	, Queue_1_t3424_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Queue_1_t3424_VTable/* vtableMethods */
	, Queue_1_t3424_RGCTXData/* rgctxDefinition */
	, 5574/* fieldStart */
	, 7107/* methodStart */
	, -1/* eventStart */
	, 1438/* propertyStart */

};
TypeInfo Queue_1_t3424_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Queue`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &Queue_1_t3424_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2351/* custom_attributes_cache */
	, &Queue_1_t3424_0_0_0/* byval_arg */
	, &Queue_1_t3424_1_0_0/* this_arg */
	, &Queue_1_t3424_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 75/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 3/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 10/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Queue`1/Enumerator
extern TypeInfo Enumerator_t3425_il2cpp_TypeInfo;
static const EncodedMethodIndex Enumerator_t3425_VTable[8] = 
{
	652,
	601,
	653,
	654,
	1950,
	1951,
	1952,
	1953,
};
extern const Il2CppType IEnumerator_t464_0_0_0;
extern const Il2CppType IDisposable_t538_0_0_0;
extern const Il2CppType IEnumerator_1_t3723_0_0_0;
static const Il2CppType* Enumerator_t3425_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDisposable_t538_0_0_0,
	&IEnumerator_1_t3723_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t3425_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IDisposable_t538_0_0_0, 6},
	{ &IEnumerator_1_t3723_0_0_0, 7},
};
extern const Il2CppType Enumerator_t3425_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Enumerator_t3425_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5517 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4498 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Enumerator_t3425_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata Enumerator_t3425_DefinitionMetadata = 
{
	&Queue_1_t3424_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t3425_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t3425_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Enumerator_t3425_VTable/* vtableMethods */
	, Enumerator_t3425_RGCTXData/* rgctxDefinition */
	, 5579/* fieldStart */
	, 7119/* methodStart */
	, -1/* eventStart */
	, 1441/* propertyStart */

};
TypeInfo Enumerator_t3425_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t3425_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t3425_0_0_0/* byval_arg */
	, &Enumerator_t3425_1_0_0/* this_arg */
	, &Enumerator_t3425_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 76/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Stack`1
extern TypeInfo Stack_1_t3426_il2cpp_TypeInfo;
extern const Il2CppType Enumerator_t3427_0_0_0;
static const Il2CppType* Stack_1_t3426_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Enumerator_t3427_0_0_0,
};
static const EncodedMethodIndex Stack_1_t3426_VTable[10] = 
{
	626,
	601,
	627,
	628,
	1954,
	1955,
	1956,
	1957,
	1958,
	1959,
};
extern const Il2CppType IEnumerable_1_t3725_0_0_0;
static const Il2CppType* Stack_1_t3426_InterfacesTypeInfos[] = 
{
	&ICollection_t1528_0_0_0,
	&IEnumerable_t1097_0_0_0,
	&IEnumerable_1_t3725_0_0_0,
};
static Il2CppInterfaceOffsetPair Stack_1_t3426_InterfacesOffsets[] = 
{
	{ &ICollection_t1528_0_0_0, 4},
	{ &IEnumerable_t1097_0_0_0, 8},
	{ &IEnumerable_1_t3725_0_0_0, 9},
};
extern const Il2CppType Enumerator_t3726_0_0_0;
extern const Il2CppRGCTXDefinition Stack_1_t3426_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5518 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4502 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5519 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5520 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Stack_1_t3426_0_0_0;
extern const Il2CppType Stack_1_t3426_1_0_0;
struct Stack_1_t3426;
const Il2CppTypeDefinitionMetadata Stack_1_t3426_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Stack_1_t3426_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Stack_1_t3426_InterfacesTypeInfos/* implementedInterfaces */
	, Stack_1_t3426_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Stack_1_t3426_VTable/* vtableMethods */
	, Stack_1_t3426_RGCTXData/* rgctxDefinition */
	, 5582/* fieldStart */
	, 7124/* methodStart */
	, -1/* eventStart */
	, 1443/* propertyStart */

};
TypeInfo Stack_1_t3426_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Stack`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &Stack_1_t3426_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2352/* custom_attributes_cache */
	, &Stack_1_t3426_0_0_0/* byval_arg */
	, &Stack_1_t3426_1_0_0/* this_arg */
	, &Stack_1_t3426_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 77/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 10/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Stack`1/Enumerator
extern TypeInfo Enumerator_t3427_il2cpp_TypeInfo;
static const EncodedMethodIndex Enumerator_t3427_VTable[8] = 
{
	652,
	601,
	653,
	654,
	1960,
	1961,
	1962,
	1963,
};
extern const Il2CppType IEnumerator_1_t3727_0_0_0;
static const Il2CppType* Enumerator_t3427_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDisposable_t538_0_0_0,
	&IEnumerator_1_t3727_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t3427_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IDisposable_t538_0_0_0, 6},
	{ &IEnumerator_1_t3727_0_0_0, 7},
};
extern const Il2CppType Enumerator_t3427_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Enumerator_t3427_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5521 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4505 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Enumerator_t3427_1_0_0;
const Il2CppTypeDefinitionMetadata Enumerator_t3427_DefinitionMetadata = 
{
	&Stack_1_t3426_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t3427_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t3427_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Enumerator_t3427_VTable/* vtableMethods */
	, Enumerator_t3427_RGCTXData/* rgctxDefinition */
	, 5586/* fieldStart */
	, 7135/* methodStart */
	, -1/* eventStart */
	, 1446/* propertyStart */

};
TypeInfo Enumerator_t3427_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t3427_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t3427_0_0_0/* byval_arg */
	, &Enumerator_t3427_1_0_0/* this_arg */
	, &Enumerator_t3427_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 78/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.Specialized.HybridDictionary
#include "System_System_Collections_Specialized_HybridDictionary.h"
// Metadata Definition System.Collections.Specialized.HybridDictionary
extern TypeInfo HybridDictionary_t1379_il2cpp_TypeInfo;
// System.Collections.Specialized.HybridDictionary
#include "System_System_Collections_Specialized_HybridDictionaryMethodDeclarations.h"
static const EncodedMethodIndex HybridDictionary_t1379_VTable[15] = 
{
	626,
	601,
	627,
	628,
	1964,
	1965,
	1966,
	1967,
	1968,
	1969,
	1970,
	1971,
	1972,
	1973,
	1974,
};
extern const Il2CppType IDictionary_t1462_0_0_0;
static const Il2CppType* HybridDictionary_t1379_InterfacesTypeInfos[] = 
{
	&ICollection_t1528_0_0_0,
	&IDictionary_t1462_0_0_0,
	&IEnumerable_t1097_0_0_0,
};
static Il2CppInterfaceOffsetPair HybridDictionary_t1379_InterfacesOffsets[] = 
{
	{ &ICollection_t1528_0_0_0, 4},
	{ &IDictionary_t1462_0_0_0, 8},
	{ &IEnumerable_t1097_0_0_0, 14},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType HybridDictionary_t1379_0_0_0;
extern const Il2CppType HybridDictionary_t1379_1_0_0;
struct HybridDictionary_t1379;
const Il2CppTypeDefinitionMetadata HybridDictionary_t1379_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, HybridDictionary_t1379_InterfacesTypeInfos/* implementedInterfaces */
	, HybridDictionary_t1379_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, HybridDictionary_t1379_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5589/* fieldStart */
	, 7140/* methodStart */
	, -1/* eventStart */
	, 1448/* propertyStart */

};
TypeInfo HybridDictionary_t1379_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "HybridDictionary"/* name */
	, "System.Collections.Specialized"/* namespaze */
	, NULL/* methods */
	, &HybridDictionary_t1379_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2353/* custom_attributes_cache */
	, &HybridDictionary_t1379_0_0_0/* byval_arg */
	, &HybridDictionary_t1379_1_0_0/* this_arg */
	, &HybridDictionary_t1379_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HybridDictionary_t1379)/* instance_size */
	, sizeof (HybridDictionary_t1379)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.Specialized.ListDictionary
#include "System_System_Collections_Specialized_ListDictionary.h"
// Metadata Definition System.Collections.Specialized.ListDictionary
extern TypeInfo ListDictionary_t1378_il2cpp_TypeInfo;
// System.Collections.Specialized.ListDictionary
#include "System_System_Collections_Specialized_ListDictionaryMethodDeclarations.h"
extern const Il2CppType DictionaryNode_t1380_0_0_0;
extern const Il2CppType DictionaryNodeEnumerator_t1381_0_0_0;
static const Il2CppType* ListDictionary_t1378_il2cpp_TypeInfo__nestedTypes[2] =
{
	&DictionaryNode_t1380_0_0_0,
	&DictionaryNodeEnumerator_t1381_0_0_0,
};
static const EncodedMethodIndex ListDictionary_t1378_VTable[16] = 
{
	626,
	601,
	627,
	628,
	1975,
	1976,
	1977,
	1978,
	1979,
	1980,
	1981,
	1982,
	1983,
	1984,
	1985,
	1986,
};
static const Il2CppType* ListDictionary_t1378_InterfacesTypeInfos[] = 
{
	&ICollection_t1528_0_0_0,
	&IDictionary_t1462_0_0_0,
	&IEnumerable_t1097_0_0_0,
};
static Il2CppInterfaceOffsetPair ListDictionary_t1378_InterfacesOffsets[] = 
{
	{ &ICollection_t1528_0_0_0, 4},
	{ &IDictionary_t1462_0_0_0, 8},
	{ &IEnumerable_t1097_0_0_0, 14},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ListDictionary_t1378_0_0_0;
extern const Il2CppType ListDictionary_t1378_1_0_0;
struct ListDictionary_t1378;
const Il2CppTypeDefinitionMetadata ListDictionary_t1378_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ListDictionary_t1378_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ListDictionary_t1378_InterfacesTypeInfos/* implementedInterfaces */
	, ListDictionary_t1378_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ListDictionary_t1378_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5592/* fieldStart */
	, 7155/* methodStart */
	, -1/* eventStart */
	, 1453/* propertyStart */

};
TypeInfo ListDictionary_t1378_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ListDictionary"/* name */
	, "System.Collections.Specialized"/* namespaze */
	, NULL/* methods */
	, &ListDictionary_t1378_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2354/* custom_attributes_cache */
	, &ListDictionary_t1378_0_0_0/* byval_arg */
	, &ListDictionary_t1378_1_0_0/* this_arg */
	, &ListDictionary_t1378_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ListDictionary_t1378)/* instance_size */
	, sizeof (ListDictionary_t1378)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 4/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 16/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.Specialized.ListDictionary/DictionaryNode
#include "System_System_Collections_Specialized_ListDictionary_Diction.h"
// Metadata Definition System.Collections.Specialized.ListDictionary/DictionaryNode
extern TypeInfo DictionaryNode_t1380_il2cpp_TypeInfo;
// System.Collections.Specialized.ListDictionary/DictionaryNode
#include "System_System_Collections_Specialized_ListDictionary_DictionMethodDeclarations.h"
static const EncodedMethodIndex DictionaryNode_t1380_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType DictionaryNode_t1380_1_0_0;
struct DictionaryNode_t1380;
const Il2CppTypeDefinitionMetadata DictionaryNode_t1380_DefinitionMetadata = 
{
	&ListDictionary_t1378_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DictionaryNode_t1380_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5596/* fieldStart */
	, 7172/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DictionaryNode_t1380_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "DictionaryNode"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DictionaryNode_t1380_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DictionaryNode_t1380_0_0_0/* byval_arg */
	, &DictionaryNode_t1380_1_0_0/* this_arg */
	, &DictionaryNode_t1380_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DictionaryNode_t1380)/* instance_size */
	, sizeof (DictionaryNode_t1380)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator
#include "System_System_Collections_Specialized_ListDictionary_Diction_0.h"
// Metadata Definition System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator
extern TypeInfo DictionaryNodeEnumerator_t1381_il2cpp_TypeInfo;
// System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator
#include "System_System_Collections_Specialized_ListDictionary_Diction_0MethodDeclarations.h"
static const EncodedMethodIndex DictionaryNodeEnumerator_t1381_VTable[10] = 
{
	626,
	601,
	627,
	628,
	1987,
	1988,
	1989,
	1990,
	1991,
	1992,
};
extern const Il2CppType IDictionaryEnumerator_t1527_0_0_0;
static const Il2CppType* DictionaryNodeEnumerator_t1381_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDictionaryEnumerator_t1527_0_0_0,
};
static Il2CppInterfaceOffsetPair DictionaryNodeEnumerator_t1381_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IDictionaryEnumerator_t1527_0_0_0, 6},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType DictionaryNodeEnumerator_t1381_1_0_0;
struct DictionaryNodeEnumerator_t1381;
const Il2CppTypeDefinitionMetadata DictionaryNodeEnumerator_t1381_DefinitionMetadata = 
{
	&ListDictionary_t1378_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, DictionaryNodeEnumerator_t1381_InterfacesTypeInfos/* implementedInterfaces */
	, DictionaryNodeEnumerator_t1381_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DictionaryNodeEnumerator_t1381_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5599/* fieldStart */
	, 7173/* methodStart */
	, -1/* eventStart */
	, 1457/* propertyStart */

};
TypeInfo DictionaryNodeEnumerator_t1381_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "DictionaryNodeEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DictionaryNodeEnumerator_t1381_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DictionaryNodeEnumerator_t1381_0_0_0/* byval_arg */
	, &DictionaryNodeEnumerator_t1381_1_0_0/* this_arg */
	, &DictionaryNodeEnumerator_t1381_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DictionaryNodeEnumerator_t1381)/* instance_size */
	, sizeof (DictionaryNodeEnumerator_t1381)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 5/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Specialized.NameObjectCollectionBase
#include "System_System_Collections_Specialized_NameObjectCollectionBa_2.h"
// Metadata Definition System.Collections.Specialized.NameObjectCollectionBase
extern TypeInfo NameObjectCollectionBase_t1383_il2cpp_TypeInfo;
// System.Collections.Specialized.NameObjectCollectionBase
#include "System_System_Collections_Specialized_NameObjectCollectionBa_2MethodDeclarations.h"
extern const Il2CppType _Item_t1382_0_0_0;
extern const Il2CppType _KeysEnumerator_t1384_0_0_0;
extern const Il2CppType KeysCollection_t1385_0_0_0;
static const Il2CppType* NameObjectCollectionBase_t1383_il2cpp_TypeInfo__nestedTypes[3] =
{
	&_Item_t1382_0_0_0,
	&_KeysEnumerator_t1384_0_0_0,
	&KeysCollection_t1385_0_0_0,
};
static const EncodedMethodIndex NameObjectCollectionBase_t1383_VTable[16] = 
{
	626,
	601,
	627,
	628,
	1993,
	1994,
	1995,
	1996,
	1997,
	1998,
	1999,
	2000,
	1998,
	1999,
	1993,
	1997,
};
extern const Il2CppType IDeserializationCallback_t2213_0_0_0;
extern const Il2CppType ISerializable_t2210_0_0_0;
static const Il2CppType* NameObjectCollectionBase_t1383_InterfacesTypeInfos[] = 
{
	&ICollection_t1528_0_0_0,
	&IDeserializationCallback_t2213_0_0_0,
	&IEnumerable_t1097_0_0_0,
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair NameObjectCollectionBase_t1383_InterfacesOffsets[] = 
{
	{ &ICollection_t1528_0_0_0, 4},
	{ &IDeserializationCallback_t2213_0_0_0, 8},
	{ &IEnumerable_t1097_0_0_0, 9},
	{ &ISerializable_t2210_0_0_0, 10},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType NameObjectCollectionBase_t1383_0_0_0;
extern const Il2CppType NameObjectCollectionBase_t1383_1_0_0;
struct NameObjectCollectionBase_t1383;
const Il2CppTypeDefinitionMetadata NameObjectCollectionBase_t1383_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NameObjectCollectionBase_t1383_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NameObjectCollectionBase_t1383_InterfacesTypeInfos/* implementedInterfaces */
	, NameObjectCollectionBase_t1383_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, NameObjectCollectionBase_t1383_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5603/* fieldStart */
	, 7182/* methodStart */
	, -1/* eventStart */
	, 1462/* propertyStart */

};
TypeInfo NameObjectCollectionBase_t1383_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "NameObjectCollectionBase"/* name */
	, "System.Collections.Specialized"/* namespaze */
	, NULL/* methods */
	, &NameObjectCollectionBase_t1383_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NameObjectCollectionBase_t1383_0_0_0/* byval_arg */
	, &NameObjectCollectionBase_t1383_1_0_0/* this_arg */
	, &NameObjectCollectionBase_t1383_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NameObjectCollectionBase_t1383)/* instance_size */
	, sizeof (NameObjectCollectionBase_t1383)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 5/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 16/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Collections.Specialized.NameObjectCollectionBase/_Item
#include "System_System_Collections_Specialized_NameObjectCollectionBa.h"
// Metadata Definition System.Collections.Specialized.NameObjectCollectionBase/_Item
extern TypeInfo _Item_t1382_il2cpp_TypeInfo;
// System.Collections.Specialized.NameObjectCollectionBase/_Item
#include "System_System_Collections_Specialized_NameObjectCollectionBaMethodDeclarations.h"
static const EncodedMethodIndex _Item_t1382_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType _Item_t1382_1_0_0;
struct _Item_t1382;
const Il2CppTypeDefinitionMetadata _Item_t1382_DefinitionMetadata = 
{
	&NameObjectCollectionBase_t1383_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, _Item_t1382_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5613/* fieldStart */
	, 7199/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _Item_t1382_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Item"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &_Item_t1382_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &_Item_t1382_0_0_0/* byval_arg */
	, &_Item_t1382_1_0_0/* this_arg */
	, &_Item_t1382_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (_Item_t1382)/* instance_size */
	, sizeof (_Item_t1382)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048581/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator
#include "System_System_Collections_Specialized_NameObjectCollectionBa_0.h"
// Metadata Definition System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator
extern TypeInfo _KeysEnumerator_t1384_il2cpp_TypeInfo;
// System.Collections.Specialized.NameObjectCollectionBase/_KeysEnumerator
#include "System_System_Collections_Specialized_NameObjectCollectionBa_0MethodDeclarations.h"
static const EncodedMethodIndex _KeysEnumerator_t1384_VTable[7] = 
{
	626,
	601,
	627,
	628,
	2001,
	2002,
	2003,
};
static const Il2CppType* _KeysEnumerator_t1384_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
};
static Il2CppInterfaceOffsetPair _KeysEnumerator_t1384_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType _KeysEnumerator_t1384_1_0_0;
struct _KeysEnumerator_t1384;
const Il2CppTypeDefinitionMetadata _KeysEnumerator_t1384_DefinitionMetadata = 
{
	&NameObjectCollectionBase_t1383_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, _KeysEnumerator_t1384_InterfacesTypeInfos/* implementedInterfaces */
	, _KeysEnumerator_t1384_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, _KeysEnumerator_t1384_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5615/* fieldStart */
	, 7200/* methodStart */
	, -1/* eventStart */
	, 1467/* propertyStart */

};
TypeInfo _KeysEnumerator_t1384_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "_KeysEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &_KeysEnumerator_t1384_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &_KeysEnumerator_t1384_0_0_0/* byval_arg */
	, &_KeysEnumerator_t1384_1_0_0/* this_arg */
	, &_KeysEnumerator_t1384_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (_KeysEnumerator_t1384)/* instance_size */
	, sizeof (_KeysEnumerator_t1384)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056773/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
#include "System_System_Collections_Specialized_NameObjectCollectionBa_1.h"
// Metadata Definition System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
extern TypeInfo KeysCollection_t1385_il2cpp_TypeInfo;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
#include "System_System_Collections_Specialized_NameObjectCollectionBa_1MethodDeclarations.h"
static const EncodedMethodIndex KeysCollection_t1385_VTable[9] = 
{
	626,
	601,
	627,
	628,
	2004,
	2005,
	2006,
	2007,
	2008,
};
static const Il2CppType* KeysCollection_t1385_InterfacesTypeInfos[] = 
{
	&ICollection_t1528_0_0_0,
	&IEnumerable_t1097_0_0_0,
};
static Il2CppInterfaceOffsetPair KeysCollection_t1385_InterfacesOffsets[] = 
{
	{ &ICollection_t1528_0_0_0, 4},
	{ &IEnumerable_t1097_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType KeysCollection_t1385_1_0_0;
struct KeysCollection_t1385;
const Il2CppTypeDefinitionMetadata KeysCollection_t1385_DefinitionMetadata = 
{
	&NameObjectCollectionBase_t1383_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, KeysCollection_t1385_InterfacesTypeInfos/* implementedInterfaces */
	, KeysCollection_t1385_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, KeysCollection_t1385_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5617/* fieldStart */
	, 7204/* methodStart */
	, -1/* eventStart */
	, 1468/* propertyStart */

};
TypeInfo KeysCollection_t1385_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeysCollection"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &KeysCollection_t1385_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2356/* custom_attributes_cache */
	, &KeysCollection_t1385_0_0_0/* byval_arg */
	, &KeysCollection_t1385_1_0_0/* this_arg */
	, &KeysCollection_t1385_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeysCollection_t1385)/* instance_size */
	, sizeof (KeysCollection_t1385)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Specialized.NameValueCollection
#include "System_System_Collections_Specialized_NameValueCollection.h"
// Metadata Definition System.Collections.Specialized.NameValueCollection
extern TypeInfo NameValueCollection_t1388_il2cpp_TypeInfo;
// System.Collections.Specialized.NameValueCollection
#include "System_System_Collections_Specialized_NameValueCollectionMethodDeclarations.h"
static const EncodedMethodIndex NameValueCollection_t1388_VTable[19] = 
{
	626,
	601,
	627,
	628,
	1993,
	1994,
	1995,
	1996,
	1997,
	1998,
	1999,
	2000,
	1998,
	1999,
	1993,
	1997,
	2009,
	2010,
	2011,
};
static Il2CppInterfaceOffsetPair NameValueCollection_t1388_InterfacesOffsets[] = 
{
	{ &ICollection_t1528_0_0_0, 4},
	{ &IDeserializationCallback_t2213_0_0_0, 8},
	{ &IEnumerable_t1097_0_0_0, 9},
	{ &ISerializable_t2210_0_0_0, 10},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType NameValueCollection_t1388_0_0_0;
extern const Il2CppType NameValueCollection_t1388_1_0_0;
struct NameValueCollection_t1388;
const Il2CppTypeDefinitionMetadata NameValueCollection_t1388_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NameValueCollection_t1388_InterfacesOffsets/* interfaceOffsets */
	, &NameObjectCollectionBase_t1383_0_0_0/* parent */
	, NameValueCollection_t1388_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5618/* fieldStart */
	, 7210/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NameValueCollection_t1388_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "NameValueCollection"/* name */
	, "System.Collections.Specialized"/* namespaze */
	, NULL/* methods */
	, &NameValueCollection_t1388_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2357/* custom_attributes_cache */
	, &NameValueCollection_t1388_0_0_0/* byval_arg */
	, &NameValueCollection_t1388_1_0_0/* this_arg */
	, &NameValueCollection_t1388_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NameValueCollection_t1388)/* instance_size */
	, sizeof (NameValueCollection_t1388)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.ComponentModel.DefaultValueAttribute
#include "System_System_ComponentModel_DefaultValueAttribute.h"
// Metadata Definition System.ComponentModel.DefaultValueAttribute
extern TypeInfo DefaultValueAttribute_t1389_il2cpp_TypeInfo;
// System.ComponentModel.DefaultValueAttribute
#include "System_System_ComponentModel_DefaultValueAttributeMethodDeclarations.h"
static const EncodedMethodIndex DefaultValueAttribute_t1389_VTable[5] = 
{
	2012,
	601,
	2013,
	628,
	2014,
};
static Il2CppInterfaceOffsetPair DefaultValueAttribute_t1389_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType DefaultValueAttribute_t1389_0_0_0;
extern const Il2CppType DefaultValueAttribute_t1389_1_0_0;
struct DefaultValueAttribute_t1389;
const Il2CppTypeDefinitionMetadata DefaultValueAttribute_t1389_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultValueAttribute_t1389_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, DefaultValueAttribute_t1389_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5620/* fieldStart */
	, 7217/* methodStart */
	, -1/* eventStart */
	, 1471/* propertyStart */

};
TypeInfo DefaultValueAttribute_t1389_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultValueAttribute"/* name */
	, "System.ComponentModel"/* namespaze */
	, NULL/* methods */
	, &DefaultValueAttribute_t1389_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2358/* custom_attributes_cache */
	, &DefaultValueAttribute_t1389_0_0_0/* byval_arg */
	, &DefaultValueAttribute_t1389_1_0_0/* this_arg */
	, &DefaultValueAttribute_t1389_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultValueAttribute_t1389)/* instance_size */
	, sizeof (DefaultValueAttribute_t1389)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttribute.h"
// Metadata Definition System.ComponentModel.EditorBrowsableAttribute
extern TypeInfo EditorBrowsableAttribute_t1390_il2cpp_TypeInfo;
// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttributeMethodDeclarations.h"
static const EncodedMethodIndex EditorBrowsableAttribute_t1390_VTable[4] = 
{
	2015,
	601,
	2016,
	628,
};
static Il2CppInterfaceOffsetPair EditorBrowsableAttribute_t1390_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType EditorBrowsableAttribute_t1390_0_0_0;
extern const Il2CppType EditorBrowsableAttribute_t1390_1_0_0;
struct EditorBrowsableAttribute_t1390;
const Il2CppTypeDefinitionMetadata EditorBrowsableAttribute_t1390_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EditorBrowsableAttribute_t1390_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, EditorBrowsableAttribute_t1390_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5621/* fieldStart */
	, 7221/* methodStart */
	, -1/* eventStart */
	, 1472/* propertyStart */

};
TypeInfo EditorBrowsableAttribute_t1390_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "EditorBrowsableAttribute"/* name */
	, "System.ComponentModel"/* namespaze */
	, NULL/* methods */
	, &EditorBrowsableAttribute_t1390_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2359/* custom_attributes_cache */
	, &EditorBrowsableAttribute_t1390_0_0_0/* byval_arg */
	, &EditorBrowsableAttribute_t1390_1_0_0/* this_arg */
	, &EditorBrowsableAttribute_t1390_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EditorBrowsableAttribute_t1390)/* instance_size */
	, sizeof (EditorBrowsableAttribute_t1390)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.ComponentModel.EditorBrowsableState
#include "System_System_ComponentModel_EditorBrowsableState.h"
// Metadata Definition System.ComponentModel.EditorBrowsableState
extern TypeInfo EditorBrowsableState_t1391_il2cpp_TypeInfo;
// System.ComponentModel.EditorBrowsableState
#include "System_System_ComponentModel_EditorBrowsableStateMethodDeclarations.h"
static const EncodedMethodIndex EditorBrowsableState_t1391_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair EditorBrowsableState_t1391_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType EditorBrowsableState_t1391_0_0_0;
extern const Il2CppType EditorBrowsableState_t1391_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata EditorBrowsableState_t1391_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EditorBrowsableState_t1391_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, EditorBrowsableState_t1391_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5622/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EditorBrowsableState_t1391_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "EditorBrowsableState"/* name */
	, "System.ComponentModel"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EditorBrowsableState_t1391_0_0_0/* byval_arg */
	, &EditorBrowsableState_t1391_1_0_0/* this_arg */
	, &EditorBrowsableState_t1391_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EditorBrowsableState_t1391)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EditorBrowsableState_t1391)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.ComponentModel.TypeConverter
#include "System_System_ComponentModel_TypeConverter.h"
// Metadata Definition System.ComponentModel.TypeConverter
extern TypeInfo TypeConverter_t1392_il2cpp_TypeInfo;
// System.ComponentModel.TypeConverter
#include "System_System_ComponentModel_TypeConverterMethodDeclarations.h"
static const EncodedMethodIndex TypeConverter_t1392_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType TypeConverter_t1392_0_0_0;
extern const Il2CppType TypeConverter_t1392_1_0_0;
struct TypeConverter_t1392;
const Il2CppTypeDefinitionMetadata TypeConverter_t1392_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TypeConverter_t1392_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeConverter_t1392_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeConverter"/* name */
	, "System.ComponentModel"/* namespaze */
	, NULL/* methods */
	, &TypeConverter_t1392_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2360/* custom_attributes_cache */
	, &TypeConverter_t1392_0_0_0/* byval_arg */
	, &TypeConverter_t1392_1_0_0/* this_arg */
	, &TypeConverter_t1392_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeConverter_t1392)/* instance_size */
	, sizeof (TypeConverter_t1392)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ComponentModel.TypeConverterAttribute
#include "System_System_ComponentModel_TypeConverterAttribute.h"
// Metadata Definition System.ComponentModel.TypeConverterAttribute
extern TypeInfo TypeConverterAttribute_t1393_il2cpp_TypeInfo;
// System.ComponentModel.TypeConverterAttribute
#include "System_System_ComponentModel_TypeConverterAttributeMethodDeclarations.h"
static const EncodedMethodIndex TypeConverterAttribute_t1393_VTable[4] = 
{
	2017,
	601,
	2018,
	628,
};
static Il2CppInterfaceOffsetPair TypeConverterAttribute_t1393_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType TypeConverterAttribute_t1393_0_0_0;
extern const Il2CppType TypeConverterAttribute_t1393_1_0_0;
struct TypeConverterAttribute_t1393;
const Il2CppTypeDefinitionMetadata TypeConverterAttribute_t1393_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeConverterAttribute_t1393_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, TypeConverterAttribute_t1393_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5626/* fieldStart */
	, 7225/* methodStart */
	, -1/* eventStart */
	, 1473/* propertyStart */

};
TypeInfo TypeConverterAttribute_t1393_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeConverterAttribute"/* name */
	, "System.ComponentModel"/* namespaze */
	, NULL/* methods */
	, &TypeConverterAttribute_t1393_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2361/* custom_attributes_cache */
	, &TypeConverterAttribute_t1393_0_0_0/* byval_arg */
	, &TypeConverterAttribute_t1393_1_0_0/* this_arg */
	, &TypeConverterAttribute_t1393_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeConverterAttribute_t1393)/* instance_size */
	, sizeof (TypeConverterAttribute_t1393)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TypeConverterAttribute_t1393_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Net.Security.AuthenticationLevel
#include "System_System_Net_Security_AuthenticationLevel.h"
// Metadata Definition System.Net.Security.AuthenticationLevel
extern TypeInfo AuthenticationLevel_t1394_il2cpp_TypeInfo;
// System.Net.Security.AuthenticationLevel
#include "System_System_Net_Security_AuthenticationLevelMethodDeclarations.h"
static const EncodedMethodIndex AuthenticationLevel_t1394_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair AuthenticationLevel_t1394_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType AuthenticationLevel_t1394_0_0_0;
extern const Il2CppType AuthenticationLevel_t1394_1_0_0;
const Il2CppTypeDefinitionMetadata AuthenticationLevel_t1394_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AuthenticationLevel_t1394_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, AuthenticationLevel_t1394_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5628/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AuthenticationLevel_t1394_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "AuthenticationLevel"/* name */
	, "System.Net.Security"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AuthenticationLevel_t1394_0_0_0/* byval_arg */
	, &AuthenticationLevel_t1394_1_0_0/* this_arg */
	, &AuthenticationLevel_t1394_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AuthenticationLevel_t1394)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AuthenticationLevel_t1394)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Net.Security.SslPolicyErrors
#include "System_System_Net_Security_SslPolicyErrors.h"
// Metadata Definition System.Net.Security.SslPolicyErrors
extern TypeInfo SslPolicyErrors_t1395_il2cpp_TypeInfo;
// System.Net.Security.SslPolicyErrors
#include "System_System_Net_Security_SslPolicyErrorsMethodDeclarations.h"
static const EncodedMethodIndex SslPolicyErrors_t1395_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair SslPolicyErrors_t1395_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType SslPolicyErrors_t1395_0_0_0;
extern const Il2CppType SslPolicyErrors_t1395_1_0_0;
const Il2CppTypeDefinitionMetadata SslPolicyErrors_t1395_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SslPolicyErrors_t1395_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, SslPolicyErrors_t1395_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5632/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SslPolicyErrors_t1395_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "SslPolicyErrors"/* name */
	, "System.Net.Security"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2362/* custom_attributes_cache */
	, &SslPolicyErrors_t1395_0_0_0/* byval_arg */
	, &SslPolicyErrors_t1395_1_0_0/* this_arg */
	, &SslPolicyErrors_t1395_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SslPolicyErrors_t1395)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SslPolicyErrors_t1395)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Net.Sockets.AddressFamily
#include "System_System_Net_Sockets_AddressFamily.h"
// Metadata Definition System.Net.Sockets.AddressFamily
extern TypeInfo AddressFamily_t1396_il2cpp_TypeInfo;
// System.Net.Sockets.AddressFamily
#include "System_System_Net_Sockets_AddressFamilyMethodDeclarations.h"
static const EncodedMethodIndex AddressFamily_t1396_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair AddressFamily_t1396_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType AddressFamily_t1396_0_0_0;
extern const Il2CppType AddressFamily_t1396_1_0_0;
const Il2CppTypeDefinitionMetadata AddressFamily_t1396_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AddressFamily_t1396_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, AddressFamily_t1396_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5637/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AddressFamily_t1396_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "AddressFamily"/* name */
	, "System.Net.Sockets"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AddressFamily_t1396_0_0_0/* byval_arg */
	, &AddressFamily_t1396_1_0_0/* this_arg */
	, &AddressFamily_t1396_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AddressFamily_t1396)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AddressFamily_t1396)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 32/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Net.DefaultCertificatePolicy
#include "System_System_Net_DefaultCertificatePolicy.h"
// Metadata Definition System.Net.DefaultCertificatePolicy
extern TypeInfo DefaultCertificatePolicy_t1397_il2cpp_TypeInfo;
// System.Net.DefaultCertificatePolicy
#include "System_System_Net_DefaultCertificatePolicyMethodDeclarations.h"
static const EncodedMethodIndex DefaultCertificatePolicy_t1397_VTable[5] = 
{
	626,
	601,
	627,
	628,
	2019,
};
extern const Il2CppType ICertificatePolicy_t1365_0_0_0;
static const Il2CppType* DefaultCertificatePolicy_t1397_InterfacesTypeInfos[] = 
{
	&ICertificatePolicy_t1365_0_0_0,
};
static Il2CppInterfaceOffsetPair DefaultCertificatePolicy_t1397_InterfacesOffsets[] = 
{
	{ &ICertificatePolicy_t1365_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType DefaultCertificatePolicy_t1397_0_0_0;
extern const Il2CppType DefaultCertificatePolicy_t1397_1_0_0;
struct DefaultCertificatePolicy_t1397;
const Il2CppTypeDefinitionMetadata DefaultCertificatePolicy_t1397_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, DefaultCertificatePolicy_t1397_InterfacesTypeInfos/* implementedInterfaces */
	, DefaultCertificatePolicy_t1397_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DefaultCertificatePolicy_t1397_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7231/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DefaultCertificatePolicy_t1397_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultCertificatePolicy"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &DefaultCertificatePolicy_t1397_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultCertificatePolicy_t1397_0_0_0/* byval_arg */
	, &DefaultCertificatePolicy_t1397_1_0_0/* this_arg */
	, &DefaultCertificatePolicy_t1397_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultCertificatePolicy_t1397)/* instance_size */
	, sizeof (DefaultCertificatePolicy_t1397)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Net.FileWebRequest
#include "System_System_Net_FileWebRequest.h"
// Metadata Definition System.Net.FileWebRequest
extern TypeInfo FileWebRequest_t1400_il2cpp_TypeInfo;
// System.Net.FileWebRequest
#include "System_System_Net_FileWebRequestMethodDeclarations.h"
static const EncodedMethodIndex FileWebRequest_t1400_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2020,
	2021,
};
static const Il2CppType* FileWebRequest_t1400_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair FileWebRequest_t1400_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType FileWebRequest_t1400_0_0_0;
extern const Il2CppType FileWebRequest_t1400_1_0_0;
extern const Il2CppType WebRequest_t1364_0_0_0;
struct FileWebRequest_t1400;
const Il2CppTypeDefinitionMetadata FileWebRequest_t1400_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FileWebRequest_t1400_InterfacesTypeInfos/* implementedInterfaces */
	, FileWebRequest_t1400_InterfacesOffsets/* interfaceOffsets */
	, &WebRequest_t1364_0_0_0/* parent */
	, FileWebRequest_t1400_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5669/* fieldStart */
	, 7233/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FileWebRequest_t1400_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileWebRequest"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &FileWebRequest_t1400_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FileWebRequest_t1400_0_0_0/* byval_arg */
	, &FileWebRequest_t1400_1_0_0/* this_arg */
	, &FileWebRequest_t1400_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileWebRequest_t1400)/* instance_size */
	, sizeof (FileWebRequest_t1400)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Net.FileWebRequestCreator
#include "System_System_Net_FileWebRequestCreator.h"
// Metadata Definition System.Net.FileWebRequestCreator
extern TypeInfo FileWebRequestCreator_t1401_il2cpp_TypeInfo;
// System.Net.FileWebRequestCreator
#include "System_System_Net_FileWebRequestCreatorMethodDeclarations.h"
static const EncodedMethodIndex FileWebRequestCreator_t1401_VTable[5] = 
{
	626,
	601,
	627,
	628,
	2022,
};
extern const Il2CppType IWebRequestCreate_t3428_0_0_0;
static const Il2CppType* FileWebRequestCreator_t1401_InterfacesTypeInfos[] = 
{
	&IWebRequestCreate_t3428_0_0_0,
};
static Il2CppInterfaceOffsetPair FileWebRequestCreator_t1401_InterfacesOffsets[] = 
{
	{ &IWebRequestCreate_t3428_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType FileWebRequestCreator_t1401_0_0_0;
extern const Il2CppType FileWebRequestCreator_t1401_1_0_0;
struct FileWebRequestCreator_t1401;
const Il2CppTypeDefinitionMetadata FileWebRequestCreator_t1401_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FileWebRequestCreator_t1401_InterfacesTypeInfos/* implementedInterfaces */
	, FileWebRequestCreator_t1401_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FileWebRequestCreator_t1401_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7237/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FileWebRequestCreator_t1401_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileWebRequestCreator"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &FileWebRequestCreator_t1401_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FileWebRequestCreator_t1401_0_0_0/* byval_arg */
	, &FileWebRequestCreator_t1401_1_0_0/* this_arg */
	, &FileWebRequestCreator_t1401_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileWebRequestCreator_t1401)/* instance_size */
	, sizeof (FileWebRequestCreator_t1401)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Net.FtpRequestCreator
#include "System_System_Net_FtpRequestCreator.h"
// Metadata Definition System.Net.FtpRequestCreator
extern TypeInfo FtpRequestCreator_t1402_il2cpp_TypeInfo;
// System.Net.FtpRequestCreator
#include "System_System_Net_FtpRequestCreatorMethodDeclarations.h"
static const EncodedMethodIndex FtpRequestCreator_t1402_VTable[5] = 
{
	626,
	601,
	627,
	628,
	2023,
};
static const Il2CppType* FtpRequestCreator_t1402_InterfacesTypeInfos[] = 
{
	&IWebRequestCreate_t3428_0_0_0,
};
static Il2CppInterfaceOffsetPair FtpRequestCreator_t1402_InterfacesOffsets[] = 
{
	{ &IWebRequestCreate_t3428_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType FtpRequestCreator_t1402_0_0_0;
extern const Il2CppType FtpRequestCreator_t1402_1_0_0;
struct FtpRequestCreator_t1402;
const Il2CppTypeDefinitionMetadata FtpRequestCreator_t1402_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FtpRequestCreator_t1402_InterfacesTypeInfos/* implementedInterfaces */
	, FtpRequestCreator_t1402_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FtpRequestCreator_t1402_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7239/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FtpRequestCreator_t1402_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "FtpRequestCreator"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &FtpRequestCreator_t1402_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FtpRequestCreator_t1402_0_0_0/* byval_arg */
	, &FtpRequestCreator_t1402_1_0_0/* this_arg */
	, &FtpRequestCreator_t1402_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FtpRequestCreator_t1402)/* instance_size */
	, sizeof (FtpRequestCreator_t1402)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Net.FtpWebRequest
#include "System_System_Net_FtpWebRequest.h"
// Metadata Definition System.Net.FtpWebRequest
extern TypeInfo FtpWebRequest_t1403_il2cpp_TypeInfo;
// System.Net.FtpWebRequest
#include "System_System_Net_FtpWebRequestMethodDeclarations.h"
static const EncodedMethodIndex FtpWebRequest_t1403_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2024,
	2025,
};
static Il2CppInterfaceOffsetPair FtpWebRequest_t1403_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType FtpWebRequest_t1403_0_0_0;
extern const Il2CppType FtpWebRequest_t1403_1_0_0;
struct FtpWebRequest_t1403;
const Il2CppTypeDefinitionMetadata FtpWebRequest_t1403_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FtpWebRequest_t1403_InterfacesOffsets/* interfaceOffsets */
	, &WebRequest_t1364_0_0_0/* parent */
	, FtpWebRequest_t1403_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5678/* fieldStart */
	, 7241/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FtpWebRequest_t1403_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "FtpWebRequest"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &FtpWebRequest_t1403_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FtpWebRequest_t1403_0_0_0/* byval_arg */
	, &FtpWebRequest_t1403_1_0_0/* this_arg */
	, &FtpWebRequest_t1403_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FtpWebRequest_t1403)/* instance_size */
	, sizeof (FtpWebRequest_t1403)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(FtpWebRequest_t1403_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Net.GlobalProxySelection
#include "System_System_Net_GlobalProxySelection.h"
// Metadata Definition System.Net.GlobalProxySelection
extern TypeInfo GlobalProxySelection_t1404_il2cpp_TypeInfo;
// System.Net.GlobalProxySelection
#include "System_System_Net_GlobalProxySelectionMethodDeclarations.h"
static const EncodedMethodIndex GlobalProxySelection_t1404_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType GlobalProxySelection_t1404_0_0_0;
extern const Il2CppType GlobalProxySelection_t1404_1_0_0;
struct GlobalProxySelection_t1404;
const Il2CppTypeDefinitionMetadata GlobalProxySelection_t1404_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GlobalProxySelection_t1404_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7244/* methodStart */
	, -1/* eventStart */
	, 1474/* propertyStart */

};
TypeInfo GlobalProxySelection_t1404_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "GlobalProxySelection"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &GlobalProxySelection_t1404_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2366/* custom_attributes_cache */
	, &GlobalProxySelection_t1404_0_0_0/* byval_arg */
	, &GlobalProxySelection_t1404_1_0_0/* this_arg */
	, &GlobalProxySelection_t1404_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GlobalProxySelection_t1404)/* instance_size */
	, sizeof (GlobalProxySelection_t1404)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.HttpRequestCreator
#include "System_System_Net_HttpRequestCreator.h"
// Metadata Definition System.Net.HttpRequestCreator
extern TypeInfo HttpRequestCreator_t1405_il2cpp_TypeInfo;
// System.Net.HttpRequestCreator
#include "System_System_Net_HttpRequestCreatorMethodDeclarations.h"
static const EncodedMethodIndex HttpRequestCreator_t1405_VTable[5] = 
{
	626,
	601,
	627,
	628,
	2026,
};
static const Il2CppType* HttpRequestCreator_t1405_InterfacesTypeInfos[] = 
{
	&IWebRequestCreate_t3428_0_0_0,
};
static Il2CppInterfaceOffsetPair HttpRequestCreator_t1405_InterfacesOffsets[] = 
{
	{ &IWebRequestCreate_t3428_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType HttpRequestCreator_t1405_0_0_0;
extern const Il2CppType HttpRequestCreator_t1405_1_0_0;
struct HttpRequestCreator_t1405;
const Il2CppTypeDefinitionMetadata HttpRequestCreator_t1405_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, HttpRequestCreator_t1405_InterfacesTypeInfos/* implementedInterfaces */
	, HttpRequestCreator_t1405_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, HttpRequestCreator_t1405_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7245/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HttpRequestCreator_t1405_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "HttpRequestCreator"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &HttpRequestCreator_t1405_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HttpRequestCreator_t1405_0_0_0/* byval_arg */
	, &HttpRequestCreator_t1405_1_0_0/* this_arg */
	, &HttpRequestCreator_t1405_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HttpRequestCreator_t1405)/* instance_size */
	, sizeof (HttpRequestCreator_t1405)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Net.HttpVersion
#include "System_System_Net_HttpVersion.h"
// Metadata Definition System.Net.HttpVersion
extern TypeInfo HttpVersion_t1406_il2cpp_TypeInfo;
// System.Net.HttpVersion
#include "System_System_Net_HttpVersionMethodDeclarations.h"
static const EncodedMethodIndex HttpVersion_t1406_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType HttpVersion_t1406_0_0_0;
extern const Il2CppType HttpVersion_t1406_1_0_0;
struct HttpVersion_t1406;
const Il2CppTypeDefinitionMetadata HttpVersion_t1406_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, HttpVersion_t1406_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5689/* fieldStart */
	, 7247/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HttpVersion_t1406_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "HttpVersion"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &HttpVersion_t1406_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HttpVersion_t1406_0_0_0/* byval_arg */
	, &HttpVersion_t1406_1_0_0/* this_arg */
	, &HttpVersion_t1406_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HttpVersion_t1406)/* instance_size */
	, sizeof (HttpVersion_t1406)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(HttpVersion_t1406_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.HttpWebRequest
#include "System_System_Net_HttpWebRequest.h"
// Metadata Definition System.Net.HttpWebRequest
extern TypeInfo HttpWebRequest_t1279_il2cpp_TypeInfo;
// System.Net.HttpWebRequest
#include "System_System_Net_HttpWebRequestMethodDeclarations.h"
static const EncodedMethodIndex HttpWebRequest_t1279_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2027,
	2028,
};
static const Il2CppType* HttpWebRequest_t1279_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair HttpWebRequest_t1279_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType HttpWebRequest_t1279_0_0_0;
extern const Il2CppType HttpWebRequest_t1279_1_0_0;
struct HttpWebRequest_t1279;
const Il2CppTypeDefinitionMetadata HttpWebRequest_t1279_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, HttpWebRequest_t1279_InterfacesTypeInfos/* implementedInterfaces */
	, HttpWebRequest_t1279_InterfacesOffsets/* interfaceOffsets */
	, &WebRequest_t1364_0_0_0/* parent */
	, HttpWebRequest_t1279_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5691/* fieldStart */
	, 7248/* methodStart */
	, -1/* eventStart */
	, 1475/* propertyStart */

};
TypeInfo HttpWebRequest_t1279_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "HttpWebRequest"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &HttpWebRequest_t1279_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HttpWebRequest_t1279_0_0_0/* byval_arg */
	, &HttpWebRequest_t1279_1_0_0/* this_arg */
	, &HttpWebRequest_t1279_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HttpWebRequest_t1279)/* instance_size */
	, sizeof (HttpWebRequest_t1279)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(HttpWebRequest_t1279_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8193/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 2/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Net.ICertificatePolicy
extern TypeInfo ICertificatePolicy_t1365_il2cpp_TypeInfo;
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ICertificatePolicy_t1365_1_0_0;
struct ICertificatePolicy_t1365;
const Il2CppTypeDefinitionMetadata ICertificatePolicy_t1365_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7256/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ICertificatePolicy_t1365_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICertificatePolicy"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &ICertificatePolicy_t1365_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ICertificatePolicy_t1365_0_0_0/* byval_arg */
	, &ICertificatePolicy_t1365_1_0_0/* this_arg */
	, &ICertificatePolicy_t1365_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Net.ICredentials
extern TypeInfo ICredentials_t1413_il2cpp_TypeInfo;
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ICredentials_t1413_0_0_0;
extern const Il2CppType ICredentials_t1413_1_0_0;
struct ICredentials_t1413;
const Il2CppTypeDefinitionMetadata ICredentials_t1413_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ICredentials_t1413_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICredentials"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &ICredentials_t1413_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ICredentials_t1413_0_0_0/* byval_arg */
	, &ICredentials_t1413_1_0_0/* this_arg */
	, &ICredentials_t1413_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.IPAddress
#include "System_System_Net_IPAddress.h"
// Metadata Definition System.Net.IPAddress
extern TypeInfo IPAddress_t1408_il2cpp_TypeInfo;
// System.Net.IPAddress
#include "System_System_Net_IPAddressMethodDeclarations.h"
static const EncodedMethodIndex IPAddress_t1408_VTable[4] = 
{
	2029,
	601,
	2030,
	2031,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IPAddress_t1408_0_0_0;
extern const Il2CppType IPAddress_t1408_1_0_0;
struct IPAddress_t1408;
const Il2CppTypeDefinitionMetadata IPAddress_t1408_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IPAddress_t1408_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5715/* fieldStart */
	, 7257/* methodStart */
	, -1/* eventStart */
	, 1477/* propertyStart */

};
TypeInfo IPAddress_t1408_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IPAddress"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &IPAddress_t1408_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IPAddress_t1408_0_0_0/* byval_arg */
	, &IPAddress_t1408_1_0_0/* this_arg */
	, &IPAddress_t1408_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IPAddress_t1408)/* instance_size */
	, sizeof (IPAddress_t1408)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(IPAddress_t1408_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 3/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.IPv6Address
#include "System_System_Net_IPv6Address.h"
// Metadata Definition System.Net.IPv6Address
extern TypeInfo IPv6Address_t1409_il2cpp_TypeInfo;
// System.Net.IPv6Address
#include "System_System_Net_IPv6AddressMethodDeclarations.h"
static const EncodedMethodIndex IPv6Address_t1409_VTable[4] = 
{
	2032,
	601,
	2033,
	2034,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IPv6Address_t1409_0_0_0;
extern const Il2CppType IPv6Address_t1409_1_0_0;
struct IPv6Address_t1409;
const Il2CppTypeDefinitionMetadata IPv6Address_t1409_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IPv6Address_t1409_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5726/* fieldStart */
	, 7276/* methodStart */
	, -1/* eventStart */
	, 1480/* propertyStart */

};
TypeInfo IPv6Address_t1409_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IPv6Address"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &IPv6Address_t1409_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2368/* custom_attributes_cache */
	, &IPv6Address_t1409_0_0_0/* byval_arg */
	, &IPv6Address_t1409_1_0_0/* this_arg */
	, &IPv6Address_t1409_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IPv6Address_t1409)/* instance_size */
	, sizeof (IPv6Address_t1409)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(IPv6Address_t1409_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Net.IWebProxy
extern TypeInfo IWebProxy_t1399_il2cpp_TypeInfo;
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IWebProxy_t1399_0_0_0;
extern const Il2CppType IWebProxy_t1399_1_0_0;
struct IWebProxy_t1399;
const Il2CppTypeDefinitionMetadata IWebProxy_t1399_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7297/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IWebProxy_t1399_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IWebProxy"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &IWebProxy_t1399_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IWebProxy_t1399_0_0_0/* byval_arg */
	, &IWebProxy_t1399_1_0_0/* this_arg */
	, &IWebProxy_t1399_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Net.IWebRequestCreate
extern TypeInfo IWebRequestCreate_t3428_il2cpp_TypeInfo;
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IWebRequestCreate_t3428_1_0_0;
struct IWebRequestCreate_t3428;
const Il2CppTypeDefinitionMetadata IWebRequestCreate_t3428_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IWebRequestCreate_t3428_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IWebRequestCreate"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &IWebRequestCreate_t3428_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IWebRequestCreate_t3428_0_0_0/* byval_arg */
	, &IWebRequestCreate_t3428_1_0_0/* this_arg */
	, &IWebRequestCreate_t3428_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.SecurityProtocolType
#include "System_System_Net_SecurityProtocolType.h"
// Metadata Definition System.Net.SecurityProtocolType
extern TypeInfo SecurityProtocolType_t1410_il2cpp_TypeInfo;
// System.Net.SecurityProtocolType
#include "System_System_Net_SecurityProtocolTypeMethodDeclarations.h"
static const EncodedMethodIndex SecurityProtocolType_t1410_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair SecurityProtocolType_t1410_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType SecurityProtocolType_t1410_0_0_0;
extern const Il2CppType SecurityProtocolType_t1410_1_0_0;
const Il2CppTypeDefinitionMetadata SecurityProtocolType_t1410_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SecurityProtocolType_t1410_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, SecurityProtocolType_t1410_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5731/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SecurityProtocolType_t1410_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityProtocolType"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2369/* custom_attributes_cache */
	, &SecurityProtocolType_t1410_0_0_0/* byval_arg */
	, &SecurityProtocolType_t1410_1_0_0/* this_arg */
	, &SecurityProtocolType_t1410_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityProtocolType_t1410)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SecurityProtocolType_t1410)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Net.ServicePoint
#include "System_System_Net_ServicePoint.h"
// Metadata Definition System.Net.ServicePoint
extern TypeInfo ServicePoint_t1360_il2cpp_TypeInfo;
// System.Net.ServicePoint
#include "System_System_Net_ServicePointMethodDeclarations.h"
static const EncodedMethodIndex ServicePoint_t1360_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ServicePoint_t1360_0_0_0;
extern const Il2CppType ServicePoint_t1360_1_0_0;
struct ServicePoint_t1360;
const Il2CppTypeDefinitionMetadata ServicePoint_t1360_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ServicePoint_t1360_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5734/* fieldStart */
	, 7299/* methodStart */
	, -1/* eventStart */
	, 1482/* propertyStart */

};
TypeInfo ServicePoint_t1360_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ServicePoint"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &ServicePoint_t1360_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ServicePoint_t1360_0_0_0/* byval_arg */
	, &ServicePoint_t1360_1_0_0/* this_arg */
	, &ServicePoint_t1360_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ServicePoint_t1360)/* instance_size */
	, sizeof (ServicePoint_t1360)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 9/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.ServicePointManager
#include "System_System_Net_ServicePointManager.h"
// Metadata Definition System.Net.ServicePointManager
extern TypeInfo ServicePointManager_t1349_il2cpp_TypeInfo;
// System.Net.ServicePointManager
#include "System_System_Net_ServicePointManagerMethodDeclarations.h"
extern const Il2CppType SPKey_t1411_0_0_0;
static const Il2CppType* ServicePointManager_t1349_il2cpp_TypeInfo__nestedTypes[1] =
{
	&SPKey_t1411_0_0_0,
};
static const EncodedMethodIndex ServicePointManager_t1349_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType ServicePointManager_t1349_0_0_0;
extern const Il2CppType ServicePointManager_t1349_1_0_0;
struct ServicePointManager_t1349;
const Il2CppTypeDefinitionMetadata ServicePointManager_t1349_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ServicePointManager_t1349_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ServicePointManager_t1349_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5745/* fieldStart */
	, 7310/* methodStart */
	, -1/* eventStart */
	, 1491/* propertyStart */

};
TypeInfo ServicePointManager_t1349_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "ServicePointManager"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &ServicePointManager_t1349_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ServicePointManager_t1349_0_0_0/* byval_arg */
	, &ServicePointManager_t1349_1_0_0/* this_arg */
	, &ServicePointManager_t1349_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ServicePointManager_t1349)/* instance_size */
	, sizeof (ServicePointManager_t1349)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ServicePointManager_t1349_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 4/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.ServicePointManager/SPKey
#include "System_System_Net_ServicePointManager_SPKey.h"
// Metadata Definition System.Net.ServicePointManager/SPKey
extern TypeInfo SPKey_t1411_il2cpp_TypeInfo;
// System.Net.ServicePointManager/SPKey
#include "System_System_Net_ServicePointManager_SPKeyMethodDeclarations.h"
static const EncodedMethodIndex SPKey_t1411_VTable[4] = 
{
	2035,
	601,
	2036,
	628,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType SPKey_t1411_1_0_0;
struct SPKey_t1411;
const Il2CppTypeDefinitionMetadata SPKey_t1411_DefinitionMetadata = 
{
	&ServicePointManager_t1349_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SPKey_t1411_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5755/* fieldStart */
	, 7317/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SPKey_t1411_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "SPKey"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SPKey_t1411_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SPKey_t1411_0_0_0/* byval_arg */
	, &SPKey_t1411_1_0_0/* this_arg */
	, &SPKey_t1411_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SPKey_t1411)/* instance_size */
	, sizeof (SPKey_t1411)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Net.WebHeaderCollection
#include "System_System_Net_WebHeaderCollection.h"
// Metadata Definition System.Net.WebHeaderCollection
extern TypeInfo WebHeaderCollection_t1398_il2cpp_TypeInfo;
// System.Net.WebHeaderCollection
#include "System_System_Net_WebHeaderCollectionMethodDeclarations.h"
static const EncodedMethodIndex WebHeaderCollection_t1398_VTable[19] = 
{
	626,
	601,
	627,
	2037,
	2038,
	1994,
	1995,
	1996,
	2039,
	2040,
	2041,
	2042,
	2040,
	2043,
	2038,
	2039,
	2044,
	2045,
	2046,
};
static const Il2CppType* WebHeaderCollection_t1398_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair WebHeaderCollection_t1398_InterfacesOffsets[] = 
{
	{ &ICollection_t1528_0_0_0, 4},
	{ &IDeserializationCallback_t2213_0_0_0, 8},
	{ &IEnumerable_t1097_0_0_0, 9},
	{ &ISerializable_t2210_0_0_0, 10},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType WebHeaderCollection_t1398_0_0_0;
extern const Il2CppType WebHeaderCollection_t1398_1_0_0;
struct WebHeaderCollection_t1398;
const Il2CppTypeDefinitionMetadata WebHeaderCollection_t1398_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WebHeaderCollection_t1398_InterfacesTypeInfos/* implementedInterfaces */
	, WebHeaderCollection_t1398_InterfacesOffsets/* interfaceOffsets */
	, &NameValueCollection_t1388_0_0_0/* parent */
	, WebHeaderCollection_t1398_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5757/* fieldStart */
	, 7320/* methodStart */
	, -1/* eventStart */
	, 1495/* propertyStart */

};
TypeInfo WebHeaderCollection_t1398_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebHeaderCollection"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &WebHeaderCollection_t1398_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2372/* custom_attributes_cache */
	, &WebHeaderCollection_t1398_0_0_0/* byval_arg */
	, &WebHeaderCollection_t1398_1_0_0/* this_arg */
	, &WebHeaderCollection_t1398_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WebHeaderCollection_t1398)/* instance_size */
	, sizeof (WebHeaderCollection_t1398)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WebHeaderCollection_t1398_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8193/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Net.WebProxy
#include "System_System_Net_WebProxy.h"
// Metadata Definition System.Net.WebProxy
extern TypeInfo WebProxy_t1414_il2cpp_TypeInfo;
// System.Net.WebProxy
#include "System_System_Net_WebProxyMethodDeclarations.h"
static const EncodedMethodIndex WebProxy_t1414_VTable[8] = 
{
	626,
	601,
	627,
	628,
	2047,
	2048,
	2049,
	2050,
};
static const Il2CppType* WebProxy_t1414_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
	&IWebProxy_t1399_0_0_0,
};
static Il2CppInterfaceOffsetPair WebProxy_t1414_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &IWebProxy_t1399_0_0_0, 5},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType WebProxy_t1414_0_0_0;
extern const Il2CppType WebProxy_t1414_1_0_0;
struct WebProxy_t1414;
const Il2CppTypeDefinitionMetadata WebProxy_t1414_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WebProxy_t1414_InterfacesTypeInfos/* implementedInterfaces */
	, WebProxy_t1414_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WebProxy_t1414_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5762/* fieldStart */
	, 7338/* methodStart */
	, -1/* eventStart */
	, 1497/* propertyStart */

};
TypeInfo WebProxy_t1414_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebProxy"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &WebProxy_t1414_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WebProxy_t1414_0_0_0/* byval_arg */
	, &WebProxy_t1414_1_0_0/* this_arg */
	, &WebProxy_t1414_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WebProxy_t1414)/* instance_size */
	, sizeof (WebProxy_t1414)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Net.WebRequest
#include "System_System_Net_WebRequest.h"
// Metadata Definition System.Net.WebRequest
extern TypeInfo WebRequest_t1364_il2cpp_TypeInfo;
// System.Net.WebRequest
#include "System_System_Net_WebRequestMethodDeclarations.h"
static const EncodedMethodIndex WebRequest_t1364_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2024,
	2025,
};
static const Il2CppType* WebRequest_t1364_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair WebRequest_t1364_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType WebRequest_t1364_1_0_0;
extern const Il2CppType MarshalByRefObject_t1415_0_0_0;
struct WebRequest_t1364;
const Il2CppTypeDefinitionMetadata WebRequest_t1364_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WebRequest_t1364_InterfacesTypeInfos/* implementedInterfaces */
	, WebRequest_t1364_InterfacesOffsets/* interfaceOffsets */
	, &MarshalByRefObject_t1415_0_0_0/* parent */
	, WebRequest_t1364_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5767/* fieldStart */
	, 7347/* methodStart */
	, -1/* eventStart */
	, 1498/* propertyStart */

};
TypeInfo WebRequest_t1364_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebRequest"/* name */
	, "System.Net"/* namespaze */
	, NULL/* methods */
	, &WebRequest_t1364_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WebRequest_t1364_0_0_0/* byval_arg */
	, &WebRequest_t1364_1_0_0/* this_arg */
	, &WebRequest_t1364_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WebRequest_t1364)/* instance_size */
	, sizeof (WebRequest_t1364)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WebRequest_t1364_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8321/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.OpenFlags
#include "System_System_Security_Cryptography_X509Certificates_OpenFla.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.OpenFlags
extern TypeInfo OpenFlags_t1416_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.OpenFlags
#include "System_System_Security_Cryptography_X509Certificates_OpenFlaMethodDeclarations.h"
static const EncodedMethodIndex OpenFlags_t1416_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair OpenFlags_t1416_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType OpenFlags_t1416_0_0_0;
extern const Il2CppType OpenFlags_t1416_1_0_0;
const Il2CppTypeDefinitionMetadata OpenFlags_t1416_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OpenFlags_t1416_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, OpenFlags_t1416_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5772/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OpenFlags_t1416_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "OpenFlags"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2375/* custom_attributes_cache */
	, &OpenFlags_t1416_0_0_0/* byval_arg */
	, &OpenFlags_t1416_1_0_0/* this_arg */
	, &OpenFlags_t1416_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OpenFlags_t1416)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (OpenFlags_t1416)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.PublicKey
#include "System_System_Security_Cryptography_X509Certificates_PublicK.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.PublicKey
extern TypeInfo PublicKey_t1419_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.PublicKey
#include "System_System_Security_Cryptography_X509Certificates_PublicKMethodDeclarations.h"
static const EncodedMethodIndex PublicKey_t1419_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType PublicKey_t1419_0_0_0;
extern const Il2CppType PublicKey_t1419_1_0_0;
struct PublicKey_t1419;
const Il2CppTypeDefinitionMetadata PublicKey_t1419_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PublicKey_t1419_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5778/* fieldStart */
	, 7357/* methodStart */
	, -1/* eventStart */
	, 1499/* propertyStart */

};
TypeInfo PublicKey_t1419_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "PublicKey"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &PublicKey_t1419_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PublicKey_t1419_0_0_0/* byval_arg */
	, &PublicKey_t1419_1_0_0/* this_arg */
	, &PublicKey_t1419_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PublicKey_t1419)/* instance_size */
	, sizeof (PublicKey_t1419)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PublicKey_t1419_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.StoreLocation
#include "System_System_Security_Cryptography_X509Certificates_StoreLo.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.StoreLocation
extern TypeInfo StoreLocation_t1420_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.StoreLocation
#include "System_System_Security_Cryptography_X509Certificates_StoreLoMethodDeclarations.h"
static const EncodedMethodIndex StoreLocation_t1420_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair StoreLocation_t1420_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType StoreLocation_t1420_0_0_0;
extern const Il2CppType StoreLocation_t1420_1_0_0;
const Il2CppTypeDefinitionMetadata StoreLocation_t1420_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StoreLocation_t1420_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, StoreLocation_t1420_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5783/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StoreLocation_t1420_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "StoreLocation"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StoreLocation_t1420_0_0_0/* byval_arg */
	, &StoreLocation_t1420_1_0_0/* this_arg */
	, &StoreLocation_t1420_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StoreLocation_t1420)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StoreLocation_t1420)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.StoreName
#include "System_System_Security_Cryptography_X509Certificates_StoreNa.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.StoreName
extern TypeInfo StoreName_t1421_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.StoreName
#include "System_System_Security_Cryptography_X509Certificates_StoreNaMethodDeclarations.h"
static const EncodedMethodIndex StoreName_t1421_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair StoreName_t1421_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType StoreName_t1421_0_0_0;
extern const Il2CppType StoreName_t1421_1_0_0;
const Il2CppTypeDefinitionMetadata StoreName_t1421_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StoreName_t1421_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, StoreName_t1421_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5786/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StoreName_t1421_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "StoreName"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StoreName_t1421_0_0_0/* byval_arg */
	, &StoreName_t1421_1_0_0/* this_arg */
	, &StoreName_t1421_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StoreName_t1421)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StoreName_t1421)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X500DistinguishedName
#include "System_System_Security_Cryptography_X509Certificates_X500Dis.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X500DistinguishedName
extern TypeInfo X500DistinguishedName_t1422_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X500DistinguishedName
#include "System_System_Security_Cryptography_X509Certificates_X500DisMethodDeclarations.h"
static const EncodedMethodIndex X500DistinguishedName_t1422_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2051,
	2052,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X500DistinguishedName_t1422_0_0_0;
extern const Il2CppType X500DistinguishedName_t1422_1_0_0;
extern const Il2CppType AsnEncodedData_t1417_0_0_0;
struct X500DistinguishedName_t1422;
const Il2CppTypeDefinitionMetadata X500DistinguishedName_t1422_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsnEncodedData_t1417_0_0_0/* parent */
	, X500DistinguishedName_t1422_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5795/* fieldStart */
	, 7365/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X500DistinguishedName_t1422_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X500DistinguishedName"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X500DistinguishedName_t1422_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2377/* custom_attributes_cache */
	, &X500DistinguishedName_t1422_0_0_0/* byval_arg */
	, &X500DistinguishedName_t1422_1_0_0/* this_arg */
	, &X500DistinguishedName_t1422_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X500DistinguishedName_t1422)/* instance_size */
	, sizeof (X500DistinguishedName_t1422)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags
#include "System_System_Security_Cryptography_X509Certificates_X500Dis_0.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags
extern TypeInfo X500DistinguishedNameFlags_t1423_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags
#include "System_System_Security_Cryptography_X509Certificates_X500Dis_0MethodDeclarations.h"
static const EncodedMethodIndex X500DistinguishedNameFlags_t1423_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair X500DistinguishedNameFlags_t1423_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X500DistinguishedNameFlags_t1423_0_0_0;
extern const Il2CppType X500DistinguishedNameFlags_t1423_1_0_0;
const Il2CppTypeDefinitionMetadata X500DistinguishedNameFlags_t1423_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X500DistinguishedNameFlags_t1423_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, X500DistinguishedNameFlags_t1423_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5796/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X500DistinguishedNameFlags_t1423_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X500DistinguishedNameFlags"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2378/* custom_attributes_cache */
	, &X500DistinguishedNameFlags_t1423_0_0_0/* byval_arg */
	, &X500DistinguishedNameFlags_t1423_1_0_0/* this_arg */
	, &X500DistinguishedNameFlags_t1423_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X500DistinguishedNameFlags_t1423)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X500DistinguishedNameFlags_t1423)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension
#include "System_System_Security_Cryptography_X509Certificates_X509Bas.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension
extern TypeInfo X509BasicConstraintsExtension_t1424_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension
#include "System_System_Security_Cryptography_X509Certificates_X509BasMethodDeclarations.h"
static const EncodedMethodIndex X509BasicConstraintsExtension_t1424_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2053,
	2054,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509BasicConstraintsExtension_t1424_0_0_0;
extern const Il2CppType X509BasicConstraintsExtension_t1424_1_0_0;
extern const Il2CppType X509Extension_t1425_0_0_0;
struct X509BasicConstraintsExtension_t1424;
const Il2CppTypeDefinitionMetadata X509BasicConstraintsExtension_t1424_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &X509Extension_t1425_0_0_0/* parent */
	, X509BasicConstraintsExtension_t1424_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5807/* fieldStart */
	, 7371/* methodStart */
	, -1/* eventStart */
	, 1503/* propertyStart */

};
TypeInfo X509BasicConstraintsExtension_t1424_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509BasicConstraintsExtension"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509BasicConstraintsExtension_t1424_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509BasicConstraintsExtension_t1424_0_0_0/* byval_arg */
	, &X509BasicConstraintsExtension_t1424_1_0_0/* this_arg */
	, &X509BasicConstraintsExtension_t1424_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509BasicConstraintsExtension_t1424)/* instance_size */
	, sizeof (X509BasicConstraintsExtension_t1424)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 3/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509Certificate2
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_0.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509Certificate2
extern TypeInfo X509Certificate2_t1362_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509Certificate2
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_0MethodDeclarations.h"
static const EncodedMethodIndex X509Certificate2_t1362_VTable[18] = 
{
	2055,
	601,
	2056,
	2057,
	2058,
	2059,
	2060,
	2061,
	2062,
	2063,
	2064,
	2065,
	2066,
	2067,
	2068,
	2069,
	2070,
	2071,
};
static Il2CppInterfaceOffsetPair X509Certificate2_t1362_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &IDeserializationCallback_t2213_0_0_0, 5},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509Certificate2_t1362_0_0_0;
extern const Il2CppType X509Certificate2_t1362_1_0_0;
extern const Il2CppType X509Certificate_t1304_0_0_0;
struct X509Certificate2_t1362;
const Il2CppTypeDefinitionMetadata X509Certificate2_t1362_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509Certificate2_t1362_InterfacesOffsets/* interfaceOffsets */
	, &X509Certificate_t1304_0_0_0/* parent */
	, X509Certificate2_t1362_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5813/* fieldStart */
	, 7381/* methodStart */
	, -1/* eventStart */
	, 1506/* propertyStart */

};
TypeInfo X509Certificate2_t1362_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Certificate2"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509Certificate2_t1362_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Certificate2_t1362_0_0_0/* byval_arg */
	, &X509Certificate2_t1362_1_0_0/* this_arg */
	, &X509Certificate2_t1362_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Certificate2_t1362)/* instance_size */
	, sizeof (X509Certificate2_t1362)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(X509Certificate2_t1362_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 12/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_2.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509Certificate2Collection
extern TypeInfo X509Certificate2Collection_t1427_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509Certificate2Collection
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_2MethodDeclarations.h"
static const EncodedMethodIndex X509Certificate2Collection_t1427_VTable[29] = 
{
	626,
	601,
	2072,
	628,
	2073,
	1735,
	1736,
	1737,
	1738,
	1739,
	1740,
	1741,
	1742,
	1743,
	1744,
	1745,
	1746,
	1747,
	1748,
	1749,
	1750,
	1751,
	1752,
	1753,
	1754,
	1755,
	1756,
	1757,
	1758,
};
extern const Il2CppType IList_t1487_0_0_0;
static Il2CppInterfaceOffsetPair X509Certificate2Collection_t1427_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509Certificate2Collection_t1427_0_0_0;
extern const Il2CppType X509Certificate2Collection_t1427_1_0_0;
extern const Il2CppType X509CertificateCollection_t1303_0_0_0;
struct X509Certificate2Collection_t1427;
const Il2CppTypeDefinitionMetadata X509Certificate2Collection_t1427_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509Certificate2Collection_t1427_InterfacesOffsets/* interfaceOffsets */
	, &X509CertificateCollection_t1303_0_0_0/* parent */
	, X509Certificate2Collection_t1427_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7405/* methodStart */
	, -1/* eventStart */
	, 1518/* propertyStart */

};
TypeInfo X509Certificate2Collection_t1427_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Certificate2Collection"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509Certificate2Collection_t1427_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2382/* custom_attributes_cache */
	, &X509Certificate2Collection_t1427_0_0_0/* byval_arg */
	, &X509Certificate2Collection_t1427_1_0_0/* this_arg */
	, &X509Certificate2Collection_t1427_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Certificate2Collection_t1427)/* instance_size */
	, sizeof (X509Certificate2Collection_t1427)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 29/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_3.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator
extern TypeInfo X509Certificate2Enumerator_t1428_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509Certificate2Enumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_3MethodDeclarations.h"
static const EncodedMethodIndex X509Certificate2Enumerator_t1428_VTable[7] = 
{
	626,
	601,
	627,
	628,
	2074,
	2075,
	2076,
};
static const Il2CppType* X509Certificate2Enumerator_t1428_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
};
static Il2CppInterfaceOffsetPair X509Certificate2Enumerator_t1428_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509Certificate2Enumerator_t1428_0_0_0;
extern const Il2CppType X509Certificate2Enumerator_t1428_1_0_0;
struct X509Certificate2Enumerator_t1428;
const Il2CppTypeDefinitionMetadata X509Certificate2Enumerator_t1428_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509Certificate2Enumerator_t1428_InterfacesTypeInfos/* implementedInterfaces */
	, X509Certificate2Enumerator_t1428_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Certificate2Enumerator_t1428_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5826/* fieldStart */
	, 7413/* methodStart */
	, -1/* eventStart */
	, 1519/* propertyStart */

};
TypeInfo X509Certificate2Enumerator_t1428_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Certificate2Enumerator"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509Certificate2Enumerator_t1428_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Certificate2Enumerator_t1428_0_0_0/* byval_arg */
	, &X509Certificate2Enumerator_t1428_1_0_0/* this_arg */
	, &X509Certificate2Enumerator_t1428_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Certificate2Enumerator_t1428)/* instance_size */
	, sizeof (X509Certificate2Enumerator_t1428)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
#include "System_System_Security_Cryptography_X509Certificates_X509Cer.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509CertificateCollection
extern TypeInfo X509CertificateCollection_t1303_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
#include "System_System_Security_Cryptography_X509Certificates_X509CerMethodDeclarations.h"
extern const Il2CppType X509CertificateEnumerator_t1368_0_0_0;
static const Il2CppType* X509CertificateCollection_t1303_il2cpp_TypeInfo__nestedTypes[1] =
{
	&X509CertificateEnumerator_t1368_0_0_0,
};
static const EncodedMethodIndex X509CertificateCollection_t1303_VTable[29] = 
{
	626,
	601,
	2072,
	628,
	2073,
	1735,
	1736,
	1737,
	1738,
	1739,
	1740,
	1741,
	1742,
	1743,
	1744,
	1745,
	1746,
	1747,
	1748,
	1749,
	1750,
	1751,
	1752,
	1753,
	1754,
	1755,
	1756,
	1757,
	1758,
};
static Il2CppInterfaceOffsetPair X509CertificateCollection_t1303_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509CertificateCollection_t1303_1_0_0;
extern const Il2CppType CollectionBase_t1236_0_0_0;
struct X509CertificateCollection_t1303;
const Il2CppTypeDefinitionMetadata X509CertificateCollection_t1303_DefinitionMetadata = 
{
	NULL/* declaringType */
	, X509CertificateCollection_t1303_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509CertificateCollection_t1303_InterfacesOffsets/* interfaceOffsets */
	, &CollectionBase_t1236_0_0_0/* parent */
	, X509CertificateCollection_t1303_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7418/* methodStart */
	, -1/* eventStart */
	, 1521/* propertyStart */

};
TypeInfo X509CertificateCollection_t1303_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509CertificateCollection"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509CertificateCollection_t1303_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2385/* custom_attributes_cache */
	, &X509CertificateCollection_t1303_0_0_0/* byval_arg */
	, &X509CertificateCollection_t1303_1_0_0/* this_arg */
	, &X509CertificateCollection_t1303_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509CertificateCollection_t1303)/* instance_size */
	, sizeof (X509CertificateCollection_t1303)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 29/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_1.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator
extern TypeInfo X509CertificateEnumerator_t1368_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection/X509CertificateEnumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Cer_1MethodDeclarations.h"
static const EncodedMethodIndex X509CertificateEnumerator_t1368_VTable[7] = 
{
	626,
	601,
	627,
	628,
	2077,
	2078,
	2079,
};
static const Il2CppType* X509CertificateEnumerator_t1368_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
};
static Il2CppInterfaceOffsetPair X509CertificateEnumerator_t1368_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509CertificateEnumerator_t1368_1_0_0;
struct X509CertificateEnumerator_t1368;
const Il2CppTypeDefinitionMetadata X509CertificateEnumerator_t1368_DefinitionMetadata = 
{
	&X509CertificateCollection_t1303_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, X509CertificateEnumerator_t1368_InterfacesTypeInfos/* implementedInterfaces */
	, X509CertificateEnumerator_t1368_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509CertificateEnumerator_t1368_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5827/* fieldStart */
	, 7424/* methodStart */
	, -1/* eventStart */
	, 1522/* propertyStart */

};
TypeInfo X509CertificateEnumerator_t1368_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509CertificateEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &X509CertificateEnumerator_t1368_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509CertificateEnumerator_t1368_0_0_0/* byval_arg */
	, &X509CertificateEnumerator_t1368_1_0_0/* this_arg */
	, &X509CertificateEnumerator_t1368_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509CertificateEnumerator_t1368)/* instance_size */
	, sizeof (X509CertificateEnumerator_t1368)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509Chain
#include "System_System_Security_Cryptography_X509Certificates_X509Cha.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509Chain
extern TypeInfo X509Chain_t1363_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509Chain
#include "System_System_Security_Cryptography_X509Certificates_X509ChaMethodDeclarations.h"
static const EncodedMethodIndex X509Chain_t1363_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509Chain_t1363_0_0_0;
extern const Il2CppType X509Chain_t1363_1_0_0;
struct X509Chain_t1363;
const Il2CppTypeDefinitionMetadata X509Chain_t1363_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Chain_t1363_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5828/* fieldStart */
	, 7429/* methodStart */
	, -1/* eventStart */
	, 1524/* propertyStart */

};
TypeInfo X509Chain_t1363_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Chain"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509Chain_t1363_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Chain_t1363_0_0_0/* byval_arg */
	, &X509Chain_t1363_1_0_0/* this_arg */
	, &X509Chain_t1363_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Chain_t1363)/* instance_size */
	, sizeof (X509Chain_t1363)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(X509Chain_t1363_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 4/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509ChainElement
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_0.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509ChainElement
extern TypeInfo X509ChainElement_t1433_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509ChainElement
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_0MethodDeclarations.h"
static const EncodedMethodIndex X509ChainElement_t1433_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509ChainElement_t1433_0_0_0;
extern const Il2CppType X509ChainElement_t1433_1_0_0;
struct X509ChainElement_t1433;
const Il2CppTypeDefinitionMetadata X509ChainElement_t1433_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509ChainElement_t1433_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5843/* fieldStart */
	, 7459/* methodStart */
	, -1/* eventStart */
	, 1528/* propertyStart */

};
TypeInfo X509ChainElement_t1433_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ChainElement"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509ChainElement_t1433_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509ChainElement_t1433_0_0_0/* byval_arg */
	, &X509ChainElement_t1433_1_0_0/* this_arg */
	, &X509ChainElement_t1433_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ChainElement_t1433)/* instance_size */
	, sizeof (X509ChainElement_t1433)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509ChainElementCollection
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_2.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509ChainElementCollection
extern TypeInfo X509ChainElementCollection_t1429_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509ChainElementCollection
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_2MethodDeclarations.h"
static const EncodedMethodIndex X509ChainElementCollection_t1429_VTable[9] = 
{
	626,
	601,
	627,
	628,
	2080,
	2081,
	2082,
	2083,
	2084,
};
static const Il2CppType* X509ChainElementCollection_t1429_InterfacesTypeInfos[] = 
{
	&ICollection_t1528_0_0_0,
	&IEnumerable_t1097_0_0_0,
};
static Il2CppInterfaceOffsetPair X509ChainElementCollection_t1429_InterfacesOffsets[] = 
{
	{ &ICollection_t1528_0_0_0, 4},
	{ &IEnumerable_t1097_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509ChainElementCollection_t1429_0_0_0;
extern const Il2CppType X509ChainElementCollection_t1429_1_0_0;
struct X509ChainElementCollection_t1429;
const Il2CppTypeDefinitionMetadata X509ChainElementCollection_t1429_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509ChainElementCollection_t1429_InterfacesTypeInfos/* implementedInterfaces */
	, X509ChainElementCollection_t1429_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509ChainElementCollection_t1429_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5847/* fieldStart */
	, 7467/* methodStart */
	, -1/* eventStart */
	, 1531/* propertyStart */

};
TypeInfo X509ChainElementCollection_t1429_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ChainElementCollection"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509ChainElementCollection_t1429_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2390/* custom_attributes_cache */
	, &X509ChainElementCollection_t1429_0_0_0/* byval_arg */
	, &X509ChainElementCollection_t1429_1_0_0/* this_arg */
	, &X509ChainElementCollection_t1429_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ChainElementCollection_t1429)/* instance_size */
	, sizeof (X509ChainElementCollection_t1429)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 4/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_3.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator
extern TypeInfo X509ChainElementEnumerator_t1435_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_3MethodDeclarations.h"
static const EncodedMethodIndex X509ChainElementEnumerator_t1435_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2085,
	2086,
};
static const Il2CppType* X509ChainElementEnumerator_t1435_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
};
static Il2CppInterfaceOffsetPair X509ChainElementEnumerator_t1435_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509ChainElementEnumerator_t1435_0_0_0;
extern const Il2CppType X509ChainElementEnumerator_t1435_1_0_0;
struct X509ChainElementEnumerator_t1435;
const Il2CppTypeDefinitionMetadata X509ChainElementEnumerator_t1435_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509ChainElementEnumerator_t1435_InterfacesTypeInfos/* implementedInterfaces */
	, X509ChainElementEnumerator_t1435_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509ChainElementEnumerator_t1435_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5848/* fieldStart */
	, 7478/* methodStart */
	, -1/* eventStart */
	, 1535/* propertyStart */

};
TypeInfo X509ChainElementEnumerator_t1435_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ChainElementEnumerator"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509ChainElementEnumerator_t1435_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509ChainElementEnumerator_t1435_0_0_0/* byval_arg */
	, &X509ChainElementEnumerator_t1435_1_0_0/* this_arg */
	, &X509ChainElementEnumerator_t1435_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ChainElementEnumerator_t1435)/* instance_size */
	, sizeof (X509ChainElementEnumerator_t1435)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509ChainPolicy
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_4.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509ChainPolicy
extern TypeInfo X509ChainPolicy_t1430_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509ChainPolicy
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_4MethodDeclarations.h"
static const EncodedMethodIndex X509ChainPolicy_t1430_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509ChainPolicy_t1430_0_0_0;
extern const Il2CppType X509ChainPolicy_t1430_1_0_0;
struct X509ChainPolicy_t1430;
const Il2CppTypeDefinitionMetadata X509ChainPolicy_t1430_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509ChainPolicy_t1430_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5849/* fieldStart */
	, 7482/* methodStart */
	, -1/* eventStart */
	, 1537/* propertyStart */

};
TypeInfo X509ChainPolicy_t1430_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ChainPolicy"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509ChainPolicy_t1430_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509ChainPolicy_t1430_0_0_0/* byval_arg */
	, &X509ChainPolicy_t1430_1_0_0/* this_arg */
	, &X509ChainPolicy_t1430_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ChainPolicy_t1430)/* instance_size */
	, sizeof (X509ChainPolicy_t1430)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 5/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509ChainStatus
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509ChainStatus
extern TypeInfo X509ChainStatus_t1432_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509ChainStatus
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5MethodDeclarations.h"
static const EncodedMethodIndex X509ChainStatus_t1432_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509ChainStatus_t1432_0_0_0;
extern const Il2CppType X509ChainStatus_t1432_1_0_0;
const Il2CppTypeDefinitionMetadata X509ChainStatus_t1432_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, X509ChainStatus_t1432_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5857/* fieldStart */
	, 7489/* methodStart */
	, -1/* eventStart */
	, 1542/* propertyStart */

};
TypeInfo X509ChainStatus_t1432_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ChainStatus"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509ChainStatus_t1432_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509ChainStatus_t1432_0_0_0/* byval_arg */
	, &X509ChainStatus_t1432_1_0_0/* this_arg */
	, &X509ChainStatus_t1432_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)X509ChainStatus_t1432_marshal/* marshal_to_native_func */
	, (methodPointerType)X509ChainStatus_t1432_marshal_back/* marshal_from_native_func */
	, (methodPointerType)X509ChainStatus_t1432_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (X509ChainStatus_t1432)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509ChainStatus_t1432)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(X509ChainStatus_t1432_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
extern TypeInfo X509ChainStatusFlags_t1438_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1MethodDeclarations.h"
static const EncodedMethodIndex X509ChainStatusFlags_t1438_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair X509ChainStatusFlags_t1438_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509ChainStatusFlags_t1438_0_0_0;
extern const Il2CppType X509ChainStatusFlags_t1438_1_0_0;
const Il2CppTypeDefinitionMetadata X509ChainStatusFlags_t1438_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509ChainStatusFlags_t1438_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, X509ChainStatusFlags_t1438_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5859/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509ChainStatusFlags_t1438_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ChainStatusFlags"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2391/* custom_attributes_cache */
	, &X509ChainStatusFlags_t1438_0_0_0/* byval_arg */
	, &X509ChainStatusFlags_t1438_1_0_0/* this_arg */
	, &X509ChainStatusFlags_t1438_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ChainStatusFlags_t1438)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509ChainStatusFlags_t1438)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension
#include "System_System_Security_Cryptography_X509Certificates_X509Enh.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension
extern TypeInfo X509EnhancedKeyUsageExtension_t1439_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension
#include "System_System_Security_Cryptography_X509Certificates_X509EnhMethodDeclarations.h"
static const EncodedMethodIndex X509EnhancedKeyUsageExtension_t1439_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2087,
	2088,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509EnhancedKeyUsageExtension_t1439_0_0_0;
extern const Il2CppType X509EnhancedKeyUsageExtension_t1439_1_0_0;
struct X509EnhancedKeyUsageExtension_t1439;
const Il2CppTypeDefinitionMetadata X509EnhancedKeyUsageExtension_t1439_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &X509Extension_t1425_0_0_0/* parent */
	, X509EnhancedKeyUsageExtension_t1439_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5883/* fieldStart */
	, 7494/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509EnhancedKeyUsageExtension_t1439_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509EnhancedKeyUsageExtension"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509EnhancedKeyUsageExtension_t1439_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509EnhancedKeyUsageExtension_t1439_0_0_0/* byval_arg */
	, &X509EnhancedKeyUsageExtension_t1439_1_0_0/* this_arg */
	, &X509EnhancedKeyUsageExtension_t1439_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509EnhancedKeyUsageExtension_t1439)/* instance_size */
	, sizeof (X509EnhancedKeyUsageExtension_t1439)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(X509EnhancedKeyUsageExtension_t1439_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509Extension
#include "System_System_Security_Cryptography_X509Certificates_X509Ext.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509Extension
extern TypeInfo X509Extension_t1425_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509Extension
#include "System_System_Security_Cryptography_X509Certificates_X509ExtMethodDeclarations.h"
static const EncodedMethodIndex X509Extension_t1425_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2089,
	2052,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509Extension_t1425_1_0_0;
struct X509Extension_t1425;
const Il2CppTypeDefinitionMetadata X509Extension_t1425_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsnEncodedData_t1417_0_0_0/* parent */
	, X509Extension_t1425_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5886/* fieldStart */
	, 7498/* methodStart */
	, -1/* eventStart */
	, 1544/* propertyStart */

};
TypeInfo X509Extension_t1425_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Extension"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509Extension_t1425_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Extension_t1425_0_0_0/* byval_arg */
	, &X509Extension_t1425_1_0_0/* this_arg */
	, &X509Extension_t1425_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Extension_t1425)/* instance_size */
	, sizeof (X509Extension_t1425)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509ExtensionCollection
#include "System_System_Security_Cryptography_X509Certificates_X509Ext_0.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509ExtensionCollection
extern TypeInfo X509ExtensionCollection_t1426_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509ExtensionCollection
#include "System_System_Security_Cryptography_X509Certificates_X509Ext_0MethodDeclarations.h"
static const EncodedMethodIndex X509ExtensionCollection_t1426_VTable[9] = 
{
	626,
	601,
	627,
	628,
	2090,
	2091,
	2092,
	2093,
	2094,
};
static const Il2CppType* X509ExtensionCollection_t1426_InterfacesTypeInfos[] = 
{
	&ICollection_t1528_0_0_0,
	&IEnumerable_t1097_0_0_0,
};
static Il2CppInterfaceOffsetPair X509ExtensionCollection_t1426_InterfacesOffsets[] = 
{
	{ &ICollection_t1528_0_0_0, 4},
	{ &IEnumerable_t1097_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509ExtensionCollection_t1426_0_0_0;
extern const Il2CppType X509ExtensionCollection_t1426_1_0_0;
struct X509ExtensionCollection_t1426;
const Il2CppTypeDefinitionMetadata X509ExtensionCollection_t1426_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509ExtensionCollection_t1426_InterfacesTypeInfos/* implementedInterfaces */
	, X509ExtensionCollection_t1426_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509ExtensionCollection_t1426_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5887/* fieldStart */
	, 7504/* methodStart */
	, -1/* eventStart */
	, 1545/* propertyStart */

};
TypeInfo X509ExtensionCollection_t1426_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ExtensionCollection"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509ExtensionCollection_t1426_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2393/* custom_attributes_cache */
	, &X509ExtensionCollection_t1426_0_0_0/* byval_arg */
	, &X509ExtensionCollection_t1426_1_0_0/* this_arg */
	, &X509ExtensionCollection_t1426_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ExtensionCollection_t1426)/* instance_size */
	, sizeof (X509ExtensionCollection_t1426)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Ext_1.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator
extern TypeInfo X509ExtensionEnumerator_t1440_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509ExtensionEnumerator
#include "System_System_Security_Cryptography_X509Certificates_X509Ext_1MethodDeclarations.h"
static const EncodedMethodIndex X509ExtensionEnumerator_t1440_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2095,
	2096,
};
static const Il2CppType* X509ExtensionEnumerator_t1440_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
};
static Il2CppInterfaceOffsetPair X509ExtensionEnumerator_t1440_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509ExtensionEnumerator_t1440_0_0_0;
extern const Il2CppType X509ExtensionEnumerator_t1440_1_0_0;
struct X509ExtensionEnumerator_t1440;
const Il2CppTypeDefinitionMetadata X509ExtensionEnumerator_t1440_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509ExtensionEnumerator_t1440_InterfacesTypeInfos/* implementedInterfaces */
	, X509ExtensionEnumerator_t1440_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509ExtensionEnumerator_t1440_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5888/* fieldStart */
	, 7512/* methodStart */
	, -1/* eventStart */
	, 1549/* propertyStart */

};
TypeInfo X509ExtensionEnumerator_t1440_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ExtensionEnumerator"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509ExtensionEnumerator_t1440_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509ExtensionEnumerator_t1440_0_0_0/* byval_arg */
	, &X509ExtensionEnumerator_t1440_1_0_0/* this_arg */
	, &X509ExtensionEnumerator_t1440_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ExtensionEnumerator_t1440)/* instance_size */
	, sizeof (X509ExtensionEnumerator_t1440)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509FindType
#include "System_System_Security_Cryptography_X509Certificates_X509Fin.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509FindType
extern TypeInfo X509FindType_t1441_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509FindType
#include "System_System_Security_Cryptography_X509Certificates_X509FinMethodDeclarations.h"
static const EncodedMethodIndex X509FindType_t1441_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair X509FindType_t1441_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509FindType_t1441_0_0_0;
extern const Il2CppType X509FindType_t1441_1_0_0;
const Il2CppTypeDefinitionMetadata X509FindType_t1441_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509FindType_t1441_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, X509FindType_t1441_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5889/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509FindType_t1441_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509FindType"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509FindType_t1441_0_0_0/* byval_arg */
	, &X509FindType_t1441_1_0_0/* this_arg */
	, &X509FindType_t1441_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509FindType_t1441)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509FindType_t1441)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509KeyUsageExtension
#include "System_System_Security_Cryptography_X509Certificates_X509Key.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509KeyUsageExtension
extern TypeInfo X509KeyUsageExtension_t1442_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509KeyUsageExtension
#include "System_System_Security_Cryptography_X509Certificates_X509KeyMethodDeclarations.h"
static const EncodedMethodIndex X509KeyUsageExtension_t1442_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2097,
	2098,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509KeyUsageExtension_t1442_0_0_0;
extern const Il2CppType X509KeyUsageExtension_t1442_1_0_0;
struct X509KeyUsageExtension_t1442;
const Il2CppTypeDefinitionMetadata X509KeyUsageExtension_t1442_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &X509Extension_t1425_0_0_0/* parent */
	, X509KeyUsageExtension_t1442_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5905/* fieldStart */
	, 7516/* methodStart */
	, -1/* eventStart */
	, 1551/* propertyStart */

};
TypeInfo X509KeyUsageExtension_t1442_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509KeyUsageExtension"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509KeyUsageExtension_t1442_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509KeyUsageExtension_t1442_0_0_0/* byval_arg */
	, &X509KeyUsageExtension_t1442_1_0_0/* this_arg */
	, &X509KeyUsageExtension_t1442_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509KeyUsageExtension_t1442)/* instance_size */
	, sizeof (X509KeyUsageExtension_t1442)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
extern TypeInfo X509KeyUsageFlags_t1443_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0MethodDeclarations.h"
static const EncodedMethodIndex X509KeyUsageFlags_t1443_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair X509KeyUsageFlags_t1443_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509KeyUsageFlags_t1443_0_0_0;
extern const Il2CppType X509KeyUsageFlags_t1443_1_0_0;
const Il2CppTypeDefinitionMetadata X509KeyUsageFlags_t1443_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509KeyUsageFlags_t1443_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, X509KeyUsageFlags_t1443_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5910/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509KeyUsageFlags_t1443_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509KeyUsageFlags"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2394/* custom_attributes_cache */
	, &X509KeyUsageFlags_t1443_0_0_0/* byval_arg */
	, &X509KeyUsageFlags_t1443_1_0_0/* this_arg */
	, &X509KeyUsageFlags_t1443_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509KeyUsageFlags_t1443)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509KeyUsageFlags_t1443)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509NameType
#include "System_System_Security_Cryptography_X509Certificates_X509Nam.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509NameType
extern TypeInfo X509NameType_t1444_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509NameType
#include "System_System_Security_Cryptography_X509Certificates_X509NamMethodDeclarations.h"
static const EncodedMethodIndex X509NameType_t1444_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair X509NameType_t1444_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509NameType_t1444_0_0_0;
extern const Il2CppType X509NameType_t1444_1_0_0;
const Il2CppTypeDefinitionMetadata X509NameType_t1444_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509NameType_t1444_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, X509NameType_t1444_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5921/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509NameType_t1444_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509NameType"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509NameType_t1444_0_0_0/* byval_arg */
	, &X509NameType_t1444_1_0_0/* this_arg */
	, &X509NameType_t1444_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509NameType_t1444)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509NameType_t1444)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509RevocationFlag
#include "System_System_Security_Cryptography_X509Certificates_X509Rev.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509RevocationFlag
extern TypeInfo X509RevocationFlag_t1445_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509RevocationFlag
#include "System_System_Security_Cryptography_X509Certificates_X509RevMethodDeclarations.h"
static const EncodedMethodIndex X509RevocationFlag_t1445_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair X509RevocationFlag_t1445_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509RevocationFlag_t1445_0_0_0;
extern const Il2CppType X509RevocationFlag_t1445_1_0_0;
const Il2CppTypeDefinitionMetadata X509RevocationFlag_t1445_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509RevocationFlag_t1445_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, X509RevocationFlag_t1445_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5928/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509RevocationFlag_t1445_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509RevocationFlag"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509RevocationFlag_t1445_0_0_0/* byval_arg */
	, &X509RevocationFlag_t1445_1_0_0/* this_arg */
	, &X509RevocationFlag_t1445_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509RevocationFlag_t1445)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509RevocationFlag_t1445)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509RevocationMode
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509RevocationMode
extern TypeInfo X509RevocationMode_t1446_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509RevocationMode
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0MethodDeclarations.h"
static const EncodedMethodIndex X509RevocationMode_t1446_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair X509RevocationMode_t1446_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509RevocationMode_t1446_0_0_0;
extern const Il2CppType X509RevocationMode_t1446_1_0_0;
const Il2CppTypeDefinitionMetadata X509RevocationMode_t1446_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509RevocationMode_t1446_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, X509RevocationMode_t1446_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5932/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509RevocationMode_t1446_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509RevocationMode"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509RevocationMode_t1446_0_0_0/* byval_arg */
	, &X509RevocationMode_t1446_1_0_0/* this_arg */
	, &X509RevocationMode_t1446_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509RevocationMode_t1446)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509RevocationMode_t1446)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509Store
#include "System_System_Security_Cryptography_X509Certificates_X509Sto.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509Store
extern TypeInfo X509Store_t1434_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509Store
#include "System_System_Security_Cryptography_X509Certificates_X509StoMethodDeclarations.h"
static const EncodedMethodIndex X509Store_t1434_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509Store_t1434_0_0_0;
extern const Il2CppType X509Store_t1434_1_0_0;
struct X509Store_t1434;
const Il2CppTypeDefinitionMetadata X509Store_t1434_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Store_t1434_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5936/* fieldStart */
	, 7525/* methodStart */
	, -1/* eventStart */
	, 1552/* propertyStart */

};
TypeInfo X509Store_t1434_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Store"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509Store_t1434_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Store_t1434_0_0_0/* byval_arg */
	, &X509Store_t1434_1_0_0/* this_arg */
	, &X509Store_t1434_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Store_t1434)/* instance_size */
	, sizeof (X509Store_t1434)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(X509Store_t1434_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension
#include "System_System_Security_Cryptography_X509Certificates_X509Sub.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension
extern TypeInfo X509SubjectKeyIdentifierExtension_t1447_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension
#include "System_System_Security_Cryptography_X509Certificates_X509SubMethodDeclarations.h"
static const EncodedMethodIndex X509SubjectKeyIdentifierExtension_t1447_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2099,
	2100,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509SubjectKeyIdentifierExtension_t1447_0_0_0;
extern const Il2CppType X509SubjectKeyIdentifierExtension_t1447_1_0_0;
struct X509SubjectKeyIdentifierExtension_t1447;
const Il2CppTypeDefinitionMetadata X509SubjectKeyIdentifierExtension_t1447_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &X509Extension_t1425_0_0_0/* parent */
	, X509SubjectKeyIdentifierExtension_t1447_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5942/* fieldStart */
	, 7531/* methodStart */
	, -1/* eventStart */
	, 1555/* propertyStart */

};
TypeInfo X509SubjectKeyIdentifierExtension_t1447_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509SubjectKeyIdentifierExtension"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &X509SubjectKeyIdentifierExtension_t1447_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509SubjectKeyIdentifierExtension_t1447_0_0_0/* byval_arg */
	, &X509SubjectKeyIdentifierExtension_t1447_1_0_0/* this_arg */
	, &X509SubjectKeyIdentifierExtension_t1447_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509SubjectKeyIdentifierExtension_t1447)/* instance_size */
	, sizeof (X509SubjectKeyIdentifierExtension_t1447)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm
#include "System_System_Security_Cryptography_X509Certificates_X509Sub_0.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm
extern TypeInfo X509SubjectKeyIdentifierHashAlgorithm_t1448_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm
#include "System_System_Security_Cryptography_X509Certificates_X509Sub_0MethodDeclarations.h"
static const EncodedMethodIndex X509SubjectKeyIdentifierHashAlgorithm_t1448_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair X509SubjectKeyIdentifierHashAlgorithm_t1448_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509SubjectKeyIdentifierHashAlgorithm_t1448_0_0_0;
extern const Il2CppType X509SubjectKeyIdentifierHashAlgorithm_t1448_1_0_0;
const Il2CppTypeDefinitionMetadata X509SubjectKeyIdentifierHashAlgorithm_t1448_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509SubjectKeyIdentifierHashAlgorithm_t1448_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, X509SubjectKeyIdentifierHashAlgorithm_t1448_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5947/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509SubjectKeyIdentifierHashAlgorithm_t1448_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509SubjectKeyIdentifierHashAlgorithm"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509SubjectKeyIdentifierHashAlgorithm_t1448_0_0_0/* byval_arg */
	, &X509SubjectKeyIdentifierHashAlgorithm_t1448_1_0_0/* this_arg */
	, &X509SubjectKeyIdentifierHashAlgorithm_t1448_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509SubjectKeyIdentifierHashAlgorithm_t1448)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509SubjectKeyIdentifierHashAlgorithm_t1448)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.X509Certificates.X509VerificationFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Ver.h"
// Metadata Definition System.Security.Cryptography.X509Certificates.X509VerificationFlags
extern TypeInfo X509VerificationFlags_t1449_il2cpp_TypeInfo;
// System.Security.Cryptography.X509Certificates.X509VerificationFlags
#include "System_System_Security_Cryptography_X509Certificates_X509VerMethodDeclarations.h"
static const EncodedMethodIndex X509VerificationFlags_t1449_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair X509VerificationFlags_t1449_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType X509VerificationFlags_t1449_0_0_0;
extern const Il2CppType X509VerificationFlags_t1449_1_0_0;
const Il2CppTypeDefinitionMetadata X509VerificationFlags_t1449_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509VerificationFlags_t1449_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, X509VerificationFlags_t1449_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5951/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509VerificationFlags_t1449_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509VerificationFlags"/* name */
	, "System.Security.Cryptography.X509Certificates"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2396/* custom_attributes_cache */
	, &X509VerificationFlags_t1449_0_0_0/* byval_arg */
	, &X509VerificationFlags_t1449_1_0_0/* this_arg */
	, &X509VerificationFlags_t1449_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509VerificationFlags_t1449)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509VerificationFlags_t1449)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.AsnDecodeStatus
#include "System_System_Security_Cryptography_AsnDecodeStatus.h"
// Metadata Definition System.Security.Cryptography.AsnDecodeStatus
extern TypeInfo AsnDecodeStatus_t1450_il2cpp_TypeInfo;
// System.Security.Cryptography.AsnDecodeStatus
#include "System_System_Security_Cryptography_AsnDecodeStatusMethodDeclarations.h"
static const EncodedMethodIndex AsnDecodeStatus_t1450_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair AsnDecodeStatus_t1450_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType AsnDecodeStatus_t1450_0_0_0;
extern const Il2CppType AsnDecodeStatus_t1450_1_0_0;
const Il2CppTypeDefinitionMetadata AsnDecodeStatus_t1450_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AsnDecodeStatus_t1450_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, AsnDecodeStatus_t1450_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5966/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AsnDecodeStatus_t1450_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsnDecodeStatus"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AsnDecodeStatus_t1450_0_0_0/* byval_arg */
	, &AsnDecodeStatus_t1450_1_0_0/* this_arg */
	, &AsnDecodeStatus_t1450_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsnDecodeStatus_t1450)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AsnDecodeStatus_t1450)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Cryptography.AsnEncodedData
#include "System_System_Security_Cryptography_AsnEncodedData.h"
// Metadata Definition System.Security.Cryptography.AsnEncodedData
extern TypeInfo AsnEncodedData_t1417_il2cpp_TypeInfo;
// System.Security.Cryptography.AsnEncodedData
#include "System_System_Security_Cryptography_AsnEncodedDataMethodDeclarations.h"
static const EncodedMethodIndex AsnEncodedData_t1417_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2051,
	2052,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType AsnEncodedData_t1417_1_0_0;
struct AsnEncodedData_t1417;
const Il2CppTypeDefinitionMetadata AsnEncodedData_t1417_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsnEncodedData_t1417_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5973/* fieldStart */
	, 7545/* methodStart */
	, -1/* eventStart */
	, 1556/* propertyStart */

};
TypeInfo AsnEncodedData_t1417_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsnEncodedData"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &AsnEncodedData_t1417_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AsnEncodedData_t1417_0_0_0/* byval_arg */
	, &AsnEncodedData_t1417_1_0_0/* this_arg */
	, &AsnEncodedData_t1417_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsnEncodedData_t1417)/* instance_size */
	, sizeof (AsnEncodedData_t1417)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AsnEncodedData_t1417_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.Oid
#include "System_System_Security_Cryptography_Oid.h"
// Metadata Definition System.Security.Cryptography.Oid
extern TypeInfo Oid_t1418_il2cpp_TypeInfo;
// System.Security.Cryptography.Oid
#include "System_System_Security_Cryptography_OidMethodDeclarations.h"
static const EncodedMethodIndex Oid_t1418_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Oid_t1418_0_0_0;
extern const Il2CppType Oid_t1418_1_0_0;
struct Oid_t1418;
const Il2CppTypeDefinitionMetadata Oid_t1418_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Oid_t1418_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5976/* fieldStart */
	, 7561/* methodStart */
	, -1/* eventStart */
	, 1558/* propertyStart */

};
TypeInfo Oid_t1418_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Oid"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &Oid_t1418_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Oid_t1418_0_0_0/* byval_arg */
	, &Oid_t1418_1_0_0/* this_arg */
	, &Oid_t1418_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Oid_t1418)/* instance_size */
	, sizeof (Oid_t1418)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Oid_t1418_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.OidCollection
#include "System_System_Security_Cryptography_OidCollection.h"
// Metadata Definition System.Security.Cryptography.OidCollection
extern TypeInfo OidCollection_t1436_il2cpp_TypeInfo;
// System.Security.Cryptography.OidCollection
#include "System_System_Security_Cryptography_OidCollectionMethodDeclarations.h"
static const EncodedMethodIndex OidCollection_t1436_VTable[9] = 
{
	626,
	601,
	627,
	628,
	2101,
	2102,
	2103,
	2104,
	2105,
};
static const Il2CppType* OidCollection_t1436_InterfacesTypeInfos[] = 
{
	&ICollection_t1528_0_0_0,
	&IEnumerable_t1097_0_0_0,
};
static Il2CppInterfaceOffsetPair OidCollection_t1436_InterfacesOffsets[] = 
{
	{ &ICollection_t1528_0_0_0, 4},
	{ &IEnumerable_t1097_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType OidCollection_t1436_0_0_0;
extern const Il2CppType OidCollection_t1436_1_0_0;
struct OidCollection_t1436;
const Il2CppTypeDefinitionMetadata OidCollection_t1436_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, OidCollection_t1436_InterfacesTypeInfos/* implementedInterfaces */
	, OidCollection_t1436_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, OidCollection_t1436_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5979/* fieldStart */
	, 7568/* methodStart */
	, -1/* eventStart */
	, 1560/* propertyStart */

};
TypeInfo OidCollection_t1436_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "OidCollection"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &OidCollection_t1436_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2399/* custom_attributes_cache */
	, &OidCollection_t1436_0_0_0/* byval_arg */
	, &OidCollection_t1436_1_0_0/* this_arg */
	, &OidCollection_t1436_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OidCollection_t1436)/* instance_size */
	, sizeof (OidCollection_t1436)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.OidEnumerator
#include "System_System_Security_Cryptography_OidEnumerator.h"
// Metadata Definition System.Security.Cryptography.OidEnumerator
extern TypeInfo OidEnumerator_t1451_il2cpp_TypeInfo;
// System.Security.Cryptography.OidEnumerator
#include "System_System_Security_Cryptography_OidEnumeratorMethodDeclarations.h"
static const EncodedMethodIndex OidEnumerator_t1451_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2106,
	2107,
};
static const Il2CppType* OidEnumerator_t1451_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
};
static Il2CppInterfaceOffsetPair OidEnumerator_t1451_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType OidEnumerator_t1451_0_0_0;
extern const Il2CppType OidEnumerator_t1451_1_0_0;
struct OidEnumerator_t1451;
const Il2CppTypeDefinitionMetadata OidEnumerator_t1451_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, OidEnumerator_t1451_InterfacesTypeInfos/* implementedInterfaces */
	, OidEnumerator_t1451_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, OidEnumerator_t1451_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5981/* fieldStart */
	, 7576/* methodStart */
	, -1/* eventStart */
	, 1564/* propertyStart */

};
TypeInfo OidEnumerator_t1451_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "OidEnumerator"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &OidEnumerator_t1451_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OidEnumerator_t1451_0_0_0/* byval_arg */
	, &OidEnumerator_t1451_1_0_0/* this_arg */
	, &OidEnumerator_t1451_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OidEnumerator_t1451)/* instance_size */
	, sizeof (OidEnumerator_t1451)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.BaseMachine
#include "System_System_Text_RegularExpressions_BaseMachine.h"
// Metadata Definition System.Text.RegularExpressions.BaseMachine
extern TypeInfo BaseMachine_t1453_il2cpp_TypeInfo;
// System.Text.RegularExpressions.BaseMachine
#include "System_System_Text_RegularExpressions_BaseMachineMethodDeclarations.h"
extern const Il2CppType MatchAppendEvaluator_t1452_0_0_0;
static const Il2CppType* BaseMachine_t1453_il2cpp_TypeInfo__nestedTypes[1] =
{
	&MatchAppendEvaluator_t1452_0_0_0,
};
static const EncodedMethodIndex BaseMachine_t1453_VTable[8] = 
{
	626,
	601,
	627,
	628,
	2108,
	2109,
	2109,
	2108,
};
extern const Il2CppType IMachine_t1458_0_0_0;
static const Il2CppType* BaseMachine_t1453_InterfacesTypeInfos[] = 
{
	&IMachine_t1458_0_0_0,
};
static Il2CppInterfaceOffsetPair BaseMachine_t1453_InterfacesOffsets[] = 
{
	{ &IMachine_t1458_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType BaseMachine_t1453_0_0_0;
extern const Il2CppType BaseMachine_t1453_1_0_0;
struct BaseMachine_t1453;
const Il2CppTypeDefinitionMetadata BaseMachine_t1453_DefinitionMetadata = 
{
	NULL/* declaringType */
	, BaseMachine_t1453_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, BaseMachine_t1453_InterfacesTypeInfos/* implementedInterfaces */
	, BaseMachine_t1453_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BaseMachine_t1453_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5983/* fieldStart */
	, 7579/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BaseMachine_t1453_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseMachine"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &BaseMachine_t1453_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseMachine_t1453_0_0_0/* byval_arg */
	, &BaseMachine_t1453_1_0_0/* this_arg */
	, &BaseMachine_t1453_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseMachine_t1453)/* instance_size */
	, sizeof (BaseMachine_t1453)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.BaseMachine/MatchAppendEvaluator
#include "System_System_Text_RegularExpressions_BaseMachine_MatchAppen.h"
// Metadata Definition System.Text.RegularExpressions.BaseMachine/MatchAppendEvaluator
extern TypeInfo MatchAppendEvaluator_t1452_il2cpp_TypeInfo;
// System.Text.RegularExpressions.BaseMachine/MatchAppendEvaluator
#include "System_System_Text_RegularExpressions_BaseMachine_MatchAppenMethodDeclarations.h"
static const EncodedMethodIndex MatchAppendEvaluator_t1452_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	2110,
	2111,
	2112,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
static Il2CppInterfaceOffsetPair MatchAppendEvaluator_t1452_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType MatchAppendEvaluator_t1452_1_0_0;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
struct MatchAppendEvaluator_t1452;
const Il2CppTypeDefinitionMetadata MatchAppendEvaluator_t1452_DefinitionMetadata = 
{
	&BaseMachine_t1453_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MatchAppendEvaluator_t1452_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, MatchAppendEvaluator_t1452_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7584/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MatchAppendEvaluator_t1452_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatchAppendEvaluator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MatchAppendEvaluator_t1452_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatchAppendEvaluator_t1452_0_0_0/* byval_arg */
	, &MatchAppendEvaluator_t1452_1_0_0/* this_arg */
	, &MatchAppendEvaluator_t1452_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_MatchAppendEvaluator_t1452/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatchAppendEvaluator_t1452)/* instance_size */
	, sizeof (MatchAppendEvaluator_t1452)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 261/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.Capture
#include "System_System_Text_RegularExpressions_Capture.h"
// Metadata Definition System.Text.RegularExpressions.Capture
extern TypeInfo Capture_t1454_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Capture
#include "System_System_Text_RegularExpressions_CaptureMethodDeclarations.h"
static const EncodedMethodIndex Capture_t1454_VTable[4] = 
{
	626,
	601,
	627,
	2113,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Capture_t1454_0_0_0;
extern const Il2CppType Capture_t1454_1_0_0;
struct Capture_t1454;
const Il2CppTypeDefinitionMetadata Capture_t1454_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Capture_t1454_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5984/* fieldStart */
	, 7588/* methodStart */
	, -1/* eventStart */
	, 1565/* propertyStart */

};
TypeInfo Capture_t1454_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Capture"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &Capture_t1454_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Capture_t1454_0_0_0/* byval_arg */
	, &Capture_t1454_1_0_0/* this_arg */
	, &Capture_t1454_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Capture_t1454)/* instance_size */
	, sizeof (Capture_t1454)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 4/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.CaptureCollection
#include "System_System_Text_RegularExpressions_CaptureCollection.h"
// Metadata Definition System.Text.RegularExpressions.CaptureCollection
extern TypeInfo CaptureCollection_t1456_il2cpp_TypeInfo;
// System.Text.RegularExpressions.CaptureCollection
#include "System_System_Text_RegularExpressions_CaptureCollectionMethodDeclarations.h"
static const EncodedMethodIndex CaptureCollection_t1456_VTable[9] = 
{
	626,
	601,
	627,
	628,
	2114,
	2115,
	2116,
	2117,
	2118,
};
static const Il2CppType* CaptureCollection_t1456_InterfacesTypeInfos[] = 
{
	&ICollection_t1528_0_0_0,
	&IEnumerable_t1097_0_0_0,
};
static Il2CppInterfaceOffsetPair CaptureCollection_t1456_InterfacesOffsets[] = 
{
	{ &ICollection_t1528_0_0_0, 4},
	{ &IEnumerable_t1097_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType CaptureCollection_t1456_0_0_0;
extern const Il2CppType CaptureCollection_t1456_1_0_0;
struct CaptureCollection_t1456;
const Il2CppTypeDefinitionMetadata CaptureCollection_t1456_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CaptureCollection_t1456_InterfacesTypeInfos/* implementedInterfaces */
	, CaptureCollection_t1456_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CaptureCollection_t1456_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5987/* fieldStart */
	, 7595/* methodStart */
	, -1/* eventStart */
	, 1569/* propertyStart */

};
TypeInfo CaptureCollection_t1456_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "CaptureCollection"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &CaptureCollection_t1456_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2400/* custom_attributes_cache */
	, &CaptureCollection_t1456_0_0_0/* byval_arg */
	, &CaptureCollection_t1456_1_0_0/* this_arg */
	, &CaptureCollection_t1456_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CaptureCollection_t1456)/* instance_size */
	, sizeof (CaptureCollection_t1456)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.Group
#include "System_System_Text_RegularExpressions_Group.h"
// Metadata Definition System.Text.RegularExpressions.Group
extern TypeInfo Group_t1373_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Group
#include "System_System_Text_RegularExpressions_GroupMethodDeclarations.h"
static const EncodedMethodIndex Group_t1373_VTable[4] = 
{
	626,
	601,
	627,
	2113,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Group_t1373_0_0_0;
extern const Il2CppType Group_t1373_1_0_0;
struct Group_t1373;
const Il2CppTypeDefinitionMetadata Group_t1373_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Capture_t1454_0_0_0/* parent */
	, Group_t1373_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5988/* fieldStart */
	, 7602/* methodStart */
	, -1/* eventStart */
	, 1572/* propertyStart */

};
TypeInfo Group_t1373_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Group"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &Group_t1373_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Group_t1373_0_0_0/* byval_arg */
	, &Group_t1373_1_0_0/* this_arg */
	, &Group_t1373_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Group_t1373)/* instance_size */
	, sizeof (Group_t1373)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Group_t1373_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.GroupCollection
#include "System_System_Text_RegularExpressions_GroupCollection.h"
// Metadata Definition System.Text.RegularExpressions.GroupCollection
extern TypeInfo GroupCollection_t1372_il2cpp_TypeInfo;
// System.Text.RegularExpressions.GroupCollection
#include "System_System_Text_RegularExpressions_GroupCollectionMethodDeclarations.h"
static const EncodedMethodIndex GroupCollection_t1372_VTable[9] = 
{
	626,
	601,
	627,
	628,
	2119,
	2120,
	2121,
	2122,
	2123,
};
static const Il2CppType* GroupCollection_t1372_InterfacesTypeInfos[] = 
{
	&ICollection_t1528_0_0_0,
	&IEnumerable_t1097_0_0_0,
};
static Il2CppInterfaceOffsetPair GroupCollection_t1372_InterfacesOffsets[] = 
{
	{ &ICollection_t1528_0_0_0, 4},
	{ &IEnumerable_t1097_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType GroupCollection_t1372_0_0_0;
extern const Il2CppType GroupCollection_t1372_1_0_0;
struct GroupCollection_t1372;
const Il2CppTypeDefinitionMetadata GroupCollection_t1372_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, GroupCollection_t1372_InterfacesTypeInfos/* implementedInterfaces */
	, GroupCollection_t1372_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GroupCollection_t1372_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5991/* fieldStart */
	, 7608/* methodStart */
	, -1/* eventStart */
	, 1574/* propertyStart */

};
TypeInfo GroupCollection_t1372_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "GroupCollection"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &GroupCollection_t1372_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2401/* custom_attributes_cache */
	, &GroupCollection_t1372_0_0_0/* byval_arg */
	, &GroupCollection_t1372_1_0_0/* this_arg */
	, &GroupCollection_t1372_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GroupCollection_t1372)/* instance_size */
	, sizeof (GroupCollection_t1372)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.Match
#include "System_System_Text_RegularExpressions_Match.h"
// Metadata Definition System.Text.RegularExpressions.Match
extern TypeInfo Match_t1371_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Match
#include "System_System_Text_RegularExpressions_MatchMethodDeclarations.h"
static const EncodedMethodIndex Match_t1371_VTable[5] = 
{
	626,
	601,
	627,
	2113,
	2124,
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Match_t1371_0_0_0;
extern const Il2CppType Match_t1371_1_0_0;
struct Match_t1371;
const Il2CppTypeDefinitionMetadata Match_t1371_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Group_t1373_0_0_0/* parent */
	, Match_t1371_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5993/* fieldStart */
	, 7616/* methodStart */
	, -1/* eventStart */
	, 1578/* propertyStart */

};
TypeInfo Match_t1371_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Match"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &Match_t1371_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Match_t1371_0_0_0/* byval_arg */
	, &Match_t1371_1_0_0/* this_arg */
	, &Match_t1371_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Match_t1371)/* instance_size */
	, sizeof (Match_t1371)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Match_t1371_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 3/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.RegularExpressions.MatchCollection
#include "System_System_Text_RegularExpressions_MatchCollection.h"
// Metadata Definition System.Text.RegularExpressions.MatchCollection
extern TypeInfo MatchCollection_t1370_il2cpp_TypeInfo;
// System.Text.RegularExpressions.MatchCollection
#include "System_System_Text_RegularExpressions_MatchCollectionMethodDeclarations.h"
extern const Il2CppType Enumerator_t1459_0_0_0;
static const Il2CppType* MatchCollection_t1370_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Enumerator_t1459_0_0_0,
};
static const EncodedMethodIndex MatchCollection_t1370_VTable[10] = 
{
	626,
	601,
	627,
	628,
	2125,
	2126,
	2127,
	2128,
	2129,
	2130,
};
static const Il2CppType* MatchCollection_t1370_InterfacesTypeInfos[] = 
{
	&ICollection_t1528_0_0_0,
	&IEnumerable_t1097_0_0_0,
};
static Il2CppInterfaceOffsetPair MatchCollection_t1370_InterfacesOffsets[] = 
{
	{ &ICollection_t1528_0_0_0, 4},
	{ &IEnumerable_t1097_0_0_0, 8},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType MatchCollection_t1370_0_0_0;
extern const Il2CppType MatchCollection_t1370_1_0_0;
struct MatchCollection_t1370;
const Il2CppTypeDefinitionMetadata MatchCollection_t1370_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MatchCollection_t1370_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, MatchCollection_t1370_InterfacesTypeInfos/* implementedInterfaces */
	, MatchCollection_t1370_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MatchCollection_t1370_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5998/* fieldStart */
	, 7624/* methodStart */
	, -1/* eventStart */
	, 1581/* propertyStart */

};
TypeInfo MatchCollection_t1370_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatchCollection"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &MatchCollection_t1370_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2402/* custom_attributes_cache */
	, &MatchCollection_t1370_0_0_0/* byval_arg */
	, &MatchCollection_t1370_1_0_0/* this_arg */
	, &MatchCollection_t1370_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatchCollection_t1370)/* instance_size */
	, sizeof (MatchCollection_t1370)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 5/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.RegularExpressions.MatchCollection/Enumerator
#include "System_System_Text_RegularExpressions_MatchCollection_Enumer.h"
// Metadata Definition System.Text.RegularExpressions.MatchCollection/Enumerator
extern TypeInfo Enumerator_t1459_il2cpp_TypeInfo;
// System.Text.RegularExpressions.MatchCollection/Enumerator
#include "System_System_Text_RegularExpressions_MatchCollection_EnumerMethodDeclarations.h"
static const EncodedMethodIndex Enumerator_t1459_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2131,
	2132,
};
static const Il2CppType* Enumerator_t1459_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t1459_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Enumerator_t1459_1_0_0;
struct Enumerator_t1459;
const Il2CppTypeDefinitionMetadata Enumerator_t1459_DefinitionMetadata = 
{
	&MatchCollection_t1370_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t1459_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t1459_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerator_t1459_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6000/* fieldStart */
	, 7633/* methodStart */
	, -1/* eventStart */
	, 1586/* propertyStart */

};
TypeInfo Enumerator_t1459_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t1459_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t1459_0_0_0/* byval_arg */
	, &Enumerator_t1459_1_0_0/* this_arg */
	, &Enumerator_t1459_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t1459)/* instance_size */
	, sizeof (Enumerator_t1459)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.Regex
#include "System_System_Text_RegularExpressions_Regex.h"
// Metadata Definition System.Text.RegularExpressions.Regex
extern TypeInfo Regex_t521_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Regex
#include "System_System_Text_RegularExpressions_RegexMethodDeclarations.h"
static const EncodedMethodIndex Regex_t521_VTable[5] = 
{
	626,
	601,
	627,
	2133,
	2134,
};
static const Il2CppType* Regex_t521_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair Regex_t521_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Regex_t521_0_0_0;
extern const Il2CppType Regex_t521_1_0_0;
struct Regex_t521;
const Il2CppTypeDefinitionMetadata Regex_t521_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Regex_t521_InterfacesTypeInfos/* implementedInterfaces */
	, Regex_t521_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Regex_t521_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6002/* fieldStart */
	, 7636/* methodStart */
	, -1/* eventStart */
	, 1587/* propertyStart */

};
TypeInfo Regex_t521_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Regex"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &Regex_t521_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Regex_t521_0_0_0/* byval_arg */
	, &Regex_t521_1_0_0/* this_arg */
	, &Regex_t521_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Regex_t521)/* instance_size */
	, sizeof (Regex_t521)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Regex_t521_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 5/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.RegularExpressions.RegexOptions
#include "System_System_Text_RegularExpressions_RegexOptions.h"
// Metadata Definition System.Text.RegularExpressions.RegexOptions
extern TypeInfo RegexOptions_t1464_il2cpp_TypeInfo;
// System.Text.RegularExpressions.RegexOptions
#include "System_System_Text_RegularExpressions_RegexOptionsMethodDeclarations.h"
static const EncodedMethodIndex RegexOptions_t1464_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair RegexOptions_t1464_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType RegexOptions_t1464_0_0_0;
extern const Il2CppType RegexOptions_t1464_1_0_0;
const Il2CppTypeDefinitionMetadata RegexOptions_t1464_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RegexOptions_t1464_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, RegexOptions_t1464_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6016/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RegexOptions_t1464_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "RegexOptions"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2407/* custom_attributes_cache */
	, &RegexOptions_t1464_0_0_0/* byval_arg */
	, &RegexOptions_t1464_1_0_0/* this_arg */
	, &RegexOptions_t1464_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RegexOptions_t1464)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RegexOptions_t1464)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.OpCode
#include "System_System_Text_RegularExpressions_OpCode.h"
// Metadata Definition System.Text.RegularExpressions.OpCode
extern TypeInfo OpCode_t1465_il2cpp_TypeInfo;
// System.Text.RegularExpressions.OpCode
#include "System_System_Text_RegularExpressions_OpCodeMethodDeclarations.h"
static const EncodedMethodIndex OpCode_t1465_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair OpCode_t1465_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType OpCode_t1465_0_0_0;
extern const Il2CppType OpCode_t1465_1_0_0;
// System.UInt16
#include "mscorlib_System_UInt16.h"
extern TypeInfo UInt16_t1122_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata OpCode_t1465_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OpCode_t1465_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, OpCode_t1465_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6026/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OpCode_t1465_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "OpCode"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &UInt16_t1122_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OpCode_t1465_0_0_0/* byval_arg */
	, &OpCode_t1465_1_0_0/* this_arg */
	, &OpCode_t1465_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OpCode_t1465)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (OpCode_t1465)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 26/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.OpFlags
#include "System_System_Text_RegularExpressions_OpFlags.h"
// Metadata Definition System.Text.RegularExpressions.OpFlags
extern TypeInfo OpFlags_t1466_il2cpp_TypeInfo;
// System.Text.RegularExpressions.OpFlags
#include "System_System_Text_RegularExpressions_OpFlagsMethodDeclarations.h"
static const EncodedMethodIndex OpFlags_t1466_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair OpFlags_t1466_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType OpFlags_t1466_0_0_0;
extern const Il2CppType OpFlags_t1466_1_0_0;
const Il2CppTypeDefinitionMetadata OpFlags_t1466_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OpFlags_t1466_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, OpFlags_t1466_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6052/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OpFlags_t1466_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "OpFlags"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &UInt16_t1122_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2408/* custom_attributes_cache */
	, &OpFlags_t1466_0_0_0/* byval_arg */
	, &OpFlags_t1466_1_0_0/* this_arg */
	, &OpFlags_t1466_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OpFlags_t1466)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (OpFlags_t1466)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Text.RegularExpressions.Position
#include "System_System_Text_RegularExpressions_Position.h"
// Metadata Definition System.Text.RegularExpressions.Position
extern TypeInfo Position_t1467_il2cpp_TypeInfo;
// System.Text.RegularExpressions.Position
#include "System_System_Text_RegularExpressions_PositionMethodDeclarations.h"
static const EncodedMethodIndex Position_t1467_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Position_t1467_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType Position_t1467_0_0_0;
extern const Il2CppType Position_t1467_1_0_0;
const Il2CppTypeDefinitionMetadata Position_t1467_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Position_t1467_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Position_t1467_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6058/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Position_t1467_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Position"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &UInt16_t1122_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Position_t1467_0_0_0/* byval_arg */
	, &Position_t1467_1_0_0/* this_arg */
	, &Position_t1467_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Position_t1467)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Position_t1467)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Text.RegularExpressions.IMachine
extern TypeInfo IMachine_t1458_il2cpp_TypeInfo;
extern Il2CppImage g_System_dll_Image;
extern const Il2CppType IMachine_t1458_1_0_0;
struct IMachine_t1458;
const Il2CppTypeDefinitionMetadata IMachine_t1458_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7666/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IMachine_t1458_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMachine"/* name */
	, "System.Text.RegularExpressions"/* namespaze */
	, NULL/* methods */
	, &IMachine_t1458_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IMachine_t1458_0_0_0/* byval_arg */
	, &IMachine_t1458_1_0_0/* this_arg */
	, &IMachine_t1458_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
