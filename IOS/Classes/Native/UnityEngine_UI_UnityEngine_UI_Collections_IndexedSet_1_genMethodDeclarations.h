﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>
struct IndexedSet_1_t639;
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t770;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ICanvasElement>
struct IEnumerator_1_t3085;
// UnityEngine.UI.ICanvasElement[]
struct ICanvasElementU5BU5D_t2536;
// System.Predicate`1<UnityEngine.UI.ICanvasElement>
struct Predicate_1_t641;
// System.Comparison`1<UnityEngine.UI.ICanvasElement>
struct Comparison_1_t640;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
#define IndexedSet_1__ctor_m4658(__this, method) (( void (*) (IndexedSet_1_t639 *, const MethodInfo*))IndexedSet_1__ctor_m17771_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17772(__this, method) (( Object_t * (*) (IndexedSet_1_t639 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17773_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Add(T)
#define IndexedSet_1_Add_m17774(__this, ___item, method) (( void (*) (IndexedSet_1_t639 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m17775_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Remove(T)
#define IndexedSet_1_Remove_m17776(__this, ___item, method) (( bool (*) (IndexedSet_1_t639 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m17777_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m17778(__this, method) (( Object_t* (*) (IndexedSet_1_t639 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m17779_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Clear()
#define IndexedSet_1_Clear_m17780(__this, method) (( void (*) (IndexedSet_1_t639 *, const MethodInfo*))IndexedSet_1_Clear_m17781_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Contains(T)
#define IndexedSet_1_Contains_m17782(__this, ___item, method) (( bool (*) (IndexedSet_1_t639 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m17783_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m17784(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t639 *, ICanvasElementU5BU5D_t2536*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m17785_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Count()
#define IndexedSet_1_get_Count_m17786(__this, method) (( int32_t (*) (IndexedSet_1_t639 *, const MethodInfo*))IndexedSet_1_get_Count_m17787_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m17788(__this, method) (( bool (*) (IndexedSet_1_t639 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m17789_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::IndexOf(T)
#define IndexedSet_1_IndexOf_m17790(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t639 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m17791_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m17792(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t639 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m17793_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m17794(__this, ___index, method) (( void (*) (IndexedSet_1_t639 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m17795_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m17796(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t639 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m17797_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m17798(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t639 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m17799_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m4663(__this, ___match, method) (( void (*) (IndexedSet_1_t639 *, Predicate_1_t641 *, const MethodInfo*))IndexedSet_1_RemoveAll_m17800_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m4664(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t639 *, Comparison_1_t640 *, const MethodInfo*))IndexedSet_1_Sort_m17801_gshared)(__this, ___sortLayoutFunction, method)
