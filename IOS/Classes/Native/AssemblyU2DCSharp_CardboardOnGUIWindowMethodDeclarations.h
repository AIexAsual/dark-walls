﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CardboardOnGUIWindow
struct CardboardOnGUIWindow_t224;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CardboardOnGUIWindow::.ctor()
extern "C" void CardboardOnGUIWindow__ctor_m1408 (CardboardOnGUIWindow_t224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUIWindow::Awake()
extern "C" void CardboardOnGUIWindow_Awake_m1409 (CardboardOnGUIWindow_t224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUIWindow::Reset()
extern "C" void CardboardOnGUIWindow_Reset_m1410 (CardboardOnGUIWindow_t224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUIWindow::Create(UnityEngine.RenderTexture)
extern "C" void CardboardOnGUIWindow_Create_m1411 (CardboardOnGUIWindow_t224 * __this, RenderTexture_t15 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUIWindow::OnDisable()
extern "C" void CardboardOnGUIWindow_OnDisable_m1412 (CardboardOnGUIWindow_t224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUIWindow::LateUpdate()
extern "C" void CardboardOnGUIWindow_LateUpdate_m1413 (CardboardOnGUIWindow_t224 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
