﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_PlanetMars
struct CameraFilterPack_TV_PlanetMars_t190;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_PlanetMars::.ctor()
extern "C" void CameraFilterPack_TV_PlanetMars__ctor_m1230 (CameraFilterPack_TV_PlanetMars_t190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_PlanetMars::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_PlanetMars_get_material_m1231 (CameraFilterPack_TV_PlanetMars_t190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_PlanetMars::Start()
extern "C" void CameraFilterPack_TV_PlanetMars_Start_m1232 (CameraFilterPack_TV_PlanetMars_t190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_PlanetMars::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_PlanetMars_OnRenderImage_m1233 (CameraFilterPack_TV_PlanetMars_t190 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_PlanetMars::Update()
extern "C" void CameraFilterPack_TV_PlanetMars_Update_m1234 (CameraFilterPack_TV_PlanetMars_t190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_PlanetMars::OnDisable()
extern "C" void CameraFilterPack_TV_PlanetMars_OnDisable_m1235 (CameraFilterPack_TV_PlanetMars_t190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
