﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<iTween/EaseType>
struct Comparer_1_t2455;
// System.Object
struct Object_t;
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"

// System.Void System.Collections.Generic.Comparer`1<iTween/EaseType>::.ctor()
extern "C" void Comparer_1__ctor_m16490_gshared (Comparer_1_t2455 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m16490(__this, method) (( void (*) (Comparer_1_t2455 *, const MethodInfo*))Comparer_1__ctor_m16490_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<iTween/EaseType>::.cctor()
extern "C" void Comparer_1__cctor_m16491_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m16491(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m16491_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<iTween/EaseType>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m16492_gshared (Comparer_1_t2455 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m16492(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t2455 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m16492_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<iTween/EaseType>::get_Default()
extern "C" Comparer_1_t2455 * Comparer_1_get_Default_m16493_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m16493(__this /* static, unused */, method) (( Comparer_1_t2455 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m16493_gshared)(__this /* static, unused */, method)
