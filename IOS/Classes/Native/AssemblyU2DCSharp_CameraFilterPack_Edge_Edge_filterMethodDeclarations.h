﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Edge_Edge_filter
struct CameraFilterPack_Edge_Edge_filter_t113;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Edge_Edge_filter::.ctor()
extern "C" void CameraFilterPack_Edge_Edge_filter__ctor_m725 (CameraFilterPack_Edge_Edge_filter_t113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Edge_Edge_filter::get_material()
extern "C" Material_t2 * CameraFilterPack_Edge_Edge_filter_get_material_m726 (CameraFilterPack_Edge_Edge_filter_t113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Edge_filter::Start()
extern "C" void CameraFilterPack_Edge_Edge_filter_Start_m727 (CameraFilterPack_Edge_Edge_filter_t113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Edge_filter::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Edge_Edge_filter_OnRenderImage_m728 (CameraFilterPack_Edge_Edge_filter_t113 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Edge_filter::OnValidate()
extern "C" void CameraFilterPack_Edge_Edge_filter_OnValidate_m729 (CameraFilterPack_Edge_Edge_filter_t113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Edge_filter::Update()
extern "C" void CameraFilterPack_Edge_Edge_filter_Update_m730 (CameraFilterPack_Edge_Edge_filter_t113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Edge_filter::OnDisable()
extern "C" void CameraFilterPack_Edge_Edge_filter_OnDisable_m731 (CameraFilterPack_Edge_Edge_filter_t113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
