﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SFX/<WaitAndPlaySounds>c__Iterator17
struct U3CWaitAndPlaySoundsU3Ec__Iterator17_t407;
// System.Object
struct Object_t;

// System.Void SFX/<WaitAndPlaySounds>c__Iterator17::.ctor()
extern "C" void U3CWaitAndPlaySoundsU3Ec__Iterator17__ctor_m2318 (U3CWaitAndPlaySoundsU3Ec__Iterator17_t407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SFX/<WaitAndPlaySounds>c__Iterator17::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndPlaySoundsU3Ec__Iterator17_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2319 (U3CWaitAndPlaySoundsU3Ec__Iterator17_t407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SFX/<WaitAndPlaySounds>c__Iterator17::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndPlaySoundsU3Ec__Iterator17_System_Collections_IEnumerator_get_Current_m2320 (U3CWaitAndPlaySoundsU3Ec__Iterator17_t407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SFX/<WaitAndPlaySounds>c__Iterator17::MoveNext()
extern "C" bool U3CWaitAndPlaySoundsU3Ec__Iterator17_MoveNext_m2321 (U3CWaitAndPlaySoundsU3Ec__Iterator17_t407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFX/<WaitAndPlaySounds>c__Iterator17::Dispose()
extern "C" void U3CWaitAndPlaySoundsU3Ec__Iterator17_Dispose_m2322 (U3CWaitAndPlaySoundsU3Ec__Iterator17_t407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFX/<WaitAndPlaySounds>c__Iterator17::Reset()
extern "C" void U3CWaitAndPlaySoundsU3Ec__Iterator17_Reset_m2323 (U3CWaitAndPlaySoundsU3Ec__Iterator17_t407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
