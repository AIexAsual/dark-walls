﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<iTween/EaseType>
struct IList_1_t569;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>
struct  ReadOnlyCollection_1_t2450  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::list
	Object_t* ___list_0;
};
