﻿#pragma once
#include <stdint.h>
// UnityEngine.Networking.Match.MatchDesc[]
struct MatchDescU5BU5D_t2731;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>
struct  List_1_t975  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::_items
	MatchDescU5BU5D_t2731* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::_version
	int32_t ____version_3;
};
struct List_1_t975_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::EmptyArray
	MatchDescU5BU5D_t2731* ___EmptyArray_4;
};
