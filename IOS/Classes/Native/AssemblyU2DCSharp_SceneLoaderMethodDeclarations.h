﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SceneLoader
struct SceneLoader_t382;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;

// System.Void SceneLoader::.ctor()
extern "C" void SceneLoader__ctor_m2336 (SceneLoader_t382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneLoader::Start()
extern "C" void SceneLoader_Start_m2337 (SceneLoader_t382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneLoader::Update()
extern "C" void SceneLoader_Update_m2338 (SceneLoader_t382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneLoader::SceneChange(System.String)
extern "C" void SceneLoader_SceneChange_m2339 (SceneLoader_t382 * __this, String_t* ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SceneLoader::WaitAndLoad(System.Single)
extern "C" Object_t * SceneLoader_WaitAndLoad_m2340 (SceneLoader_t382 * __this, float ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
