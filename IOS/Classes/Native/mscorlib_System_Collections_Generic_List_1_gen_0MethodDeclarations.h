﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t510;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerable_1_t2973;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t2974;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.RaycastResult>
struct ICollection_1_t2975;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>
struct ReadOnlyCollection_1_t2251;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t2247;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t2255;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t590;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void List_1__ctor_m4606_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1__ctor_m4606(__this, method) (( void (*) (List_1_t510 *, const MethodInfo*))List_1__ctor_m4606_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m13759_gshared (List_1_t510 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m13759(__this, ___collection, method) (( void (*) (List_1_t510 *, Object_t*, const MethodInfo*))List_1__ctor_m13759_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Int32)
extern "C" void List_1__ctor_m13760_gshared (List_1_t510 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m13760(__this, ___capacity, method) (( void (*) (List_1_t510 *, int32_t, const MethodInfo*))List_1__ctor_m13760_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.cctor()
extern "C" void List_1__cctor_m13761_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m13761(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13761_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13762_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13762(__this, method) (( Object_t* (*) (List_1_t510 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13762_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m13763_gshared (List_1_t510 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m13763(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t510 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m13763_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m13764_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m13764(__this, method) (( Object_t * (*) (List_1_t510 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m13764_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m13765_gshared (List_1_t510 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m13765(__this, ___item, method) (( int32_t (*) (List_1_t510 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m13765_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m13766_gshared (List_1_t510 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m13766(__this, ___item, method) (( bool (*) (List_1_t510 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m13766_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m13767_gshared (List_1_t510 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m13767(__this, ___item, method) (( int32_t (*) (List_1_t510 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m13767_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m13768_gshared (List_1_t510 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m13768(__this, ___index, ___item, method) (( void (*) (List_1_t510 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m13768_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m13769_gshared (List_1_t510 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m13769(__this, ___item, method) (( void (*) (List_1_t510 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m13769_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13770_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13770(__this, method) (( bool (*) (List_1_t510 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13770_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m13771_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m13771(__this, method) (( bool (*) (List_1_t510 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m13771_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m13772_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m13772(__this, method) (( Object_t * (*) (List_1_t510 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m13772_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m13773_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m13773(__this, method) (( bool (*) (List_1_t510 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m13773_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m13774_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m13774(__this, method) (( bool (*) (List_1_t510 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m13774_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m13775_gshared (List_1_t510 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m13775(__this, ___index, method) (( Object_t * (*) (List_1_t510 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m13775_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m13776_gshared (List_1_t510 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m13776(__this, ___index, ___value, method) (( void (*) (List_1_t510 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m13776_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Add(T)
extern "C" void List_1_Add_m13777_gshared (List_1_t510 * __this, RaycastResult_t511  ___item, const MethodInfo* method);
#define List_1_Add_m13777(__this, ___item, method) (( void (*) (List_1_t510 *, RaycastResult_t511 , const MethodInfo*))List_1_Add_m13777_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m13778_gshared (List_1_t510 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m13778(__this, ___newCount, method) (( void (*) (List_1_t510 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13778_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m13779_gshared (List_1_t510 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m13779(__this, ___collection, method) (( void (*) (List_1_t510 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13779_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m13780_gshared (List_1_t510 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m13780(__this, ___enumerable, method) (( void (*) (List_1_t510 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13780_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m13781_gshared (List_1_t510 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m13781(__this, ___collection, method) (( void (*) (List_1_t510 *, Object_t*, const MethodInfo*))List_1_AddRange_m13781_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2251 * List_1_AsReadOnly_m13782_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m13782(__this, method) (( ReadOnlyCollection_1_t2251 * (*) (List_1_t510 *, const MethodInfo*))List_1_AsReadOnly_m13782_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Clear()
extern "C" void List_1_Clear_m13783_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1_Clear_m13783(__this, method) (( void (*) (List_1_t510 *, const MethodInfo*))List_1_Clear_m13783_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool List_1_Contains_m13784_gshared (List_1_t510 * __this, RaycastResult_t511  ___item, const MethodInfo* method);
#define List_1_Contains_m13784(__this, ___item, method) (( bool (*) (List_1_t510 *, RaycastResult_t511 , const MethodInfo*))List_1_Contains_m13784_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m13785_gshared (List_1_t510 * __this, RaycastResultU5BU5D_t2247* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m13785(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t510 *, RaycastResultU5BU5D_t2247*, int32_t, const MethodInfo*))List_1_CopyTo_m13785_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Find(System.Predicate`1<T>)
extern "C" RaycastResult_t511  List_1_Find_m13786_gshared (List_1_t510 * __this, Predicate_1_t2255 * ___match, const MethodInfo* method);
#define List_1_Find_m13786(__this, ___match, method) (( RaycastResult_t511  (*) (List_1_t510 *, Predicate_1_t2255 *, const MethodInfo*))List_1_Find_m13786_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m13787_gshared (Object_t * __this /* static, unused */, Predicate_1_t2255 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m13787(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2255 *, const MethodInfo*))List_1_CheckMatch_m13787_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m13788_gshared (List_1_t510 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2255 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m13788(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t510 *, int32_t, int32_t, Predicate_1_t2255 *, const MethodInfo*))List_1_GetIndex_m13788_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Enumerator_t2249  List_1_GetEnumerator_m13789_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m13789(__this, method) (( Enumerator_t2249  (*) (List_1_t510 *, const MethodInfo*))List_1_GetEnumerator_m13789_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m13790_gshared (List_1_t510 * __this, RaycastResult_t511  ___item, const MethodInfo* method);
#define List_1_IndexOf_m13790(__this, ___item, method) (( int32_t (*) (List_1_t510 *, RaycastResult_t511 , const MethodInfo*))List_1_IndexOf_m13790_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m13791_gshared (List_1_t510 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m13791(__this, ___start, ___delta, method) (( void (*) (List_1_t510 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13791_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m13792_gshared (List_1_t510 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m13792(__this, ___index, method) (( void (*) (List_1_t510 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13792_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m13793_gshared (List_1_t510 * __this, int32_t ___index, RaycastResult_t511  ___item, const MethodInfo* method);
#define List_1_Insert_m13793(__this, ___index, ___item, method) (( void (*) (List_1_t510 *, int32_t, RaycastResult_t511 , const MethodInfo*))List_1_Insert_m13793_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m13794_gshared (List_1_t510 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m13794(__this, ___collection, method) (( void (*) (List_1_t510 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13794_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Remove(T)
extern "C" bool List_1_Remove_m13795_gshared (List_1_t510 * __this, RaycastResult_t511  ___item, const MethodInfo* method);
#define List_1_Remove_m13795(__this, ___item, method) (( bool (*) (List_1_t510 *, RaycastResult_t511 , const MethodInfo*))List_1_Remove_m13795_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m13796_gshared (List_1_t510 * __this, Predicate_1_t2255 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m13796(__this, ___match, method) (( int32_t (*) (List_1_t510 *, Predicate_1_t2255 *, const MethodInfo*))List_1_RemoveAll_m13796_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m13797_gshared (List_1_t510 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m13797(__this, ___index, method) (( void (*) (List_1_t510 *, int32_t, const MethodInfo*))List_1_RemoveAt_m13797_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Reverse()
extern "C" void List_1_Reverse_m13798_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1_Reverse_m13798(__this, method) (( void (*) (List_1_t510 *, const MethodInfo*))List_1_Reverse_m13798_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort()
extern "C" void List_1_Sort_m13799_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1_Sort_m13799(__this, method) (( void (*) (List_1_t510 *, const MethodInfo*))List_1_Sort_m13799_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m4570_gshared (List_1_t510 * __this, Comparison_1_t590 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m4570(__this, ___comparison, method) (( void (*) (List_1_t510 *, Comparison_1_t590 *, const MethodInfo*))List_1_Sort_m4570_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::ToArray()
extern "C" RaycastResultU5BU5D_t2247* List_1_ToArray_m13800_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1_ToArray_m13800(__this, method) (( RaycastResultU5BU5D_t2247* (*) (List_1_t510 *, const MethodInfo*))List_1_ToArray_m13800_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::TrimExcess()
extern "C" void List_1_TrimExcess_m13801_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m13801(__this, method) (( void (*) (List_1_t510 *, const MethodInfo*))List_1_TrimExcess_m13801_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m13802_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m13802(__this, method) (( int32_t (*) (List_1_t510 *, const MethodInfo*))List_1_get_Capacity_m13802_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m13803_gshared (List_1_t510 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m13803(__this, ___value, method) (( void (*) (List_1_t510 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13803_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t List_1_get_Count_m13804_gshared (List_1_t510 * __this, const MethodInfo* method);
#define List_1_get_Count_m13804(__this, method) (( int32_t (*) (List_1_t510 *, const MethodInfo*))List_1_get_Count_m13804_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t511  List_1_get_Item_m13805_gshared (List_1_t510 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m13805(__this, ___index, method) (( RaycastResult_t511  (*) (List_1_t510 *, int32_t, const MethodInfo*))List_1_get_Item_m13805_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m13806_gshared (List_1_t510 * __this, int32_t ___index, RaycastResult_t511  ___value, const MethodInfo* method);
#define List_1_set_Item_m13806(__this, ___index, ___value, method) (( void (*) (List_1_t510 *, int32_t, RaycastResult_t511 , const MethodInfo*))List_1_set_Item_m13806_gshared)(__this, ___index, ___value, method)
