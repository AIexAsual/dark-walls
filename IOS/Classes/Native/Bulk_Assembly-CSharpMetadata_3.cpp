﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// AppManager
#include "AssemblyU2DCSharp_AppManager.h"
// Metadata Definition AppManager
extern TypeInfo AppManager_t350_il2cpp_TypeInfo;
// AppManager
#include "AssemblyU2DCSharp_AppManagerMethodDeclarations.h"
extern const Il2CppType OnStateChangeHandler_t349_0_0_0;
static const Il2CppType* AppManager_t350_il2cpp_TypeInfo__nestedTypes[1] =
{
	&OnStateChangeHandler_t349_0_0_0,
};
static const EncodedMethodIndex AppManager_t350_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType AppManager_t350_0_0_0;
extern const Il2CppType AppManager_t350_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct AppManager_t350;
const Il2CppTypeDefinitionMetadata AppManager_t350_DefinitionMetadata = 
{
	NULL/* declaringType */
	, AppManager_t350_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AppManager_t350_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2233/* fieldStart */
	, 2057/* methodStart */
	, 7/* eventStart */
	, 281/* propertyStart */

};
TypeInfo AppManager_t350_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppManager"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &AppManager_t350_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AppManager_t350_0_0_0/* byval_arg */
	, &AppManager_t350_1_0_0/* this_arg */
	, &AppManager_t350_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppManager_t350)/* instance_size */
	, sizeof (AppManager_t350)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AppManager_t350_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// AppManager/OnStateChangeHandler
#include "AssemblyU2DCSharp_AppManager_OnStateChangeHandler.h"
// Metadata Definition AppManager/OnStateChangeHandler
extern TypeInfo OnStateChangeHandler_t349_il2cpp_TypeInfo;
// AppManager/OnStateChangeHandler
#include "AssemblyU2DCSharp_AppManager_OnStateChangeHandlerMethodDeclarations.h"
static const EncodedMethodIndex OnStateChangeHandler_t349_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	782,
	783,
	784,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
extern const Il2CppType ISerializable_t2210_0_0_0;
static Il2CppInterfaceOffsetPair OnStateChangeHandler_t349_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType OnStateChangeHandler_t349_1_0_0;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
struct OnStateChangeHandler_t349;
const Il2CppTypeDefinitionMetadata OnStateChangeHandler_t349_DefinitionMetadata = 
{
	&AppManager_t350_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnStateChangeHandler_t349_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, OnStateChangeHandler_t349_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2065/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnStateChangeHandler_t349_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnStateChangeHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &OnStateChangeHandler_t349_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OnStateChangeHandler_t349_0_0_0/* byval_arg */
	, &OnStateChangeHandler_t349_1_0_0/* this_arg */
	, &OnStateChangeHandler_t349_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_OnStateChangeHandler_t349/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnStateChangeHandler_t349)/* instance_size */
	, sizeof (OnStateChangeHandler_t349)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Stages
#include "AssemblyU2DCSharp_Stages.h"
// Metadata Definition Stages
extern TypeInfo Stages_t351_il2cpp_TypeInfo;
// Stages
#include "AssemblyU2DCSharp_StagesMethodDeclarations.h"
static const EncodedMethodIndex Stages_t351_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Stages_t351_0_0_0;
extern const Il2CppType Stages_t351_1_0_0;
struct Stages_t351;
const Il2CppTypeDefinitionMetadata Stages_t351_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Stages_t351_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2236/* fieldStart */
	, 2069/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Stages_t351_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Stages"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Stages_t351_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Stages_t351_0_0_0/* byval_arg */
	, &Stages_t351_1_0_0/* this_arg */
	, &Stages_t351_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Stages_t351)/* instance_size */
	, sizeof (Stages_t351)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Stages_t351_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// GameManager
#include "AssemblyU2DCSharp_GameManager.h"
// Metadata Definition GameManager
extern TypeInfo GameManager_t354_il2cpp_TypeInfo;
// GameManager
#include "AssemblyU2DCSharp_GameManagerMethodDeclarations.h"
extern const Il2CppType U3CObjectSpawnerU3Ec__IteratorD_t352_0_0_0;
extern const Il2CppType U3CMyGameObjectHandlerU3Ec__IteratorE_t353_0_0_0;
static const Il2CppType* GameManager_t354_il2cpp_TypeInfo__nestedTypes[2] =
{
	&U3CObjectSpawnerU3Ec__IteratorD_t352_0_0_0,
	&U3CMyGameObjectHandlerU3Ec__IteratorE_t353_0_0_0,
};
static const EncodedMethodIndex GameManager_t354_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType GameManager_t354_0_0_0;
extern const Il2CppType GameManager_t354_1_0_0;
extern const Il2CppType MonoBehaviour_t4_0_0_0;
struct GameManager_t354;
const Il2CppTypeDefinitionMetadata GameManager_t354_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GameManager_t354_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, GameManager_t354_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2251/* fieldStart */
	, 2071/* methodStart */
	, -1/* eventStart */
	, 282/* propertyStart */

};
TypeInfo GameManager_t354_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "GameManager"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &GameManager_t354_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GameManager_t354_0_0_0/* byval_arg */
	, &GameManager_t354_1_0_0/* this_arg */
	, &GameManager_t354_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GameManager_t354)/* instance_size */
	, sizeof (GameManager_t354)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GameManager_t354_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 23/* method_count */
	, 2/* property_count */
	, 50/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// GameManager/<ObjectSpawner>c__IteratorD
#include "AssemblyU2DCSharp_GameManager_U3CObjectSpawnerU3Ec__Iterator.h"
// Metadata Definition GameManager/<ObjectSpawner>c__IteratorD
extern TypeInfo U3CObjectSpawnerU3Ec__IteratorD_t352_il2cpp_TypeInfo;
// GameManager/<ObjectSpawner>c__IteratorD
#include "AssemblyU2DCSharp_GameManager_U3CObjectSpawnerU3Ec__IteratorMethodDeclarations.h"
static const EncodedMethodIndex U3CObjectSpawnerU3Ec__IteratorD_t352_VTable[9] = 
{
	626,
	601,
	627,
	628,
	785,
	786,
	787,
	788,
	789,
};
extern const Il2CppType IEnumerator_t464_0_0_0;
extern const Il2CppType IEnumerator_1_t2280_0_0_0;
extern const Il2CppType IDisposable_t538_0_0_0;
static const Il2CppType* U3CObjectSpawnerU3Ec__IteratorD_t352_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CObjectSpawnerU3Ec__IteratorD_t352_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CObjectSpawnerU3Ec__IteratorD_t352_1_0_0;
struct U3CObjectSpawnerU3Ec__IteratorD_t352;
const Il2CppTypeDefinitionMetadata U3CObjectSpawnerU3Ec__IteratorD_t352_DefinitionMetadata = 
{
	&GameManager_t354_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CObjectSpawnerU3Ec__IteratorD_t352_InterfacesTypeInfos/* implementedInterfaces */
	, U3CObjectSpawnerU3Ec__IteratorD_t352_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CObjectSpawnerU3Ec__IteratorD_t352_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2301/* fieldStart */
	, 2094/* methodStart */
	, -1/* eventStart */
	, 284/* propertyStart */

};
TypeInfo U3CObjectSpawnerU3Ec__IteratorD_t352_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<ObjectSpawner>c__IteratorD"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CObjectSpawnerU3Ec__IteratorD_t352_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 849/* custom_attributes_cache */
	, &U3CObjectSpawnerU3Ec__IteratorD_t352_0_0_0/* byval_arg */
	, &U3CObjectSpawnerU3Ec__IteratorD_t352_1_0_0/* this_arg */
	, &U3CObjectSpawnerU3Ec__IteratorD_t352_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CObjectSpawnerU3Ec__IteratorD_t352)/* instance_size */
	, sizeof (U3CObjectSpawnerU3Ec__IteratorD_t352)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// GameManager/<MyGameObjectHandler>c__IteratorE
#include "AssemblyU2DCSharp_GameManager_U3CMyGameObjectHandlerU3Ec__It.h"
// Metadata Definition GameManager/<MyGameObjectHandler>c__IteratorE
extern TypeInfo U3CMyGameObjectHandlerU3Ec__IteratorE_t353_il2cpp_TypeInfo;
// GameManager/<MyGameObjectHandler>c__IteratorE
#include "AssemblyU2DCSharp_GameManager_U3CMyGameObjectHandlerU3Ec__ItMethodDeclarations.h"
static const EncodedMethodIndex U3CMyGameObjectHandlerU3Ec__IteratorE_t353_VTable[9] = 
{
	626,
	601,
	627,
	628,
	790,
	791,
	792,
	793,
	794,
};
static const Il2CppType* U3CMyGameObjectHandlerU3Ec__IteratorE_t353_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CMyGameObjectHandlerU3Ec__IteratorE_t353_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CMyGameObjectHandlerU3Ec__IteratorE_t353_1_0_0;
struct U3CMyGameObjectHandlerU3Ec__IteratorE_t353;
const Il2CppTypeDefinitionMetadata U3CMyGameObjectHandlerU3Ec__IteratorE_t353_DefinitionMetadata = 
{
	&GameManager_t354_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CMyGameObjectHandlerU3Ec__IteratorE_t353_InterfacesTypeInfos/* implementedInterfaces */
	, U3CMyGameObjectHandlerU3Ec__IteratorE_t353_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CMyGameObjectHandlerU3Ec__IteratorE_t353_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2308/* fieldStart */
	, 2100/* methodStart */
	, -1/* eventStart */
	, 286/* propertyStart */

};
TypeInfo U3CMyGameObjectHandlerU3Ec__IteratorE_t353_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<MyGameObjectHandler>c__IteratorE"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CMyGameObjectHandlerU3Ec__IteratorE_t353_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 854/* custom_attributes_cache */
	, &U3CMyGameObjectHandlerU3Ec__IteratorE_t353_0_0_0/* byval_arg */
	, &U3CMyGameObjectHandlerU3Ec__IteratorE_t353_1_0_0/* this_arg */
	, &U3CMyGameObjectHandlerU3Ec__IteratorE_t353_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CMyGameObjectHandlerU3Ec__IteratorE_t353)/* instance_size */
	, sizeof (U3CMyGameObjectHandlerU3Ec__IteratorE_t353)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// GazeInputEvent
#include "AssemblyU2DCSharp_GazeInputEvent.h"
// Metadata Definition GazeInputEvent
extern TypeInfo GazeInputEvent_t357_il2cpp_TypeInfo;
// GazeInputEvent
#include "AssemblyU2DCSharp_GazeInputEventMethodDeclarations.h"
static const EncodedMethodIndex GazeInputEvent_t357_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair GazeInputEvent_t357_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType GazeInputEvent_t357_0_0_0;
extern const Il2CppType GazeInputEvent_t357_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata GazeInputEvent_t357_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GazeInputEvent_t357_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, GazeInputEvent_t357_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2315/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GazeInputEvent_t357_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "GazeInputEvent"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GazeInputEvent_t357_0_0_0/* byval_arg */
	, &GazeInputEvent_t357_1_0_0/* this_arg */
	, &GazeInputEvent_t357_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GazeInputEvent_t357)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GazeInputEvent_t357)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// GazeInputManager
#include "AssemblyU2DCSharp_GazeInputManager.h"
// Metadata Definition GazeInputManager
extern TypeInfo GazeInputManager_t359_il2cpp_TypeInfo;
// GazeInputManager
#include "AssemblyU2DCSharp_GazeInputManagerMethodDeclarations.h"
extern const Il2CppType OnStateChangeHandler_t358_0_0_0;
static const Il2CppType* GazeInputManager_t359_il2cpp_TypeInfo__nestedTypes[1] =
{
	&OnStateChangeHandler_t358_0_0_0,
};
static const EncodedMethodIndex GazeInputManager_t359_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType GazeInputManager_t359_0_0_0;
extern const Il2CppType GazeInputManager_t359_1_0_0;
struct GazeInputManager_t359;
const Il2CppTypeDefinitionMetadata GazeInputManager_t359_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GazeInputManager_t359_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GazeInputManager_t359_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2319/* fieldStart */
	, 2106/* methodStart */
	, 8/* eventStart */
	, 288/* propertyStart */

};
TypeInfo GazeInputManager_t359_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "GazeInputManager"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &GazeInputManager_t359_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GazeInputManager_t359_0_0_0/* byval_arg */
	, &GazeInputManager_t359_1_0_0/* this_arg */
	, &GazeInputManager_t359_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GazeInputManager_t359)/* instance_size */
	, sizeof (GazeInputManager_t359)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GazeInputManager_t359_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// GazeInputManager/OnStateChangeHandler
#include "AssemblyU2DCSharp_GazeInputManager_OnStateChangeHandler.h"
// Metadata Definition GazeInputManager/OnStateChangeHandler
extern TypeInfo OnStateChangeHandler_t358_il2cpp_TypeInfo;
// GazeInputManager/OnStateChangeHandler
#include "AssemblyU2DCSharp_GazeInputManager_OnStateChangeHandlerMethodDeclarations.h"
static const EncodedMethodIndex OnStateChangeHandler_t358_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	795,
	796,
	797,
};
static Il2CppInterfaceOffsetPair OnStateChangeHandler_t358_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType OnStateChangeHandler_t358_1_0_0;
struct OnStateChangeHandler_t358;
const Il2CppTypeDefinitionMetadata OnStateChangeHandler_t358_DefinitionMetadata = 
{
	&GazeInputManager_t359_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnStateChangeHandler_t358_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, OnStateChangeHandler_t358_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2114/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnStateChangeHandler_t358_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnStateChangeHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &OnStateChangeHandler_t358_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OnStateChangeHandler_t358_0_0_0/* byval_arg */
	, &OnStateChangeHandler_t358_1_0_0/* this_arg */
	, &OnStateChangeHandler_t358_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_OnStateChangeHandler_t358/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnStateChangeHandler_t358)/* instance_size */
	, sizeof (OnStateChangeHandler_t358)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// InteractiveEvent
#include "AssemblyU2DCSharp_InteractiveEvent.h"
// Metadata Definition InteractiveEvent
extern TypeInfo InteractiveEvent_t360_il2cpp_TypeInfo;
// InteractiveEvent
#include "AssemblyU2DCSharp_InteractiveEventMethodDeclarations.h"
static const EncodedMethodIndex InteractiveEvent_t360_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair InteractiveEvent_t360_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType InteractiveEvent_t360_0_0_0;
extern const Il2CppType InteractiveEvent_t360_1_0_0;
const Il2CppTypeDefinitionMetadata InteractiveEvent_t360_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InteractiveEvent_t360_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, InteractiveEvent_t360_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2322/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InteractiveEvent_t360_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "InteractiveEvent"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InteractiveEvent_t360_0_0_0/* byval_arg */
	, &InteractiveEvent_t360_1_0_0/* this_arg */
	, &InteractiveEvent_t360_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InteractiveEvent_t360)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (InteractiveEvent_t360)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 45/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// InteractiveEventManager
#include "AssemblyU2DCSharp_InteractiveEventManager.h"
// Metadata Definition InteractiveEventManager
extern TypeInfo InteractiveEventManager_t362_il2cpp_TypeInfo;
// InteractiveEventManager
#include "AssemblyU2DCSharp_InteractiveEventManagerMethodDeclarations.h"
extern const Il2CppType OnStateChangeHandler_t361_0_0_0;
static const Il2CppType* InteractiveEventManager_t362_il2cpp_TypeInfo__nestedTypes[1] =
{
	&OnStateChangeHandler_t361_0_0_0,
};
static const EncodedMethodIndex InteractiveEventManager_t362_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType InteractiveEventManager_t362_0_0_0;
extern const Il2CppType InteractiveEventManager_t362_1_0_0;
struct InteractiveEventManager_t362;
const Il2CppTypeDefinitionMetadata InteractiveEventManager_t362_DefinitionMetadata = 
{
	NULL/* declaringType */
	, InteractiveEventManager_t362_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InteractiveEventManager_t362_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2367/* fieldStart */
	, 2118/* methodStart */
	, 9/* eventStart */
	, 289/* propertyStart */

};
TypeInfo InteractiveEventManager_t362_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "InteractiveEventManager"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &InteractiveEventManager_t362_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InteractiveEventManager_t362_0_0_0/* byval_arg */
	, &InteractiveEventManager_t362_1_0_0/* this_arg */
	, &InteractiveEventManager_t362_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InteractiveEventManager_t362)/* instance_size */
	, sizeof (InteractiveEventManager_t362)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(InteractiveEventManager_t362_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// InteractiveEventManager/OnStateChangeHandler
#include "AssemblyU2DCSharp_InteractiveEventManager_OnStateChangeHandl.h"
// Metadata Definition InteractiveEventManager/OnStateChangeHandler
extern TypeInfo OnStateChangeHandler_t361_il2cpp_TypeInfo;
// InteractiveEventManager/OnStateChangeHandler
#include "AssemblyU2DCSharp_InteractiveEventManager_OnStateChangeHandlMethodDeclarations.h"
static const EncodedMethodIndex OnStateChangeHandler_t361_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	798,
	799,
	800,
};
static Il2CppInterfaceOffsetPair OnStateChangeHandler_t361_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType OnStateChangeHandler_t361_1_0_0;
struct OnStateChangeHandler_t361;
const Il2CppTypeDefinitionMetadata OnStateChangeHandler_t361_DefinitionMetadata = 
{
	&InteractiveEventManager_t362_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnStateChangeHandler_t361_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, OnStateChangeHandler_t361_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2126/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnStateChangeHandler_t361_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnStateChangeHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &OnStateChangeHandler_t361_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OnStateChangeHandler_t361_0_0_0/* byval_arg */
	, &OnStateChangeHandler_t361_1_0_0/* this_arg */
	, &OnStateChangeHandler_t361_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_OnStateChangeHandler_t361/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnStateChangeHandler_t361)/* instance_size */
	, sizeof (OnStateChangeHandler_t361)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// DroppingObjects
#include "AssemblyU2DCSharp_DroppingObjects.h"
// Metadata Definition DroppingObjects
extern TypeInfo DroppingObjects_t363_il2cpp_TypeInfo;
// DroppingObjects
#include "AssemblyU2DCSharp_DroppingObjectsMethodDeclarations.h"
static const EncodedMethodIndex DroppingObjects_t363_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType DroppingObjects_t363_0_0_0;
extern const Il2CppType DroppingObjects_t363_1_0_0;
struct DroppingObjects_t363;
const Il2CppTypeDefinitionMetadata DroppingObjects_t363_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DroppingObjects_t363_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2370/* fieldStart */
	, 2130/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DroppingObjects_t363_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DroppingObjects"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DroppingObjects_t363_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DroppingObjects_t363_0_0_0/* byval_arg */
	, &DroppingObjects_t363_1_0_0/* this_arg */
	, &DroppingObjects_t363_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DroppingObjects_t363)/* instance_size */
	, sizeof (DroppingObjects_t363)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DroppingObjects_t363_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// WayPoints
#include "AssemblyU2DCSharp_WayPoints.h"
// Metadata Definition WayPoints
extern TypeInfo WayPoints_t364_il2cpp_TypeInfo;
// WayPoints
#include "AssemblyU2DCSharp_WayPointsMethodDeclarations.h"
static const EncodedMethodIndex WayPoints_t364_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType WayPoints_t364_0_0_0;
extern const Il2CppType WayPoints_t364_1_0_0;
struct WayPoints_t364;
const Il2CppTypeDefinitionMetadata WayPoints_t364_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WayPoints_t364_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2371/* fieldStart */
	, 2132/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WayPoints_t364_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WayPoints"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &WayPoints_t364_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WayPoints_t364_0_0_0/* byval_arg */
	, &WayPoints_t364_1_0_0/* this_arg */
	, &WayPoints_t364_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WayPoints_t364)/* instance_size */
	, sizeof (WayPoints_t364)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WayPoints_t364_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MamaActionTriggers
#include "AssemblyU2DCSharp_MamaActionTriggers.h"
// Metadata Definition MamaActionTriggers
extern TypeInfo MamaActionTriggers_t365_il2cpp_TypeInfo;
// MamaActionTriggers
#include "AssemblyU2DCSharp_MamaActionTriggersMethodDeclarations.h"
static const EncodedMethodIndex MamaActionTriggers_t365_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MamaActionTriggers_t365_0_0_0;
extern const Il2CppType MamaActionTriggers_t365_1_0_0;
struct MamaActionTriggers_t365;
const Il2CppTypeDefinitionMetadata MamaActionTriggers_t365_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MamaActionTriggers_t365_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2374/* fieldStart */
	, 2134/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MamaActionTriggers_t365_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MamaActionTriggers"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MamaActionTriggers_t365_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MamaActionTriggers_t365_0_0_0/* byval_arg */
	, &MamaActionTriggers_t365_1_0_0/* this_arg */
	, &MamaActionTriggers_t365_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MamaActionTriggers_t365)/* instance_size */
	, sizeof (MamaActionTriggers_t365)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MamaActionTriggers_t365_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// InteractiveObjectManager
#include "AssemblyU2DCSharp_InteractiveObjectManager.h"
// Metadata Definition InteractiveObjectManager
extern TypeInfo InteractiveObjectManager_t366_il2cpp_TypeInfo;
// InteractiveObjectManager
#include "AssemblyU2DCSharp_InteractiveObjectManagerMethodDeclarations.h"
static const EncodedMethodIndex InteractiveObjectManager_t366_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType InteractiveObjectManager_t366_0_0_0;
extern const Il2CppType InteractiveObjectManager_t366_1_0_0;
struct InteractiveObjectManager_t366;
const Il2CppTypeDefinitionMetadata InteractiveObjectManager_t366_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InteractiveObjectManager_t366_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2382/* fieldStart */
	, 2136/* methodStart */
	, -1/* eventStart */
	, 290/* propertyStart */

};
TypeInfo InteractiveObjectManager_t366_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "InteractiveObjectManager"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &InteractiveObjectManager_t366_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InteractiveObjectManager_t366_0_0_0/* byval_arg */
	, &InteractiveObjectManager_t366_1_0_0/* this_arg */
	, &InteractiveObjectManager_t366_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InteractiveObjectManager_t366)/* instance_size */
	, sizeof (InteractiveObjectManager_t366)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(InteractiveObjectManager_t366_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 1/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// myLights
#include "AssemblyU2DCSharp_myLights.h"
// Metadata Definition myLights
extern TypeInfo myLights_t369_il2cpp_TypeInfo;
// myLights
#include "AssemblyU2DCSharp_myLightsMethodDeclarations.h"
static const EncodedMethodIndex myLights_t369_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType myLights_t369_0_0_0;
extern const Il2CppType myLights_t369_1_0_0;
struct myLights_t369;
const Il2CppTypeDefinitionMetadata myLights_t369_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, myLights_t369_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2396/* fieldStart */
	, 2164/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo myLights_t369_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "myLights"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &myLights_t369_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &myLights_t369_0_0_0/* byval_arg */
	, &myLights_t369_1_0_0/* this_arg */
	, &myLights_t369_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (myLights_t369)/* instance_size */
	, sizeof (myLights_t369)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(myLights_t369_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// LightManager
#include "AssemblyU2DCSharp_LightManager.h"
// Metadata Definition LightManager
extern TypeInfo LightManager_t370_il2cpp_TypeInfo;
// LightManager
#include "AssemblyU2DCSharp_LightManagerMethodDeclarations.h"
static const EncodedMethodIndex LightManager_t370_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType LightManager_t370_0_0_0;
extern const Il2CppType LightManager_t370_1_0_0;
struct LightManager_t370;
const Il2CppTypeDefinitionMetadata LightManager_t370_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LightManager_t370_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2405/* fieldStart */
	, 2166/* methodStart */
	, -1/* eventStart */
	, 291/* propertyStart */

};
TypeInfo LightManager_t370_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "LightManager"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &LightManager_t370_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LightManager_t370_0_0_0/* byval_arg */
	, &LightManager_t370_1_0_0/* this_arg */
	, &LightManager_t370_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LightManager_t370)/* instance_size */
	, sizeof (LightManager_t370)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(LightManager_t370_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Doors
#include "AssemblyU2DCSharp_Doors.h"
// Metadata Definition Doors
extern TypeInfo Doors_t371_il2cpp_TypeInfo;
// Doors
#include "AssemblyU2DCSharp_DoorsMethodDeclarations.h"
static const EncodedMethodIndex Doors_t371_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Doors_t371_0_0_0;
extern const Il2CppType Doors_t371_1_0_0;
struct Doors_t371;
const Il2CppTypeDefinitionMetadata Doors_t371_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Doors_t371_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2409/* fieldStart */
	, 2180/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Doors_t371_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Doors"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Doors_t371_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Doors_t371_0_0_0/* byval_arg */
	, &Doors_t371_1_0_0/* this_arg */
	, &Doors_t371_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Doors_t371)/* instance_size */
	, sizeof (Doors_t371)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Doors_t371_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MovingGameObjects
#include "AssemblyU2DCSharp_MovingGameObjects.h"
// Metadata Definition MovingGameObjects
extern TypeInfo MovingGameObjects_t372_il2cpp_TypeInfo;
// MovingGameObjects
#include "AssemblyU2DCSharp_MovingGameObjectsMethodDeclarations.h"
static const EncodedMethodIndex MovingGameObjects_t372_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MovingGameObjects_t372_0_0_0;
extern const Il2CppType MovingGameObjects_t372_1_0_0;
struct MovingGameObjects_t372;
const Il2CppTypeDefinitionMetadata MovingGameObjects_t372_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MovingGameObjects_t372_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2422/* fieldStart */
	, 2182/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MovingGameObjects_t372_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MovingGameObjects"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MovingGameObjects_t372_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MovingGameObjects_t372_0_0_0/* byval_arg */
	, &MovingGameObjects_t372_1_0_0/* this_arg */
	, &MovingGameObjects_t372_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MovingGameObjects_t372)/* instance_size */
	, sizeof (MovingGameObjects_t372)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MovingGameObjects_t372_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// WardObjects
#include "AssemblyU2DCSharp_WardObjects.h"
// Metadata Definition WardObjects
extern TypeInfo WardObjects_t373_il2cpp_TypeInfo;
// WardObjects
#include "AssemblyU2DCSharp_WardObjectsMethodDeclarations.h"
static const EncodedMethodIndex WardObjects_t373_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType WardObjects_t373_0_0_0;
extern const Il2CppType WardObjects_t373_1_0_0;
struct WardObjects_t373;
const Il2CppTypeDefinitionMetadata WardObjects_t373_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WardObjects_t373_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2424/* fieldStart */
	, 2184/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WardObjects_t373_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WardObjects"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &WardObjects_t373_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WardObjects_t373_0_0_0/* byval_arg */
	, &WardObjects_t373_1_0_0/* this_arg */
	, &WardObjects_t373_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WardObjects_t373)/* instance_size */
	, sizeof (WardObjects_t373)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WardObjects_t373_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MovingObjectManager
#include "AssemblyU2DCSharp_MovingObjectManager.h"
// Metadata Definition MovingObjectManager
extern TypeInfo MovingObjectManager_t374_il2cpp_TypeInfo;
// MovingObjectManager
#include "AssemblyU2DCSharp_MovingObjectManagerMethodDeclarations.h"
static const EncodedMethodIndex MovingObjectManager_t374_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MovingObjectManager_t374_0_0_0;
extern const Il2CppType MovingObjectManager_t374_1_0_0;
struct MovingObjectManager_t374;
const Il2CppTypeDefinitionMetadata MovingObjectManager_t374_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MovingObjectManager_t374_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2431/* fieldStart */
	, 2186/* methodStart */
	, -1/* eventStart */
	, 292/* propertyStart */

};
TypeInfo MovingObjectManager_t374_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MovingObjectManager"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MovingObjectManager_t374_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MovingObjectManager_t374_0_0_0/* byval_arg */
	, &MovingObjectManager_t374_1_0_0/* this_arg */
	, &MovingObjectManager_t374_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MovingObjectManager_t374)/* instance_size */
	, sizeof (MovingObjectManager_t374)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MovingObjectManager_t374_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// PlayerEvent
#include "AssemblyU2DCSharp_PlayerEvent.h"
// Metadata Definition PlayerEvent
extern TypeInfo PlayerEvent_t376_il2cpp_TypeInfo;
// PlayerEvent
#include "AssemblyU2DCSharp_PlayerEventMethodDeclarations.h"
static const EncodedMethodIndex PlayerEvent_t376_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair PlayerEvent_t376_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType PlayerEvent_t376_0_0_0;
extern const Il2CppType PlayerEvent_t376_1_0_0;
const Il2CppTypeDefinitionMetadata PlayerEvent_t376_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PlayerEvent_t376_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, PlayerEvent_t376_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2434/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PlayerEvent_t376_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "PlayerEvent"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PlayerEvent_t376_0_0_0/* byval_arg */
	, &PlayerEvent_t376_1_0_0/* this_arg */
	, &PlayerEvent_t376_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PlayerEvent_t376)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PlayerEvent_t376)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// PlayerEventManager
#include "AssemblyU2DCSharp_PlayerEventManager.h"
// Metadata Definition PlayerEventManager
extern TypeInfo PlayerEventManager_t378_il2cpp_TypeInfo;
// PlayerEventManager
#include "AssemblyU2DCSharp_PlayerEventManagerMethodDeclarations.h"
extern const Il2CppType OnStateChangeHandler_t377_0_0_0;
static const Il2CppType* PlayerEventManager_t378_il2cpp_TypeInfo__nestedTypes[1] =
{
	&OnStateChangeHandler_t377_0_0_0,
};
static const EncodedMethodIndex PlayerEventManager_t378_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType PlayerEventManager_t378_0_0_0;
extern const Il2CppType PlayerEventManager_t378_1_0_0;
struct PlayerEventManager_t378;
const Il2CppTypeDefinitionMetadata PlayerEventManager_t378_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PlayerEventManager_t378_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PlayerEventManager_t378_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2438/* fieldStart */
	, 2196/* methodStart */
	, 10/* eventStart */
	, 293/* propertyStart */

};
TypeInfo PlayerEventManager_t378_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "PlayerEventManager"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &PlayerEventManager_t378_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PlayerEventManager_t378_0_0_0/* byval_arg */
	, &PlayerEventManager_t378_1_0_0/* this_arg */
	, &PlayerEventManager_t378_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PlayerEventManager_t378)/* instance_size */
	, sizeof (PlayerEventManager_t378)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PlayerEventManager_t378_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// PlayerEventManager/OnStateChangeHandler
#include "AssemblyU2DCSharp_PlayerEventManager_OnStateChangeHandler.h"
// Metadata Definition PlayerEventManager/OnStateChangeHandler
extern TypeInfo OnStateChangeHandler_t377_il2cpp_TypeInfo;
// PlayerEventManager/OnStateChangeHandler
#include "AssemblyU2DCSharp_PlayerEventManager_OnStateChangeHandlerMethodDeclarations.h"
static const EncodedMethodIndex OnStateChangeHandler_t377_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	801,
	802,
	803,
};
static Il2CppInterfaceOffsetPair OnStateChangeHandler_t377_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType OnStateChangeHandler_t377_1_0_0;
struct OnStateChangeHandler_t377;
const Il2CppTypeDefinitionMetadata OnStateChangeHandler_t377_DefinitionMetadata = 
{
	&PlayerEventManager_t378_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnStateChangeHandler_t377_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, OnStateChangeHandler_t377_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2204/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnStateChangeHandler_t377_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnStateChangeHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &OnStateChangeHandler_t377_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OnStateChangeHandler_t377_0_0_0/* byval_arg */
	, &OnStateChangeHandler_t377_1_0_0/* this_arg */
	, &OnStateChangeHandler_t377_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_OnStateChangeHandler_t377/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnStateChangeHandler_t377)/* instance_size */
	, sizeof (OnStateChangeHandler_t377)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// SceneEvent
#include "AssemblyU2DCSharp_SceneEvent.h"
// Metadata Definition SceneEvent
extern TypeInfo SceneEvent_t379_il2cpp_TypeInfo;
// SceneEvent
#include "AssemblyU2DCSharp_SceneEventMethodDeclarations.h"
static const EncodedMethodIndex SceneEvent_t379_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair SceneEvent_t379_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SceneEvent_t379_0_0_0;
extern const Il2CppType SceneEvent_t379_1_0_0;
const Il2CppTypeDefinitionMetadata SceneEvent_t379_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SceneEvent_t379_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, SceneEvent_t379_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2441/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SceneEvent_t379_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SceneEvent"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SceneEvent_t379_0_0_0/* byval_arg */
	, &SceneEvent_t379_1_0_0/* this_arg */
	, &SceneEvent_t379_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SceneEvent_t379)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SceneEvent_t379)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SceneManager
#include "AssemblyU2DCSharp_SceneManager.h"
// Metadata Definition SceneManager
extern TypeInfo SceneManager_t381_il2cpp_TypeInfo;
// SceneManager
#include "AssemblyU2DCSharp_SceneManagerMethodDeclarations.h"
extern const Il2CppType OnStateChangeHandler_t380_0_0_0;
static const Il2CppType* SceneManager_t381_il2cpp_TypeInfo__nestedTypes[1] =
{
	&OnStateChangeHandler_t380_0_0_0,
};
static const EncodedMethodIndex SceneManager_t381_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SceneManager_t381_0_0_0;
extern const Il2CppType SceneManager_t381_1_0_0;
struct SceneManager_t381;
const Il2CppTypeDefinitionMetadata SceneManager_t381_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SceneManager_t381_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SceneManager_t381_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2444/* fieldStart */
	, 2208/* methodStart */
	, 11/* eventStart */
	, 294/* propertyStart */

};
TypeInfo SceneManager_t381_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SceneManager"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SceneManager_t381_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SceneManager_t381_0_0_0/* byval_arg */
	, &SceneManager_t381_1_0_0/* this_arg */
	, &SceneManager_t381_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SceneManager_t381)/* instance_size */
	, sizeof (SceneManager_t381)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SceneManager_t381_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SceneManager/OnStateChangeHandler
#include "AssemblyU2DCSharp_SceneManager_OnStateChangeHandler.h"
// Metadata Definition SceneManager/OnStateChangeHandler
extern TypeInfo OnStateChangeHandler_t380_il2cpp_TypeInfo;
// SceneManager/OnStateChangeHandler
#include "AssemblyU2DCSharp_SceneManager_OnStateChangeHandlerMethodDeclarations.h"
static const EncodedMethodIndex OnStateChangeHandler_t380_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	804,
	805,
	806,
};
static Il2CppInterfaceOffsetPair OnStateChangeHandler_t380_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType OnStateChangeHandler_t380_1_0_0;
struct OnStateChangeHandler_t380;
const Il2CppTypeDefinitionMetadata OnStateChangeHandler_t380_DefinitionMetadata = 
{
	&SceneManager_t381_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnStateChangeHandler_t380_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, OnStateChangeHandler_t380_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2217/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnStateChangeHandler_t380_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnStateChangeHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &OnStateChangeHandler_t380_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OnStateChangeHandler_t380_0_0_0/* byval_arg */
	, &OnStateChangeHandler_t380_1_0_0/* this_arg */
	, &OnStateChangeHandler_t380_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_OnStateChangeHandler_t380/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnStateChangeHandler_t380)/* instance_size */
	, sizeof (OnStateChangeHandler_t380)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// ObjectSFX
#include "AssemblyU2DCSharp_ObjectSFX.h"
// Metadata Definition ObjectSFX
extern TypeInfo ObjectSFX_t383_il2cpp_TypeInfo;
// ObjectSFX
#include "AssemblyU2DCSharp_ObjectSFXMethodDeclarations.h"
static const EncodedMethodIndex ObjectSFX_t383_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ObjectSFX_t383_0_0_0;
extern const Il2CppType ObjectSFX_t383_1_0_0;
struct ObjectSFX_t383;
const Il2CppTypeDefinitionMetadata ObjectSFX_t383_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectSFX_t383_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2448/* fieldStart */
	, 2221/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ObjectSFX_t383_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectSFX"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ObjectSFX_t383_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectSFX_t383_0_0_0/* byval_arg */
	, &ObjectSFX_t383_1_0_0/* this_arg */
	, &ObjectSFX_t383_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectSFX_t383)/* instance_size */
	, sizeof (ObjectSFX_t383)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ObjectSFX_t383_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 50/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SoundManager
#include "AssemblyU2DCSharp_SoundManager.h"
// Metadata Definition SoundManager
extern TypeInfo SoundManager_t384_il2cpp_TypeInfo;
// SoundManager
#include "AssemblyU2DCSharp_SoundManagerMethodDeclarations.h"
static const EncodedMethodIndex SoundManager_t384_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SoundManager_t384_0_0_0;
extern const Il2CppType SoundManager_t384_1_0_0;
struct SoundManager_t384;
const Il2CppTypeDefinitionMetadata SoundManager_t384_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SoundManager_t384_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2498/* fieldStart */
	, 2223/* methodStart */
	, -1/* eventStart */
	, 295/* propertyStart */

};
TypeInfo SoundManager_t384_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SoundManager"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SoundManager_t384_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SoundManager_t384_0_0_0/* byval_arg */
	, &SoundManager_t384_1_0_0/* this_arg */
	, &SoundManager_t384_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SoundManager_t384)/* instance_size */
	, sizeof (SoundManager_t384)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SoundManager_t384_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// testClass
#include "AssemblyU2DCSharp_testClass.h"
// Metadata Definition testClass
extern TypeInfo testClass_t386_il2cpp_TypeInfo;
// testClass
#include "AssemblyU2DCSharp_testClassMethodDeclarations.h"
static const EncodedMethodIndex testClass_t386_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair testClass_t386_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType testClass_t386_0_0_0;
extern const Il2CppType testClass_t386_1_0_0;
const Il2CppTypeDefinitionMetadata testClass_t386_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, testClass_t386_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, testClass_t386_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2500/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo testClass_t386_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "testClass"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &testClass_t386_0_0_0/* byval_arg */
	, &testClass_t386_1_0_0/* this_arg */
	, &testClass_t386_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (testClass_t386)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (testClass_t386)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// TestEventManager
#include "AssemblyU2DCSharp_TestEventManager.h"
// Metadata Definition TestEventManager
extern TypeInfo TestEventManager_t388_il2cpp_TypeInfo;
// TestEventManager
#include "AssemblyU2DCSharp_TestEventManagerMethodDeclarations.h"
extern const Il2CppType OnStateChangeHandler_t387_0_0_0;
static const Il2CppType* TestEventManager_t388_il2cpp_TypeInfo__nestedTypes[1] =
{
	&OnStateChangeHandler_t387_0_0_0,
};
static const EncodedMethodIndex TestEventManager_t388_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType TestEventManager_t388_0_0_0;
extern const Il2CppType TestEventManager_t388_1_0_0;
struct TestEventManager_t388;
const Il2CppTypeDefinitionMetadata TestEventManager_t388_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TestEventManager_t388_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TestEventManager_t388_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2503/* fieldStart */
	, 2228/* methodStart */
	, 12/* eventStart */
	, 296/* propertyStart */

};
TypeInfo TestEventManager_t388_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TestEventManager"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &TestEventManager_t388_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TestEventManager_t388_0_0_0/* byval_arg */
	, &TestEventManager_t388_1_0_0/* this_arg */
	, &TestEventManager_t388_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TestEventManager_t388)/* instance_size */
	, sizeof (TestEventManager_t388)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TestEventManager_t388_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// TestEventManager/OnStateChangeHandler
#include "AssemblyU2DCSharp_TestEventManager_OnStateChangeHandler.h"
// Metadata Definition TestEventManager/OnStateChangeHandler
extern TypeInfo OnStateChangeHandler_t387_il2cpp_TypeInfo;
// TestEventManager/OnStateChangeHandler
#include "AssemblyU2DCSharp_TestEventManager_OnStateChangeHandlerMethodDeclarations.h"
static const EncodedMethodIndex OnStateChangeHandler_t387_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	807,
	808,
	809,
};
static Il2CppInterfaceOffsetPair OnStateChangeHandler_t387_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType OnStateChangeHandler_t387_1_0_0;
struct OnStateChangeHandler_t387;
const Il2CppTypeDefinitionMetadata OnStateChangeHandler_t387_DefinitionMetadata = 
{
	&TestEventManager_t388_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnStateChangeHandler_t387_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, OnStateChangeHandler_t387_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2236/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnStateChangeHandler_t387_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnStateChangeHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &OnStateChangeHandler_t387_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OnStateChangeHandler_t387_0_0_0/* byval_arg */
	, &OnStateChangeHandler_t387_1_0_0/* this_arg */
	, &OnStateChangeHandler_t387_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_OnStateChangeHandler_t387/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnStateChangeHandler_t387)/* instance_size */
	, sizeof (OnStateChangeHandler_t387)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// MovingObjects
#include "AssemblyU2DCSharp_MovingObjects.h"
// Metadata Definition MovingObjects
extern TypeInfo MovingObjects_t375_il2cpp_TypeInfo;
// MovingObjects
#include "AssemblyU2DCSharp_MovingObjectsMethodDeclarations.h"
extern const Il2CppType U3CWaitAndSpawnHereU3Ec__IteratorF_t389_0_0_0;
static const Il2CppType* MovingObjects_t375_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CWaitAndSpawnHereU3Ec__IteratorF_t389_0_0_0,
};
static const EncodedMethodIndex MovingObjects_t375_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType MovingObjects_t375_0_0_0;
extern const Il2CppType MovingObjects_t375_1_0_0;
struct MovingObjects_t375;
const Il2CppTypeDefinitionMetadata MovingObjects_t375_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MovingObjects_t375_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, MovingObjects_t375_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2506/* fieldStart */
	, 2240/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MovingObjects_t375_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MovingObjects"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MovingObjects_t375_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MovingObjects_t375_0_0_0/* byval_arg */
	, &MovingObjects_t375_1_0_0/* this_arg */
	, &MovingObjects_t375_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MovingObjects_t375)/* instance_size */
	, sizeof (MovingObjects_t375)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// MovingObjects/<WaitAndSpawnHere>c__IteratorF
#include "AssemblyU2DCSharp_MovingObjects_U3CWaitAndSpawnHereU3Ec__Ite.h"
// Metadata Definition MovingObjects/<WaitAndSpawnHere>c__IteratorF
extern TypeInfo U3CWaitAndSpawnHereU3Ec__IteratorF_t389_il2cpp_TypeInfo;
// MovingObjects/<WaitAndSpawnHere>c__IteratorF
#include "AssemblyU2DCSharp_MovingObjects_U3CWaitAndSpawnHereU3Ec__IteMethodDeclarations.h"
static const EncodedMethodIndex U3CWaitAndSpawnHereU3Ec__IteratorF_t389_VTable[9] = 
{
	626,
	601,
	627,
	628,
	810,
	811,
	812,
	813,
	814,
};
static const Il2CppType* U3CWaitAndSpawnHereU3Ec__IteratorF_t389_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitAndSpawnHereU3Ec__IteratorF_t389_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CWaitAndSpawnHereU3Ec__IteratorF_t389_1_0_0;
struct U3CWaitAndSpawnHereU3Ec__IteratorF_t389;
const Il2CppTypeDefinitionMetadata U3CWaitAndSpawnHereU3Ec__IteratorF_t389_DefinitionMetadata = 
{
	&MovingObjects_t375_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitAndSpawnHereU3Ec__IteratorF_t389_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitAndSpawnHereU3Ec__IteratorF_t389_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitAndSpawnHereU3Ec__IteratorF_t389_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2510/* fieldStart */
	, 2248/* methodStart */
	, -1/* eventStart */
	, 297/* propertyStart */

};
TypeInfo U3CWaitAndSpawnHereU3Ec__IteratorF_t389_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitAndSpawnHere>c__IteratorF"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CWaitAndSpawnHereU3Ec__IteratorF_t389_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 860/* custom_attributes_cache */
	, &U3CWaitAndSpawnHereU3Ec__IteratorF_t389_0_0_0/* byval_arg */
	, &U3CWaitAndSpawnHereU3Ec__IteratorF_t389_1_0_0/* this_arg */
	, &U3CWaitAndSpawnHereU3Ec__IteratorF_t389_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitAndSpawnHereU3Ec__IteratorF_t389)/* instance_size */
	, sizeof (U3CWaitAndSpawnHereU3Ec__IteratorF_t389)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// PlayerController
#include "AssemblyU2DCSharp_PlayerController.h"
// Metadata Definition PlayerController
extern TypeInfo PlayerController_t356_il2cpp_TypeInfo;
// PlayerController
#include "AssemblyU2DCSharp_PlayerControllerMethodDeclarations.h"
extern const Il2CppType U3CWaitAndStartPosU3Ec__Iterator10_t390_0_0_0;
extern const Il2CppType U3CWaitAndMoveToU3Ec__Iterator11_t391_0_0_0;
extern const Il2CppType U3CStandUpU3Ec__Iterator12_t392_0_0_0;
extern const Il2CppType U3CWaitAndGetFlashLightU3Ec__Iterator13_t393_0_0_0;
extern const Il2CppType U3CWaitAndTalkU3Ec__Iterator14_t394_0_0_0;
extern const Il2CppType U3CWaitAndSpaceU3Ec__Iterator15_t395_0_0_0;
extern const Il2CppType U3CWaitAndRestartAppU3Ec__Iterator16_t396_0_0_0;
static const Il2CppType* PlayerController_t356_il2cpp_TypeInfo__nestedTypes[7] =
{
	&U3CWaitAndStartPosU3Ec__Iterator10_t390_0_0_0,
	&U3CWaitAndMoveToU3Ec__Iterator11_t391_0_0_0,
	&U3CStandUpU3Ec__Iterator12_t392_0_0_0,
	&U3CWaitAndGetFlashLightU3Ec__Iterator13_t393_0_0_0,
	&U3CWaitAndTalkU3Ec__Iterator14_t394_0_0_0,
	&U3CWaitAndSpaceU3Ec__Iterator15_t395_0_0_0,
	&U3CWaitAndRestartAppU3Ec__Iterator16_t396_0_0_0,
};
static const EncodedMethodIndex PlayerController_t356_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType PlayerController_t356_0_0_0;
extern const Il2CppType PlayerController_t356_1_0_0;
struct PlayerController_t356;
const Il2CppTypeDefinitionMetadata PlayerController_t356_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PlayerController_t356_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, PlayerController_t356_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2517/* fieldStart */
	, 2254/* methodStart */
	, -1/* eventStart */
	, 299/* propertyStart */

};
TypeInfo PlayerController_t356_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "PlayerController"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &PlayerController_t356_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PlayerController_t356_0_0_0/* byval_arg */
	, &PlayerController_t356_1_0_0/* this_arg */
	, &PlayerController_t356_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PlayerController_t356)/* instance_size */
	, sizeof (PlayerController_t356)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PlayerController_t356_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 26/* method_count */
	, 1/* property_count */
	, 33/* field_count */
	, 0/* event_count */
	, 7/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// PlayerController/<WaitAndStartPos>c__Iterator10
#include "AssemblyU2DCSharp_PlayerController_U3CWaitAndStartPosU3Ec__I.h"
// Metadata Definition PlayerController/<WaitAndStartPos>c__Iterator10
extern TypeInfo U3CWaitAndStartPosU3Ec__Iterator10_t390_il2cpp_TypeInfo;
// PlayerController/<WaitAndStartPos>c__Iterator10
#include "AssemblyU2DCSharp_PlayerController_U3CWaitAndStartPosU3Ec__IMethodDeclarations.h"
static const EncodedMethodIndex U3CWaitAndStartPosU3Ec__Iterator10_t390_VTable[9] = 
{
	626,
	601,
	627,
	628,
	815,
	816,
	817,
	818,
	819,
};
static const Il2CppType* U3CWaitAndStartPosU3Ec__Iterator10_t390_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitAndStartPosU3Ec__Iterator10_t390_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CWaitAndStartPosU3Ec__Iterator10_t390_1_0_0;
struct U3CWaitAndStartPosU3Ec__Iterator10_t390;
const Il2CppTypeDefinitionMetadata U3CWaitAndStartPosU3Ec__Iterator10_t390_DefinitionMetadata = 
{
	&PlayerController_t356_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitAndStartPosU3Ec__Iterator10_t390_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitAndStartPosU3Ec__Iterator10_t390_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitAndStartPosU3Ec__Iterator10_t390_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2550/* fieldStart */
	, 2280/* methodStart */
	, -1/* eventStart */
	, 300/* propertyStart */

};
TypeInfo U3CWaitAndStartPosU3Ec__Iterator10_t390_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitAndStartPos>c__Iterator10"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CWaitAndStartPosU3Ec__Iterator10_t390_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 872/* custom_attributes_cache */
	, &U3CWaitAndStartPosU3Ec__Iterator10_t390_0_0_0/* byval_arg */
	, &U3CWaitAndStartPosU3Ec__Iterator10_t390_1_0_0/* this_arg */
	, &U3CWaitAndStartPosU3Ec__Iterator10_t390_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitAndStartPosU3Ec__Iterator10_t390)/* instance_size */
	, sizeof (U3CWaitAndStartPosU3Ec__Iterator10_t390)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// PlayerController/<WaitAndMoveTo>c__Iterator11
#include "AssemblyU2DCSharp_PlayerController_U3CWaitAndMoveToU3Ec__Ite.h"
// Metadata Definition PlayerController/<WaitAndMoveTo>c__Iterator11
extern TypeInfo U3CWaitAndMoveToU3Ec__Iterator11_t391_il2cpp_TypeInfo;
// PlayerController/<WaitAndMoveTo>c__Iterator11
#include "AssemblyU2DCSharp_PlayerController_U3CWaitAndMoveToU3Ec__IteMethodDeclarations.h"
static const EncodedMethodIndex U3CWaitAndMoveToU3Ec__Iterator11_t391_VTable[9] = 
{
	626,
	601,
	627,
	628,
	820,
	821,
	822,
	823,
	824,
};
static const Il2CppType* U3CWaitAndMoveToU3Ec__Iterator11_t391_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitAndMoveToU3Ec__Iterator11_t391_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CWaitAndMoveToU3Ec__Iterator11_t391_1_0_0;
struct U3CWaitAndMoveToU3Ec__Iterator11_t391;
const Il2CppTypeDefinitionMetadata U3CWaitAndMoveToU3Ec__Iterator11_t391_DefinitionMetadata = 
{
	&PlayerController_t356_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitAndMoveToU3Ec__Iterator11_t391_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitAndMoveToU3Ec__Iterator11_t391_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitAndMoveToU3Ec__Iterator11_t391_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2555/* fieldStart */
	, 2286/* methodStart */
	, -1/* eventStart */
	, 302/* propertyStart */

};
TypeInfo U3CWaitAndMoveToU3Ec__Iterator11_t391_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitAndMoveTo>c__Iterator11"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CWaitAndMoveToU3Ec__Iterator11_t391_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 877/* custom_attributes_cache */
	, &U3CWaitAndMoveToU3Ec__Iterator11_t391_0_0_0/* byval_arg */
	, &U3CWaitAndMoveToU3Ec__Iterator11_t391_1_0_0/* this_arg */
	, &U3CWaitAndMoveToU3Ec__Iterator11_t391_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitAndMoveToU3Ec__Iterator11_t391)/* instance_size */
	, sizeof (U3CWaitAndMoveToU3Ec__Iterator11_t391)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// PlayerController/<StandUp>c__Iterator12
#include "AssemblyU2DCSharp_PlayerController_U3CStandUpU3Ec__Iterator1.h"
// Metadata Definition PlayerController/<StandUp>c__Iterator12
extern TypeInfo U3CStandUpU3Ec__Iterator12_t392_il2cpp_TypeInfo;
// PlayerController/<StandUp>c__Iterator12
#include "AssemblyU2DCSharp_PlayerController_U3CStandUpU3Ec__Iterator1MethodDeclarations.h"
static const EncodedMethodIndex U3CStandUpU3Ec__Iterator12_t392_VTable[9] = 
{
	626,
	601,
	627,
	628,
	825,
	826,
	827,
	828,
	829,
};
static const Il2CppType* U3CStandUpU3Ec__Iterator12_t392_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CStandUpU3Ec__Iterator12_t392_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CStandUpU3Ec__Iterator12_t392_1_0_0;
struct U3CStandUpU3Ec__Iterator12_t392;
const Il2CppTypeDefinitionMetadata U3CStandUpU3Ec__Iterator12_t392_DefinitionMetadata = 
{
	&PlayerController_t356_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CStandUpU3Ec__Iterator12_t392_InterfacesTypeInfos/* implementedInterfaces */
	, U3CStandUpU3Ec__Iterator12_t392_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CStandUpU3Ec__Iterator12_t392_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2562/* fieldStart */
	, 2292/* methodStart */
	, -1/* eventStart */
	, 304/* propertyStart */

};
TypeInfo U3CStandUpU3Ec__Iterator12_t392_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<StandUp>c__Iterator12"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CStandUpU3Ec__Iterator12_t392_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 882/* custom_attributes_cache */
	, &U3CStandUpU3Ec__Iterator12_t392_0_0_0/* byval_arg */
	, &U3CStandUpU3Ec__Iterator12_t392_1_0_0/* this_arg */
	, &U3CStandUpU3Ec__Iterator12_t392_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CStandUpU3Ec__Iterator12_t392)/* instance_size */
	, sizeof (U3CStandUpU3Ec__Iterator12_t392)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// PlayerController/<WaitAndGetFlashLight>c__Iterator13
#include "AssemblyU2DCSharp_PlayerController_U3CWaitAndGetFlashLightU3.h"
// Metadata Definition PlayerController/<WaitAndGetFlashLight>c__Iterator13
extern TypeInfo U3CWaitAndGetFlashLightU3Ec__Iterator13_t393_il2cpp_TypeInfo;
// PlayerController/<WaitAndGetFlashLight>c__Iterator13
#include "AssemblyU2DCSharp_PlayerController_U3CWaitAndGetFlashLightU3MethodDeclarations.h"
static const EncodedMethodIndex U3CWaitAndGetFlashLightU3Ec__Iterator13_t393_VTable[9] = 
{
	626,
	601,
	627,
	628,
	830,
	831,
	832,
	833,
	834,
};
static const Il2CppType* U3CWaitAndGetFlashLightU3Ec__Iterator13_t393_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitAndGetFlashLightU3Ec__Iterator13_t393_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CWaitAndGetFlashLightU3Ec__Iterator13_t393_1_0_0;
struct U3CWaitAndGetFlashLightU3Ec__Iterator13_t393;
const Il2CppTypeDefinitionMetadata U3CWaitAndGetFlashLightU3Ec__Iterator13_t393_DefinitionMetadata = 
{
	&PlayerController_t356_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitAndGetFlashLightU3Ec__Iterator13_t393_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitAndGetFlashLightU3Ec__Iterator13_t393_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitAndGetFlashLightU3Ec__Iterator13_t393_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2567/* fieldStart */
	, 2298/* methodStart */
	, -1/* eventStart */
	, 306/* propertyStart */

};
TypeInfo U3CWaitAndGetFlashLightU3Ec__Iterator13_t393_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitAndGetFlashLight>c__Iterator13"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CWaitAndGetFlashLightU3Ec__Iterator13_t393_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 887/* custom_attributes_cache */
	, &U3CWaitAndGetFlashLightU3Ec__Iterator13_t393_0_0_0/* byval_arg */
	, &U3CWaitAndGetFlashLightU3Ec__Iterator13_t393_1_0_0/* this_arg */
	, &U3CWaitAndGetFlashLightU3Ec__Iterator13_t393_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitAndGetFlashLightU3Ec__Iterator13_t393)/* instance_size */
	, sizeof (U3CWaitAndGetFlashLightU3Ec__Iterator13_t393)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// PlayerController/<WaitAndTalk>c__Iterator14
#include "AssemblyU2DCSharp_PlayerController_U3CWaitAndTalkU3Ec__Itera.h"
// Metadata Definition PlayerController/<WaitAndTalk>c__Iterator14
extern TypeInfo U3CWaitAndTalkU3Ec__Iterator14_t394_il2cpp_TypeInfo;
// PlayerController/<WaitAndTalk>c__Iterator14
#include "AssemblyU2DCSharp_PlayerController_U3CWaitAndTalkU3Ec__IteraMethodDeclarations.h"
static const EncodedMethodIndex U3CWaitAndTalkU3Ec__Iterator14_t394_VTable[9] = 
{
	626,
	601,
	627,
	628,
	835,
	836,
	837,
	838,
	839,
};
static const Il2CppType* U3CWaitAndTalkU3Ec__Iterator14_t394_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitAndTalkU3Ec__Iterator14_t394_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CWaitAndTalkU3Ec__Iterator14_t394_1_0_0;
struct U3CWaitAndTalkU3Ec__Iterator14_t394;
const Il2CppTypeDefinitionMetadata U3CWaitAndTalkU3Ec__Iterator14_t394_DefinitionMetadata = 
{
	&PlayerController_t356_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitAndTalkU3Ec__Iterator14_t394_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitAndTalkU3Ec__Iterator14_t394_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitAndTalkU3Ec__Iterator14_t394_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2574/* fieldStart */
	, 2304/* methodStart */
	, -1/* eventStart */
	, 308/* propertyStart */

};
TypeInfo U3CWaitAndTalkU3Ec__Iterator14_t394_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitAndTalk>c__Iterator14"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CWaitAndTalkU3Ec__Iterator14_t394_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 892/* custom_attributes_cache */
	, &U3CWaitAndTalkU3Ec__Iterator14_t394_0_0_0/* byval_arg */
	, &U3CWaitAndTalkU3Ec__Iterator14_t394_1_0_0/* this_arg */
	, &U3CWaitAndTalkU3Ec__Iterator14_t394_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitAndTalkU3Ec__Iterator14_t394)/* instance_size */
	, sizeof (U3CWaitAndTalkU3Ec__Iterator14_t394)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// PlayerController/<WaitAndSpace>c__Iterator15
#include "AssemblyU2DCSharp_PlayerController_U3CWaitAndSpaceU3Ec__Iter.h"
// Metadata Definition PlayerController/<WaitAndSpace>c__Iterator15
extern TypeInfo U3CWaitAndSpaceU3Ec__Iterator15_t395_il2cpp_TypeInfo;
// PlayerController/<WaitAndSpace>c__Iterator15
#include "AssemblyU2DCSharp_PlayerController_U3CWaitAndSpaceU3Ec__IterMethodDeclarations.h"
static const EncodedMethodIndex U3CWaitAndSpaceU3Ec__Iterator15_t395_VTable[9] = 
{
	626,
	601,
	627,
	628,
	840,
	841,
	842,
	843,
	844,
};
static const Il2CppType* U3CWaitAndSpaceU3Ec__Iterator15_t395_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitAndSpaceU3Ec__Iterator15_t395_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CWaitAndSpaceU3Ec__Iterator15_t395_1_0_0;
struct U3CWaitAndSpaceU3Ec__Iterator15_t395;
const Il2CppTypeDefinitionMetadata U3CWaitAndSpaceU3Ec__Iterator15_t395_DefinitionMetadata = 
{
	&PlayerController_t356_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitAndSpaceU3Ec__Iterator15_t395_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitAndSpaceU3Ec__Iterator15_t395_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitAndSpaceU3Ec__Iterator15_t395_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2581/* fieldStart */
	, 2310/* methodStart */
	, -1/* eventStart */
	, 310/* propertyStart */

};
TypeInfo U3CWaitAndSpaceU3Ec__Iterator15_t395_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitAndSpace>c__Iterator15"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CWaitAndSpaceU3Ec__Iterator15_t395_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 897/* custom_attributes_cache */
	, &U3CWaitAndSpaceU3Ec__Iterator15_t395_0_0_0/* byval_arg */
	, &U3CWaitAndSpaceU3Ec__Iterator15_t395_1_0_0/* this_arg */
	, &U3CWaitAndSpaceU3Ec__Iterator15_t395_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitAndSpaceU3Ec__Iterator15_t395)/* instance_size */
	, sizeof (U3CWaitAndSpaceU3Ec__Iterator15_t395)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// PlayerController/<WaitAndRestartApp>c__Iterator16
#include "AssemblyU2DCSharp_PlayerController_U3CWaitAndRestartAppU3Ec_.h"
// Metadata Definition PlayerController/<WaitAndRestartApp>c__Iterator16
extern TypeInfo U3CWaitAndRestartAppU3Ec__Iterator16_t396_il2cpp_TypeInfo;
// PlayerController/<WaitAndRestartApp>c__Iterator16
#include "AssemblyU2DCSharp_PlayerController_U3CWaitAndRestartAppU3Ec_MethodDeclarations.h"
static const EncodedMethodIndex U3CWaitAndRestartAppU3Ec__Iterator16_t396_VTable[9] = 
{
	626,
	601,
	627,
	628,
	845,
	846,
	847,
	848,
	849,
};
static const Il2CppType* U3CWaitAndRestartAppU3Ec__Iterator16_t396_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitAndRestartAppU3Ec__Iterator16_t396_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CWaitAndRestartAppU3Ec__Iterator16_t396_1_0_0;
struct U3CWaitAndRestartAppU3Ec__Iterator16_t396;
const Il2CppTypeDefinitionMetadata U3CWaitAndRestartAppU3Ec__Iterator16_t396_DefinitionMetadata = 
{
	&PlayerController_t356_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitAndRestartAppU3Ec__Iterator16_t396_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitAndRestartAppU3Ec__Iterator16_t396_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitAndRestartAppU3Ec__Iterator16_t396_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2586/* fieldStart */
	, 2316/* methodStart */
	, -1/* eventStart */
	, 312/* propertyStart */

};
TypeInfo U3CWaitAndRestartAppU3Ec__Iterator16_t396_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitAndRestartApp>c__Iterator16"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CWaitAndRestartAppU3Ec__Iterator16_t396_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 902/* custom_attributes_cache */
	, &U3CWaitAndRestartAppU3Ec__Iterator16_t396_0_0_0/* byval_arg */
	, &U3CWaitAndRestartAppU3Ec__Iterator16_t396_1_0_0/* this_arg */
	, &U3CWaitAndRestartAppU3Ec__Iterator16_t396_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitAndRestartAppU3Ec__Iterator16_t396)/* instance_size */
	, sizeof (U3CWaitAndRestartAppU3Ec__Iterator16_t396)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// RagdollHelper
#include "AssemblyU2DCSharp_RagdollHelper.h"
// Metadata Definition RagdollHelper
extern TypeInfo RagdollHelper_t402_il2cpp_TypeInfo;
// RagdollHelper
#include "AssemblyU2DCSharp_RagdollHelperMethodDeclarations.h"
extern const Il2CppType RagdollState_t399_0_0_0;
extern const Il2CppType BodyPart_t400_0_0_0;
static const Il2CppType* RagdollHelper_t402_il2cpp_TypeInfo__nestedTypes[2] =
{
	&RagdollState_t399_0_0_0,
	&BodyPart_t400_0_0_0,
};
static const EncodedMethodIndex RagdollHelper_t402_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType RagdollHelper_t402_0_0_0;
extern const Il2CppType RagdollHelper_t402_1_0_0;
struct RagdollHelper_t402;
const Il2CppTypeDefinitionMetadata RagdollHelper_t402_DefinitionMetadata = 
{
	NULL/* declaringType */
	, RagdollHelper_t402_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, RagdollHelper_t402_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2590/* fieldStart */
	, 2322/* methodStart */
	, -1/* eventStart */
	, 314/* propertyStart */

};
TypeInfo RagdollHelper_t402_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "RagdollHelper"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &RagdollHelper_t402_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RagdollHelper_t402_0_0_0/* byval_arg */
	, &RagdollHelper_t402_1_0_0/* this_arg */
	, &RagdollHelper_t402_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RagdollHelper_t402)/* instance_size */
	, sizeof (RagdollHelper_t402)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// RagdollHelper/RagdollState
#include "AssemblyU2DCSharp_RagdollHelper_RagdollState.h"
// Metadata Definition RagdollHelper/RagdollState
extern TypeInfo RagdollState_t399_il2cpp_TypeInfo;
// RagdollHelper/RagdollState
#include "AssemblyU2DCSharp_RagdollHelper_RagdollStateMethodDeclarations.h"
static const EncodedMethodIndex RagdollState_t399_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair RagdollState_t399_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType RagdollState_t399_1_0_0;
const Il2CppTypeDefinitionMetadata RagdollState_t399_DefinitionMetadata = 
{
	&RagdollHelper_t402_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RagdollState_t399_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, RagdollState_t399_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2599/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RagdollState_t399_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "RagdollState"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RagdollState_t399_0_0_0/* byval_arg */
	, &RagdollState_t399_1_0_0/* this_arg */
	, &RagdollState_t399_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RagdollState_t399)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RagdollState_t399)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// RagdollHelper/BodyPart
#include "AssemblyU2DCSharp_RagdollHelper_BodyPart.h"
// Metadata Definition RagdollHelper/BodyPart
extern TypeInfo BodyPart_t400_il2cpp_TypeInfo;
// RagdollHelper/BodyPart
#include "AssemblyU2DCSharp_RagdollHelper_BodyPartMethodDeclarations.h"
static const EncodedMethodIndex BodyPart_t400_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType BodyPart_t400_1_0_0;
struct BodyPart_t400;
const Il2CppTypeDefinitionMetadata BodyPart_t400_DefinitionMetadata = 
{
	&RagdollHelper_t402_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BodyPart_t400_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2603/* fieldStart */
	, 2329/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BodyPart_t400_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "BodyPart"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &BodyPart_t400_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BodyPart_t400_0_0_0/* byval_arg */
	, &BodyPart_t400_1_0_0/* this_arg */
	, &BodyPart_t400_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BodyPart_t400)/* instance_size */
	, sizeof (BodyPart_t400)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// RagdollPartScript
#include "AssemblyU2DCSharp_RagdollPartScript.h"
// Metadata Definition RagdollPartScript
extern TypeInfo RagdollPartScript_t404_il2cpp_TypeInfo;
// RagdollPartScript
#include "AssemblyU2DCSharp_RagdollPartScriptMethodDeclarations.h"
static const EncodedMethodIndex RagdollPartScript_t404_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType RagdollPartScript_t404_0_0_0;
extern const Il2CppType RagdollPartScript_t404_1_0_0;
struct RagdollPartScript_t404;
const Il2CppTypeDefinitionMetadata RagdollPartScript_t404_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, RagdollPartScript_t404_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2606/* fieldStart */
	, 2330/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RagdollPartScript_t404_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "RagdollPartScript"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &RagdollPartScript_t404_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RagdollPartScript_t404_0_0_0/* byval_arg */
	, &RagdollPartScript_t404_1_0_0/* this_arg */
	, &RagdollPartScript_t404_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RagdollPartScript_t404)/* instance_size */
	, sizeof (RagdollPartScript_t404)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// RagdollScript
#include "AssemblyU2DCSharp_RagdollScript.h"
// Metadata Definition RagdollScript
extern TypeInfo RagdollScript_t405_il2cpp_TypeInfo;
// RagdollScript
#include "AssemblyU2DCSharp_RagdollScriptMethodDeclarations.h"
static const EncodedMethodIndex RagdollScript_t405_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType RagdollScript_t405_0_0_0;
extern const Il2CppType RagdollScript_t405_1_0_0;
struct RagdollScript_t405;
const Il2CppTypeDefinitionMetadata RagdollScript_t405_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, RagdollScript_t405_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2334/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RagdollScript_t405_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "RagdollScript"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &RagdollScript_t405_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RagdollScript_t405_0_0_0/* byval_arg */
	, &RagdollScript_t405_1_0_0/* this_arg */
	, &RagdollScript_t405_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RagdollScript_t405)/* instance_size */
	, sizeof (RagdollScript_t405)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Reticle
#include "AssemblyU2DCSharp_Reticle.h"
// Metadata Definition Reticle
extern TypeInfo Reticle_t406_il2cpp_TypeInfo;
// Reticle
#include "AssemblyU2DCSharp_ReticleMethodDeclarations.h"
static const EncodedMethodIndex Reticle_t406_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Reticle_t406_0_0_0;
extern const Il2CppType Reticle_t406_1_0_0;
struct Reticle_t406;
const Il2CppTypeDefinitionMetadata Reticle_t406_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, Reticle_t406_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2607/* fieldStart */
	, 2339/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Reticle_t406_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Reticle"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Reticle_t406_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Reticle_t406_0_0_0/* byval_arg */
	, &Reticle_t406_1_0_0/* this_arg */
	, &Reticle_t406_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Reticle_t406)/* instance_size */
	, sizeof (Reticle_t406)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SFX
#include "AssemblyU2DCSharp_SFX.h"
// Metadata Definition SFX
extern TypeInfo SFX_t385_il2cpp_TypeInfo;
// SFX
#include "AssemblyU2DCSharp_SFXMethodDeclarations.h"
extern const Il2CppType U3CWaitAndPlaySoundsU3Ec__Iterator17_t407_0_0_0;
static const Il2CppType* SFX_t385_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CWaitAndPlaySoundsU3Ec__Iterator17_t407_0_0_0,
};
static const EncodedMethodIndex SFX_t385_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SFX_t385_0_0_0;
extern const Il2CppType SFX_t385_1_0_0;
struct SFX_t385;
const Il2CppTypeDefinitionMetadata SFX_t385_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SFX_t385_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, SFX_t385_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2611/* fieldStart */
	, 2342/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SFX_t385_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SFX"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SFX_t385_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SFX_t385_0_0_0/* byval_arg */
	, &SFX_t385_1_0_0/* this_arg */
	, &SFX_t385_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SFX_t385)/* instance_size */
	, sizeof (SFX_t385)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SFX/<WaitAndPlaySounds>c__Iterator17
#include "AssemblyU2DCSharp_SFX_U3CWaitAndPlaySoundsU3Ec__Iterator17.h"
// Metadata Definition SFX/<WaitAndPlaySounds>c__Iterator17
extern TypeInfo U3CWaitAndPlaySoundsU3Ec__Iterator17_t407_il2cpp_TypeInfo;
// SFX/<WaitAndPlaySounds>c__Iterator17
#include "AssemblyU2DCSharp_SFX_U3CWaitAndPlaySoundsU3Ec__Iterator17MethodDeclarations.h"
static const EncodedMethodIndex U3CWaitAndPlaySoundsU3Ec__Iterator17_t407_VTable[9] = 
{
	626,
	601,
	627,
	628,
	850,
	851,
	852,
	853,
	854,
};
static const Il2CppType* U3CWaitAndPlaySoundsU3Ec__Iterator17_t407_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitAndPlaySoundsU3Ec__Iterator17_t407_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CWaitAndPlaySoundsU3Ec__Iterator17_t407_1_0_0;
struct U3CWaitAndPlaySoundsU3Ec__Iterator17_t407;
const Il2CppTypeDefinitionMetadata U3CWaitAndPlaySoundsU3Ec__Iterator17_t407_DefinitionMetadata = 
{
	&SFX_t385_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitAndPlaySoundsU3Ec__Iterator17_t407_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitAndPlaySoundsU3Ec__Iterator17_t407_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitAndPlaySoundsU3Ec__Iterator17_t407_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2612/* fieldStart */
	, 2348/* methodStart */
	, -1/* eventStart */
	, 315/* propertyStart */

};
TypeInfo U3CWaitAndPlaySoundsU3Ec__Iterator17_t407_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitAndPlaySounds>c__Iterator17"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CWaitAndPlaySoundsU3Ec__Iterator17_t407_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 908/* custom_attributes_cache */
	, &U3CWaitAndPlaySoundsU3Ec__Iterator17_t407_0_0_0/* byval_arg */
	, &U3CWaitAndPlaySoundsU3Ec__Iterator17_t407_1_0_0/* this_arg */
	, &U3CWaitAndPlaySoundsU3Ec__Iterator17_t407_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitAndPlaySoundsU3Ec__Iterator17_t407)/* instance_size */
	, sizeof (U3CWaitAndPlaySoundsU3Ec__Iterator17_t407)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SceneLoader
#include "AssemblyU2DCSharp_SceneLoader.h"
// Metadata Definition SceneLoader
extern TypeInfo SceneLoader_t382_il2cpp_TypeInfo;
// SceneLoader
#include "AssemblyU2DCSharp_SceneLoaderMethodDeclarations.h"
extern const Il2CppType U3CWaitAndLoadU3Ec__Iterator18_t408_0_0_0;
static const Il2CppType* SceneLoader_t382_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CWaitAndLoadU3Ec__Iterator18_t408_0_0_0,
};
static const EncodedMethodIndex SceneLoader_t382_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType SceneLoader_t382_0_0_0;
extern const Il2CppType SceneLoader_t382_1_0_0;
struct SceneLoader_t382;
const Il2CppTypeDefinitionMetadata SceneLoader_t382_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SceneLoader_t382_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, SceneLoader_t382_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2617/* fieldStart */
	, 2354/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SceneLoader_t382_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SceneLoader"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SceneLoader_t382_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SceneLoader_t382_0_0_0/* byval_arg */
	, &SceneLoader_t382_1_0_0/* this_arg */
	, &SceneLoader_t382_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SceneLoader_t382)/* instance_size */
	, sizeof (SceneLoader_t382)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SceneLoader/<WaitAndLoad>c__Iterator18
#include "AssemblyU2DCSharp_SceneLoader_U3CWaitAndLoadU3Ec__Iterator18.h"
// Metadata Definition SceneLoader/<WaitAndLoad>c__Iterator18
extern TypeInfo U3CWaitAndLoadU3Ec__Iterator18_t408_il2cpp_TypeInfo;
// SceneLoader/<WaitAndLoad>c__Iterator18
#include "AssemblyU2DCSharp_SceneLoader_U3CWaitAndLoadU3Ec__Iterator18MethodDeclarations.h"
static const EncodedMethodIndex U3CWaitAndLoadU3Ec__Iterator18_t408_VTable[9] = 
{
	626,
	601,
	627,
	628,
	855,
	856,
	857,
	858,
	859,
};
static const Il2CppType* U3CWaitAndLoadU3Ec__Iterator18_t408_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitAndLoadU3Ec__Iterator18_t408_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CWaitAndLoadU3Ec__Iterator18_t408_1_0_0;
struct U3CWaitAndLoadU3Ec__Iterator18_t408;
const Il2CppTypeDefinitionMetadata U3CWaitAndLoadU3Ec__Iterator18_t408_DefinitionMetadata = 
{
	&SceneLoader_t382_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitAndLoadU3Ec__Iterator18_t408_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitAndLoadU3Ec__Iterator18_t408_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitAndLoadU3Ec__Iterator18_t408_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2618/* fieldStart */
	, 2359/* methodStart */
	, -1/* eventStart */
	, 317/* propertyStart */

};
TypeInfo U3CWaitAndLoadU3Ec__Iterator18_t408_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitAndLoad>c__Iterator18"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CWaitAndLoadU3Ec__Iterator18_t408_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 914/* custom_attributes_cache */
	, &U3CWaitAndLoadU3Ec__Iterator18_t408_0_0_0/* byval_arg */
	, &U3CWaitAndLoadU3Ec__Iterator18_t408_1_0_0/* this_arg */
	, &U3CWaitAndLoadU3Ec__Iterator18_t408_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitAndLoadU3Ec__Iterator18_t408)/* instance_size */
	, sizeof (U3CWaitAndLoadU3Ec__Iterator18_t408)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// ScoreTextScript
#include "AssemblyU2DCSharp_ScoreTextScript.h"
// Metadata Definition ScoreTextScript
extern TypeInfo ScoreTextScript_t409_il2cpp_TypeInfo;
// ScoreTextScript
#include "AssemblyU2DCSharp_ScoreTextScriptMethodDeclarations.h"
static const EncodedMethodIndex ScoreTextScript_t409_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ScoreTextScript_t409_0_0_0;
extern const Il2CppType ScoreTextScript_t409_1_0_0;
struct ScoreTextScript_t409;
const Il2CppTypeDefinitionMetadata ScoreTextScript_t409_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, ScoreTextScript_t409_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2623/* fieldStart */
	, 2365/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ScoreTextScript_t409_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScoreTextScript"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ScoreTextScript_t409_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScoreTextScript_t409_0_0_0/* byval_arg */
	, &ScoreTextScript_t409_1_0_0/* this_arg */
	, &ScoreTextScript_t409_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScoreTextScript_t409)/* instance_size */
	, sizeof (ScoreTextScript_t409)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// StairDismount
#include "AssemblyU2DCSharp_StairDismount.h"
// Metadata Definition StairDismount
extern TypeInfo StairDismount_t403_il2cpp_TypeInfo;
// StairDismount
#include "AssemblyU2DCSharp_StairDismountMethodDeclarations.h"
static const EncodedMethodIndex StairDismount_t403_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType StairDismount_t403_0_0_0;
extern const Il2CppType StairDismount_t403_1_0_0;
struct StairDismount_t403;
const Il2CppTypeDefinitionMetadata StairDismount_t403_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, StairDismount_t403_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2625/* fieldStart */
	, 2368/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StairDismount_t403_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "StairDismount"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &StairDismount_t403_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StairDismount_t403_0_0_0/* byval_arg */
	, &StairDismount_t403_1_0_0/* this_arg */
	, &StairDismount_t403_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StairDismount_t403)/* instance_size */
	, sizeof (StairDismount_t403)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Teddy
#include "AssemblyU2DCSharp_Teddy.h"
// Metadata Definition Teddy
extern TypeInfo Teddy_t367_il2cpp_TypeInfo;
// Teddy
#include "AssemblyU2DCSharp_TeddyMethodDeclarations.h"
static const EncodedMethodIndex Teddy_t367_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Teddy_t367_0_0_0;
extern const Il2CppType Teddy_t367_1_0_0;
struct Teddy_t367;
const Il2CppTypeDefinitionMetadata Teddy_t367_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &RagdollScript_t405_0_0_0/* parent */
	, Teddy_t367_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2630/* fieldStart */
	, 2371/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Teddy_t367_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Teddy"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Teddy_t367_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Teddy_t367_0_0_0/* byval_arg */
	, &Teddy_t367_1_0_0/* this_arg */
	, &Teddy_t367_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Teddy_t367)/* instance_size */
	, sizeof (Teddy_t367)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// TextAppearance
#include "AssemblyU2DCSharp_TextAppearance.h"
// Metadata Definition TextAppearance
extern TypeInfo TextAppearance_t368_il2cpp_TypeInfo;
// TextAppearance
#include "AssemblyU2DCSharp_TextAppearanceMethodDeclarations.h"
static const EncodedMethodIndex TextAppearance_t368_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType TextAppearance_t368_0_0_0;
extern const Il2CppType TextAppearance_t368_1_0_0;
struct TextAppearance_t368;
const Il2CppTypeDefinitionMetadata TextAppearance_t368_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, TextAppearance_t368_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2631/* fieldStart */
	, 2376/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TextAppearance_t368_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextAppearance"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &TextAppearance_t368_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextAppearance_t368_0_0_0/* byval_arg */
	, &TextAppearance_t368_1_0_0/* this_arg */
	, &TextAppearance_t368_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextAppearance_t368)/* instance_size */
	, sizeof (TextAppearance_t368)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter
#include "AssemblyU2DCSharp_UnitySampleAssets_Characters_ThirdPerson_T_2.h"
// Metadata Definition UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter
extern TypeInfo ThirdPersonCharacter_t413_il2cpp_TypeInfo;
// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter
#include "AssemblyU2DCSharp_UnitySampleAssets_Characters_ThirdPerson_T_2MethodDeclarations.h"
extern const Il2CppType AdvancedSettings_t411_0_0_0;
extern const Il2CppType RayHitComparer_t412_0_0_0;
extern const Il2CppType U3CBlendLookWeightU3Ec__Iterator19_t414_0_0_0;
static const Il2CppType* ThirdPersonCharacter_t413_il2cpp_TypeInfo__nestedTypes[3] =
{
	&AdvancedSettings_t411_0_0_0,
	&RayHitComparer_t412_0_0_0,
	&U3CBlendLookWeightU3Ec__Iterator19_t414_0_0_0,
};
static const EncodedMethodIndex ThirdPersonCharacter_t413_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ThirdPersonCharacter_t413_0_0_0;
extern const Il2CppType ThirdPersonCharacter_t413_1_0_0;
struct ThirdPersonCharacter_t413;
const Il2CppTypeDefinitionMetadata ThirdPersonCharacter_t413_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ThirdPersonCharacter_t413_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, ThirdPersonCharacter_t413_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2632/* fieldStart */
	, 2382/* methodStart */
	, -1/* eventStart */
	, 319/* propertyStart */

};
TypeInfo ThirdPersonCharacter_t413_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThirdPersonCharacter"/* name */
	, "UnitySampleAssets.Characters.ThirdPerson"/* namespaze */
	, NULL/* methods */
	, &ThirdPersonCharacter_t413_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ThirdPersonCharacter_t413_0_0_0/* byval_arg */
	, &ThirdPersonCharacter_t413_1_0_0/* this_arg */
	, &ThirdPersonCharacter_t413_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThirdPersonCharacter_t413)/* instance_size */
	, sizeof (ThirdPersonCharacter_t413)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 1/* property_count */
	, 26/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings
#include "AssemblyU2DCSharp_UnitySampleAssets_Characters_ThirdPerson_T.h"
// Metadata Definition UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings
extern TypeInfo AdvancedSettings_t411_il2cpp_TypeInfo;
// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings
#include "AssemblyU2DCSharp_UnitySampleAssets_Characters_ThirdPerson_TMethodDeclarations.h"
static const EncodedMethodIndex AdvancedSettings_t411_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType AdvancedSettings_t411_1_0_0;
struct AdvancedSettings_t411;
const Il2CppTypeDefinitionMetadata AdvancedSettings_t411_DefinitionMetadata = 
{
	&ThirdPersonCharacter_t413_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AdvancedSettings_t411_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2658/* fieldStart */
	, 2403/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AdvancedSettings_t411_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "AdvancedSettings"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &AdvancedSettings_t411_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AdvancedSettings_t411_0_0_0/* byval_arg */
	, &AdvancedSettings_t411_1_0_0/* this_arg */
	, &AdvancedSettings_t411_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AdvancedSettings_t411)/* instance_size */
	, sizeof (AdvancedSettings_t411)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/RayHitComparer
#include "AssemblyU2DCSharp_UnitySampleAssets_Characters_ThirdPerson_T_0.h"
// Metadata Definition UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/RayHitComparer
extern TypeInfo RayHitComparer_t412_il2cpp_TypeInfo;
// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/RayHitComparer
#include "AssemblyU2DCSharp_UnitySampleAssets_Characters_ThirdPerson_T_0MethodDeclarations.h"
static const EncodedMethodIndex RayHitComparer_t412_VTable[5] = 
{
	626,
	601,
	627,
	628,
	860,
};
extern const Il2CppType IComparer_t416_0_0_0;
static const Il2CppType* RayHitComparer_t412_InterfacesTypeInfos[] = 
{
	&IComparer_t416_0_0_0,
};
static Il2CppInterfaceOffsetPair RayHitComparer_t412_InterfacesOffsets[] = 
{
	{ &IComparer_t416_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType RayHitComparer_t412_1_0_0;
struct RayHitComparer_t412;
const Il2CppTypeDefinitionMetadata RayHitComparer_t412_DefinitionMetadata = 
{
	&ThirdPersonCharacter_t413_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, RayHitComparer_t412_InterfacesTypeInfos/* implementedInterfaces */
	, RayHitComparer_t412_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RayHitComparer_t412_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2404/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RayHitComparer_t412_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "RayHitComparer"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &RayHitComparer_t412_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RayHitComparer_t412_0_0_0/* byval_arg */
	, &RayHitComparer_t412_1_0_0/* this_arg */
	, &RayHitComparer_t412_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RayHitComparer_t412)/* instance_size */
	, sizeof (RayHitComparer_t412)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/<BlendLookWeight>c__Iterator19
#include "AssemblyU2DCSharp_UnitySampleAssets_Characters_ThirdPerson_T_1.h"
// Metadata Definition UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/<BlendLookWeight>c__Iterator19
extern TypeInfo U3CBlendLookWeightU3Ec__Iterator19_t414_il2cpp_TypeInfo;
// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/<BlendLookWeight>c__Iterator19
#include "AssemblyU2DCSharp_UnitySampleAssets_Characters_ThirdPerson_T_1MethodDeclarations.h"
static const EncodedMethodIndex U3CBlendLookWeightU3Ec__Iterator19_t414_VTable[9] = 
{
	626,
	601,
	627,
	628,
	861,
	862,
	863,
	864,
	865,
};
static const Il2CppType* U3CBlendLookWeightU3Ec__Iterator19_t414_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CBlendLookWeightU3Ec__Iterator19_t414_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CBlendLookWeightU3Ec__Iterator19_t414_1_0_0;
struct U3CBlendLookWeightU3Ec__Iterator19_t414;
const Il2CppTypeDefinitionMetadata U3CBlendLookWeightU3Ec__Iterator19_t414_DefinitionMetadata = 
{
	&ThirdPersonCharacter_t413_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CBlendLookWeightU3Ec__Iterator19_t414_InterfacesTypeInfos/* implementedInterfaces */
	, U3CBlendLookWeightU3Ec__Iterator19_t414_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CBlendLookWeightU3Ec__Iterator19_t414_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2670/* fieldStart */
	, 2406/* methodStart */
	, -1/* eventStart */
	, 320/* propertyStart */

};
TypeInfo U3CBlendLookWeightU3Ec__Iterator19_t414_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<BlendLookWeight>c__Iterator19"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CBlendLookWeightU3Ec__Iterator19_t414_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 930/* custom_attributes_cache */
	, &U3CBlendLookWeightU3Ec__Iterator19_t414_0_0_0/* byval_arg */
	, &U3CBlendLookWeightU3Ec__Iterator19_t414_1_0_0/* this_arg */
	, &U3CBlendLookWeightU3Ec__Iterator19_t414_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CBlendLookWeightU3Ec__Iterator19_t414)/* instance_size */
	, sizeof (U3CBlendLookWeightU3Ec__Iterator19_t414)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// goHere
#include "AssemblyU2DCSharp_goHere.h"
// Metadata Definition goHere
extern TypeInfo goHere_t417_il2cpp_TypeInfo;
// goHere
#include "AssemblyU2DCSharp_goHereMethodDeclarations.h"
static const EncodedMethodIndex goHere_t417_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType goHere_t417_0_0_0;
extern const Il2CppType goHere_t417_1_0_0;
struct goHere_t417;
const Il2CppTypeDefinitionMetadata goHere_t417_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, goHere_t417_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2674/* fieldStart */
	, 2412/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo goHere_t417_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "goHere"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &goHere_t417_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &goHere_t417_0_0_0/* byval_arg */
	, &goHere_t417_1_0_0/* this_arg */
	, &goHere_t417_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (goHere_t417)/* instance_size */
	, sizeof (goHere_t417)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// testScript
#include "AssemblyU2DCSharp_testScript.h"
// Metadata Definition testScript
extern TypeInfo testScript_t418_il2cpp_TypeInfo;
// testScript
#include "AssemblyU2DCSharp_testScriptMethodDeclarations.h"
static const EncodedMethodIndex testScript_t418_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType testScript_t418_0_0_0;
extern const Il2CppType testScript_t418_1_0_0;
struct testScript_t418;
const Il2CppTypeDefinitionMetadata testScript_t418_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, testScript_t418_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2676/* fieldStart */
	, 2416/* methodStart */
	, -1/* eventStart */
	, 322/* propertyStart */

};
TypeInfo testScript_t418_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "testScript"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &testScript_t418_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &testScript_t418_0_0_0/* byval_arg */
	, &testScript_t418_1_0_0/* this_arg */
	, &testScript_t418_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (testScript_t418)/* instance_size */
	, sizeof (testScript_t418)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(testScript_t418_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// StartAndStopTween
#include "AssemblyU2DCSharp_StartAndStopTween.h"
// Metadata Definition StartAndStopTween
extern TypeInfo StartAndStopTween_t419_il2cpp_TypeInfo;
// StartAndStopTween
#include "AssemblyU2DCSharp_StartAndStopTweenMethodDeclarations.h"
static const EncodedMethodIndex StartAndStopTween_t419_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType StartAndStopTween_t419_0_0_0;
extern const Il2CppType StartAndStopTween_t419_1_0_0;
struct StartAndStopTween_t419;
const Il2CppTypeDefinitionMetadata StartAndStopTween_t419_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, StartAndStopTween_t419_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2677/* fieldStart */
	, 2420/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StartAndStopTween_t419_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "StartAndStopTween"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &StartAndStopTween_t419_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StartAndStopTween_t419_0_0_0/* byval_arg */
	, &StartAndStopTween_t419_1_0_0/* this_arg */
	, &StartAndStopTween_t419_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StartAndStopTween_t419)/* instance_size */
	, sizeof (StartAndStopTween_t419)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// EventParamMappings
#include "AssemblyU2DCSharp_EventParamMappings.h"
// Metadata Definition EventParamMappings
extern TypeInfo EventParamMappings_t421_il2cpp_TypeInfo;
// EventParamMappings
#include "AssemblyU2DCSharp_EventParamMappingsMethodDeclarations.h"
static const EncodedMethodIndex EventParamMappings_t421_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType EventParamMappings_t421_0_0_0;
extern const Il2CppType EventParamMappings_t421_1_0_0;
struct EventParamMappings_t421;
const Il2CppTypeDefinitionMetadata EventParamMappings_t421_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EventParamMappings_t421_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2678/* fieldStart */
	, 2422/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EventParamMappings_t421_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventParamMappings"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &EventParamMappings_t421_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EventParamMappings_t421_0_0_0/* byval_arg */
	, &EventParamMappings_t421_1_0_0/* this_arg */
	, &EventParamMappings_t421_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventParamMappings_t421)/* instance_size */
	, sizeof (EventParamMappings_t421)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(EventParamMappings_t421_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vector3OrTransform
#include "AssemblyU2DCSharp_Vector3OrTransform.h"
// Metadata Definition Vector3OrTransform
extern TypeInfo Vector3OrTransform_t422_il2cpp_TypeInfo;
// Vector3OrTransform
#include "AssemblyU2DCSharp_Vector3OrTransformMethodDeclarations.h"
static const EncodedMethodIndex Vector3OrTransform_t422_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Vector3OrTransform_t422_0_0_0;
extern const Il2CppType Vector3OrTransform_t422_1_0_0;
struct Vector3OrTransform_t422;
const Il2CppTypeDefinitionMetadata Vector3OrTransform_t422_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Vector3OrTransform_t422_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2679/* fieldStart */
	, 2424/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Vector3OrTransform_t422_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector3OrTransform"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Vector3OrTransform_t422_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Vector3OrTransform_t422_0_0_0/* byval_arg */
	, &Vector3OrTransform_t422_1_0_0/* this_arg */
	, &Vector3OrTransform_t422_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector3OrTransform_t422)/* instance_size */
	, sizeof (Vector3OrTransform_t422)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Vector3OrTransform_t422_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vector3OrTransformArray
#include "AssemblyU2DCSharp_Vector3OrTransformArray.h"
// Metadata Definition Vector3OrTransformArray
extern TypeInfo Vector3OrTransformArray_t424_il2cpp_TypeInfo;
// Vector3OrTransformArray
#include "AssemblyU2DCSharp_Vector3OrTransformArrayMethodDeclarations.h"
static const EncodedMethodIndex Vector3OrTransformArray_t424_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Vector3OrTransformArray_t424_0_0_0;
extern const Il2CppType Vector3OrTransformArray_t424_1_0_0;
struct Vector3OrTransformArray_t424;
const Il2CppTypeDefinitionMetadata Vector3OrTransformArray_t424_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Vector3OrTransformArray_t424_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2685/* fieldStart */
	, 2426/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Vector3OrTransformArray_t424_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector3OrTransformArray"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Vector3OrTransformArray_t424_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Vector3OrTransformArray_t424_0_0_0/* byval_arg */
	, &Vector3OrTransformArray_t424_1_0_0/* this_arg */
	, &Vector3OrTransformArray_t424_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector3OrTransformArray_t424)/* instance_size */
	, sizeof (Vector3OrTransformArray_t424)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Vector3OrTransformArray_t424_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// iTween
#include "AssemblyU2DCSharp_iTween.h"
// Metadata Definition iTween
extern TypeInfo iTween_t432_il2cpp_TypeInfo;
// iTween
#include "AssemblyU2DCSharp_iTweenMethodDeclarations.h"
extern const Il2CppType EaseType_t425_0_0_0;
extern const Il2CppType LoopType_t426_0_0_0;
extern const Il2CppType NamedValueColor_t427_0_0_0;
extern const Il2CppType Defaults_t428_0_0_0;
extern const Il2CppType CRSpline_t429_0_0_0;
extern const Il2CppType EasingFunction_t430_0_0_0;
extern const Il2CppType ApplyTween_t431_0_0_0;
extern const Il2CppType U3CTweenDelayU3Ec__Iterator1A_t433_0_0_0;
extern const Il2CppType U3CTweenRestartU3Ec__Iterator1B_t434_0_0_0;
extern const Il2CppType U3CStartU3Ec__Iterator1C_t435_0_0_0;
static const Il2CppType* iTween_t432_il2cpp_TypeInfo__nestedTypes[10] =
{
	&EaseType_t425_0_0_0,
	&LoopType_t426_0_0_0,
	&NamedValueColor_t427_0_0_0,
	&Defaults_t428_0_0_0,
	&CRSpline_t429_0_0_0,
	&EasingFunction_t430_0_0_0,
	&ApplyTween_t431_0_0_0,
	&U3CTweenDelayU3Ec__Iterator1A_t433_0_0_0,
	&U3CTweenRestartU3Ec__Iterator1B_t434_0_0_0,
	&U3CStartU3Ec__Iterator1C_t435_0_0_0,
};
static const EncodedMethodIndex iTween_t432_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType iTween_t432_0_0_0;
extern const Il2CppType iTween_t432_1_0_0;
struct iTween_t432;
const Il2CppTypeDefinitionMetadata iTween_t432_DefinitionMetadata = 
{
	NULL/* declaringType */
	, iTween_t432_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, iTween_t432_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2693/* fieldStart */
	, 2428/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo iTween_t432_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "iTween"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &iTween_t432_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &iTween_t432_0_0_0/* byval_arg */
	, &iTween_t432_1_0_0/* this_arg */
	, &iTween_t432_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (iTween_t432)/* instance_size */
	, sizeof (iTween_t432)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(iTween_t432_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 254/* method_count */
	, 0/* property_count */
	, 50/* field_count */
	, 0/* event_count */
	, 10/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"
// Metadata Definition iTween/EaseType
extern TypeInfo EaseType_t425_il2cpp_TypeInfo;
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseTypeMethodDeclarations.h"
static const EncodedMethodIndex EaseType_t425_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair EaseType_t425_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType EaseType_t425_1_0_0;
const Il2CppTypeDefinitionMetadata EaseType_t425_DefinitionMetadata = 
{
	&iTween_t432_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EaseType_t425_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, EaseType_t425_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2743/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EaseType_t425_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "EaseType"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EaseType_t425_0_0_0/* byval_arg */
	, &EaseType_t425_1_0_0/* this_arg */
	, &EaseType_t425_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EaseType_t425)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EaseType_t425)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 34/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"
// Metadata Definition iTween/LoopType
extern TypeInfo LoopType_t426_il2cpp_TypeInfo;
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopTypeMethodDeclarations.h"
static const EncodedMethodIndex LoopType_t426_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair LoopType_t426_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType LoopType_t426_1_0_0;
const Il2CppTypeDefinitionMetadata LoopType_t426_DefinitionMetadata = 
{
	&iTween_t432_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LoopType_t426_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, LoopType_t426_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2777/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LoopType_t426_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "LoopType"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LoopType_t426_0_0_0/* byval_arg */
	, &LoopType_t426_1_0_0/* this_arg */
	, &LoopType_t426_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LoopType_t426)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LoopType_t426)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// iTween/NamedValueColor
#include "AssemblyU2DCSharp_iTween_NamedValueColor.h"
// Metadata Definition iTween/NamedValueColor
extern TypeInfo NamedValueColor_t427_il2cpp_TypeInfo;
// iTween/NamedValueColor
#include "AssemblyU2DCSharp_iTween_NamedValueColorMethodDeclarations.h"
static const EncodedMethodIndex NamedValueColor_t427_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair NamedValueColor_t427_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType NamedValueColor_t427_1_0_0;
const Il2CppTypeDefinitionMetadata NamedValueColor_t427_DefinitionMetadata = 
{
	&iTween_t432_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NamedValueColor_t427_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, NamedValueColor_t427_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2781/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NamedValueColor_t427_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "NamedValueColor"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NamedValueColor_t427_0_0_0/* byval_arg */
	, &NamedValueColor_t427_1_0_0/* this_arg */
	, &NamedValueColor_t427_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NamedValueColor_t427)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (NamedValueColor_t427)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// iTween/Defaults
#include "AssemblyU2DCSharp_iTween_Defaults.h"
// Metadata Definition iTween/Defaults
extern TypeInfo Defaults_t428_il2cpp_TypeInfo;
// iTween/Defaults
#include "AssemblyU2DCSharp_iTween_DefaultsMethodDeclarations.h"
static const EncodedMethodIndex Defaults_t428_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType Defaults_t428_1_0_0;
struct Defaults_t428;
const Il2CppTypeDefinitionMetadata Defaults_t428_DefinitionMetadata = 
{
	&iTween_t432_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Defaults_t428_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2786/* fieldStart */
	, 2682/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Defaults_t428_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Defaults"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Defaults_t428_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Defaults_t428_0_0_0/* byval_arg */
	, &Defaults_t428_1_0_0/* this_arg */
	, &Defaults_t428_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Defaults_t428)/* instance_size */
	, sizeof (Defaults_t428)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Defaults_t428_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048962/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// iTween/CRSpline
#include "AssemblyU2DCSharp_iTween_CRSpline.h"
// Metadata Definition iTween/CRSpline
extern TypeInfo CRSpline_t429_il2cpp_TypeInfo;
// iTween/CRSpline
#include "AssemblyU2DCSharp_iTween_CRSplineMethodDeclarations.h"
static const EncodedMethodIndex CRSpline_t429_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CRSpline_t429_1_0_0;
struct CRSpline_t429;
const Il2CppTypeDefinitionMetadata CRSpline_t429_DefinitionMetadata = 
{
	&iTween_t432_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CRSpline_t429_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2802/* fieldStart */
	, 2683/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CRSpline_t429_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CRSpline"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CRSpline_t429_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CRSpline_t429_0_0_0/* byval_arg */
	, &CRSpline_t429_1_0_0/* this_arg */
	, &CRSpline_t429_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CRSpline_t429)/* instance_size */
	, sizeof (CRSpline_t429)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// iTween/EasingFunction
#include "AssemblyU2DCSharp_iTween_EasingFunction.h"
// Metadata Definition iTween/EasingFunction
extern TypeInfo EasingFunction_t430_il2cpp_TypeInfo;
// iTween/EasingFunction
#include "AssemblyU2DCSharp_iTween_EasingFunctionMethodDeclarations.h"
static const EncodedMethodIndex EasingFunction_t430_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	866,
	867,
	868,
};
static Il2CppInterfaceOffsetPair EasingFunction_t430_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType EasingFunction_t430_1_0_0;
struct EasingFunction_t430;
const Il2CppTypeDefinitionMetadata EasingFunction_t430_DefinitionMetadata = 
{
	&iTween_t432_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EasingFunction_t430_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, EasingFunction_t430_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2685/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EasingFunction_t430_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "EasingFunction"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &EasingFunction_t430_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EasingFunction_t430_0_0_0/* byval_arg */
	, &EasingFunction_t430_1_0_0/* this_arg */
	, &EasingFunction_t430_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_EasingFunction_t430/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EasingFunction_t430)/* instance_size */
	, sizeof (EasingFunction_t430)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// iTween/ApplyTween
#include "AssemblyU2DCSharp_iTween_ApplyTween.h"
// Metadata Definition iTween/ApplyTween
extern TypeInfo ApplyTween_t431_il2cpp_TypeInfo;
// iTween/ApplyTween
#include "AssemblyU2DCSharp_iTween_ApplyTweenMethodDeclarations.h"
static const EncodedMethodIndex ApplyTween_t431_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	869,
	870,
	871,
};
static Il2CppInterfaceOffsetPair ApplyTween_t431_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ApplyTween_t431_1_0_0;
struct ApplyTween_t431;
const Il2CppTypeDefinitionMetadata ApplyTween_t431_DefinitionMetadata = 
{
	&iTween_t432_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ApplyTween_t431_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, ApplyTween_t431_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2689/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ApplyTween_t431_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ApplyTween"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ApplyTween_t431_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ApplyTween_t431_0_0_0/* byval_arg */
	, &ApplyTween_t431_1_0_0/* this_arg */
	, &ApplyTween_t431_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ApplyTween_t431/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ApplyTween_t431)/* instance_size */
	, sizeof (ApplyTween_t431)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// iTween/<TweenDelay>c__Iterator1A
#include "AssemblyU2DCSharp_iTween_U3CTweenDelayU3Ec__Iterator1A.h"
// Metadata Definition iTween/<TweenDelay>c__Iterator1A
extern TypeInfo U3CTweenDelayU3Ec__Iterator1A_t433_il2cpp_TypeInfo;
// iTween/<TweenDelay>c__Iterator1A
#include "AssemblyU2DCSharp_iTween_U3CTweenDelayU3Ec__Iterator1AMethodDeclarations.h"
static const EncodedMethodIndex U3CTweenDelayU3Ec__Iterator1A_t433_VTable[9] = 
{
	626,
	601,
	627,
	628,
	872,
	873,
	874,
	875,
	876,
};
static const Il2CppType* U3CTweenDelayU3Ec__Iterator1A_t433_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CTweenDelayU3Ec__Iterator1A_t433_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CTweenDelayU3Ec__Iterator1A_t433_1_0_0;
struct U3CTweenDelayU3Ec__Iterator1A_t433;
const Il2CppTypeDefinitionMetadata U3CTweenDelayU3Ec__Iterator1A_t433_DefinitionMetadata = 
{
	&iTween_t432_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CTweenDelayU3Ec__Iterator1A_t433_InterfacesTypeInfos/* implementedInterfaces */
	, U3CTweenDelayU3Ec__Iterator1A_t433_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CTweenDelayU3Ec__Iterator1A_t433_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2803/* fieldStart */
	, 2693/* methodStart */
	, -1/* eventStart */
	, 323/* propertyStart */

};
TypeInfo U3CTweenDelayU3Ec__Iterator1A_t433_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<TweenDelay>c__Iterator1A"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CTweenDelayU3Ec__Iterator1A_t433_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 953/* custom_attributes_cache */
	, &U3CTweenDelayU3Ec__Iterator1A_t433_0_0_0/* byval_arg */
	, &U3CTweenDelayU3Ec__Iterator1A_t433_1_0_0/* this_arg */
	, &U3CTweenDelayU3Ec__Iterator1A_t433_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CTweenDelayU3Ec__Iterator1A_t433)/* instance_size */
	, sizeof (U3CTweenDelayU3Ec__Iterator1A_t433)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// iTween/<TweenRestart>c__Iterator1B
#include "AssemblyU2DCSharp_iTween_U3CTweenRestartU3Ec__Iterator1B.h"
// Metadata Definition iTween/<TweenRestart>c__Iterator1B
extern TypeInfo U3CTweenRestartU3Ec__Iterator1B_t434_il2cpp_TypeInfo;
// iTween/<TweenRestart>c__Iterator1B
#include "AssemblyU2DCSharp_iTween_U3CTweenRestartU3Ec__Iterator1BMethodDeclarations.h"
static const EncodedMethodIndex U3CTweenRestartU3Ec__Iterator1B_t434_VTable[9] = 
{
	626,
	601,
	627,
	628,
	877,
	878,
	879,
	880,
	881,
};
static const Il2CppType* U3CTweenRestartU3Ec__Iterator1B_t434_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CTweenRestartU3Ec__Iterator1B_t434_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CTweenRestartU3Ec__Iterator1B_t434_1_0_0;
struct U3CTweenRestartU3Ec__Iterator1B_t434;
const Il2CppTypeDefinitionMetadata U3CTweenRestartU3Ec__Iterator1B_t434_DefinitionMetadata = 
{
	&iTween_t432_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CTweenRestartU3Ec__Iterator1B_t434_InterfacesTypeInfos/* implementedInterfaces */
	, U3CTweenRestartU3Ec__Iterator1B_t434_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CTweenRestartU3Ec__Iterator1B_t434_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2806/* fieldStart */
	, 2699/* methodStart */
	, -1/* eventStart */
	, 325/* propertyStart */

};
TypeInfo U3CTweenRestartU3Ec__Iterator1B_t434_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<TweenRestart>c__Iterator1B"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CTweenRestartU3Ec__Iterator1B_t434_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 958/* custom_attributes_cache */
	, &U3CTweenRestartU3Ec__Iterator1B_t434_0_0_0/* byval_arg */
	, &U3CTweenRestartU3Ec__Iterator1B_t434_1_0_0/* this_arg */
	, &U3CTweenRestartU3Ec__Iterator1B_t434_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CTweenRestartU3Ec__Iterator1B_t434)/* instance_size */
	, sizeof (U3CTweenRestartU3Ec__Iterator1B_t434)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// iTween/<Start>c__Iterator1C
#include "AssemblyU2DCSharp_iTween_U3CStartU3Ec__Iterator1C.h"
// Metadata Definition iTween/<Start>c__Iterator1C
extern TypeInfo U3CStartU3Ec__Iterator1C_t435_il2cpp_TypeInfo;
// iTween/<Start>c__Iterator1C
#include "AssemblyU2DCSharp_iTween_U3CStartU3Ec__Iterator1CMethodDeclarations.h"
static const EncodedMethodIndex U3CStartU3Ec__Iterator1C_t435_VTable[9] = 
{
	626,
	601,
	627,
	628,
	882,
	883,
	884,
	885,
	886,
};
static const Il2CppType* U3CStartU3Ec__Iterator1C_t435_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CStartU3Ec__Iterator1C_t435_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CStartU3Ec__Iterator1C_t435_1_0_0;
struct U3CStartU3Ec__Iterator1C_t435;
const Il2CppTypeDefinitionMetadata U3CStartU3Ec__Iterator1C_t435_DefinitionMetadata = 
{
	&iTween_t432_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CStartU3Ec__Iterator1C_t435_InterfacesTypeInfos/* implementedInterfaces */
	, U3CStartU3Ec__Iterator1C_t435_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CStartU3Ec__Iterator1C_t435_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2809/* fieldStart */
	, 2705/* methodStart */
	, -1/* eventStart */
	, 327/* propertyStart */

};
TypeInfo U3CStartU3Ec__Iterator1C_t435_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Start>c__Iterator1C"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CStartU3Ec__Iterator1C_t435_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 963/* custom_attributes_cache */
	, &U3CStartU3Ec__Iterator1C_t435_0_0_0/* byval_arg */
	, &U3CStartU3Ec__Iterator1C_t435_1_0_0/* this_arg */
	, &U3CStartU3Ec__Iterator1C_t435_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CStartU3Ec__Iterator1C_t435)/* instance_size */
	, sizeof (U3CStartU3Ec__Iterator1C_t435)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// ArrayIndexes
#include "AssemblyU2DCSharp_ArrayIndexes.h"
// Metadata Definition ArrayIndexes
extern TypeInfo ArrayIndexes_t441_il2cpp_TypeInfo;
// ArrayIndexes
#include "AssemblyU2DCSharp_ArrayIndexesMethodDeclarations.h"
static const EncodedMethodIndex ArrayIndexes_t441_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType ArrayIndexes_t441_0_0_0;
extern const Il2CppType ArrayIndexes_t441_1_0_0;
struct ArrayIndexes_t441;
const Il2CppTypeDefinitionMetadata ArrayIndexes_t441_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArrayIndexes_t441_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2812/* fieldStart */
	, 2711/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArrayIndexes_t441_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayIndexes"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ArrayIndexes_t441_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArrayIndexes_t441_0_0_0/* byval_arg */
	, &ArrayIndexes_t441_1_0_0/* this_arg */
	, &ArrayIndexes_t441_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArrayIndexes_t441)/* instance_size */
	, sizeof (ArrayIndexes_t441)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// iTweenEvent
#include "AssemblyU2DCSharp_iTweenEvent.h"
// Metadata Definition iTweenEvent
extern TypeInfo iTweenEvent_t443_il2cpp_TypeInfo;
// iTweenEvent
#include "AssemblyU2DCSharp_iTweenEventMethodDeclarations.h"
extern const Il2CppRGCTXDefinition iTweenEvent_AddToList_m24076_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5402 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition iTweenEvent_AddToList_m24077_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5403 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition iTweenEvent_AddToList_m24078_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5404 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType iTweenEvent_AddToList_m24079_gp_0_0_0_0;
extern const Il2CppType ICollection_1_t3615_0_0_0;
extern const Il2CppRGCTXDefinition iTweenEvent_AddToList_m24079_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 3628 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6065 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType TweenType_t442_0_0_0;
extern const Il2CppType U3CStartEventU3Ec__Iterator1D_t444_0_0_0;
extern const Il2CppType U3CGetEventU3Ec__AnonStorey1F_t447_0_0_0;
static const Il2CppType* iTweenEvent_t443_il2cpp_TypeInfo__nestedTypes[3] =
{
	&TweenType_t442_0_0_0,
	&U3CStartEventU3Ec__Iterator1D_t444_0_0_0,
	&U3CGetEventU3Ec__AnonStorey1F_t447_0_0_0,
};
static const EncodedMethodIndex iTweenEvent_t443_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType iTweenEvent_t443_0_0_0;
extern const Il2CppType iTweenEvent_t443_1_0_0;
struct iTweenEvent_t443;
const Il2CppTypeDefinitionMetadata iTweenEvent_t443_DefinitionMetadata = 
{
	NULL/* declaringType */
	, iTweenEvent_t443_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, iTweenEvent_t443_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2813/* fieldStart */
	, 2712/* methodStart */
	, -1/* eventStart */
	, 329/* propertyStart */

};
TypeInfo iTweenEvent_t443_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "iTweenEvent"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &iTweenEvent_t443_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &iTweenEvent_t443_0_0_0/* byval_arg */
	, &iTweenEvent_t443_1_0_0/* this_arg */
	, &iTweenEvent_t443_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (iTweenEvent_t443)/* instance_size */
	, sizeof (iTweenEvent_t443)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 1/* property_count */
	, 29/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"
// Metadata Definition iTweenEvent/TweenType
extern TypeInfo TweenType_t442_il2cpp_TypeInfo;
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenTypeMethodDeclarations.h"
static const EncodedMethodIndex TweenType_t442_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair TweenType_t442_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType TweenType_t442_1_0_0;
const Il2CppTypeDefinitionMetadata TweenType_t442_DefinitionMetadata = 
{
	&iTweenEvent_t443_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TweenType_t442_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, TweenType_t442_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2842/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TweenType_t442_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweenType"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TweenType_t442_0_0_0/* byval_arg */
	, &TweenType_t442_1_0_0/* this_arg */
	, &TweenType_t442_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TweenType_t442)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TweenType_t442)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 37/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// iTweenEvent/<StartEvent>c__Iterator1D
#include "AssemblyU2DCSharp_iTweenEvent_U3CStartEventU3Ec__Iterator1D.h"
// Metadata Definition iTweenEvent/<StartEvent>c__Iterator1D
extern TypeInfo U3CStartEventU3Ec__Iterator1D_t444_il2cpp_TypeInfo;
// iTweenEvent/<StartEvent>c__Iterator1D
#include "AssemblyU2DCSharp_iTweenEvent_U3CStartEventU3Ec__Iterator1DMethodDeclarations.h"
static const EncodedMethodIndex U3CStartEventU3Ec__Iterator1D_t444_VTable[9] = 
{
	626,
	601,
	627,
	628,
	887,
	888,
	889,
	890,
	891,
};
static const Il2CppType* U3CStartEventU3Ec__Iterator1D_t444_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CStartEventU3Ec__Iterator1D_t444_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IEnumerator_1_t2280_0_0_0, 6},
	{ &IDisposable_t538_0_0_0, 7},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CStartEventU3Ec__Iterator1D_t444_1_0_0;
struct U3CStartEventU3Ec__Iterator1D_t444;
const Il2CppTypeDefinitionMetadata U3CStartEventU3Ec__Iterator1D_t444_DefinitionMetadata = 
{
	&iTweenEvent_t443_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CStartEventU3Ec__Iterator1D_t444_InterfacesTypeInfos/* implementedInterfaces */
	, U3CStartEventU3Ec__Iterator1D_t444_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CStartEventU3Ec__Iterator1D_t444_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2879/* fieldStart */
	, 2727/* methodStart */
	, -1/* eventStart */
	, 330/* propertyStart */

};
TypeInfo U3CStartEventU3Ec__Iterator1D_t444_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<StartEvent>c__Iterator1D"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CStartEventU3Ec__Iterator1D_t444_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 988/* custom_attributes_cache */
	, &U3CStartEventU3Ec__Iterator1D_t444_0_0_0/* byval_arg */
	, &U3CStartEventU3Ec__Iterator1D_t444_1_0_0/* this_arg */
	, &U3CStartEventU3Ec__Iterator1D_t444_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CStartEventU3Ec__Iterator1D_t444)/* instance_size */
	, sizeof (U3CStartEventU3Ec__Iterator1D_t444)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// iTweenEvent/<GetEvent>c__AnonStorey1F
#include "AssemblyU2DCSharp_iTweenEvent_U3CGetEventU3Ec__AnonStorey1F.h"
// Metadata Definition iTweenEvent/<GetEvent>c__AnonStorey1F
extern TypeInfo U3CGetEventU3Ec__AnonStorey1F_t447_il2cpp_TypeInfo;
// iTweenEvent/<GetEvent>c__AnonStorey1F
#include "AssemblyU2DCSharp_iTweenEvent_U3CGetEventU3Ec__AnonStorey1FMethodDeclarations.h"
static const EncodedMethodIndex U3CGetEventU3Ec__AnonStorey1F_t447_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CGetEventU3Ec__AnonStorey1F_t447_1_0_0;
struct U3CGetEventU3Ec__AnonStorey1F_t447;
const Il2CppTypeDefinitionMetadata U3CGetEventU3Ec__AnonStorey1F_t447_DefinitionMetadata = 
{
	&iTweenEvent_t443_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetEventU3Ec__AnonStorey1F_t447_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2885/* fieldStart */
	, 2733/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CGetEventU3Ec__AnonStorey1F_t447_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetEvent>c__AnonStorey1F"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CGetEventU3Ec__AnonStorey1F_t447_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 993/* custom_attributes_cache */
	, &U3CGetEventU3Ec__AnonStorey1F_t447_0_0_0/* byval_arg */
	, &U3CGetEventU3Ec__AnonStorey1F_t447_1_0_0/* this_arg */
	, &U3CGetEventU3Ec__AnonStorey1F_t447_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetEventU3Ec__AnonStorey1F_t447)/* instance_size */
	, sizeof (U3CGetEventU3Ec__AnonStorey1F_t447)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// iTweenPath
#include "AssemblyU2DCSharp_iTweenPath.h"
// Metadata Definition iTweenPath
extern TypeInfo iTweenPath_t459_il2cpp_TypeInfo;
// iTweenPath
#include "AssemblyU2DCSharp_iTweenPathMethodDeclarations.h"
static const EncodedMethodIndex iTweenPath_t459_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType iTweenPath_t459_0_0_0;
extern const Il2CppType iTweenPath_t459_1_0_0;
struct iTweenPath_t459;
const Il2CppTypeDefinitionMetadata iTweenPath_t459_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, iTweenPath_t459_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2886/* fieldStart */
	, 2735/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo iTweenPath_t459_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "iTweenPath"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &iTweenPath_t459_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &iTweenPath_t459_0_0_0/* byval_arg */
	, &iTweenPath_t459_1_0_0/* this_arg */
	, &iTweenPath_t459_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (iTweenPath_t459)/* instance_size */
	, sizeof (iTweenPath_t459)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(iTweenPath_t459_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
extern TypeInfo U3CPrivateImplementationDetailsU3E_t463_il2cpp_TypeInfo;
// <PrivateImplementationDetails>
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
extern const Il2CppType U24ArrayTypeU2448_t460_0_0_0;
extern const Il2CppType U24ArrayTypeU2420_t461_0_0_0;
extern const Il2CppType U24ArrayTypeU24124_t462_0_0_0;
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t463_il2cpp_TypeInfo__nestedTypes[3] =
{
	&U24ArrayTypeU2448_t460_0_0_0,
	&U24ArrayTypeU2420_t461_0_0_0,
	&U24ArrayTypeU24124_t462_0_0_0,
};
static const EncodedMethodIndex U3CPrivateImplementationDetailsU3E_t463_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t463_0_0_0;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t463_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t463;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t463_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t463_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t463_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2893/* fieldStart */
	, 2740/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t463_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CPrivateImplementationDetailsU3E_t463_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 994/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t463_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t463_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t463_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t463)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t463)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t463_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 43/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$48
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E_U24Arra.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$48
extern TypeInfo U24ArrayTypeU2448_t460_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$48
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E_U24ArraMethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2448_t460_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U24ArrayTypeU2448_t460_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2448_t460_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t463_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2448_t460_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2448_t460_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$48"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2448_t460_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2448_t460_0_0_0/* byval_arg */
	, &U24ArrayTypeU2448_t460_1_0_0/* this_arg */
	, &U24ArrayTypeU2448_t460_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2448_t460_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t460_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t460_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2448_t460)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2448_t460)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2448_t460_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$20
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E_U24Arra_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$20
extern TypeInfo U24ArrayTypeU2420_t461_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$20
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E_U24Arra_0MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2420_t461_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U24ArrayTypeU2420_t461_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2420_t461_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t463_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2420_t461_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2420_t461_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$20"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2420_t461_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2420_t461_0_0_0/* byval_arg */
	, &U24ArrayTypeU2420_t461_1_0_0/* this_arg */
	, &U24ArrayTypeU2420_t461_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2420_t461_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t461_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t461_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2420_t461)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2420_t461)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2420_t461_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$124
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E_U24Arra_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$124
extern TypeInfo U24ArrayTypeU24124_t462_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$124
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E_U24Arra_1MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU24124_t462_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U24ArrayTypeU24124_t462_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24124_t462_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t463_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU24124_t462_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU24124_t462_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$124"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU24124_t462_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24124_t462_0_0_0/* byval_arg */
	, &U24ArrayTypeU24124_t462_1_0_0/* this_arg */
	, &U24ArrayTypeU24124_t462_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24124_t462_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24124_t462_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24124_t462_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24124_t462)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24124_t462)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24124_t462_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
