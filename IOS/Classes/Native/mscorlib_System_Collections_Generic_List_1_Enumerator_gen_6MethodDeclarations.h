﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.IEventSystemHandler>
struct Enumerator_t2263;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.IEventSystemHandler
struct IEventSystemHandler_t2261;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>
struct List_1_t767;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.IEventSystemHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m13989(__this, ___l, method) (( void (*) (Enumerator_t2263 *, List_1_t767 *, const MethodInfo*))Enumerator__ctor_m13586_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.IEventSystemHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m13990(__this, method) (( Object_t * (*) (Enumerator_t2263 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13587_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.IEventSystemHandler>::Dispose()
#define Enumerator_Dispose_m13991(__this, method) (( void (*) (Enumerator_t2263 *, const MethodInfo*))Enumerator_Dispose_m13588_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.IEventSystemHandler>::VerifyState()
#define Enumerator_VerifyState_m13992(__this, method) (( void (*) (Enumerator_t2263 *, const MethodInfo*))Enumerator_VerifyState_m13589_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.IEventSystemHandler>::MoveNext()
#define Enumerator_MoveNext_m13993(__this, method) (( bool (*) (Enumerator_t2263 *, const MethodInfo*))Enumerator_MoveNext_m13590_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.IEventSystemHandler>::get_Current()
#define Enumerator_get_Current_m13994(__this, method) (( Object_t * (*) (Enumerator_t2263 *, const MethodInfo*))Enumerator_get_Current_m13591_gshared)(__this, method)
