﻿#pragma once
struct Object_t;
typedef Object_t Il2CppCodeGenObject;
// System.Array
#include "mscorlib_System_Array.h"
typedef Array_t Il2CppCodeGenArray;
struct String_t;
typedef String_t Il2CppCodeGenString;
struct Type_t;
typedef Type_t Il2CppCodeGenType;
struct Exception_t520;
typedef Exception_t520 Il2CppCodeGenException;
struct Exception_t520;
typedef Exception_t520 Il2CppCodeGenException;
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
typedef RuntimeTypeHandle_t1550 Il2CppCodeGenRuntimeTypeHandle;
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
typedef RuntimeFieldHandle_t1551 Il2CppCodeGenRuntimeFieldHandle;
// System.RuntimeArgumentHandle
#include "mscorlib_System_RuntimeArgumentHandle.h"
typedef RuntimeArgumentHandle_t1569 Il2CppCodeGenRuntimeArgumentHandle;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
typedef RuntimeMethodHandle_t2132 Il2CppCodeGenRuntimeMethodHandle;
struct StringBuilder_t780;
typedef StringBuilder_t780 Il2CppCodeGenStringBuilder;
struct MulticastDelegate_t219;
typedef MulticastDelegate_t219 Il2CppCodeGenMulticastDelegate;
struct MethodBase_t1163;
typedef MethodBase_t1163 Il2CppCodeGenMethodBase;
