﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t302;
// System.Object
struct Object_t;
// UnityEngine.Texture2D
struct Texture2D_t9;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`1<UnityEngine.Texture2D>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_7MethodDeclarations.h"
#define Action_1__ctor_m3108(__this, ___object, ___method, method) (( void (*) (Action_1_t302 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m14500_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.Texture2D>::Invoke(T)
#define Action_1_Invoke_m3102(__this, ___obj, method) (( void (*) (Action_1_t302 *, Texture2D_t9 *, const MethodInfo*))Action_1_Invoke_m14501_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.Texture2D>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m14502(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t302 *, Texture2D_t9 *, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m14503_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.Texture2D>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m14504(__this, ___result, method) (( void (*) (Action_1_t302 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m14505_gshared)(__this, ___result, method)
