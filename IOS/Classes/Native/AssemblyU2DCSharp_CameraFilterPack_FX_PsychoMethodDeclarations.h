﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Psycho
struct CameraFilterPack_FX_Psycho_t138;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Psycho::.ctor()
extern "C" void CameraFilterPack_FX_Psycho__ctor_m893 (CameraFilterPack_FX_Psycho_t138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Psycho::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Psycho_get_material_m894 (CameraFilterPack_FX_Psycho_t138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Psycho::Start()
extern "C" void CameraFilterPack_FX_Psycho_Start_m895 (CameraFilterPack_FX_Psycho_t138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Psycho::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Psycho_OnRenderImage_m896 (CameraFilterPack_FX_Psycho_t138 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Psycho::OnValidate()
extern "C" void CameraFilterPack_FX_Psycho_OnValidate_m897 (CameraFilterPack_FX_Psycho_t138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Psycho::Update()
extern "C" void CameraFilterPack_FX_Psycho_Update_m898 (CameraFilterPack_FX_Psycho_t138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Psycho::OnDisable()
extern "C" void CameraFilterPack_FX_Psycho_OnDisable_m899 (CameraFilterPack_FX_Psycho_t138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
