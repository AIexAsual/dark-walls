﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// Stages
struct  Stages_t351  : public Object_t
{
};
struct Stages_t351_StaticFields{
	// System.String Stages::INTRO_ROOM
	String_t* ___INTRO_ROOM_0;
	// System.String Stages::EMERGENCY_ROOM
	String_t* ___EMERGENCY_ROOM_1;
	// System.String Stages::HALLWAY_ROOM
	String_t* ___HALLWAY_ROOM_2;
	// System.String Stages::COMFORT_ROOM
	String_t* ___COMFORT_ROOM_3;
	// System.String Stages::LOBBY_ROOM
	String_t* ___LOBBY_ROOM_4;
	// System.String Stages::NURSERY_ROOM
	String_t* ___NURSERY_ROOM_5;
	// System.String Stages::MORGUE_ROOM
	String_t* ___MORGUE_ROOM_6;
	// System.String Stages::WARD_ROOM
	String_t* ___WARD_ROOM_7;
	// System.String Stages::EMERGENCY_OBJECTS
	String_t* ___EMERGENCY_OBJECTS_8;
	// System.String Stages::HALLWAY_OBJECTS
	String_t* ___HALLWAY_OBJECTS_9;
	// System.String Stages::COMFORT_OBJECTS
	String_t* ___COMFORT_OBJECTS_10;
	// System.String Stages::LOBBY_OBJECTS
	String_t* ___LOBBY_OBJECTS_11;
	// System.String Stages::NURSERY_OBJECTS
	String_t* ___NURSERY_OBJECTS_12;
	// System.String Stages::MORGUE_OBJECTS
	String_t* ___MORGUE_OBJECTS_13;
	// System.String Stages::WARD_OBJECTS
	String_t* ___WARD_OBJECTS_14;
};
