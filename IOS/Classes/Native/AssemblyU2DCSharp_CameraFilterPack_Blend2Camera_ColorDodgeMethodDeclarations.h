﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_ColorDodge
struct CameraFilterPack_Blend2Camera_ColorDodge_t20;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_ColorDodge::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_ColorDodge__ctor_m74 (CameraFilterPack_Blend2Camera_ColorDodge_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_ColorDodge::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_ColorDodge_get_material_m75 (CameraFilterPack_Blend2Camera_ColorDodge_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorDodge::Start()
extern "C" void CameraFilterPack_Blend2Camera_ColorDodge_Start_m76 (CameraFilterPack_Blend2Camera_ColorDodge_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorDodge::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_ColorDodge_OnRenderImage_m77 (CameraFilterPack_Blend2Camera_ColorDodge_t20 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorDodge::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_ColorDodge_OnValidate_m78 (CameraFilterPack_Blend2Camera_ColorDodge_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorDodge::Update()
extern "C" void CameraFilterPack_Blend2Camera_ColorDodge_Update_m79 (CameraFilterPack_Blend2Camera_ColorDodge_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorDodge::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_ColorDodge_OnEnable_m80 (CameraFilterPack_Blend2Camera_ColorDodge_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_ColorDodge::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_ColorDodge_OnDisable_m81 (CameraFilterPack_Blend2Camera_ColorDodge_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
