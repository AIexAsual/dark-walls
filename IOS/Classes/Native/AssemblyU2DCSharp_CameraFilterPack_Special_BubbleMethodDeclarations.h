﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Special_Bubble
struct CameraFilterPack_Special_Bubble_t174;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Special_Bubble::.ctor()
extern "C" void CameraFilterPack_Special_Bubble__ctor_m1126 (CameraFilterPack_Special_Bubble_t174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Special_Bubble::get_material()
extern "C" Material_t2 * CameraFilterPack_Special_Bubble_get_material_m1127 (CameraFilterPack_Special_Bubble_t174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Special_Bubble::Start()
extern "C" void CameraFilterPack_Special_Bubble_Start_m1128 (CameraFilterPack_Special_Bubble_t174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Special_Bubble::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Special_Bubble_OnRenderImage_m1129 (CameraFilterPack_Special_Bubble_t174 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Special_Bubble::OnValidate()
extern "C" void CameraFilterPack_Special_Bubble_OnValidate_m1130 (CameraFilterPack_Special_Bubble_t174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Special_Bubble::Update()
extern "C" void CameraFilterPack_Special_Bubble_Update_m1131 (CameraFilterPack_Special_Bubble_t174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Special_Bubble::OnDisable()
extern "C" void CameraFilterPack_Special_Bubble_OnDisable_m1132 (CameraFilterPack_Special_Bubble_t174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
