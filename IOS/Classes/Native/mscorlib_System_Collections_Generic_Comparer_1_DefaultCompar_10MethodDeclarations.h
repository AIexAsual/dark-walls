﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t2563;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C" void DefaultComparer__ctor_m18257_gshared (DefaultComparer_t2563 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m18257(__this, method) (( void (*) (DefaultComparer_t2563 *, const MethodInfo*))DefaultComparer__ctor_m18257_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m18258_gshared (DefaultComparer_t2563 * __this, UIVertex_t685  ___x, UIVertex_t685  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m18258(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2563 *, UIVertex_t685 , UIVertex_t685 , const MethodInfo*))DefaultComparer_Compare_m18258_gshared)(__this, ___x, ___y, method)
