﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<iTween/EaseType>
struct DefaultComparer_t2456;
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<iTween/EaseType>::.ctor()
extern "C" void DefaultComparer__ctor_m16494_gshared (DefaultComparer_t2456 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m16494(__this, method) (( void (*) (DefaultComparer_t2456 *, const MethodInfo*))DefaultComparer__ctor_m16494_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<iTween/EaseType>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m16495_gshared (DefaultComparer_t2456 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m16495(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2456 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m16495_gshared)(__this, ___x, ___y, method)
