﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t2254;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void DefaultComparer__ctor_m13889_gshared (DefaultComparer_t2254 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m13889(__this, method) (( void (*) (DefaultComparer_t2254 *, const MethodInfo*))DefaultComparer__ctor_m13889_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m13890_gshared (DefaultComparer_t2254 * __this, RaycastResult_t511  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m13890(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2254 *, RaycastResult_t511 , const MethodInfo*))DefaultComparer_GetHashCode_m13890_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m13891_gshared (DefaultComparer_t2254 * __this, RaycastResult_t511  ___x, RaycastResult_t511  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m13891(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2254 *, RaycastResult_t511 , RaycastResult_t511 , const MethodInfo*))DefaultComparer_Equals_m13891_gshared)(__this, ___x, ___y, method)
