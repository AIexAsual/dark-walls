﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// LightController/<WaitAndSetFlicker>c__IteratorC
struct U3CWaitAndSetFlickerU3Ec__IteratorC_t345;
// System.Object
struct Object_t;

// System.Void LightController/<WaitAndSetFlicker>c__IteratorC::.ctor()
extern "C" void U3CWaitAndSetFlickerU3Ec__IteratorC__ctor_m2008 (U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LightController/<WaitAndSetFlicker>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndSetFlickerU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2009 (U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LightController/<WaitAndSetFlicker>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndSetFlickerU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m2010 (U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LightController/<WaitAndSetFlicker>c__IteratorC::MoveNext()
extern "C" bool U3CWaitAndSetFlickerU3Ec__IteratorC_MoveNext_m2011 (U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightController/<WaitAndSetFlicker>c__IteratorC::Dispose()
extern "C" void U3CWaitAndSetFlickerU3Ec__IteratorC_Dispose_m2012 (U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightController/<WaitAndSetFlicker>c__IteratorC::Reset()
extern "C" void U3CWaitAndSetFlickerU3Ec__IteratorC_Reset_m2013 (U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
