﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1061;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Events.BaseInvokableCall>
struct  Comparison_1_t2862  : public MulticastDelegate_t219
{
};
