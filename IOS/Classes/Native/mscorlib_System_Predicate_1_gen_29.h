﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Graphic
struct Graphic_t654;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Graphic>
struct  Predicate_1_t2573  : public MulticastDelegate_t219
{
};
