﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_Drawing_Manga2
struct  CameraFilterPack_Drawing_Manga2_t103  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Drawing_Manga2::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Drawing_Manga2::TimeX
	float ___TimeX_3;
	// UnityEngine.Material CameraFilterPack_Drawing_Manga2::SCMaterial
	Material_t2 * ___SCMaterial_4;
	// System.Single CameraFilterPack_Drawing_Manga2::DotSize
	float ___DotSize_5;
};
struct CameraFilterPack_Drawing_Manga2_t103_StaticFields{
	// System.Single CameraFilterPack_Drawing_Manga2::ChangeDotSize
	float ___ChangeDotSize_6;
};
