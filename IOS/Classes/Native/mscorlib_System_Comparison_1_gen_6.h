﻿#pragma once
#include <stdint.h>
// UnityEngine.Component
struct Component_t477;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Component>
struct  Comparison_1_t2274  : public MulticastDelegate_t219
{
};
