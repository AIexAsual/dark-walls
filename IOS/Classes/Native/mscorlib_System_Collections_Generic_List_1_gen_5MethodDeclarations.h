﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t457;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t3030;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t3031;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector3>
struct ICollection_1_t3032;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>
struct ReadOnlyCollection_1_t2423;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t317;
// System.Predicate`1<UnityEngine.Vector3>
struct Predicate_1_t2427;
// System.Comparison`1<UnityEngine.Vector3>
struct Comparison_1_t2430;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C" void List_1__ctor_m3392_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1__ctor_m3392(__this, method) (( void (*) (List_1_t457 *, const MethodInfo*))List_1__ctor_m3392_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m15932_gshared (List_1_t457 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m15932(__this, ___collection, method) (( void (*) (List_1_t457 *, Object_t*, const MethodInfo*))List_1__ctor_m15932_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Int32)
extern "C" void List_1__ctor_m15933_gshared (List_1_t457 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m15933(__this, ___capacity, method) (( void (*) (List_1_t457 *, int32_t, const MethodInfo*))List_1__ctor_m15933_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.cctor()
extern "C" void List_1__cctor_m15934_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m15934(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15934_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15935_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15935(__this, method) (( Object_t* (*) (List_1_t457 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15935_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m15936_gshared (List_1_t457 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m15936(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t457 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15936_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m15937_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m15937(__this, method) (( Object_t * (*) (List_1_t457 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15937_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m15938_gshared (List_1_t457 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m15938(__this, ___item, method) (( int32_t (*) (List_1_t457 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m15938_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m15939_gshared (List_1_t457 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m15939(__this, ___item, method) (( bool (*) (List_1_t457 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m15939_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m15940_gshared (List_1_t457 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m15940(__this, ___item, method) (( int32_t (*) (List_1_t457 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m15940_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m15941_gshared (List_1_t457 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m15941(__this, ___index, ___item, method) (( void (*) (List_1_t457 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m15941_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m15942_gshared (List_1_t457 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m15942(__this, ___item, method) (( void (*) (List_1_t457 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m15942_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15943_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15943(__this, method) (( bool (*) (List_1_t457 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15943_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m15944_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m15944(__this, method) (( bool (*) (List_1_t457 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15944_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m15945_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m15945(__this, method) (( Object_t * (*) (List_1_t457 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15945_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m15946_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m15946(__this, method) (( bool (*) (List_1_t457 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15946_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m15947_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m15947(__this, method) (( bool (*) (List_1_t457 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15947_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m15948_gshared (List_1_t457 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m15948(__this, ___index, method) (( Object_t * (*) (List_1_t457 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m15948_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m15949_gshared (List_1_t457 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m15949(__this, ___index, ___value, method) (( void (*) (List_1_t457 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m15949_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(T)
extern "C" void List_1_Add_m15950_gshared (List_1_t457 * __this, Vector3_t215  ___item, const MethodInfo* method);
#define List_1_Add_m15950(__this, ___item, method) (( void (*) (List_1_t457 *, Vector3_t215 , const MethodInfo*))List_1_Add_m15950_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m15951_gshared (List_1_t457 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m15951(__this, ___newCount, method) (( void (*) (List_1_t457 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15951_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m15952_gshared (List_1_t457 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m15952(__this, ___collection, method) (( void (*) (List_1_t457 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15952_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m15953_gshared (List_1_t457 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m15953(__this, ___enumerable, method) (( void (*) (List_1_t457 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15953_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m15954_gshared (List_1_t457 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m15954(__this, ___collection, method) (( void (*) (List_1_t457 *, Object_t*, const MethodInfo*))List_1_AddRange_m15954_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2423 * List_1_AsReadOnly_m15955_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m15955(__this, method) (( ReadOnlyCollection_1_t2423 * (*) (List_1_t457 *, const MethodInfo*))List_1_AsReadOnly_m15955_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear()
extern "C" void List_1_Clear_m15956_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1_Clear_m15956(__this, method) (( void (*) (List_1_t457 *, const MethodInfo*))List_1_Clear_m15956_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Contains(T)
extern "C" bool List_1_Contains_m15957_gshared (List_1_t457 * __this, Vector3_t215  ___item, const MethodInfo* method);
#define List_1_Contains_m15957(__this, ___item, method) (( bool (*) (List_1_t457 *, Vector3_t215 , const MethodInfo*))List_1_Contains_m15957_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m15958_gshared (List_1_t457 * __this, Vector3U5BU5D_t317* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m15958(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t457 *, Vector3U5BU5D_t317*, int32_t, const MethodInfo*))List_1_CopyTo_m15958_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::Find(System.Predicate`1<T>)
extern "C" Vector3_t215  List_1_Find_m15959_gshared (List_1_t457 * __this, Predicate_1_t2427 * ___match, const MethodInfo* method);
#define List_1_Find_m15959(__this, ___match, method) (( Vector3_t215  (*) (List_1_t457 *, Predicate_1_t2427 *, const MethodInfo*))List_1_Find_m15959_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m15960_gshared (Object_t * __this /* static, unused */, Predicate_1_t2427 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m15960(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2427 *, const MethodInfo*))List_1_CheckMatch_m15960_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m15961_gshared (List_1_t457 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2427 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m15961(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t457 *, int32_t, int32_t, Predicate_1_t2427 *, const MethodInfo*))List_1_GetIndex_m15961_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Enumerator_t2422  List_1_GetEnumerator_m15962_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m15962(__this, method) (( Enumerator_t2422  (*) (List_1_t457 *, const MethodInfo*))List_1_GetEnumerator_m15962_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m15963_gshared (List_1_t457 * __this, Vector3_t215  ___item, const MethodInfo* method);
#define List_1_IndexOf_m15963(__this, ___item, method) (( int32_t (*) (List_1_t457 *, Vector3_t215 , const MethodInfo*))List_1_IndexOf_m15963_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m15964_gshared (List_1_t457 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m15964(__this, ___start, ___delta, method) (( void (*) (List_1_t457 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15964_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m15965_gshared (List_1_t457 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m15965(__this, ___index, method) (( void (*) (List_1_t457 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15965_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m15966_gshared (List_1_t457 * __this, int32_t ___index, Vector3_t215  ___item, const MethodInfo* method);
#define List_1_Insert_m15966(__this, ___index, ___item, method) (( void (*) (List_1_t457 *, int32_t, Vector3_t215 , const MethodInfo*))List_1_Insert_m15966_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m15967_gshared (List_1_t457 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m15967(__this, ___collection, method) (( void (*) (List_1_t457 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15967_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Remove(T)
extern "C" bool List_1_Remove_m15968_gshared (List_1_t457 * __this, Vector3_t215  ___item, const MethodInfo* method);
#define List_1_Remove_m15968(__this, ___item, method) (( bool (*) (List_1_t457 *, Vector3_t215 , const MethodInfo*))List_1_Remove_m15968_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m15969_gshared (List_1_t457 * __this, Predicate_1_t2427 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m15969(__this, ___match, method) (( int32_t (*) (List_1_t457 *, Predicate_1_t2427 *, const MethodInfo*))List_1_RemoveAll_m15969_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m15970_gshared (List_1_t457 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m15970(__this, ___index, method) (( void (*) (List_1_t457 *, int32_t, const MethodInfo*))List_1_RemoveAt_m15970_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Reverse()
extern "C" void List_1_Reverse_m15971_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1_Reverse_m15971(__this, method) (( void (*) (List_1_t457 *, const MethodInfo*))List_1_Reverse_m15971_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Sort()
extern "C" void List_1_Sort_m15972_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1_Sort_m15972(__this, method) (( void (*) (List_1_t457 *, const MethodInfo*))List_1_Sort_m15972_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m15973_gshared (List_1_t457 * __this, Comparison_1_t2430 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m15973(__this, ___comparison, method) (( void (*) (List_1_t457 *, Comparison_1_t2430 *, const MethodInfo*))List_1_Sort_m15973_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
extern "C" Vector3U5BU5D_t317* List_1_ToArray_m3405_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1_ToArray_m3405(__this, method) (( Vector3U5BU5D_t317* (*) (List_1_t457 *, const MethodInfo*))List_1_ToArray_m3405_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::TrimExcess()
extern "C" void List_1_TrimExcess_m15974_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m15974(__this, method) (( void (*) (List_1_t457 *, const MethodInfo*))List_1_TrimExcess_m15974_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m15975_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m15975(__this, method) (( int32_t (*) (List_1_t457 *, const MethodInfo*))List_1_get_Capacity_m15975_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m15976_gshared (List_1_t457 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m15976(__this, ___value, method) (( void (*) (List_1_t457 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15976_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t List_1_get_Count_m15977_gshared (List_1_t457 * __this, const MethodInfo* method);
#define List_1_get_Count_m15977(__this, method) (( int32_t (*) (List_1_t457 *, const MethodInfo*))List_1_get_Count_m15977_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" Vector3_t215  List_1_get_Item_m15978_gshared (List_1_t457 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m15978(__this, ___index, method) (( Vector3_t215  (*) (List_1_t457 *, int32_t, const MethodInfo*))List_1_get_Item_m15978_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m15979_gshared (List_1_t457 * __this, int32_t ___index, Vector3_t215  ___value, const MethodInfo* method);
#define List_1_set_Item_m15979(__this, ___index, ___value, method) (( void (*) (List_1_t457 *, int32_t, Vector3_t215 , const MethodInfo*))List_1_set_Item_m15979_gshared)(__this, ___index, ___value, method)
