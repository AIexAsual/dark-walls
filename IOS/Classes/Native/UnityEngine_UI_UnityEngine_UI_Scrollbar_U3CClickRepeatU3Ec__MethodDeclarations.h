﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4
struct U3CClickRepeatU3Ec__Iterator4_t697;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::.ctor()
extern "C" void U3CClickRepeatU3Ec__Iterator4__ctor_m4040 (U3CClickRepeatU3Ec__Iterator4_t697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CClickRepeatU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4041 (U3CClickRepeatU3Ec__Iterator4_t697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CClickRepeatU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m4042 (U3CClickRepeatU3Ec__Iterator4_t697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::MoveNext()
extern "C" bool U3CClickRepeatU3Ec__Iterator4_MoveNext_m4043 (U3CClickRepeatU3Ec__Iterator4_t697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::Dispose()
extern "C" void U3CClickRepeatU3Ec__Iterator4_Dispose_m4044 (U3CClickRepeatU3Ec__Iterator4_t697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4::Reset()
extern "C" void U3CClickRepeatU3Ec__Iterator4_Reset_m4045 (U3CClickRepeatU3Ec__Iterator4_t697 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
