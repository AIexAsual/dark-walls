﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct ObjectPool_1_t750;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct UnityAction_1_t751;
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t777;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m4961(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t750 *, UnityAction_1_t751 *, UnityAction_1_t751 *, const MethodInfo*))ObjectPool_1__ctor_m13999_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countAll()
#define ObjectPool_1_get_countAll_m19250(__this, method) (( int32_t (*) (ObjectPool_1_t750 *, const MethodInfo*))ObjectPool_1_get_countAll_m14001_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m19251(__this, ___value, method) (( void (*) (ObjectPool_1_t750 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m14003_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countActive()
#define ObjectPool_1_get_countActive_m19252(__this, method) (( int32_t (*) (ObjectPool_1_t750 *, const MethodInfo*))ObjectPool_1_get_countActive_m14005_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m19253(__this, method) (( int32_t (*) (ObjectPool_1_t750 *, const MethodInfo*))ObjectPool_1_get_countInactive_m14007_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Get()
#define ObjectPool_1_Get_m4962(__this, method) (( List_1_t777 * (*) (ObjectPool_1_t750 *, const MethodInfo*))ObjectPool_1_Get_m14009_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Release(T)
#define ObjectPool_1_Release_m4963(__this, ___element, method) (( void (*) (ObjectPool_1_t750 *, List_1_t777 *, const MethodInfo*))ObjectPool_1_Release_m14011_gshared)(__this, ___element, method)
