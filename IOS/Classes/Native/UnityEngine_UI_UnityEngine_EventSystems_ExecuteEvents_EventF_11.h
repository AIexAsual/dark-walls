﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.IPointerExitHandler
struct IPointerExitHandler_t761;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t490;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
struct  EventFunction_1_t599  : public MulticastDelegate_t219
{
};
