﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// LightController
struct LightController_t343;
// System.Object
#include "mscorlib_System_Object.h"
// LightController/<FlickerMe>c__IteratorB
struct  U3CFlickerMeU3Ec__IteratorB_t344  : public Object_t
{
	// System.Single LightController/<FlickerMe>c__IteratorB::time
	float ___time_0;
	// System.Boolean LightController/<FlickerMe>c__IteratorB::val
	bool ___val_1;
	// System.Int32 LightController/<FlickerMe>c__IteratorB::$PC
	int32_t ___U24PC_2;
	// System.Object LightController/<FlickerMe>c__IteratorB::$current
	Object_t * ___U24current_3;
	// System.Single LightController/<FlickerMe>c__IteratorB::<$>time
	float ___U3CU24U3Etime_4;
	// System.Boolean LightController/<FlickerMe>c__IteratorB::<$>val
	bool ___U3CU24U3Eval_5;
	// LightController LightController/<FlickerMe>c__IteratorB::<>f__this
	LightController_t343 * ___U3CU3Ef__this_6;
};
