﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,iTweenPath>
struct KeyValuePair_2_t2484;
// System.String
struct String_t;
// iTweenPath
struct iTweenPath_t459;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,iTweenPath>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m17063(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2484 *, String_t*, iTweenPath_t459 *, const MethodInfo*))KeyValuePair_2__ctor_m14739_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,iTweenPath>::get_Key()
#define KeyValuePair_2_get_Key_m17064(__this, method) (( String_t* (*) (KeyValuePair_2_t2484 *, const MethodInfo*))KeyValuePair_2_get_Key_m14741_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,iTweenPath>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17065(__this, ___value, method) (( void (*) (KeyValuePair_2_t2484 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m14743_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,iTweenPath>::get_Value()
#define KeyValuePair_2_get_Value_m17066(__this, method) (( iTweenPath_t459 * (*) (KeyValuePair_2_t2484 *, const MethodInfo*))KeyValuePair_2_get_Value_m14745_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,iTweenPath>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17067(__this, ___value, method) (( void (*) (KeyValuePair_2_t2484 *, iTweenPath_t459 *, const MethodInfo*))KeyValuePair_2_set_Value_m14747_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,iTweenPath>::ToString()
#define KeyValuePair_2_ToString_m17068(__this, method) (( String_t* (*) (KeyValuePair_2_t2484 *, const MethodInfo*))KeyValuePair_2_ToString_m14749_gshared)(__this, method)
