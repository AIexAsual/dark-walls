﻿#pragma once
#include <stdint.h>
// System.Collections.CaseInsensitiveHashCodeProvider
struct CaseInsensitiveHashCodeProvider_t1531;
// System.Object
struct Object_t;
// System.Globalization.TextInfo
struct TextInfo_t1596;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.CaseInsensitiveHashCodeProvider
struct  CaseInsensitiveHashCodeProvider_t1531  : public Object_t
{
	// System.Globalization.TextInfo System.Collections.CaseInsensitiveHashCodeProvider::m_text
	TextInfo_t1596 * ___m_text_2;
};
struct CaseInsensitiveHashCodeProvider_t1531_StaticFields{
	// System.Collections.CaseInsensitiveHashCodeProvider System.Collections.CaseInsensitiveHashCodeProvider::singletonInvariant
	CaseInsensitiveHashCodeProvider_t1531 * ___singletonInvariant_0;
	// System.Object System.Collections.CaseInsensitiveHashCodeProvider::sync
	Object_t * ___sync_1;
};
