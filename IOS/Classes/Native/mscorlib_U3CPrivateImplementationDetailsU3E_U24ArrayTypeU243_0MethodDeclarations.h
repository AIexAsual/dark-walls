﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$32
struct U24ArrayTypeU2432_t2157;
struct U24ArrayTypeU2432_t2157_marshaled;

void U24ArrayTypeU2432_t2157_marshal(const U24ArrayTypeU2432_t2157& unmarshaled, U24ArrayTypeU2432_t2157_marshaled& marshaled);
void U24ArrayTypeU2432_t2157_marshal_back(const U24ArrayTypeU2432_t2157_marshaled& marshaled, U24ArrayTypeU2432_t2157& unmarshaled);
void U24ArrayTypeU2432_t2157_marshal_cleanup(U24ArrayTypeU2432_t2157_marshaled& marshaled);
