﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.QualitySettings
struct QualitySettings_t852;
// UnityEngine.ColorSpace
#include "UnityEngine_UnityEngine_ColorSpace.h"

// System.Int32 UnityEngine.QualitySettings::get_antiAliasing()
extern "C" int32_t QualitySettings_get_antiAliasing_m3058 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern "C" int32_t QualitySettings_get_activeColorSpace_m5064 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
