﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_FX_8bits
struct  CameraFilterPack_FX_8bits_t120  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_FX_8bits::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_FX_8bits::TimeX
	float ___TimeX_3;
	// UnityEngine.Material CameraFilterPack_FX_8bits::SCMaterial
	Material_t2 * ___SCMaterial_4;
	// System.Single CameraFilterPack_FX_8bits::Brightness
	float ___Brightness_5;
	// System.Int32 CameraFilterPack_FX_8bits::ResolutionX
	int32_t ___ResolutionX_6;
	// System.Int32 CameraFilterPack_FX_8bits::ResolutionY
	int32_t ___ResolutionY_7;
};
struct CameraFilterPack_FX_8bits_t120_StaticFields{
	// System.Single CameraFilterPack_FX_8bits::ChangeBrightness
	float ___ChangeBrightness_8;
	// System.Int32 CameraFilterPack_FX_8bits::ChangeResolutionX
	int32_t ___ChangeResolutionX_9;
	// System.Int32 CameraFilterPack_FX_8bits::ChangeResolutionY
	int32_t ___ChangeResolutionY_10;
};
