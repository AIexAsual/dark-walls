﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
struct ThreadSafeDictionary_2_t2766;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1094;
// System.Object
struct Object_t;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>
struct ThreadSafeDictionaryValueFactory_2_t2764;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2786;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t3011;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
extern "C" void ThreadSafeDictionary_2__ctor_m21058_gshared (ThreadSafeDictionary_2_t2766 * __this, ThreadSafeDictionaryValueFactory_2_t2764 * ___valueFactory, const MethodInfo* method);
#define ThreadSafeDictionary_2__ctor_m21058(__this, ___valueFactory, method) (( void (*) (ThreadSafeDictionary_2_t2766 *, ThreadSafeDictionaryValueFactory_2_t2764 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m21058_gshared)(__this, ___valueFactory, method)
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m21060_gshared (ThreadSafeDictionary_2_t2766 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m21060(__this, method) (( Object_t * (*) (ThreadSafeDictionary_2_t2766 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m21060_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Get(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_Get_m21062_gshared (ThreadSafeDictionary_2_t2766 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_Get_m21062(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t2766 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m21062_gshared)(__this, ___key, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::AddValue(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_AddValue_m21064_gshared (ThreadSafeDictionary_2_t2766 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_AddValue_m21064(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t2766 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m21064_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C" void ThreadSafeDictionary_2_Add_m21066_gshared (ThreadSafeDictionary_2_t2766 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_Add_m21066(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t2766 *, Object_t *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_Add_m21066_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Keys()
extern "C" Object_t* ThreadSafeDictionary_2_get_Keys_m21068_gshared (ThreadSafeDictionary_2_t2766 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Keys_m21068(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t2766 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m21068_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool ThreadSafeDictionary_2_TryGetValue_m21070_gshared (ThreadSafeDictionary_2_t2766 * __this, Object_t * ___key, Object_t ** ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_TryGetValue_m21070(__this, ___key, ___value, method) (( bool (*) (ThreadSafeDictionary_2_t2766 *, Object_t *, Object_t **, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m21070_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Values()
extern "C" Object_t* ThreadSafeDictionary_2_get_Values_m21072_gshared (ThreadSafeDictionary_2_t2766 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Values_m21072(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t2766 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m21072_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_get_Item_m21074_gshared (ThreadSafeDictionary_2_t2766 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Item_m21074(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t2766 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m21074_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C" void ThreadSafeDictionary_2_set_Item_m21076_gshared (ThreadSafeDictionary_2_t2766 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_set_Item_m21076(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t2766 *, Object_t *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m21076_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void ThreadSafeDictionary_2_Add_m21078_gshared (ThreadSafeDictionary_2_t2766 * __this, KeyValuePair_2_t2350  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Add_m21078(__this, ___item, method) (( void (*) (ThreadSafeDictionary_2_t2766 *, KeyValuePair_2_t2350 , const MethodInfo*))ThreadSafeDictionary_2_Add_m21078_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Clear()
extern "C" void ThreadSafeDictionary_2_Clear_m21080_gshared (ThreadSafeDictionary_2_t2766 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_Clear_m21080(__this, method) (( void (*) (ThreadSafeDictionary_2_t2766 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m21080_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool ThreadSafeDictionary_2_Contains_m21082_gshared (ThreadSafeDictionary_2_t2766 * __this, KeyValuePair_2_t2350  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Contains_m21082(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t2766 *, KeyValuePair_2_t2350 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m21082_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void ThreadSafeDictionary_2_CopyTo_m21084_gshared (ThreadSafeDictionary_2_t2766 * __this, KeyValuePair_2U5BU5D_t2786* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define ThreadSafeDictionary_2_CopyTo_m21084(__this, ___array, ___arrayIndex, method) (( void (*) (ThreadSafeDictionary_2_t2766 *, KeyValuePair_2U5BU5D_t2786*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m21084_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Count()
extern "C" int32_t ThreadSafeDictionary_2_get_Count_m21086_gshared (ThreadSafeDictionary_2_t2766 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Count_m21086(__this, method) (( int32_t (*) (ThreadSafeDictionary_2_t2766 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m21086_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern "C" bool ThreadSafeDictionary_2_get_IsReadOnly_m21088_gshared (ThreadSafeDictionary_2_t2766 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_IsReadOnly_m21088(__this, method) (( bool (*) (ThreadSafeDictionary_2_t2766 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m21088_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool ThreadSafeDictionary_2_Remove_m21090_gshared (ThreadSafeDictionary_2_t2766 * __this, KeyValuePair_2_t2350  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Remove_m21090(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t2766 *, KeyValuePair_2_t2350 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m21090_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C" Object_t* ThreadSafeDictionary_2_GetEnumerator_m21092_gshared (ThreadSafeDictionary_2_t2766 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_GetEnumerator_m21092(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t2766 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m21092_gshared)(__this, method)
