﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractiveMama/<WaitAndHideMama>c__Iterator9
struct U3CWaitAndHideMamaU3Ec__Iterator9_t333;
// System.Object
struct Object_t;

// System.Void InteractiveMama/<WaitAndHideMama>c__Iterator9::.ctor()
extern "C" void U3CWaitAndHideMamaU3Ec__Iterator9__ctor_m1947 (U3CWaitAndHideMamaU3Ec__Iterator9_t333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InteractiveMama/<WaitAndHideMama>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndHideMamaU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1948 (U3CWaitAndHideMamaU3Ec__Iterator9_t333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InteractiveMama/<WaitAndHideMama>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndHideMamaU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m1949 (U3CWaitAndHideMamaU3Ec__Iterator9_t333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InteractiveMama/<WaitAndHideMama>c__Iterator9::MoveNext()
extern "C" bool U3CWaitAndHideMamaU3Ec__Iterator9_MoveNext_m1950 (U3CWaitAndHideMamaU3Ec__Iterator9_t333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveMama/<WaitAndHideMama>c__Iterator9::Dispose()
extern "C" void U3CWaitAndHideMamaU3Ec__Iterator9_Dispose_m1951 (U3CWaitAndHideMamaU3Ec__Iterator9_t333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveMama/<WaitAndHideMama>c__Iterator9::Reset()
extern "C" void U3CWaitAndHideMamaU3Ec__Iterator9_Reset_m1952 (U3CWaitAndHideMamaU3Ec__Iterator9_t333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
