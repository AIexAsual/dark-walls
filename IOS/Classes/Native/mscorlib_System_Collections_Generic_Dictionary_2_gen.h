﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t269;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t2315;
// iTweenEvent/TweenType[]
struct TweenTypeU5BU5D_t2316;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>[]
struct Dictionary_2U5BU5D_t2317;
// System.Collections.Generic.IEqualityComparer`1<iTweenEvent/TweenType>
struct IEqualityComparer_1_t2318;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1104;
// System.Collections.Generic.Dictionary`2/Transform`1<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>,System.Collections.DictionaryEntry>
struct Transform_1_t2319;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct  Dictionary_2_t420  : public Object_t
{
	// System.Int32[] System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::table
	Int32U5BU5D_t269* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::linkSlots
	LinkU5BU5D_t2315* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::keySlots
	TweenTypeU5BU5D_t2316* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::valueSlots
	Dictionary_2U5BU5D_t2317* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::hcp
	Object_t* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::serialization_info
	SerializationInfo_t1104 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::generation
	int32_t ___generation_14;
};
struct Dictionary_2_t420_StaticFields{
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::<>f__am$cacheB
	Transform_1_t2319 * ___U3CU3Ef__amU24cacheB_15;
};
