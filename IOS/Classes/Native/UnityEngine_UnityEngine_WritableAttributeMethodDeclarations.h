﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WritableAttribute
struct WritableAttribute_t1016;

// System.Void UnityEngine.WritableAttribute::.ctor()
extern "C" void WritableAttribute__ctor_m6163 (WritableAttribute_t1016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
