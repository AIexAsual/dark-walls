﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// InteractiveEvent
#include "AssemblyU2DCSharp_InteractiveEvent.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// InteractiveEventManager/OnStateChangeHandler
struct  OnStateChangeHandler_t361  : public MulticastDelegate_t219
{
};
