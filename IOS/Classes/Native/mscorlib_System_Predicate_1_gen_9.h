﻿#pragma once
#include <stdint.h>
// RagdollHelper/BodyPart
struct BodyPart_t400;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<RagdollHelper/BodyPart>
struct  Predicate_1_t2310  : public MulticastDelegate_t219
{
};
