﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Gradients_Hue
struct CameraFilterPack_Gradients_Hue_t149;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Gradients_Hue::.ctor()
extern "C" void CameraFilterPack_Gradients_Hue__ctor_m965 (CameraFilterPack_Gradients_Hue_t149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_Hue::get_material()
extern "C" Material_t2 * CameraFilterPack_Gradients_Hue_get_material_m966 (CameraFilterPack_Gradients_Hue_t149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Hue::Start()
extern "C" void CameraFilterPack_Gradients_Hue_Start_m967 (CameraFilterPack_Gradients_Hue_t149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Hue::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Gradients_Hue_OnRenderImage_m968 (CameraFilterPack_Gradients_Hue_t149 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Hue::Update()
extern "C" void CameraFilterPack_Gradients_Hue_Update_m969 (CameraFilterPack_Gradients_Hue_t149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Hue::OnDisable()
extern "C" void CameraFilterPack_Gradients_Hue_OnDisable_m970 (CameraFilterPack_Gradients_Hue_t149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
