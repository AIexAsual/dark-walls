﻿#pragma once
#include <stdint.h>
// UnityEngine.Light
struct Light_t318;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// goHere
struct  goHere_t417  : public MonoBehaviour_t4
{
	// UnityEngine.Light goHere::mylight
	Light_t318 * ___mylight_2;
	// System.Single goHere::val
	float ___val_3;
};
