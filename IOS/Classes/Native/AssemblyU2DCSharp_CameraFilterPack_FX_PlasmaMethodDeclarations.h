﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Plasma
struct CameraFilterPack_FX_Plasma_t137;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Plasma::.ctor()
extern "C" void CameraFilterPack_FX_Plasma__ctor_m886 (CameraFilterPack_FX_Plasma_t137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Plasma::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Plasma_get_material_m887 (CameraFilterPack_FX_Plasma_t137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Plasma::Start()
extern "C" void CameraFilterPack_FX_Plasma_Start_m888 (CameraFilterPack_FX_Plasma_t137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Plasma::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Plasma_OnRenderImage_m889 (CameraFilterPack_FX_Plasma_t137 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Plasma::OnValidate()
extern "C" void CameraFilterPack_FX_Plasma_OnValidate_m890 (CameraFilterPack_FX_Plasma_t137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Plasma::Update()
extern "C" void CameraFilterPack_FX_Plasma_Update_m891 (CameraFilterPack_FX_Plasma_t137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Plasma::OnDisable()
extern "C" void CameraFilterPack_FX_Plasma_OnDisable_m892 (CameraFilterPack_FX_Plasma_t137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
