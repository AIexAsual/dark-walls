﻿#pragma once
#include <stdint.h>
// System.Reflection.Assembly
struct Assembly_t1535;
// System.Byte[]
struct ByteU5BU5D_t469;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Policy.Hash
struct  Hash_t2010  : public Object_t
{
	// System.Reflection.Assembly System.Security.Policy.Hash::assembly
	Assembly_t1535 * ___assembly_0;
	// System.Byte[] System.Security.Policy.Hash::data
	ByteU5BU5D_t469* ___data_1;
};
