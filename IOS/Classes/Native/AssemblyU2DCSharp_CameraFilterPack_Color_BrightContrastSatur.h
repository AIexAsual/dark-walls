﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Color_BrightContrastSaturation
struct  CameraFilterPack_Color_BrightContrastSaturation_t60  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Color_BrightContrastSaturation::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Color_BrightContrastSaturation::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Color_BrightContrastSaturation::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Color_BrightContrastSaturation::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Color_BrightContrastSaturation::Brightness
	float ___Brightness_6;
	// System.Single CameraFilterPack_Color_BrightContrastSaturation::Saturation
	float ___Saturation_7;
	// System.Single CameraFilterPack_Color_BrightContrastSaturation::Contrast
	float ___Contrast_8;
};
struct CameraFilterPack_Color_BrightContrastSaturation_t60_StaticFields{
	// System.Single CameraFilterPack_Color_BrightContrastSaturation::ChangeBrightness
	float ___ChangeBrightness_9;
	// System.Single CameraFilterPack_Color_BrightContrastSaturation::ChangeSaturation
	float ___ChangeSaturation_10;
	// System.Single CameraFilterPack_Color_BrightContrastSaturation::ChangeContrast
	float ___ChangeContrast_11;
};
