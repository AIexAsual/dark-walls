﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<iTweenEvent/TweenType,System.Object,iTweenEvent/TweenType>
struct Transform_1_t2334;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<iTweenEvent/TweenType,System.Object,iTweenEvent/TweenType>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m14877_gshared (Transform_1_t2334 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m14877(__this, ___object, ___method, method) (( void (*) (Transform_1_t2334 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m14877_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<iTweenEvent/TweenType,System.Object,iTweenEvent/TweenType>::Invoke(TKey,TValue)
extern "C" int32_t Transform_1_Invoke_m14878_gshared (Transform_1_t2334 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Transform_1_Invoke_m14878(__this, ___key, ___value, method) (( int32_t (*) (Transform_1_t2334 *, int32_t, Object_t *, const MethodInfo*))Transform_1_Invoke_m14878_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<iTweenEvent/TweenType,System.Object,iTweenEvent/TweenType>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m14879_gshared (Transform_1_t2334 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m14879(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2334 *, int32_t, Object_t *, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m14879_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<iTweenEvent/TweenType,System.Object,iTweenEvent/TweenType>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Transform_1_EndInvoke_m14880_gshared (Transform_1_t2334 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m14880(__this, ___result, method) (( int32_t (*) (Transform_1_t2334 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m14880_gshared)(__this, ___result, method)
