﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>
struct Enumerator_t2881;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2876;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_29.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22427_gshared (Enumerator_t2881 * __this, Dictionary_2_t2876 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m22427(__this, ___dictionary, method) (( void (*) (Enumerator_t2881 *, Dictionary_2_t2876 *, const MethodInfo*))Enumerator__ctor_m22427_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22428_gshared (Enumerator_t2881 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22428(__this, method) (( Object_t * (*) (Enumerator_t2881 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22428_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t552  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22429_gshared (Enumerator_t2881 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22429(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2881 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22429_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22430_gshared (Enumerator_t2881 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22430(__this, method) (( Object_t * (*) (Enumerator_t2881 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22430_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22431_gshared (Enumerator_t2881 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22431(__this, method) (( Object_t * (*) (Enumerator_t2881 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22431_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22432_gshared (Enumerator_t2881 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22432(__this, method) (( bool (*) (Enumerator_t2881 *, const MethodInfo*))Enumerator_MoveNext_m22432_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" KeyValuePair_2_t2877  Enumerator_get_Current_m22433_gshared (Enumerator_t2881 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22433(__this, method) (( KeyValuePair_2_t2877  (*) (Enumerator_t2881 *, const MethodInfo*))Enumerator_get_Current_m22433_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m22434_gshared (Enumerator_t2881 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m22434(__this, method) (( Object_t * (*) (Enumerator_t2881 *, const MethodInfo*))Enumerator_get_CurrentKey_m22434_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentValue()
extern "C" bool Enumerator_get_CurrentValue_m22435_gshared (Enumerator_t2881 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m22435(__this, method) (( bool (*) (Enumerator_t2881 *, const MethodInfo*))Enumerator_get_CurrentValue_m22435_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyState()
extern "C" void Enumerator_VerifyState_m22436_gshared (Enumerator_t2881 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22436(__this, method) (( void (*) (Enumerator_t2881 *, const MethodInfo*))Enumerator_VerifyState_m22436_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m22437_gshared (Enumerator_t2881 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m22437(__this, method) (( void (*) (Enumerator_t2881 *, const MethodInfo*))Enumerator_VerifyCurrent_m22437_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m22438_gshared (Enumerator_t2881 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22438(__this, method) (( void (*) (Enumerator_t2881 *, const MethodInfo*))Enumerator_Dispose_m22438_gshared)(__this, method)
