﻿#pragma once
#include <stdint.h>
// System.DBNull
struct DBNull_t2087;
// System.Object
#include "mscorlib_System_Object.h"
// System.DBNull
struct  DBNull_t2087  : public Object_t
{
};
struct DBNull_t2087_StaticFields{
	// System.DBNull System.DBNull::Value
	DBNull_t2087 * ___Value_0;
};
