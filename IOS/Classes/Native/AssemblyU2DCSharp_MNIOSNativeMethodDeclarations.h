﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MNIOSNative
struct MNIOSNative_t291;
// System.String
struct String_t;

// System.Void MNIOSNative::.ctor()
extern "C" void MNIOSNative__ctor_m1795 (MNIOSNative_t291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::_MNP_ShowRateUsPopUp(System.String,System.String,System.String,System.String,System.String)
extern "C" void MNIOSNative__MNP_ShowRateUsPopUp_m1796 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___rate, String_t* ___remind, String_t* ___declined, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::_MNP_ShowDialog(System.String,System.String,System.String,System.String)
extern "C" void MNIOSNative__MNP_ShowDialog_m1797 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::_MNP_ShowMessage(System.String,System.String,System.String)
extern "C" void MNIOSNative__MNP_ShowMessage_m1798 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::_MNP_DismissCurrentAlert()
extern "C" void MNIOSNative__MNP_DismissCurrentAlert_m1799 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::_MNP_RedirectToAppStoreRatingPage(System.String)
extern "C" void MNIOSNative__MNP_RedirectToAppStoreRatingPage_m1800 (Object_t * __this /* static, unused */, String_t* ___appId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::_MNP_ShowPreloader()
extern "C" void MNIOSNative__MNP_ShowPreloader_m1801 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::_MNP_HidePreloader()
extern "C" void MNIOSNative__MNP_HidePreloader_m1802 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::dismissCurrentAlert()
extern "C" void MNIOSNative_dismissCurrentAlert_m1803 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::showRateUsPopUP(System.String,System.String,System.String,System.String,System.String)
extern "C" void MNIOSNative_showRateUsPopUP_m1804 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___rate, String_t* ___remind, String_t* ___declined, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::showDialog(System.String,System.String)
extern "C" void MNIOSNative_showDialog_m1805 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::showDialog(System.String,System.String,System.String,System.String)
extern "C" void MNIOSNative_showDialog_m1806 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::showMessage(System.String,System.String)
extern "C" void MNIOSNative_showMessage_m1807 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::showMessage(System.String,System.String,System.String)
extern "C" void MNIOSNative_showMessage_m1808 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::RedirectToAppStoreRatingPage(System.String)
extern "C" void MNIOSNative_RedirectToAppStoreRatingPage_m1809 (Object_t * __this /* static, unused */, String_t* ___appleId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::ShowPreloader()
extern "C" void MNIOSNative_ShowPreloader_m1810 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSNative::HidePreloader()
extern "C" void MNIOSNative_HidePreloader_m1811 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
