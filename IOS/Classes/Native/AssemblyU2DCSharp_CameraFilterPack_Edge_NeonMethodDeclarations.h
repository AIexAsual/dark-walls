﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Edge_Neon
struct CameraFilterPack_Edge_Neon_t115;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Edge_Neon::.ctor()
extern "C" void CameraFilterPack_Edge_Neon__ctor_m738 (CameraFilterPack_Edge_Neon_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Edge_Neon::get_material()
extern "C" Material_t2 * CameraFilterPack_Edge_Neon_get_material_m739 (CameraFilterPack_Edge_Neon_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Neon::Start()
extern "C" void CameraFilterPack_Edge_Neon_Start_m740 (CameraFilterPack_Edge_Neon_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Neon::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Edge_Neon_OnRenderImage_m741 (CameraFilterPack_Edge_Neon_t115 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Neon::OnValidate()
extern "C" void CameraFilterPack_Edge_Neon_OnValidate_m742 (CameraFilterPack_Edge_Neon_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Neon::Update()
extern "C" void CameraFilterPack_Edge_Neon_Update_m743 (CameraFilterPack_Edge_Neon_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Neon::OnDisable()
extern "C" void CameraFilterPack_Edge_Neon_OnDisable_m744 (CameraFilterPack_Edge_Neon_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
