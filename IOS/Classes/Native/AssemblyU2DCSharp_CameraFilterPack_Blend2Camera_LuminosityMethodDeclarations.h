﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_Luminosity
struct CameraFilterPack_Blend2Camera_Luminosity_t35;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_Luminosity::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_Luminosity__ctor_m193 (CameraFilterPack_Blend2Camera_Luminosity_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Luminosity::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_Luminosity_get_material_m194 (CameraFilterPack_Blend2Camera_Luminosity_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Luminosity::Start()
extern "C" void CameraFilterPack_Blend2Camera_Luminosity_Start_m195 (CameraFilterPack_Blend2Camera_Luminosity_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Luminosity::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_Luminosity_OnRenderImage_m196 (CameraFilterPack_Blend2Camera_Luminosity_t35 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Luminosity::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_Luminosity_OnValidate_m197 (CameraFilterPack_Blend2Camera_Luminosity_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Luminosity::Update()
extern "C" void CameraFilterPack_Blend2Camera_Luminosity_Update_m198 (CameraFilterPack_Blend2Camera_Luminosity_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Luminosity::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_Luminosity_OnEnable_m199 (CameraFilterPack_Blend2Camera_Luminosity_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Luminosity::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_Luminosity_OnDisable_m200 (CameraFilterPack_Blend2Camera_Luminosity_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
