﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Space>
struct InternalEnumerator_1_t2385;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Space>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15464_gshared (InternalEnumerator_1_t2385 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15464(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2385 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15464_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Space>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15465_gshared (InternalEnumerator_1_t2385 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15465(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2385 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15465_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Space>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15466_gshared (InternalEnumerator_1_t2385 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15466(__this, method) (( void (*) (InternalEnumerator_1_t2385 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15466_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Space>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15467_gshared (InternalEnumerator_1_t2385 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15467(__this, method) (( bool (*) (InternalEnumerator_1_t2385 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15467_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Space>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m15468_gshared (InternalEnumerator_1_t2385 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15468(__this, method) (( int32_t (*) (InternalEnumerator_1_t2385 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15468_gshared)(__this, method)
