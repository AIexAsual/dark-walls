﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_TV_Vcr
struct  CameraFilterPack_TV_Vcr_t196  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_TV_Vcr::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_TV_Vcr::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_TV_Vcr::Distortion
	float ___Distortion_4;
	// UnityEngine.Material CameraFilterPack_TV_Vcr::SCMaterial
	Material_t2 * ___SCMaterial_5;
};
struct CameraFilterPack_TV_Vcr_t196_StaticFields{
	// System.Single CameraFilterPack_TV_Vcr::ChangeDistortion
	float ___ChangeDistortion_6;
};
