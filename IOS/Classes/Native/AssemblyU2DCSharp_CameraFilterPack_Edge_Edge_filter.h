﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Edge_Edge_filter
struct  CameraFilterPack_Edge_Edge_filter_t113  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Edge_Edge_filter::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Edge_Edge_filter::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Edge_Edge_filter::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Edge_Edge_filter::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Edge_Edge_filter::RedAmplifier
	float ___RedAmplifier_6;
	// System.Single CameraFilterPack_Edge_Edge_filter::GreenAmplifier
	float ___GreenAmplifier_7;
	// System.Single CameraFilterPack_Edge_Edge_filter::BlueAmplifier
	float ___BlueAmplifier_8;
};
struct CameraFilterPack_Edge_Edge_filter_t113_StaticFields{
	// System.Single CameraFilterPack_Edge_Edge_filter::ChangeRedAmplifier
	float ___ChangeRedAmplifier_9;
	// System.Single CameraFilterPack_Edge_Edge_filter::ChangeGreenAmplifier
	float ___ChangeGreenAmplifier_10;
	// System.Single CameraFilterPack_Edge_Edge_filter::ChangeBlueAmplifier
	float ___ChangeBlueAmplifier_11;
};
