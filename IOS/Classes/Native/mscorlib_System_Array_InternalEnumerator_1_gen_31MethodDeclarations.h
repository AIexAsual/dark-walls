﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<ArrayIndexes>
struct InternalEnumerator_1_t2389;
// System.Object
struct Object_t;
// ArrayIndexes
struct ArrayIndexes_t441;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<ArrayIndexes>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15484(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2389 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13479_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<ArrayIndexes>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15485(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2389 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13480_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ArrayIndexes>::Dispose()
#define InternalEnumerator_1_Dispose_m15486(__this, method) (( void (*) (InternalEnumerator_1_t2389 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13481_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ArrayIndexes>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15487(__this, method) (( bool (*) (InternalEnumerator_1_t2389 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13482_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ArrayIndexes>::get_Current()
#define InternalEnumerator_1_get_Current_m15488(__this, method) (( ArrayIndexes_t441 * (*) (InternalEnumerator_1_t2389 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13483_gshared)(__this, method)
