﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// InteractivePlasticBag
struct InteractivePlasticBag_t335;
// System.Object
#include "mscorlib_System_Object.h"
// InteractivePlasticBag/<WaitAndWiggle>c__IteratorA
struct  U3CWaitAndWiggleU3Ec__IteratorA_t336  : public Object_t
{
	// System.Single InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::time
	float ___time_0;
	// System.Int32 InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::$PC
	int32_t ___U24PC_1;
	// System.Object InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::$current
	Object_t * ___U24current_2;
	// System.Single InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::<$>time
	float ___U3CU24U3Etime_3;
	// InteractivePlasticBag InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::<>f__this
	InteractivePlasticBag_t335 * ___U3CU3Ef__this_4;
};
