﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ContextBoundObject
struct ContextBoundObject_t2086;

// System.Void System.ContextBoundObject::.ctor()
extern "C" void ContextBoundObject__ctor_m12671 (ContextBoundObject_t2086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
