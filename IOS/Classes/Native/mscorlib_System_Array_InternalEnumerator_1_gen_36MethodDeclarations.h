﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>
struct InternalEnumerator_1_t2592;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UI.InputField/ContentType
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18716_gshared (InternalEnumerator_1_t2592 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m18716(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2592 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m18716_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18717_gshared (InternalEnumerator_1_t2592 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18717(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2592 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18717_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18718_gshared (InternalEnumerator_1_t2592 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18718(__this, method) (( void (*) (InternalEnumerator_1_t2592 *, const MethodInfo*))InternalEnumerator_1_Dispose_m18718_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18719_gshared (InternalEnumerator_1_t2592 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18719(__this, method) (( bool (*) (InternalEnumerator_1_t2592 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m18719_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m18720_gshared (InternalEnumerator_1_t2592 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18720(__this, method) (( int32_t (*) (InternalEnumerator_1_t2592 *, const MethodInfo*))InternalEnumerator_1_get_Current_m18720_gshared)(__this, method)
