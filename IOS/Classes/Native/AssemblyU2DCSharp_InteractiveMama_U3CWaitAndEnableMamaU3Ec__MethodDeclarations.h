﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractiveMama/<WaitAndEnableMama>c__Iterator7
struct U3CWaitAndEnableMamaU3Ec__Iterator7_t331;
// System.Object
struct Object_t;

// System.Void InteractiveMama/<WaitAndEnableMama>c__Iterator7::.ctor()
extern "C" void U3CWaitAndEnableMamaU3Ec__Iterator7__ctor_m1935 (U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InteractiveMama/<WaitAndEnableMama>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndEnableMamaU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1936 (U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InteractiveMama/<WaitAndEnableMama>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndEnableMamaU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1937 (U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InteractiveMama/<WaitAndEnableMama>c__Iterator7::MoveNext()
extern "C" bool U3CWaitAndEnableMamaU3Ec__Iterator7_MoveNext_m1938 (U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveMama/<WaitAndEnableMama>c__Iterator7::Dispose()
extern "C" void U3CWaitAndEnableMamaU3Ec__Iterator7_Dispose_m1939 (U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveMama/<WaitAndEnableMama>c__Iterator7::Reset()
extern "C" void U3CWaitAndEnableMamaU3Ec__Iterator7_Reset_m1940 (U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
