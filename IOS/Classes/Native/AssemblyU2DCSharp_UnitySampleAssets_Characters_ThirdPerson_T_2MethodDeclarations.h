﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter
struct ThirdPersonCharacter_t413;
// UnityEngine.Transform
struct Transform_t243;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::.ctor()
extern "C" void ThirdPersonCharacter__ctor_m2367 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::get_lookTarget()
extern "C" Transform_t243 * ThirdPersonCharacter_get_lookTarget_m2368 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::set_lookTarget(UnityEngine.Transform)
extern "C" void ThirdPersonCharacter_set_lookTarget_m2369 (ThirdPersonCharacter_t413 * __this, Transform_t243 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::Start()
extern "C" void ThirdPersonCharacter_Start_m2370 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::BlendLookWeight()
extern "C" Object_t * ThirdPersonCharacter_BlendLookWeight_m2371 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::OnEnable()
extern "C" void ThirdPersonCharacter_OnEnable_m2372 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::Move(UnityEngine.Vector3,System.Boolean,System.Boolean,UnityEngine.Vector3)
extern "C" void ThirdPersonCharacter_Move_m2373 (ThirdPersonCharacter_t413 * __this, Vector3_t215  ___move, bool ___crouch, bool ___jump, Vector3_t215  ___lookPos, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::ConvertMoveInput()
extern "C" void ThirdPersonCharacter_ConvertMoveInput_m2374 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::TurnTowardsCameraForward()
extern "C" void ThirdPersonCharacter_TurnTowardsCameraForward_m2375 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::PreventStandingInLowHeadroom()
extern "C" void ThirdPersonCharacter_PreventStandingInLowHeadroom_m2376 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::ScaleCapsuleForCrouching()
extern "C" void ThirdPersonCharacter_ScaleCapsuleForCrouching_m2377 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::ApplyExtraTurnRotation()
extern "C" void ThirdPersonCharacter_ApplyExtraTurnRotation_m2378 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::GroundCheck()
extern "C" void ThirdPersonCharacter_GroundCheck_m2379 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::SetFriction()
extern "C" void ThirdPersonCharacter_SetFriction_m2380 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::HandleGroundedVelocities()
extern "C" void ThirdPersonCharacter_HandleGroundedVelocities_m2381 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::HandleAirborneVelocities()
extern "C" void ThirdPersonCharacter_HandleAirborneVelocities_m2382 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::UpdateAnimator()
extern "C" void ThirdPersonCharacter_UpdateAnimator_m2383 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::OnAnimatorIK(System.Int32)
extern "C" void ThirdPersonCharacter_OnAnimatorIK_m2384 (ThirdPersonCharacter_t413 * __this, int32_t ___layerIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::SetUpAnimator()
extern "C" void ThirdPersonCharacter_SetUpAnimator_m2385 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::OnAnimatorMove()
extern "C" void ThirdPersonCharacter_OnAnimatorMove_m2386 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::OnDisable()
extern "C" void ThirdPersonCharacter_OnDisable_m2387 (ThirdPersonCharacter_t413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
