﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_VividLight
struct CameraFilterPack_Blend2Camera_VividLight_t46;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_VividLight::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_VividLight__ctor_m274 (CameraFilterPack_Blend2Camera_VividLight_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_VividLight::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_VividLight_get_material_m275 (CameraFilterPack_Blend2Camera_VividLight_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_VividLight::Start()
extern "C" void CameraFilterPack_Blend2Camera_VividLight_Start_m276 (CameraFilterPack_Blend2Camera_VividLight_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_VividLight::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_VividLight_OnRenderImage_m277 (CameraFilterPack_Blend2Camera_VividLight_t46 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_VividLight::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_VividLight_OnValidate_m278 (CameraFilterPack_Blend2Camera_VividLight_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_VividLight::Update()
extern "C" void CameraFilterPack_Blend2Camera_VividLight_Update_m279 (CameraFilterPack_Blend2Camera_VividLight_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_VividLight::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_VividLight_OnEnable_m280 (CameraFilterPack_Blend2Camera_VividLight_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_VividLight::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_VividLight_OnDisable_m281 (CameraFilterPack_Blend2Camera_VividLight_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
