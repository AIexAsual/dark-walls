﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Light_Water2
struct CameraFilterPack_Light_Water2_t158;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Light_Water2::.ctor()
extern "C" void CameraFilterPack_Light_Water2__ctor_m1022 (CameraFilterPack_Light_Water2_t158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Light_Water2::get_material()
extern "C" Material_t2 * CameraFilterPack_Light_Water2_get_material_m1023 (CameraFilterPack_Light_Water2_t158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water2::Start()
extern "C" void CameraFilterPack_Light_Water2_Start_m1024 (CameraFilterPack_Light_Water2_t158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Light_Water2_OnRenderImage_m1025 (CameraFilterPack_Light_Water2_t158 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water2::OnValidate()
extern "C" void CameraFilterPack_Light_Water2_OnValidate_m1026 (CameraFilterPack_Light_Water2_t158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water2::Update()
extern "C" void CameraFilterPack_Light_Water2_Update_m1027 (CameraFilterPack_Light_Water2_t158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water2::OnDisable()
extern "C" void CameraFilterPack_Light_Water2_OnDisable_m1028 (CameraFilterPack_Light_Water2_t158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
