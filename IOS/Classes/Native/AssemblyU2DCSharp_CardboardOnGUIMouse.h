﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture
struct Texture_t221;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// CardboardOnGUIMouse
struct  CardboardOnGUIMouse_t222  : public MonoBehaviour_t4
{
	// UnityEngine.Texture CardboardOnGUIMouse::pointerImage
	Texture_t221 * ___pointerImage_2;
	// UnityEngine.Vector2 CardboardOnGUIMouse::pointerSize
	Vector2_t7  ___pointerSize_3;
	// UnityEngine.Vector2 CardboardOnGUIMouse::pointerSpot
	Vector2_t7  ___pointerSpot_4;
	// System.Boolean CardboardOnGUIMouse::pointerVisible
	bool ___pointerVisible_5;
	// System.Int32 CardboardOnGUIMouse::pointerX
	int32_t ___pointerX_6;
	// System.Int32 CardboardOnGUIMouse::pointerY
	int32_t ___pointerY_7;
};
