﻿#pragma once
#include <stdint.h>
// System.Reflection.Assembly
struct Assembly_t1535;
// System.Object
struct Object_t;
// System.ResolveEventArgs
struct ResolveEventArgs_t2131;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.ResolveEventHandler
struct  ResolveEventHandler_t2075  : public MulticastDelegate_t219
{
};
