﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
struct Enumerator_t2711;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t2705;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m20463_gshared (Enumerator_t2711 * __this, Dictionary_2_t2705 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m20463(__this, ___dictionary, method) (( void (*) (Enumerator_t2711 *, Dictionary_2_t2705 *, const MethodInfo*))Enumerator__ctor_m20463_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20464_gshared (Enumerator_t2711 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20464(__this, method) (( Object_t * (*) (Enumerator_t2711 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20464_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t552  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20465_gshared (Enumerator_t2711 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20465(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2711 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20465_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20466_gshared (Enumerator_t2711 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20466(__this, method) (( Object_t * (*) (Enumerator_t2711 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20466_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20467_gshared (Enumerator_t2711 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20467(__this, method) (( Object_t * (*) (Enumerator_t2711 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20467_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20468_gshared (Enumerator_t2711 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m20468(__this, method) (( bool (*) (Enumerator_t2711 *, const MethodInfo*))Enumerator_MoveNext_m20468_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_Current()
extern "C" KeyValuePair_2_t2706  Enumerator_get_Current_m20469_gshared (Enumerator_t2711 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m20469(__this, method) (( KeyValuePair_2_t2706  (*) (Enumerator_t2711 *, const MethodInfo*))Enumerator_get_Current_m20469_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m20470_gshared (Enumerator_t2711 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m20470(__this, method) (( Object_t * (*) (Enumerator_t2711 *, const MethodInfo*))Enumerator_get_CurrentKey_m20470_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_CurrentValue()
extern "C" int64_t Enumerator_get_CurrentValue_m20471_gshared (Enumerator_t2711 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m20471(__this, method) (( int64_t (*) (Enumerator_t2711 *, const MethodInfo*))Enumerator_get_CurrentValue_m20471_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::VerifyState()
extern "C" void Enumerator_VerifyState_m20472_gshared (Enumerator_t2711 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m20472(__this, method) (( void (*) (Enumerator_t2711 *, const MethodInfo*))Enumerator_VerifyState_m20472_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m20473_gshared (Enumerator_t2711 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m20473(__this, method) (( void (*) (Enumerator_t2711 *, const MethodInfo*))Enumerator_VerifyCurrent_m20473_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::Dispose()
extern "C" void Enumerator_Dispose_m20474_gshared (Enumerator_t2711 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m20474(__this, method) (( void (*) (Enumerator_t2711 *, const MethodInfo*))Enumerator_Dispose_m20474_gshared)(__this, method)
