﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Char>
struct DefaultComparer_t2295;

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Char>::.ctor()
extern "C" void DefaultComparer__ctor_m14466_gshared (DefaultComparer_t2295 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m14466(__this, method) (( void (*) (DefaultComparer_t2295 *, const MethodInfo*))DefaultComparer__ctor_m14466_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Char>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m14467_gshared (DefaultComparer_t2295 * __this, uint16_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m14467(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2295 *, uint16_t, const MethodInfo*))DefaultComparer_GetHashCode_m14467_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Char>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m14468_gshared (DefaultComparer_t2295 * __this, uint16_t ___x, uint16_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m14468(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2295 *, uint16_t, uint16_t, const MethodInfo*))DefaultComparer_Equals_m14468_gshared)(__this, ___x, ___y, method)
