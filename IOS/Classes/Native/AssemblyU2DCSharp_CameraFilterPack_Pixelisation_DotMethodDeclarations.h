﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Pixelisation_Dot
struct CameraFilterPack_Pixelisation_Dot_t170;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Pixelisation_Dot::.ctor()
extern "C" void CameraFilterPack_Pixelisation_Dot__ctor_m1098 (CameraFilterPack_Pixelisation_Dot_t170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Pixelisation_Dot::get_material()
extern "C" Material_t2 * CameraFilterPack_Pixelisation_Dot_get_material_m1099 (CameraFilterPack_Pixelisation_Dot_t170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_Dot::Start()
extern "C" void CameraFilterPack_Pixelisation_Dot_Start_m1100 (CameraFilterPack_Pixelisation_Dot_t170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_Dot::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Pixelisation_Dot_OnRenderImage_m1101 (CameraFilterPack_Pixelisation_Dot_t170 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_Dot::OnValidate()
extern "C" void CameraFilterPack_Pixelisation_Dot_OnValidate_m1102 (CameraFilterPack_Pixelisation_Dot_t170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_Dot::Update()
extern "C" void CameraFilterPack_Pixelisation_Dot_Update_m1103 (CameraFilterPack_Pixelisation_Dot_t170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_Dot::OnDisable()
extern "C" void CameraFilterPack_Pixelisation_Dot_OnDisable_m1104 (CameraFilterPack_Pixelisation_Dot_t170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
