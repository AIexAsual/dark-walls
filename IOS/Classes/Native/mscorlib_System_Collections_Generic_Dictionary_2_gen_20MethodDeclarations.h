﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t2513;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3012;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1094;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Object>
struct KeyCollection_t2516;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>
struct ValueCollection_t2520;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2510;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Object>
struct IDictionary_2_t3077;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1104;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t3078;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerator_1_t3079;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1527;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m17432_gshared (Dictionary_2_t2513 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m17432(__this, method) (( void (*) (Dictionary_2_t2513 *, const MethodInfo*))Dictionary_2__ctor_m17432_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m17434_gshared (Dictionary_2_t2513 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m17434(__this, ___comparer, method) (( void (*) (Dictionary_2_t2513 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17434_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m17436_gshared (Dictionary_2_t2513 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m17436(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2513 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17436_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m17438_gshared (Dictionary_2_t2513 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m17438(__this, ___capacity, method) (( void (*) (Dictionary_2_t2513 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m17438_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m17440_gshared (Dictionary_2_t2513 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m17440(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2513 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17440_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m17442_gshared (Dictionary_2_t2513 * __this, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m17442(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2513 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2__ctor_m17442_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m17444_gshared (Dictionary_2_t2513 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m17444(__this, method) (( Object_t* (*) (Dictionary_2_t2513 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m17444_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m17446_gshared (Dictionary_2_t2513 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m17446(__this, method) (( Object_t* (*) (Dictionary_2_t2513 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m17446_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m17448_gshared (Dictionary_2_t2513 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m17448(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2513 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m17448_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m17450_gshared (Dictionary_2_t2513 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m17450(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2513 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m17450_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m17452_gshared (Dictionary_2_t2513 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m17452(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2513 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m17452_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m17454_gshared (Dictionary_2_t2513 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m17454(__this, ___key, method) (( bool (*) (Dictionary_2_t2513 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m17454_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m17456_gshared (Dictionary_2_t2513 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m17456(__this, ___key, method) (( void (*) (Dictionary_2_t2513 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m17456_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17458_gshared (Dictionary_2_t2513 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17458(__this, method) (( bool (*) (Dictionary_2_t2513 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17458_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17460_gshared (Dictionary_2_t2513 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17460(__this, method) (( Object_t * (*) (Dictionary_2_t2513 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17460_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17462_gshared (Dictionary_2_t2513 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17462(__this, method) (( bool (*) (Dictionary_2_t2513 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17462_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17464_gshared (Dictionary_2_t2513 * __this, KeyValuePair_2_t2514  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17464(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2513 *, KeyValuePair_2_t2514 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17464_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17466_gshared (Dictionary_2_t2513 * __this, KeyValuePair_2_t2514  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17466(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2513 *, KeyValuePair_2_t2514 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17466_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17468_gshared (Dictionary_2_t2513 * __this, KeyValuePair_2U5BU5D_t3078* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17468(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2513 *, KeyValuePair_2U5BU5D_t3078*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17468_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17470_gshared (Dictionary_2_t2513 * __this, KeyValuePair_2_t2514  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17470(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2513 *, KeyValuePair_2_t2514 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17470_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m17472_gshared (Dictionary_2_t2513 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m17472(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2513 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m17472_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17474_gshared (Dictionary_2_t2513 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17474(__this, method) (( Object_t * (*) (Dictionary_2_t2513 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17474_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17476_gshared (Dictionary_2_t2513 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17476(__this, method) (( Object_t* (*) (Dictionary_2_t2513 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17476_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17478_gshared (Dictionary_2_t2513 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17478(__this, method) (( Object_t * (*) (Dictionary_2_t2513 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17478_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m17480_gshared (Dictionary_2_t2513 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m17480(__this, method) (( int32_t (*) (Dictionary_2_t2513 *, const MethodInfo*))Dictionary_2_get_Count_m17480_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m17482_gshared (Dictionary_2_t2513 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m17482(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2513 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m17482_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m17484_gshared (Dictionary_2_t2513 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m17484(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2513 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m17484_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m17486_gshared (Dictionary_2_t2513 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m17486(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2513 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m17486_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m17488_gshared (Dictionary_2_t2513 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m17488(__this, ___size, method) (( void (*) (Dictionary_2_t2513 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m17488_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m17490_gshared (Dictionary_2_t2513 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m17490(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2513 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m17490_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2514  Dictionary_2_make_pair_m17492_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m17492(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2514  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m17492_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m17494_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m17494(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m17494_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m17496_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m17496(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m17496_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m17498_gshared (Dictionary_2_t2513 * __this, KeyValuePair_2U5BU5D_t3078* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m17498(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2513 *, KeyValuePair_2U5BU5D_t3078*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m17498_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m17500_gshared (Dictionary_2_t2513 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m17500(__this, method) (( void (*) (Dictionary_2_t2513 *, const MethodInfo*))Dictionary_2_Resize_m17500_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m17502_gshared (Dictionary_2_t2513 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m17502(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2513 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m17502_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m17504_gshared (Dictionary_2_t2513 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m17504(__this, method) (( void (*) (Dictionary_2_t2513 *, const MethodInfo*))Dictionary_2_Clear_m17504_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m17506_gshared (Dictionary_2_t2513 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m17506(__this, ___key, method) (( bool (*) (Dictionary_2_t2513 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m17506_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m17508_gshared (Dictionary_2_t2513 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m17508(__this, ___value, method) (( bool (*) (Dictionary_2_t2513 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m17508_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m17510_gshared (Dictionary_2_t2513 * __this, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m17510(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2513 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2_GetObjectData_m17510_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m17512_gshared (Dictionary_2_t2513 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m17512(__this, ___sender, method) (( void (*) (Dictionary_2_t2513 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m17512_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m17514_gshared (Dictionary_2_t2513 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m17514(__this, ___key, method) (( bool (*) (Dictionary_2_t2513 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m17514_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m17516_gshared (Dictionary_2_t2513 * __this, int32_t ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m17516(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2513 *, int32_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m17516_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Keys()
extern "C" KeyCollection_t2516 * Dictionary_2_get_Keys_m17518_gshared (Dictionary_2_t2513 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m17518(__this, method) (( KeyCollection_t2516 * (*) (Dictionary_2_t2513 *, const MethodInfo*))Dictionary_2_get_Keys_m17518_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Values()
extern "C" ValueCollection_t2520 * Dictionary_2_get_Values_m17519_gshared (Dictionary_2_t2513 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m17519(__this, method) (( ValueCollection_t2520 * (*) (Dictionary_2_t2513 *, const MethodInfo*))Dictionary_2_get_Values_m17519_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m17521_gshared (Dictionary_2_t2513 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m17521(__this, ___key, method) (( int32_t (*) (Dictionary_2_t2513 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m17521_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m17523_gshared (Dictionary_2_t2513 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m17523(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t2513 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m17523_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m17525_gshared (Dictionary_2_t2513 * __this, KeyValuePair_2_t2514  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m17525(__this, ___pair, method) (( bool (*) (Dictionary_2_t2513 *, KeyValuePair_2_t2514 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m17525_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetEnumerator()
extern "C" Enumerator_t2518  Dictionary_2_GetEnumerator_m17526_gshared (Dictionary_2_t2513 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m17526(__this, method) (( Enumerator_t2518  (*) (Dictionary_2_t2513 *, const MethodInfo*))Dictionary_2_GetEnumerator_m17526_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t552  Dictionary_2_U3CCopyToU3Em__0_m17528_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m17528(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t552  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m17528_gshared)(__this /* static, unused */, ___key, ___value, method)
