﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_Posterize
struct CameraFilterPack_TV_Posterize_t191;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_Posterize::.ctor()
extern "C" void CameraFilterPack_TV_Posterize__ctor_m1236 (CameraFilterPack_TV_Posterize_t191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Posterize::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_Posterize_get_material_m1237 (CameraFilterPack_TV_Posterize_t191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Posterize::Start()
extern "C" void CameraFilterPack_TV_Posterize_Start_m1238 (CameraFilterPack_TV_Posterize_t191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Posterize::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_Posterize_OnRenderImage_m1239 (CameraFilterPack_TV_Posterize_t191 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Posterize::OnValidate()
extern "C" void CameraFilterPack_TV_Posterize_OnValidate_m1240 (CameraFilterPack_TV_Posterize_t191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Posterize::Update()
extern "C" void CameraFilterPack_TV_Posterize_Update_m1241 (CameraFilterPack_TV_Posterize_t191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Posterize::OnDisable()
extern "C" void CameraFilterPack_TV_Posterize_OnDisable_m1242 (CameraFilterPack_TV_Posterize_t191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
