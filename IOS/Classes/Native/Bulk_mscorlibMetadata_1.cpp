﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// Mono.Globalization.Unicode.SimpleCollator
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator.h"
// Metadata Definition Mono.Globalization.Unicode.SimpleCollator
extern TypeInfo SimpleCollator_t1595_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.SimpleCollator
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollatorMethodDeclarations.h"
extern const Il2CppType Context_t1591_0_0_0;
extern const Il2CppType PreviousInfo_t1592_0_0_0;
extern const Il2CppType Escape_t1593_0_0_0;
extern const Il2CppType ExtenderType_t1594_0_0_0;
static const Il2CppType* SimpleCollator_t1595_il2cpp_TypeInfo__nestedTypes[4] =
{
	&Context_t1591_0_0_0,
	&PreviousInfo_t1592_0_0_0,
	&Escape_t1593_0_0_0,
	&ExtenderType_t1594_0_0_0,
};
static const EncodedMethodIndex SimpleCollator_t1595_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SimpleCollator_t1595_0_0_0;
extern const Il2CppType SimpleCollator_t1595_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct SimpleCollator_t1595;
const Il2CppTypeDefinitionMetadata SimpleCollator_t1595_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SimpleCollator_t1595_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SimpleCollator_t1595_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6591/* fieldStart */
	, 9302/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SimpleCollator_t1595_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SimpleCollator"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &SimpleCollator_t1595_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SimpleCollator_t1595_0_0_0/* byval_arg */
	, &SimpleCollator_t1595_1_0_0/* this_arg */
	, &SimpleCollator_t1595_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SimpleCollator_t1595)/* instance_size */
	, sizeof (SimpleCollator_t1595)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SimpleCollator_t1595_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 52/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 4/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.SimpleCollator/Context
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Context.h"
// Metadata Definition Mono.Globalization.Unicode.SimpleCollator/Context
extern TypeInfo Context_t1591_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.SimpleCollator/Context
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ContextMethodDeclarations.h"
static const EncodedMethodIndex Context_t1591_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Context_t1591_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata Context_t1591_DefinitionMetadata = 
{
	&SimpleCollator_t1595_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Context_t1591_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6604/* fieldStart */
	, 9354/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Context_t1591_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Context"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Context_t1591_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Context_t1591_0_0_0/* byval_arg */
	, &Context_t1591_1_0_0/* this_arg */
	, &Context_t1591_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Context_t1591_marshal/* marshal_to_native_func */
	, (methodPointerType)Context_t1591_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Context_t1591_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Context_t1591)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Context_t1591)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Context_t1591_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.SimpleCollator/PreviousInfo
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_PreviousI.h"
// Metadata Definition Mono.Globalization.Unicode.SimpleCollator/PreviousInfo
extern TypeInfo PreviousInfo_t1592_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.SimpleCollator/PreviousInfo
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_PreviousIMethodDeclarations.h"
static const EncodedMethodIndex PreviousInfo_t1592_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PreviousInfo_t1592_1_0_0;
const Il2CppTypeDefinitionMetadata PreviousInfo_t1592_DefinitionMetadata = 
{
	&SimpleCollator_t1595_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, PreviousInfo_t1592_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6612/* fieldStart */
	, 9355/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PreviousInfo_t1592_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PreviousInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &PreviousInfo_t1592_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PreviousInfo_t1592_0_0_0/* byval_arg */
	, &PreviousInfo_t1592_1_0_0/* this_arg */
	, &PreviousInfo_t1592_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)PreviousInfo_t1592_marshal/* marshal_to_native_func */
	, (methodPointerType)PreviousInfo_t1592_marshal_back/* marshal_from_native_func */
	, (methodPointerType)PreviousInfo_t1592_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (PreviousInfo_t1592)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PreviousInfo_t1592)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(PreviousInfo_t1592_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.SimpleCollator/Escape
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Escape.h"
// Metadata Definition Mono.Globalization.Unicode.SimpleCollator/Escape
extern TypeInfo Escape_t1593_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.SimpleCollator/Escape
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_EscapeMethodDeclarations.h"
static const EncodedMethodIndex Escape_t1593_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Escape_t1593_1_0_0;
const Il2CppTypeDefinitionMetadata Escape_t1593_DefinitionMetadata = 
{
	&SimpleCollator_t1595_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Escape_t1593_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6614/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Escape_t1593_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Escape"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Escape_t1593_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Escape_t1593_0_0_0/* byval_arg */
	, &Escape_t1593_1_0_0/* this_arg */
	, &Escape_t1593_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Escape_t1593_marshal/* marshal_to_native_func */
	, (methodPointerType)Escape_t1593_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Escape_t1593_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Escape_t1593)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Escape_t1593)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Escape_t1593_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.SimpleCollator/ExtenderType
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderT.h"
// Metadata Definition Mono.Globalization.Unicode.SimpleCollator/ExtenderType
extern TypeInfo ExtenderType_t1594_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.SimpleCollator/ExtenderType
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderTMethodDeclarations.h"
static const EncodedMethodIndex ExtenderType_t1594_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair ExtenderType_t1594_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ExtenderType_t1594_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata ExtenderType_t1594_DefinitionMetadata = 
{
	&SimpleCollator_t1595_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExtenderType_t1594_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ExtenderType_t1594_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6619/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ExtenderType_t1594_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExtenderType"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExtenderType_t1594_0_0_0/* byval_arg */
	, &ExtenderType_t1594_1_0_0/* this_arg */
	, &ExtenderType_t1594_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExtenderType_t1594)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ExtenderType_t1594)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Globalization.SortKey
#include "mscorlib_System_Globalization_SortKey.h"
// Metadata Definition System.Globalization.SortKey
extern TypeInfo SortKey_t1599_il2cpp_TypeInfo;
// System.Globalization.SortKey
#include "mscorlib_System_Globalization_SortKeyMethodDeclarations.h"
static const EncodedMethodIndex SortKey_t1599_VTable[6] = 
{
	2701,
	601,
	2702,
	2703,
	2704,
	2705,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SortKey_t1599_0_0_0;
extern const Il2CppType SortKey_t1599_1_0_0;
struct SortKey_t1599;
const Il2CppTypeDefinitionMetadata SortKey_t1599_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SortKey_t1599_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6625/* fieldStart */
	, 9356/* methodStart */
	, -1/* eventStart */
	, 1760/* propertyStart */

};
TypeInfo SortKey_t1599_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SortKey"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &SortKey_t1599_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2667/* custom_attributes_cache */
	, &SortKey_t1599_0_0_0/* byval_arg */
	, &SortKey_t1599_1_0_0/* this_arg */
	, &SortKey_t1599_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SortKey_t1599)/* instance_size */
	, sizeof (SortKey_t1599)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.SortKeyBuffer
#include "mscorlib_Mono_Globalization_Unicode_SortKeyBuffer.h"
// Metadata Definition Mono.Globalization.Unicode.SortKeyBuffer
extern TypeInfo SortKeyBuffer_t1600_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.SortKeyBuffer
#include "mscorlib_Mono_Globalization_Unicode_SortKeyBufferMethodDeclarations.h"
static const EncodedMethodIndex SortKeyBuffer_t1600_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SortKeyBuffer_t1600_0_0_0;
extern const Il2CppType SortKeyBuffer_t1600_1_0_0;
struct SortKeyBuffer_t1600;
const Il2CppTypeDefinitionMetadata SortKeyBuffer_t1600_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SortKeyBuffer_t1600_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6629/* fieldStart */
	, 9364/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SortKeyBuffer_t1600_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SortKeyBuffer"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &SortKeyBuffer_t1600_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SortKeyBuffer_t1600_0_0_0/* byval_arg */
	, &SortKeyBuffer_t1600_1_0_0/* this_arg */
	, &SortKeyBuffer_t1600_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SortKeyBuffer_t1600)/* instance_size */
	, sizeof (SortKeyBuffer_t1600)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 22/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.Prime.Generator.PrimeGeneratorBase
#include "mscorlib_Mono_Math_Prime_Generator_PrimeGeneratorBase.h"
// Metadata Definition Mono.Math.Prime.Generator.PrimeGeneratorBase
extern TypeInfo PrimeGeneratorBase_t1601_il2cpp_TypeInfo;
// Mono.Math.Prime.Generator.PrimeGeneratorBase
#include "mscorlib_Mono_Math_Prime_Generator_PrimeGeneratorBaseMethodDeclarations.h"
static const EncodedMethodIndex PrimeGeneratorBase_t1601_VTable[8] = 
{
	626,
	601,
	627,
	628,
	2706,
	2707,
	2708,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PrimeGeneratorBase_t1601_0_0_0;
extern const Il2CppType PrimeGeneratorBase_t1601_1_0_0;
struct PrimeGeneratorBase_t1601;
const Il2CppTypeDefinitionMetadata PrimeGeneratorBase_t1601_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PrimeGeneratorBase_t1601_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9375/* methodStart */
	, -1/* eventStart */
	, 1762/* propertyStart */

};
TypeInfo PrimeGeneratorBase_t1601_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimeGeneratorBase"/* name */
	, "Mono.Math.Prime.Generator"/* namespaze */
	, NULL/* methods */
	, &PrimeGeneratorBase_t1601_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimeGeneratorBase_t1601_0_0_0/* byval_arg */
	, &PrimeGeneratorBase_t1601_1_0_0/* this_arg */
	, &PrimeGeneratorBase_t1601_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimeGeneratorBase_t1601)/* instance_size */
	, sizeof (PrimeGeneratorBase_t1601)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase
#include "mscorlib_Mono_Math_Prime_Generator_SequentialSearchPrimeGene.h"
// Metadata Definition Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase
extern TypeInfo SequentialSearchPrimeGeneratorBase_t1602_il2cpp_TypeInfo;
// Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase
#include "mscorlib_Mono_Math_Prime_Generator_SequentialSearchPrimeGeneMethodDeclarations.h"
static const EncodedMethodIndex SequentialSearchPrimeGeneratorBase_t1602_VTable[11] = 
{
	626,
	601,
	627,
	628,
	2706,
	2707,
	2708,
	2709,
	2710,
	2711,
	2712,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SequentialSearchPrimeGeneratorBase_t1602_0_0_0;
extern const Il2CppType SequentialSearchPrimeGeneratorBase_t1602_1_0_0;
struct SequentialSearchPrimeGeneratorBase_t1602;
const Il2CppTypeDefinitionMetadata SequentialSearchPrimeGeneratorBase_t1602_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PrimeGeneratorBase_t1601_0_0_0/* parent */
	, SequentialSearchPrimeGeneratorBase_t1602_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9380/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SequentialSearchPrimeGeneratorBase_t1602_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SequentialSearchPrimeGeneratorBase"/* name */
	, "Mono.Math.Prime.Generator"/* namespaze */
	, NULL/* methods */
	, &SequentialSearchPrimeGeneratorBase_t1602_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SequentialSearchPrimeGeneratorBase_t1602_0_0_0/* byval_arg */
	, &SequentialSearchPrimeGeneratorBase_t1602_1_0_0/* this_arg */
	, &SequentialSearchPrimeGeneratorBase_t1602_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SequentialSearchPrimeGeneratorBase_t1602)/* instance_size */
	, sizeof (SequentialSearchPrimeGeneratorBase_t1602)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.Prime.ConfidenceFactor
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
// Metadata Definition Mono.Math.Prime.ConfidenceFactor
extern TypeInfo ConfidenceFactor_t1603_il2cpp_TypeInfo;
// Mono.Math.Prime.ConfidenceFactor
#include "mscorlib_Mono_Math_Prime_ConfidenceFactorMethodDeclarations.h"
static const EncodedMethodIndex ConfidenceFactor_t1603_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ConfidenceFactor_t1603_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConfidenceFactor_t1603_0_0_0;
extern const Il2CppType ConfidenceFactor_t1603_1_0_0;
const Il2CppTypeDefinitionMetadata ConfidenceFactor_t1603_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConfidenceFactor_t1603_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ConfidenceFactor_t1603_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6651/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ConfidenceFactor_t1603_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConfidenceFactor"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConfidenceFactor_t1603_0_0_0/* byval_arg */
	, &ConfidenceFactor_t1603_1_0_0/* this_arg */
	, &ConfidenceFactor_t1603_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConfidenceFactor_t1603)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ConfidenceFactor_t1603)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Math.Prime.PrimalityTests
#include "mscorlib_Mono_Math_Prime_PrimalityTests.h"
// Metadata Definition Mono.Math.Prime.PrimalityTests
extern TypeInfo PrimalityTests_t1604_il2cpp_TypeInfo;
// Mono.Math.Prime.PrimalityTests
#include "mscorlib_Mono_Math_Prime_PrimalityTestsMethodDeclarations.h"
static const EncodedMethodIndex PrimalityTests_t1604_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PrimalityTests_t1604_0_0_0;
extern const Il2CppType PrimalityTests_t1604_1_0_0;
struct PrimalityTests_t1604;
const Il2CppTypeDefinitionMetadata PrimalityTests_t1604_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PrimalityTests_t1604_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9385/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PrimalityTests_t1604_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimalityTests"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, NULL/* methods */
	, &PrimalityTests_t1604_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimalityTests_t1604_0_0_0/* byval_arg */
	, &PrimalityTests_t1604_1_0_0/* this_arg */
	, &PrimalityTests_t1604_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimalityTests_t1604)/* instance_size */
	, sizeof (PrimalityTests_t1604)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.BigInteger
#include "mscorlib_Mono_Math_BigInteger.h"
// Metadata Definition Mono.Math.BigInteger
extern TypeInfo BigInteger_t1606_il2cpp_TypeInfo;
// Mono.Math.BigInteger
#include "mscorlib_Mono_Math_BigIntegerMethodDeclarations.h"
extern const Il2CppType Sign_t1605_0_0_0;
extern const Il2CppType ModulusRing_t1607_0_0_0;
extern const Il2CppType Kernel_t1608_0_0_0;
static const Il2CppType* BigInteger_t1606_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Sign_t1605_0_0_0,
	&ModulusRing_t1607_0_0_0,
	&Kernel_t1608_0_0_0,
};
static const EncodedMethodIndex BigInteger_t1606_VTable[4] = 
{
	2713,
	601,
	2714,
	2715,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BigInteger_t1606_0_0_0;
extern const Il2CppType BigInteger_t1606_1_0_0;
struct BigInteger_t1606;
const Il2CppTypeDefinitionMetadata BigInteger_t1606_DefinitionMetadata = 
{
	NULL/* declaringType */
	, BigInteger_t1606_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BigInteger_t1606_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6658/* fieldStart */
	, 9389/* methodStart */
	, -1/* eventStart */
	, 1765/* propertyStart */

};
TypeInfo BigInteger_t1606_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BigInteger"/* name */
	, "Mono.Math"/* namespaze */
	, NULL/* methods */
	, &BigInteger_t1606_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BigInteger_t1606_0_0_0/* byval_arg */
	, &BigInteger_t1606_1_0_0/* this_arg */
	, &BigInteger_t1606_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BigInteger_t1606)/* instance_size */
	, sizeof (BigInteger_t1606)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BigInteger_t1606_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 49/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.BigInteger/Sign
#include "mscorlib_Mono_Math_BigInteger_Sign.h"
// Metadata Definition Mono.Math.BigInteger/Sign
extern TypeInfo Sign_t1605_il2cpp_TypeInfo;
// Mono.Math.BigInteger/Sign
#include "mscorlib_Mono_Math_BigInteger_SignMethodDeclarations.h"
static const EncodedMethodIndex Sign_t1605_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Sign_t1605_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Sign_t1605_1_0_0;
const Il2CppTypeDefinitionMetadata Sign_t1605_DefinitionMetadata = 
{
	&BigInteger_t1606_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Sign_t1605_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Sign_t1605_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6662/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Sign_t1605_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Sign"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Sign_t1605_0_0_0/* byval_arg */
	, &Sign_t1605_1_0_0/* this_arg */
	, &Sign_t1605_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Sign_t1605)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Sign_t1605)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Math.BigInteger/ModulusRing
#include "mscorlib_Mono_Math_BigInteger_ModulusRing.h"
// Metadata Definition Mono.Math.BigInteger/ModulusRing
extern TypeInfo ModulusRing_t1607_il2cpp_TypeInfo;
// Mono.Math.BigInteger/ModulusRing
#include "mscorlib_Mono_Math_BigInteger_ModulusRingMethodDeclarations.h"
static const EncodedMethodIndex ModulusRing_t1607_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ModulusRing_t1607_1_0_0;
struct ModulusRing_t1607;
const Il2CppTypeDefinitionMetadata ModulusRing_t1607_DefinitionMetadata = 
{
	&BigInteger_t1606_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ModulusRing_t1607_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6666/* fieldStart */
	, 9438/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ModulusRing_t1607_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ModulusRing"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ModulusRing_t1607_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ModulusRing_t1607_0_0_0/* byval_arg */
	, &ModulusRing_t1607_1_0_0/* this_arg */
	, &ModulusRing_t1607_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ModulusRing_t1607)/* instance_size */
	, sizeof (ModulusRing_t1607)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048837/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.BigInteger/Kernel
#include "mscorlib_Mono_Math_BigInteger_Kernel.h"
// Metadata Definition Mono.Math.BigInteger/Kernel
extern TypeInfo Kernel_t1608_il2cpp_TypeInfo;
// Mono.Math.BigInteger/Kernel
#include "mscorlib_Mono_Math_BigInteger_KernelMethodDeclarations.h"
static const EncodedMethodIndex Kernel_t1608_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Kernel_t1608_1_0_0;
struct Kernel_t1608;
const Il2CppTypeDefinitionMetadata Kernel_t1608_DefinitionMetadata = 
{
	&BigInteger_t1606_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Kernel_t1608_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9444/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Kernel_t1608_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Kernel"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Kernel_t1608_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Kernel_t1608_0_0_0/* byval_arg */
	, &Kernel_t1608_1_0_0/* this_arg */
	, &Kernel_t1608_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Kernel_t1608)/* instance_size */
	, sizeof (Kernel_t1608)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.CryptoConvert
#include "mscorlib_Mono_Security_Cryptography_CryptoConvert.h"
// Metadata Definition Mono.Security.Cryptography.CryptoConvert
extern TypeInfo CryptoConvert_t1609_il2cpp_TypeInfo;
// Mono.Security.Cryptography.CryptoConvert
#include "mscorlib_Mono_Security_Cryptography_CryptoConvertMethodDeclarations.h"
static const EncodedMethodIndex CryptoConvert_t1609_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CryptoConvert_t1609_0_0_0;
extern const Il2CppType CryptoConvert_t1609_1_0_0;
struct CryptoConvert_t1609;
const Il2CppTypeDefinitionMetadata CryptoConvert_t1609_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CryptoConvert_t1609_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9460/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CryptoConvert_t1609_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CryptoConvert"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &CryptoConvert_t1609_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CryptoConvert_t1609_0_0_0/* byval_arg */
	, &CryptoConvert_t1609_1_0_0/* this_arg */
	, &CryptoConvert_t1609_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CryptoConvert_t1609)/* instance_size */
	, sizeof (CryptoConvert_t1609)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.KeyBuilder
#include "mscorlib_Mono_Security_Cryptography_KeyBuilder.h"
// Metadata Definition Mono.Security.Cryptography.KeyBuilder
extern TypeInfo KeyBuilder_t1610_il2cpp_TypeInfo;
// Mono.Security.Cryptography.KeyBuilder
#include "mscorlib_Mono_Security_Cryptography_KeyBuilderMethodDeclarations.h"
static const EncodedMethodIndex KeyBuilder_t1610_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyBuilder_t1610_0_0_0;
extern const Il2CppType KeyBuilder_t1610_1_0_0;
struct KeyBuilder_t1610;
const Il2CppTypeDefinitionMetadata KeyBuilder_t1610_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, KeyBuilder_t1610_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6668/* fieldStart */
	, 9468/* methodStart */
	, -1/* eventStart */
	, 1766/* propertyStart */

};
TypeInfo KeyBuilder_t1610_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyBuilder"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &KeyBuilder_t1610_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyBuilder_t1610_0_0_0/* byval_arg */
	, &KeyBuilder_t1610_1_0_0/* this_arg */
	, &KeyBuilder_t1610_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyBuilder_t1610)/* instance_size */
	, sizeof (KeyBuilder_t1610)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(KeyBuilder_t1610_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.BlockProcessor
#include "mscorlib_Mono_Security_Cryptography_BlockProcessor.h"
// Metadata Definition Mono.Security.Cryptography.BlockProcessor
extern TypeInfo BlockProcessor_t1611_il2cpp_TypeInfo;
// Mono.Security.Cryptography.BlockProcessor
#include "mscorlib_Mono_Security_Cryptography_BlockProcessorMethodDeclarations.h"
static const EncodedMethodIndex BlockProcessor_t1611_VTable[4] = 
{
	626,
	2716,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BlockProcessor_t1611_0_0_0;
extern const Il2CppType BlockProcessor_t1611_1_0_0;
struct BlockProcessor_t1611;
const Il2CppTypeDefinitionMetadata BlockProcessor_t1611_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BlockProcessor_t1611_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6669/* fieldStart */
	, 9471/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BlockProcessor_t1611_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BlockProcessor"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &BlockProcessor_t1611_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BlockProcessor_t1611_0_0_0/* byval_arg */
	, &BlockProcessor_t1611_1_0_0/* this_arg */
	, &BlockProcessor_t1611_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BlockProcessor_t1611)/* instance_size */
	, sizeof (BlockProcessor_t1611)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.DSAManaged
#include "mscorlib_Mono_Security_Cryptography_DSAManaged.h"
// Metadata Definition Mono.Security.Cryptography.DSAManaged
extern TypeInfo DSAManaged_t1613_il2cpp_TypeInfo;
// Mono.Security.Cryptography.DSAManaged
#include "mscorlib_Mono_Security_Cryptography_DSAManagedMethodDeclarations.h"
extern const Il2CppType KeyGeneratedEventHandler_t1612_0_0_0;
static const Il2CppType* DSAManaged_t1613_il2cpp_TypeInfo__nestedTypes[1] =
{
	&KeyGeneratedEventHandler_t1612_0_0_0,
};
static const EncodedMethodIndex DSAManaged_t1613_VTable[14] = 
{
	626,
	2717,
	627,
	628,
	1704,
	2718,
	1706,
	2719,
	2720,
	2721,
	2722,
	2723,
	2724,
	2725,
};
extern const Il2CppType IDisposable_t538_0_0_0;
static Il2CppInterfaceOffsetPair DSAManaged_t1613_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSAManaged_t1613_0_0_0;
extern const Il2CppType DSAManaged_t1613_1_0_0;
extern const Il2CppType DSA_t1232_0_0_0;
struct DSAManaged_t1613;
const Il2CppTypeDefinitionMetadata DSAManaged_t1613_DefinitionMetadata = 
{
	NULL/* declaringType */
	, DSAManaged_t1613_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DSAManaged_t1613_InterfacesOffsets/* interfaceOffsets */
	, &DSA_t1232_0_0_0/* parent */
	, DSAManaged_t1613_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6673/* fieldStart */
	, 9477/* methodStart */
	, 21/* eventStart */
	, 1767/* propertyStart */

};
TypeInfo DSAManaged_t1613_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSAManaged"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DSAManaged_t1613_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DSAManaged_t1613_0_0_0/* byval_arg */
	, &DSAManaged_t1613_1_0_0/* this_arg */
	, &DSAManaged_t1613_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DSAManaged_t1613)/* instance_size */
	, sizeof (DSAManaged_t1613)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 3/* property_count */
	, 13/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 14/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Cryptography.DSAManaged/KeyGeneratedEventHandler
#include "mscorlib_Mono_Security_Cryptography_DSAManaged_KeyGeneratedE.h"
// Metadata Definition Mono.Security.Cryptography.DSAManaged/KeyGeneratedEventHandler
extern TypeInfo KeyGeneratedEventHandler_t1612_il2cpp_TypeInfo;
// Mono.Security.Cryptography.DSAManaged/KeyGeneratedEventHandler
#include "mscorlib_Mono_Security_Cryptography_DSAManaged_KeyGeneratedEMethodDeclarations.h"
static const EncodedMethodIndex KeyGeneratedEventHandler_t1612_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	2726,
	2727,
	2728,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
extern const Il2CppType ISerializable_t2210_0_0_0;
static Il2CppInterfaceOffsetPair KeyGeneratedEventHandler_t1612_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyGeneratedEventHandler_t1612_1_0_0;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
struct KeyGeneratedEventHandler_t1612;
const Il2CppTypeDefinitionMetadata KeyGeneratedEventHandler_t1612_DefinitionMetadata = 
{
	&DSAManaged_t1613_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, KeyGeneratedEventHandler_t1612_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, KeyGeneratedEventHandler_t1612_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9494/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo KeyGeneratedEventHandler_t1612_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyGeneratedEventHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &KeyGeneratedEventHandler_t1612_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyGeneratedEventHandler_t1612_0_0_0/* byval_arg */
	, &KeyGeneratedEventHandler_t1612_1_0_0/* this_arg */
	, &KeyGeneratedEventHandler_t1612_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1612/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyGeneratedEventHandler_t1612)/* instance_size */
	, sizeof (KeyGeneratedEventHandler_t1612)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Cryptography.KeyPairPersistence
#include "mscorlib_Mono_Security_Cryptography_KeyPairPersistence.h"
// Metadata Definition Mono.Security.Cryptography.KeyPairPersistence
extern TypeInfo KeyPairPersistence_t1614_il2cpp_TypeInfo;
// Mono.Security.Cryptography.KeyPairPersistence
#include "mscorlib_Mono_Security_Cryptography_KeyPairPersistenceMethodDeclarations.h"
static const EncodedMethodIndex KeyPairPersistence_t1614_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyPairPersistence_t1614_0_0_0;
extern const Il2CppType KeyPairPersistence_t1614_1_0_0;
struct KeyPairPersistence_t1614;
const Il2CppTypeDefinitionMetadata KeyPairPersistence_t1614_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, KeyPairPersistence_t1614_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6686/* fieldStart */
	, 9498/* methodStart */
	, -1/* eventStart */
	, 1770/* propertyStart */

};
TypeInfo KeyPairPersistence_t1614_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyPairPersistence"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &KeyPairPersistence_t1614_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyPairPersistence_t1614_0_0_0/* byval_arg */
	, &KeyPairPersistence_t1614_1_0_0/* this_arg */
	, &KeyPairPersistence_t1614_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyPairPersistence_t1614)/* instance_size */
	, sizeof (KeyPairPersistence_t1614)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(KeyPairPersistence_t1614_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 8/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.MACAlgorithm
#include "mscorlib_Mono_Security_Cryptography_MACAlgorithm.h"
// Metadata Definition Mono.Security.Cryptography.MACAlgorithm
extern TypeInfo MACAlgorithm_t1615_il2cpp_TypeInfo;
// Mono.Security.Cryptography.MACAlgorithm
#include "mscorlib_Mono_Security_Cryptography_MACAlgorithmMethodDeclarations.h"
static const EncodedMethodIndex MACAlgorithm_t1615_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MACAlgorithm_t1615_0_0_0;
extern const Il2CppType MACAlgorithm_t1615_1_0_0;
struct MACAlgorithm_t1615;
const Il2CppTypeDefinitionMetadata MACAlgorithm_t1615_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MACAlgorithm_t1615_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6695/* fieldStart */
	, 9526/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MACAlgorithm_t1615_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MACAlgorithm"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &MACAlgorithm_t1615_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MACAlgorithm_t1615_0_0_0/* byval_arg */
	, &MACAlgorithm_t1615_1_0_0/* this_arg */
	, &MACAlgorithm_t1615_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MACAlgorithm_t1615)/* instance_size */
	, sizeof (MACAlgorithm_t1615)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.PKCS1
#include "mscorlib_Mono_Security_Cryptography_PKCS1.h"
// Metadata Definition Mono.Security.Cryptography.PKCS1
extern TypeInfo PKCS1_t1616_il2cpp_TypeInfo;
// Mono.Security.Cryptography.PKCS1
#include "mscorlib_Mono_Security_Cryptography_PKCS1MethodDeclarations.h"
static const EncodedMethodIndex PKCS1_t1616_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PKCS1_t1616_0_0_0;
extern const Il2CppType PKCS1_t1616_1_0_0;
struct PKCS1_t1616;
const Il2CppTypeDefinitionMetadata PKCS1_t1616_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PKCS1_t1616_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6700/* fieldStart */
	, 9530/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PKCS1_t1616_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PKCS1"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &PKCS1_t1616_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PKCS1_t1616_0_0_0/* byval_arg */
	, &PKCS1_t1616_1_0_0/* this_arg */
	, &PKCS1_t1616_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PKCS1_t1616)/* instance_size */
	, sizeof (PKCS1_t1616)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PKCS1_t1616_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.PKCS8
#include "mscorlib_Mono_Security_Cryptography_PKCS8.h"
// Metadata Definition Mono.Security.Cryptography.PKCS8
extern TypeInfo PKCS8_t1619_il2cpp_TypeInfo;
// Mono.Security.Cryptography.PKCS8
#include "mscorlib_Mono_Security_Cryptography_PKCS8MethodDeclarations.h"
extern const Il2CppType PrivateKeyInfo_t1617_0_0_0;
extern const Il2CppType EncryptedPrivateKeyInfo_t1618_0_0_0;
static const Il2CppType* PKCS8_t1619_il2cpp_TypeInfo__nestedTypes[2] =
{
	&PrivateKeyInfo_t1617_0_0_0,
	&EncryptedPrivateKeyInfo_t1618_0_0_0,
};
static const EncodedMethodIndex PKCS8_t1619_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PKCS8_t1619_0_0_0;
extern const Il2CppType PKCS8_t1619_1_0_0;
struct PKCS8_t1619;
const Il2CppTypeDefinitionMetadata PKCS8_t1619_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PKCS8_t1619_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PKCS8_t1619_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PKCS8_t1619_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PKCS8"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &PKCS8_t1619_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PKCS8_t1619_0_0_0/* byval_arg */
	, &PKCS8_t1619_1_0_0/* this_arg */
	, &PKCS8_t1619_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PKCS8_t1619)/* instance_size */
	, sizeof (PKCS8_t1619)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
#include "mscorlib_Mono_Security_Cryptography_PKCS8_PrivateKeyInfo.h"
// Metadata Definition Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
extern TypeInfo PrivateKeyInfo_t1617_il2cpp_TypeInfo;
// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
#include "mscorlib_Mono_Security_Cryptography_PKCS8_PrivateKeyInfoMethodDeclarations.h"
static const EncodedMethodIndex PrivateKeyInfo_t1617_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PrivateKeyInfo_t1617_1_0_0;
struct PrivateKeyInfo_t1617;
const Il2CppTypeDefinitionMetadata PrivateKeyInfo_t1617_DefinitionMetadata = 
{
	&PKCS8_t1619_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PrivateKeyInfo_t1617_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6704/* fieldStart */
	, 9542/* methodStart */
	, -1/* eventStart */
	, 1778/* propertyStart */

};
TypeInfo PrivateKeyInfo_t1617_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrivateKeyInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &PrivateKeyInfo_t1617_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrivateKeyInfo_t1617_0_0_0/* byval_arg */
	, &PrivateKeyInfo_t1617_1_0_0/* this_arg */
	, &PrivateKeyInfo_t1617_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrivateKeyInfo_t1617)/* instance_size */
	, sizeof (PrivateKeyInfo_t1617)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo
#include "mscorlib_Mono_Security_Cryptography_PKCS8_EncryptedPrivateKe.h"
// Metadata Definition Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo
extern TypeInfo EncryptedPrivateKeyInfo_t1618_il2cpp_TypeInfo;
// Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo
#include "mscorlib_Mono_Security_Cryptography_PKCS8_EncryptedPrivateKeMethodDeclarations.h"
static const EncodedMethodIndex EncryptedPrivateKeyInfo_t1618_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncryptedPrivateKeyInfo_t1618_1_0_0;
struct EncryptedPrivateKeyInfo_t1618;
const Il2CppTypeDefinitionMetadata EncryptedPrivateKeyInfo_t1618_DefinitionMetadata = 
{
	&PKCS8_t1619_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EncryptedPrivateKeyInfo_t1618_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6708/* fieldStart */
	, 9550/* methodStart */
	, -1/* eventStart */
	, 1779/* propertyStart */

};
TypeInfo EncryptedPrivateKeyInfo_t1618_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncryptedPrivateKeyInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &EncryptedPrivateKeyInfo_t1618_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncryptedPrivateKeyInfo_t1618_0_0_0/* byval_arg */
	, &EncryptedPrivateKeyInfo_t1618_1_0_0/* this_arg */
	, &EncryptedPrivateKeyInfo_t1618_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncryptedPrivateKeyInfo_t1618)/* instance_size */
	, sizeof (EncryptedPrivateKeyInfo_t1618)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 4/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.RSAManaged
#include "mscorlib_Mono_Security_Cryptography_RSAManaged.h"
// Metadata Definition Mono.Security.Cryptography.RSAManaged
extern TypeInfo RSAManaged_t1621_il2cpp_TypeInfo;
// Mono.Security.Cryptography.RSAManaged
#include "mscorlib_Mono_Security_Cryptography_RSAManagedMethodDeclarations.h"
extern const Il2CppType KeyGeneratedEventHandler_t1620_0_0_0;
static const Il2CppType* RSAManaged_t1621_il2cpp_TypeInfo__nestedTypes[1] =
{
	&KeyGeneratedEventHandler_t1620_0_0_0,
};
static const EncodedMethodIndex RSAManaged_t1621_VTable[14] = 
{
	626,
	2729,
	627,
	628,
	1704,
	2730,
	1706,
	2731,
	1708,
	2732,
	2733,
	2734,
	2735,
	2736,
};
static Il2CppInterfaceOffsetPair RSAManaged_t1621_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RSAManaged_t1621_0_0_0;
extern const Il2CppType RSAManaged_t1621_1_0_0;
extern const Il2CppType RSA_t1226_0_0_0;
struct RSAManaged_t1621;
const Il2CppTypeDefinitionMetadata RSAManaged_t1621_DefinitionMetadata = 
{
	NULL/* declaringType */
	, RSAManaged_t1621_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RSAManaged_t1621_InterfacesOffsets/* interfaceOffsets */
	, &RSA_t1226_0_0_0/* parent */
	, RSAManaged_t1621_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6712/* fieldStart */
	, 9557/* methodStart */
	, 22/* eventStart */
	, 1783/* propertyStart */

};
TypeInfo RSAManaged_t1621_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSAManaged"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSAManaged_t1621_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RSAManaged_t1621_0_0_0/* byval_arg */
	, &RSAManaged_t1621_1_0_0/* this_arg */
	, &RSAManaged_t1621_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSAManaged_t1621)/* instance_size */
	, sizeof (RSAManaged_t1621)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 3/* property_count */
	, 13/* field_count */
	, 1/* event_count */
	, 1/* nested_type_count */
	, 14/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
#include "mscorlib_Mono_Security_Cryptography_RSAManaged_KeyGeneratedE.h"
// Metadata Definition Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
extern TypeInfo KeyGeneratedEventHandler_t1620_il2cpp_TypeInfo;
// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
#include "mscorlib_Mono_Security_Cryptography_RSAManaged_KeyGeneratedEMethodDeclarations.h"
static const EncodedMethodIndex KeyGeneratedEventHandler_t1620_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	2737,
	2738,
	2739,
};
static Il2CppInterfaceOffsetPair KeyGeneratedEventHandler_t1620_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyGeneratedEventHandler_t1620_1_0_0;
struct KeyGeneratedEventHandler_t1620;
const Il2CppTypeDefinitionMetadata KeyGeneratedEventHandler_t1620_DefinitionMetadata = 
{
	&RSAManaged_t1621_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, KeyGeneratedEventHandler_t1620_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, KeyGeneratedEventHandler_t1620_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9572/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo KeyGeneratedEventHandler_t1620_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyGeneratedEventHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &KeyGeneratedEventHandler_t1620_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyGeneratedEventHandler_t1620_0_0_0/* byval_arg */
	, &KeyGeneratedEventHandler_t1620_1_0_0/* this_arg */
	, &KeyGeneratedEventHandler_t1620_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1620/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyGeneratedEventHandler_t1620)/* instance_size */
	, sizeof (KeyGeneratedEventHandler_t1620)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Cryptography.SymmetricTransform
#include "mscorlib_Mono_Security_Cryptography_SymmetricTransform.h"
// Metadata Definition Mono.Security.Cryptography.SymmetricTransform
extern TypeInfo SymmetricTransform_t1622_il2cpp_TypeInfo;
// Mono.Security.Cryptography.SymmetricTransform
#include "mscorlib_Mono_Security_Cryptography_SymmetricTransformMethodDeclarations.h"
static const EncodedMethodIndex SymmetricTransform_t1622_VTable[18] = 
{
	626,
	2740,
	627,
	628,
	2741,
	2742,
	2743,
	2744,
	2745,
	2742,
	2746,
	0,
	2747,
	2748,
	2749,
	2750,
	2743,
	2744,
};
extern const Il2CppType ICryptoTransform_t1188_0_0_0;
static const Il2CppType* SymmetricTransform_t1622_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
	&ICryptoTransform_t1188_0_0_0,
};
static Il2CppInterfaceOffsetPair SymmetricTransform_t1622_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SymmetricTransform_t1622_0_0_0;
extern const Il2CppType SymmetricTransform_t1622_1_0_0;
struct SymmetricTransform_t1622;
const Il2CppTypeDefinitionMetadata SymmetricTransform_t1622_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SymmetricTransform_t1622_InterfacesTypeInfos/* implementedInterfaces */
	, SymmetricTransform_t1622_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SymmetricTransform_t1622_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6725/* fieldStart */
	, 9576/* methodStart */
	, -1/* eventStart */
	, 1786/* propertyStart */

};
TypeInfo SymmetricTransform_t1622_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SymmetricTransform"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SymmetricTransform_t1622_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SymmetricTransform_t1622_0_0_0/* byval_arg */
	, &SymmetricTransform_t1622_1_0_0/* this_arg */
	, &SymmetricTransform_t1622_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SymmetricTransform_t1622)/* instance_size */
	, sizeof (SymmetricTransform_t1622)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 20/* method_count */
	, 2/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.X509.SafeBag
#include "mscorlib_Mono_Security_X509_SafeBag.h"
// Metadata Definition Mono.Security.X509.SafeBag
extern TypeInfo SafeBag_t1624_il2cpp_TypeInfo;
// Mono.Security.X509.SafeBag
#include "mscorlib_Mono_Security_X509_SafeBagMethodDeclarations.h"
static const EncodedMethodIndex SafeBag_t1624_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SafeBag_t1624_0_0_0;
extern const Il2CppType SafeBag_t1624_1_0_0;
struct SafeBag_t1624;
const Il2CppTypeDefinitionMetadata SafeBag_t1624_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SafeBag_t1624_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6737/* fieldStart */
	, 9596/* methodStart */
	, -1/* eventStart */
	, 1788/* propertyStart */

};
TypeInfo SafeBag_t1624_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SafeBag"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &SafeBag_t1624_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SafeBag_t1624_0_0_0/* byval_arg */
	, &SafeBag_t1624_1_0_0/* this_arg */
	, &SafeBag_t1624_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SafeBag_t1624)/* instance_size */
	, sizeof (SafeBag_t1624)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.PKCS12
#include "mscorlib_Mono_Security_X509_PKCS12.h"
// Metadata Definition Mono.Security.X509.PKCS12
extern TypeInfo PKCS12_t1627_il2cpp_TypeInfo;
// Mono.Security.X509.PKCS12
#include "mscorlib_Mono_Security_X509_PKCS12MethodDeclarations.h"
extern const Il2CppType DeriveBytes_t1625_0_0_0;
static const Il2CppType* PKCS12_t1627_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DeriveBytes_t1625_0_0_0,
};
static const EncodedMethodIndex PKCS12_t1627_VTable[4] = 
{
	626,
	2751,
	627,
	628,
};
static const Il2CppType* PKCS12_t1627_InterfacesTypeInfos[] = 
{
	&ICloneable_t3433_0_0_0,
};
static Il2CppInterfaceOffsetPair PKCS12_t1627_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PKCS12_t1627_0_0_0;
extern const Il2CppType PKCS12_t1627_1_0_0;
struct PKCS12_t1627;
const Il2CppTypeDefinitionMetadata PKCS12_t1627_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PKCS12_t1627_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, PKCS12_t1627_InterfacesTypeInfos/* implementedInterfaces */
	, PKCS12_t1627_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PKCS12_t1627_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6739/* fieldStart */
	, 9599/* methodStart */
	, -1/* eventStart */
	, 1790/* propertyStart */

};
TypeInfo PKCS12_t1627_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PKCS12"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &PKCS12_t1627_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PKCS12_t1627_0_0_0/* byval_arg */
	, &PKCS12_t1627_1_0_0/* this_arg */
	, &PKCS12_t1627_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PKCS12_t1627)/* instance_size */
	, sizeof (PKCS12_t1627)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PKCS12_t1627_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 3/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.X509.PKCS12/DeriveBytes
#include "mscorlib_Mono_Security_X509_PKCS12_DeriveBytes.h"
// Metadata Definition Mono.Security.X509.PKCS12/DeriveBytes
extern TypeInfo DeriveBytes_t1625_il2cpp_TypeInfo;
// Mono.Security.X509.PKCS12/DeriveBytes
#include "mscorlib_Mono_Security_X509_PKCS12_DeriveBytesMethodDeclarations.h"
static const EncodedMethodIndex DeriveBytes_t1625_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DeriveBytes_t1625_1_0_0;
struct DeriveBytes_t1625;
const Il2CppTypeDefinitionMetadata DeriveBytes_t1625_DefinitionMetadata = 
{
	&PKCS12_t1627_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DeriveBytes_t1625_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6754/* fieldStart */
	, 9616/* methodStart */
	, -1/* eventStart */
	, 1793/* propertyStart */

};
TypeInfo DeriveBytes_t1625_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DeriveBytes"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DeriveBytes_t1625_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DeriveBytes_t1625_0_0_0/* byval_arg */
	, &DeriveBytes_t1625_1_0_0/* this_arg */
	, &DeriveBytes_t1625_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DeriveBytes_t1625)/* instance_size */
	, sizeof (DeriveBytes_t1625)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DeriveBytes_t1625_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 4/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.X501
#include "mscorlib_Mono_Security_X509_X501.h"
// Metadata Definition Mono.Security.X509.X501
extern TypeInfo X501_t1628_il2cpp_TypeInfo;
// Mono.Security.X509.X501
#include "mscorlib_Mono_Security_X509_X501MethodDeclarations.h"
static const EncodedMethodIndex X501_t1628_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X501_t1628_0_0_0;
extern const Il2CppType X501_t1628_1_0_0;
struct X501_t1628;
const Il2CppTypeDefinitionMetadata X501_t1628_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X501_t1628_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6761/* fieldStart */
	, 9627/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X501_t1628_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X501"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X501_t1628_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X501_t1628_0_0_0/* byval_arg */
	, &X501_t1628_1_0_0/* this_arg */
	, &X501_t1628_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X501_t1628)/* instance_size */
	, sizeof (X501_t1628)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(X501_t1628_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.X509Certificate
#include "mscorlib_Mono_Security_X509_X509Certificate.h"
// Metadata Definition Mono.Security.X509.X509Certificate
extern TypeInfo X509Certificate_t1630_il2cpp_TypeInfo;
// Mono.Security.X509.X509Certificate
#include "mscorlib_Mono_Security_X509_X509CertificateMethodDeclarations.h"
static const EncodedMethodIndex X509Certificate_t1630_VTable[13] = 
{
	626,
	601,
	627,
	628,
	2752,
	2753,
	2754,
	2755,
	2756,
	2757,
	2758,
	2759,
	2752,
};
static const Il2CppType* X509Certificate_t1630_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair X509Certificate_t1630_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X509Certificate_t1630_0_0_0;
extern const Il2CppType X509Certificate_t1630_1_0_0;
struct X509Certificate_t1630;
const Il2CppTypeDefinitionMetadata X509Certificate_t1630_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509Certificate_t1630_InterfacesTypeInfos/* implementedInterfaces */
	, X509Certificate_t1630_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Certificate_t1630_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6776/* fieldStart */
	, 9631/* methodStart */
	, -1/* eventStart */
	, 1797/* propertyStart */

};
TypeInfo X509Certificate_t1630_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Certificate"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509Certificate_t1630_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Certificate_t1630_0_0_0/* byval_arg */
	, &X509Certificate_t1630_1_0_0/* this_arg */
	, &X509Certificate_t1630_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Certificate_t1630)/* instance_size */
	, sizeof (X509Certificate_t1630)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(X509Certificate_t1630_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 8/* property_count */
	, 21/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.X509.X509CertificateCollection
#include "mscorlib_Mono_Security_X509_X509CertificateCollection.h"
// Metadata Definition Mono.Security.X509.X509CertificateCollection
extern TypeInfo X509CertificateCollection_t1626_il2cpp_TypeInfo;
// Mono.Security.X509.X509CertificateCollection
#include "mscorlib_Mono_Security_X509_X509CertificateCollectionMethodDeclarations.h"
extern const Il2CppType X509CertificateEnumerator_t1631_0_0_0;
static const Il2CppType* X509CertificateCollection_t1626_il2cpp_TypeInfo__nestedTypes[1] =
{
	&X509CertificateEnumerator_t1631_0_0_0,
};
static const EncodedMethodIndex X509CertificateCollection_t1626_VTable[29] = 
{
	626,
	601,
	2760,
	628,
	2761,
	1735,
	1736,
	1737,
	1738,
	1739,
	1740,
	1741,
	1742,
	1743,
	1744,
	1745,
	1746,
	1747,
	1748,
	1749,
	1750,
	1751,
	1752,
	1753,
	1754,
	1755,
	1756,
	1757,
	1758,
};
extern const Il2CppType IEnumerable_t1097_0_0_0;
static const Il2CppType* X509CertificateCollection_t1626_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
};
extern const Il2CppType ICollection_t1528_0_0_0;
extern const Il2CppType IList_t1487_0_0_0;
static Il2CppInterfaceOffsetPair X509CertificateCollection_t1626_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X509CertificateCollection_t1626_0_0_0;
extern const Il2CppType X509CertificateCollection_t1626_1_0_0;
extern const Il2CppType CollectionBase_t1236_0_0_0;
struct X509CertificateCollection_t1626;
const Il2CppTypeDefinitionMetadata X509CertificateCollection_t1626_DefinitionMetadata = 
{
	NULL/* declaringType */
	, X509CertificateCollection_t1626_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, X509CertificateCollection_t1626_InterfacesTypeInfos/* implementedInterfaces */
	, X509CertificateCollection_t1626_InterfacesOffsets/* interfaceOffsets */
	, &CollectionBase_t1236_0_0_0/* parent */
	, X509CertificateCollection_t1626_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9647/* methodStart */
	, -1/* eventStart */
	, 1805/* propertyStart */

};
TypeInfo X509CertificateCollection_t1626_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509CertificateCollection"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509CertificateCollection_t1626_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2672/* custom_attributes_cache */
	, &X509CertificateCollection_t1626_0_0_0/* byval_arg */
	, &X509CertificateCollection_t1626_1_0_0/* this_arg */
	, &X509CertificateCollection_t1626_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509CertificateCollection_t1626)/* instance_size */
	, sizeof (X509CertificateCollection_t1626)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 29/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator
#include "mscorlib_Mono_Security_X509_X509CertificateCollection_X509Ce.h"
// Metadata Definition Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator
extern TypeInfo X509CertificateEnumerator_t1631_il2cpp_TypeInfo;
// Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator
#include "mscorlib_Mono_Security_X509_X509CertificateCollection_X509CeMethodDeclarations.h"
static const EncodedMethodIndex X509CertificateEnumerator_t1631_VTable[7] = 
{
	626,
	601,
	627,
	628,
	2762,
	2763,
	2764,
};
extern const Il2CppType IEnumerator_t464_0_0_0;
static const Il2CppType* X509CertificateEnumerator_t1631_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
};
static Il2CppInterfaceOffsetPair X509CertificateEnumerator_t1631_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X509CertificateEnumerator_t1631_1_0_0;
struct X509CertificateEnumerator_t1631;
const Il2CppTypeDefinitionMetadata X509CertificateEnumerator_t1631_DefinitionMetadata = 
{
	&X509CertificateCollection_t1626_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, X509CertificateEnumerator_t1631_InterfacesTypeInfos/* implementedInterfaces */
	, X509CertificateEnumerator_t1631_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509CertificateEnumerator_t1631_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6797/* fieldStart */
	, 9653/* methodStart */
	, -1/* eventStart */
	, 1806/* propertyStart */

};
TypeInfo X509CertificateEnumerator_t1631_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509CertificateEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &X509CertificateEnumerator_t1631_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509CertificateEnumerator_t1631_0_0_0/* byval_arg */
	, &X509CertificateEnumerator_t1631_1_0_0/* this_arg */
	, &X509CertificateEnumerator_t1631_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509CertificateEnumerator_t1631)/* instance_size */
	, sizeof (X509CertificateEnumerator_t1631)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.X509.X509Extension
#include "mscorlib_Mono_Security_X509_X509Extension.h"
// Metadata Definition Mono.Security.X509.X509Extension
extern TypeInfo X509Extension_t1632_il2cpp_TypeInfo;
// Mono.Security.X509.X509Extension
#include "mscorlib_Mono_Security_X509_X509ExtensionMethodDeclarations.h"
static const EncodedMethodIndex X509Extension_t1632_VTable[5] = 
{
	2765,
	601,
	2766,
	2767,
	2768,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X509Extension_t1632_0_0_0;
extern const Il2CppType X509Extension_t1632_1_0_0;
struct X509Extension_t1632;
const Il2CppTypeDefinitionMetadata X509Extension_t1632_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Extension_t1632_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6798/* fieldStart */
	, 9658/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509Extension_t1632_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Extension"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509Extension_t1632_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Extension_t1632_0_0_0/* byval_arg */
	, &X509Extension_t1632_1_0_0/* this_arg */
	, &X509Extension_t1632_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Extension_t1632)/* instance_size */
	, sizeof (X509Extension_t1632)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.X509ExtensionCollection
#include "mscorlib_Mono_Security_X509_X509ExtensionCollection.h"
// Metadata Definition Mono.Security.X509.X509ExtensionCollection
extern TypeInfo X509ExtensionCollection_t1629_il2cpp_TypeInfo;
// Mono.Security.X509.X509ExtensionCollection
#include "mscorlib_Mono_Security_X509_X509ExtensionCollectionMethodDeclarations.h"
static const EncodedMethodIndex X509ExtensionCollection_t1629_VTable[29] = 
{
	626,
	601,
	627,
	628,
	2769,
	1735,
	1736,
	1737,
	1738,
	1739,
	1740,
	1741,
	1742,
	1743,
	1744,
	1745,
	1746,
	1747,
	1748,
	1749,
	1750,
	1751,
	1752,
	1753,
	1754,
	1755,
	1756,
	1757,
	1758,
};
static const Il2CppType* X509ExtensionCollection_t1629_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
};
static Il2CppInterfaceOffsetPair X509ExtensionCollection_t1629_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType X509ExtensionCollection_t1629_0_0_0;
extern const Il2CppType X509ExtensionCollection_t1629_1_0_0;
struct X509ExtensionCollection_t1629;
const Il2CppTypeDefinitionMetadata X509ExtensionCollection_t1629_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509ExtensionCollection_t1629_InterfacesTypeInfos/* implementedInterfaces */
	, X509ExtensionCollection_t1629_InterfacesOffsets/* interfaceOffsets */
	, &CollectionBase_t1236_0_0_0/* parent */
	, X509ExtensionCollection_t1629_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6801/* fieldStart */
	, 9664/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509ExtensionCollection_t1629_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ExtensionCollection"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509ExtensionCollection_t1629_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2673/* custom_attributes_cache */
	, &X509ExtensionCollection_t1629_0_0_0/* byval_arg */
	, &X509ExtensionCollection_t1629_1_0_0/* this_arg */
	, &X509ExtensionCollection_t1629_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ExtensionCollection_t1629)/* instance_size */
	, sizeof (X509ExtensionCollection_t1629)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 29/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.ASN1
#include "mscorlib_Mono_Security_ASN1.h"
// Metadata Definition Mono.Security.ASN1
extern TypeInfo ASN1_t1623_il2cpp_TypeInfo;
// Mono.Security.ASN1
#include "mscorlib_Mono_Security_ASN1MethodDeclarations.h"
static const EncodedMethodIndex ASN1_t1623_VTable[5] = 
{
	626,
	601,
	627,
	2770,
	2771,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ASN1_t1623_0_0_0;
extern const Il2CppType ASN1_t1623_1_0_0;
struct ASN1_t1623;
const Il2CppTypeDefinitionMetadata ASN1_t1623_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ASN1_t1623_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6802/* fieldStart */
	, 9667/* methodStart */
	, -1/* eventStart */
	, 1808/* propertyStart */

};
TypeInfo ASN1_t1623_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ASN1"/* name */
	, "Mono.Security"/* namespaze */
	, NULL/* methods */
	, &ASN1_t1623_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2674/* custom_attributes_cache */
	, &ASN1_t1623_0_0_0/* byval_arg */
	, &ASN1_t1623_1_0_0/* this_arg */
	, &ASN1_t1623_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ASN1_t1623)/* instance_size */
	, sizeof (ASN1_t1623)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.ASN1Convert
#include "mscorlib_Mono_Security_ASN1Convert.h"
// Metadata Definition Mono.Security.ASN1Convert
extern TypeInfo ASN1Convert_t1633_il2cpp_TypeInfo;
// Mono.Security.ASN1Convert
#include "mscorlib_Mono_Security_ASN1ConvertMethodDeclarations.h"
static const EncodedMethodIndex ASN1Convert_t1633_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ASN1Convert_t1633_0_0_0;
extern const Il2CppType ASN1Convert_t1633_1_0_0;
struct ASN1Convert_t1633;
const Il2CppTypeDefinitionMetadata ASN1Convert_t1633_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ASN1Convert_t1633_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9684/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ASN1Convert_t1633_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ASN1Convert"/* name */
	, "Mono.Security"/* namespaze */
	, NULL/* methods */
	, &ASN1Convert_t1633_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ASN1Convert_t1633_0_0_0/* byval_arg */
	, &ASN1Convert_t1633_1_0_0/* this_arg */
	, &ASN1Convert_t1633_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ASN1Convert_t1633)/* instance_size */
	, sizeof (ASN1Convert_t1633)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.BitConverterLE
#include "mscorlib_Mono_Security_BitConverterLE.h"
// Metadata Definition Mono.Security.BitConverterLE
extern TypeInfo BitConverterLE_t1634_il2cpp_TypeInfo;
// Mono.Security.BitConverterLE
#include "mscorlib_Mono_Security_BitConverterLEMethodDeclarations.h"
static const EncodedMethodIndex BitConverterLE_t1634_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BitConverterLE_t1634_0_0_0;
extern const Il2CppType BitConverterLE_t1634_1_0_0;
struct BitConverterLE_t1634;
const Il2CppTypeDefinitionMetadata BitConverterLE_t1634_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BitConverterLE_t1634_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9687/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BitConverterLE_t1634_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BitConverterLE"/* name */
	, "Mono.Security"/* namespaze */
	, NULL/* methods */
	, &BitConverterLE_t1634_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BitConverterLE_t1634_0_0_0/* byval_arg */
	, &BitConverterLE_t1634_1_0_0/* this_arg */
	, &BitConverterLE_t1634_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BitConverterLE_t1634)/* instance_size */
	, sizeof (BitConverterLE_t1634)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.PKCS7
#include "mscorlib_Mono_Security_PKCS7.h"
// Metadata Definition Mono.Security.PKCS7
extern TypeInfo PKCS7_t1637_il2cpp_TypeInfo;
// Mono.Security.PKCS7
#include "mscorlib_Mono_Security_PKCS7MethodDeclarations.h"
extern const Il2CppType ContentInfo_t1635_0_0_0;
extern const Il2CppType EncryptedData_t1636_0_0_0;
static const Il2CppType* PKCS7_t1637_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ContentInfo_t1635_0_0_0,
	&EncryptedData_t1636_0_0_0,
};
static const EncodedMethodIndex PKCS7_t1637_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PKCS7_t1637_0_0_0;
extern const Il2CppType PKCS7_t1637_1_0_0;
struct PKCS7_t1637;
const Il2CppTypeDefinitionMetadata PKCS7_t1637_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PKCS7_t1637_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PKCS7_t1637_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PKCS7_t1637_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PKCS7"/* name */
	, "Mono.Security"/* namespaze */
	, NULL/* methods */
	, &PKCS7_t1637_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PKCS7_t1637_0_0_0/* byval_arg */
	, &PKCS7_t1637_1_0_0/* this_arg */
	, &PKCS7_t1637_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PKCS7_t1637)/* instance_size */
	, sizeof (PKCS7_t1637)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.PKCS7/ContentInfo
#include "mscorlib_Mono_Security_PKCS7_ContentInfo.h"
// Metadata Definition Mono.Security.PKCS7/ContentInfo
extern TypeInfo ContentInfo_t1635_il2cpp_TypeInfo;
// Mono.Security.PKCS7/ContentInfo
#include "mscorlib_Mono_Security_PKCS7_ContentInfoMethodDeclarations.h"
static const EncodedMethodIndex ContentInfo_t1635_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ContentInfo_t1635_1_0_0;
struct ContentInfo_t1635;
const Il2CppTypeDefinitionMetadata ContentInfo_t1635_DefinitionMetadata = 
{
	&PKCS7_t1637_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ContentInfo_t1635_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6805/* fieldStart */
	, 9696/* methodStart */
	, -1/* eventStart */
	, 1813/* propertyStart */

};
TypeInfo ContentInfo_t1635_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContentInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ContentInfo_t1635_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ContentInfo_t1635_0_0_0/* byval_arg */
	, &ContentInfo_t1635_1_0_0/* this_arg */
	, &ContentInfo_t1635_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContentInfo_t1635)/* instance_size */
	, sizeof (ContentInfo_t1635)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.PKCS7/EncryptedData
#include "mscorlib_Mono_Security_PKCS7_EncryptedData.h"
// Metadata Definition Mono.Security.PKCS7/EncryptedData
extern TypeInfo EncryptedData_t1636_il2cpp_TypeInfo;
// Mono.Security.PKCS7/EncryptedData
#include "mscorlib_Mono_Security_PKCS7_EncryptedDataMethodDeclarations.h"
static const EncodedMethodIndex EncryptedData_t1636_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncryptedData_t1636_1_0_0;
struct EncryptedData_t1636;
const Il2CppTypeDefinitionMetadata EncryptedData_t1636_DefinitionMetadata = 
{
	&PKCS7_t1637_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EncryptedData_t1636_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6807/* fieldStart */
	, 9703/* methodStart */
	, -1/* eventStart */
	, 1815/* propertyStart */

};
TypeInfo EncryptedData_t1636_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncryptedData"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &EncryptedData_t1636_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncryptedData_t1636_0_0_0/* byval_arg */
	, &EncryptedData_t1636_1_0_0/* this_arg */
	, &EncryptedData_t1636_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncryptedData_t1636)/* instance_size */
	, sizeof (EncryptedData_t1636)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.StrongName
#include "mscorlib_Mono_Security_StrongName.h"
// Metadata Definition Mono.Security.StrongName
extern TypeInfo StrongName_t1638_il2cpp_TypeInfo;
// Mono.Security.StrongName
#include "mscorlib_Mono_Security_StrongNameMethodDeclarations.h"
static const EncodedMethodIndex StrongName_t1638_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StrongName_t1638_0_0_0;
extern const Il2CppType StrongName_t1638_1_0_0;
struct StrongName_t1638;
const Il2CppTypeDefinitionMetadata StrongName_t1638_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StrongName_t1638_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6811/* fieldStart */
	, 9707/* methodStart */
	, -1/* eventStart */
	, 1817/* propertyStart */

};
TypeInfo StrongName_t1638_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StrongName"/* name */
	, "Mono.Security"/* namespaze */
	, NULL/* methods */
	, &StrongName_t1638_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StrongName_t1638_0_0_0/* byval_arg */
	, &StrongName_t1638_1_0_0/* this_arg */
	, &StrongName_t1638_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StrongName_t1638)/* instance_size */
	, sizeof (StrongName_t1638)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StrongName_t1638_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 3/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Xml.SecurityParser
#include "mscorlib_Mono_Xml_SecurityParser.h"
// Metadata Definition Mono.Xml.SecurityParser
extern TypeInfo SecurityParser_t1640_il2cpp_TypeInfo;
// Mono.Xml.SecurityParser
#include "mscorlib_Mono_Xml_SecurityParserMethodDeclarations.h"
static const EncodedMethodIndex SecurityParser_t1640_VTable[11] = 
{
	626,
	601,
	627,
	628,
	2772,
	2773,
	2774,
	2775,
	2776,
	2777,
	2778,
};
extern const Il2CppType IContentHandler_t1643_0_0_0;
static const Il2CppType* SecurityParser_t1640_InterfacesTypeInfos[] = 
{
	&IContentHandler_t1643_0_0_0,
};
static Il2CppInterfaceOffsetPair SecurityParser_t1640_InterfacesOffsets[] = 
{
	{ &IContentHandler_t1643_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityParser_t1640_0_0_0;
extern const Il2CppType SecurityParser_t1640_1_0_0;
extern const Il2CppType SmallXmlParser_t1641_0_0_0;
struct SecurityParser_t1640;
const Il2CppTypeDefinitionMetadata SecurityParser_t1640_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SecurityParser_t1640_InterfacesTypeInfos/* implementedInterfaces */
	, SecurityParser_t1640_InterfacesOffsets/* interfaceOffsets */
	, &SmallXmlParser_t1641_0_0_0/* parent */
	, SecurityParser_t1640_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6817/* fieldStart */
	, 9711/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SecurityParser_t1640_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityParser"/* name */
	, "Mono.Xml"/* namespaze */
	, NULL/* methods */
	, &SecurityParser_t1640_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SecurityParser_t1640_0_0_0/* byval_arg */
	, &SecurityParser_t1640_1_0_0/* this_arg */
	, &SecurityParser_t1640_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityParser_t1640)/* instance_size */
	, sizeof (SecurityParser_t1640)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Xml.SmallXmlParser
#include "mscorlib_Mono_Xml_SmallXmlParser.h"
// Metadata Definition Mono.Xml.SmallXmlParser
extern TypeInfo SmallXmlParser_t1641_il2cpp_TypeInfo;
// Mono.Xml.SmallXmlParser
#include "mscorlib_Mono_Xml_SmallXmlParserMethodDeclarations.h"
extern const Il2CppType IAttrList_t2178_0_0_0;
extern const Il2CppType AttrListImpl_t1642_0_0_0;
static const Il2CppType* SmallXmlParser_t1641_il2cpp_TypeInfo__nestedTypes[3] =
{
	&IContentHandler_t1643_0_0_0,
	&IAttrList_t2178_0_0_0,
	&AttrListImpl_t1642_0_0_0,
};
static const EncodedMethodIndex SmallXmlParser_t1641_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SmallXmlParser_t1641_1_0_0;
struct SmallXmlParser_t1641;
const Il2CppTypeDefinitionMetadata SmallXmlParser_t1641_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SmallXmlParser_t1641_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SmallXmlParser_t1641_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6820/* fieldStart */
	, 9721/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SmallXmlParser_t1641_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SmallXmlParser"/* name */
	, "Mono.Xml"/* namespaze */
	, NULL/* methods */
	, &SmallXmlParser_t1641_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SmallXmlParser_t1641_0_0_0/* byval_arg */
	, &SmallXmlParser_t1641_1_0_0/* this_arg */
	, &SmallXmlParser_t1641_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SmallXmlParser_t1641)/* instance_size */
	, sizeof (SmallXmlParser_t1641)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SmallXmlParser_t1641_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 23/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition Mono.Xml.SmallXmlParser/IContentHandler
extern TypeInfo IContentHandler_t1643_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContentHandler_t1643_1_0_0;
struct IContentHandler_t1643;
const Il2CppTypeDefinitionMetadata IContentHandler_t1643_DefinitionMetadata = 
{
	&SmallXmlParser_t1641_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9744/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IContentHandler_t1643_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContentHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &IContentHandler_t1643_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IContentHandler_t1643_0_0_0/* byval_arg */
	, &IContentHandler_t1643_1_0_0/* this_arg */
	, &IContentHandler_t1643_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 162/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition Mono.Xml.SmallXmlParser/IAttrList
extern TypeInfo IAttrList_t2178_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IAttrList_t2178_1_0_0;
struct IAttrList_t2178;
const Il2CppTypeDefinitionMetadata IAttrList_t2178_DefinitionMetadata = 
{
	&SmallXmlParser_t1641_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9751/* methodStart */
	, -1/* eventStart */
	, 1820/* propertyStart */

};
TypeInfo IAttrList_t2178_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAttrList"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &IAttrList_t2178_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IAttrList_t2178_0_0_0/* byval_arg */
	, &IAttrList_t2178_1_0_0/* this_arg */
	, &IAttrList_t2178_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 162/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Xml.SmallXmlParser/AttrListImpl
#include "mscorlib_Mono_Xml_SmallXmlParser_AttrListImpl.h"
// Metadata Definition Mono.Xml.SmallXmlParser/AttrListImpl
extern TypeInfo AttrListImpl_t1642_il2cpp_TypeInfo;
// Mono.Xml.SmallXmlParser/AttrListImpl
#include "mscorlib_Mono_Xml_SmallXmlParser_AttrListImplMethodDeclarations.h"
static const EncodedMethodIndex AttrListImpl_t1642_VTable[10] = 
{
	626,
	601,
	627,
	628,
	2779,
	2780,
	2781,
	2782,
	2783,
	2784,
};
static const Il2CppType* AttrListImpl_t1642_InterfacesTypeInfos[] = 
{
	&IAttrList_t2178_0_0_0,
};
static Il2CppInterfaceOffsetPair AttrListImpl_t1642_InterfacesOffsets[] = 
{
	{ &IAttrList_t2178_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AttrListImpl_t1642_1_0_0;
struct AttrListImpl_t1642;
const Il2CppTypeDefinitionMetadata AttrListImpl_t1642_DefinitionMetadata = 
{
	&SmallXmlParser_t1641_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, AttrListImpl_t1642_InterfacesTypeInfos/* implementedInterfaces */
	, AttrListImpl_t1642_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AttrListImpl_t1642_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6833/* fieldStart */
	, 9757/* methodStart */
	, -1/* eventStart */
	, 1823/* propertyStart */

};
TypeInfo AttrListImpl_t1642_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttrListImpl"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &AttrListImpl_t1642_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AttrListImpl_t1642_0_0_0/* byval_arg */
	, &AttrListImpl_t1642_1_0_0/* this_arg */
	, &AttrListImpl_t1642_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttrListImpl_t1642)/* instance_size */
	, sizeof (AttrListImpl_t1642)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Xml.SmallXmlParserException
#include "mscorlib_Mono_Xml_SmallXmlParserException.h"
// Metadata Definition Mono.Xml.SmallXmlParserException
extern TypeInfo SmallXmlParserException_t1645_il2cpp_TypeInfo;
// Mono.Xml.SmallXmlParserException
#include "mscorlib_Mono_Xml_SmallXmlParserExceptionMethodDeclarations.h"
static const EncodedMethodIndex SmallXmlParserException_t1645_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
extern const Il2CppType _Exception_t3443_0_0_0;
static Il2CppInterfaceOffsetPair SmallXmlParserException_t1645_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SmallXmlParserException_t1645_0_0_0;
extern const Il2CppType SmallXmlParserException_t1645_1_0_0;
extern const Il2CppType SystemException_t1536_0_0_0;
struct SmallXmlParserException_t1645;
const Il2CppTypeDefinitionMetadata SmallXmlParserException_t1645_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SmallXmlParserException_t1645_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, SmallXmlParserException_t1645_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6835/* fieldStart */
	, 9766/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SmallXmlParserException_t1645_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SmallXmlParserException"/* name */
	, "Mono.Xml"/* namespaze */
	, NULL/* methods */
	, &SmallXmlParserException_t1645_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SmallXmlParserException_t1645_0_0_0/* byval_arg */
	, &SmallXmlParserException_t1645_1_0_0/* this_arg */
	, &SmallXmlParserException_t1645_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SmallXmlParserException_t1645)/* instance_size */
	, sizeof (SmallXmlParserException_t1645)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Runtime
#include "mscorlib_Mono_Runtime.h"
// Metadata Definition Mono.Runtime
extern TypeInfo Runtime_t1646_il2cpp_TypeInfo;
// Mono.Runtime
#include "mscorlib_Mono_RuntimeMethodDeclarations.h"
static const EncodedMethodIndex Runtime_t1646_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Runtime_t1646_0_0_0;
extern const Il2CppType Runtime_t1646_1_0_0;
struct Runtime_t1646;
const Il2CppTypeDefinitionMetadata Runtime_t1646_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Runtime_t1646_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9767/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Runtime_t1646_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Runtime"/* name */
	, "Mono"/* namespaze */
	, NULL/* methods */
	, &Runtime_t1646_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Runtime_t1646_0_0_0/* byval_arg */
	, &Runtime_t1646_1_0_0/* this_arg */
	, &Runtime_t1646_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Runtime_t1646)/* instance_size */
	, sizeof (Runtime_t1646)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.CollectionDebuggerView`1
extern TypeInfo CollectionDebuggerView_1_t3444_il2cpp_TypeInfo;
static const EncodedMethodIndex CollectionDebuggerView_1_t3444_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CollectionDebuggerView_1_t3444_0_0_0;
extern const Il2CppType CollectionDebuggerView_1_t3444_1_0_0;
struct CollectionDebuggerView_1_t3444;
const Il2CppTypeDefinitionMetadata CollectionDebuggerView_1_t3444_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CollectionDebuggerView_1_t3444_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CollectionDebuggerView_1_t3444_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CollectionDebuggerView`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &CollectionDebuggerView_1_t3444_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CollectionDebuggerView_1_t3444_0_0_0/* byval_arg */
	, &CollectionDebuggerView_1_t3444_1_0_0/* this_arg */
	, &CollectionDebuggerView_1_t3444_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 142/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.CollectionDebuggerView`2
extern TypeInfo CollectionDebuggerView_2_t3445_il2cpp_TypeInfo;
static const EncodedMethodIndex CollectionDebuggerView_2_t3445_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CollectionDebuggerView_2_t3445_0_0_0;
extern const Il2CppType CollectionDebuggerView_2_t3445_1_0_0;
struct CollectionDebuggerView_2_t3445;
const Il2CppTypeDefinitionMetadata CollectionDebuggerView_2_t3445_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CollectionDebuggerView_2_t3445_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CollectionDebuggerView_2_t3445_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CollectionDebuggerView`2"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &CollectionDebuggerView_2_t3445_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CollectionDebuggerView_2_t3445_0_0_0/* byval_arg */
	, &CollectionDebuggerView_2_t3445_1_0_0/* this_arg */
	, &CollectionDebuggerView_2_t3445_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 143/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Comparer`1
extern TypeInfo Comparer_1_t3446_il2cpp_TypeInfo;
extern const Il2CppType DefaultComparer_t3447_0_0_0;
static const Il2CppType* Comparer_1_t3446_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DefaultComparer_t3447_0_0_0,
};
static const EncodedMethodIndex Comparer_1_t3446_VTable[7] = 
{
	626,
	601,
	627,
	628,
	2785,
	2786,
	0,
};
extern const Il2CppType IComparer_1_t3788_0_0_0;
extern const Il2CppType IComparer_t416_0_0_0;
static const Il2CppType* Comparer_1_t3446_InterfacesTypeInfos[] = 
{
	&IComparer_1_t3788_0_0_0,
	&IComparer_t416_0_0_0,
};
static Il2CppInterfaceOffsetPair Comparer_1_t3446_InterfacesOffsets[] = 
{
	{ &IComparer_1_t3788_0_0_0, 4},
	{ &IComparer_t416_0_0_0, 5},
};
extern const Il2CppType IComparable_1_t3789_0_0_0;
extern const Il2CppType Comparer_1_t3446_gp_0_0_0_0;
extern const Il2CppType Comparer_1_t3791_0_0_0;
extern const Il2CppType DefaultComparer_t3792_0_0_0;
extern const Il2CppRGCTXDefinition Comparer_1_t3446_RGCTXData[9] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 7257 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, 4956 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4957 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4957 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7258 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5576 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4956 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5577 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Comparer_1_t3446_0_0_0;
extern const Il2CppType Comparer_1_t3446_1_0_0;
struct Comparer_1_t3446;
const Il2CppTypeDefinitionMetadata Comparer_1_t3446_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Comparer_1_t3446_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Comparer_1_t3446_InterfacesTypeInfos/* implementedInterfaces */
	, Comparer_1_t3446_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Comparer_1_t3446_VTable/* vtableMethods */
	, Comparer_1_t3446_RGCTXData/* rgctxDefinition */
	, 6837/* fieldStart */
	, 9768/* methodStart */
	, -1/* eventStart */
	, 1826/* propertyStart */

};
TypeInfo Comparer_1_t3446_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &Comparer_1_t3446_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Comparer_1_t3446_0_0_0/* byval_arg */
	, &Comparer_1_t3446_1_0_0/* this_arg */
	, &Comparer_1_t3446_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 144/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8321/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 7/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer
extern TypeInfo DefaultComparer_t3447_il2cpp_TypeInfo;
static const EncodedMethodIndex DefaultComparer_t3447_VTable[7] = 
{
	626,
	601,
	627,
	628,
	2787,
	2147486436,
	2787,
};
extern const Il2CppType IComparer_1_t3793_0_0_0;
static Il2CppInterfaceOffsetPair DefaultComparer_t3447_InterfacesOffsets[] = 
{
	{ &IComparer_1_t3793_0_0_0, 4},
	{ &IComparer_t416_0_0_0, 5},
};
extern const Il2CppType Comparer_1_t3794_0_0_0;
extern const Il2CppType DefaultComparer_t3447_gp_0_0_0_0;
extern const Il2CppType IComparable_1_t3796_0_0_0;
extern const Il2CppRGCTXDefinition DefaultComparer_t3447_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5579 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7262 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4959 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7263 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DefaultComparer_t3447_1_0_0;
struct DefaultComparer_t3447;
const Il2CppTypeDefinitionMetadata DefaultComparer_t3447_DefinitionMetadata = 
{
	&Comparer_1_t3446_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultComparer_t3447_InterfacesOffsets/* interfaceOffsets */
	, &Comparer_1_t3794_0_0_0/* parent */
	, DefaultComparer_t3447_VTable/* vtableMethods */
	, DefaultComparer_t3447_RGCTXData/* rgctxDefinition */
	, -1/* fieldStart */
	, 9773/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DefaultComparer_t3447_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultComparer"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DefaultComparer_t3447_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultComparer_t3447_0_0_0/* byval_arg */
	, &DefaultComparer_t3447_1_0_0/* this_arg */
	, &DefaultComparer_t3447_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 145/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.GenericComparer`1
extern TypeInfo GenericComparer_1_t3391_il2cpp_TypeInfo;
static const EncodedMethodIndex GenericComparer_1_t3391_VTable[7] = 
{
	626,
	601,
	627,
	628,
	2789,
	2147486438,
	2789,
};
extern const Il2CppType IComparer_1_t3797_0_0_0;
static Il2CppInterfaceOffsetPair GenericComparer_1_t3391_InterfacesOffsets[] = 
{
	{ &IComparer_1_t3797_0_0_0, 4},
	{ &IComparer_t416_0_0_0, 5},
};
extern const Il2CppType Comparer_1_t3798_0_0_0;
extern const Il2CppType GenericComparer_1_t3391_gp_0_0_0_0;
extern const Il2CppType IComparable_1_t3800_0_0_0;
extern const Il2CppRGCTXDefinition GenericComparer_1_t3391_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5581 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7266 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4960 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4961 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GenericComparer_1_t3391_0_0_0;
extern const Il2CppType GenericComparer_1_t3391_1_0_0;
struct GenericComparer_1_t3391;
const Il2CppTypeDefinitionMetadata GenericComparer_1_t3391_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GenericComparer_1_t3391_InterfacesOffsets/* interfaceOffsets */
	, &Comparer_1_t3798_0_0_0/* parent */
	, GenericComparer_1_t3391_VTable/* vtableMethods */
	, GenericComparer_1_t3391_RGCTXData/* rgctxDefinition */
	, -1/* fieldStart */
	, 9775/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GenericComparer_1_t3391_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &GenericComparer_1_t3391_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GenericComparer_1_t3391_0_0_0/* byval_arg */
	, &GenericComparer_1_t3391_1_0_0/* this_arg */
	, &GenericComparer_1_t3391_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 146/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Generic.Link
#include "mscorlib_System_Collections_Generic_Link.h"
// Metadata Definition System.Collections.Generic.Link
extern TypeInfo Link_t1647_il2cpp_TypeInfo;
// System.Collections.Generic.Link
#include "mscorlib_System_Collections_Generic_LinkMethodDeclarations.h"
static const EncodedMethodIndex Link_t1647_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Link_t1647_0_0_0;
extern const Il2CppType Link_t1647_1_0_0;
const Il2CppTypeDefinitionMetadata Link_t1647_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Link_t1647_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6838/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Link_t1647_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Link"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &Link_t1647_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Link_t1647_0_0_0/* byval_arg */
	, &Link_t1647_1_0_0/* this_arg */
	, &Link_t1647_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Link_t1647)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Link_t1647)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Link_t1647 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Dictionary`2
extern TypeInfo Dictionary_2_t3448_il2cpp_TypeInfo;
extern const Il2CppType Dictionary_2_Do_CopyTo_m24657_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_Do_CopyTo_m24657_gp_1_0_0_0;
extern const Il2CppRGCTXDefinition Dictionary_2_Do_CopyTo_m24657_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5582 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4974 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4972 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m24662_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Dictionary_2_Do_ICollectionCopyTo_m24662_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 4976 }/* Type */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5583 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType ShimEnumerator_t3449_0_0_0;
extern const Il2CppType Enumerator_t3450_0_0_0;
extern const Il2CppType KeyCollection_t3451_0_0_0;
extern const Il2CppType ValueCollection_t3453_0_0_0;
extern const Il2CppType Transform_1_t3455_0_0_0;
static const Il2CppType* Dictionary_2_t3448_il2cpp_TypeInfo__nestedTypes[5] =
{
	&ShimEnumerator_t3449_0_0_0,
	&Enumerator_t3450_0_0_0,
	&KeyCollection_t3451_0_0_0,
	&ValueCollection_t3453_0_0_0,
	&Transform_1_t3455_0_0_0,
};
static const EncodedMethodIndex Dictionary_2_t3448_VTable[35] = 
{
	626,
	601,
	627,
	628,
	2791,
	2792,
	2793,
	2794,
	2795,
	2796,
	2793,
	2797,
	2798,
	2799,
	2800,
	2801,
	2802,
	2803,
	2804,
	2805,
	2806,
	2807,
	2808,
	2809,
	2810,
	2811,
	2812,
	2813,
	2814,
	2815,
	2816,
	2817,
	2792,
	2816,
	2818,
};
extern const Il2CppType ICollection_1_t3804_0_0_0;
extern const Il2CppType IEnumerable_1_t3805_0_0_0;
extern const Il2CppType IDictionary_2_t3806_0_0_0;
extern const Il2CppType IDictionary_t1462_0_0_0;
extern const Il2CppType IDeserializationCallback_t2213_0_0_0;
static const Il2CppType* Dictionary_2_t3448_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ISerializable_t2210_0_0_0,
	&ICollection_t1528_0_0_0,
	&ICollection_1_t3804_0_0_0,
	&IEnumerable_1_t3805_0_0_0,
	&IDictionary_2_t3806_0_0_0,
	&IDictionary_t1462_0_0_0,
	&IDeserializationCallback_t2213_0_0_0,
};
static Il2CppInterfaceOffsetPair Dictionary_2_t3448_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 5},
	{ &ICollection_t1528_0_0_0, 6},
	{ &ICollection_1_t3804_0_0_0, 10},
	{ &IEnumerable_1_t3805_0_0_0, 17},
	{ &IDictionary_2_t3806_0_0_0, 18},
	{ &IDictionary_t1462_0_0_0, 24},
	{ &IDeserializationCallback_t2213_0_0_0, 30},
};
extern const Il2CppType IEnumerator_1_t3807_0_0_0;
extern const Il2CppType Dictionary_2_t3448_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t3448_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2U5BU5D_t3810_0_0_0;
extern const Il2CppType Dictionary_2_t3811_0_0_0;
extern const Il2CppType Transform_1_t3812_0_0_0;
extern const Il2CppType Transform_1_t3813_0_0_0;
extern const Il2CppType Enumerator_t3814_0_0_0;
extern const Il2CppType ShimEnumerator_t3815_0_0_0;
extern const Il2CppType IEqualityComparer_1_t3816_0_0_0;
extern const Il2CppType EqualityComparer_1_t3817_0_0_0;
extern const Il2CppType TKeyU5BU5D_t3818_0_0_0;
extern const Il2CppType TValueU5BU5D_t3819_0_0_0;
extern const Il2CppType KeyValuePair_2_t3820_0_0_0;
extern const Il2CppType EqualityComparer_1_t3821_0_0_0;
extern const Il2CppType IEqualityComparer_1_t3822_0_0_0;
extern const Il2CppType KeyCollection_t3823_0_0_0;
extern const Il2CppType ValueCollection_t3824_0_0_0;
extern const Il2CppRGCTXDefinition Dictionary_2_t3448_RGCTXData[61] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5584 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5585 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7274 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7275 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4970 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5586 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5587 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5588 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5589 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5590 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4963 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5591 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5592 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5593 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4965 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5594 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5595 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5596 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5597 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5598 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4969 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5599 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7276 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5600 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7277 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5601 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5602 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5603 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7278 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5604 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5605 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4980 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5606 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7279 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5607 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4962 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5608 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5609 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7280 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5610 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7281 }/* Array */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7282 }/* Array */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5611 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4968 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5612 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5613 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5614 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7283 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7284 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4969 }/* Array */,
	{ IL2CPP_RGCTX_DATA_TYPE, 4962 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, 4969 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4978 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5615 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4979 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5616 }/* Method */,
	{ IL2CPP_RGCTX_DATA_TYPE, 4963 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, 4965 }/* Type */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5617 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5618 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Dictionary_2_t3448_0_0_0;
extern const Il2CppType Dictionary_2_t3448_1_0_0;
struct Dictionary_2_t3448;
const Il2CppTypeDefinitionMetadata Dictionary_2_t3448_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Dictionary_2_t3448_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Dictionary_2_t3448_InterfacesTypeInfos/* implementedInterfaces */
	, Dictionary_2_t3448_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Dictionary_2_t3448_VTable/* vtableMethods */
	, Dictionary_2_t3448_RGCTXData/* rgctxDefinition */
	, 6840/* fieldStart */
	, 9777/* methodStart */
	, -1/* eventStart */
	, 1827/* propertyStart */

};
TypeInfo Dictionary_2_t3448_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Dictionary`2"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &Dictionary_2_t3448_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2676/* custom_attributes_cache */
	, &Dictionary_2_t3448_0_0_0/* byval_arg */
	, &Dictionary_2_t3448_1_0_0/* this_arg */
	, &Dictionary_2_t3448_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 149/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 52/* method_count */
	, 10/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 5/* nested_type_count */
	, 35/* vtable_count */
	, 8/* interfaces_count */
	, 8/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Dictionary`2/ShimEnumerator
extern TypeInfo ShimEnumerator_t3449_il2cpp_TypeInfo;
static const EncodedMethodIndex ShimEnumerator_t3449_VTable[9] = 
{
	626,
	601,
	627,
	628,
	2819,
	2820,
	2821,
	2822,
	2823,
};
extern const Il2CppType IDictionaryEnumerator_t1527_0_0_0;
static const Il2CppType* ShimEnumerator_t3449_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDictionaryEnumerator_t1527_0_0_0,
};
static Il2CppInterfaceOffsetPair ShimEnumerator_t3449_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IDictionaryEnumerator_t1527_0_0_0, 6},
};
extern const Il2CppType Enumerator_t3825_0_0_0;
extern const Il2CppType ShimEnumerator_t3449_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3449_gp_1_0_0_0;
extern const Il2CppRGCTXDefinition ShimEnumerator_t3449_RGCTXData[10] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5619 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5620 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7287 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5621 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5622 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4987 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5623 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4988 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5624 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ShimEnumerator_t3449_1_0_0;
struct ShimEnumerator_t3449;
const Il2CppTypeDefinitionMetadata ShimEnumerator_t3449_DefinitionMetadata = 
{
	&Dictionary_2_t3448_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, ShimEnumerator_t3449_InterfacesTypeInfos/* implementedInterfaces */
	, ShimEnumerator_t3449_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ShimEnumerator_t3449_VTable/* vtableMethods */
	, ShimEnumerator_t3449_RGCTXData/* rgctxDefinition */
	, 6856/* fieldStart */
	, 9829/* methodStart */
	, -1/* eventStart */
	, 1837/* propertyStart */

};
TypeInfo ShimEnumerator_t3449_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ShimEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ShimEnumerator_t3449_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ShimEnumerator_t3449_0_0_0/* byval_arg */
	, &ShimEnumerator_t3449_1_0_0/* this_arg */
	, &ShimEnumerator_t3449_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 150/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 4/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Dictionary`2/Enumerator
extern TypeInfo Enumerator_t3450_il2cpp_TypeInfo;
static const EncodedMethodIndex Enumerator_t3450_VTable[11] = 
{
	652,
	601,
	653,
	654,
	2824,
	2825,
	2826,
	2827,
	2828,
	2829,
	2830,
};
extern const Il2CppType IEnumerator_1_t3828_0_0_0;
static const Il2CppType* Enumerator_t3450_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDisposable_t538_0_0_0,
	&IEnumerator_1_t3828_0_0_0,
	&IDictionaryEnumerator_t1527_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t3450_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IDisposable_t538_0_0_0, 6},
	{ &IEnumerator_1_t3828_0_0_0, 7},
	{ &IDictionaryEnumerator_t1527_0_0_0, 8},
};
extern const Il2CppType KeyValuePair_2_t3829_0_0_0;
extern const Il2CppType Enumerator_t3450_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3450_gp_1_0_0_0;
extern const Il2CppRGCTXDefinition Enumerator_t3450_RGCTXData[11] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5625 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4993 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5626 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4991 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5627 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4992 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5628 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5629 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5630 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5631 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Enumerator_t3450_1_0_0;
const Il2CppTypeDefinitionMetadata Enumerator_t3450_DefinitionMetadata = 
{
	&Dictionary_2_t3448_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t3450_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t3450_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Enumerator_t3450_VTable/* vtableMethods */
	, Enumerator_t3450_RGCTXData/* rgctxDefinition */
	, 6857/* fieldStart */
	, 9835/* methodStart */
	, -1/* eventStart */
	, 1841/* propertyStart */

};
TypeInfo Enumerator_t3450_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t3450_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t3450_0_0_0/* byval_arg */
	, &Enumerator_t3450_1_0_0/* this_arg */
	, &Enumerator_t3450_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 151/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 7/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Dictionary`2/KeyCollection
extern TypeInfo KeyCollection_t3451_il2cpp_TypeInfo;
extern const Il2CppType Enumerator_t3452_0_0_0;
static const Il2CppType* KeyCollection_t3451_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Enumerator_t3452_0_0_0,
};
static const EncodedMethodIndex KeyCollection_t3451_VTable[17] = 
{
	626,
	601,
	627,
	628,
	2831,
	2832,
	2833,
	2834,
	2835,
	2832,
	2836,
	2837,
	2838,
	2839,
	2840,
	2841,
	2842,
};
extern const Il2CppType ICollection_1_t3832_0_0_0;
extern const Il2CppType IEnumerable_1_t3833_0_0_0;
static const Il2CppType* KeyCollection_t3451_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICollection_t1528_0_0_0,
	&ICollection_1_t3832_0_0_0,
	&IEnumerable_1_t3833_0_0_0,
};
static Il2CppInterfaceOffsetPair KeyCollection_t3451_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &ICollection_1_t3832_0_0_0, 9},
	{ &IEnumerable_1_t3833_0_0_0, 16},
};
extern const Il2CppType Enumerator_t3834_0_0_0;
extern const Il2CppType TKeyU5BU5D_t3835_0_0_0;
extern const Il2CppType Transform_1_t3836_0_0_0;
extern const Il2CppRGCTXDefinition KeyCollection_t3451_RGCTXData[14] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5632 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5633 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5001 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5000 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5634 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5635 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5636 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7294 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5637 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5638 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5639 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5640 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5641 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyCollection_t3451_1_0_0;
struct KeyCollection_t3451;
const Il2CppTypeDefinitionMetadata KeyCollection_t3451_DefinitionMetadata = 
{
	&Dictionary_2_t3448_0_0_0/* declaringType */
	, KeyCollection_t3451_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, KeyCollection_t3451_InterfacesTypeInfos/* implementedInterfaces */
	, KeyCollection_t3451_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, KeyCollection_t3451_VTable/* vtableMethods */
	, KeyCollection_t3451_RGCTXData/* rgctxDefinition */
	, 6861/* fieldStart */
	, 9847/* methodStart */
	, -1/* eventStart */
	, 1848/* propertyStart */

};
TypeInfo KeyCollection_t3451_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyCollection"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &KeyCollection_t3451_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2679/* custom_attributes_cache */
	, &KeyCollection_t3451_0_0_0/* byval_arg */
	, &KeyCollection_t3451_1_0_0/* this_arg */
	, &KeyCollection_t3451_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 152/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057026/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 4/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 17/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator
extern TypeInfo Enumerator_t3452_il2cpp_TypeInfo;
static const EncodedMethodIndex Enumerator_t3452_VTable[8] = 
{
	652,
	601,
	653,
	654,
	2843,
	2844,
	2845,
	2846,
};
extern const Il2CppType IEnumerator_1_t3837_0_0_0;
static const Il2CppType* Enumerator_t3452_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDisposable_t538_0_0_0,
	&IEnumerator_1_t3837_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t3452_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IDisposable_t538_0_0_0, 6},
	{ &IEnumerator_1_t3837_0_0_0, 7},
};
extern const Il2CppType Enumerator_t3452_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Enumerator_t3452_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5642 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5643 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5004 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5644 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5645 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5646 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Enumerator_t3452_1_0_0;
const Il2CppTypeDefinitionMetadata Enumerator_t3452_DefinitionMetadata = 
{
	&KeyCollection_t3451_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t3452_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t3452_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Enumerator_t3452_VTable/* vtableMethods */
	, Enumerator_t3452_RGCTXData/* rgctxDefinition */
	, 6862/* fieldStart */
	, 9861/* methodStart */
	, -1/* eventStart */
	, 1852/* propertyStart */

};
TypeInfo Enumerator_t3452_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t3452_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t3452_0_0_0/* byval_arg */
	, &Enumerator_t3452_1_0_0/* this_arg */
	, &Enumerator_t3452_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 153/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection
extern TypeInfo ValueCollection_t3453_il2cpp_TypeInfo;
extern const Il2CppType Enumerator_t3454_0_0_0;
static const Il2CppType* ValueCollection_t3453_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Enumerator_t3454_0_0_0,
};
static const EncodedMethodIndex ValueCollection_t3453_VTable[17] = 
{
	626,
	601,
	627,
	628,
	2847,
	2848,
	2849,
	2850,
	2851,
	2848,
	2852,
	2853,
	2854,
	2855,
	2856,
	2857,
	2858,
};
extern const Il2CppType ICollection_1_t3839_0_0_0;
extern const Il2CppType IEnumerable_1_t3840_0_0_0;
static const Il2CppType* ValueCollection_t3453_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICollection_t1528_0_0_0,
	&ICollection_1_t3839_0_0_0,
	&IEnumerable_1_t3840_0_0_0,
};
static Il2CppInterfaceOffsetPair ValueCollection_t3453_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &ICollection_1_t3839_0_0_0, 9},
	{ &IEnumerable_1_t3840_0_0_0, 16},
};
extern const Il2CppType Enumerator_t3841_0_0_0;
extern const Il2CppType TValueU5BU5D_t3842_0_0_0;
extern const Il2CppType Transform_1_t3843_0_0_0;
extern const Il2CppRGCTXDefinition ValueCollection_t3453_RGCTXData[14] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5647 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5648 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5012 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5011 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5649 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5650 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5651 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7301 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5652 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5653 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5654 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5655 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5656 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ValueCollection_t3453_1_0_0;
struct ValueCollection_t3453;
const Il2CppTypeDefinitionMetadata ValueCollection_t3453_DefinitionMetadata = 
{
	&Dictionary_2_t3448_0_0_0/* declaringType */
	, ValueCollection_t3453_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ValueCollection_t3453_InterfacesTypeInfos/* implementedInterfaces */
	, ValueCollection_t3453_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ValueCollection_t3453_VTable/* vtableMethods */
	, ValueCollection_t3453_RGCTXData/* rgctxDefinition */
	, 6863/* fieldStart */
	, 9866/* methodStart */
	, -1/* eventStart */
	, 1854/* propertyStart */

};
TypeInfo ValueCollection_t3453_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ValueCollection"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ValueCollection_t3453_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2680/* custom_attributes_cache */
	, &ValueCollection_t3453_0_0_0/* byval_arg */
	, &ValueCollection_t3453_1_0_0/* this_arg */
	, &ValueCollection_t3453_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 154/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057026/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 4/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 17/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator
extern TypeInfo Enumerator_t3454_il2cpp_TypeInfo;
static const EncodedMethodIndex Enumerator_t3454_VTable[8] = 
{
	652,
	601,
	653,
	654,
	2859,
	2860,
	2861,
	2862,
};
extern const Il2CppType IEnumerator_1_t3844_0_0_0;
static const Il2CppType* Enumerator_t3454_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDisposable_t538_0_0_0,
	&IEnumerator_1_t3844_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t3454_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IDisposable_t538_0_0_0, 6},
	{ &IEnumerator_1_t3844_0_0_0, 7},
};
extern const Il2CppType Enumerator_t3454_gp_1_0_0_0;
extern const Il2CppRGCTXDefinition Enumerator_t3454_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5657 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5658 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5016 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5659 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5660 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5661 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Enumerator_t3454_1_0_0;
const Il2CppTypeDefinitionMetadata Enumerator_t3454_DefinitionMetadata = 
{
	&ValueCollection_t3453_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t3454_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t3454_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Enumerator_t3454_VTable/* vtableMethods */
	, Enumerator_t3454_RGCTXData/* rgctxDefinition */
	, 6864/* fieldStart */
	, 9880/* methodStart */
	, -1/* eventStart */
	, 1858/* propertyStart */

};
TypeInfo Enumerator_t3454_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t3454_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t3454_0_0_0/* byval_arg */
	, &Enumerator_t3454_1_0_0/* this_arg */
	, &Enumerator_t3454_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 155/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1
extern TypeInfo Transform_1_t3455_il2cpp_TypeInfo;
static const EncodedMethodIndex Transform_1_t3455_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	2863,
	2864,
	2865,
};
static Il2CppInterfaceOffsetPair Transform_1_t3455_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Transform_1_t3455_1_0_0;
struct Transform_1_t3455;
const Il2CppTypeDefinitionMetadata Transform_1_t3455_DefinitionMetadata = 
{
	&Dictionary_2_t3448_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Transform_1_t3455_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, Transform_1_t3455_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9885/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Transform_1_t3455_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Transform`1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Transform_1_t3455_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Transform_1_t3455_0_0_0/* byval_arg */
	, &Transform_1_t3455_1_0_0/* this_arg */
	, &Transform_1_t3455_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 156/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.EqualityComparer`1
extern TypeInfo EqualityComparer_1_t3456_il2cpp_TypeInfo;
extern const Il2CppType DefaultComparer_t3457_0_0_0;
static const Il2CppType* EqualityComparer_1_t3456_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DefaultComparer_t3457_0_0_0,
};
static const EncodedMethodIndex EqualityComparer_1_t3456_VTable[10] = 
{
	626,
	601,
	627,
	628,
	2866,
	2867,
	2868,
	2869,
	0,
	0,
};
extern const Il2CppType IEqualityComparer_1_t3846_0_0_0;
extern const Il2CppType IEqualityComparer_t1387_0_0_0;
static const Il2CppType* EqualityComparer_1_t3456_InterfacesTypeInfos[] = 
{
	&IEqualityComparer_1_t3846_0_0_0,
	&IEqualityComparer_t1387_0_0_0,
};
static Il2CppInterfaceOffsetPair EqualityComparer_1_t3456_InterfacesOffsets[] = 
{
	{ &IEqualityComparer_1_t3846_0_0_0, 4},
	{ &IEqualityComparer_t1387_0_0_0, 6},
};
extern const Il2CppType IEquatable_1_t3847_0_0_0;
extern const Il2CppType EqualityComparer_1_t3456_gp_0_0_0_0;
extern const Il2CppType EqualityComparer_1_t3849_0_0_0;
extern const Il2CppType DefaultComparer_t3850_0_0_0;
extern const Il2CppRGCTXDefinition EqualityComparer_1_t3456_RGCTXData[10] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 7308 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, 5021 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5022 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5022 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7309 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5662 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5021 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5663 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5664 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EqualityComparer_1_t3456_0_0_0;
extern const Il2CppType EqualityComparer_1_t3456_1_0_0;
struct EqualityComparer_1_t3456;
const Il2CppTypeDefinitionMetadata EqualityComparer_1_t3456_DefinitionMetadata = 
{
	NULL/* declaringType */
	, EqualityComparer_1_t3456_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, EqualityComparer_1_t3456_InterfacesTypeInfos/* implementedInterfaces */
	, EqualityComparer_1_t3456_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EqualityComparer_1_t3456_VTable/* vtableMethods */
	, EqualityComparer_1_t3456_RGCTXData/* rgctxDefinition */
	, 6865/* fieldStart */
	, 9889/* methodStart */
	, -1/* eventStart */
	, 1860/* propertyStart */

};
TypeInfo EqualityComparer_1_t3456_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EqualityComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &EqualityComparer_1_t3456_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EqualityComparer_1_t3456_0_0_0/* byval_arg */
	, &EqualityComparer_1_t3456_1_0_0/* this_arg */
	, &EqualityComparer_1_t3456_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 157/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8321/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer
extern TypeInfo DefaultComparer_t3457_il2cpp_TypeInfo;
static const EncodedMethodIndex DefaultComparer_t3457_VTable[10] = 
{
	626,
	601,
	627,
	628,
	2870,
	2871,
	2147486520,
	2147486521,
	2871,
	2870,
};
extern const Il2CppType IEqualityComparer_1_t3851_0_0_0;
static Il2CppInterfaceOffsetPair DefaultComparer_t3457_InterfacesOffsets[] = 
{
	{ &IEqualityComparer_1_t3851_0_0_0, 4},
	{ &IEqualityComparer_t1387_0_0_0, 6},
};
extern const Il2CppType EqualityComparer_1_t3852_0_0_0;
extern const Il2CppType DefaultComparer_t3457_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition DefaultComparer_t3457_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5667 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7313 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5024 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DefaultComparer_t3457_1_0_0;
struct DefaultComparer_t3457;
const Il2CppTypeDefinitionMetadata DefaultComparer_t3457_DefinitionMetadata = 
{
	&EqualityComparer_1_t3456_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultComparer_t3457_InterfacesOffsets/* interfaceOffsets */
	, &EqualityComparer_1_t3852_0_0_0/* parent */
	, DefaultComparer_t3457_VTable/* vtableMethods */
	, DefaultComparer_t3457_RGCTXData/* rgctxDefinition */
	, -1/* fieldStart */
	, 9896/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DefaultComparer_t3457_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultComparer"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DefaultComparer_t3457_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DefaultComparer_t3457_0_0_0/* byval_arg */
	, &DefaultComparer_t3457_1_0_0/* this_arg */
	, &DefaultComparer_t3457_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 158/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057027/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.GenericEqualityComparer`1
extern TypeInfo GenericEqualityComparer_1_t3390_il2cpp_TypeInfo;
static const EncodedMethodIndex GenericEqualityComparer_1_t3390_VTable[10] = 
{
	626,
	601,
	627,
	628,
	2874,
	2875,
	2147486524,
	2147486525,
	2875,
	2874,
};
extern const Il2CppType IEqualityComparer_1_t3854_0_0_0;
static Il2CppInterfaceOffsetPair GenericEqualityComparer_1_t3390_InterfacesOffsets[] = 
{
	{ &IEqualityComparer_1_t3854_0_0_0, 4},
	{ &IEqualityComparer_t1387_0_0_0, 6},
};
extern const Il2CppType EqualityComparer_1_t3855_0_0_0;
extern const Il2CppType GenericEqualityComparer_1_t3390_gp_0_0_0_0;
extern const Il2CppType IEquatable_1_t3857_0_0_0;
extern const Il2CppRGCTXDefinition GenericEqualityComparer_1_t3390_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5670 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7316 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5025 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5026 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GenericEqualityComparer_1_t3390_0_0_0;
extern const Il2CppType GenericEqualityComparer_1_t3390_1_0_0;
struct GenericEqualityComparer_1_t3390;
const Il2CppTypeDefinitionMetadata GenericEqualityComparer_1_t3390_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GenericEqualityComparer_1_t3390_InterfacesOffsets/* interfaceOffsets */
	, &EqualityComparer_1_t3855_0_0_0/* parent */
	, GenericEqualityComparer_1_t3390_VTable/* vtableMethods */
	, GenericEqualityComparer_1_t3390_RGCTXData/* rgctxDefinition */
	, -1/* fieldStart */
	, 9899/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GenericEqualityComparer_1_t3390_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericEqualityComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &GenericEqualityComparer_1_t3390_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GenericEqualityComparer_1_t3390_0_0_0/* byval_arg */
	, &GenericEqualityComparer_1_t3390_1_0_0/* this_arg */
	, &GenericEqualityComparer_1_t3390_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 159/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.IComparer`1
extern TypeInfo IComparer_1_t3458_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IComparer_1_t3458_0_0_0;
extern const Il2CppType IComparer_1_t3458_1_0_0;
struct IComparer_1_t3458;
const Il2CppTypeDefinitionMetadata IComparer_1_t3458_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9902/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IComparer_1_t3458_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &IComparer_1_t3458_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IComparer_1_t3458_0_0_0/* byval_arg */
	, &IComparer_1_t3458_1_0_0/* this_arg */
	, &IComparer_1_t3458_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 160/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.IDictionary`2
extern TypeInfo IDictionary_2_t3459_il2cpp_TypeInfo;
extern const Il2CppType ICollection_1_t3858_0_0_0;
extern const Il2CppType IEnumerable_1_t3859_0_0_0;
static const Il2CppType* IDictionary_2_t3459_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICollection_1_t3858_0_0_0,
	&IEnumerable_1_t3859_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IDictionary_2_t3459_0_0_0;
extern const Il2CppType IDictionary_2_t3459_1_0_0;
struct IDictionary_2_t3459;
const Il2CppTypeDefinitionMetadata IDictionary_2_t3459_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IDictionary_2_t3459_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9903/* methodStart */
	, -1/* eventStart */
	, 1861/* propertyStart */

};
TypeInfo IDictionary_2_t3459_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDictionary`2"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &IDictionary_2_t3459_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2681/* custom_attributes_cache */
	, &IDictionary_2_t3459_0_0_0/* byval_arg */
	, &IDictionary_2_t3459_1_0_0/* this_arg */
	, &IDictionary_2_t3459_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 161/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.IEqualityComparer`1
extern TypeInfo IEqualityComparer_1_t3460_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEqualityComparer_1_t3460_0_0_0;
extern const Il2CppType IEqualityComparer_1_t3460_1_0_0;
struct IEqualityComparer_1_t3460;
const Il2CppTypeDefinitionMetadata IEqualityComparer_1_t3460_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9909/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IEqualityComparer_1_t3460_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEqualityComparer`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &IEqualityComparer_1_t3460_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IEqualityComparer_1_t3460_0_0_0/* byval_arg */
	, &IEqualityComparer_1_t3460_1_0_0/* this_arg */
	, &IEqualityComparer_1_t3460_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 162/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Generic.KeyNotFoundException
#include "mscorlib_System_Collections_Generic_KeyNotFoundException.h"
// Metadata Definition System.Collections.Generic.KeyNotFoundException
extern TypeInfo KeyNotFoundException_t1648_il2cpp_TypeInfo;
// System.Collections.Generic.KeyNotFoundException
#include "mscorlib_System_Collections_Generic_KeyNotFoundExceptionMethodDeclarations.h"
static const EncodedMethodIndex KeyNotFoundException_t1648_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static const Il2CppType* KeyNotFoundException_t1648_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair KeyNotFoundException_t1648_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyNotFoundException_t1648_0_0_0;
extern const Il2CppType KeyNotFoundException_t1648_1_0_0;
struct KeyNotFoundException_t1648;
const Il2CppTypeDefinitionMetadata KeyNotFoundException_t1648_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, KeyNotFoundException_t1648_InterfacesTypeInfos/* implementedInterfaces */
	, KeyNotFoundException_t1648_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, KeyNotFoundException_t1648_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9911/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo KeyNotFoundException_t1648_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyNotFoundException"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &KeyNotFoundException_t1648_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2682/* custom_attributes_cache */
	, &KeyNotFoundException_t1648_0_0_0/* byval_arg */
	, &KeyNotFoundException_t1648_1_0_0/* this_arg */
	, &KeyNotFoundException_t1648_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyNotFoundException_t1648)/* instance_size */
	, sizeof (KeyNotFoundException_t1648)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.KeyValuePair`2
extern TypeInfo KeyValuePair_2_t3461_il2cpp_TypeInfo;
static const EncodedMethodIndex KeyValuePair_2_t3461_VTable[4] = 
{
	652,
	601,
	653,
	2878,
};
extern const Il2CppType KeyValuePair_2_t3461_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t3461_gp_1_0_0_0;
extern const Il2CppRGCTXDefinition KeyValuePair_2_t3461_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5671 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5672 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5673 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5034 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5674 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5035 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyValuePair_2_t3461_0_0_0;
extern const Il2CppType KeyValuePair_2_t3461_1_0_0;
const Il2CppTypeDefinitionMetadata KeyValuePair_2_t3461_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, KeyValuePair_2_t3461_VTable/* vtableMethods */
	, KeyValuePair_2_t3461_RGCTXData/* rgctxDefinition */
	, 6866/* fieldStart */
	, 9913/* methodStart */
	, -1/* eventStart */
	, 1864/* propertyStart */

};
TypeInfo KeyValuePair_2_t3461_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &KeyValuePair_2_t3461_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2683/* custom_attributes_cache */
	, &KeyValuePair_2_t3461_0_0_0/* byval_arg */
	, &KeyValuePair_2_t3461_1_0_0/* this_arg */
	, &KeyValuePair_2_t3461_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 163/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.List`1
extern TypeInfo List_1_t3462_il2cpp_TypeInfo;
extern const Il2CppType Enumerator_t3463_0_0_0;
static const Il2CppType* List_1_t3462_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Enumerator_t3463_0_0_0,
};
static const EncodedMethodIndex List_1_t3462_VTable[33] = 
{
	626,
	601,
	627,
	628,
	2879,
	2880,
	2881,
	2882,
	2883,
	2884,
	2885,
	2886,
	2887,
	2888,
	2889,
	2890,
	2891,
	2892,
	2893,
	2894,
	2880,
	2895,
	2896,
	2889,
	2897,
	2898,
	2899,
	2900,
	2901,
	2902,
	2894,
	2903,
	2904,
};
extern const Il2CppType ICollection_1_t3862_0_0_0;
extern const Il2CppType IEnumerable_1_t3863_0_0_0;
extern const Il2CppType IList_1_t3864_0_0_0;
static const Il2CppType* List_1_t3462_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICollection_t1528_0_0_0,
	&IList_t1487_0_0_0,
	&ICollection_1_t3862_0_0_0,
	&IEnumerable_1_t3863_0_0_0,
	&IList_1_t3864_0_0_0,
};
static Il2CppInterfaceOffsetPair List_1_t3462_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
	{ &ICollection_1_t3862_0_0_0, 20},
	{ &IEnumerable_1_t3863_0_0_0, 27},
	{ &IList_1_t3864_0_0_0, 28},
};
extern const Il2CppType List_1_t3865_0_0_0;
extern const Il2CppType TU5BU5D_t3866_0_0_0;
extern const Il2CppType Enumerator_t3867_0_0_0;
extern const Il2CppType List_1_t3462_gp_0_0_0_0;
extern const Il2CppType IEnumerator_1_t3869_0_0_0;
extern const Il2CppType ReadOnlyCollection_1_t3870_0_0_0;
extern const Il2CppType Comparer_1_t3871_0_0_0;
extern const Il2CppRGCTXDefinition List_1_t3462_RGCTXData[37] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 7332 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5675 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5041 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5676 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5043 }/* Array */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5677 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5678 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5045 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5039 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5679 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5680 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5681 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5682 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5683 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5684 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5685 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5686 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5687 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5688 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5689 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5038 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5040 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5042 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5690 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5691 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5692 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5693 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5694 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5695 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5696 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5697 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5698 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7333 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5699 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5700 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5701 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType List_1_t3462_0_0_0;
extern const Il2CppType List_1_t3462_1_0_0;
struct List_1_t3462;
const Il2CppTypeDefinitionMetadata List_1_t3462_DefinitionMetadata = 
{
	NULL/* declaringType */
	, List_1_t3462_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, List_1_t3462_InterfacesTypeInfos/* implementedInterfaces */
	, List_1_t3462_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, List_1_t3462_VTable/* vtableMethods */
	, List_1_t3462_RGCTXData/* rgctxDefinition */
	, 6868/* fieldStart */
	, 9919/* methodStart */
	, -1/* eventStart */
	, 1866/* propertyStart */

};
TypeInfo List_1_t3462_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "List`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &List_1_t3462_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2684/* custom_attributes_cache */
	, &List_1_t3462_0_0_0/* byval_arg */
	, &List_1_t3462_1_0_0/* this_arg */
	, &List_1_t3462_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 164/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 50/* method_count */
	, 9/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 33/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.List`1/Enumerator
extern TypeInfo Enumerator_t3463_il2cpp_TypeInfo;
static const EncodedMethodIndex Enumerator_t3463_VTable[8] = 
{
	652,
	601,
	653,
	654,
	2905,
	2906,
	2907,
	2908,
};
extern const Il2CppType IEnumerator_1_t3872_0_0_0;
static const Il2CppType* Enumerator_t3463_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDisposable_t538_0_0_0,
	&IEnumerator_1_t3872_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t3463_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IDisposable_t538_0_0_0, 6},
	{ &IEnumerator_1_t3872_0_0_0, 7},
};
extern const Il2CppType Enumerator_t3463_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3874_0_0_0;
extern const Il2CppRGCTXDefinition Enumerator_t3463_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5702 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5050 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7337 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Enumerator_t3463_1_0_0;
const Il2CppTypeDefinitionMetadata Enumerator_t3463_DefinitionMetadata = 
{
	&List_1_t3462_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t3463_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t3463_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Enumerator_t3463_VTable/* vtableMethods */
	, Enumerator_t3463_RGCTXData/* rgctxDefinition */
	, 6873/* fieldStart */
	, 9969/* methodStart */
	, -1/* eventStart */
	, 1875/* propertyStart */

};
TypeInfo Enumerator_t3463_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t3463_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t3463_0_0_0/* byval_arg */
	, &Enumerator_t3463_1_0_0/* this_arg */
	, &Enumerator_t3463_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 165/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Collections.ObjectModel.Collection`1
extern TypeInfo Collection_1_t3464_il2cpp_TypeInfo;
static const EncodedMethodIndex Collection_1_t3464_VTable[37] = 
{
	626,
	601,
	627,
	628,
	2909,
	2910,
	2911,
	2912,
	2913,
	2914,
	2915,
	2916,
	2917,
	2918,
	2919,
	2920,
	2921,
	2922,
	2923,
	2924,
	2910,
	2925,
	2926,
	2919,
	2927,
	2928,
	2929,
	2930,
	2931,
	2924,
	2932,
	2933,
	2934,
	2935,
	2936,
	2937,
	2938,
};
extern const Il2CppType ICollection_1_t3875_0_0_0;
extern const Il2CppType IList_1_t3876_0_0_0;
extern const Il2CppType IEnumerable_1_t3877_0_0_0;
static const Il2CppType* Collection_1_t3464_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICollection_t1528_0_0_0,
	&IList_t1487_0_0_0,
	&ICollection_1_t3875_0_0_0,
	&IList_1_t3876_0_0_0,
	&IEnumerable_1_t3877_0_0_0,
};
static Il2CppInterfaceOffsetPair Collection_1_t3464_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
	{ &ICollection_1_t3875_0_0_0, 20},
	{ &IList_1_t3876_0_0_0, 27},
	{ &IEnumerable_1_t3877_0_0_0, 32},
};
extern const Il2CppType List_1_t3878_0_0_0;
extern const Il2CppType Collection_1_t3879_0_0_0;
extern const Il2CppType Collection_1_t3464_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Collection_1_t3464_RGCTXData[19] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 7341 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5703 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7339 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7340 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5704 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7342 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5705 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5706 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5053 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5056 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5707 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5708 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5709 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5710 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5711 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5712 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5713 }/* Method */,
	{ IL2CPP_RGCTX_DATA_TYPE, 5053 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Collection_1_t3464_0_0_0;
extern const Il2CppType Collection_1_t3464_1_0_0;
struct Collection_1_t3464;
const Il2CppTypeDefinitionMetadata Collection_1_t3464_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Collection_1_t3464_InterfacesTypeInfos/* implementedInterfaces */
	, Collection_1_t3464_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Collection_1_t3464_VTable/* vtableMethods */
	, Collection_1_t3464_RGCTXData/* rgctxDefinition */
	, 6877/* fieldStart */
	, 9975/* methodStart */
	, -1/* eventStart */
	, 1877/* propertyStart */

};
TypeInfo Collection_1_t3464_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Collection`1"/* name */
	, "System.Collections.ObjectModel"/* namespaze */
	, NULL/* methods */
	, &Collection_1_t3464_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2685/* custom_attributes_cache */
	, &Collection_1_t3464_0_0_0/* byval_arg */
	, &Collection_1_t3464_1_0_0/* this_arg */
	, &Collection_1_t3464_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 166/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 36/* method_count */
	, 8/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 37/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1
extern TypeInfo ReadOnlyCollection_1_t3465_il2cpp_TypeInfo;
static const EncodedMethodIndex ReadOnlyCollection_1_t3465_VTable[34] = 
{
	626,
	601,
	627,
	628,
	2939,
	2940,
	2941,
	2942,
	2943,
	2944,
	2945,
	2946,
	2947,
	2948,
	2949,
	2950,
	2951,
	2952,
	2953,
	2954,
	2940,
	2955,
	2956,
	2957,
	2958,
	2959,
	2960,
	2961,
	2962,
	2963,
	2964,
	2965,
	2966,
	2967,
};
extern const Il2CppType ICollection_1_t3881_0_0_0;
extern const Il2CppType IList_1_t3882_0_0_0;
extern const Il2CppType IEnumerable_1_t3883_0_0_0;
static const Il2CppType* ReadOnlyCollection_1_t3465_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICollection_t1528_0_0_0,
	&IList_t1487_0_0_0,
	&ICollection_1_t3881_0_0_0,
	&IList_1_t3882_0_0_0,
	&IEnumerable_1_t3883_0_0_0,
};
static Il2CppInterfaceOffsetPair ReadOnlyCollection_1_t3465_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
	{ &ICollection_1_t3881_0_0_0, 20},
	{ &IList_1_t3882_0_0_0, 27},
	{ &IEnumerable_1_t3883_0_0_0, 32},
};
extern const Il2CppType Collection_1_t3884_0_0_0;
extern const Il2CppType ReadOnlyCollection_1_t3465_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition ReadOnlyCollection_1_t3465_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5714 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5715 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7347 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5059 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7345 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5058 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7346 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReadOnlyCollection_1_t3465_0_0_0;
extern const Il2CppType ReadOnlyCollection_1_t3465_1_0_0;
struct ReadOnlyCollection_1_t3465;
const Il2CppTypeDefinitionMetadata ReadOnlyCollection_1_t3465_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ReadOnlyCollection_1_t3465_InterfacesTypeInfos/* implementedInterfaces */
	, ReadOnlyCollection_1_t3465_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReadOnlyCollection_1_t3465_VTable/* vtableMethods */
	, ReadOnlyCollection_1_t3465_RGCTXData/* rgctxDefinition */
	, 6879/* fieldStart */
	, 10011/* methodStart */
	, -1/* eventStart */
	, 1885/* propertyStart */

};
TypeInfo ReadOnlyCollection_1_t3465_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReadOnlyCollection`1"/* name */
	, "System.Collections.ObjectModel"/* namespaze */
	, NULL/* methods */
	, &ReadOnlyCollection_1_t3465_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2686/* custom_attributes_cache */
	, &ReadOnlyCollection_1_t3465_0_0_0/* byval_arg */
	, &ReadOnlyCollection_1_t3465_1_0_0/* this_arg */
	, &ReadOnlyCollection_1_t3465_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 167/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 9/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// System.Collections.ArrayList
#include "mscorlib_System_Collections_ArrayList.h"
// Metadata Definition System.Collections.ArrayList
extern TypeInfo ArrayList_t436_il2cpp_TypeInfo;
// System.Collections.ArrayList
#include "mscorlib_System_Collections_ArrayListMethodDeclarations.h"
extern const Il2CppType SimpleEnumerator_t1649_0_0_0;
extern const Il2CppType ArrayListWrapper_t1650_0_0_0;
extern const Il2CppType SynchronizedArrayListWrapper_t1651_0_0_0;
extern const Il2CppType FixedSizeArrayListWrapper_t1652_0_0_0;
extern const Il2CppType ReadOnlyArrayListWrapper_t1653_0_0_0;
static const Il2CppType* ArrayList_t436_il2cpp_TypeInfo__nestedTypes[5] =
{
	&SimpleEnumerator_t1649_0_0_0,
	&ArrayListWrapper_t1650_0_0_0,
	&SynchronizedArrayListWrapper_t1651_0_0_0,
	&FixedSizeArrayListWrapper_t1652_0_0_0,
	&ReadOnlyArrayListWrapper_t1653_0_0_0,
};
static const EncodedMethodIndex ArrayList_t436_VTable[49] = 
{
	626,
	601,
	627,
	628,
	2968,
	2969,
	2970,
	2971,
	2972,
	2973,
	2974,
	2975,
	2976,
	2977,
	2978,
	2979,
	2980,
	2981,
	2982,
	2983,
	2975,
	2976,
	2969,
	2984,
	2985,
	2973,
	2974,
	2970,
	2971,
	2977,
	2978,
	2979,
	2980,
	2986,
	2987,
	2981,
	2988,
	2982,
	2983,
	2989,
	2972,
	2990,
	2968,
	2991,
	2992,
	2993,
	2994,
	2995,
	2996,
};
static const Il2CppType* ArrayList_t436_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICloneable_t3433_0_0_0,
	&ICollection_t1528_0_0_0,
	&IList_t1487_0_0_0,
};
static Il2CppInterfaceOffsetPair ArrayList_t436_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 5},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArrayList_t436_0_0_0;
extern const Il2CppType ArrayList_t436_1_0_0;
struct ArrayList_t436;
const Il2CppTypeDefinitionMetadata ArrayList_t436_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ArrayList_t436_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ArrayList_t436_InterfacesTypeInfos/* implementedInterfaces */
	, ArrayList_t436_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArrayList_t436_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6880/* fieldStart */
	, 10041/* methodStart */
	, -1/* eventStart */
	, 1894/* propertyStart */

};
TypeInfo ArrayList_t436_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayList"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &ArrayList_t436_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2687/* custom_attributes_cache */
	, &ArrayList_t436_0_0_0/* byval_arg */
	, &ArrayList_t436_1_0_0/* this_arg */
	, &ArrayList_t436_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArrayList_t436)/* instance_size */
	, sizeof (ArrayList_t436)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ArrayList_t436_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 39/* method_count */
	, 7/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 5/* nested_type_count */
	, 49/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Collections.ArrayList/SimpleEnumerator
#include "mscorlib_System_Collections_ArrayList_SimpleEnumerator.h"
// Metadata Definition System.Collections.ArrayList/SimpleEnumerator
extern TypeInfo SimpleEnumerator_t1649_il2cpp_TypeInfo;
// System.Collections.ArrayList/SimpleEnumerator
#include "mscorlib_System_Collections_ArrayList_SimpleEnumeratorMethodDeclarations.h"
static const EncodedMethodIndex SimpleEnumerator_t1649_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2997,
	2998,
};
static const Il2CppType* SimpleEnumerator_t1649_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&ICloneable_t3433_0_0_0,
};
static Il2CppInterfaceOffsetPair SimpleEnumerator_t1649_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SimpleEnumerator_t1649_1_0_0;
struct SimpleEnumerator_t1649;
const Il2CppTypeDefinitionMetadata SimpleEnumerator_t1649_DefinitionMetadata = 
{
	&ArrayList_t436_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, SimpleEnumerator_t1649_InterfacesTypeInfos/* implementedInterfaces */
	, SimpleEnumerator_t1649_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SimpleEnumerator_t1649_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6885/* fieldStart */
	, 10080/* methodStart */
	, -1/* eventStart */
	, 1901/* propertyStart */

};
TypeInfo SimpleEnumerator_t1649_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SimpleEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SimpleEnumerator_t1649_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SimpleEnumerator_t1649_0_0_0/* byval_arg */
	, &SimpleEnumerator_t1649_1_0_0/* this_arg */
	, &SimpleEnumerator_t1649_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SimpleEnumerator_t1649)/* instance_size */
	, sizeof (SimpleEnumerator_t1649)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SimpleEnumerator_t1649_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.ArrayList/ArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_ArrayListWrapper.h"
// Metadata Definition System.Collections.ArrayList/ArrayListWrapper
extern TypeInfo ArrayListWrapper_t1650_il2cpp_TypeInfo;
// System.Collections.ArrayList/ArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_ArrayListWrapperMethodDeclarations.h"
static const EncodedMethodIndex ArrayListWrapper_t1650_VTable[49] = 
{
	626,
	601,
	627,
	628,
	2999,
	3000,
	3001,
	3002,
	3003,
	3004,
	3005,
	3006,
	3007,
	3008,
	3009,
	3010,
	3011,
	3012,
	3013,
	3014,
	3006,
	3007,
	3000,
	3015,
	3016,
	3004,
	3005,
	3001,
	3002,
	3008,
	3009,
	3010,
	3011,
	3017,
	3018,
	3012,
	3019,
	3013,
	3014,
	3020,
	3003,
	3021,
	2999,
	3022,
	3023,
	3024,
	3025,
	3026,
	3027,
};
static Il2CppInterfaceOffsetPair ArrayListWrapper_t1650_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 5},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArrayListWrapper_t1650_1_0_0;
struct ArrayListWrapper_t1650;
const Il2CppTypeDefinitionMetadata ArrayListWrapper_t1650_DefinitionMetadata = 
{
	&ArrayList_t436_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArrayListWrapper_t1650_InterfacesOffsets/* interfaceOffsets */
	, &ArrayList_t436_0_0_0/* parent */
	, ArrayListWrapper_t1650_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6890/* fieldStart */
	, 10084/* methodStart */
	, -1/* eventStart */
	, 1902/* propertyStart */

};
TypeInfo ArrayListWrapper_t1650_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayListWrapper"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ArrayListWrapper_t1650_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2688/* custom_attributes_cache */
	, &ArrayListWrapper_t1650_0_0_0/* byval_arg */
	, &ArrayListWrapper_t1650_1_0_0/* this_arg */
	, &ArrayListWrapper_t1650_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArrayListWrapper_t1650)/* instance_size */
	, sizeof (ArrayListWrapper_t1650)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 7/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 49/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Collections.ArrayList/SynchronizedArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_SynchronizedArrayListW.h"
// Metadata Definition System.Collections.ArrayList/SynchronizedArrayListWrapper
extern TypeInfo SynchronizedArrayListWrapper_t1651_il2cpp_TypeInfo;
// System.Collections.ArrayList/SynchronizedArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_SynchronizedArrayListWMethodDeclarations.h"
static const EncodedMethodIndex SynchronizedArrayListWrapper_t1651_VTable[49] = 
{
	626,
	601,
	627,
	628,
	3028,
	3029,
	3030,
	3031,
	3032,
	3033,
	3034,
	3035,
	3036,
	3037,
	3038,
	3039,
	3040,
	3041,
	3042,
	3043,
	3035,
	3036,
	3029,
	3044,
	3045,
	3033,
	3034,
	3030,
	3031,
	3037,
	3038,
	3039,
	3040,
	3046,
	3047,
	3041,
	3048,
	3042,
	3043,
	3049,
	3032,
	3050,
	3028,
	3051,
	3052,
	3053,
	3054,
	3055,
	3056,
};
static Il2CppInterfaceOffsetPair SynchronizedArrayListWrapper_t1651_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 5},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SynchronizedArrayListWrapper_t1651_1_0_0;
struct SynchronizedArrayListWrapper_t1651;
const Il2CppTypeDefinitionMetadata SynchronizedArrayListWrapper_t1651_DefinitionMetadata = 
{
	&ArrayList_t436_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SynchronizedArrayListWrapper_t1651_InterfacesOffsets/* interfaceOffsets */
	, &ArrayListWrapper_t1650_0_0_0/* parent */
	, SynchronizedArrayListWrapper_t1651_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6891/* fieldStart */
	, 10114/* methodStart */
	, -1/* eventStart */
	, 1909/* propertyStart */

};
TypeInfo SynchronizedArrayListWrapper_t1651_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SynchronizedArrayListWrapper"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SynchronizedArrayListWrapper_t1651_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2689/* custom_attributes_cache */
	, &SynchronizedArrayListWrapper_t1651_0_0_0/* byval_arg */
	, &SynchronizedArrayListWrapper_t1651_1_0_0/* this_arg */
	, &SynchronizedArrayListWrapper_t1651_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SynchronizedArrayListWrapper_t1651)/* instance_size */
	, sizeof (SynchronizedArrayListWrapper_t1651)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057027/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 7/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 49/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Collections.ArrayList/FixedSizeArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_FixedSizeArrayListWrap.h"
// Metadata Definition System.Collections.ArrayList/FixedSizeArrayListWrapper
extern TypeInfo FixedSizeArrayListWrapper_t1652_il2cpp_TypeInfo;
// System.Collections.ArrayList/FixedSizeArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_FixedSizeArrayListWrapMethodDeclarations.h"
static const EncodedMethodIndex FixedSizeArrayListWrapper_t1652_VTable[50] = 
{
	626,
	601,
	627,
	628,
	2999,
	3000,
	3001,
	3002,
	3003,
	3057,
	3005,
	3006,
	3007,
	3058,
	3059,
	3010,
	3011,
	3060,
	3061,
	3062,
	3006,
	3007,
	3000,
	3063,
	3064,
	3057,
	3005,
	3001,
	3002,
	3058,
	3059,
	3010,
	3011,
	3017,
	3018,
	3060,
	3065,
	3061,
	3062,
	3020,
	3003,
	3021,
	2999,
	3066,
	3023,
	3024,
	3025,
	3026,
	3027,
	3067,
};
static Il2CppInterfaceOffsetPair FixedSizeArrayListWrapper_t1652_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 5},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FixedSizeArrayListWrapper_t1652_1_0_0;
struct FixedSizeArrayListWrapper_t1652;
const Il2CppTypeDefinitionMetadata FixedSizeArrayListWrapper_t1652_DefinitionMetadata = 
{
	&ArrayList_t436_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FixedSizeArrayListWrapper_t1652_InterfacesOffsets/* interfaceOffsets */
	, &ArrayListWrapper_t1650_0_0_0/* parent */
	, FixedSizeArrayListWrapper_t1652_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10144/* methodStart */
	, -1/* eventStart */
	, 1916/* propertyStart */

};
TypeInfo FixedSizeArrayListWrapper_t1652_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FixedSizeArrayListWrapper"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &FixedSizeArrayListWrapper_t1652_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FixedSizeArrayListWrapper_t1652_0_0_0/* byval_arg */
	, &FixedSizeArrayListWrapper_t1652_1_0_0/* this_arg */
	, &FixedSizeArrayListWrapper_t1652_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FixedSizeArrayListWrapper_t1652)/* instance_size */
	, sizeof (FixedSizeArrayListWrapper_t1652)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 50/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Collections.ArrayList/ReadOnlyArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_ReadOnlyArrayListWrapp.h"
// Metadata Definition System.Collections.ArrayList/ReadOnlyArrayListWrapper
extern TypeInfo ReadOnlyArrayListWrapper_t1653_il2cpp_TypeInfo;
// System.Collections.ArrayList/ReadOnlyArrayListWrapper
#include "mscorlib_System_Collections_ArrayList_ReadOnlyArrayListWrappMethodDeclarations.h"
static const EncodedMethodIndex ReadOnlyArrayListWrapper_t1653_VTable[50] = 
{
	626,
	601,
	627,
	628,
	2999,
	3000,
	3001,
	3002,
	3003,
	3057,
	3068,
	3069,
	3070,
	3058,
	3059,
	3010,
	3011,
	3060,
	3061,
	3062,
	3069,
	3070,
	3000,
	3063,
	3064,
	3057,
	3068,
	3001,
	3002,
	3058,
	3059,
	3010,
	3011,
	3017,
	3018,
	3060,
	3065,
	3061,
	3062,
	3020,
	3003,
	3021,
	2999,
	3066,
	3071,
	3072,
	3025,
	3026,
	3027,
	3073,
};
static Il2CppInterfaceOffsetPair ReadOnlyArrayListWrapper_t1653_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 5},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReadOnlyArrayListWrapper_t1653_1_0_0;
struct ReadOnlyArrayListWrapper_t1653;
const Il2CppTypeDefinitionMetadata ReadOnlyArrayListWrapper_t1653_DefinitionMetadata = 
{
	&ArrayList_t436_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReadOnlyArrayListWrapper_t1653_InterfacesOffsets/* interfaceOffsets */
	, &FixedSizeArrayListWrapper_t1652_0_0_0/* parent */
	, ReadOnlyArrayListWrapper_t1653_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10156/* methodStart */
	, -1/* eventStart */
	, 1919/* propertyStart */

};
TypeInfo ReadOnlyArrayListWrapper_t1653_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReadOnlyArrayListWrapper"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ReadOnlyArrayListWrapper_t1653_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2690/* custom_attributes_cache */
	, &ReadOnlyArrayListWrapper_t1653_0_0_0/* byval_arg */
	, &ReadOnlyArrayListWrapper_t1653_1_0_0/* this_arg */
	, &ReadOnlyArrayListWrapper_t1653_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReadOnlyArrayListWrapper_t1653)/* instance_size */
	, sizeof (ReadOnlyArrayListWrapper_t1653)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057027/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 50/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Collections.BitArray
#include "mscorlib_System_Collections_BitArray.h"
// Metadata Definition System.Collections.BitArray
extern TypeInfo BitArray_t1510_il2cpp_TypeInfo;
// System.Collections.BitArray
#include "mscorlib_System_Collections_BitArrayMethodDeclarations.h"
extern const Il2CppType BitArrayEnumerator_t1654_0_0_0;
static const Il2CppType* BitArray_t1510_il2cpp_TypeInfo__nestedTypes[1] =
{
	&BitArrayEnumerator_t1654_0_0_0,
};
static const EncodedMethodIndex BitArray_t1510_VTable[9] = 
{
	626,
	601,
	627,
	628,
	3074,
	3075,
	3076,
	3077,
	3078,
};
static const Il2CppType* BitArray_t1510_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICloneable_t3433_0_0_0,
	&ICollection_t1528_0_0_0,
};
static Il2CppInterfaceOffsetPair BitArray_t1510_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 5},
	{ &ICollection_t1528_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BitArray_t1510_0_0_0;
extern const Il2CppType BitArray_t1510_1_0_0;
struct BitArray_t1510;
const Il2CppTypeDefinitionMetadata BitArray_t1510_DefinitionMetadata = 
{
	NULL/* declaringType */
	, BitArray_t1510_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, BitArray_t1510_InterfacesTypeInfos/* implementedInterfaces */
	, BitArray_t1510_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BitArray_t1510_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6892/* fieldStart */
	, 10163/* methodStart */
	, -1/* eventStart */
	, 1922/* propertyStart */

};
TypeInfo BitArray_t1510_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BitArray"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &BitArray_t1510_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2691/* custom_attributes_cache */
	, &BitArray_t1510_0_0_0/* byval_arg */
	, &BitArray_t1510_1_0_0/* this_arg */
	, &BitArray_t1510_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BitArray_t1510)/* instance_size */
	, sizeof (BitArray_t1510)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.BitArray/BitArrayEnumerator
#include "mscorlib_System_Collections_BitArray_BitArrayEnumerator.h"
// Metadata Definition System.Collections.BitArray/BitArrayEnumerator
extern TypeInfo BitArrayEnumerator_t1654_il2cpp_TypeInfo;
// System.Collections.BitArray/BitArrayEnumerator
#include "mscorlib_System_Collections_BitArray_BitArrayEnumeratorMethodDeclarations.h"
static const EncodedMethodIndex BitArrayEnumerator_t1654_VTable[6] = 
{
	626,
	601,
	627,
	628,
	3079,
	3080,
};
static const Il2CppType* BitArrayEnumerator_t1654_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&ICloneable_t3433_0_0_0,
};
static Il2CppInterfaceOffsetPair BitArrayEnumerator_t1654_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BitArrayEnumerator_t1654_1_0_0;
struct BitArrayEnumerator_t1654;
const Il2CppTypeDefinitionMetadata BitArrayEnumerator_t1654_DefinitionMetadata = 
{
	&BitArray_t1510_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, BitArrayEnumerator_t1654_InterfacesTypeInfos/* implementedInterfaces */
	, BitArrayEnumerator_t1654_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BitArrayEnumerator_t1654_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6895/* fieldStart */
	, 10175/* methodStart */
	, -1/* eventStart */
	, 1927/* propertyStart */

};
TypeInfo BitArrayEnumerator_t1654_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BitArrayEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &BitArrayEnumerator_t1654_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BitArrayEnumerator_t1654_0_0_0/* byval_arg */
	, &BitArrayEnumerator_t1654_1_0_0/* this_arg */
	, &BitArrayEnumerator_t1654_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BitArrayEnumerator_t1654)/* instance_size */
	, sizeof (BitArrayEnumerator_t1654)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.CaseInsensitiveComparer
#include "mscorlib_System_Collections_CaseInsensitiveComparer.h"
// Metadata Definition System.Collections.CaseInsensitiveComparer
extern TypeInfo CaseInsensitiveComparer_t1530_il2cpp_TypeInfo;
// System.Collections.CaseInsensitiveComparer
#include "mscorlib_System_Collections_CaseInsensitiveComparerMethodDeclarations.h"
static const EncodedMethodIndex CaseInsensitiveComparer_t1530_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3081,
};
static const Il2CppType* CaseInsensitiveComparer_t1530_InterfacesTypeInfos[] = 
{
	&IComparer_t416_0_0_0,
};
static Il2CppInterfaceOffsetPair CaseInsensitiveComparer_t1530_InterfacesOffsets[] = 
{
	{ &IComparer_t416_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CaseInsensitiveComparer_t1530_0_0_0;
extern const Il2CppType CaseInsensitiveComparer_t1530_1_0_0;
struct CaseInsensitiveComparer_t1530;
const Il2CppTypeDefinitionMetadata CaseInsensitiveComparer_t1530_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CaseInsensitiveComparer_t1530_InterfacesTypeInfos/* implementedInterfaces */
	, CaseInsensitiveComparer_t1530_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CaseInsensitiveComparer_t1530_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6899/* fieldStart */
	, 10179/* methodStart */
	, -1/* eventStart */
	, 1928/* propertyStart */

};
TypeInfo CaseInsensitiveComparer_t1530_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CaseInsensitiveComparer"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &CaseInsensitiveComparer_t1530_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2692/* custom_attributes_cache */
	, &CaseInsensitiveComparer_t1530_0_0_0/* byval_arg */
	, &CaseInsensitiveComparer_t1530_1_0_0/* this_arg */
	, &CaseInsensitiveComparer_t1530_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CaseInsensitiveComparer_t1530)/* instance_size */
	, sizeof (CaseInsensitiveComparer_t1530)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CaseInsensitiveComparer_t1530_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Collections.CaseInsensitiveHashCodeProvider
#include "mscorlib_System_Collections_CaseInsensitiveHashCodeProvider.h"
// Metadata Definition System.Collections.CaseInsensitiveHashCodeProvider
extern TypeInfo CaseInsensitiveHashCodeProvider_t1531_il2cpp_TypeInfo;
// System.Collections.CaseInsensitiveHashCodeProvider
#include "mscorlib_System_Collections_CaseInsensitiveHashCodeProviderMethodDeclarations.h"
static const EncodedMethodIndex CaseInsensitiveHashCodeProvider_t1531_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3082,
};
extern const Il2CppType IHashCodeProvider_t1386_0_0_0;
static const Il2CppType* CaseInsensitiveHashCodeProvider_t1531_InterfacesTypeInfos[] = 
{
	&IHashCodeProvider_t1386_0_0_0,
};
static Il2CppInterfaceOffsetPair CaseInsensitiveHashCodeProvider_t1531_InterfacesOffsets[] = 
{
	{ &IHashCodeProvider_t1386_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CaseInsensitiveHashCodeProvider_t1531_0_0_0;
extern const Il2CppType CaseInsensitiveHashCodeProvider_t1531_1_0_0;
struct CaseInsensitiveHashCodeProvider_t1531;
const Il2CppTypeDefinitionMetadata CaseInsensitiveHashCodeProvider_t1531_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CaseInsensitiveHashCodeProvider_t1531_InterfacesTypeInfos/* implementedInterfaces */
	, CaseInsensitiveHashCodeProvider_t1531_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CaseInsensitiveHashCodeProvider_t1531_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6902/* fieldStart */
	, 10184/* methodStart */
	, -1/* eventStart */
	, 1929/* propertyStart */

};
TypeInfo CaseInsensitiveHashCodeProvider_t1531_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CaseInsensitiveHashCodeProvider"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &CaseInsensitiveHashCodeProvider_t1531_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2693/* custom_attributes_cache */
	, &CaseInsensitiveHashCodeProvider_t1531_0_0_0/* byval_arg */
	, &CaseInsensitiveHashCodeProvider_t1531_1_0_0/* this_arg */
	, &CaseInsensitiveHashCodeProvider_t1531_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CaseInsensitiveHashCodeProvider_t1531)/* instance_size */
	, sizeof (CaseInsensitiveHashCodeProvider_t1531)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CaseInsensitiveHashCodeProvider_t1531_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Collections.CollectionBase
#include "mscorlib_System_Collections_CollectionBase.h"
// Metadata Definition System.Collections.CollectionBase
extern TypeInfo CollectionBase_t1236_il2cpp_TypeInfo;
// System.Collections.CollectionBase
#include "mscorlib_System_Collections_CollectionBaseMethodDeclarations.h"
static const EncodedMethodIndex CollectionBase_t1236_VTable[29] = 
{
	626,
	601,
	627,
	628,
	2073,
	1735,
	1736,
	1737,
	1738,
	1739,
	1740,
	1741,
	1742,
	1743,
	1744,
	1745,
	1746,
	1747,
	1748,
	1749,
	1750,
	1751,
	1752,
	1753,
	1754,
	1755,
	1756,
	1757,
	1758,
};
static const Il2CppType* CollectionBase_t1236_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICollection_t1528_0_0_0,
	&IList_t1487_0_0_0,
};
static Il2CppInterfaceOffsetPair CollectionBase_t1236_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CollectionBase_t1236_1_0_0;
struct CollectionBase_t1236;
const Il2CppTypeDefinitionMetadata CollectionBase_t1236_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CollectionBase_t1236_InterfacesTypeInfos/* implementedInterfaces */
	, CollectionBase_t1236_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CollectionBase_t1236_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6905/* fieldStart */
	, 10191/* methodStart */
	, -1/* eventStart */
	, 1930/* propertyStart */

};
TypeInfo CollectionBase_t1236_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CollectionBase"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &CollectionBase_t1236_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2694/* custom_attributes_cache */
	, &CollectionBase_t1236_0_0_0/* byval_arg */
	, &CollectionBase_t1236_1_0_0/* this_arg */
	, &CollectionBase_t1236_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CollectionBase_t1236)/* instance_size */
	, sizeof (CollectionBase_t1236)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 8/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 29/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.CollectionDebuggerView
#include "mscorlib_System_Collections_CollectionDebuggerView.h"
// Metadata Definition System.Collections.CollectionDebuggerView
extern TypeInfo CollectionDebuggerView_t1655_il2cpp_TypeInfo;
// System.Collections.CollectionDebuggerView
#include "mscorlib_System_Collections_CollectionDebuggerViewMethodDeclarations.h"
static const EncodedMethodIndex CollectionDebuggerView_t1655_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CollectionDebuggerView_t1655_0_0_0;
extern const Il2CppType CollectionDebuggerView_t1655_1_0_0;
struct CollectionDebuggerView_t1655;
const Il2CppTypeDefinitionMetadata CollectionDebuggerView_t1655_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CollectionDebuggerView_t1655_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CollectionDebuggerView_t1655_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CollectionDebuggerView"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &CollectionDebuggerView_t1655_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CollectionDebuggerView_t1655_0_0_0/* byval_arg */
	, &CollectionDebuggerView_t1655_1_0_0/* this_arg */
	, &CollectionDebuggerView_t1655_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CollectionDebuggerView_t1655)/* instance_size */
	, sizeof (CollectionDebuggerView_t1655)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Comparer
#include "mscorlib_System_Collections_Comparer.h"
// Metadata Definition System.Collections.Comparer
extern TypeInfo Comparer_t1656_il2cpp_TypeInfo;
// System.Collections.Comparer
#include "mscorlib_System_Collections_ComparerMethodDeclarations.h"
static const EncodedMethodIndex Comparer_t1656_VTable[6] = 
{
	626,
	601,
	627,
	628,
	3083,
	3084,
};
static const Il2CppType* Comparer_t1656_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
	&IComparer_t416_0_0_0,
};
static Il2CppInterfaceOffsetPair Comparer_t1656_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &IComparer_t416_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Comparer_t1656_0_0_0;
extern const Il2CppType Comparer_t1656_1_0_0;
struct Comparer_t1656;
const Il2CppTypeDefinitionMetadata Comparer_t1656_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Comparer_t1656_InterfacesTypeInfos/* implementedInterfaces */
	, Comparer_t1656_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Comparer_t1656_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6906/* fieldStart */
	, 10219/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Comparer_t1656_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparer"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &Comparer_t1656_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2695/* custom_attributes_cache */
	, &Comparer_t1656_0_0_0/* byval_arg */
	, &Comparer_t1656_1_0_0/* this_arg */
	, &Comparer_t1656_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Comparer_t1656)/* instance_size */
	, sizeof (Comparer_t1656)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Comparer_t1656_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// Metadata Definition System.Collections.DictionaryEntry
extern TypeInfo DictionaryEntry_t552_il2cpp_TypeInfo;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntryMethodDeclarations.h"
static const EncodedMethodIndex DictionaryEntry_t552_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DictionaryEntry_t552_0_0_0;
extern const Il2CppType DictionaryEntry_t552_1_0_0;
const Il2CppTypeDefinitionMetadata DictionaryEntry_t552_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, DictionaryEntry_t552_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6909/* fieldStart */
	, 10224/* methodStart */
	, -1/* eventStart */
	, 1938/* propertyStart */

};
TypeInfo DictionaryEntry_t552_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DictionaryEntry"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &DictionaryEntry_t552_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2696/* custom_attributes_cache */
	, &DictionaryEntry_t552_0_0_0/* byval_arg */
	, &DictionaryEntry_t552_1_0_0/* this_arg */
	, &DictionaryEntry_t552_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DictionaryEntry_t552)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DictionaryEntry_t552)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Hashtable
#include "mscorlib_System_Collections_Hashtable.h"
// Metadata Definition System.Collections.Hashtable
extern TypeInfo Hashtable_t348_il2cpp_TypeInfo;
// System.Collections.Hashtable
#include "mscorlib_System_Collections_HashtableMethodDeclarations.h"
extern const Il2CppType Slot_t1657_0_0_0;
extern const Il2CppType KeyMarker_t1658_0_0_0;
extern const Il2CppType EnumeratorMode_t1659_0_0_0;
extern const Il2CppType Enumerator_t1660_0_0_0;
extern const Il2CppType HashKeys_t1661_0_0_0;
extern const Il2CppType HashValues_t1662_0_0_0;
static const Il2CppType* Hashtable_t348_il2cpp_TypeInfo__nestedTypes[6] =
{
	&Slot_t1657_0_0_0,
	&KeyMarker_t1658_0_0_0,
	&EnumeratorMode_t1659_0_0_0,
	&Enumerator_t1660_0_0_0,
	&HashKeys_t1661_0_0_0,
	&HashValues_t1662_0_0_0,
};
static const EncodedMethodIndex Hashtable_t348_VTable[36] = 
{
	626,
	601,
	627,
	628,
	3085,
	3086,
	3087,
	3088,
	3089,
	3090,
	3091,
	3092,
	3093,
	3094,
	3095,
	3096,
	3097,
	3087,
	3088,
	3089,
	3098,
	3099,
	3091,
	3092,
	3090,
	3093,
	3100,
	3094,
	3095,
	3096,
	3101,
	3102,
	3086,
	3097,
	3103,
	3104,
};
static const Il2CppType* Hashtable_t348_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICloneable_t3433_0_0_0,
	&ISerializable_t2210_0_0_0,
	&ICollection_t1528_0_0_0,
	&IDictionary_t1462_0_0_0,
	&IDeserializationCallback_t2213_0_0_0,
};
static Il2CppInterfaceOffsetPair Hashtable_t348_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 5},
	{ &ISerializable_t2210_0_0_0, 5},
	{ &ICollection_t1528_0_0_0, 6},
	{ &IDictionary_t1462_0_0_0, 10},
	{ &IDeserializationCallback_t2213_0_0_0, 16},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Hashtable_t348_0_0_0;
extern const Il2CppType Hashtable_t348_1_0_0;
struct Hashtable_t348;
const Il2CppTypeDefinitionMetadata Hashtable_t348_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Hashtable_t348_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Hashtable_t348_InterfacesTypeInfos/* implementedInterfaces */
	, Hashtable_t348_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Hashtable_t348_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6911/* fieldStart */
	, 10227/* methodStart */
	, -1/* eventStart */
	, 1940/* propertyStart */

};
TypeInfo Hashtable_t348_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Hashtable"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &Hashtable_t348_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2697/* custom_attributes_cache */
	, &Hashtable_t348_0_0_0/* byval_arg */
	, &Hashtable_t348_1_0_0/* this_arg */
	, &Hashtable_t348_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Hashtable_t348)/* instance_size */
	, sizeof (Hashtable_t348)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Hashtable_t348_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 44/* method_count */
	, 8/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 6/* nested_type_count */
	, 36/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// System.Collections.Hashtable/Slot
#include "mscorlib_System_Collections_Hashtable_Slot.h"
// Metadata Definition System.Collections.Hashtable/Slot
extern TypeInfo Slot_t1657_il2cpp_TypeInfo;
// System.Collections.Hashtable/Slot
#include "mscorlib_System_Collections_Hashtable_SlotMethodDeclarations.h"
static const EncodedMethodIndex Slot_t1657_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Slot_t1657_1_0_0;
const Il2CppTypeDefinitionMetadata Slot_t1657_DefinitionMetadata = 
{
	&Hashtable_t348_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Slot_t1657_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6925/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Slot_t1657_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Slot"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Slot_t1657_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Slot_t1657_0_0_0/* byval_arg */
	, &Slot_t1657_1_0_0/* this_arg */
	, &Slot_t1657_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Slot_t1657)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Slot_t1657)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057037/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Hashtable/KeyMarker
#include "mscorlib_System_Collections_Hashtable_KeyMarker.h"
// Metadata Definition System.Collections.Hashtable/KeyMarker
extern TypeInfo KeyMarker_t1658_il2cpp_TypeInfo;
// System.Collections.Hashtable/KeyMarker
#include "mscorlib_System_Collections_Hashtable_KeyMarkerMethodDeclarations.h"
static const EncodedMethodIndex KeyMarker_t1658_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType KeyMarker_t1658_1_0_0;
struct KeyMarker_t1658;
const Il2CppTypeDefinitionMetadata KeyMarker_t1658_DefinitionMetadata = 
{
	&Hashtable_t348_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, KeyMarker_t1658_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6927/* fieldStart */
	, 10271/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo KeyMarker_t1658_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyMarker"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &KeyMarker_t1658_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyMarker_t1658_0_0_0/* byval_arg */
	, &KeyMarker_t1658_1_0_0/* this_arg */
	, &KeyMarker_t1658_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyMarker_t1658)/* instance_size */
	, sizeof (KeyMarker_t1658)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(KeyMarker_t1658_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056773/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Hashtable/EnumeratorMode
#include "mscorlib_System_Collections_Hashtable_EnumeratorMode.h"
// Metadata Definition System.Collections.Hashtable/EnumeratorMode
extern TypeInfo EnumeratorMode_t1659_il2cpp_TypeInfo;
// System.Collections.Hashtable/EnumeratorMode
#include "mscorlib_System_Collections_Hashtable_EnumeratorModeMethodDeclarations.h"
static const EncodedMethodIndex EnumeratorMode_t1659_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair EnumeratorMode_t1659_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EnumeratorMode_t1659_1_0_0;
const Il2CppTypeDefinitionMetadata EnumeratorMode_t1659_DefinitionMetadata = 
{
	&Hashtable_t348_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EnumeratorMode_t1659_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, EnumeratorMode_t1659_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6928/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EnumeratorMode_t1659_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnumeratorMode"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EnumeratorMode_t1659_0_0_0/* byval_arg */
	, &EnumeratorMode_t1659_1_0_0/* this_arg */
	, &EnumeratorMode_t1659_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnumeratorMode_t1659)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EnumeratorMode_t1659)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.Hashtable/Enumerator
#include "mscorlib_System_Collections_Hashtable_Enumerator.h"
// Metadata Definition System.Collections.Hashtable/Enumerator
extern TypeInfo Enumerator_t1660_il2cpp_TypeInfo;
// System.Collections.Hashtable/Enumerator
#include "mscorlib_System_Collections_Hashtable_EnumeratorMethodDeclarations.h"
static const EncodedMethodIndex Enumerator_t1660_VTable[10] = 
{
	626,
	601,
	627,
	628,
	3105,
	3106,
	3107,
	3108,
	3109,
	3110,
};
static const Il2CppType* Enumerator_t1660_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDictionaryEnumerator_t1527_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t1660_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IDictionaryEnumerator_t1527_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Enumerator_t1660_1_0_0;
struct Enumerator_t1660;
const Il2CppTypeDefinitionMetadata Enumerator_t1660_DefinitionMetadata = 
{
	&Hashtable_t348_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t1660_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t1660_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerator_t1660_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6932/* fieldStart */
	, 10273/* methodStart */
	, -1/* eventStart */
	, 1948/* propertyStart */

};
TypeInfo Enumerator_t1660_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t1660_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t1660_0_0_0/* byval_arg */
	, &Enumerator_t1660_1_0_0/* this_arg */
	, &Enumerator_t1660_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t1660)/* instance_size */
	, sizeof (Enumerator_t1660)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Enumerator_t1660_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057027/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 4/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Hashtable/HashKeys
#include "mscorlib_System_Collections_Hashtable_HashKeys.h"
// Metadata Definition System.Collections.Hashtable/HashKeys
extern TypeInfo HashKeys_t1661_il2cpp_TypeInfo;
// System.Collections.Hashtable/HashKeys
#include "mscorlib_System_Collections_Hashtable_HashKeysMethodDeclarations.h"
static const EncodedMethodIndex HashKeys_t1661_VTable[14] = 
{
	626,
	601,
	627,
	628,
	3111,
	3112,
	3113,
	3114,
	3115,
	3112,
	3113,
	3114,
	3115,
	3111,
};
static const Il2CppType* HashKeys_t1661_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICollection_t1528_0_0_0,
};
static Il2CppInterfaceOffsetPair HashKeys_t1661_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HashKeys_t1661_1_0_0;
struct HashKeys_t1661;
const Il2CppTypeDefinitionMetadata HashKeys_t1661_DefinitionMetadata = 
{
	&Hashtable_t348_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, HashKeys_t1661_InterfacesTypeInfos/* implementedInterfaces */
	, HashKeys_t1661_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, HashKeys_t1661_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6940/* fieldStart */
	, 10282/* methodStart */
	, -1/* eventStart */
	, 1952/* propertyStart */

};
TypeInfo HashKeys_t1661_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HashKeys"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &HashKeys_t1661_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2708/* custom_attributes_cache */
	, &HashKeys_t1661_0_0_0/* byval_arg */
	, &HashKeys_t1661_1_0_0/* this_arg */
	, &HashKeys_t1661_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HashKeys_t1661)/* instance_size */
	, sizeof (HashKeys_t1661)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Hashtable/HashValues
#include "mscorlib_System_Collections_Hashtable_HashValues.h"
// Metadata Definition System.Collections.Hashtable/HashValues
extern TypeInfo HashValues_t1662_il2cpp_TypeInfo;
// System.Collections.Hashtable/HashValues
#include "mscorlib_System_Collections_Hashtable_HashValuesMethodDeclarations.h"
static const EncodedMethodIndex HashValues_t1662_VTable[14] = 
{
	626,
	601,
	627,
	628,
	3116,
	3117,
	3118,
	3119,
	3120,
	3117,
	3118,
	3119,
	3120,
	3116,
};
static const Il2CppType* HashValues_t1662_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICollection_t1528_0_0_0,
};
static Il2CppInterfaceOffsetPair HashValues_t1662_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HashValues_t1662_1_0_0;
struct HashValues_t1662;
const Il2CppTypeDefinitionMetadata HashValues_t1662_DefinitionMetadata = 
{
	&Hashtable_t348_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, HashValues_t1662_InterfacesTypeInfos/* implementedInterfaces */
	, HashValues_t1662_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, HashValues_t1662_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6941/* fieldStart */
	, 10288/* methodStart */
	, -1/* eventStart */
	, 1955/* propertyStart */

};
TypeInfo HashValues_t1662_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HashValues"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &HashValues_t1662_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2709/* custom_attributes_cache */
	, &HashValues_t1662_0_0_0/* byval_arg */
	, &HashValues_t1662_1_0_0/* this_arg */
	, &HashValues_t1662_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HashValues_t1662)/* instance_size */
	, sizeof (HashValues_t1662)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.IComparer
extern TypeInfo IComparer_t416_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IComparer_t416_1_0_0;
struct IComparer_t416;
const Il2CppTypeDefinitionMetadata IComparer_t416_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10294/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IComparer_t416_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparer"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &IComparer_t416_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2710/* custom_attributes_cache */
	, &IComparer_t416_0_0_0/* byval_arg */
	, &IComparer_t416_1_0_0/* this_arg */
	, &IComparer_t416_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
