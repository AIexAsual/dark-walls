﻿#pragma once
#include <stdint.h>
// System.MonoTypeInfo
struct MonoTypeInfo_t2122;
// System.Type
#include "mscorlib_System_Type.h"
// System.MonoType
struct  MonoType_t  : public Type_t
{
	// System.MonoTypeInfo System.MonoType::type_info
	MonoTypeInfo_t2122 * ___type_info_8;
};
