﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>
struct ReadOnlyCollection_1_t2450;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<iTween/EaseType>
struct IList_1_t569;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// iTween/EaseType[]
struct EaseTypeU5BU5D_t451;
// System.Collections.Generic.IEnumerator`1<iTween/EaseType>
struct IEnumerator_1_t3040;
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m16412_gshared (ReadOnlyCollection_1_t2450 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m16412(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2450 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m16412_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m16413_gshared (ReadOnlyCollection_1_t2450 * __this, int32_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m16413(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2450 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m16413_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m16414_gshared (ReadOnlyCollection_1_t2450 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m16414(__this, method) (( void (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m16414_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m16415_gshared (ReadOnlyCollection_1_t2450 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m16415(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2450 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m16415_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m16416_gshared (ReadOnlyCollection_1_t2450 * __this, int32_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m16416(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2450 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m16416_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m16417_gshared (ReadOnlyCollection_1_t2450 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m16417(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2450 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m16417_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m16418_gshared (ReadOnlyCollection_1_t2450 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m16418(__this, ___index, method) (( int32_t (*) (ReadOnlyCollection_1_t2450 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m16418_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m16419_gshared (ReadOnlyCollection_1_t2450 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m16419(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2450 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m16419_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16420_gshared (ReadOnlyCollection_1_t2450 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16420(__this, method) (( bool (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16420_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m16421_gshared (ReadOnlyCollection_1_t2450 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m16421(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2450 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m16421_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m16422_gshared (ReadOnlyCollection_1_t2450 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m16422(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m16422_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m16423_gshared (ReadOnlyCollection_1_t2450 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m16423(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2450 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m16423_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m16424_gshared (ReadOnlyCollection_1_t2450 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m16424(__this, method) (( void (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m16424_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m16425_gshared (ReadOnlyCollection_1_t2450 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m16425(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2450 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m16425_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m16426_gshared (ReadOnlyCollection_1_t2450 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m16426(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2450 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m16426_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m16427_gshared (ReadOnlyCollection_1_t2450 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m16427(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2450 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m16427_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m16428_gshared (ReadOnlyCollection_1_t2450 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m16428(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2450 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m16428_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m16429_gshared (ReadOnlyCollection_1_t2450 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m16429(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2450 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m16429_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m16430_gshared (ReadOnlyCollection_1_t2450 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m16430(__this, method) (( bool (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m16430_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m16431_gshared (ReadOnlyCollection_1_t2450 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m16431(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m16431_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m16432_gshared (ReadOnlyCollection_1_t2450 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m16432(__this, method) (( bool (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m16432_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m16433_gshared (ReadOnlyCollection_1_t2450 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m16433(__this, method) (( bool (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m16433_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m16434_gshared (ReadOnlyCollection_1_t2450 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m16434(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2450 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m16434_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m16435_gshared (ReadOnlyCollection_1_t2450 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m16435(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2450 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m16435_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m16436_gshared (ReadOnlyCollection_1_t2450 * __this, int32_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m16436(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2450 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m16436_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m16437_gshared (ReadOnlyCollection_1_t2450 * __this, EaseTypeU5BU5D_t451* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m16437(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2450 *, EaseTypeU5BU5D_t451*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m16437_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m16438_gshared (ReadOnlyCollection_1_t2450 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m16438(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m16438_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m16439_gshared (ReadOnlyCollection_1_t2450 * __this, int32_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m16439(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2450 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m16439_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m16440_gshared (ReadOnlyCollection_1_t2450 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m16440(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2450 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m16440_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>::get_Item(System.Int32)
extern "C" int32_t ReadOnlyCollection_1_get_Item_m16441_gshared (ReadOnlyCollection_1_t2450 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m16441(__this, ___index, method) (( int32_t (*) (ReadOnlyCollection_1_t2450 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m16441_gshared)(__this, ___index, method)
