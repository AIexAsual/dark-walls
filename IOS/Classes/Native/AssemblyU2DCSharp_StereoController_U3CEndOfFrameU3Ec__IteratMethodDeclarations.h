﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// StereoController/<EndOfFrame>c__Iterator2
struct U3CEndOfFrameU3Ec__Iterator2_t264;
// System.Object
struct Object_t;

// System.Void StereoController/<EndOfFrame>c__Iterator2::.ctor()
extern "C" void U3CEndOfFrameU3Ec__Iterator2__ctor_m1602 (U3CEndOfFrameU3Ec__Iterator2_t264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StereoController/<EndOfFrame>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CEndOfFrameU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1603 (U3CEndOfFrameU3Ec__Iterator2_t264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object StereoController/<EndOfFrame>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CEndOfFrameU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1604 (U3CEndOfFrameU3Ec__Iterator2_t264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StereoController/<EndOfFrame>c__Iterator2::MoveNext()
extern "C" bool U3CEndOfFrameU3Ec__Iterator2_MoveNext_m1605 (U3CEndOfFrameU3Ec__Iterator2_t264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StereoController/<EndOfFrame>c__Iterator2::Dispose()
extern "C" void U3CEndOfFrameU3Ec__Iterator2_Dispose_m1606 (U3CEndOfFrameU3Ec__Iterator2_t264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StereoController/<EndOfFrame>c__Iterator2::Reset()
extern "C" void U3CEndOfFrameU3Ec__Iterator2_Reset_m1607 (U3CEndOfFrameU3Ec__Iterator2_t264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
