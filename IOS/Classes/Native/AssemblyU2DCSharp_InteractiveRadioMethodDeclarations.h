﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractiveRadio
struct InteractiveRadio_t340;

// System.Void InteractiveRadio::.ctor()
extern "C" void InteractiveRadio__ctor_m1988 (InteractiveRadio_t340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveRadio::Awake()
extern "C" void InteractiveRadio_Awake_m1989 (InteractiveRadio_t340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveRadio::Update()
extern "C" void InteractiveRadio_Update_m1990 (InteractiveRadio_t340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveRadio::SetGazedAt(System.Boolean)
extern "C" void InteractiveRadio_SetGazedAt_m1991 (InteractiveRadio_t340 * __this, bool ___gazeAt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveRadio::changeClip(System.Int32)
extern "C" void InteractiveRadio_changeClip_m1992 (InteractiveRadio_t340 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
